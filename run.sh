#!/bin/bash
docker run -it --rm -e DISPLAY=$DISPLAY -e UID=$(id -u) -e GID=$(id -g) -e USER=$(whoami) -v /tmp/.X11-unix:/tmp/.X11-unix:ro -v /dev:/dev -v /home:/home:z  --name="beatle3-alpine" beatle3.alpine.naked
# -v ~:/root
