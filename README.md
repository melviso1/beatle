# beatle
Inspired on modelling, a python development environment. Currently, supporting C++ and phyton.

# install
## Windows
### Prerequisites
    * Git for windows (mandatory)
    * doxygen (if you wish to generate documentation from your code)
    * gcc build tools (MSYS2, mingwin or cygwin) 
    
you can found a installers in the Windows installers folder:
    * SetupBeatle3.0.1 ... experimental install including python 3.8 

## linux (tested last time on Ubuntu 20.04)

### ensure not apt lock 

    sudo systemctl stop unattended-upgrades.service
    sudo apt-get update

    sudo apt-get -y install build-essential libgtk-3-dev libgl-dev libjpeg-dev libgstreamer1.0-dev \
        libwebkit2gtk-4.0-dev libgstreamermm-1.0-dev libstdc++-10-dev libtiff-dev libboost-all-dev \
        freeglut3-dev libsdl-dev libsdl2-dev libinotifytools0-dev libcppunit-dev \
        pybind11-dev libmsgpack-dev git python3-pip

### local install

    sudo python3 -m pip install pathlib2==2.3.5 pywheel==0.1 pyembed==1.3.3 \
	    GitPython==3.1.11 trepan3k==1.2.0 watchdog==1.0.2 PyPDF2==1.26.0 \
	    appdirs setuptools six twine sphinx requests \
	    pytest pytest-xdist  pytest-timeout numpy pillow 

### custom install of wxPython because we want to have both the wxWidgets and the wxPython  installed as useful for further developments

    mkdir -p ./temp
    cd ./temp
    python3 -m pip download --no-deps wxPython==4.0.5
    tar -xzvf wxPython-4.0.5.tar.gz
    cd wxPython-4.0.5
    python3 -m build build_wx
    sudo python3 -m build install_wx --no_magic
    python3 -m build build_py
    sudo python3 -m build install_py
    cd ../..
    rm -rf temp
    sudo python3 -m pip install "requests[security]"
    cd /usr/local/lib/python3.8/
    sudo git clone --depth 1 https://gitlab.com/melviso1/beatle.git -b beatle3
    sudo mv beatle/beatle dist-packages
    sudo cp start_beatle.py dist-packages
    sudo rm -rf beatle

### restart upgrades

    sudo systemctl start unattended-upgrades.service

### test launch

    python3 start_beatle.py


