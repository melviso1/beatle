#!/bin/sh
				echo 'Prerrequisites'
				apt-get --yes --force-yes install libgconf2-dev libgtk2.0-dev libgtk-3-dev mesa-common-dev libgl1-mesa-dev libglu1-mesa-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libgconfmm-2.6-dev libwebkitgtk-dev python-gtk2
				mkdir -p external
				cd external
				echo "Downloading wxPython 3.0.2.0"
				wget http://downloads.sourceforge.net/project/wxpython/wxPython/3.0.2.0/wxPython-src-3.0.2.0.tar.bz2
				echo "Done"
				echo 'Uncompressing ...'
				tar -xjvf wxPython-src-3.0.2.0.tar.bz2
				echo 'Patching  ...'
				cd wxPython-src-3.0.2.0
				sed -i '1i \#include <math.h>' ./src/stc/scintilla/src/Editor.cxx
				cd wxPython
				sed -i -e 's/PyErr_Format(PyExc_RuntimeError, mesg)/PyErr_Format(PyExc_RuntimeError, "%s", mesg)/g' src/gtk/*.cpp contrib/gizmos/gtk/*.cpp
				# gstreamer continues to break compilation:
				sed -i -e 's/enable-mediactrl/disable-mediactrl/g' ../build/tools/build-wxwidgets.py
				python ./build-wxpython.py --build_dir=../bld --install
				ldconfig
				cd ../../..
				#rm -rf external
			
