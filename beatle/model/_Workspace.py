# -*- coding: utf-8 -*-

import copy

from beatle.lib.api import context
from beatle.lib.decorators import upgrade_version
from beatle.model import TComponent,Project


class Workspace(TComponent):
    """Representation of Workspace"""

    #class_container = False
    #folder_container = False
    #diagram_container = True
    #module_container = False
    #namespace_container = False
    project_container = True
    repository_container = True
    _dir = None  # used for pickle modification on load

    def __init__(self, **kwargs):
        """Initialization of workspace"""
        self._dir = kwargs['dir']
        self._author = kwargs.get('author', "<unknown>")
        self._date = kwargs.get('date', "08-10-2966")
        self._license = kwargs.get('license', None)
        self._modified = True
        self._lastHdrTime = None
        super(Workspace, self).__init__(**kwargs)

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        super(Workspace, self).on_undo_redo_removing()
        context.get_app().remove_workspace(self)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        context.get_app().add_workspace(self)
        super(Workspace, self).on_undo_redo_add()

    def get_tab_bitmap(self):
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc        
        return rc.get_bitmap('workspace')

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc        
        return rc.get_bitmap_index('workspace')

    @property
    def dir(self):
        return self._dir

    @dir.setter
    def dir(self, value):
        self._dir = value
    
    def on_undo_redo_unloaded(self):
        """Remove object before unload"""
        context.get_app().remove_workspace(self)
        super(Workspace, self).on_undo_redo_unloaded()

    def __getstate__(self):
        """Set pickle context"""
        # we dont want to serialize projects directly
        state = copy.copy(self.__dict__)
        state['_child'] = copy.copy(self._child)
        state['_child'][Project] = [
            (x._dir.replace("//", "/") + "/" + x._name + ".pcc").replace("//", "/")
            for x in self[Project] if type(x) is Project]
        return state

    def __setstate__(self, d):
        """Load pickle context"""
        plist = d['_child'].get(Project, None)
        # The current dir and the stored dir may be not the same
        if Workspace._dir and Workspace._dir != d['_dir']:
            old_dir = d['_dir']
            d['_dir'] = Workspace._dir
        else:
            old_dir = None
        if plist:
            del d['_child'][Project]
        self.__dict__ = d
        if plist:
            app = context.get_app()
            for pf in plist:
                if old_dir:
                    pf = pf.replace(old_dir, self._dir, 1)
                app.load_project(pf, self, skip_compare=True)

        @upgrade_version
        def set_state(this, data_dict):
            return {
                'rename': {'_readOnly': '_read_only'},
                'add': {'_child_index': -1},
            }

        set_state(self,d)

    @property
    def modified(self):
        """Get the modified state"""
        return self._modified

    @modified.setter
    def modified(self, value):
        """Set the modified state"""
        self._lastHdrTime = None
        self._modified = value

