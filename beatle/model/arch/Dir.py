# -*- coding: utf-8 -*-

import re,os

from beatle.model import TCommon
from beatle.lib.tran import TransactionFSObject
from .File import File


class Dir(TCommon, TransactionFSObject):
    """Declares a file with transactional support"""
    def __init__(self, **kwargs):
        """init"""
        if 'name' not in kwargs:
            kwargs['name'] = os.path.split(kwargs['file'])[1]
        kwargs['uri'] = kwargs['parent'].project
        super(Dir, self).__init__(**kwargs)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("folder")

    @property
    def bitmap_open_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("folder_open")

    def Refresh(self):
        """Refresh the files contained in a sngle projec"""
        # The project itself represents the root directory
        # so we need to scan the root dir

        _dir = self.abs_file
        if not os.access(_dir, os.R_OK):
            return False
        # list all files excluding hidden files
        mask_files = [r'\.(.)*', r'__pycache__', r'.*\.pyc', r'(.)*\.pcc']
        excluded = [re.compile(x) for x in mask_files]
        entries = os.listdir(_dir)
        entries.sort()
        _fset = [s for s in entries if not any(y.match(s) for y in excluded)]
        knowns = self[File] + self[Dir]
        name_knowns = dict([(x.abs_file, x) for x in knowns])
        kwargs = {'parent': self, 'transactional': False}
        for f in _fset:
            element = os.path.join(_dir, f)
            if element in name_knowns:
                name_knowns[element].Refresh()
                del name_knowns[element]
                continue
            kwargs['file'] = element
            if os.path.isdir(element):
                # don't show symbolic linked dirs
                if not os.path.islink(element):
                    Dir(**kwargs).Refresh()
            else:
                File(**kwargs)
        for k in name_knowns:
            name_knowns[k].delete()

    def __setstate__(self, state):
        super(Dir, self).__setstate__(state)
        TransactionFSObject.__setstate__(self, state)
