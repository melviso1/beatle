# -*- coding: utf-8 -*-

import re,os,copy

import wx
from .._TCommon import TCommon
from beatle.model import cc, py
from beatle.lib.decorators import upgrade_version
from beatle.lib.tran import TransactionFSObject
from beatle.lib.ostools import MimeHandler


class File(TCommon, TransactionFSObject):
    """Declares a file with transactional support"""
    def __init__(self, **kwargs):
        """init"""
        _file = kwargs['file']
        if not os.path.exists(_file):
            try:
                with open(_file, 'a'):
                    os.utime(_file, None)
            except:
                pass
        if 'name' not in kwargs:
            kwargs['name'] = os.path.split(_file)[1]
        kwargs['uri'] = kwargs['parent'].project
        super(File, self).__init__(**kwargs)
        self._generator = None
        self.refresh_generator()
        if self._generator:
            self._read_only = type(self._generator) not in [py.Module]

    def translate_open_line(self, line):
        """Attempts to translate open to element"""
        # return False
        forward = getattr(self._generator, 'open_line', None)
        if forward is not None:
            return forward(line)
        return False

    @property
    def breakpoints(self):
        """return the breakpoints attached to the current file"""
        return self.project.breakpoints(self)

    @property
    def bookmarks(self):
        """return the bookmarks attached to the current file"""
        return self.project.bookmarks(self)

    @property
    def read_only(self):
        return self._read_only

    def Refresh(self):
        """refresh file"""
        if not hasattr(self, '_generator'):
            self._generator = None
        self.refresh_generator()
        self._read_only = bool(self._generator and
            type(self._generator) not in [py.Module])

    def GetText(self):
        """Returns file contents"""
        with open(self.abs_file, "r") as f:
            data = f.read()
        return data

    def SetText(self, text):
        """Change text contents"""
        with open(self.abs_file, "w") as f:
            f.truncate(0)
            f.write(text)
        if not self._generator:
            return
        # if the generator is python, it may be a module or package
        # ** The reverse engineering must be completely disabled
        # ** until the analyze is fixed. For the moment, if you
        # ** change the source this way, it would be rewritten later.
        # if type(self._generator) is py.Module:
        #     self._generator.save_state()
        #     self._generator._content = text
        #     self._generator.analyze()

    def refresh_generator(self):
        """Any file may correspond to any generator that resides in a model.
        This method finds the responsible generator and do alink with it"""
        project = self.project
        r = os.path.realpath
        j = os.path.join
        if project.language == 'c++':
            if re.match(r'ppc\.(.)', self._file[::-1]):
                if self._generator and self._generator.abs_path_source == self.abs_file:
                    self._generator.set_source(self)  # fix missing reference
                    return
                s = project.sources_dir
                file_owners = project.level_classes + project.modules
                cpp_set = dict([(c.abs_path_source,c) for c in file_owners])
                generator = cpp_set.get(self.abs_file, None)
                if generator is not self._generator:
                    if self._generator:
                        self._generator.set_source(None)
                    self._generator = generator
                    if self._generator:
                        self._generator.set_source(self)
            elif re.match(r'h\.(.)*', self._file[::-1]):
                if self._generator and self._generator.abs_path_header == self.abs_file:
                    self._generator.set_header(self)  # fix missing reference
                    return
                try:
                    h_set = project.h_set
                except:
                    s = project.headers_dir
                    plc = project.level_classes
                    h_set = dict([(r(j(s, '{0}.h'.format(c.base_filename))), c) for c in plc])
                generator = h_set.get(self.abs_file, None)
                if generator is not self._generator:
                    if self._generator:
                        self._generator.set_header(None)
                    self._generator = generator
                    if self._generator:
                        self._generator.set_header(self)
        elif project.language == 'python':
            if re.match(r'yp\.(.)*', self._file[::-1]):
                if self._generator and self._generator.abs_path == self.abs_file:
                    return
                try:
                    file_dict = project.file_dict
                except:
                    file_dict = dict([(r(j(x.dir, '{x._name}.py'.format(x=x))), x)
                        for x in project.modules])
                    file_dict.update([(r(j(x.dir, '__init__.py')), x)
                        for x in project.packages])
                generator = file_dict.get(self.abs_file, None)
                if generator is not self._generator:
                    if self._generator:
                        self._generator.set_file(None)
                    self._generator = generator
                    if self._generator:
                        self._generator.set_file(self)

    @property
    def bitmap_index(self):
        """Returns tree image id"""
        # this is a little hack because in undo/redo as the file couldn't exists yet
        # mime handlers could fail
        try:
            index = getattr(self,'_bitmap_index', None)
            if index is None or index == wx.NOT_FOUND:
                index = MimeHandler.file_image_index(self.abs_file)
                setattr(self, '_bitmap_index', index)
            return index
        except:
            return wx.NOT_FOUND

    def __getstate__(self):
        """Set pickle context"""
        state = dict(super(File, self).__getstate__())
        state['_version'] = 0
        if '_bitmap_index' in state:
            del state['_bitmap_index']
        return state

    @upgrade_version
    def __setstate__(self, state):
        super(File, self).__setstate__(state)
        TransactionFSObject.__setstate__(self, state)
        return {
            'rename': {'_readOnly': '_read_only'},
            'delete': ['_bitmap_index'],
            'add': {'_child_index': -1},
        }

    @property
    def is_binary(self):
        """Check if this is a binary file"""
        valid_chars = set((7, 8, 9, 10, 19, 12, 13, 27)) | set(range(32, 256)) - set([127])
        text_chars = bytearray(valid_chars)

        def is_binary_string(_bytes):
            return bool(_bytes.translate(None, text_chars))

        try:
            with open(self.abs_file, "rb") as f:
                for line in f:
                    if is_binary_string(line):
                        return True
        except Exception as e:
            pass
        return False

    def delete(self):
        """Delete """
        # new version
        if self._generator:
            if type(self._generator) is cc.Class:
                if hasattr(self._generator, '_header_obj') and self._generator._header_obj == self:
                    self._generator.set_header(None)
                if hasattr(self._generator, '_source_obj') and self._generator._source_obj == self:
                    self._generator.set_source(None)
        super(File, self).delete()

