# -*- coding: utf-8 -*-
from beatle.lib.tran import TransactionStack,TransactionalMethod, TransactionalMoveObject
from ._Member import Member

# TODO : cambiar _scope por _belongs : el termino scope se confunde con visibilidad (namespace container)


class MemberData(Member):
    """Implements member data"""
    context_container = True

    # visual methods
    @TransactionalMethod('move member {0}')
    def drop(self, to):
        """Drops data-member inside project or another folder """
        target = to.inner_member_container
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._scope = kwargs.get('scope', "class")
        self._value = kwargs.get('value', "")
        super(MemberData, self).__init__(**kwargs)
        if self._scope != 'class' and kwargs.get('update_ctors', True):
            self.inner_class.auto_init()
        self.inner_code_generator.export_code_files()

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'scope': self._scope,
            'value': self._value
        }
        kwargs.update(super(MemberData, self).get_kwargs())
        return kwargs

    def get_initializer(self):
        """Return the initializer sequence"""
        if len(self._value) > 0:
            return self._value
        return '{self._name}=None'.format(self=self)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_member")

    def delete(self):
        """Remove object"""
        k = self.inner_module or self.inner_package
        super(MemberData, self).delete()
        if k:
            k.export_code_files()

    def remove_relations(self):
        cls = self.inner_class
        super(MemberData, self).remove_relations()
        if cls is not None and not TransactionStack.in_undo_redo():
            if self._scope != 'class':
                cls.auto_init_remove(self)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(MemberData, self).on_undo_redo_add()

    def on_undo_redo_changed(self):
        """Make changes in the model as result of change"""
        super(MemberData, self).on_undo_redo_changed()
        if self._scope != 'class':
            self.inner_class.auto_init()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def export_python_code(self, wf):
        """Write code"""
        if self._scope == 'class':
            if len(self._value):
                wf.write_line('{self._name} = {self._value}'.format(self=self))
            else:
                wf.write_line('{self._name} = None'.format(self=self))
