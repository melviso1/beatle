# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:31:28 2013

@author: mel
"""
from beatle.lib.tran import TransactionStack
from ._PyTComponent import PyTComponent
from ._Argument import Argument
from ._ArgsArgument import ArgsArgument
from ._KwArgsArgument import KwArgsArgument


class Decorator(PyTComponent):
    """Implements Decorator representation"""
    argument_container = True

    def __init__(self, **kwargs):
        """Initialization"""
        from ._MemberMethod import MemberMethod
        self._call = kwargs.get("call", False)
        super(Decorator, self).__init__(**kwargs)
        container = self.outer_class or self.outer_module
        container._lastSrcTime = None
        container._lastHdrTime = None
        parent = self.parent
        if type(parent) is MemberMethod:
            if self.name == 'staticmethod':
                parent._staticmethod = self
            elif self._name == 'classmethod':
                parent._classmethod = self
            elif self._name == 'property':
                parent._property = self
        k = self.inner_module or self.inner_package
        if k:
            k.export_code_files()

    def delete(self):
        """Handle delete"""
        k = self.inner_module or self.inner_package
        super(Decorator, self).delete()
        if k:
            k.export_code_files()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        return super(Decorator, self).get_kwargs()

    def on_undo_redo_changed(self):
        """Update from app"""
        self.parent.on_undo_redo_changed()
        super(Decorator, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def on_undo_redo_removing(self):
        """Do required actions for removing"""
        from ._MemberMethod import MemberMethod
        parent = self.parent
        if type(parent) is MemberMethod:
            if parent._staticmethod == self:
                parent._staticmethod = None
            elif parent._classmethod == self:
                parent._classmethod = None
            elif parent._property == self:
                parent._property = None
        super(Decorator, self).on_undo_redo_removing()
        self.parent.on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        from ._MemberMethod import MemberMethod
        super(Decorator, self).on_undo_redo_add()
        parent = self.parent
        if type(parent) is MemberMethod:
            if self.name == 'staticmethod':
                parent._staticmethod = self
            elif self._name == 'classmethod':
                parent._classmethod = self
            elif self._name == 'property':
                parent._property = self
        self.parent.on_undo_redo_changed()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('decorator')

    def export_python_code(self, wf):
        """Export python code"""
        wf.write_line(self.label)

    @property
    def label(self):
        """Get tree label"""
        if self._call:
            arg_list = [x for x in self.sorted_child if type(x) is Argument]
            arg_list = ', '.join(arg.label for arg in arg_list + self[ArgsArgument] + self[KwArgsArgument])
            return '@{self._name}({arg_list})'.format(self=self, arg_list=arg_list)
        else:
            return '@{self._name}'.format(self=self)

