# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 08:28:25 2013

@author: mel
"""


from ._PyTComponent import PyTComponent


class Member(PyTComponent):
    """Implements member"""
    def __init__(self, **kwargs):
        """Initialization"""
        super(Member, self).__init__(**kwargs)
        top_class = self.outer_class
        top_class._lastSrcTime = None
        top_class._lastHdrTime = None

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        return super(Member, self).get_kwargs()
