# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:08:46 2013

@author: mel
"""
from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject, \
    format_current_transaction_name
from beatle.lib.decorators import upgrade_version
from beatle.lib.api import context

from ._Member import Member
from ._Argument import Argument
from ._Decorator import Decorator
from ._ArgsArgument import ArgsArgument
from ._KwArgsArgument import KwArgsArgument


class MemberMethod(Member):
    """Implements member method"""
    context_container = True
    argument_container = True
    import_container = True

    # visual methods
    @TransactionalMethod('move method {0}')
    def drop(self, to):
        """Drops data-member inside project or another folder """
        target = to.inner_member_container
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._staticmethod = kwargs.get('static_method', False)
        self._classmethod = kwargs.get('class_method', False)
        self._property = kwargs.get('property', False)
        self._content = kwargs.get('content', "")
        super(MemberMethod, self).__init__(**kwargs)
        self._read_only_content = kwargs.get('read_only_content', self.read_only)
        if not self._staticmethod and not kwargs.get('raw', False):
            if self._property or not self._classmethod:
                if not self.exists_named_argument('self'):
                    Argument(parent=self, name='self')
            else:
                if not self.exists_named_argument('self'):
                    Argument(parent=self, name='cls')
        if self._staticmethod:
            self._staticmethod = Decorator(parent=self, name='staticmethod')
        if self._classmethod:
            self._classmethod = Decorator(parent=self, name='classmethod')
        if self._property:
            self._property = Decorator(parent=self, name='property')
        k = self.inner_module or self.inner_package
        if k:
            k.export_code_files()

    @property
    def content(self):
        return self._content

    @TransactionalMethod('update code of {0}')
    def set_content(self, value):
        self.save_state()
        self._content = value
        format_current_transaction_name(self._name)
        return True

    def delete(self):
        """Handle delete"""
        k = self.inner_module or self.inner_package
        super(MemberMethod, self).delete()
        if k:
            k.export_code_files()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'static_method':  bool(self._staticmethod),
            'class_method': bool(self._classmethod),
            'property': bool(self._property),
            'content': self._content,
            'read_only_content': self._read_only_content
        }
        kwargs.update(super(MemberMethod, self).get_kwargs())
        return kwargs

    @property
    def code_lines(self):
        """Return the number of line codes.
        This is used for mapping from file to object.
        """
        return len(self._content.splitlines())

    def write_code(self, f):
        """Write code to file"""
        for deco in self[Decorator]:
            deco.export_python_code(f)
        f.open_brace(self.declare)
        if len(self._note.strip()) > 0:
            f.write_comment(self._note)
        setattr(self, 'source_line', f.line)  # store start
        # f.write_line('# line:{}'.format(f.line))
        f.write_line(self._content)
        # f.write_line('# line:{}'.format(f.line))
        f.close_brace()
        f.write_newline()

    def open_line(self, line):
        """This is a special method for open until some useful circumstances"""
        frame = context.get_frame()
        book = frame.docBook
        this_pane = getattr(self, '_pane', None)
        if this_pane is None:
            from beatle.activity.models.py.ui import pane
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            book.AddPage(this_pane, self.tab_label, True, self.bitmap_index)
        else:
            index = book.GetPageIndex(this_pane)
            book.SetSelection(index)
        this_pane.goto_line(line, True)
        return True

    def on_undo_redo_changed(self):
        """Update from app"""
        super(MemberMethod, self).on_undo_redo_changed()
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(this_pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            if this_pane.m_editor.GetText() != self._content:
                this_pane.m_editor.SetText(self._content)
        k = self.inner_module or self.inner_package
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(MemberMethod, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for unload"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(MemberMethod, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(MemberMethod, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from beatle.activity.models.py.ui import pane
            frame = context.get_frame()
            book = frame.docBook
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')

    def export_python_code(self, wf):
        """Write code"""
        # first, write decorators, if any
        self.write_code(wf)

    def get_tab_bitmap(self):
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc
        return rc.get_bitmap("py_method")

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_method")

    @property
    def tree_label(self):
        """Get tree label"""
        arg_list = [x for x in self.sorted_child if type(x) is Argument]
        arg_list = ', '.join(x.label for x in arg_list + self[ArgsArgument] + self[KwArgsArgument])
        return '{self._name}({arg_list})'.format(self=self, arg_list=arg_list)

    @property
    def tab_label(self):
        """Get tab label"""
        arg_list = [x for x in self.sorted_child if type(x) is Argument]
        arg_list = ', '.join(x.label for x in arg_list + self[ArgsArgument] + self[KwArgsArgument])
        return '{self._name}({arg_list})'.format(self=self, arg_list=arg_list)

    @property
    def declare(self):
        """Get tab label"""
        arg_list = [x for x in self.sorted_child if type(x) is Argument]
        arg_list = ', '.join(x.label for x in arg_list + self[ArgsArgument] + self[KwArgsArgument])
        return 'def {self._name}({arg_list}):'.format(self=self, arg_list=arg_list)

    def exists_named_argument(self, name):
        """Check about the existence of an argument"""
        return name in [x.name for x in self(Argument, ArgsArgument, KwArgsArgument)]

    @upgrade_version
    def __setstate__(self, dict_):
        # temporary patch on 23/01
        return {
            'add': {'_read_only_content': dict_['_read_only'], '_child_index': -1},
        }
