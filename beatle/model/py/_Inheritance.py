# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
from beatle.lib.tran import TransactionStack
from ._PyTComponent import PyTComponent


class Inheritance(PyTComponent):
    """Implements class representation"""
    def __init__(self, **kwargs):
        """ Initialize the inheritance. Required parameters:
                ancestor: the parent class
            parent: the child class
            Optional parameters are:
               name: the name of the inheritance. This parameter is
            really not expected, because the inheritance cannot be aliased."""
        assert 'ancestor' in kwargs
        assert 'parent' in kwargs
        self._ancestor = kwargs['ancestor']
        if 'name' not in kwargs:
            kwargs['name'] = self._ancestor.name
        super(Inheritance, self).__init__(**kwargs)
        # prevent external inheritances hangs
        if self._ancestor:
            self._ancestor._deriv.append(self.parent)
        if not kwargs.get('raw', False):
            self.parent.auto_init()
        k = self.inner_module or self.inner_package
        if k:
            k.export_code_files()

    @property
    def ancestor(self):
        return self._ancestor

    def delete(self):
        """Handle delete"""
        k = self.inner_module or self.inner_package
        super(Inheritance, self).delete()
        if k:
            k.export_code_files()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {'ancestor': self._ancestor}
        kwargs.update(super(Inheritance, self).get_kwargs())
        return kwargs

    def set_ancestor(self, ancestor):
        """Update ancestor"""
        if self._ancestor != ancestor:
            if not self._ancestor is None:
                self._ancestor._deriv.remove(self.parent)
        self._ancestor = ancestor
        if not self._ancestor is None:
            self._ancestor._deriv.append(self.parent)
            self._name = self._ancestor.name

    def remove_relations(self):
        """Utility for undo/redo"""
        if self._ancestor:
            if self.parent in self._ancestor._deriv:
                self._ancestor._deriv.remove(self.parent)
        super(Inheritance, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        if self._ancestor:
            self._ancestor._deriv.append(self.parent)
        super(Inheritance, self).restore_relations()

    def on_undo_redo_changed(self):
        """Reflect changes"""
        for x in self.parent._deriv:
            x.on_undo_redo_changed()
        super(Inheritance, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_inheritance")

    @property
    def tree_label(self):
        """Get tree label"""
        return "inherits " + self._name


