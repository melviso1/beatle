# -*- coding: utf-8 -*-
"""
Created on 31 ago. 2018

@author: mel
"""
import re
import os
import wx

from beatle.lib.api import context
from beatle.lib.tran import DelayedMethod
from ._Folder import Folder
from ._Class import Class
from ._Module import Module
from ._MemberMethod import MemberMethod


class PyProject(object):
    """Handler of Python projects.
    In order to maintain the Project class as more agnostic as possible,
    all kind of projects must implement his own handlers.
    Comments on transactions:
    The project handlers, like PyProject instances will be always stored as
    Project::_handler values. Modifications of handled values, creation or
    destruction will be always handled at Project instance level. For this
    reason, project handlers, like CCProject d'ont need to (and can't)
    inherit from TCommon.
    When needed some customization, like labeling transactions, these will
    be conducted from Project, querying handlers."""

    module_classes = [Module]  # Custom modules defined by plugins
    member_method_classes = [MemberMethod]

    def __init__(self, project, **kwargs):
        self._project = project
        self._dir = kwargs['dir']
        self._author = kwargs.get('author', "<unknown>")
        self._date = kwargs.get('date', "08-10-2966")
        self._license = kwargs.get('license', None)
        self._type = kwargs.get('type', "unspecified")
        self._version = kwargs.get('version', [1, 0, 0])
        super(PyProject, self).__init__()

    def initialize(self):
        """Set custom properties for project"""
        self._project.namespace_container = False
        self._project.class_container = False
        self._project.package_container = True
        self._project.main_file = None
        self._project.py_file_pattern = re.compile('^(.)*.py$', re.I)
        # for python project, add hidden class object
        Class(parent=self._project, name='object', visibleInTree=False, declare=False, implement=False)
        self.register_output_dirs()

    def get_kwargs(self):
        return {
                'dir': self._dir,
                'sourcedir': self._srcDir,
                'author': self._author,
                'date': self._date,
                'license': self._license,
                'type': self._type,
                'version': self._version,
                }

    @staticmethod
    def get_tab_bitmap():
        from beatle.app import resources as rc
        return rc.get_bitmap('python')

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('python')

    @property
    def buildable(self):
        """The project may be built?"""
        return False

    @property
    def dir(self):
        """return the project directory"""
        return self._dir

    @dir.setter
    def dir(self, value):
        """sets the project directory"""
        self._dir = value

    @property
    def level_classes(self):
        """return the list of classes inside this level"""
        return self._project(Class, filter=lambda x: x.parent.outer_class is None)

    @property
    def main_classes(self):
        """return the list of classes not belonging to any module or package.
        This is a similar behaviour to c++ where the class defines his own module.
        This is introduced by the requirement of the wxUI working case."""
        return self._project(Class, filter=lambda x: x.inner_module is None and x.inner_package is None)

    @property
    def modules(self):
        """returns the list of all modules"""
        return self._project(Module)

    @staticmethod
    def create_folder(**kwargs):
        """Override global folders with special constructor"""
        return Folder(**kwargs)

    @DelayedMethod()
    def export_code_files(self, force=False, logger=None):
        """Exports all the source code for the project"""
        # create target directories if needed
        if not self.register_output_dirs(logger):
            return
        # TO DO : TRANSLATE PENDING CONTEXTS TO PYTHON ROOT
        logger and logger.AppendReportLine('exporting code files', wx.ICON_INFORMATION)
        for obj in self._project.packages:
            if obj.parent.inner_package:
                continue
            obj.export_code_files(force, logger)
        for obj in self._project.modules:
            if obj.parent.inner_package:
                continue
            obj.export_code_files(force, logger)
        for obj in self.main_classes:
            if getattr(obj,'_implement', False):
                obj.export_code_files(force, logger)


    def register_output_dirs(self, logger=None):
        """Finds target generation directories and create it if missing."""
        if not os.path.exists(self._dir):
            os.makedirs(self._dir)
            if not os.path.exists(self._dir):
                e = "Project path missing: " + self._dir
                if logger is not None:
                    logger.AppendReportLine(e, wx.ICON_ERROR)
                wx.MessageBox(e, "Error", wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
                return False
        return True
