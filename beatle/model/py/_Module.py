# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 23:24:30 2013

@author: mel
"""

import os, sys, traceback


import wx

from beatle.lib.tran import TransactionStack, DelayedMethod
from beatle.lib.api import context
from beatle.lib.ostools import import_dir
from beatle.model.writer import Writer

from ._Import import Import
from ._ImportsFolder import ImportsFolder
from ._Class import Class
from ._Function import Function
from ._Data import Data
from ._PyTComponent import PyTComponent
from ._Folder import Folder


class Module(PyTComponent):
    """Implements a Module representation"""
    class_container = True
    folder_container = True
    diagram_container = True
    namespace_container = False
    function_container = True
    variable_container = True
    enum_container = False
    import_container = True

    def __init__(self, **kwargs):
        """Initialization"""
        self._lastSrcTime = None
        self._lastHdrTime = None
        self._entry = False
        self._content = kwargs.get('content', "")
        self._file_obj = None  # vincle with file object
        super(Module, self).__init__(**kwargs)
        self.export_code_files()

    @property
    def entry(self):
        """return true if this is a startup module"""
        return self._entry

    @property
    def content(self):
        return  self._content

    def set_file(self, file_obj):
        """Sets the vincle with file object"""
        self._file_obj = file_obj

    def delete(self):
        """delete the object"""
        if hasattr(self, '_file_obj') and self._file_obj:
            self._file_obj.delete()
        super(Module, self).delete()

    @property
    def label(self):
        """Get tree label"""
        return '{self._name}'.format(self=self)

    def open_line(self, line):
        """Open member method based on line, if possible"""
        from ._Package import Package
        from ._InitMethod import InitMethod
        from ._MemberMethod import MemberMethod
        method_classes = [x for x in self.project.member_method_classes] + [InitMethod, Function, Package]
        elements = self(*method_classes)
        elements.append(self)
        info_list = [x for x in elements if getattr(x, 'source_line', line+1) <= line]
        if len(info_list) == 0:
            return False
        candidate = max([(x.source_line,x) for x in info_list])[1]
        if not hasattr(candidate, 'open_line'):
            return False
        # before doing this as good, compute relative offset and check the line is inside
        line_offset = line - candidate.source_line
        if line_offset >= candidate.code_lines:
            return False
        if candidate is not self:
            return candidate.open_line(line_offset)
        frame = context.get_frame()
        book = frame.docBook
        this_pane = getattr(self, '_pane', None)
        if this_pane is None:
            from beatle.activity.models.py.ui import pane
            this_pane = pane.ModulePane(book, frame, self)
            setattr(self, '_pane', this_pane)
            book.AddPage(this_pane, self.tab_label, True, self.bitmap_index)
        else:
            index = book.GetPageIndex(this_pane)
            book.SetSelection(index)
        this_pane.goto_line(line_offset, True)
        return True

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'content': self._content
        }
        kwargs.update(super(Module, self).get_kwargs())
        return kwargs

    @property
    def code_lines(self):
        """Return the number of line codes.
        This is used for mapping from file to object.
        """
        return len(self._content.splitlines())

    def write_code(self, fw):
        """write his own code to file"""
        setattr(self, 'source_line', fw.line)
        # fw.write_line('# line:{}'.format(fw.line))
        fw.write_line(self._content)
        # fw.write_line('# line:{}'.format(fw.line))

    @property
    def key_file(self):
        """This method returns the key file. For modules, is
        the implementation file. For packages, is the __init__ file."""
        return os.path.realpath(os.path.join(self.dir, '{self._name}.py'.format(self=self)))

    @property
    def dir(self):
        """return the container dir"""
        return self.parent.dir

    @property
    def path_name(self):
        """Get the full path"""
        if self.parent.inner_module is not None:
            return '{self.parent.inner_module.path_name}.{self._name}'.format(self=self)
        return self._name

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Module, self).on_undo_redo_changed()
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(this_pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            if this_pane.m_editor.GetText() != self._content:
                this_pane.m_editor.SetText(self._content)
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def on_undo_redo_removing(self):
        """Handle on_undo_redo_removing, prevent generating files"""
        stack = TransactionStack
        method = self.export_code_files.inner
        if method not in stack.delayedCallsFiltered:
            stack.delayedCallsFiltered.append(method)
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Module, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for unload"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Module, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Module, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from beatle.activity.models.py.ui import pane
            frame = context.get_frame()
            book = frame.docBook
            this_pane = pane.ModulePane(book, frame, self)
            setattr(self, '_pane',  this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')

    def export_folder_code_files(self, fw, folder, force, logger=None):
        """does python export for folders"""
        for element in folder.sorted_child:
            if type(element) in [Folder, ImportsFolder]:
                self.export_folder_code_files(fw, element, force, logger)
            else:
                element.export_python_code(fw)

    @property
    def abs_path(self):
        """return the absolute path"""
        return os.path.join(self.dir, '{self._name}.py'.format(self=self))

    @DelayedMethod()
    def export_code_files(self, force=False, logger=None):
        """does source generation"""
        # Generate module python code
        file_name = self.abs_path
        logger and logger.AppendReportLine('exporting module {0}'.format(self._name), wx.ICON_INFORMATION)
        try:
            with open(file_name, 'wt') as f:
                fw = Writer.for_file(f, 'python')

                # first thing: generate module comment
                fw.write_line("# -*- coding: utf-8 -*-")
                if len(self._note.strip()) > 0:
                    fw.write_comment(self._note)
                    fw.write_newline()

                # Now, the user decides the order of elements
                for element in self.sorted_child:
                    if type(element) in [Folder, ImportsFolder]:
                        self.export_folder_code_files(fw, element, force, logger)
                    elif element._implement:
                        element.export_python_code(fw)
                # custom code last
                self.write_code(fw)

        except Exception as inst:
            traceback.print_exc(file=sys.stdout)
            print(type(inst))     # the exception instance
            print(inst.args)      # arguments stored in .args
            print(inst)
            if logger:
                logger.AppendReportLine('failed opening {0}'.format(file_name), wx.NOT_FOUND)
            return False

    @staticmethod
    def get_tab_bitmap():
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc
        return rc.get_bitmap("py_module")

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_module")

    @property
    def inner_class(self):
        """Get the innermost class container"""
        return None

    def analyze(self):
        """Do an analysis of the raw content and extracts
        whatever its possible to extract"""
        from beatle import analytic
        self.delete_children()
        s = analytic.PyParse(self, self._content)
        self._content = s.inline()
        return s._status

    @property
    def locals(self):
        """This method returns all the top elements defined
        in the module"""
        raw = self(Class, Data, Function, filter=lambda x: x.inner_module == self)
        return dict([(e.name, e) for e in raw])

    def _local_import(self, name):
        """This method finds for a module or package (in this order)
        matching the name of the import, inside the same directory of
        the module. Please take consideration of that
        the name must be simple, without dots. Returns the module or package
        or None if not found"""
        from ._Package import Package
        container = self.inner_package
        for x in self(Module, filter=lambda z: z != self and z.inner_package == container, cut=True):
            if x.name == name:
                return x
        for x in self(Package, filter=lambda z: z.parent.inner_package == container, cut=True):
            if x.name == name:
                return x
        return None

    def _global_import(self, name):
        """This method finds for a module or package (in this order)
        matching the name of the import, inside the root directory of
        the project. Please take consideration of that
        the name must be simple, without dots. Returns the module or package
        or None if not found"""
        from ._Package import Package
        for x in self(Module, filter=lambda z: z != self and z.inner_package is None, cut=True):
            if x.name == name:
                return x
        for x in self(Package, filter=lambda z: z.parent.inner_package is None, cut=True):
            if x.name == name:
                return x
        return None

    def _import(self, name, root=True):
        """Get the import (if possible)"""
        sections = name.split('.')
        if not len(sections):
            return None
        base = sections[0]
        del sections[0]
        base = self._local_import(base) or self._global_import(base)
        if base is None:
            # this import is external
            return import_dir(name)
        for _next in sections:
            base = base._local_import(_next)
            if base is None:
                return None
        return base

    @property
    def globals(self):
        """This method returns all the globals 'visible' inside the module"""
        # basically, we iterate through the imports
        from ._Package import Package
        imports = self(Import, filter=lambda x: x.inner_module == self)
        #
        d = {}
        for imp in imports:
            # find for module or source to import
            m = self._import(imp.name)
            if m is None:
                # pending log failed import
                continue
            # m could be a  Module, Package or a symbol list
            # from a native import
            if imp._from is not None:
                # we import a symbol
                if type(m) is list:
                    if imp._from not in m:
                        # pending log missing symbol
                        continue
                    symbol = m[imp._from]
                elif type(m) is Package:
                    symbol = m._local_import(imp._from)
                    if imp._from not in m:
                        # pending log misssing symbol
                        continue
                    symbol = m[imp._from]
                elif type(m) is Module:
                    l = m.locals()
                    l.extend(m.globals())
                    if imp._from not in l:
                        # pending log missing symbol
                        continue
                    symbol = l[imp._from]
            else:
                symbol = m
            if imp._as:
                d[imp._as] = symbol
            else:
                d[imp._name] = symbol
        return d
