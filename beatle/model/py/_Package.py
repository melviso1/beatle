# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 23:24:30 2013

@author: mel
"""

import os
import sys
import wx


from beatle.lib.api import context
from beatle.lib.tran import TransactionStack, DelayedMethod

from beatle.model.writer import Writer

from ._Module import Module
from ._PyTComponent import PyTComponent
from ._Import import Import
from ._Data import Data
from ._Function import Function
from ._Class import Class

import traceback


class Package(PyTComponent):
    """Implements a Module representation"""
    class_container = True
    folder_container = True
    diagram_container = True
    namespace_container = False
    function_container = True
    variable_container = True
    enum_container = False
    import_container = True
    package_container = True
    module_container = True

    def __init__(self, **kwargs):
        """Initialization"""
        self._lastSrcTime = None
        self._lastHdrTime = None
        self._entry = False
        self._file_obj = None  # link with file object
        super(Package, self).__init__(**kwargs)
        if not kwargs.get('noinit', False):
            Module(parent=self, name='__init__', read_only=False)
        self.export_code_files()

    @property
    def entry(self):
        """return true if this is a startup package"""
        return self._entry

    def get_init(self, file_obj):
        init_list =  self(Module, filter=lambda x: x.parent.inner_package == self and x.name == '__init__', cut=True)
        return (len(init_list) == 1 and init_list[0]) or None

    def set_file(self, file_obj):
        """Sets the link  with file object"""
        self._file_obj = file_obj

    def delete(self):
        """delete the object"""
        if hasattr(self, '_file_obj') and self._file_obj:
            self._file_obj.delete()
        super(Package, self).delete()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        return super(Package, self).get_kwargs()

    @property
    def key_file(self):
        """This method returns the key file. For modules, is
        the implementation file. For packages, is the __init__ file."""
        return os.path.realpath(os.path.join(self.dir, '__init__.py'))

    @property
    def dir(self):
        """returns the absolute path"""
        d = os.path.join(self.parent.dir, self._name)
        return os.path.realpath(d)

    def on_undo_redo_removing(self):
        """Handle on_undo_redo_removing, prevent generating files"""
        stack = TransactionStack
        method = self.export_code_files.inner
        if method not in stack.delayedCallsFiltered:
            stack.delayedCallsFiltered.append(method)
        super(Package, self).on_undo_redo_removing()

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Package, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.export_code_files()

    @property
    def abs_path(self):
        """return the absolute path of __init__"""
        return os.path.join(self.dir, '__init__.py'.format(self=self))

    @DelayedMethod()
    def export_code_files(self, force=False, logger=None):
        """does source generation"""
        # Any package is just a directory. And an init file. The first implementation
        # was really a bad choice, because the best option is simply to handle it as a
        # folder with an init method.
        package_dir = self.dir
        logger and logger.AppendReportLine('exporting package {0}'.format(self._name), wx.ICON_INFORMATION)
        if not os.path.exists(package_dir):
            os.makedirs(package_dir)
            if not os.path.exists(package_dir):
                e = "Failed creating package directory " + package_dir
                if logger is not None:
                    logger.AppendReportLine(e, wx.ICON_ERROR)
                else:
                    wx.MessageBox(e, "Error", wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
                return False

        # do the same as for projects
        for obj in self(Package, filter=lambda x: x.parent.inner_package == self, cut=True):
            obj.export_code_files(force, logger)
        for obj in self(Module, filter=lambda x: x.parent.inner_package == self, cut=True):
            obj.export_code_files(force, logger)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_package")

    def _local_import(self, name):
        """This method finds for a module or package (in this order)
        matching the name of the import, inside the nested directory of
        the module. Please take consideration of that
        the name must be simple, without dots. Returns the module or package
        or None if not found"""
        container = self
        # if the package holds __init__, the import is translated
        # to this file
        m = dict([(x.name, x) for x in self(
            Module, filter=lambda z: z != self and z.inner_package == container, cut=True)])
        if '__init__' in m:
            return m['__init__']
        if name in m:
            return m[name]
        for x in self(Package, filter=lambda z: z.parent.inner_package == container, cut=True):
            if x.name == name:
                return x
        return None

    @property
    def inner_class(self):
        """Get the innermost class container"""
        return None

