# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:31:28 2013

@author: mel
"""

from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject
from ._PyTComponent import PyTComponent


class Import(PyTComponent):
    """Implements argument representation"""

    context_container = False

    @TransactionalMethod('move import {0}')
    def drop(self, to):
        """drop this element to another place"""
        target = to.inner_import_container
        if not target or to.project != self.project:
            return False  # avoid move arguments between projects
        index = 0  # trick for insert as first child
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._as = kwargs.get('as', None)
        self._from = kwargs.get('from', None)  # (list of modules)
        super(Import, self).__init__(**kwargs)
        container = self.outer_class or self.outer_module
        container._lastSrcTime = None
        container._lastHdrTime = None
        self.inner_code_generator.export_code_files()

    def delete(self):
        """Handle delete"""
        k = self.inner_module or self.inner_package
        super(Import, self).delete()
        if k:
            k.export_code_files()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'as': self._as,
            'from': self._from
        }
        kwargs.update(super(Import, self).get_kwargs())
        return kwargs

    def on_undo_redo_changed(self):
        """Update from app"""
        self.parent.on_undo_redo_changed()
        super(Import, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def on_undo_redo_removing(self):
        """Do required actions for removing"""
        super(Import, self).on_undo_redo_removing()
        self.parent.on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Import, self).on_undo_redo_add()
        self.parent.on_undo_redo_changed()

    def export_python_code(self, wf):
        """Write code"""
        wf.write_line(self.label)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('py_import')

    @property
    def label(self):
        """Get tree label"""
        if self._from:
            if self._as:
                return 'from {self._name} import {self._from} as {self._as}'.format(self=self)
            else:
                return 'from {self._name} import {self._from}'.format(self=self)
        if self._as:
            return 'import {self._name} as {self._as}'.format(self=self)
        return 'import {self._name}'.format(self=self)
