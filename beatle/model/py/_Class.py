# -*- coding: utf-8 -*-

"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""

import os
from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject, DelayedMethod
from beatle.model.writer import Writer
from ._Inheritance import Inheritance
from ._Import import Import
from ._ImportsFolder import ImportsFolder
from ._InitMethod import InitMethod
from ._MemberMethod import MemberMethod
from ._Function import Function
from ._PyTComponent import PyTComponent
from ._Folder import Folder


class Class(PyTComponent):
    """Implements python class representation"""
    class_container = True
    context_container = True
    folder_container = True
    diagram_container = True
    inheritance_container = True
    member_container = True
    relation_container = True
    enum_container = False
    type_container = False
    import_container = True

    # visual methods
    @TransactionalMethod('move python class {0}')
    def drop(self, to):
        """Drops class inside project or another folder """
        target = to.inner_class_container
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization method"""
        self._deriv = kwargs.get('derivatives', [])
        self._memberPrefix = kwargs.get('prefix', '_')
        self._export = kwargs.get('export', False)

        self._lastHdrTime = None
        self._lastSrcTime = None
        super(Class, self).__init__(**kwargs)
        # add object derivative
        if not kwargs.get('raw', False):
            if self._name != 'object':
                for cls in self.project.level_classes:
                    if cls._name == 'object':
                        Inheritance(parent=self,
                            ancestor=cls, note='standard inheritance')
                        break
        self.inner_code_generator.export_code_files()

    def open_line(self, line):
        """Open member method based on line, if possible"""
        from ._Package import Package
        elements = self(InitMethod,
                        MemberMethod, Function, Package,)
        info_list = [x for x in elements if getattr(x, 'source_line', line+1) <= line]
        if len(info_list) == 0:
            return False
        candidate = max([(x.source_line, x) for x in info_list])[1]
        if not hasattr(candidate, 'open_line'):
            return False
        # before doing this as good, compute relative offset and check the line is inside
        line_offset = line - candidate.source_line
        if line_offset >= candidate.code_lines:
            return False
        return candidate.open_line(line_offset)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'derivatives': self._deriv,
            'prefix': self._memberPrefix
        }

        kwargs.update(super(Class, self).get_kwargs())
        return kwargs

    def lower(self):
        """Criteria for sorting when generating code"""
        return self._inheritance_level

    def get_constructor_arguments(self):
        """Get the list of arguments required by the class"""
        return []

    def update_class_relations(self):
        """This method gets invoked when something changes in
        class relations specification
        and ensure mainteinance of internal methods and ctors"""
        pass

    def auto_init(self):
        """Update all init ctors"""
        init_methods = self(InitMethod, filter=lambda z: z.inner_class == self, cut=True)
        for method in init_methods:
            method.auto_init()

    def auto_init_remove(self, var):
        """Update all init ctors removing a variable"""
        init_methods = self(InitMethod, filter=lambda z: z.inner_class == self, cut=True)
        for method in init_methods:
            method.auto_init_remove(var)

    def update_init_call(self):
        """Update the calls to init method"""
        pass

    def update_exit_calls(self):
        """Update the calls to exit method"""
        pass

    def auto_args(self):
        """Update all ctor arguments"""
        pass

    def update_sources(self, force=False):
        """does source generation"""
        pass

    def get_preferred_constructor(self):
        """Obtiene el constructor por defecto o None"""
        return None

    def get_serial_constructor(self):
        """Obtiene el constructor de serializacion o None"""
        return None

    def delete(self):
        """Delete diagram objects"""
        k = self.inner_module or self.inner_package
        super(Class, self).delete()
        if k:
            k.export_code_files()

    def remove_relations(self):
        """Utility for undo/redo"""
        super(Class, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(Class, self).restore_relations()

    def save_state(self):
        """Utility for saving state"""
        super(Class, self).save_state()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(Class, self).on_undo_redo_removing()

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Class, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Class, self).on_undo_redo_add()

    @DelayedMethod()
    def export_code_files(self, force=False, logger=None):
        """Do direct code generation of not packed classes"""
        if not self._implement:
            return
        parent = self.inner_package or self.project
        file_name = os.path.join(parent.dir, '{self._name}.py'.format(self=self))
        try:
            with open(file_name, 'wt') as f:
                fw = Writer.for_file(f, 'python')
                self.export_python_code(fw)
        except Exception as inst:
            import traceback
            import sys
            traceback.print_exc(file=sys.stdout)
            print(type(inst))  # the exception instance
            print(inst.args)  # arguments stored in .args
            print(inst)

    def export_folder_code_files(self, wf, folder):
        for element in folder.sorted_child:
            if type(element) in [Folder, ImportsFolder]:
                self.export_folder_code_files(wf, element)
            else:
                element.export_python_code(wf)

    def export_python_code(self, wf):
        """Export python code"""
        wf.write_newline()
        wf.write_newline()
        children = self.sorted_child
        # class level imports must be moved outside the class
        for x in children:
            if type(x) is Import:
                x.export_python_code(wf)
            elif type(x) is ImportsFolder:
                self.export_folder_code_files(wf, x)
        ins = ','.join([x.name for x in self[Inheritance]])
        if ins:
            ins = '(' + ins + ')'
        wf.open_brace('{self.label}{ins}:'.format(self=self, ins=ins))
        wf.write_comment(self._note)
        for element in children:
            if type(element) in [Inheritance, Import, ImportsFolder]:
                continue
            if type(element) is Folder:
                self.export_folder_code_files(wf, element)
                continue
            element.export_python_code(wf)
        wf.close_brace()
        wf.write_newline()
        return True

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('py_class')

    @property
    def label(self):
        """Get tree label"""
        return 'class {clase._name}'.format(clase=self)

    @property
    def path_name(self):
        """Get the full path"""
        if self.inner_module is not None:
            return '{self.inner_module.path_name}.{self._name}'.format(self=self)
        return self._name

    @property
    def file_container(self):
        """get the container filename (without extension)"""
        if self.inner_module:
            return self.inner_module._name
        if self.inner_package:
            return self.inner_package._name
        return self.project._name

    @property
    def context_name(self):
        """get the class name with path through parent classes"""
        pclass = self.parent.inner_class
        if pclass:
            return '{pclass.context_name}.{self._name}'.format(pclass=pclass, self=self)
        return self._name

    @property
    def tree_label(self):
        """Get tree label"""
        return 'class {clase._name}'.format(clase=self)

    def is_ancestor(self, cls):
        """Checks for ancestor relationship"""
        if cls is None:
            return False
        if not Inheritance in cls._child:
            return False
        inhmap = cls[Inheritance]
        for inh in inhmap:
            if inh._ancestor == self:
                return True
            if self.is_ancestor(inh._ancestor):
                return True
        return False

    def update_init_method(self):
        """Update the init methods"""
        # TO-DO
        pass

    def update_exit_method(self):
        """update the exit methods"""
        # TO-DO
        pass

    @property
    def inner_class(self):
        """Get the inner class container"""
        return self

    @property
    def outer_class(self):
        """Get the outer class container"""
        return (self.parent and self.parent.outer_class) or self

    @property
    def is_class_methods(self):
        """returns the list of all is_class_methods"""
        # TO-DO
        return []

    @property
    def init_methods(self):
        """returns the setup relation method"""
        # TO-DO
        return []

    @property
    def exit_methods(self):
        """returns the clean relations method"""
        # TO-DO
        return []

    @property
    def nested_classes(self):
        """Returns the list of nested classes (including self)"""
        if type(self.parent) not in [Folder, Class]:
            return [self]
        return self.parent.nested_classes + [self]

    @property
    def scope(self):
        """Get the scope"""
        return '{self.parent.scope}{self._name}.'.format(self=self)

    @property
    def inheritance(self):
        """Get the inheritance list"""
        # TO-DO
        return []


