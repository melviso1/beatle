# -*- coding: utf-8 -*-

from ._Folder import Folder


class ImportsFolder(Folder):
    """Implements a Folder representation"""
    class_container = False
    type_container = False
    function_container = False
    variable_container = False
    module_container = False
    namespace_container = False
    member_container = False
    import_container = True

    def __init__(self, **kwargs):
        """Initialization"""
        kwargs['name'] = 'imports'
        super(ImportsFolder, self).__init__(**kwargs)
        self.update_container()

    def update_container(self):
        """Update the container info"""
        self.context_container = False
        self.folder_container = False
        self.diagram_container = False
        self.namespace_container = False

    def export_python_code(self, wf):
        """Write code"""
        from ._Import import Import
        for x in self[Import]:
            x.export_python_code(wf)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("folderI")
