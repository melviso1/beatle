# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:08:46 2013

@author: mel
"""

import re
from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject
from beatle.lib.api import context
from ._MemberData import MemberData
from ._MemberMethod import MemberMethod
from ._Argument import Argument
from ._ArgsArgument import ArgsArgument
from ._KwArgsArgument import KwArgsArgument


class InitMethod(MemberMethod):
    """Implements member method"""

    # visual methods
    @TransactionalMethod('move init method {0}')
    def drop(self, to):
        """Drops data-member inside project or another folder """
        target = to.inner_member_container
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        kwargs["static_method"] = False
        kwargs["class_method"] = False
        kwargs["property"] = False
        kwargs["name"] = '__init__'
        super(InitMethod, self).__init__(**kwargs)

    def auto_init(self):
        """Initialize the method with the instance variables.
        This method is very aggressive detecting bad indented lines.
        The idea is remove all previous local variables initializations
        including multiline ones and preserving original code at the end."""
        base_class = self.inner_class
        instance_members = self.inner_class(
            MemberData, filter=lambda x: x._scope != 'class' and x.inner_class == base_class)
        prev_lines = self._content.splitlines()
        new_lines = ['self.{name} = {value}'.format(name=x.name, value=x.value) for x in instance_members]
        # delete all the self.{name} = {value} lines
        expr = re.compile(r'self.[a-zA-Z_]\w*\s*=', re.DOTALL)
        indent = '    '
        conserved_lines = []
        for x in prev_lines:
            if len(indent) > 0:
                try:
                    if x.index(indent) == 0:
                        continue
                except ValueError:
                    pass
                indent = ''
            match = expr.match(x)
            if match is not None:
                indent = '    '
                continue
            conserved_lines.append(x)
            new_lines = [x for x in new_lines if x not in conserved_lines]
        self._content = '\n'.join(new_lines+conserved_lines)
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(this_pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            if this_pane.m_editor.GetText() != self._content:
                if self.read_only:
                    this_pane.m_editor.SetReadOnly(False)
                this_pane.m_editor.SetText(self._content)
                this_pane.m_editor.ResetModified()
                this_pane.m_editor.SetModified(False)
                if self.read_only:
                    this_pane.m_editor.SetReadOnly(True)

    def auto_init_remove(self, var):
        """Remove a declared variable from autoinit.
        This method must be revisited for multiline initializations"""
        prev_lines = self._content.splitlines()
        try:
            index = prev_lines.index('self.{name} = {value}'.format(name=var.name,value=var.value))
            del prev_lines[index]
            self._content = '\n'.join(prev_lines)
        except ValueError:
            return
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(this_pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            if this_pane.m_editor.GetText() != self._content:
                if self._read_only:
                    this_pane.m_editor.SetReadOnly(False)
                    this_pane.m_editor.SetText(self._content)
                    this_pane.m_editor.ResetModified()
                    this_pane.m_editor.SetModified(False)
                    this_pane.m_editor.SetReadOnly(True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs.update(super(InitMethod, self).get_kwargs())
        del kwargs['static_method']
        del kwargs['class_method']
        del kwargs['property']
        return kwargs

    def on_undo_redo_changed(self):
        """Update from app"""
        super(InitMethod, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        super(InitMethod, self).on_undo_redo_removing()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(InitMethod, self).on_undo_redo_add()

    def get_tab_bitmap(self):
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc
        return rc.get_bitmap("py_init")

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_init")

    @property
    def tree_label(self):
        """Get tree label"""
        arg_list = [x for x in self.sorted_child if type(x) is Argument]
        arg_list = ', '.join(
            arg.label for arg in arg_list + self[ArgsArgument] + self[KwArgsArgument])
        return '__init__({arg_list})'.format(arg_list=arg_list)

    def exists_named_argument(self, name):
        """Check about the existence of an argument"""
        return name in [x.name for x in self(Argument, ArgsArgument, KwArgsArgument)]
