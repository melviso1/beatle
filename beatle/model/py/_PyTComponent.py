# -*- coding: utf-8 -*-
from beatle.model import TComponent


class PyTComponent(TComponent):
    """
    This class acts as TComponent specialization for python data model classes.
    The properties that has sense only for python are sored here.
    This is critical specially for managed properties of Projects, because
    of the __getattr__ fallback.
    """

    def __init__(self, **kwargs):
        """
        Constructor
        """
        super(PyTComponent, self).__init__(**kwargs)

    @property
    def dir(self):
        """return the container dir"""
        return self.parent.dir

    @property
    def inner_code_generator(self):
        return self.inner_module or self.inner_class

    @property
    def inner_folder(self):
        """returns the innermost folder"""
        from ._Folder import Folder
        return self.inner(Folder)

    @property
    def level_classes(self):
        """return the list of classes inside this level"""
        from ._Class import Class
        cls = self.outer_class
        return self(Class, filter=lambda x: x.parent.outer_class == cls)

    def write_comment(self, pf):
        """Write the note using the writer"""
        # ok, now we place comments, if any
        if len(self._note.strip()) > 0:
            txt = self._note.encode('ascii', 'ignore')
            txt.replace('\r', '')
            lines = txt.split("\n")
            for line in lines:
                pf.write_line("# {0}".format(line))
