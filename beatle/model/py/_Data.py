# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 08:28:25 2013

@author: mel
"""


from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject

from ._PyTComponent import PyTComponent


class Data(PyTComponent):
    """Implements data"""
    context_container = True

    # visual methods
    @TransactionalMethod('move python variable {0}')
    def drop(self, to):
        """Drops data-member inside project or another folder """
        target = to.inner_variable_container
        if not target:
            return False  # avoid move classes between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._value = kwargs.get('value', "'None'")
        super(Data, self).__init__(**kwargs)
        self.inner_code_generator.export_code_files()

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v

    def delete(self):
        """Handle delete"""
        k = self.inner_module or self.inner_package
        super(Data, self).delete()
        if k:
            k.export_code_files()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {'value': self._value}
        kwargs.update(super(Data, self).get_kwargs())
        return kwargs

    def write_code(self, pf):
        """write data definition"""
        pass

    def get_initializer(self):
        """Return the initializer sequence"""
        if len(self._value) > 0:
            return self._value
        return self._name

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_variable")

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Data, self).on_undo_redo_add()

    def on_undo_redo_changed(self):
        """Make changes in the model as result of change"""
        super(Data, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def export_python_code(self, wf):
        """Write code"""
        if len(self._value):
            wf.write_line('{self._name} = {self._value}'.format(self=self))
        else:
            wf.write_line('{self._name} = None'.format(self=self))
