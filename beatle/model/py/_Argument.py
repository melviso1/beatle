# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:31:28 2013

@author: mel
"""

from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject

from ._PyTComponent import PyTComponent


class Argument(PyTComponent):
    """Implements argument representation"""

    context_container = True

    # visual methods
    @TransactionalMethod('move argument {0}')
    def drop(self, to):
        """drop this element to another place"""
        target = to.inner_argument_container
        if not target or to.project != self.project:
            return False  # avoid move arguments between projects
        index = 0  # trick for insert as first child
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        # arguments:
        # ... default : the default or the call value
        # ... context : the use case. may be:
        #    ... declare : when the argument represents a argument declaration
        #    ....value : when the argument represents a value in call (the name is irrelevant)
        #    ....keyword : when the argument represent a keyword value in call (default is mandatory)
        #    ....args : when the argument represent a string arg sequence in call
        #    ....kwargs : when the argument represent a keyword args in call
        #
        # normal context is declare. The rest of contexts are argument-reuse for few cases, like
        # decorator instantiations
        self._default = kwargs.get('default', '')
        self._context = kwargs.get('context', 'declare')
        super(Argument, self).__init__(**kwargs)
        if not (self._context != 'keyword' or self._default):
            raise RuntimeError('Invalid Argument initialization')
        container = self.outer_class or self.outer_module
        container._lastSrcTime = None
        container._lastHdrTime = None
        self.inner_code_generator.export_code_files()

    @property
    def default(self):
        return self._default

    def delete(self):
        """Handle delete"""
        k = self.inner_module or self.inner_package
        super(Argument, self).delete()
        if k:
            k.export_code_files()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'default': self._default
        }
        kwargs.update(super(Argument, self).get_kwargs())
        return kwargs

    def on_undo_redo_changed(self):
        """Update from app"""
        self.parent.on_undo_redo_changed()
        super(Argument, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.inner_code_generator.export_code_files()

    def on_undo_redo_removing(self):
        """Do required actions for removing"""
        super(Argument, self).on_undo_redo_removing()
        self.parent.on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Argument, self).on_undo_redo_add()
        self.parent.on_undo_redo_changed()

    @property
    def label(self):
        """Get tree label"""
        if self._context == 'declare':
            if self._default:
                return '{self._name}={self._default}'.format(self=self)
            else:
                return self._name
        if self._context == 'value':
            return self._name
        if self._context == 'keyword':
            return '{self._name}={self._default}'.format(self=self)
        if self._context == 'args':
            return '*{self._default}'.format(self=self)
        if self._context == 'kwargs':
            return '**{self._default}'.format(self=self)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('py_argument')
