# -*- coding: utf-8 -*-
from beatle.model.writer import Writer


class FileWriter(Writer):
    """file-writer is a commodity class for writing C/C++ files"""

    def __init__(self, file_handle, language="c++"):
        """initialization"""
        super(FileWriter, self).__init__(language)
        self.file = file_handle

    def write_line(self, txt=''):
        """write an indented sequence of lines"""
        if self._language == 'c++':
            if type(txt) is bytes:
                txt = txt.decode('ascii', 'replace')
        lines = txt.splitlines()
        for line in lines:
            self.file.write("{0}{1}\n".format(self.current_indent, line))
        self._line += len(lines)

    def write_newline(self, count=1):
        """write a newline"""
        self.file.write("\n"*count)
        self._line += count
