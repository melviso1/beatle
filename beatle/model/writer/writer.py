# -*- coding: utf-8 -*-


class Writer(object):
    """Implements a generic C++ writer."""

    def __init__(self, language="c++"):
        """initialize the writer class"""
        self.current_indent = ""
        self._language = language
        self._line = 0
        super(Writer, self).__init__()

    @property
    def line(self):
        return self._line

    @classmethod
    def for_file(cls, file_handle, language="c++"):
        """method for new writer"""
        from beatle.model.writer import FileWriter
        return FileWriter(file_handle, language)

    @classmethod
    def for_mem(cls):
        """method for new writer"""
        from beatle.model.writer import MemWriter
        return MemWriter()

    def indent(self):
        """Increase indent size"""
        self.current_indent += " "*4

    def unindent(self):
        """Decrease indent size"""
        self.current_indent = self.current_indent[:-4]

    def open_brace(self, text=''):
        """Opens a brace"""
        if self._language == 'c++':
            self.write_line("{0}{{".format(text))
        else:
            self.write_line("{0}".format(text))
        self.indent()

    def close_brace(self, text=''):
        """Closes a brace"""
        self.unindent()
        if self._language == 'c++':
            self.write_line("}}{0}".format(text))
        else:
            self.write_line("{0}".format(text))

    def write_comment(self, text):
        """Write a multi-line comment respecting indentation"""
        self.open_comment()
        if type(text) is not str:  # avoid null fields
            text = str(text)
        for x in text.split('\n'):
            self.write_line('{0}'.format(x.strip()))
        self.close_comment()

    def open_comment(self):
        """Open a c comment"""
        if self._language == 'c++':
            self.write_line("/*")
        if self._language == 'python':
            self.write_line("\"\"\"")

    def close_comment(self):
        """Close a comment"""
        if self._language == 'c++':
            self.write_line("*/")
        elif self._language == 'python':
            self.write_line("\"\"\"")
