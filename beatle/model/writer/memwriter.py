# -*- coding: utf-8 -*-
from beatle.model.writer import Writer


class MemWriter(Writer):
    """MemWriter is a comodity class for writing C/C++ files"""
    def __init__(self, language="c++"):
        """initialization"""
        super(MemWriter, self).__init__(language)
        self.data = []

    def write_line(self, txt=''):
        """write an indented sequence of lines"""
        lines = txt.splitlines()
        for line in lines:
            self.data.append("{0}{1}\n".format(self.current_indent, line))
        self._line += len(lines)

    def write_newline(self, count=1):
        """write a newline"""
        self.data.append('\n'*count)
        self._line += count

    def __str__(self):
        """dumps the string"""
        return ''.join(self.data)



