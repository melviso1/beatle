# -*- coding: utf-8 -*-

"""This represents a pending task.
The task uses glass_clock as tree icon"""

import wx

from beatle.model import TComponent
from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject
from beatle.lib.decorators import upgrade_version


class Task(TComponent):
    """Declares the task element"""
    task_container = True  # allow subtasks

    @TransactionalMethod('move task {0}')
    def drop(self, to):
        """drop this elemento to another place"""
        target = to.inner_task_container
        if not target or to.project != self.project:
            return False  # avoid move arguments between projects
        index = 0
        new_cont = target.status_container
        if new_cont is not self.status_container:
            new_cont.SetStatus(self)
        self.project.modified = True
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    @property
    def status_container(self):
        """return the status container"""
        return self.parent.status_container

    """Declares a task entry"""
    def __init__(self, **kwargs):
        """Constructor"""
        self._status = kwargs.get('status', 'pending')
        self._priority = kwargs.get('priority', 'normal')
        self._taskType = kwargs.get('type', 'CHANGE')
        self._reference = kwargs.get('reference', 'None')
        self._dateCreated = kwargs.get('dateCreated', 'None')
        self._dateBegin = kwargs.get('dateBegin', 'None')
        self._dateEnd = kwargs.get('dateEnd', '')
        super(Task, self).__init__(**kwargs)

    def get_kwargs(self):
        """Get the info about the class"""
        kwargs = {
            'status': self._status,
            'priority': self._priority,
            'type': self._taskType,
            'reference': self._reference,
            'dateCreated': self._dateCreated,
            'dateBegin': self._dateBegin,
            'dateEnd': self._dateEnd
            }
        kwargs.update(super(Task, self).get_kwargs())
        return kwargs

    @upgrade_version
    def __setstate__(self, d):
        """Add the creation field if missing"""
        return {
            'add': {'_dateCreated': wx.DateTime.Now().Format('%Y-%m-%d %H:%M:%S'), '_child_index': -1},
        }

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        if self._status != "done":
            if self._priority == "High":
                return rc.get_bitmap_index('task_high')
            if self._priority == "Critical":
                return rc.get_bitmap_index('task_critical')
        return rc.get_bitmap_index('glass_clock')
