# -*- coding: utf-8 -*-

"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
import wx

from .._Folder import Folder
from .Task import Task
from .TaskFolder import TaskFolder

class CurrentTasks(Folder):
    """Clase que representa a las tareas pendientes"""

    task_container = True

    @property
    def status_container(self):
        """return the status container"""
        return self

    def SetStatus(self, element):
        """Set the task as working"""
        if type(element) is Task:
            element.save_state()
            element._status = 'working'
            element._dateBegin = wx.DateTime.Now().Format('%Y-%m-%d %H:%M:%S')
            element._dateEnd = '--/--/---- --:--:--'
        for subtask in element[Task]:
            self.SetStatus(subtask)
        for subtask in element[TaskFolder]:
            self.SetStatus(subtask)

    def __init__(self, **kwargs):
        """Inicializacion"""
        if 'name' not in kwargs:
            kwargs['name'] = 'Current Tasks'
        kwargs['read_only'] = True
        super(CurrentTasks, self).__init__(**kwargs)

    @property
    def can_delete(self):
        """Check abot if class can be deleted"""
        return super(CurrentTasks, self).can_delete

    def delete(self):
        """Delete diagram objects"""
        super(CurrentTasks, self).delete()

    def remove_relations(self):
        """Utility for undo/redo"""
        super(CurrentTasks, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(CurrentTasks, self).restore_relations()

    def save_state(self):
        """Utility for saving state"""
        super(CurrentTasks, self).save_state()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(CurrentTasks, self).on_undo_redo_removing()

    def on_undo_redo_changed(self):
        """Update from app"""
        super(CurrentTasks, self).on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(CurrentTasks, self).on_undo_redo_add()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('folder_current')

    @property
    def bitmap_open_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('folder_current_open')

    @property
    def label(self):
        """Get tree label"""
        return '{self._name}'.format(self=self)
