# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
import sys, os, re, shutil, time

import wx

#import beatle
from beatle.lib.api import context
from beatle.lib.decorators import upgrade_version
from beatle.lib.tran import TransactionStack, DelayedMethod
from beatle.model import cc, py, arch, tasks, database
from beatle.lib.handlers import identifier
from beatle.lib.decorators import delegated

from ._TComponent import TComponent

recurse = False

ID_YESALL = identifier("ID_YESALL")


class Project(TComponent):
    """Representation of generic project"""

    folder_container = True
    diagram_container = True
    module_container = True

    def __getattr__(self, name):
        """Get handler attribute's as if they were in project"""
        try:
            if name not in ['_handler','__dict__'] and self._handler is not None:
                return getattr(self._handler,name)
        except Exception as e:
            return super(Project, self).__getattribute__(name)

    def __setattr__(self, name, value):
        """Set handler attribute's as if they were in project"""
        try:
            if name not in ['_handler', '__dict__'] and getattr(self, '_handler', None):
                if hasattr(self._handler, name):
                    setattr(self._handler, name, value)
                    return
        except AttributeError as e:
            import traceback
            import sys
            traceback.print_exc(file=sys.stdout)
            print(type(e))  # the exception instance
            print(e.args)  # arguments stored in .args
            print(e)
        try:
            super(Project, self).__setattr__(name, value)
        except:
            self.__dict__[name] = value

    def __init__(self, **kwargs):
        """Initialization of project"""
        self._handler = None
        self._language = kwargs.get('language', 'c++')
        if self._language == 'python':
            self._handler = py.PyProject(self, **kwargs)
        elif self._language == 'c++':
            self._handler = cc.CCProject(self, **kwargs)
        elif self._language == 'databases':
            self._handler = database.DatabaseProject(self, **kwargs)

        self._tasks = kwargs.get('tasks', [])
        self._modified = True
        self._bookmarks = {}
        self._breakpoints = {}
        self._lastHdrTime = None
        super(Project, self).__init__(**kwargs)
        #         if self._language == 'c++':
        #             self.namespace_container = True
        #             self.class_container = True
        #             self.package_container = False
        #             LibrariesFolder(parent=self, read_only=True)
        #             cc.TypesFolder(parent=self, read_only=True)
        #Add tasks control
        if getattr(self, '_handler', False):
            self._handler.initialize()

        self.initialize_tasks()

    @property
    def language(self):
        return self._language

    def initialize_tasks(self):
        """Create tasks for project"""
        tasks.PendingTasks(parent=self)
        tasks.CurrentTasks(parent=self)
        tasks.FinishedTasks(parent=self)

    def breakpoints(self, model_file):
        """The breakpoints are stored using the uid of the object
        for reference"""
        uid = model_file.uid
        if uid not in self._breakpoints:
            self._breakpoints[uid] = {}
        return self._breakpoints[uid]

    def bookmarks(self, editable):
        """The bookmarks are stored using the uid of the object
        for reference. The editable objects may include ctors,
        dtors, standard methods or code files."""
        uid = editable.uid
        if uid not in self._bookmarks:
            self._bookmarks[uid] = {}
        return self._bookmarks[uid]

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'language': self._language,
            'types': self._types,
            'contexts': self._contexts
            }
        if self._handler:
            kwargs.update(self._handler.get_kwargs())
        kwargs.update(super(Project, self).get_kwargs())
        return kwargs

    @property
    def handler(self):
        """Get the project handler"""
        return self._handler

    def write_timestamp(self, pf, logger=None):
        """Write the timestamp and other info"""
        pf.write_line('/***')
        pf.write_newline()
        pf.write_line(' File    :{0}'.format(self._masterInclude))
        pf.write_line(' Created :{0}'.format(time.strftime("%d/%m/%Y %H:%M:%S")))
        pf.write_line(' Author  :{0}'.format(self._author))
        if self._license:
            pf.write_newline()
            pf.write_line(' Licensed under {0}'.format(self._license))
        pf.write_newline()
        pf.write_line('***/')
        pf.write_newline()

    def on_undo_redo_removing(self):
        """Handle on_undo_redo_removing, prevent generating files"""
        app = context.get_app()
        method = None
        if self._language == 'c++':
            method = self.export_code_files.inner
        elif self._language == "python":
            method = self.export_code_files.inner
        if method and method not in TransactionStack.delayedCallsFiltered:
            TransactionStack.delayedCallsFiltered.append(method)
        super(Project, self).on_undo_redo_removing()
        app.remove_project(self)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        app = context.get_app()
        app.add_project(self)
        method = None
        super(Project, self).on_undo_redo_add()

    def on_undo_redo_changed(self):
        """Update from app"""
        if not TransactionStack.in_undo_redo():
            if self._language == 'c++':
                self._handler.on_undo_redo_changed()
                self.export_code_files()
            elif self._language == "python":
                self.export_code_files()
        super(Project, self).on_undo_redo_changed()

    def on_undo_redo_unloaded(self):
        """Remove object before unload"""
        app = context.get_app()
        app.remove_project(self)
        super(Project, self).on_undo_redo_unloaded()

    def __getstate__(self):
        """Get the pickle context. The projects are always
        stored with independence of the workspace container"""
        state = dict(self.__dict__)
        state['_parent'] = None
        return state

    @upgrade_version
    def __setstate__(self, data_dict):
        # temporary patch on 22/04
        if '_handler' in data_dict:
            if hasattr(data_dict['_handler'], '_serial'):
                del data_dict['_handler']._serial
        return {
            'rename': {'_readOnly': '_read_only'},
            'add': {'_bookmarks': {}, '_breakpoints': {}, '_child_index': -1},
            'delete': ['dir', '_transactional', '_license'],
        }

    @property
    def modified(self):
        """Get the modified state"""
        return self._modified

    @modified.setter
    def modified(self, value):
        """Set the modified state"""
        self._lastHdrTime = None
        self._modified = value

    def import_cpp_dir(self, parent, path, progress=None, yes_all=[False, ]):
        """Import the contents of the path into the project."""
        # check that path is not inside parent
        target = os.path.realpath(parent.dir)
        path = os.path.realpath(path)
        if len(path) >= len(target) and target == path[:len(target)]:
            raise ValueError('path must be not contained in target')
        entries = os.listdir(path)
        source_pattern = re.compile(r'(?P<name>[^\.]+)\.(cpp|cxx|cc)')
        header_pattern = re.compile(r'(?P<name>[^\.]+)\.(h|hh|hxx|hpp)')
        source_files = dict([(x[0].group('name'),x[1]) for x in [(source_pattern.match(y), y)
                                                           for y in entries] if x[0] is not None])
        header_files = dict([(x[0].group('name'),x[1]) for x in [(header_pattern.match(y), y)
                                                           for y in entries] if x[0] is not None])
        for name in source_files:
            module = cc.Module(parent=self, name=name,
                               header='{}.h'.format(name),
                               source='{}.cpp'.format(name)
                               )
            abspath = os.path.join(path, source_files[name])
            if progress:
                progress.m_text.SetValue('working now on {0}'.format(abspath))
            try:
                with open(abspath, "r", encoding='utf-8') as module_file:
                    module.user_code_s2 = module_file.read()
            except:
                pass
            if name in header_files:
                abspath = os.path.join(path, header_files[name])
                try:
                    with open(abspath, "r", encoding='utf-8') as module_file:
                        module.user_code_h4 = module_file.read()
                except:
                    pass
                del header_files[name]
        for name in header_files:
            module = cc.Module(parent=self, name=name,
                               header='{}.h'.format(name),
                               source='{}.cpp'.format(name)
                               )
            abspath = os.path.join(path, header_files[name])
            if progress:
                progress.m_text.SetValue('working now on {0}'.format(abspath))
            try:
                with open(abspath, "r", encoding='utf-8') as module_file:
                    module.user_code_h4 = module_file.read()
            except:
                pass

    def import_python_dir(self, parent, path, progress=None, yes_all=[False, ]):
        """Import the contents of the path into the project.
        The parent argument must be the self project or any existent package.
        The contents of the path and recursively scanned subdirs will be copied
        into the directory corresponding to the parent.
        This method will fail if path belongs to the project."""

        # check that path is not inside parent
        target = os.path.realpath(parent.dir)
        path = os.path.realpath(path)
        if len(path) >= len(target) and target == path[:len(target)]:
            raise ValueError('path must be not contained in target')
        entries = os.listdir(path)
        entries.sort()
        for element in entries:
            if element[0] == '.':
                continue
            if len(element) > 1:
                if element[-2:] == '.o':
                    continue
                if len(element) > 3:
                    if element[-4:] in ['.pyc', '.obj', '.sbr']:
                        continue
            abspath = os.path.join(path, element)
            if progress:
                progress.m_text.SetValue('working now on {0}'.format(abspath))
                # wx.YieldIfNeeded()
            if os.path.isdir(abspath):
                # skip __pycache__
                if element == '__pycache__':
                    continue
                new_directory = os.path.join(target, element)
                try:
                    if not os.path.exists(new_directory):
                        os.makedirs(new_directory)
                except OSError as e:
                    # very bad thing happens
                    wx.MessageBox('failed creating dir {0}: {1}'.format(
                        new_directory, str(e)))
                    continue
                # create a package and iterate
                kwargs = {
                    'parent': parent,
                    'name': element,
                    'noinit': True
                }
                package = py.Package(**kwargs)
                self.import_python_dir(package, abspath, progress, yes_all)
                continue
            if not os.path.isfile(abspath):
                # may be a link or a block device ... skip
                continue
            # Ok, create the same file
            new_file = os.path.join(target, element)
            if os.path.exists(new_file):
                # since the file already exists, we must ask user
                if not yes_all[0]:
                    from beatle.app.ui.dlg import FileAlreadyExistsDialog
                    dlg = FileAlreadyExistsDialog(context.get_frame(), element)
                    choice = dlg.ShowModal()
                    if choice == ID_YESALL:
                        yes_all[0] = True
                    elif choice == wx.NO:
                        continue
                    elif choice == wx.CANCEL:
                        return False
#                 choice = wx.MessageBox(
#                     'The file {0} already exists. Overwrite it?'.format(element),
#                     'Attention', wx.YES_NO | wx.CANCEL | wx.ICON_EXCLAMATION, parent=context.frame)
#                 if choice == wx.NO:
#                     continue
#                 if choice == wx.CANCEL:
#                     return False
            # copy the file
            try:
                shutil.copyfile(abspath, new_file)
            except Exception as e:
                # very bad thing happens
                wx.MessageBox('failed creating file {0}'.format(new_file))
                continue
            if self.py_file_pattern.match(element):
                #create new module
                if progress:
                    progress.m_text.SetValue('import {0}'.format(abspath))
                    # wx.YieldIfNeeded()
                with open(abspath, "r", encoding='utf-8') as module_file:
                    content = module_file.read()
                kwargs = {'parent': parent, 'name': element[:-3],
                    'content': content}
                module = py.Module(**kwargs)
                if not module.analyze():
                    print('Failed to analyze module {module.name}'.format(module=module))
                    module.delete()
        return True

    def update_from_sources(self, **kwargs):
        """read sources and update model"""
        src_dir = kwargs.get('dir', self._dir)  # this is the root dir
        src_dir = os.path.abspath(src_dir)
        target_dir = os.path.abspath(self._dir)
        # ensure the dir exists
        import traceback
        try:
            if not os.path.exists(target_dir):
                os.makedirs(target_dir)
            progress = kwargs.get('progress', None)
            if self._language == 'python':
                from beatle.analytic import PyParse
                PyParse.set_project(self)
                if self.import_python_dir(self, src_dir, progress):
                    # refresh vincled files
                    self.refresh_project_files()
            elif self._language == 'c++':
                if self.import_cpp_dir(self, src_dir, progress):
                    # refresh vincled files
                    self.refresh_project_files()

        except Exception as e:
            wx.MessageBox('Failed : {0}'.format(e), 'Error', wx.OK | wx.ICON_ERROR)
            traceback.print_exc(file=sys.stdout)
            print(type(e))     # the exception instance
            print(e.args)      # arguments stored in .args
            print(e)
            return False
        return True

    def refresh_project_files(self):
        """Refresh the files contained in a sngle project"""
        # The project itself represents the root directory
        # so we need to scan the root dir
        _dir = os.path.realpath(self._dir)
        if not os.access(_dir, os.R_OK):
            return False
        # create a temporal dictionary
        r = os.path.realpath
        j = os.path.join
        if self._language == 'c++':
            main_class_list = self.level_classes
            modules = self.modules
            self.cpp_set = dict([('{}.cpp'.format(c.base_filename), c) for c in main_class_list])
            self.cpp_set.update(dict([(x._source, x) for x in modules if x._source is not None]))
            self.h_set = dict([('{}.h'.format(c.base_filename),c) for c in main_class_list])
            self.h_set.update(dict([(x._header, x) for x in modules if x._header is not None]))
        elif self._language == 'python':
            self.file_dict = dict([(r(j(x.dir, '{x._name}.py'.format(x=x))), x)
                for x in self.modules])
            self.file_dict.update([(r(j(x.dir, '__init__.py')), x)
                for x in self.packages])

        # list all files excludding hidden files
        mask_files = [r'\.(.)*', r'__pycache__', r'.*\.pyc', r'(.)*\.pcc']
        excluded = [re.compile(x) for x in mask_files]
        entries = os.listdir(_dir)
        entries.sort()
        _fset = [s for s in entries if not any(y.match(s) for y in excluded)]
        kwargs = {'parent': self, 'transactional': False}
        knowns = self[arch.File] + self[arch.Dir]
        name_knowns = dict([(x.abs_file, x) for x in knowns])
        _fset.sort()
        for f in _fset:
            element = os.path.join(_dir, f)
            if element in name_knowns:
                name_knowns[element].Refresh()
                del name_knowns[element]
                continue
            kwargs['file'] = element
            if os.path.isdir(element):
                arch.Dir(**kwargs).Refresh()
            else:
                arch.File(**kwargs).Refresh()
        for k in name_knowns:
            name_knowns[k].delete()
        if self._language == 'c++':
            if hasattr(self, 'cpp_set'):
                del self.cpp_set
            if hasattr(self, 'h_set'):
                del self.h_set
        elif self._language == 'python':
            if hasattr(self, 'file_dict'):
                del self.file_dict
        return True

