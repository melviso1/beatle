# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:31:28 2013

@author: mel
"""


from beatle.lib.tran import TransactionalMethod, TransactionStack, TransactionalMoveObject
from ._CCTComponent import CCTComponent


class Argument(CCTComponent):
    """Implements argument representation"""

    context_container = True

    @TransactionalMethod('move argument {0}')
    def drop(self, to):
        """drop this elemento to another place"""
        target = to.inner_argument_container
        if not target or to.project != self.project:
            return False  # avoid move arguments between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._typei = kwargs['type']
        self._default = kwargs.get('default', '')
        super(Argument, self).__init__(**kwargs)
        container = self.outer_class or self.outer_module
        container._lastSrcTime = None
        container._lastHdrTime = None
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def type_instance(self):
        """return the type instance"""
        return self._typei

    @property
    def default(self):
        return self._default

    @default.setter
    def default(self, value):
        self._default = value

    def delete(self):
        """Handle delete"""
        from ._Constructor import Constructor
        if type(self._parent) is Constructor:
            self._parent.save_state()
            self._parent._argsTouched = True
        k = self.outer_class or self.outer_module
        super(Argument, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs['type'] = self._typei
        kwargs['default'] = self._default
        kwargs.update(super(Argument, self).get_kwargs())
        return kwargs

    def on_undo_redo_changed(self):
        """Update from app"""
        self.parent.on_undo_redo_changed()
        super(Argument, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    def on_undo_redo_removing(self):
        """Do required actions for removing"""
        super(Argument, self).on_undo_redo_removing()
        self.parent.on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo.
        Note: The order of the factors is very critical!"""
        super(Argument, self).on_undo_redo_add()
        self.parent.on_undo_redo_changed()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('member')

    @property
    def tree_label(self):
        """Get tree label"""
        init = ''
        if len(self._default) > 0:
            init += "=" + self._default
        return str(self._typei).format(self._name) + init

    @property
    def implement(self):
        """return the label whitout default value"""
        return str(self._typei).format(self._name)

    @property
    def declare(self):
        """return the label whitout default value"""
        init = ''
        if len(self._default) > 0:
            init += "=" + self._default
        return str(self._typei).format(self._name) + init

