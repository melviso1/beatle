# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 08:28:25 2013

@author: mel
"""



from beatle.lib.tran import TransactionStack, TransactionalMoveObject, TransactionalMethod

from ._CCContext import ContextDeclaration
from ._CCTComponent import CCTComponent

class Enum(CCTComponent):
    """Implements member data"""
    context_container = True

    #visual methods
    @TransactionalMethod('move enum {0}')
    def drop(self, to):
        """drop this elemento to another place"""
        if self.inner_class is None:
            return False
        target = to.inner_member_container
        if not target or to.project != self.project:
            return False  # avoid move arguments between projects
        index = 0  # trick for insert as first child
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    @property
    def is_template(self):
        return False

    def __init__(self, **kwargs):
        """Initialization. The items argument is an optional list
        whose elements are a pair (label, value). """
        self._items = kwargs.get('items', [])
        self._access = kwargs.get('access', 'public')
        super(Enum, self).__init__(**kwargs)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(Enum, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs['items'] = self._items
        kwargs['access'] = self._access
        kwargs.update(super(Enum, self).get_kwargs())
        return kwargs

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("enum", self._access)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Enum, self).on_undo_redo_add()

    def on_undo_redo_changed(self):
        """Make changes in the model as result of change"""
        super(Enum, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    @property
    def scoped(self):
        """Get the scope"""
        return '{self.parent.scope}{self._name}'.format(self=self)

    @property
    def scope(self):
        """Get the scope"""
        return '{scoped}::'.format(scoped=self.scoped)

    @property
    def label(self):
        """Get tree label"""
        return 'enum {self._name}'.format(self=self)

    @ContextDeclaration()
    def write_declaration(self, pf):
        """Write the declaration of the enum using writer"""
        cls = self.inner_class
        if cls is not None:
            cls.ensure_access(pf, self._access)
        #ok, write it
        self.write_comment(pf)
        pf.write_line('enum {self._name}'.format(self=self))
        pf.open_brace()
        if len(self._items) > 0:
            for i in range(0, len(self._items) - 1):
                item = self._items[i]
                k = item[0].strip()
                s = item[1].strip()
                if len(s):
                    pf.write_line("{0}={1},".format(k, s))
                else:
                    pf.write_line("{0},".format(k))
            item = self._items[-1]
            k = item[0].strip()
            s = item[1].strip()
            if len(s):
                pf.write_line("{0}={1}".format(k, s))
            else:
                pf.write_line("{0}".format(k))
        pf.close_brace(";")


