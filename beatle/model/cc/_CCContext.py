# -*- coding: utf-8 -*-

from beatle.lib.tran import TransactionObject


class ContextDeclaration(object):
    """Class decorator for context dclaration"""
    def __init__(self):
        """"""
        super(ContextDeclaration, self).__init__()

    def __call__(self, method):
        """"""
        def wrapped_call(*args, **kwargs):
            """Code for context method"""
            from beatle.model import Project, Workspace
            this = args[0]
            pf = args[1]
            if getattr(this, '_declare', True):
                path = [x for x in this.path if type(x) not in [Workspace, Project]]
                for element in path:
                    element.write_contexts_prefix_declaration(pf)
                method(*args, **kwargs)
                for element in path:
                    element.write_contexts_suffix_declaration(pf)
        return wrapped_call


class ContextImplementation(object):
    """Class decorator for context implementation"""
    def __init__(self):
        """Initialice context for implementation"""
        super(ContextImplementation, self).__init__()

    def __call__(self, method):
        """"""
        def wrapped_call(*args, **kwargs):
            """Code for context method"""
            from beatle.model import Project, Workspace
            this = args[0]
            pf = args[1]
            if getattr(this, '_implement', True):
                path = [x for x in this.path if type(x) not in [Workspace, Project]]
                for element in path:
                    element.write_contexts_prefix_implementation(pf)
                method(*args, **kwargs)
                for element in path:
                    element.write_contexts_suffix_implementation(pf)
        return wrapped_call


class ContextItem(TransactionObject):
    """Implements a context item used"""
    def __init__(self, **kwargs):
        """Initialize context item"""
        self._name = kwargs.get('name', '')
        self._define = kwargs.get('define', '')
        self._enable = kwargs.get('enable', False)
        self._prefix_declaration = kwargs.get('prefix_declaration', '')
        self._suffix_declaration = kwargs.get('suffix_declaration', '')
        self._prefix_implementation = kwargs.get('prefix_implementation', '')
        self._suffix_implementation = kwargs.get('suffix_implementation', '')
        self._note = kwargs.get('note', '')
        super(ContextItem, self).__init__()

    @property
    def name(self):
        return self._name

    def save_state(self):
        """Do save state"""
        super(ContextItem, self).save_state()

    def delete(self):
        """Do a transactional delete for any object"""
        super(ContextItem, self).delete()
