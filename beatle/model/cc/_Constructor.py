# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:08:46 2013

@author: mel
"""


import copy, re

from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject
from beatle.lib.api import context
from ._CCContext import ContextDeclaration, ContextImplementation
from ._Member import Member
from ._Argument import Argument
from ._Type import typeinst
from ._Inheritance import Inheritance
from ._MemberData import MemberData


class Constructor(Member):
    """Implements ctor method"""
    context_container = True
    argument_container = True

    # visual methods
    @TransactionalMethod('move constructor {0}')
    def drop(self, to):
        """Drops class inside project or another folder """
        target = to.inner_member_container
        if not target or self.inner_class != target.inner_class:
            return False  # avoid move ctors outside his class
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        # the name must be allways the same of the class
        kwargs['name'] = kwargs['parent'].inner_class.name
        self._access = kwargs.get('access', "public")
        self._explicit = kwargs.get('explicit', False)
        self._inline = kwargs.get('inline', False)
        self._calling_convention = kwargs.get('calling_convention', None)
        self._init = kwargs.get('init', "")
        self._content = kwargs.get('content', "")
        self._preferred = kwargs.get('preferred', False)
        self._deleted = kwargs.get('deleted', False)
        self._initTouched = False
        self._argsTouched = False
        self._bodyTouched = False
        self._serial = False  # internal flag from TCommon
        self._template = kwargs.get('template', None)
        self._template_types = kwargs.get('template_types', [])
        if not hasattr(kwargs, 'note'):
            kwargs['note'] = 'Constructor.'
        super(Constructor, self).__init__(**kwargs)
        autoinit_forced = 'autoinit' in kwargs
        auto_init = True
        if autoinit_forced:
            auto_init = kwargs['autoinit']
        autoargs_forced = 'autoargs' in kwargs
        autoargs = True
        if autoargs_forced:
            autoargs = kwargs['autoargs']
        if autoargs:
            self.auto_args(force=autoargs_forced)
        if auto_init:
            self.auto_init(save=False, force=autoinit_forced)
            self.update_init_call(save=False)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def is_template(self):
        if self._template is not None and len(self._template) > 0:
            return True
        else:
            return False

    @property
    def access(self):
        return self._access

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(Constructor, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'access': self._access,
            'explicit': self._explicit,
            'inline': self._inline,
            'calling_convention': self._calling_convention,
            'init': self._init,
            'content': self._content,
            'template': self._template,
            'template_types': self._template_types,
            'preferred': self._preferred,
            'deleted': getattr(self, '_deleted', False),  # to reduce in next releases
        }
        kwargs.update(super(Constructor, self).get_kwargs())
        return kwargs

    @property
    def init(self):
        return self._init

    @property
    def content(self):
        return self._content

    @ContextDeclaration()
    def write_declaration(self, file_writer):
        """Write the ctor declaration"""
        i = self.inner_class
        i.ensure_access(file_writer, self._access)
        file_writer.write_line(self.declare)
        if self._inline or i.is_template or self._template:
            self.outer_class._inlines.append(self)

    def auto_args(self, force=False):
        """Computes required arguments"""
        from ._Relation import RelationFrom
        from ._InitMethod import InitMethod
        if not force and ( self._argsTouched or self._read_only ):
            return
        s = copy.copy(self[Argument])
        for arg in s:
            arg.delete()

        parent_class = self.inner_class
        # get_constructor_arguments returns a list of arguments
        # required by initialization of base clases or members
        args = []
        # Add required arguments from init methods first
        init_method = parent_class(InitMethod, filter=lambda x: x.inner_class == parent_class, cut=True)
        if len(init_method) > 0:
            args = init_method[0][Argument]
        args = args + parent_class.get_constructor_arguments()
        used = []
        with_default = [x for x in args if len(x.default) > 0]
        without_default = [x for x in args if x not in with_default]
        with_default_names = [x.name for x in with_default]
        for x in without_default:
            if x.name in used or x.name in with_default_names:
                continue
            used.append(x.name)
            if type(x) is RelationFrom:  # this is old stuff? I think that never happens because of init method args
                base_type = x.to.inner_class
                if base_type.is_template:
                    arg_type_instance = typeinst(
                        type=base_type, type_args=base_type.template_args,  ref=True, const=True)
                else:
                    arg_type_instance = typeinst(type=base_type,  ref=True, const=True)
            else:
                arg_type_instance = copy.copy(x.type_instance)
            Argument(parent=self, name=x.name, type=arg_type_instance)
        for x in with_default:
            if x.name in used:
                continue
            used.append(x.name)
            if type(x) is RelationFrom:  # this is old stuff? I think that never happens because of init method args
                base_type = x.to.inner_class
                if base_type.is_template:
                    arg_type_instance = typeinst(
                        type=base_type, type_args=base_type.template_args,  ref=True, const=True)
                else:
                    arg_type_instance = typeinst(type=base_type,  ref=True, const=True)
            else:
                arg_type_instance = copy.copy(x.type_instance)
            Argument(parent=self, name=x.name, type=arg_type_instance, default=x.default)

    def update_init_call(self, save=True):
        """Updates the call to init method"""
        if self._serial:  # if this is a serial ctor, is special
            return
        if save:
            self.save_state()
        method = self.inner_class.init_methods
        mask = r'\t__init__\s*\(.*\);\n'
        if not method:
            # if there are not init methods, remove the call
            self._content = re.sub(mask, '', self._content)
            return
        method = method[0]
        # create the call
        call_string = '\t__init__( '+', '.join(arg.name for arg in method[Argument])+' );\n'
        # we need to find the method call
        if re.search(mask, self._content):
            self._content = re.sub(mask, call_string, self._content)
        else:
            self._content = call_string + self._content

    def auto_init(self, save=True, force=False):
        """Computes the init segment"""
        # Some remembering here: If this is a serial constructor, we must
        # take the care to init nested (non trivial) serializable members
        # by also using his serial constructors
        if not force and (self._initTouched or (self._read_only and not self._serial)):
            return
        if save:
            self.save_state()
        # get inheritance classes
        cls = self.inner_class
        # get the virtual base classes
        virtual_bases = cls.virtual_bases
        local_inheritance_map = dict(
            [(x, x.ancestor) for x in cls(Inheritance, filter=lambda x: x.inner_class == cls, cut=True)
             if x.ancestor not in virtual_bases])
        # create a lines array
        lines = []
        if len(virtual_bases) > 0:
            # l.append("\t//virtual non_virtual_bases")
            for x in virtual_bases:
                ctor = x.get_preferred_constructor(self._serial)
                s = ''
                if ctor is not None:
                    s = ','.join(arg.name for arg in ctor[Argument])
                lines.append(f",    {x.scoped}{{{s}}}")
        if len(local_inheritance_map) > 0:
            # l.append("\t//direct bases")
            for inherit in local_inheritance_map:
                x = local_inheritance_map[inherit]
                ctor = x.get_preferred_constructor(self._serial)
                s = ''
                if ctor is not None:
                    s = ','.join(arg.name for arg in ctor[Argument])
                # for template classes we must add template arguments
                # because the template can appear in base classes or more than once
                prefix = [item._prefix_implementation for item in getattr(x, '_contexts', [])
                          if len(item._prefix_implementation) > 0]
                for prefix_item in prefix:
                    lines.append(f"    {prefix_item}")
                if x.is_template:
                    lines.append(f",    {x.scoped}<{inherit.template_args}>{{{s}}}")
                else:
                    lines.append(f",    {x.scoped}{{{s}}}")
                suffix = [item._suffix_implementation for item in getattr(x, '_contexts', [])
                          if len(item._suffix_implementation) > 0]
                for suffix_item in prefix:
                    lines.append(f"    {suffix_item}")
        # ok, now initialize members
        members = cls(MemberData, filter=lambda x: x.inner_class == cls and not x.static)
        if len(members) > 0:
            # l.append("\t//data members")
            for x in members:
                prefix = [item._prefix_implementation for item in getattr(x, '_contexts', [])
                          if len(item._prefix_implementation) > 0]
                for prefix_item in prefix:
                    lines.append(f"    {prefix_item}")

                initializer = x.get_initializer(self.is_serial)
                # filter old-style construction methods by value
                # TODO : implement a style flag for choosing the initialization
                # to use initializer list or not, in the arguments
                if initializer in ['CRITICAL_SECTION_INIT', ]:
                    lines.append(f",    {x.prefixed_name}({initializer})")
                else:
                    lines.append(f",    {x.prefixed_name}{{{initializer}}}")
                suffix = [item._suffix_implementation for item in getattr(x, '_contexts', [])
                          if len(item._suffix_implementation) > 0]
                for suffix_item in suffix:
                    lines.append(f"    {suffix_item}")
        # Ok, now, generate initialization
        self._init = '\n'.join(lines).replace(',', ':', 1)

        # Update visuals
        if getattr(self, '_pane', None) is not None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(self._pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            self._pane.m_init.SetText(self._init)

    def set_preferred(self, flag):
        """Set the preferred flag"""
        if flag:
            # if any one ctor has preferred flag, simply
            # save his state and change
            cls = self.inner_class
            preferred_constructors = cls(Constructor, filter=lambda x: x.inner_class == cls and x.is_preferred() == True)
            if len(preferred_constructors):
                for x in preferred_constructors:
                    setattr(x, '_preferred', False)
        self._preferred = flag

    def is_preferred(self):
        """Get the preferred flag"""
        return self._preferred

    @property
    def code_lines(self):
        """Return the number of line codes.
        This is used for mapping from file to object.
        """
        lines = 1 + len(self._content.splitlines())
        if len(self._init) > 0:
            lines += len(self._init.splitlines())
        return lines

    @ContextImplementation()
    def write_code(self, f):
        """Write code to file"""
        self.write_comment(f)
        f.write_line(self.implement)
        if self._inline:
            setattr(self, 'source_line', -1)  # store start
        else:
            setattr(self, 'source_line', f.line)  # store start
        if len(self._init) > 0:
            f.write_line(self._init)
        f.open_brace()
        f.write_line(self._content)
        f.close_brace()
        f.write_newline()

    def open_line(self, line):
        """This is a special method for open until some useful circumstances"""
        frame = context.get_frame()
        book = frame.docBook
        this_pane = getattr(self, '_pane', None)
        if this_pane is None:
            from beatle.activity.models.cc.ui import pane
            this_pane = pane.ConstructorPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            book.AddPage(this_pane, self.tab_label, True, self.bitmap_index)
        else:
            index = book.GetPageIndex(this_pane)
            book.SetSelection(index)
        if len(self._init) > 0:
            init_lines = len(self._init.splitlines())
            if  init_lines >= line:
                this_pane.goto_init_line(line)
                return True
            line -= init_lines
        this_pane.goto_content_line(line - 1, True)
        return True

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Constructor, self).on_undo_redo_changed()
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(this_pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            if self._pane.m_init.GetText() != self._init:
                self._pane.m_init.SetText(self._init)
            if self._pane.m_editor.GetText() != self._content:
                self._pane.m_editor.SetText(self._content)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Constructor, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for unload"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Constructor, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Constructor, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from beatle.activity.models.cc.ui import pane
            frame = context.get_frame()
            book = frame.docBook
            this_pane = pane.ConstructorPane(book, frame, self)
            setattr(self, '_pane',  this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    def get_tab_bitmap(self):
        """Get the bitmap for tab control"""
        from beatle.app.resources import get_bitmap
        return get_bitmap("constructor", self._access)

    @property
    def deleted(self):
        return getattr(self, '_deleted', False)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('constructor', self._access)

    @property
    def tree_label(self):
        """Get tree label"""
        if self._template:
            prolog = 'template<{0}> '.format(self._template)
        else:
            prolog = ''
            if self._inline:
                prolog = "inline " + prolog
        argl = ', '.join(x.declare for x in self.sorted_child if type(x) is Argument)
        if self.deleted:
            return '{prolog}{name}({argl}) = delete'.format(prolog=prolog, name=self._name, argl=argl)
        else:
            return '{prolog}{name}({argl})'.format(prolog=prolog, name=self._name, argl=argl)

    @property
    def call(self):
        """Call default expression"""
        argl = ', '.join(arg.name for arg in self[Argument])
        cls = self.inner_class
        scope = cls.parent.scope
        return "{scope}{name}({argl})".format(scope=scope, name=self._name, argl=argl)

    @property
    def tab_label(self):
        """Get tree label"""
        argl = ', '.join(arg.implement for arg in self[Argument])
        return '{name}({argl})'.format(name=self._name, argl=argl)

    @property
    def declare(self):
        """Get the string for declaration."""
        s = ''
        if self._template:
            s = 'template<{0}> '.format(self._template)
        if self._explicit:
            s += 'explicit '
        epilog = ""
        if self._calling_convention is not None:
            epilog += " __attribute__(({self._calling_convention}))".format(self=self)

        arg_list = ', '.join(x.declare for x in self.sorted_child if type(x) is Argument)
        if self.deleted:
            return s+'{self._name}({args}) = delete;'.format(
                self=self, args=arg_list)
        else:
            return s+'{self._name}({args}){epilog};'.format(
                self=self, args=arg_list, epilog=epilog)

    @property
    def implement(self, no_default=False):
        """Get the string for implementation"""
        arg_list = ', '.join(x.implement for x in self.sorted_child if type(x) is Argument)
        cls = self.inner_class
        # we don't handle nested template classes until now
        # for support of relations we add this trick
        s = ''
        template_class = (cls.is_template and cls) or (cls.outer_class.is_template and cls.outer_class) or None
        if template_class is not None:
            return 'template<{cls.clean_template}> {self.scope}{self._name}({args})'.format(
                cls=template_class, self=self, args=arg_list)
        if self._template:
            return 'template<{self.clean_template}> {self.scope}{self._name}({args})'.format(
                self=self, args=arg_list)
        if self._inline:
            return 'inline {self.scope}{self._name}({args})'.format(
                self=self, args=arg_list)
        return '{self.scope}{self._name}({args})'.format(
            self=self, args=arg_list)

    def exists_named_argument(self, name):
        """Returns information about argument existence"""
        return self.find_child(name=name, type=Argument)

    @property
    def template_types(self):
        """Returns the list of nested template types"""
        # we need to add types from template nested classes
        template_types = self._template_types
        super_template_types = super(Constructor, self).template_types
        template_types.extend([x for x in super_template_types if x not in template_types])
        return template_types

    @property
    def is_template(self):
        if self._template is not None and len(self._template) > 0:
            return True
        else:
            return False

    @property
    def is_inline(self):
        return self._inline

    @property
    def clean_template(self):
        """Return template string without defaults"""
        return re.sub('=[^,]*', '', self._template)

    def __setstate__(self, d):
        """Load pickle context.
        This will add some missing attributes in previous releases."""
        d['_argsTouched'] = d.get('_argsTouched', False)
        d['_bodyTouched'] = d.get('_bodyTouched', False)
        if hasattr(d, '_calling'):
            d['_calling_convention'] = d['_callingText']
            del d['_calling']
            del d['_callingText']
        d['_calling_convention'] = d.get('_calling_convention', None)
        super(Constructor, self).__setstate__(d)
