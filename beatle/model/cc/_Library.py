# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 23:24:30 2013

@author: mel
"""
from ._Folder import Folder
from ._Namespace import Namespace
from ._Class import Class
from ._Enum import Enum
from ._MemberData import MemberData
from ._Constructor import Constructor
from ._MemberMethod import MemberMethod
from ._Destructor import Destructor
from ._Module import Module
from ._Function import Function
from ._Data import Data

from ._CCContext import ContextDeclaration
from ._CCTComponent import CCTComponent

class Library(CCTComponent):
    """Implements a Library representation"""
    class_container = True
    context_container = True
    folder_container = True
    diagram_container = True
    module_container = True
    namespace_container = True
    function_container = True
    variable_container = True
    member_container = False

    def __init__(self, **kwargs):
        """Initialization"""
        super(Library, self).__init__(**kwargs)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        return super(Library, self).get_kwargs()

    @ContextDeclaration()
    def write_declaration(self, pf):
        """Write the folder declaration"""
        self.write_comment(pf)
        #we will follow and respect the visual order
        child_types = [Folder, Namespace, Class, Enum, MemberData, Constructor, 
                       MemberMethod, Destructor, Module, Function, Data]
        for _type in child_types:
            if len(self[_type]):
                for item in self[_type]:
                    item.write_declaration(pf)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("library")

    def exists_named_member_data(self, name):
        """Check recursively about the existence of nested child member"""
        return self.find_child(name=name, type=MemberData)

    @property
    def nested_classes(self):
        """Returns the list of nested classes (including self)"""
        if type(self.parent) not in [Folder, Class]:
            return []
        return self.parent.nested_classes
