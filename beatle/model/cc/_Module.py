# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 23:24:30 2013

@author: mel
"""

import os


from beatle.lib.tran import DelayedMethod
from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject, TransactionStack, DelayedMethod
from beatle.model.writer import Writer
from ._CCTComponent import CCTComponent
from ._SetterMethod import SetterMethod
from ._GetterMethod import GetterMethod
from ._Constructor import Constructor
from ._Destructor import Destructor
from ._MemberMethod import MemberMethod
from ._InitMethod import InitMethod
from ._ExitMethod import ExitMethod
from ._IsClassMethod import IsClassMethod
from ._Function import Function


class Module(CCTComponent):
    """Implements a Module representation"""
    class_container = False
    folder_container = True
    diagram_container = True
    namespace_container = True
    function_container = True
    variable_container = True
    enum_container = True

    def __init__(self, **kwargs):
        """Initialization"""
        self._header = kwargs.get('header', None)
        self._source = kwargs.get('source', None)
        self._lastSrcTime = None
        self._lastHdrTime = None
        self._header_obj = None  # vincle with file
        self._source_obj = None  # vincle with file
        if 'user_code_h1' in kwargs:
            self.user_code_h1 = kwargs['user_code_h1']
        if 'user_code_h4' in kwargs:
            self.user_code_h4 = kwargs['user_code_h4']
        if 'user_code_s1' in kwargs:
            self.user_code_s1 = kwargs['user_code_s1']
        if 'user_code_s2' in kwargs:
            self.user_code_s2 = kwargs['user_code_s2']
        if 'user_code_s3' in kwargs:
            self.user_code_s3 = kwargs['user_code_s3']

        super(Module, self).__init__(**kwargs)
        self.export_code_files(force=True)

    def open_line(self, line):
        """Open member method based on line, if possible"""
        from beatle.model.cc import CCProject
        method_classes = [x for x in CCProject.member_method_classes]+[Constructor, Destructor, Function]
        elements = self(*method_classes)
        info_list = [x for x in elements if getattr(x,'source_line', line+1) <= line]
        if len(info_list) == 0:
            return False
        candidate = max([(x.source_line,x) for x in info_list])[1]
        if not hasattr(candidate, 'open_line'):
            return False
        # before doing this as good, compute relative offset and check the line is inside
        line_offset = line - candidate.source_line
        lines = len(candidate.content.splitlines())
        if lines <= line_offset:
            return False
        return candidate.open_line(line_offset)

    # visual methods
    @TransactionalMethod('move module {0}')
    def drop(self, to):
        """Drops module inside project or folder """
        target = to.inner_module_container
        if not target or self.project != target.project:
            return False  # avoid move modules between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def delete(self):
        """Handle delete"""
        super(Module, self).delete()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = super(Module, self).get_kwargs()
        kwargs['header']= self._header
        kwargs['source']= self._source
        if hasattr(self, 'user_code_h1'):
            kwargs['user_code_h1'] = self.user_code_h1
        if hasattr(self, 'user_code_h4'):
            kwargs['user_code_h4'] = self.user_code_h4
        if hasattr(self, 'user_code_s1'):
            kwargs['user_code_s1'] = self.user_code_s1
        if hasattr(self, 'user_code_s2'):
            kwargs['user_code_s2'] = self.user_code_s2
        if hasattr(self, 'user_code_s3'):
            kwargs['user_code_s3'] = self.user_code_s3
        return kwargs

    def on_undo_redo_add(self):
        """Update from app"""
        super(Module, self).on_undo_redo_add()
        self.export_code_files()

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Module, self).on_undo_redo_changed()
        project = self.project
        if not TransactionStack.in_undo_redo():
            project.export_code_files(force=True)

    def on_undo_redo_removing(self):
        """Handle on_undo_redo_removing, prevent generating files"""
        # self.export_code_files()
        super(Module, self).on_undo_redo_removing()

    @property
    def abs_path_source(self):
        """Return the absolute path of source file container
        """
        if self._source is None:
            return ''
        return os.path.join(self.project.sources_dir, self._source)

    def set_source(self, source_obj):
        """Sets a vincle with a file"""
        self._source_obj = source_obj

    @DelayedMethod()
    def export_code_files(self, force=True, logger=None):
        """Do code generation"""
        self.update_sources(force)
        self.update_headers(force)

    def write_code(self, file_writer):
        """Write code for module"""
        #first the variables
        for variable in self.variables:
            variable.write_code(file_writer)
        #next functions
        for function in self.functions:
            function.write_code(file_writer)

    def update_sources(self, force=False):
        """does source generation"""
        if self._source is None:
            return
        sources_dir = self.project.sources_dir
        fname = self.abs_path_source
        regenerate = False
        if force or not os.path.exists(fname) or self._lastSrcTime is None:
            regenerate = True
        elif os.path.getmtime(fname) < self._lastSrcTime:
            regenerate = True
        if not regenerate:
            return
        if not os.path.exists(sources_dir):
            os.makedirs(sources_dir)
            if not os.path.exists(sources_dir):
                return
        with open(fname, 'wt', encoding='ascii', errors='ignore') as f:
            file_writer = Writer.for_file(f)
            #master include file
            project = self.project
            if project._license is not None:
                file_writer.write_line(
                    "/*****************************************************************************************")
                file_writer.write_line(" *")
                file_writer.write_line(" * (c) {date} by {author} ".format(date=project._date, author=project._author))
                file_writer.write_line(" *")
                file_writer.write_line(" * This file is {file}, part of the project {project}.".format(
                    file=self._source, project=project.name))
                file_writer.write_line(" * This project is licensed under the terms of the {license} license.".format(
                    license=project._license))
                file_writer.write_line(" *")
                file_writer.write_line(" * Read the companion LICENSE file for further details.")
                file_writer.write_line(" *")
                file_writer.write_line(
                    " ****************************************************************************************/")
                file_writer.write_newline()

            file_writer.write_line("// {user.before.include.begin}")
            file_writer.write_line(getattr(self, "user_code_s1",""))
            file_writer.write_line("// {user.before.include.end}")
            file_writer.write_newline()
            if project.use_master_include:
                file_writer.write_line('#include "{0}"'.format(project.master_include))
            else:
                file_writer.write_line('#include "{0}"'.format(self._header))
            file_writer.write_newline()
            file_writer.write_line("// {user.before.code.begin}")
            file_writer.write_line(getattr(self, "user_code_s2",""))
            file_writer.write_line("// {user.before.code.end}")
            file_writer.write_newline()
            self.write_code(file_writer)
            file_writer.write_newline()
            file_writer.write_line("// {user.after.code.begin}")
            file_writer.write_line(getattr(self, "user_code_s3",""))
            file_writer.write_line("// {user.after.code.end}")
            file_writer.write_newline()

    def update_headers(self, force=False):
        """Realiza la generacion de fuentes"""
        if self._header is None:
            return
        headers_dir = self.project.headers_dir
        fname = os.path.realpath(os.path.join(headers_dir, self._header))
        regenerate = False
        if force or not os.path.exists(fname) or self._lastHdrTime is None:
            regenerate = True
        elif os.path.getmtime(fname) < self._lastHdrTime:
            regenerate = True
        if not regenerate:
            return
        if not os.path.exists(headers_dir):
            os.makedirs(headers_dir)
            if not os.path.exists(headers_dir):
                return False
        f = open(fname, 'wt', encoding='ascii', errors='ignore')
        file_writer = Writer.for_file(f)
        inlines = []
        project = self.project
        if project._license is not None:
            file_writer.write_line(
                "/*****************************************************************************************")
            file_writer.write_line(" *")
            file_writer.write_line(" * (c) {date} by {author} ".format(date=project._date, author=project._author))
            file_writer.write_line(" *")
            file_writer.write_line(" * This file is {file}, part of the project {project}.".format(
                file=self._header, project=project.name))
            file_writer.write_line(" * This project is licensed under the terms of the {license} license.".format(
                license=project._license))
            file_writer.write_line(" *")
            file_writer.write_line(" * Read the companion LICENSE file for further details.")
            file_writer.write_line(" *")
            file_writer.write_line(
                " ****************************************************************************************/")
            file_writer.write_newline()

        #create safeguard
        safeguard = self._header.upper() + "_INCLUDED"
        safeguard = safeguard.replace('.','_')
        file_writer.write_line()
        file_writer.write_line("#if !defined({0})".format(safeguard))
        file_writer.write_line("#define {0}".format(safeguard))

        file_writer.write_line("// {{user.before.declarations.begin}}")
        file_writer.write_line(getattr(self, 'user_code_h1',''))
        file_writer.write_line("// {{user.before.declarations.end}}")

        file_writer.write_newline()
        #ok, now we place comments, if any
        if len(self._note) > 0:
            file_writer.write_line("/**")
            txt = self._note
            txt.replace('\r', '')
            lines = txt.split("\n")
            for line in lines:
                line.replace('*/', '* /')
                file_writer.write_line("* {0}".format(line))
            file_writer.write_line("**/")
        #write variables (not static)

        file_writer.write_newline()

        # number of variables to declare
        variable_count = len([x for x in self.variables if not x.static])
        method_count = len(self.functions)
        non_template_methods = [x for x in self.functions if not x.is_template]
        non_template_method_count = len(non_template_methods)
        template_methods = [x for x in self.functions if x.is_template]
        template_method_count = len(template_methods)

        if variable_count + method_count > 0:
            ns = self.inner_namespace
            if ns is not None:
                file_writer.write_line('namespace {}{{'.format(ns.scoped))
                file_writer.write_newline()
                file_writer.indent()

            if variable_count > 0:
                file_writer.write_line('//non static global variables')
                #first the (non static) variables
                for variable in self.variables:
                    if not variable.static:
                        variable.write_declaration(file_writer)
                file_writer.write_newline()

            if non_template_method_count > 0:
                file_writer.write_line('//non template methods')
                for function in non_template_methods:
                    function.write_declaration(file_writer)
                file_writer.write_newline()

            if template_method_count > 0:
                file_writer.write_line('//template methods')
                for function in template_methods:
                    function.write_declaration(file_writer)

            if ns is not None:
                file_writer.unindent()
                file_writer.write_line('}}  // end namespace {}'.format(ns.scoped))
                file_writer.write_newline()

        file_writer.write_line("// {{user.after.declarations.begin}}")
        file_writer.write_line(getattr(self, 'user_code_h4',''))
        file_writer.write_line("// {{user.after.declarations.end}}")
        file_writer.write_newline()

        #end safeguard
        file_writer.write_line("#endif //{0}".format(safeguard))
        #write inlines
        file_writer.write_newline()
        if len(inlines) > 0:
            file_writer.write_line("#if defined(INCLUDE_INLINES)")
            file_writer.write_line("#if !defined(INCLUDE_INLINES_{name})".format(name=self._header.upper()))
            file_writer.write_line("#define INCLUDE_INLINES_{name}".format(name=self._header.upper()))
            file_writer.write_newline()
            #write inlines here

            file_writer.write_line("#endif //(INCLUDE_INLINES_{name})".format(name=self._header.upper()))
            file_writer.write_line("#endif //INCLUDE_INLINES")
            file_writer.write_newline()
        self._lastHdrTime = os.path.getmtime(fname)

        file_writer.write_newline()
        f.close()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("module")

    @property
    def inner_class(self):
        """Get the innermost class container"""
        return None

