# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
from beatle.lib.tran import TransactionStack
from ._Data import Data
from ._Function import Function
from ._MemberData import MemberData
from ._MemberMethod import MemberMethod
from ._Argument import Argument
from ._CCTComponent import CCTComponent


class Type(CCTComponent):
    """Implements class representation"""
    def __init__(self, **kwargs):
        """Initialization method"""
        self._access = kwargs.get('access', "public")
        self._definition = kwargs.get('definition', "")
        self._template = kwargs.get('template', None)
        super(Type, self).__init__(**kwargs)
        self.project.export_code_files(force=True)

    def delete(self):
        """Handle delete"""
        project = self.project
        super(Type, self).delete()
        project.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'access': self._access,
            'definition' : self._definition,
            'template' : self._template
        }
        kwargs.update(super(Type, self).get_kwargs())
        return kwargs

    @property
    def is_template(self):
        if self._template is not None and len(self._template) > 0:
            return True
        else:
            return False

    @property
    def template(self):
        return self._template

    @property
    def template_args(self):
        """Retorna una cadena que representa los argumentos del patron.
        Por ejemplo, si la cadena del template es 'typename T,class S=x'
        retorna T,S. Por el momento no se considera el uso de variadic"""
        if self._template is None:
            return ''
        s = str(self.template_types)[1:-1]  # remove '[' ']'
        s = s.replace("u'", '')
        s = s.replace('u"', '')
        s = s.replace("'", '')
        s = s.replace('"', '')
        return s

    @property
    def template_types(self):
        """Retorna la lista de identificadores de tipo utilizados
        en la declaracion de patron"""
        if self._template is None:
            return []
        l = self._template.split(',')
        prologues = ['int', 'class', 'typename']
        r = []
        for u in l:
            v = u.split('=')[0]
            for p in prologues:
                v = v.replace(p, '')
            r.append(v.strip())
        return r

    @property
    def access(self):
        return self._access

    def remove_relations(self):
        """Utility for undo/redo"""
        super(Type, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(Type, self).restore_relations()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(Type, self).on_undo_redo_removing()

    @property
    def can_delete(self):
        """Comprueba si el tipo puede ser eliminado"""
        if self._read_only:
            return False
        project = self.project
        # Obtenemos la lista de miembros de clase
        if not project is None:
            # comprobamos si el tipo esta siendo utilizado
            typed_classes = [MemberData, Function, Argument] + project.member_method_classes
            typed = project(*typed_classes)
            for x in typed:
                if x._typei._type == self:
                    return False
            return True

    def on_undo_redo_changed(self):
        """Update from app"""
        # Obtenemos la lista de miembros de clase
        filter_ = lambda x: x.type_instance.type == self
        for typed in self.project(Data,Function,MemberData,MemberMethod,Argument, filter=filter_):
            typed.on_undo_redo_changed()
        super(Type, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Type, self).on_undo_redo_add()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("type", self._access)

    @property
    def scoped(self):
        """Get the scope"""
        return '{self.parent.scope}{self._name}'.format(self=self)

    def scoped_str(self, scope_list, typename_if_required=True):
        """Gets the string representation with minimal relative scope"""
        s = self.scoped
        best_match = 0
        best_scope = None
        for x in scope_list:
            if s.find(x) == 0:
                if len(x) > best_match:
                    best_match = len(x)
                    best_scope = x
        if best_scope:
            s = s[best_match:]
            if s[:2] == '::':
                s = s[2:]
        if typename_if_required and '<' in s:
            # if this type is dependant, we need to add a typename spec
            index = s.find('>')
            if index > 0 and index+1 < len(s):
                if not s.startswith('typename '):
                    s = "typename " + s
        return s

    @property
    def tree_label(self):
        """Get tree label"""
        # modify old versions
        if not hasattr(self, '_template'):
            self._template = None
        #endif
        if self._template is not None:
            return '{self._name}<{self.template_args}>'.format(self=self)
        return self._name


class typeinst(object):
    """This class represents the common usage of a type"""
    def __init__(self, **kwargs):
        """Initialice type instance"""
        self._type = kwargs.get('type', None)
        self._const = kwargs.get('const', False)
        self._ptr = kwargs.get('ptr', False)
        self._ref = kwargs.get('ref', False)
        self._rref = kwargs.get('rref', False)          # right-value reference
        self._ptr_to_ptr = kwargs.get('ptrptr', False)
        self._const_ptr = kwargs.get('constptr', False)
        self._array = kwargs.get('array', False)
        self._array_size = kwargs.get('arraysize', 0)
        self._funptr = kwargs.get('funptr', False)
        self._type_alias = kwargs.get('type_alias', None)   # for on-the-fly template types
        self._type_args = kwargs.get('type_args', None)   # for template types instance
        if self._funptr:
            self._argdecl = kwargs.get('argdecl', '()')
            self._const_fn = kwargs.get('const_fn', False)
            self._class_fn = kwargs.get('class_fn', None)
        super(typeinst, self).__init__()

    def get_kwargs(self):
        kwargs = {
            'type': self._type,
            'const': self._const,
            'ptr': self._ptr,
            'ref': self._ref,
            'rref': getattr(self, '_rref', False),  # temporary
            'ptrptr': self._ptr_to_ptr,
            'constptr': self._const_ptr,
            'array': self._array,
            'arraysize': self._array_size,
            'funptr': self._funptr,
            'type_alias': self._type_alias,
            'type_args': self._type_args
        }
        if self._funptr:
            kwargs['argdecl'] = self._argdecl
            kwargs['const_fn'] = self._const_fn
            kwargs['class_fn'] = self._class_fn
        return kwargs

    @property
    def type(self):
        return self._type

    @property
    def type_name(self):
        """Return effective type name"""
        if self._type_alias is not None:
            return self._type_alias
        else:
            return self._type.scoped

    @property
    def is_ptr(self):
        """Return info about if the type is a pointer"""
        return self._ptr

    @property
    def is_array(self):
        """Return info about if the type is an array"""
        return self._array

    @property
    def is_ref(self):
        """Return info about if the type is a reference"""
        return self._ref

    @property
    def is_const(self):
        """Return info about constness of type instance"""
        return self._const

    @property
    def is_const_ptr(self):
        """Return info about constness of type instance"""
        return self._const_ptr

    @property
    def is_void(self):
        """Return info about if the type is void"""
        return not self.is_ptr and self.type_name == 'void'

    @property
    def is_rref(self):
        """return info about if the type is right value reference"""
        return getattr(self,'_rref', False)  # temporary getattr filter while old models

    @property
    def is_ptr_to_ptr(self):
        return self._ptr_to_ptr

    @property
    def base_type(self):
        """Return the base type"""
        return self._type

    @property
    def is_template(self):
        return self._type.is_template

    @property
    def template(self):
        return self._type.template

    @property
    def type_alias(self):
        return self._type_alias

    @property
    def type_args(self):
        return self._type_args

    @property
    def scoped(self):
        """Get the scope"""
        if self._type_alias is not None:
            return self._type_alias
        else:
            return self._type.scoped

    def scoped_str(self, scope_list, typename_if_required=True):
        """Gets the string representation with minimal relative scope"""
        s = self.scoped
        best_match = 0
        best_scope = None
        for x in scope_list:
            if s.find(x) == 0:
                if len(x) > best_match:
                    best_match = len(x)
                    best_scope = x
        if best_scope:
            s = s[best_match:]
            if s[:2] == '::':
                s = s[2:]
        typename_required = False
        if typename_if_required and '<' in s:
            # if this type is dependant, we need to add a typename spec
            index = s.find('>')
            if index > 0 and index+1 < len(s):
                typename_required = True
        if self._type_args is not None:
            s = s + '<{0}>'.format(self._type_args)
        if self._const:
            s = "const " + s
        if self._ptr:
            s += "*"
        if self._const_ptr:
            s += " const"
        if self._ptr_to_ptr:
            s += "*"
        if self._ref:
            s += "&"
        # temporary wile fixing old models
        if getattr(self,'_rref', False):
            s += "&"
        if self._funptr:
            if self._class_fn:
                s += '({cls}*'.format(cls=self._class_fn.scope)+'{0})'+ self._argdecl
            else:
                s += ' (*{0})' + self._argdecl
            if self._const_fn:
                s += " const"
        else:
            s += ' {0}'
        if self._array:
            if type(self._array_size) is str:
                s += '[{0}]'.format(self._array_size)
            else:
                s += '[{0}]'.format(str(self._array_size))
        if typename_required:
            if not s.startswith('typename '):
                return 'typename '+s
        return s

    def __str__(self):
        """Returns the type string format"""
        # migrate old versions
        if not hasattr(self, '_type_args'):
            self._type_args = None
        # end
        if self._type_alias is not None:
            s = self._type_alias    # used for on-the-fly template types
        else:
            s = '{self.parent.scope}{self._name}'.format(self=self._type)
        if self._type_args is not None:
            s = s + '<{0}>'.format(self._type_args)
        if self._const:
            s = "const " + s
        if self._ptr:
            s += "*"
        if self._const_ptr:
            s += " const"
        if self._ptr_to_ptr:
            s += "*"
        if self._ref:
            s += "&"
        # temporary wile fixing old models
        if getattr(self,'_rref', False):
            s += "&"
        if self._funptr:
            if self._class_fn:
                s += '({cls}*'.format(cls=self._class_fn.scope)+'{0})'+ self._argdecl
            else:
                s += ' (*{0})' + self._argdecl
            if self._const_fn:
                s += " const"
        else:
            s += ' {0}'
        if self._array:
            if type(self._array_size) is str:
                s += '[{0}]'.format(self._array_size)
            else:
                s += '[{0}]'.format(str(self._array_size))
        return s

