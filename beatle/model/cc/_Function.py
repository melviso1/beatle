# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:08:46 2013

@author: mel
"""

from beatle.lib.api import context
from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject
from ._CCContext import ContextDeclaration, ContextImplementation
from ._CCTComponent import CCTComponent
from ._Argument import Argument


class Function(CCTComponent):
    """Implements member function"""
    context_container = True
    argument_container = True

    # visual methods
    @TransactionalMethod('move function {0}')
    def drop(self, to):
        """Drops datamember inside project or another folder """
        target = to.inner_function_container
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._typei = kwargs['type']
        self._static = kwargs.get('static', False)
        self._inline = kwargs.get('inline', False)
        self._content = kwargs.get('content', "")
        self._template = kwargs.get('template', None)
        self._template_types = kwargs.get('template_types', [])
        self._calling_convention = kwargs.get('calling_convention', None)
        super(Function, self).__init__(**kwargs)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    @property
    def static(self):
        return self._static

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(Function, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'type': self._typei,
            'static': self._static,
            'inline': self._inline,
            'content': self._content,
            'template': self._template,
            'template_types': self._template_types
        }
        kwargs.update(super(Function, self).get_kwargs())
        return kwargs

    @ContextDeclaration()
    def write_declaration(self, f):
        """Write the member method declaration"""
        if self._template:
            f.write_newline(2)
            if len(self._note):
                f.open_comment()
                f.write_line(self._note)
                f.close_comment()
            f.write_line(self.implement)
            f.open_brace()
            f.write_line(self._content)
            f.close_brace()
        else:
            f.write_line(self.declare)

    @property
    def code_lines(self):
        """Return the number of line codes.
        This is used for mapping from file to object.
        """
        return len(self._content.splitlines())

    @ContextImplementation()
    def write_code(self, f):
        """Write code to file"""
        if not self._template:
            f.write_newline(2)
            if len(self._note):
                f.open_comment()
                f.write_line(self._note)
                f.close_comment()
            f.write_line(self.implement)
            f.open_brace()
            if self._inline:
                setattr(self, 'source_line', -1)  # store start
            else:
                setattr(self, 'source_line', f.line)  # store start
            f.write_line(self._content)
            f.close_brace()

    def open_line(self, line):
        """This is a special method for open until some useful circumstances"""
        frame = context.get_frame()
        book = frame.docBook
        this_pane = getattr(self, '_pane', None)
        if this_pane is None:
            from beatle.activity.models.cc.ui import pane
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            book.AddPage(this_pane, self.tab_label, True, self.bitmap_index)
        else:
            index = book.GetPageIndex(this_pane)
            book.SetSelection(index)
        this_pane.goto_line(line, True)
        return True

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Function, self).on_undo_redo_changed()
        if hasattr(self, '_pane') and not self._pane is None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(self._pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            if self._pane.m_editor.GetText() != self._content:
                self._pane.m_editor.SetText(self._content)
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Function, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for unload"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Function, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Function, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from beatle.activity.models.cc.ui import pane
            frame = context.get_frame()
            book = frame.docBook
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane',  this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')

    def get_tab_bitmap(self):
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc
        return rc.get_bitmap("function")

    @property
    def type_instance(self):
        """return the type instance"""
        return self._typei

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("function")

    @property
    def call(self):
        """Call default expression"""
        argl = ', '.join(arg.name for arg in self[Argument])
        return "::{name}({argl})".format(name=self.name, argl=argl)

    @property
    def tab_label(self):
        """Get tree label"""
        arg_list = ', '.join(x.implement for x in self.sorted_child if type(x) is Argument)
        string_type = (self._template and 'template<{0}> '.format(self._template)) or ''
        return "{string_type}{name}({arg_list})".format(string_type=string_type,
                                                        name=self._name, arg_list=arg_list)

    @property
    def tree_label(self):
        """Get tree label"""
        if self._template is not None:
            string_type = 'template<{0}> '.format(self._template)
        else:
            if self._inline:
                string_type = "inline "
            else:
                string_type = ''
        string_type += str(self._typei)
        arg_list = ', '.join(x.declare for x in self.sorted_child if type(x) is Argument)
        return string_type.format(self._name + "(" + arg_list + ")")

    @property
    def scoped_declaration(self):
        """Gets tab label"""
        s = str(self._typei)
        arg_list = ', '.join(x.declare for x in self.sorted_child if type(x) is Argument)
        return s.format(" {0}({1})".format(self.scoped, arg_list)) + ";"

    @property
    def declare(self):
        """Gets tab label"""
        s = (self._template is not None and 'template<{0}> '.format(self._template)) or ''
        s += str(self._typei)
        args = ', '.join(arg.declare for arg in self[Argument])
        epilog = ''
        if self._calling_convention is not None:
            epilog = " __attribute__(({self._calling_convention}))".format(self=self)
        return s.format(" {name}({args}){epilog};".format(
            name=self._name, args=args, epilog=epilog))

    @property
    def implement(self):
        """Gets tab label"""
        # --algunas observaciones --
        # cuando se trata de un template, este se declara en su namespace, de modo
        # que no hace falta incluir el scope
        arg_list = ', '.join(x.implement for x in self.sorted_child if type(x) is Argument)
        s = str(self._typei)
        if self._template is not None:
                s = 'template<{0}> '.format(self._template)+s
                if self._template:
                    # This is in header, not explicit instantiation
                    return s.format("{self._name}({arg_list})".format(self=self, arg_list=arg_list))
                else:
                    # This is an explicit instantiation, in source
                    return s.format("{self.scope}{self._name}({arg_list})".format(self=self, arg_list=arg_list))
        else:
            return s.format("{self.scope}{self._name}({arg_list})".format(self=self, arg_list=arg_list))

    def exists_named_argument(self, name):
        """Returns information about argument existence"""
        return self.find_child(name=name, type=Argument)

    @property
    def is_template(self):
        if self._template is not None and len(self._template) > 0:
            return True
        else:
            return False

    @property
    def template(self):
        return self._template

    @property
    def is_inline(self):
        return self._inline

    @property
    def template_types(self):
        """Returns the list of nested template types"""
        lt = self._template_types
        nt = super(Function, self).template_types
        lt.extend([x for x in nt if x not in self._template_types])
        return lt

    def __setstate__(self, d):
        """Load pickle context.
        This will add some missing attributes in previous releases."""
        d['_calling_convention'] = d.get('_calling_convention', None)
        super(Function, self).__setstate__(d)
