# -*- coding: utf-8 -*-
"""
Created on Fri July 3 20:50:32 2015

@author: mel
"""
from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject

from ._Folder import Folder
from ._CCContext import ContextDeclaration
from ._Class import Class
from ._Enum import Enum
from ._MemberData import MemberData
from ._Constructor import Constructor
from ._MemberMethod import MemberMethod
from ._Destructor import Destructor
from ._Module import Module
from ._Function import Function
from ._Data import Data
from ._CCTComponent import CCTComponent


class Namespace(CCTComponent):
    """Implements class representation"""

    class_container = True
    folder_container = True
    diagram_container = True
    module_container = True
    namespace_container = True
    function_container = True
    variable_container = True

    # visual methods
    @TransactionalMethod('move namespace {0}')
    def drop(self, to):
        """drop this element to another place"""
        target = to.inner_namespace_container
        if not target or to.project != self.project:
            return False  # avoid move elements between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initialization method"""
        self._lastSrcTime = None
        self._lastHdrTime = None
        super(Namespace, self).__init__(**kwargs)
        self.update_container()

    def update_container(self):
        """Update the container info"""
        if self.inner_module:
            self.class_container = False
            self.module_container = False
            self.function_container = True
            self.variable_container = True
        else:
            self.class_container = True
            self.module_container = True
            self.function_container = False
            self.variable_container = False

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs.update(super(Namespace, self).get_kwargs())
        return kwargs

    def lower(self):
        """Criteria for sorting when generating code"""
        return self._inheritance_level

    @ContextDeclaration()
    def write_declaration(self, file_writer):
        """Write the namespace declaration"""
        # we will follow and respect the visual order
        self.write_comment(file_writer)
        child_types = [
            Folder, Namespace, Class, Enum, MemberData,
            Constructor, MemberMethod, Destructor, Module, Function, Data]
        file_writer.open_brace('namespace {self._name}'.format(self=self))
        for _type in child_types:
            if len(self[_type]):
                for item in self[_type]:
                    item.write_declaration(file_writer)
        file_writer.close_brace(';')

    @property
    def can_delete(self):
        """Chack bout if this object may be deleted"""
        return super(Namespace, self).can_delete

    def remove_relations(self):
        """Utility for undo/redo"""
        super(Namespace, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(Namespace, self).restore_relations()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(Namespace, self).on_undo_redo_removing()
        k = self.outer_class or self.outer_module or self.project
        if k:
            k.export_code_files(force=True)


    def on_undo_redo_changed(self):
        """Update from app"""
        super(Namespace, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Namespace, self).on_undo_redo_add()
        k = self.outer_class or self.outer_module or self.project
        if k:
            k.export_code_files(force=True)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources
        return resources.get_bitmap_index('namespace')

    @property
    def tree_label(self):
        """Get tree label"""
        return 'namespace {namespace._name}'.format(namespace=self)

    @property
    def nested_classes(self):
        """Returns the list of nested classes (including self)"""
        if type(self.parent) in [Folder, Class, Namespace]:
            return self.parent.nested_classes
        return []

    @property
    def scoped(self):
        """Get the scope"""
        return '{self.parent.scope}{self._name}'.format(self=self)

    @property
    def scope(self):
        """Get the scope"""
        return '{scoped}::'.format(scoped=self.scoped)
