# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:08:46 2013

@author: mel
"""
import re
from beatle.lib.api import context
from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject, format_current_transaction_name
from ._CCContext import ContextDeclaration, ContextImplementation
from ._Member import Member
from ._Argument import Argument
from .._TCommon import TCommon


class MemberMethod(Member):
    """Implements member method"""
    context_container = True
    argument_container = True

    @TransactionalMethod('move method {0}')
    def drop(self, to):
        """Drops datamember inside project or another folder """
        target = to.inner_member_container
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._typei = kwargs['type']
        self._access = kwargs.get('access', 'public')
        self._static = kwargs.get('static', False)
        self._virtual = kwargs.get('virtual', False)
        self._pure = kwargs.get('pure', False)
        self._inline = kwargs.get('inline', False)
        self._const_method = kwargs.get('constmethod', False)
        self._content = kwargs.get('content', "")
        self._deleted = kwargs.get('deleted', False)
        self._template = kwargs.get('template', None)
        self._template_types = kwargs.get('template_types', [])
        self._calling_convention = kwargs.get('calling_convention', None)
        if not hasattr(kwargs,'implement'):
            kwargs['implement'] = not self._pure
        super(MemberMethod, self).__init__(**kwargs)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    @TransactionalMethod('update code of {0}')
    def set_content(self, value):
        self.save_state()
        self._content = value
        format_current_transaction_name(self._name)
        return True

    @property
    def static(self):
        return self._static

    @property
    def is_inline(self):
        return self._inline

    @property
    def is_virtual(self):
        return self._virtual

    @property
    def clean_template(self):
        """Return template string without defaults"""
        return re.sub('=[^,]*', '', self._template)

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(MemberMethod, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'type': self._typei,
            'access': self._access,
            'static': self._static,
            'virtual': self._virtual,
            'pure': self._pure,
            'inline': self._inline,
            'constmethod': self._const_method,
            'content': self._content,
            'template': self._template,
            'template_types': self._template_types,
            'calling_convention': self._calling_convention,
            'deleted': getattr(self, '_deleted', False),  # to reduce in next releases
        }
        kwargs.update(super(MemberMethod, self).get_kwargs())
        return kwargs

    @ContextDeclaration()
    def write_declaration(self, file_writer):
        """Write the member method declaration"""
        if self._declare:
            i = self.inner_class
            i.ensure_access(file_writer, self._access)
            file_writer.write_line(self.declare)
            if self._inline or i._template or self._template:
                self.outer_class._inlines.append(self)

    def write_comment(self, file_writer):
        """Write the note using the writer"""
        # ok, now we place comments, if any
        content = ''
        if len(self._note) > 0:
            content = content + self._note
        for arg in self(Argument):
            if len(arg._note) > 0:
                content = content + '\n\param {name} {desc}'.format(name=arg.name, desc=arg._note)
        if len(content) > 0:
            file_writer.write_line("/**")
            try:
                txt = str(content)  # prevent unicode collision
            except:
                txt = content.encode('utf-8')
            txt.replace('\r', '')
            lines = txt.split("\n")
            for line in lines:
                line.replace('*/', '* /')
                file_writer.write_line("* {0}".format(line))
            file_writer.write_line("**/")

    @property
    def code_lines(self):
        """Return the number of line codes.
        This is used for mapping from file to object.
        """
        return len(self._content.splitlines())

    @ContextImplementation()
    def write_code(self, f):
        """Write code to file"""
        f.write_newline(2)
        self.write_comment(f)
        f.write_line(self.implement)
        f.open_brace()
        if self._inline:
            setattr(self, 'source_line', -1)  # store start
        else:
            setattr(self, 'source_line', f.line)  # store start
        f.write_line(self._content)
        f.close_brace()

    def open_line(self, line):
        """This is a special method for open until some useful circumstances"""
        frame = context.get_frame()
        book = frame.docBook
        this_pane = getattr(self, '_pane', None)
        if this_pane is None:
            from beatle.activity.models.cc.ui import pane
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            book.AddPage(this_pane, self.tab_label, True, self.bitmap_index)
        else:
            index = book.GetPageIndex(this_pane)
            book.SetSelection(index)
        this_pane.goto_line(line, True)
        return True

    def save_state(self):
        """Save current method status"""
        # some notes here:
        # If this method is virtual, we must change at least all the
        # affected attributes of overrides. Due to that, even the
        # save state detects if the object is already saved, we must
        # prevent to recurse into overrides here, because, if not,
        # we could incur in factorial costs.
        if not super(MemberMethod, self).save_state():
            return False
        if self._virtual:

            def full_derivatives(classes_list=self.inner_class.derivatives):
                """Return the list of all the classes that have direct or indirect base from the owner class"""
                new_items = [x for z in classes_list for x in z.derivatives if x not in classes_list]
                if len(new_items) > 0:
                    classes_list.extend(new_items)
                    return full_derivatives(classes_list)
                return classes_list

            # Next loop could cause an annoying nested call with factorial cost
            # The correct solution is to implement the 'saved' method
            for cls in full_derivatives():
                for candidate in cls(MemberMethod, filter=lambda x: x._virtual and x.name == self.name, cut=False):
                    candidate.save_state()

    @TCommon.name.setter
    def name(self, value):
        if self._virtual:
            deriv = self.inner_class.derivatives
            for cls in deriv:
                for candidate in  cls(MemberMethod, filter=lambda x: x._virtual and x.name == self.name):
                    candidate.name = value
        self._name = value

    def on_undo_redo_changed(self):
        """Update from app"""
        super(MemberMethod, self).on_undo_redo_changed()
        if hasattr(self, '_pane') and not self._pane is None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(self._pane)
            book.SetPageText(index, self.tab_label)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            if self._pane.m_editor.GetText() != self._content:
                self._pane.m_editor.SetText(self._content)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(MemberMethod, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for unload"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(MemberMethod, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(MemberMethod, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from beatle.activity.models.cc.ui import pane
            frame = context.get_frame()
            book = frame.docBook
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    def get_tab_bitmap(self):
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc
        return rc.get_bitmap("method", self._access)

    @property
    def deleted(self):
        return getattr(self, '_deleted', False)

    @property
    def type_instance(self):
        """return the type instance"""
        return self._typei

    @property
    def access(self):
        return self._access

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("method", self._access)

    @property
    def tree_label(self):
        """Get tree label"""
        if self._template:
            type_format = 'template<{0}> '.format(self._template)
        else:
            type_format = ''
            if self._static:
                type_format = "static "
            elif self._virtual:
                type_format = "virtual "
            if self._inline:
                type_format = "inline " + type_format
        type_format += str(self._typei)
        arg_list = ', '.join(x.declare for x in self.sorted_child if type(x) is Argument)
        epilog = ""
        if self._const_method:
            epilog += " const"
        if self.deleted:
            epilog += " = delete"
        elif self._pure:
            epilog += " = 0"
        return type_format.format(self._name + "(" + arg_list + ")") + epilog

    @property
    def call(self):
        """Call default expression"""
        args = ', '.join(arg.name for arg in self[Argument])
        return "{name}({args})".format(name=self.name, args=args)

    @property
    def tab_label(self):
        """Get tab label"""
        return self.implement
        #         argl = ', '.join(arg.implement for arg in self[Argument])
#         stype = (self._template and 'template<{0}> '.format(self._template)) or ''
#         return "{stype}{name}({argl})".format(stype=stype, name=self.name, argl=argl)

    @property
    def scoped(self):
        """Get the scope"""
        if self._template:
            return '{self.parent.scope}{self._name}<{self._template}>'.format(self=self)
        return '{self.parent.scope}{self._name}'.format(self=self)

    @property
    def implement(self):
        """Gets tab label"""
        s = ''
        cls = self.inner_class
        template_class = (cls.is_template and cls) or (cls.outer_class.is_template and cls.outer_class) or None
        if template_class is not None:
            s = 'template<{cls.clean_template}> '.format(cls=template_class)
        if self._template:
            s = s + 'template<{0}> '.format(self.clean_template)
        if self._inline and template_class is None:
            s = "inline " + s
        s += str(self._typei)
        args = ', '.join(arg.implement for arg in self[Argument])
        epilog = ""
        if self._const_method:
            epilog += " const"
        untyped = '{self.scope}{self._name}({args})'.format(self=self, args=args)
        return s.format(untyped) + epilog

    @property
    def scoped_declaration(self):
        """declaration string inside class"""
        if self._template:
            s = 'template<{0}> '.format(self._template)
        elif self._static:
            s = "static "
        elif self._virtual:
            s = "virtual "
        else:
            s = ''
        if self._inline:
            s = "inline " + s
        s += str(self._typei)
        argl = ', '.join(arg.declare for arg in self[Argument])
        epilog = ""
        if self._const_method:
            epilog += " const"
        if self._pure:
            epilog += " = 0"
        untyped = '{0}({1}){2}'.format(self.scoped, argl, epilog)
        return s.format(untyped) + ";"

    @property
    def declare(self):
        """declaration string inside class"""
        if self._template:
            s = 'template<{0}> '.format(self._template)
        elif self._static:
            s = "static "
        elif self._virtual:
            s = "virtual "
        else:
            s = ''
        if self._inline:
            s = "inline " + s
        s += str(self._typei)
        args = ', '.join(arg.declare for arg in self[Argument])
        epilog = ""
        if self._const_method:
            epilog += " const"
        if self.deleted:
            epilog += " = delete"
        elif self._pure:
            epilog += " = 0"
        if self._calling_convention is not None:
            epilog += " __attribute__(({self._calling_convention}))".format(self=self)
        untyped = '{self._name}({args}){epilog}'.format(self=self, args=args, epilog=epilog)
        return s.format(untyped) + ";"

    def exists_named_argument(self, name):
        """Returns information about argument existence"""
        return self.find_child(name=name, type=Argument)

    @property
    def is_const_method(self):
        return self._const_method

    @property
    def is_template(self):
        if self._template is not None and len(self._template) > 0:
            return True
        else:
            return False

    @property
    def template(self):
        return self._template

    @property
    def template_types(self):
        """Returns the list of nested template types"""
        # _template_types are locally applied/defined and will override outer
        # template types (of course in any moment we need to alert from that).
        lt = self._template_types
        nt = super(MemberMethod, self).template_types
        lt.extend([x for x in nt if x not in self._template_types])
        return lt

    def __setstate__(self, d):
        """Load pickle context.
        This will add some missing attributes in previous releases."""
        d['_calling_convention'] = d.get('_calling_convention', None)
        super(MemberMethod, self).__setstate__(d)
