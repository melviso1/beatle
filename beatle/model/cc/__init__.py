# -*- coding: utf-8 -*-

from ._CCContext import ContextDeclaration, ContextImplementation, ContextItem
from ._CCProject import CCProject
from ._CCTComponent import CCTComponent
from ._Folder import Folder
from ._Namespace import Namespace
from ._Library import Library
from ._LibrariesFolder import LibrariesFolder
from ._Member import Member
from ._Type import Type, typeinst
from ._Argument import Argument
from ._Class import Class
from ._Constructor import Constructor
from ._Destructor import Destructor
from ._Inheritance import Inheritance
from ._MemberData import MemberData
from ._MemberMethod import MemberMethod
from ._SetterMethod import SetterMethod
from ._GetterMethod import GetterMethod
from ._TypesFolder import TypesFolder
from ._Module import Module
from ._Function import Function
from ._Data import Data
from ._Enum import Enum
from ._IsClassMethod import IsClassMethod
from ._InitMethod import InitMethod
from ._ExitMethod import ExitMethod
from ._Friendship import Friendship
from ._Relation import Relation
from ._Relation import RelationFrom
from ._Relation import RelationTo

from ._ClassDiagram import NO_WHERE
from ._ClassDiagram import IN_CLASS
from ._ClassDiagram import IN_INHERIT
from ._ClassDiagram import IN_AGGREG
from ._ClassDiagram import IN_NOTE
from ._ClassDiagram import IN_LINE
from ._ClassDiagram import IN_SHAPE
from ._ClassDiagram import IN_LINE_MARK

from ._ClassDiagram import DiagramElement
from ._ClassDiagram import ClosedShape
from ._ClassDiagram import RectShape
from ._ClassDiagram import ClassElement
from ._ClassDiagram import ConnectorSegment
from ._ClassDiagram import ConectorElement
from ._ClassDiagram import InheritanceElement
from ._ClassDiagram import NoteElement
from ._ClassDiagram import ClassDiagram

from ._Note import Note

from ._BuildConfig import BuildConfig, BuildSteps, Environment

from . import utils