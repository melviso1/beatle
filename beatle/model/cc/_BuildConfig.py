'''
Created on 1 sept. 2018

@author: mel
'''
import wx
from ._CCTComponent import CCTComponent


class Environment(CCTComponent):
    """Declare a environment var for debug setting"""

    def __init__(self, **kwargs):
        kwargs['visibleInTree'] = False
        self._value = kwargs.get('value','')
        self._enabled = kwargs.get('enabled',True)
        super(Environment, self).__init__(**kwargs)

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value_):
        self._value = value_

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, value):
        self._enabled = value

    def __str__(self):
        return '{self._name}={self._value} '.format(self=self)


class BuildSteps(CCTComponent):
    def __init__(self, **kwargs):
        kwargs['visibleInTree'] = False
        kwargs['name'] = 'build steps configuration'
        super(BuildSteps, self).__init__(**kwargs)
        self._before = []
        self._after = []


class TargetConfig(CCTComponent):
    def __init__(self, **kwargs):
        kwargs['visibleInTree'] = False
        kwargs['name'] = 'build target configuration'
        super(TargetConfig, self).__init__(**kwargs)
        self._type = self.project._type
        self._output = self.project.name
        if self._type == 'static library':
            self._extension = '.a'
            self._prefix = 'lib'
        elif self._type == 'shared library':
            self._extension = '.so'
            self._prefix = 'lib'
        else:
            if "__WXMSW__" in wx.PlatformInfo:
                self._extension = '.exe'
            else:
                self._extension = ''
            self._prefix = ''

    @property
    def type(self):
        return self._type

    @property
    def product(self):
        return '{self._prefix}{self._output}{self._extension}'.format(self=self)


class ArchiverConfig(CCTComponent):
    def __init__(self, **kwargs):
        kwargs['visibleInTree'] = False
        kwargs['name'] = 'build archiver configuration'
        # -- archiver --
        self._command = "ar"
        self._options = ""
        self._pattern = "{COMMAND} {FLAGS} {OUTPUT_FLAG} {OUTPUT_PREFIX}${OUTPUT} {INPUTS}"
        # -- flags --
        self._flags = ""
        super(ArchiverConfig, self).__init__(**kwargs)


class CPPConfig(CCTComponent):
    def __init__(self, **kwargs):
        kwargs['visibleInTree'] = False
        kwargs['name'] = 'build c++ compiler configuration'
        # -- g++ --
        self._command = "g++"
        self._options = ""
        self._pattern = "{COMMAND} {FLAGS} {OUTPUT_FLAG} {OUTPUT_PREFIX}${OUTPUT} {INPUTS}"
        # -- version --
        self._version = ""
        self._version_flags = ""
        # -- preprocessor --
        self._exclude_std_includes = False
        self._preprocess_only = False
        self._defined_symbols = []
        self._undefined_symbols = []
        # -- includes --
        self._include_paths = []
        self._includes = []
        # -- optimization --
        self._level = "none"
        self._level_flags = ""
        # -- debug --
        self._debug_level = "none"
        self._debug_flags = ""
        self._debug_prof = False
        self._debug_gprof = False
        self._debug_gcov = False
        # -- warnings --
        self._warning_syntax_only = False
        self._warning_pedantic = False
        self._warning_pedantic_are_errors = False
        self._warning_disable = False
        self._warning_all = False
        self._warning_extra = False
        self._warning_are_errors = False
        self._warning_conversions = False
        # -- others --
        self._other_flags = ""
        self._verbose = False
        self._position_independent_code = False
        self._pthread_support = False
        super(CPPConfig, self).__init__(**kwargs)

    @property
    def position_independent_code(self):
        return self._position_independent_code

    @position_independent_code.setter
    def position_independent_code(self, value):
        self._position_independent_code = value

    @property
    def pthread_support(self):
        return self._pthread_support

    @pthread_support.setter
    def pthread_support(self, value):
        self._pthread_support = value


    @property
    def FLAGS(self):
        """Return resulting flags, based on configuration"""
        defined_symbols = [x for x in self._defined_symbols if len(x)>0]
        undefined_symbols = [x for x in self._undefined_symbols if len(x)>0]
        include_paths = [x for x in self._include_paths if len(x)>0]
        includes = [x for x in self._includes if len(x)>0]
        flags = ''
        if self._version == "ISO C++ 98 (-std=c++98)":
            flags = '-std=c++98 '
        if self._version == "ISO C++ 03 (-std=c++03)":
            flags = '-std=c++03 '
        elif self._version == "ISO C++ 11 (-std=c++0x)":
            flags = '-std=c++0x '
        elif self._version == "ISO C++ 14 (-std=c++0y)":
            flags = '-std=c++0y '
        elif self._version == "ISO C++ 17 (-std=c++1z)":
            flags = '-std=c++1z '
        elif self._version == "ISO C++ 20 (-std=c++2a)":
            flags = '-std=c++2a '
        if len(self._version_flags)>0:
            flags = '{flags} {self._version_flags} '.format(flags=flags,self=self)
        if self._exclude_std_includes:
            flags = '{flags} -nostdinc '.format(flags=flags)
        if self._preprocess_only:
            flags = '{flags} -E '.format(flags=flags)
        if len(defined_symbols)>0:
            flags = '{flags} -D{defined} '.format(flags=flags, defined=' -D'.join(defined_symbols))
        if len(undefined_symbols)>0:
            flags = '{flags} -U{undefined} '.format(flags=flags, undefined=' -U'.join(undefined_symbols))
        if len(include_paths)>0:
            flags = '{flags} -I{ipath} '.format(flags=flags, ipath=' -I'.join(include_paths))
        if len(includes)>0:
            flags = '{flags} -include{inc} '.format(flags=flags, inc=' -include'.join(includes))
        if self._level == "none":
            flags = '{flags} -O0 '.format(flags=flags)
        elif self._level == "basic":
            flags = '{flags} -O1 '.format(flags=flags)
        elif self._level == "more":
            flags = '{flags} -O2 '.format(flags=flags)
        elif self._level == "expensive":
            flags = '{flags} -O3 '.format(flags=flags)
        elif self._level == "size":
            flags = '{flags} -Os '.format(flags=flags)
        if len(self._level_flags) > 0:
            flags = '{flags} {self._level_flags} '.format(flags=flags, self=self)
        if self._debug_level == "minimal":
            flags = '{flags} -g1 '.format(flags=flags)
        elif self._debug_level == "defaulf":
            flags = '{flags} -g '.format(flags=flags)
        elif self._debug_level == "maximum":
            flags = '{flags} -g3 '.format(flags=flags)
        if self._debug_prof:
            flags = '{flags} -p '.format(flags=flags)
        if self._debug_gprof:
            flags = '{flags} -pg '.format(flags=flags)
        if self._debug_gcov:
            flags = '{flags} -ftest-coverage -fprofile-arcs '.format(flags=flags)
        if self._warning_syntax_only:
            flags = '{flags} -fsyntax-only '.format(flags=flags)
        if self._warning_pedantic:
            flags = '{flags} -pedantic '.format(flags=flags)
        if self._warning_pedantic_are_errors:
            flags = '{flags} -pedantic-errors '.format(flags=flags)
        if self._warning_disable:
            flags = '{flags} -w '.format(flags=flags)
        if self._warning_all:
            flags = '{flags} -Wall '.format(flags=flags)
        if self._warning_extra:
            flags = '{flags} -Wextra '.format(flags=flags)
        if self._warning_are_errors:
            flags = '{flags} -Werror '.format(flags=flags)
        if self._warning_conversions:
            flags = '{flags} -Wconversion '.format(flags=flags)
        if len(self._other_flags):
            flags = '{flags} {self._other_flags} '.format(flags=flags, self=self)
        if self._verbose:
            flags = '{flags} -v '.format(flags=flags)
        if self._position_independent_code:
            flags = '{flags} -fPIC '.format(flags=flags)
        if self._pthread_support:
            flags = '{flags} -pthread '.format(flags=flags)
        return flags

    @property
    def includes(self):
        return self._includes

    @property
    def include_paths(self):
        return self._include_paths

    @include_paths.setter
    def include_paths(self, value):
        self._include_paths = value
             

class CConfig(CCTComponent):
    def __init__(self, **kwargs):
        kwargs['visibleInTree'] = False
        kwargs['name'] = 'build c compiler configuration'
        # -- gcc --
        self._command = "gcc"
        self._options = ""
        self._pattern = "{COMMAND} {FLAGS} {OUTPUT_FLAG} {OUTPUT_PREFIX}${OUTPUT} {INPUTS}"
        # -- version --
        self._version = ""
        self._version_flags = ""
        # -- preprocessor --
        self._exclude_std_includes = False
        self._preprocess_only = False
        self._defined_symbols = []
        self._undefined_symbols = []
        # -- includes --
        self._include_paths = []
        self._includes = []
        # -- optimization --
        self._level = "none"
        self._level_flags = ""
        # -- debug --
        self._debug_level = "none"
        self._debug_flags = ""
        self._debug_prof = False
        self._debug_gprof = False
        self._debug_gcov = False
        # -- warnings --
        self._warning_syntax_only = False
        self._warning_pedantic = False
        self._warning_pedantic_are_errors = False
        self._warning_disable = False
        self._warning_all = False
        self._warning_extra = False
        self._warning_are_errors = False
        self._warning_conversions = False
        # -- others --
        self._other_flags = ""
        self._verbose = False
        self._position_independent_code = False
        self._pthread_support = False
        super(CConfig, self).__init__(**kwargs)


class ASConfig(CCTComponent):
    def __init__(self, **kwargs):
        # -- as --
        kwargs['visibleInTree'] = False
        kwargs['name'] = 'build assembler configuration'
        self._command = "as"
        self._options = ""
        self._pattern = "{COMMAND} {FLAGS} {OUTPUT_FLAG} {OUTPUT_PREFIX}${OUTPUT} {INPUTS}"
        # -- flags --
        self._flags = ""
        self._includes = []
        self._suppres_warnings = False
        self._announce_version = False
        super(ASConfig, self).__init__(**kwargs)


class LinkConfig(CCTComponent):
    def __init__(self, **kwargs):
        # -- linker --
        kwargs['visibleInTree'] = False
        kwargs['name'] = 'build linker configuration'
        super(LinkConfig, self).__init__(**kwargs)
        self._command = "g++"
        self._options = ""
        self._pattern = "{COMMAND} {INPUTS} {FLAGS} {OUTPUT_FLAG} {OUTPUT_PREFIX}${OUTPUT} "
        # -- general --
        self._no_standard_start_files = False
        self._no_default_libraries = False
        self._no_standard_libraries = False
        self._strip_symbols = False
        self._pthreads = False
        # -- libs --
        self._lib_path = []
        self._libs = []
        # -- other --
        self._flags = ""
        self._other_options = []
        self._extra_objects = []
        # -- shared --
        self._shared_name = ""
        self._import_library = ""
        if self.project.type == 'shared library':
            self._shared = True
        else:
            self._shared = False
        self._default_file = ""

    @property
    def pthreads(self):
        return self._pthreads

    @pthreads.setter
    def pthreads(self, value):
        self._pthreads = value

    @property
    def libs(self):
        return self._libs

    @libs.setter
    def libs(self, value):
        self._libs = value

    @property
    def FLAGS(self):
        """Return resulting flags, based on configuration"""
        libs = [x for x in self._libs if len(x)>0]
        lib_path = [x for x in self._lib_path if len(x)>0]

        flags = ''
        if self._no_standard_libraries:
            flags = "-nostartfiles"
        if self._no_default_libraries:
            flags = "{flags} -nodefaultfiles".format(flags=flags)
        if self._no_standard_libraries:
            flags = "{flags} -nostdlib".format(flags=flags)
        if self._strip_symbols:
            flags = "{flags} -s".format(flags=flags)
        if self._pthreads:
            flags = "{flags} -pthread".format(flags=flags)
        
        if len(lib_path)>0:
            flags = '{flags} -L{paths} '.format(flags=flags, paths=' -L'.join(lib_path))
        if len(libs)>0:
            flags = '{flags} -l{paths} '.format(flags=flags, paths=' -l'.join(libs))

        if self._shared:
            flags = '{flags} -shared'.format(flags=flags)
        else:
            flags = '{flags} -no-pie'.format(flags=flags)  # exes with pie can run but OS tags them as shared libs
            
        if len(self._flags):
            flags = '{flags} {self._flags} '.format(flags=flags, self=self)
            
        return flags
    

class BuildConfig(CCTComponent):
    """Store build information for C++ projects"""

    def __init__(self, **kwargs):
        """
        Constructor
        """
        kwargs['visibleInTree'] = False
        super(BuildConfig, self).__init__(**kwargs)
        self._target = TargetConfig(parent=self)
        self._archiver = ArchiverConfig(parent=self)
        self._cpp_compiler = CPPConfig(parent=self)
        self._c_compiler = CConfig(parent=self)
        self._assembler = ASConfig(parent=self)
        self._linker = LinkConfig(parent=self)
        self._build_steps = BuildSteps(parent=self)
        
    def save_state(self):
        super(BuildConfig, self).save_state()
        self._target.save_state()
        self._archiver.save_state()
        self._cpp_compiler.save_state()
        self._c_compiler.save_state()
        self._assembler.save_state()
        self._linker.save_state()
        self._build_steps.save_state()
        
    def on_undo_redo_changed(self):
        super(BuildConfig, self).save_state()
        self.project.export_code_files()

    def append_cpp_include(self, include):
        if include not in self._cpp_compiler._includes:
            self._cpp_compiler._includes.append(include)
            
    def append_cpp_include_path(self, include_path):
        if include_path not in self._cpp_compiler._include_paths:
            self._cpp_compiler._include_paths.append(include_path)
            
    @property
    def target(self):
        return self._target

    @property
    def link_libs(self):
        return self._linker.libs

    @property
    def target_product(self):
        """Return the build product"""
        return self._target.product

    @property
    def cpp_compiler(self):
        return self._cpp_compiler
            
    @property
    def cpp_includes(self):
        return self._cpp_compiler._includes
    
    @property
    def cpp_include_paths(self):
        return self._cpp_compiler._include_paths
    
    @property
    def linker(self):
        return self._linker
            
    def save_state(self):
        self._target.save_state()
        self._archiver.save_state()
        self._cpp_compiler.save_state()
        self._c_compiler.save_state()
        self._assembler.save_state()
        self._linker.save_state()
        super(BuildConfig, self).save_state()
        
    def on_undo_redo_changed(self):
        self.parent.on_undo_redo_changed()
        super(CCTComponent, self).on_undo_redo_changed()
                

        
        
