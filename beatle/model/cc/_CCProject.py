"""
Created on 31 ago. 2018

@author: mel
"""

import os
import platform
import wx

# import beatle
from beatle.lib.api import context
from beatle.lib.utils import cached_type
from beatle.lib.tran import TransactionalMethod, DelayedMethod
from beatle.lib.decorators import upgrade_version
from beatle.model.writer import Writer

from ._Type import Type, typeinst
from ._Namespace import Namespace
from ._Class import Class
from ._Folder import Folder
# from ._LibrariesFolder import LibrariesFolder
from ._TypesFolder import TypesFolder
from ._BuildConfig import BuildConfig
from ._MemberData import MemberData
from ._GetterMethod import GetterMethod
from ._SetterMethod import SetterMethod
from ._InitMethod import InitMethod
from ._ExitMethod import ExitMethod
from ._IsClassMethod import IsClassMethod
from ._MemberMethod import MemberMethod
from ._Constructor import Constructor
from ._Destructor import Destructor
from ._Function import Function
from ._Argument import Argument
from ._Module import Module
from ._Relation import Relation
from ._Inheritance import Inheritance
from ._Friendship import Friendship


class CCProject(object):
    """Handler of C++ projects.
    In order to maintain the Project class as more agnostic as possible,
    all kind of projects must implement his own handlers.
    Comments on transactions:
    The project handlers, like CCProject instances will be always stored as
    Project::_handler values. Modifications of handled values, creation or
    destruction will be allways handled at Project instance level. For this
    reason, project handlers, like CCProject d'ont need to (and can't)
    inherit from TCommon.
    When needed some customization, like labeling transactions, these will
    be conducted from Project, querying handlers."""

    module_classes = [Module]   # Custom modules defined by plugins
    member_method_classes = [MemberMethod, GetterMethod, SetterMethod, IsClassMethod, InitMethod, ExitMethod]
    class_overrides = []        # Custom classes defined by plugins

    def __init__(self, project, **kwargs):
        name = kwargs['name']
        self._project = project
        self._dir = kwargs['dir']
        self._includeDir = kwargs.get('includedir', "include")
        self._srcDir = kwargs.get('sourcedir', "src")
        self._transactional = kwargs.get('transactional', False)
        self._useMaster = kwargs.get('usemaster', True)
        self._masterInclude = kwargs.get('masterinclude', name + ".h")
        self._author = kwargs.get('author', "<unknown>")
        self._date = kwargs.get('date', "08-10-2966")
        self._license = kwargs.get('license', None)
        self._type = kwargs.get('type', "unspecified")
        self._version = kwargs.get('version', [1, 0, 0])
        self._useMakefile = kwargs.get('usemakefile', False)
        self._types = kwargs.get('types', {})
        self._contexts = kwargs.get('contexts', [])
        self._libs = kwargs.get('libs',[])
        self._includes = kwargs.get('includes',[])
        self._references = kwargs.get('references',[])  # external references
        self._current_build_config = None
        # extensions for custom debugging
        self._debug_type = 'local'
        self._debug_arguments = None
        self._local_debug_target = None  # defaults to project.full_path_target_product
        self._local_debug_source_dir = None  # de faults to project.dir
        self._remote_debug_target = None
        self._remote_debug_source_dir = None
        self._remote_debug_host = None
        self._remote_debug_port = None
        self._remote_debug_user = None
        self._remote_debug_pwrd = None
        self.sources_dir = ''
        self.headers_dir = ''
        super(CCProject, self).__init__()

    @upgrade_version
    def __setstate__(self, data_dict):
        """While change between versions we need to maintain
        coherence and create missing elements when loading"""

        _dir = data_dict['_dir']
        if platform.system() == 'Windows':
            _dir = _dir.replace('/', '\\')
        else:
            _dir = _dir.replace('\\', '/')
        data_dict['_dir'] = _dir

        return {
            'add': {
                '_transactional': False,
            }
        }

    def initialize(self):
        """Set custom properties for project"""
        self._project.namespace_container = True
        self._project.class_container = True
        self._project.package_container = False
        self._current_build_config = BuildConfig(parent=self._project, name='Default')
        self._current_build_config.cpp_compiler.include_paths = [self._includeDir]
        for include in self._includes:
            self._current_build_config.append_cpp_include(include)
        del self._includes
        # del self._type
        # LibrariesFolder(parent=self._project, read_only=True)
        types_folder = TypesFolder(parent=self._project, read_only=True)
        self.register_output_dirs()
        # if self._type == 'executable':
        #     main_module = Module(parent=self._project, name='main', header='main.h', source='main.cpp')
        #     main_module.user_code_s2 = "\n#include <cstdio>"
        #     types_dict = dict([(x.name,x) for x in types_folder(Type)])
        #     main_method = Function(
        #         parent=main_module,
        #         name='main',
        #         type=typeinst(type=types_dict['int']),
        #         content='\tprintf("Hello world!\\n");\n\treturn 0;\n')
        #     Argument(
        #         parent=main_method,
        #         name='argc',
        #         type = typeinst(type=types_dict['int'])
        #     )
        #     Argument(
        #         parent=main_method,
        #         name='argv',
        #         type = typeinst(type=types_dict['char'], ptr=True, array=True, arraysize='')
        #     )

    @property
    def support_serialization(self):
        return self._project.is_serial

    @property
    def support_transaction(self):
        return self._transactional

    def get_kwargs(self):
        return {
                'dir': self._dir,
                'includedir': self._includeDir,
                'sourcedir': self._srcDir,
                'transactional': self._transactional,
                'usemaster': self._useMaster,
                'masterinclude': self._masterInclude,
                'author': self._author,
                'date': self._date,
                'license': self._license,
                'type': self.build_type,
                'version': self._version,
                'usemakefile': self._useMakefile,
                'libs': self._libs,
                 # 'includes': self._includes
                }

    def on_undo_redo_changed(self):
        """Update from app"""
        # Normally we must check if we are in a transactional context before doing anything
        # but this object is really a handler and that filter is already done by the project
        # manager class, so we are not in a transactional context here
        def create_serialization_support():
            # check if there is already serialization support
            if len([x for x in self._project[Namespace] if hasattr(x, 'serialization_namespace')]) > 0:
                return
            custom_announce = "/********************************************************\n" \
                              " * This file was automatically generated by beatle 3.0  *\n" \
                              " * in order to provide serialization support.           *\n" \
                              " *                                                      *\n" \
                              " *          DO NOT EDIT BY HAND!                        *\n" \
                              " *          All changes will be overwritten.            *\n" \
                              " *                                                      *\n" \
                              " ********************************************************/\n" \
                # prerrequisites
            # need  std::string type
            string_type = cached_type(self._project,'std::string')
            ostream_type = cached_type(self._project,'std::ostream')
            istream_type = cached_type(self._project, 'std::istream')
            map_type = cached_type(self._project, 'std::map')
            void_type = cached_type(self._project, 'void')

            # prevent crash while destructive update
            if None in [string_type, ostream_type, istream_type, map_type, void_type]:
                return

            # make sure that map_type is a template
            if map_type.template is None:
                setattr(map_type, '_template', 'typename Key, typename Value')

            namespace = Namespace(parent=self._project, name="serial", read_only=True)
            setattr(namespace,'serialization_namespace', True)

            # Add a runtime class that will be used as runtime common base for all serializable classes
            # create a type record  that will be used for storing information about types
            runtime = Class(
                parent=namespace, name="runtime", read_only=True,
                note="This class represents a common base class for all\n"
                     "the classes that must be serializable.")
            setattr(runtime, 'custom_announce', custom_announce)
            type_record = Class(
                parent=namespace, name="type_record", read_only=True,
                note="This class represents the common base class for all\n"
                     "the runtime types registered in the application.")
            # create a type dictionary
            setattr(type_record, 'custom_announce', custom_announce)
            type_map = Class(
                parent=namespace, name="type_map", read_only=True,
                note="This class represents a dictionary holding all the\n"
                     "runtime types registered in the application. This\n"
                     "class is a singleton only accessible trough a static\n"
                     "getter member. This is a workaround required by the\n"
                     "absence of standard initialization order. ")
            # add type instance, a template class that will instance each concrete type
            setattr(type_map, 'custom_announce', custom_announce)
            type_instance = Class(
                parent=namespace,name="type_instance", template="typename T",
                template_types=['T'], template_arguments=['T'], read_only=True,
                note="This class represents the registration of a specific runtime type.\n" \
                     "Each specialization of this template class is a singleton implemented\n" \
                     "as static member of a specialized instance of runtime_class.")
            # add the runtime instance template class that must implement the type registration
            setattr(type_instance, 'custom_announce', custom_announce)
            runtime_instance = Class(parent=namespace, name="runtime_instance", template="typename T",
                                     template_types=['T'], template_arguments=['T'], read_only=True)
            setattr(runtime_instance, 'custom_announce', custom_announce)

            # Add custom classes for handling context stream
            oxstream = Class(parent=namespace, name="oxstream", read_only=True,
                             note="This class is intended to be used as special ostream-like but able to\n"
                                  "handle nested serialization processes.")
            setattr(oxstream, 'custom_announce', custom_announce)
            ixstream = Class(parent=namespace, name="ixstream", read_only=True,
                             note="This class is intended to be used as special istream-like but able to\n"
                                  "handle nested serialization processes.")
            setattr(ixstream, 'custom_announce', custom_announce)


            # add required includes
            type_record.user_code_h1 = "#include <string>"
            runtime.user_code_h1 = "#include <iostream>"
            oxstream.user_code_h1 = "#include <iostream>\n"\
                                    "#include <list>\n"\
                                    "#include <vector>\n"\
                                    "#include <map>\n"\
                                    "#include <type_traits>"
            ixstream.user_code_h1 = "#include <iostream>\n" \
                                    "#include <list>\n"\
                                    "#include <vector>\n"\
                                    "#include <map>\n"\
                                    "#include <type_traits>"

            # ------------------------------------------------
            #
            #           type_record class construction
            #
            #--------------------------------------------------

            # Add a protected constructor
            Constructor(parent=runtime, access='protected', read_only=True)

            # add a member name
            type_name = MemberData(
                parent=type_record, name="name", access="private", type=typeinst(type=string_type), read_only=True)
            # add a getter
            getter = GetterMethod(parent=type_name, read_only=True)

            # add a aggregated avl-tree name-based relation from dictionary to records
            Relation(
                access='protected',
                aggregate=True,
                from_class=type_map, to_class=type_record,
                fromName='map', toName='type',
                implementation='avl_tree',
                member=getter,
                read_only=True
            )

            # add a protected type_record constructor
            type_record_ctor = Constructor(
                parent=type_record, access='protected', read_only=True,
                autoargs=False, autoinit=False, init=":_name(name)", content="__init__( type_map::get() );")
            Argument(parent=type_record_ctor, name='name',
                     type=typeinst(type=string_type, const=True, ref=True), read_only=True)

            # add a type_record virtual method for creating instances
            MemberMethod(parent=type_record, name="create", virtual=True, pure=True,
                         constmethod=True, type=typeinst(type=runtime, ptr=True), read_only=True)

            # add a type_record destructor
            Destructor(parent=type_record, virtual=True, read_only=True)

            # ------------------------------------------------
            #
            #           type_map class construction
            #
            #--------------------------------------------------

            # add a private destructor for the type_map
            Constructor(parent=type_map, access='private', read_only=True)

            # add a static, public getter that will be the only-one  accesor
            MemberMethod(parent=type_map, name="get", static=True, type=typeinst(type=type_map, ptr=True),
                         read_only=True,
                         content="static serial::type_map instance{};\n"
                                 "return &instance;")
            # add a destructor
            Destructor(parent=type_map, virtual=True, read_only=True)

            # ------------------------------------------------
            #
            #           type_instance class construction
            #
            #--------------------------------------------------

            # add inheritance from type_record
            Inheritance(parent=type_instance, ancestor=type_record, virtual=False, access='public', read_only=True)

            # add constructor
            Constructor(parent=type_instance, access='public', read_only=True, autoargs=False, autoinit=False,
                        init=": type_record(typeid(T).name())",note="Constructs and registers a type.")

            # add an overload for the creation method
            # add a type_record virtual method for creating instances
            MemberMethod(
                parent=type_instance, name="create", virtual=True,
                constmethod=True, type=typeinst(type=runtime, ptr=True), read_only=True,
                content="return dynamic_cast<serial::runtime* >(new T{this});\n",
                note="This method is used for creating a new instances of a runtime class, on the fly.\n"
                     "In order to avoid interferences with standard c++, these constructors use a type\n"
                     "record pointer intended only used as a resolution identification. These kind of\n"
                     "constructors will be used and maintained by beatle in an automated manner.")

            # add a destructor
            Destructor(parent=type_instance, virtual=True, read_only=True)

            # ------------------------------------------------
            #
            #           runtime class construction
            #
            # --------------------------------------------------

            # Add a virtual accessor for type_record from an instance of runtime object
            MemberMethod(parent=runtime, name="get_type", virtual=True, pure=True, read_only=True,
                         type=typeinst(type=type_record, ref=True, const=True), constmethod=True,
                         note="This method must be overriden by the runtime_type template applied to specific classes.")

            # Add methods for storing  and restoring a type
            store_type_method = MemberMethod(
                parent=runtime, name="store_type", constmethod=True, read_only=True, type=typeinst(type=void_type),
                content="std::string name = get_type().get_name();\n"
                        "size_t sz = name.size();\n"
                        "os.write(reinterpret_cast < const char * > (& sz), sizeof(size_t));\n"
                        "os.write(name.c_str(), sz);\n",
                note="This method stores a runtime type in a recoverable way.")
            Argument(parent=store_type_method, name="os", read_only=True, type=typeinst(type=ostream_type, ref=True))

            restore_type_method = MemberMethod(
                parent=runtime, name="restore_type", static=True, read_only=True,
                type=typeinst(type=runtime, ptr=True),
                content="size_t sz;\n"
                        "is.read(reinterpret_cast<char*>(&sz), sizeof(size_t));\n"
                        "char *buffer = new char[sz+1];\n"
                        "is.read(buffer, sz);\n"
                        "buffer[sz]=0;\n"
                        "std::string name(buffer);\n"
                        "delete [] buffer;\n"
                        "type_record *p_type_record = type_map::get()->find_type(name);\n"
                        "if ( p_type_record == nullptr )\n"
                        "{\n"
                        "    return nullptr;\n"
                        "}\n"
                        "return p_type_record->create();",
                note="This method restores a runtime instance from stream. It returns the pointer\n"
                     "to the new instance or nullptr in failed case.")
            Argument(parent=restore_type_method, name="is", read_only=True, type=typeinst(type=istream_type, ref=True))



            # Add virtual methods for load/save
            save_method = MemberMethod(parent=runtime, name="save", virtual=True, pure=True, read_only=True,
                                     type=typeinst(type=void_type), constmethod=True)
            Argument(parent=save_method,name="os", type=typeinst(type=oxstream, ref=True), read_only=True)

            load_method = MemberMethod(parent=runtime, name="load", virtual=True, pure=True, read_only=True,
                                     type=typeinst(type=void_type))
            Argument(parent=load_method,name="is", type=typeinst(type=ixstream, ref=True), read_only=True)

            #Add dtor
            Destructor(parent=runtime, virtual=True, read_only=True)

            # ------------------------------------------------
            #
            #           runtime_instance construction
            #
            # --------------------------------------------------

            # add inheritance from runtime
            Inheritance(parent=runtime_instance, ancestor=runtime, virtual=True, access='public',
                        read_only=True)

            # add the type registration with the form of static member
            class_info = MemberData(parent=runtime_instance, name="type", access='private', static=True,
                                    type=typeinst(type=type_instance, type_args='T'))

            # add ctor
            Constructor(parent=runtime_instance, access='protected', read_only=True)

            # add access to the type
            MemberMethod(parent=runtime_instance, name="get_type", virtual=True, read_only=True,
                         type=typeinst(type=type_record,ref=True, const=True), constmethod=True,
                         content="return _type;", note="make accessible the custom type to runtime base class.")
            # add dtor
            Destructor(parent=runtime_instance, virtual=True, read_only=True)

            # ------------------------------------------------
            #
            #           oxstream class construction
            #
            # --------------------------------------------------

            # add a reference ostream member
            MemberData(parent=oxstream, name="ostream", access='private', read_only=True,
                       type=typeinst(type=ostream_type, ref=True))

            # add a map for storing serialization elements
            MemberData(parent=oxstream, name="instance_map", access='private', read_only=True,
                       type=typeinst(type=map_type, type_args='const runtime*, size_t'),
                       note="This member stores the objects already serialized.\n"
                            "The process of serialization is responsible for calling\n"
                            "reset() before starting the whole process.")

            # add constructor
            oxstream_constructor = Constructor(
                parent=oxstream, access='public', read_only=True, autoinit=False, autoargs=False,
                init=": _ostream(os)\n"
                     ", _instance_map{}",content='',
                note="This method constructs a output stream serialization that support nested\n"
                     "serialization processes.")
            Argument(
                parent=oxstream_constructor, name='os', type=typeinst(type=ostream_type, ref=True), read_only=True)

            MemberMethod(parent=oxstream, access='public', name='reset',read_only=True,
                         type=typeinst(type=void_type),
                         content='_instance_map.clear();',
                         note='Clean the buffer for serialized elements.')

            save_method = MemberMethod(
                parent=oxstream, access='public', name='save', read_only=True, type=typeinst(type=void_type),
                template='class T', template_types=['T'],
                content="/** Due to multiple inheritance runtime cast is non-unique \n"
                        "    if we don't use virtual inheritance. This is really ugly because \n"
                        "    of C++ ctor initialization trailing.\n"
                        "***/\n"
                        "const runtime *runtime_ptr = dynamic_cast<const runtime*>(object_ptr);"
                        "//first verify if the object was already serialized\n"
                        "auto position = _instance_map.find(runtime_ptr);\n"
                        "if(  position != end(_instance_map) )\n"
                        "{\n"
                        "   //only is required to write the reference, because the object was already serialized:\n"
                        "   write(position->second);\n"
                        "   return;\n"
                        "}\n"
                        "//get the address of the object as unique identifier.\n"
                        "size_t value = reinterpret_cast<size_t>(runtime_ptr);\n"
                        "write(value);\n"
                        "//add to map sooner preventing nested calls\n"
                        "_instance_map[runtime_ptr] = value;\n"
                        "runtime_ptr->store_type(_ostream);\n"
                        "runtime_ptr->save(*this);",
                note="This method does the complete storage of a runtime object instance\n"
                     "including his type.")
            Argument(parent=save_method, name='object_ptr', type=typeinst(type_alias='T', const=True, ptr=True),
                     read_only=True)

            # raw data write
            write_method = MemberMethod(
                parent=oxstream, access='public', name='write', read_only=True, type=typeinst(type=void_type),
                template='typename T', template_types=['T',],
                content='static_assert(std::is_union<T>::value == false &&\n'
                        ' std::is_class<T>::value == false && std::is_function<T>::value == false);\n'
                        '_ostream.write(reinterpret_cast<const char*>(&value), sizeof(T));',
                note='Do a raw serialization of data as buffer. Not intended to be used with pointers.'
            )
            Argument(parent=write_method, name='value', type=typeinst(type_alias='T',const=True, ref=True),
                     read_only=True)

            # std::list write
            write_list_method = MemberMethod(
                parent=oxstream, access='public', name='write', read_only=True, type=typeinst(type=void_type),
                template='class T, class Allocator',
                template_types=['T', 'Allocator'],
                content="size_t sz{value.size()};\n"
                "write(sz);\n"
                "for( const auto& element : value)\n" \
                "{\n"
                "    if constexpr (std::is_base_of_v<T, serial::runtime> == true)\n"
                "    {\n"
                "        save(&element);\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<T, serial::runtime*> == true)\n"
                "    {\n"
                "        save(element);\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        write(element);\n"
                "    }\n"
                "}\n",
                note="Do serialization of list type as basic type."
            )
            Argument(
                parent=write_list_method, name='value',
                type=typeinst(type_alias='std::list<T, Allocator>', const=True, ref=True),
                read_only=True)

            # std::vector write
            write_vector_method = MemberMethod(
                parent=oxstream, access='public', name='write', read_only=True, type=typeinst(type=void_type),
                template='class T, class Allocator',
                template_types=['T', 'Allocator'],
                content="size_t sz{value.size()};\n"
                "write(sz);\n"
                "for( const auto& element : value)\n" \
                "{\n"
                "    if constexpr (std::is_base_of_v<T, serial::runtime> == true)\n"
                "    {\n"
                "        save(&element);\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<T, serial::runtime*> == true)\n"
                "    {\n"
                "        save(element);\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        write(element);\n"
                "    }\n"
                "}\n",
                note="Do serialization of vector type as basic type."
            )
            Argument(
                parent=write_vector_method, name='value',
                type=typeinst(
                    type_alias='std::vector<T, Allocator>', const=True, ref=True),
                read_only=True)

            # std::map write
            write_map_method = MemberMethod(
                parent=oxstream, access='public', name='write', read_only=True, type=typeinst(type=void_type),
                template='class Key, class T, class Compare, class Allocator',
                template_types=['Key', 'T', 'Compare', 'Allocator'],
                content="size_t sz{value.size()};\n"
                "write(sz);\n"
                "for( const auto& [key, data] : value)\n" \
                "{\n"
                "    if constexpr (std::is_base_of_v<Key, serial::runtime> == true)\n"
                "    {\n"
                "        save(&key);\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<Key, serial::runtime*> == true)\n"
                "    {\n"
                "        save(key);\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        write(key);\n"
                "    }\n"
                "    if constexpr (std::is_base_of_v<T, serial::runtime> == true)\n"
                "    {\n"
                "        save(&data);\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<T, serial::runtime*> == true)\n"
                "    {\n"
                "        save(data);\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        write(data);\n"
                "    }\n"
                "}\n",
                note="Do serialization of map type as basic type."
            )
            Argument(
                parent=write_map_method, name='value',
                type=typeinst(type_alias='std::map<Key, T, Compare, Allocator>',
                              const=True, ref=True), read_only=True)

            # add destructor
            Destructor(parent=oxstream, access='public', read_only=True)

            # Add support for serialization of some interesting type
            oxstream.user_code_h4 = "//specific user override for serializing some types\n"\
                                    "template<> void serial::oxstream::write(const std::string& value);\n" \

            oxstream.user_code_s3 = "/** serialize a std::string as if integral type **/\n" \
                                    "template<> void serial::oxstream::write(const std::string& value)\n" \
                                    "{\n" \
                                    "    size_t sz = value.length();\n" \
                                    "    write(sz);\n" \
                                    "    _ostream.write(value.data(), sz);\n" \
                                    "}"

            # ------------------------------------------------
            #
            #           ixstream class construction
            #
            # --------------------------------------------------

            # add a reference ostream member
            MemberData(parent=ixstream, name="istream", access='private', read_only=True,
                       type=typeinst(type=istream_type, ref=True))

            # add a map for storing serialization elements
            MemberData(parent=ixstream, name="instance_map", access='private', read_only=True,
                       type=typeinst(type=map_type, type_args='size_t, runtime*'),
                       note="This member stores the objects already serialized.\n"
                            "The process of serialization is responsible for calling\n"
                            "reset() before starting the whole process.")

            # add constructor
            ixstream_constructor = Constructor(
                parent=ixstream, access='public', read_only=True, autoinit=False, autoargs=False,
                init=": _istream(is)\n"
                     ", _instance_map{}",content='',
                note="This method constructs a input stream serialization that support nested\n"
                     "serialization processes.")
            Argument(
                parent=ixstream_constructor, name='is', type=typeinst(type=istream_type, ref=True), read_only=True)

            MemberMethod(
                parent=ixstream, access='public', name='load', read_only=True, type=typeinst(type=runtime, ptr=True),
                content="//read the object identifier\n"
                        "size_t index;\n"
                        "read(index);\n"
                        "//search the object in the already serialized map\n"
                        "auto position = _instance_map.find(index);\n"
                        "if( position != end(_instance_map) )\n"
                        "{\n"
                        "   //no need to do anythng other that return previous value\n"
                        "   return position->second;\n"
                        "}\n"
                        "runtime *object_ptr = runtime::restore_type(_istream);\n"
                        "assert( object_ptr != nullptr );\n"
                        "_instance_map[index] = object_ptr;\n"
                        "object_ptr->load(*this);\n"
                        "return object_ptr;",
                note="This method does the complete restore of a runtime object instance.")

            # raw data read
            read_method = MemberMethod(
                parent=ixstream, access='public', name='read', read_only=True, type=typeinst(type=void_type),
                template='typename T', template_types=['T'],
                content="_istream.read(reinterpret_cast<char*>(&value), sizeof(T));",
                note='Do a raw serialization of type as buffer. Not intended to be used with pointers.'
            )
            Argument(
                parent=read_method, name='value', type=typeinst(type_alias='T', ref=True), read_only=True)

            # list data read
            read_list_method = MemberMethod(
                parent=ixstream, access='public', name='read', read_only=True, type=typeinst(type=void_type),
                template='class T, class Allocator',
                template_types=['T', 'Allocator'],
                content="size_t sz;\n"
                "read(sz);\n"
                "T *t_ptr;\n"
                "while( (sz--) > 0)\n"
                "{\n"
                "    if constexpr (std::is_base_of_v<T, serial::runtime> == true)\n"
                "    {\n"
                "        t_ptr = dynamic_cast<T*>(load());\n"
                "        if( t_ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map value\");\n"
                "        }\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<T, serial::runtime*> == true)\n"
                "    {\n"
                "        static T ptr;\n"
                "        ptr = dynamic_cast<T>(load());\n"
                "        if( ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map value\");\n"
                "        }\n"
                "        t_ptr = &ptr;\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        static_assert(std::is_default_constructible_v<T> == true);\n"
                "        t_ptr = new T{};\n"
                "        read(*t_ptr);\n"
                "    }\n"
                "    value.push_back(std::forward<T>(*t_ptr));\n"
                "}\n",
                note="Do serialization of list type as basic type."
            )
            Argument(
                parent=read_list_method, name='value',
                type=typeinst(type_alias='std::list<T, Allocator>', ref=True),
                read_only=True)

            # vector data read
            read_vector_method = MemberMethod(
                parent=ixstream, access='public', name='read', read_only=True, type=typeinst(type=void_type),
                template='class T, class Allocator',
                template_types=['T', 'Allocator'],
                content="size_t sz;\n"
                "read(sz);\n"
                "T *t_ptr;\n"
                "while( (sz--) > 0)\n"
                "{\n"
                "    if constexpr (std::is_base_of_v<T, serial::runtime> == true)\n"
                "    {\n"
                "        t_ptr = dynamic_cast<T*>(load());\n"
                "        if( t_ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map value\");\n"
                "        }\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<T, serial::runtime*> == true)\n"
                "    {\n"
                "        static T ptr;\n"
                "        ptr = dynamic_cast<T>(load());\n"
                "        if( ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map value\");\n"
                "        }\n"
                "        t_ptr = &ptr;\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        static_assert(std::is_default_constructible_v<T> == true);\n"
                "        t_ptr = new T{};\n"
                "        read(*t_ptr);\n"
                "    }\n"
                "    value.push_back(std::forward<T>(*t_ptr));\n"
                "}\n",
                note="Do serialization of list type as basic type."
            )
            Argument(
                parent=read_vector_method, name='value',
                type=typeinst(type_alias='std::vector<T, Allocator>', ref=True),
                read_only=True)

            # map data read
            read_map_method = MemberMethod(
                parent=ixstream, access='public', name='read', read_only=True, type=typeinst(type=void_type),
                template='class Key, class T, class Compare, class Allocator',
                template_types=['Key', 'T', 'Compare', 'Allocator'],
                content="size_t sz;\n"
                "read(sz);\n"
                "Key *key_ptr;\n"
                "T *t_ptr;\n"
                "while( (sz--) > 0)\n"
                "{\n"
                "    if constexpr (std::is_base_of_v<Key, serial::runtime> == true)\n"
                "    {\n"
                "        key_ptr = dynamic_cast<Key*>(load());\n"
                "        if( key_ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map key\");\n"
                "        }\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<Key, serial::runtime*> == true)\n"
                "    {\n"
                "        static Key ptr;\n"
                "        ptr = dynamic_cast<Key>(load());\n"
                "        if( ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map value\");\n"
                "        }\n"
                "        key_ptr = &ptr;\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        static_assert(std::is_default_constructible_v<Key> == true);\n"
                "        key_ptr = new Key{};\n"
                "        read(*key_ptr);\n"
                "    }\n"
                "    if constexpr (std::is_base_of_v<T, serial::runtime> == true)\n"
                "    {\n"
                "        t_ptr = dynamic_cast<T*>(load());\n"
                "        if( t_ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map value\");\n"
                "        }\n"
                "    }\n"
                "    else if constexpr (std::is_convertible_v<T, serial::runtime*> == true)\n"
                "    {\n"
                "        static T ptr;\n"
                "        ptr = dynamic_cast<T>(load());\n"
                "        if( ptr == nullptr )\n"
                "        {\n"
                "            throw std::runtime_error(\"unexpected data reading map value\");\n"
                "        }\n"
                "        t_ptr = &ptr;\n"
                "    }\n"
                "    else\n"
                "    {\n"
                "        static_assert(std::is_default_constructible_v<T> == true);\n"
                "        t_ptr = new T{};\n"
                "        read(*t_ptr);\n"
                "    }\n"
                "    value.emplace(std::piecewise_construct, std::forward_as_tuple(*key_ptr),\n"
                "        std::forward_as_tuple(*t_ptr));\n"
                "}\n",
                note="Do serialization of map type as basic type."
            )
            Argument(
                parent=read_map_method, name='value', read_only=True,
                type=typeinst(type_alias='std::map<Key, T, Compare, Allocator>', ref=True))

            # add destructor
            Destructor(parent=ixstream, access='public', read_only=True)

            # Add support for serialization of some interesting type
            ixstream.user_code_h4 = "//specific user override for serializing some types\n" \
                                    "template<> void serial::ixstream::read(std::string& value);\n"

            ixstream.user_code_s3 = "/** serialize a std::string as if integral type **/\n" \
                                    "template<> void serial::ixstream::read(std::string& value)\n" \
                                    "{\n" \
                                    "    size_t sz;\n" \
                                    "    read(sz);\n" \
                                    "    char *buffer = new char[sz];\n" \
                                    "    _istream.read(buffer, sz);\n" \
                                    "    value.assign(buffer, sz);\n" \
                                    "    delete [] buffer;\n" \
                                    "}"

        def create_undo_redo_support():
            """
            Create undo/redo support for the project
            ----------------------------------------
            namespace trans
            The undo/redo support starts with a class Stack which represents the transactional stack.
            This class stores global operations

                operation : represents a operation. There are some types of operations
                        operation_new
                        operation_delete
                        operation_subdelete
                        operation_change

            A ordered set of operations are stored into a transaction.
            transactions are stored in one of two stacks: the undo stack and the redo stack.
            Transactions stored in the undo stack can be undoed and then move to the redo stack
            where they can be redoed and hence move back to the undo stack.

            Transactions can have a name.

            Transactional objects are serializable, so serial::runtime. Extra requirements must then
            be added to serial::runtime and we must create a trans::instance and trans::runtime_instance
            mimic the others but adding extra required functionality

            Future:
            For programming reasons, the stack class will have either global behaviour (application-level)
            or local behaviour (thread-level).

            Using thread-level stacks operations can be commited to global stack based on global stack coherence.
            Commit to global stack make other living stacks incoherent and not able to be commited to global stack.
            Threads can be subscribed to a notification on incoherent state while this state is also determined
            while attempting to commit to global stack using a bifurcation check.


            """
            # check if there is already undo/redo support
            if len([x for x in self._project[Namespace] if hasattr(x, 'transaction_namespace')]) > 0:
                return

            namespace = Namespace(parent=self._project, name="trans", read_only=True)
            setattr(namespace, 'transaction_namespace', True)

            runtime = cached_type(self._project,'serial::runtime')
            type_instance = cached_type(self._project,'serial::type_instance')
            type_record = cached_type(self._project,'serial::type_record')
            bool_type = cached_type(self._project,'bool')
            void_type = cached_type(self._project, 'void')
            size_type = cached_type(self._project,'size_t')

            transactional = Class(parent=namespace, name="transactional", read_only=True,
                                   note="""Represents the common interface for transactional classes. 
This is a serial::runtime specialization""")

            transactional_instance = Class(
                parent=namespace, name="transactional_instance", template="typename T",
                template_types=['T'], template_arguments=['T'], read_only=True,
                note="""This template is a direct base class for any
 transactional class T""")

            stack_class = Class(
                parent=namespace, name="stack", read_only=True,
                note="This class the undo/redo stack. ")
            transaction = Class(
                parent=namespace, name="transaction", read_only=True,
                note="This class is the base for set of operations. ")
            operation = Class(
                parent=namespace, name="operation", read_only=True,
                note="This class is the base for any transactional operation. ")
            operation_new = Class(
                parent=namespace, name="operation_new", read_only=True,
                note="This class is the base for any new operation. ")
            operation_delete = Class(
                parent=namespace, name="operation_delete", read_only=True,
                note="This class is the base for any delete operation. ")
            operation_change = Class(
                parent=namespace, name="operation_change", read_only=True,
                note="This class is the base for any change operation.")

            # Inheritances:
            # All the operations must inherit operation

            Inheritance(parent=transactional, ancestor=runtime, virtual=False, access='public', read_only=True)
            Inheritance(parent=transactional_instance, ancestor=transactional,
                        virtual=True, access='public', read_only=True)
            Inheritance(parent=operation_new, ancestor=operation, virtual=False, access='public', read_only=True)
            Inheritance(parent=operation_delete, ancestor=operation, virtual=False, access='public', read_only=True)
            Inheritance(parent=operation_change, ancestor=operation, virtual=False, access='public', read_only=True)

            # Relations
            # All the operations must belong to a transaction
            # add a aggregated avl-tree name-bassed relation from dictionary to records
            Relation(
                access='protected',
                aggregate=True,
                from_class=transaction, to_class=operation,
                fromName='transaction', toName='operation',
                implementation='standard',
                read_only=True
            )
            # Also the transactions must be stored in one or anoter queue
            Relation(
                access='protected',
                aggregate=False,
                from_class=stack_class, to_class=transaction,
                fromName='undo_stack', toName='undo_transaction',
                implementation='standard',
                read_only=True
            )
            Relation(
                access='protected',
                aggregate=False,
                from_class=stack_class, to_class=transaction,
                fromName='redo_stack', toName='redo_transaction',
                implementation='standard',
                read_only=True
            )
            # ------------------------------------------------
            #
            #           transactional construction
            #
            # --------------------------------------------------
            Constructor(parent=transactional, access='protected', read_only=True, inline=True,
                        autoinit=True)

            # event notification
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='on_undo_new', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='on_redo_new', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='on_undo_delete', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='on_redo_delete', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='on_undo_change', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='on_redo_change', virtual=True, type=typeinst(type=void_type))
            # actions
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='save_relations', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='restore_relations', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='save_contents', virtual=True, type=typeinst(type=void_type))
            MemberMethod(parent=transactional, access='public', read_only=True,
                         name='load_contents', virtual=True, type=typeinst(type=void_type))

            Destructor(parent=transactional,  read_only=True, virtual=True)

            # ------------------------------------------------
            #
            #           transactional_instance construction
            #
            # --------------------------------------------------


            # add the type registration with the form of static member
            class_info = MemberData(parent=transactional_instance, name="type", access='private', static=True,
                                    type=typeinst(type=type_instance, type_args='T'))

            # add ctor
            Constructor(parent=transactional_instance, access='protected', read_only=True, autoinit=True)

            # add access to the type
            MemberMethod(parent=transactional_instance, name="get_type", virtual=True, read_only=True,
                         type=typeinst(type=type_record, ref=True, const=True), constmethod=True,
                         content="return _type;", note="make accessible the custom type to runtime base class.")
            # add dtor
            Destructor(parent=transactional_instance, virtual=True, read_only=True)
            # ----------------------------------------------------
            #                   STACK CLASS
            # ----------------------------------------------------
            # the basic operations of the stack are undo, redo and rollback.
            # the intended use of rollback is to handle runtime exceptions.
            # Interesting operations are has_undo, has_redo and clean_redo.
            # One important member is the transaction size
            # Other important member is the static stack member that represents global stack
            MemberData(
                access='private',
                type=typeinst(type=size_type),
                name='stack_size',
                default='100',
                parent=stack_class,
                read_only=True
            )
            MemberData(
                access='private',
                type=typeinst(type=stack_class),
                name='global_stack',
                static=True,
                parent=stack_class,
                read_only=True
            )
            # add static accessor
            MemberMethod(
                parent=stack_class, access='public', read_only=True, staticmethod=True,
                type=typeinst(type=stack_class, ptr=True),
                name="global",
                note="Return the instance of the global stack.",
                content="return &_global_stack;\n"
            )
            # add ctor
            Constructor(parent=stack_class, access='protected', read_only=True, autoinit=True)

            MemberMethod(
                parent=stack_class, access='public', name='undo', read_only=True, type=typeinst(type=bool_type),
                content="if( get_undo_transaction_count() == 0 )\n"
                        "{\n"
                        "   return false;\n"
                        "}\n"
                        "return get_last_undo_transaction()->undo();\n",
                note="This method undoes the last transaction.")

            MemberMethod(
                parent=stack_class, access='public', name='redo', read_only=True, type=typeinst(type=bool_type),
                content="if( get_redo_transaction_count() == 0 )\n"
                        "{\n"
                        "   return false;\n"
                        "}\n"
                        "return get_last_redo_transaction()->redo();\n",
                note="This method redoes the last transaction.")

            MemberMethod(
                parent=stack_class, access='public', name='commit', read_only=True, type=typeinst(type=bool_type),
                content="trans::transaction* trans_ptr = get_last_undo_transaction();\n"
                        "if( trans_ptr == nullptr )\n"
                        "{\n"
                        "   return false;\n"
                        "}\n"
                        "if( ! trans_ptr->commit() )\n"
                        "{\n"
                        "   return false;\n"
                        "}\n"
                        "while( get_undo_transaction_count() > _stack_size )\n"
                        "{\n"
                        "   delete get_first_undo_transaction();\n"
                        "}\n"
                        "clean_redo();\n"
                        "return true;\n",
                note="This method commits the current transaction.")

            MemberMethod(
                parent=stack_class, access='public', name='rollback', read_only=True, type=typeinst(type=bool_type),
                content="trans::transaction* trans_ptr = get_last_undo_transaction();\n"
                        "if( trans_ptr == nullptr )\n"
                        "{\n"
                        "   return false;\n"
                        "}\n"
                        "if( ! trans_ptr->rollback() )\n"
                        "{\n"
                        "   return false;\n"
                        "}\n"
                        "delete trans_ptr;\n"
                        "return true;\n",
                note="This method rollback the open transaction.")

            MemberMethod(
                parent=stack_class, access='public', name='has_undo', read_only=True, inline=True,
                type=typeinst(type=bool_type), constmethod=True,
                content="return ( get_undo_transaction_count() != 0 ) ? true : false ;\n",
                note="This method return a boolean about if there are operations for undo.")

            MemberMethod(
                parent=stack_class, access='public', name='has_redo', read_only=True, inline=True,
                type=typeinst(type=bool_type), constmethod=True,
                content="return ( get_redo_transaction_count() != 0 ) ? true : false ;\n",
                note="This method return a boolean about if there are operations for redo.")

            MemberMethod(
                parent=stack_class, access='public', name='get_open_transaction', read_only=True,
                type=typeinst(type=transaction, ptr=True),
                content="auto *ptr = get_last_undo_transaction();\n"
                        "if( ptr != nullptr )\n"
                        "{\n"
                        "   if( ptr->is_open() )\n"
                        "   {\n"
                        "       return ptr;\n"
                        "   }\n"
                        "}\n"
                        "return nullptr;\n",
                note="This method return a boolean about if there are operations for redo.")

            MemberMethod(
                parent=stack_class, access='public', name='clean_redo', read_only=True, inline=True,
                type=typeinst(type=void_type),
                content="delete_all_redo_transaction();\n",
                note="This method cleans the redo stack.")

            MemberMethod(
                parent=stack_class, access='public', name='clean_undo', read_only=True, inline=True,
                type=typeinst(type=void_type),
                content="delete_all_undo_transaction();\n",
                note="This method cleans the undo stack.")

            # add dtor
            Destructor(parent=stack_class, virtual=True, read_only=True,
                       content="clean_undo();\n"
                               "clean_redo();\n"
                               "__exit__();\n")

            # ----------------------------------------------------
            #                   TRANSACTION CLASS
            # ----------------------------------------------------
            # some required includes
            transaction.user_code_h1 = "#include <string>\n"
            string_type = cached_type(self._project, 'std::string')
            # This class is the responsible for handling the transactions
            # The basic attributes for a transaction are.
            # The stack class is friend of this one
            Friendship(
                target=stack_class,
                parent=transaction,
                read_only=True,
                visibleInTree=False
            )
            MemberData(
                access='private',
                type=typeinst(type=bool_type),
                name='open',
                default='true',
                parent=transaction,
                read_only=True,
                note="This flags indicates that the transaction is not complete."
            )
            MemberData(
                access='private',
                type=typeinst(type=string_type),
                name='name',
                parent=transaction,
                read_only=True,
                note="This stores the transaction name."
            )
            # The constructor
            ctor = Constructor(
                parent=transaction, access='protected', read_only=True,
                autoinit=True, autoargs=False,
                content="stack_ptr->add_undo_transaction_last(this);\n")
            Argument(parent=ctor, name='stack_ptr', type=typeinst(type=stack_class, ptr=True))
            Argument(parent=ctor, name='name', default='""', type=typeinst(type=string_type, const=True, ref=True))
            MemberMethod(
                parent=transaction, access='public', name='is_open', read_only=True, inline=True,
                type=typeinst(type=bool_type), constmethod=True,
                content="return _open;\n",
                note="Return true iif the transaction construction is unfinished.")
            MemberMethod(
                parent=transaction, access='protected', name='undo', read_only=True,
                type=typeinst(type=bool_type),
                content="if( _open )\n"
                        "   return false;\n"
                        "operation_iterator iter_operation = operation_iterator(this);\n"
                        "while(--iter_operation)\n"
                        "{\n"
                        "   if(!iter_operation->undo())\n"
                        "   {\n"
                        "       while(++iter_operation)\n"
                        "       {\n"
                        "           iter_operation->redo();\n"
                        "       }\n"
                        "       return false;\n"
                        "   }\n"
                        "}\n"
                        "get_undo_stack()->add_redo_transaction_last(this);\n"
                        "get_redo_stack()->remove_undo_transaction(this);\n"
                        "return true;\n",
                note="This method undoes the operations inside the transaction.")

            MemberMethod(
                parent=transaction, access='protected', name='redo', read_only=True,
                type=typeinst(type=bool_type),
                content="if( _open )\n"
                        "   return false;\n"
                        "operation_iterator iter_operation = operation_iterator(this);\n"
                        "while(++iter_operation)\n"
                        "{\n"
                        "   if(!iter_operation->redo())\n"
                        "   {\n"
                        "       while(--iter_operation)\n"
                        "       {\n"
                        "           iter_operation->undo();\n"
                        "       }\n"
                        "       return false;\n"
                        "   }\n"
                        "}\n"
                        "get_redo_stack()->add_undo_transaction_last(this);\n"
                        "get_undo_stack()->remove_redo_transaction(this);\n"
                        "return true;\n",
                note="This method redoes the operations inside the transaction.")

            MemberMethod(
                parent=transaction, access='protected', name='rollback', read_only=True,
                type=typeinst(type=bool_type),
                content="if( ! _open )\n"
                        "   return false;\n"
                        "operation_iterator iter_operation = operation_iterator(this);\n"
                        "while(--iter_operation)\n"
                        "{\n"
                        "   if(!iter_operation->undo())\n"
                        "   {\n"
                        "       while(++iter_operation)\n"
                        "       {\n"
                        "           iter_operation->redo();\n"
                        "       }\n"
                        "       return false;\n"
                        "   }\n"
                        "}\n"
                        "return true;\n",
                note="This method rollbacks the current unclosed transaction.")

            MemberMethod(
                parent=transaction, access='protected', name='commit', read_only=True, inline=True,
                type=typeinst(type=bool_type),
                content="if( ! _open )\n"
                        "   return false;\n"
                        "_open = false;\n"
                        "return true;\n",
                note="This method closes the current transaction.")

            Destructor(parent=transaction, virtual=True, read_only=True)

            # ----------------------------------------------------
            #                   OPERATION CLASS
            # ----------------------------------------------------
            # The operation class is the base for the operations

            MemberData(parent=operation, access='private', type=typeinst(type=transactional, ptr=True),
                       const=True, name='object')

            Constructor(parent=operation, access='public', read_only=True,
                        explicit=True, preferred=True, autoinit=True, autoargs=True)

            MemberMethod(
                parent=operation, access='public', name='get_object', read_only=True, inline=True,
                virtual=True, pure=False, implement=True, type=typeinst(type=transactional, ptr=True),
                content="return _object;\n"
            )

            MemberMethod(
                parent=operation, access='public', name='undo', read_only=True, inline=True, virtual=True,
                pure=True, implement=False, type=typeinst(type=bool_type),
                content="return true;\n"
            )
            MemberMethod(
                parent=operation, access='public', name='redo', read_only=True, inline=True, virtual=True,
                pure=True, implement=False, type=typeinst(type=bool_type),
                content="return true;\n")
            Destructor(parent=operation, virtual=True, read_only=True)

            # ----------------------------------------------------
            #                   OPERATION NEW
            # ----------------------------------------------------
            # The operation new is triggered by the object constructor checking a mark in the class
            # done by class new operator. This object is stored on a trans_runtime pointer

            Constructor(parent=operation_new, access='public', read_only=True, autoinit=True, autoargs=True)

            # Undo operation do a:
            #   save relations from the object,
            #   remove relations from the object,
            MemberMethod(
                parent=operation_new, access='public', read_only=True, type=typeinst(type=bool_type),
                name='undo', virtual=True,
                note='undo the new operation',
                content="get_object()->on_undo_new();\n"
                        "get_object()->save_relations();\n"
                        "return true;\n"
            )
            MemberMethod(
                parent=operation_new, access='public', read_only=True, type=typeinst(type=bool_type),
                name='redo', virtual=True,
                note='redo the new operation',
                content="get_object()->restore_relations();\n"
                        "get_object()->on_redo_new();\n"
                        "return true;\n"
            )
            Destructor(parent=operation_new, virtual=True, read_only=True)

            # ----------------------------------------------------
            #                   OPERATION DELETE
            # ----------------------------------------------------
            # The operation new is triggered by the object constructor checking a mark in the class
            # done by class new operator. This object is stored on a trans_runtime pointer
            Constructor(parent=operation_delete, access='public', read_only=True, autoinit=True, autoargs=True)
            MemberMethod(
                parent=operation_delete, access='public', read_only=True, type=typeinst(type=bool_type),
                name='undo', virtual=True,
                note='undo the delete operation',
                content="get_object()->restore_relations();\n"
                        "get_object()->on_undo_delete();\n"
                        "return true;\n"
            )
            MemberMethod(
                parent=operation_delete, access='public', read_only=True, type=typeinst(type=bool_type),
                name='redo', virtual=True,
                note='redo the delete operation',
                content="get_object()->on_redo_delete();\n"
                        "get_object()->save_relations();\n"
                        "return true;\n"
            )
            Destructor(parent=operation_delete, virtual=True, read_only=True)

            # ----------------------------------------------------
            #                   OPERATION CHANGE
            # ----------------------------------------------------
            # The operation new is triggered by the object constructor checking a mark in the class
            # done by class new operator. This object is stored on a trans_runtime pointer
            Constructor(parent=operation_change, access='public', read_only=True, autoinit=True, autoargs=True)
            MemberMethod(
                parent=operation_change, access='public', read_only=True, type=typeinst(type=bool_type),
                name='undo', virtual=True,
                note='undo the delete operation',
                content="get_object()->load_contents();\n"
                        "get_object()->on_undo_change();\n"
                        "return true;\n"
            )
            MemberMethod(
                parent=operation_change, access='public', read_only=True, type=typeinst(type=bool_type),
                name='redo', virtual=True,
                note='redo the delete operation',
                content="get_object()->on_redo_change();\n"
                        "get_object()->save_contents();\n"
                        "return true;\n"
            )
            Destructor(parent=operation_change, virtual=True, read_only=True)

        def remove_serialization_support():
            """Remove all classes undo/redo support"""
            # Remove serialization must first check and warn about the existence of already serial classes
            serial_classes = self._project(Class, filter=lambda x: x.is_serial)
            if len(serial_classes) > 0:
                answer = wx.MessageBox(
                    "Serialization removal will remove serialization on {number} classes.".format(
                        number=len(serial_classes)
                    ), "Warning", wx.YES_NO | wx.CENTER | wx.ICON_HAND, context.get_frame())
                if answer is wx.NO:
                    return
            for x in self._project(Class, filter=lambda x: x.is_serial):
                x.set_serial(False)
                x.update_class_serialization()
            namespace_list = self._project(
                Namespace, filter=lambda x: getattr(x,'serialization_namespace',False), cut=True)
            for x in namespace_list:
                x.delete()

        def remove_undo_redo_support():
            """Remove all classes undo/redo support"""
            namespace_list = self._project(
                Namespace, filter=lambda x: getattr(x, 'transaction_namespace', False), cut=True)
            for x in namespace_list:
                x.delete()

        # update support
        if self.support_serialization:
            create_serialization_support()
        else:
            remove_serialization_support()

        if self._transactional:
            create_undo_redo_support()
        else:
            remove_undo_redo_support()

    @property
    def agnostic_project(self):
        """Return the agnostic project container"""
        return self._project

    @staticmethod
    def get_tab_bitmap(self):
        from beatle.app import resources as rc
        return rc.get_bitmap('cppproject')

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('cppproject')

    @property
    def type(self):
        return self._type

    @property
    def use_master_include(self):
        return self._useMaster

    @property
    def buildable(self):
        """The project may be built?"""
        return True

    @property
    def cpp_includes(self):
        return self._current_build_config.cpp_includes

    @property
    def cpp_include_paths(self):
        return self._current_build_config.cpp_include_paths

    @property
    def current_build_config(self):
        return self._current_build_config

    @property
    def prebuild_steps(self):
        return self._current_build_config._build_steps._before

    @property
    def postbuild_steps(self):
        return self._current_build_config._build_steps._after

    @property
    def c_compiler(self):
        return self._current_build_config._c_compiler._command

    @property
    def cpp_compiler(self):
        return self._current_build_config._cpp_compiler._command

    @property
    def cpp_flags(self):
        return self._current_build_config._cpp_compiler.FLAGS

    @property
    def link_libs(self):
        return self._current_build_config.link_libs

    @property
    def link_flags(self):
        return self._current_build_config._linker.FLAGS

    @property
    def extra_objects(self):
        return self._current_build_config._linker._extra_objects

    @property
    def cpp_archiver(self):
        return self._current_build_config._archiver._command

    @property
    def cpp_linker(self):
        return self._current_build_config._linker._command

    @property
    def build_type(self):
        return self._current_build_config.target.type

    @property
    def target_product(self):
        return self._current_build_config.target_product

    @property
    def full_path_target_product(self):
        return os.path.join(self.dir, self.target_product)

    # debug
    @property
    def debug_type(self):
        return self._debug_type

    @debug_type.setter
    def debug_type(self, value):
        self._debug_type = value

    @property
    def local_debug_target(self):
        if self._local_debug_target is None:
            return self.full_path_target_product
        else:
            return self._local_debug_target

    @local_debug_target.setter
    def local_debug_target(self, value):
        if value == self.full_path_target_product:
            self._local_debug_target = None
        else:
            self._local_debug_target = value

    @property
    def local_debug_source_dir(self):
        if self._local_debug_source_dir is None:
            return self.dir
        return self._local_debug_source_dir

    @local_debug_source_dir.setter
    def local_debug_source_dir(self, value):
        if value == self.dir:
            self._local_debug_source_dir = None
        else:
            self._local_debug_source_dir = value

    @property
    def debug_arguments(self):
        if self._debug_arguments is None:
            return ''
        return self._debug_arguments

    @debug_arguments.setter
    def debug_arguments(self, value):
        self._debug_arguments = value

    @property
    def remote_debug_target(self):
        return self._remote_debug_target or ''

    @remote_debug_target.setter
    def remote_debug_target(self, value):
        self._remote_debug_target = value

    @property
    def remote_debug_source_dir(self):
        return self._remote_debug_source_dir or ''

    @remote_debug_source_dir.setter
    def remote_debug_source_dir(self, value):
        self._remote_debug_source_dir = value

    @property
    def remote_debug_host(self):
        return self._remote_debug_host or ''

    @remote_debug_host.setter
    def remote_debug_host(self, value):
        self._remote_debug_host = value

    @property
    def remote_debug_port(self):
        return self._remote_debug_port or 22

    @remote_debug_port.setter
    def remote_debug_port(self, value):
        self._remote_debug_port = value

    @property
    def remote_debug_user(self):
        return self._remote_debug_user or ''

    @remote_debug_user.setter
    def remote_debug_user(self, value):
        self._remote_debug_user = value

    @property
    def remote_debug_pwrd(self):
        return self._remote_debug_pwrd or ''

    @remote_debug_pwrd.setter
    def remote_debug_pwrd(self, value):
        self._remote_debug_pwrd = value

    @property
    def dir(self):
        """return the project directory"""
        return self._dir

    @dir.setter
    def dir(self, value):
        if self._dir != value:
            self._dir = value
            self.register_output_dirs()

    @property
    def types(self):
        """This method gets the list of visible types"""
        pure_type = self._project(Type, filter= lambda x: not x.outer_class or getattr(x, '_access', 'public') == 'public', cut=True)
        class_type = self._project(Class, filter= lambda x:  x.outer_class is x or getattr(x, '_access', 'public') == 'public', cut=True)
        return pure_type+class_type

    @property
    def modules(self):
        """returns the list of all modules"""
        return self._project(*tuple(CCProject.module_classes))

    @property
    def sorted_classes(self):
        """Sort classes by dependency. Any class that inherits or holds
        explicit (i.e. non pointers or references) project classes data instances,
        could be declared **after** these classes. An unsortable collection
        of classes couldn't be compiled because that implies circular dependency"""
        clss = [x for x in self.level_classes if not x.is_external]
        result = []
        while len(clss) > 0:
            cls = None
            for candidate in clss:
                top_classes = set(x.outer_class for x in candidate.direct_bases)
                top_classes = set(x for x in top_classes if type(x) is Class and not x.is_external)
                dependency = next((x for x in top_classes if x in clss), None)
                if dependency is not None:
                    print('class {} postponed because inheritance requires class {}'.format(candidate.name,
                                                                                            dependency.name))
                    continue
                cont = False
                for member in candidate(MemberData, filter=lambda x:x.inner_class == candidate, cut=True):
                    ti = member.type_instance
                    # type aliases are a hack for custom code that injects unrelated types
                    # that kind of feature is out of the producer not beatle.
                    if ti.type_alias:
                        continue
                    mandatory_class = ti.base_type.outer_class
                    if mandatory_class in clss and mandatory_class is not candidate:
                        if not (ti.is_ptr or ti.is_ref or ti.is_rref or getattr(member, '_static', False)):
                            print('class {} postponed because member {} requires {}'.format(candidate.name,
                                                                                            member.name,
                                                                                            mandatory_class.name))
                            cont = True
                            break
                if cont:
                    continue
                cls = candidate
                break
            assert (cls is not None), "Circular dependency between classes!"
            result.append(cls)
            clss.remove(cls)
        return result

    @property
    def level_classes(self):
        """return the list of classes inside this level.
        Notes: This function is a little hack providing support for  plugin classes
        in order to ensure his forward declarations. No more support for that kind
        of classes is provided until now."""
        classes = self._project(Class, filter=lambda x: x.parent.outer_class is None)
        for special_class in self.class_overrides:
            classes.extend(self._project(special_class, filter=lambda x: x.parent.outer_class is None))
        return classes

    @property
    def master_include(self):
        return self._masterInclude

    @property
    def namespaces(self):
        """returns the list of all namespaces"""
        from ._Namespace import Namespace
        return self._project(Namespace)

    def create_folder(self, **kwargs):
        """Override global folders with special ones"""
        return Folder(**kwargs)

    @DelayedMethod()
    def export_code_files(self, force=False, logger=None):
        """Do export c++ files"""
        self.write_license()
        if not self.register_output_dirs(logger):
            return
        # create sorted main classes list
        try:
            sorted_classes = self.sorted_classes
        except AssertionError as e:
            wx.MessageBox('error {0}'.format(str(e)))
            return
        # create the master include file?
        try:
            if self._useMaster:
                if not self.write_master_header(sorted_classes, force, logger):
                    return
            if self._useMakefile:
                self.write_makefile(sorted_classes)
            # generate through main classes
            for obj in sorted_classes:
                if obj.is_external:
                    continue
                obj.update_sources(force)
                if logger is not None:
                    e = "Generate source for class " + obj._name
                    logger.AppendReportLine(e, wx.OK)
                    wx.YieldIfNeeded()
                obj.update_headers(force)
                if logger is not None:
                    e = "Generate include for class " + obj._name
                    logger.AppendReportLine(e, wx.OK)
                    wx.YieldIfNeeded()

            # remove temps
            #delattr(self, 'sorted_classes')
            # generate through modules
            for obj in [x for x in self._project.modules if not x.inner_library]:
                obj.update_sources(force)
                if logger is not None:
                    e = "Generate source for module " + obj._name
                    logger.AppendReportLine(e, wx.OK)
                obj.update_headers(force)
                if logger is not None:
                    e = "Generate include for module " + obj._name
                    logger.AppendReportLine(e, wx.OK)
            # refresh project files
            self._project.refresh_project_files()
        except Exception as e:
            print('Failed exporting c++ source files for project {0}: {1}'.format(
                self._project.name, str(e)))
            import traceback
            import sys
            traceback.print_exc(file=sys.stdout)
            print(type(e))     # the exception instance
            print(e.args)      # arguments stored in .args
            print(e)

    def register_output_dirs(self, logger=None):
        """Finds target generation directories and create it if missing."""
        if not os.path.exists(self._dir):
            os.makedirs(self._dir)
            if not os.path.exists(self._dir):
                e = "Project path missing: " + self._dir
                if logger is not None:
                    logger.AppendReportLine(e, wx.ICON_ERROR)
                wx.MessageBox(e, "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
                return False
        # for python projects, there are not other requiriments
        sources_dir = os.path.join(self._dir, self._srcDir)
        headers_dir = os.path.join(self._dir, self._includeDir)
        sources_dir = os.path.realpath(sources_dir)
        headers_dir = os.path.realpath(headers_dir)
        if not os.path.exists(sources_dir):
            os.makedirs(sources_dir)
            if not os.path.exists(sources_dir):
                e = "Failed creating sources directory " + sources_dir
                if logger is not None:
                    logger.AppendReportLine(e, wx.ICON_ERROR)
                wx.MessageBox(e, "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
                return False
        if not os.path.exists(headers_dir):
            os.makedirs(headers_dir)
            if not os.path.exists(headers_dir):
                e = "Failed creating headers directory " + headers_dir
                if logger is not None:
                    logger.AppendReportLine(e, wx.ICON_ERROR)
                wx.MessageBox(e, "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
                return False
        self.sources_dir = sources_dir
        self.headers_dir = headers_dir
        return True

    @DelayedMethod()
    def write_master_header(self, sorted_classes, force=False, logger=None):
        """Write the master include file"""
        # open the file
        fname = os.path.join(self.headers_dir, self._masterInclude)
        regenerate = False
        if force or not os.path.exists(fname) or self._project._lastHdrTime is None:
            regenerate = True
        elif os.path.getmtime(fname) < self._project._lastHdrTime:
            regenerate = True
        if not regenerate:
            return True
        with open(fname, mode='wt', encoding='ascii', errors='ignore') as f:
            pf = Writer.for_file(f)

            #write a timestamp and version information
            self._project.write_timestamp(pf)

            safe_name = self._masterInclude.upper().replace('.', '_').replace('-','_')

            #write include safeward
            pf.write_line('#if !defined({name}_INCLUDED)'.format(name=safe_name))
            pf.write_line('#define {name}_INCLUDED'.format(name=safe_name))
            pf.write_newline()

            #write standard includes
            pf.write_line('//standard includes')
            for include in self.cpp_includes:
                pf.write_line('#include <{include}>'.format(include=include))
            if len(self.cpp_includes) > 0:
                pf.write_newline()

            #write the custom definitions
            if len(self._contexts) > 0:
                for item in self._contexts:
                    if item._enable and len(item._define):
                        pf.write_line('/**')
                        pf.write_line('context {name}:'.format(name=item._name))
                        note = item._note
                        if type(note) is bytes:
                            note = note.decode('ascii', 'replace')
                        pf.write_line('{note}'.format(note=note))
                        pf.write_line('**/')
                        pf.write_newline()
                        pf.write_line('{0}'.format(item._define))
                        pf.write_newline()

            # write custom types
            for t in self._project(Type, filter=lambda x: type(x.parent) is TypesFolder):
                if t._read_only:
                    continue
                if len(t._definition.strip()) == 0:  # skip empty declarations
                    continue
                pf.write_line('//type definition for {0}'.format(t._name))
                pf.write_line('{0}'.format(t._definition))
            pf.write_newline()

            #write forward references
            self.write_forwards(pf, sorted_classes)

            #write prolog user definitions
            #self.WriteUserProlog()

            #write clases includes without inlines
            for cls in sorted_classes:
                if cls.is_external:
                    continue
                pf.write_line('#include "{name}.h"'.format(name=cls.base_filename))
            pf.write_newline()
            pf.write_line('#define INCLUDE_INLINES')

            #write class includes again for inlines
            for cls in sorted_classes:
                if cls.is_external:
                    continue
                pf.write_line('#include "{name}.h"'.format(name=cls.base_filename))

            #write modules includes
            pf.write_newline()
            for module in self._project.modules:
                if module.inner_library or not module._header:
                    continue
                pf.write_line('#include "{module_header}"'.format(module_header=module._header))

            #write end safeguard
            pf.write_newline()
            pf.write_line('#endif //{name}_INCLUDED'.format(name=safe_name))
            pf.write_newline()
        self._project._lastHdrTime = os.path.getmtime(fname)
        return True

    def write_forwards(self, pf, sorted_classes):
        """Write the forward class declarations"""
        pf.write_line('//forward references')
        namespace_map = dict(
            (x,[s for s in sorted_classes if not s.is_external and s.inner_namespace is x])
            for x in self.namespaces)
        for ns in namespace_map:
            if len(namespace_map[ns]) == 0:
                continue
            pf.write_line('namespace {decl}{{'.format(decl=ns.scoped))
            for cls in namespace_map[ns]:
                pf.write_line('{decl};'.format(decl=cls.tree_label))
            pf.write_line('}')
        global_classes = [x for x in sorted_classes if x.inner_namespace is None]
        for cls in global_classes:
            if cls.is_external:
                continue
            pf.write_line('{decl};'.format(decl=cls.reference))
        pf.write_newline()

    def write_license(self):
        if self._license is None:
            return
        from beatle.lib.api import get_licenses
        licenses = get_licenses()
        if self._license not in licenses:
            return  # error
        content = licenses[self._license]
        disclaimer = os.path.join(self._dir, 'LICENSE')
        with open(disclaimer, mode='wt', encoding='ascii', errors='ignore') as f:
            f.write(content)

    def write_makefile(self, sorted_classes=None, logger=None):
        """Write the makefile"""
        # if not subbmited, prepare a sorted list of classes
        if sorted_classes is None:
            try:
                sorted_classes = self.sorted_classes
            except AssertionError as e:
                wx.MessageBox('error {0}'.str(e))
                return
        # open the makefile
        makefile = os.path.join(self._dir, 'Makefile')
        makefile = os.path.realpath(makefile)
        f = open(makefile, 'w')

        # create writer
        pf = Writer.for_file(f)

        # ---------------------------------------------------------------------------------
        #
        #                               DEFINITIONS
        #
        #
        # ---------------------------------------------------------------------------------
        pf.write_line('# definitions')
        pf.write_newline()
        pf.write_line(f'CC = {self.c_compiler}')
        pf.write_line(f'CXX = {self.cpp_compiler}')
        pf.write_line(f'AR = {self.cpp_archiver}')
        pf.write_line(f'LINK = {self.cpp_linker}')

        pf.write_line(f'CPPFLAGS = {self.cpp_flags}')
        pf.write_line(f'SRC = {self._srcDir}')
        pf.write_line(f'LINKFLAGS = {self.link_flags}')
        pf.write_newline()
        # make targets all/clean
        # all : first, the prerequisites
        product = self.target_product
        target_all = ''
        classes = [x for x in sorted_classes if (not x.is_external) and (not x.is_template)]
        modules = [x for x in self._project.modules if not getattr(x, 'skip_build', False)]
        phony = 'clean all'
        if len(self.prebuild_steps):
            phony += ' '.join(('before', phony))
            target_all = ' '.join(('before', target_all))
            pf.write_line('before:')
            for step in self.prebuild_steps:
                pf.write_line('\t{step}'.format(step=step))
            pf.write_newline()
        target_all += product
        prerequisites = ' '.join(['{name}.o'.format(name=x.base_filename) for x in classes])
        prerequisites += ' '
        prerequisites += ' '.join(['{name}.o'.format(name=x.name) for x in modules])
        prerequisites += ' '
        prerequisites += ' '.join(self.extra_objects)

        # master target
        pf.write_line('all : {target_all}'.format(target_all=target_all))
        pf.write_newline()
        pf.write_line('{product}: {prerequisites}'.format(product=product, prerequisites=prerequisites))
        build_type = self.build_type
        if build_type == 'static library':
            pf.write_line('\t$(AR) -r -s ./{product} {prerequisites}'.format(
                product=product, prerequisites=prerequisites))
        elif build_type == 'shared library':
            pf.write_line('\t$(CXX) {prerequisites} $(LINKFLAGS) -o {product}'.format(
                product=product, prerequisites=prerequisites))
        elif build_type == 'executable':
            pf.write_line('\t$(CXX) {prerequisites} $(LINKFLAGS) -o {product}'.format(
                product=product, prerequisites=prerequisites))
        # postbuild-steps (if any)
        for step in self.postbuild_steps:
            pf.write_line('\t{step}'.format(step=step))
        pf.write_newline()

        # target files
        for cls in sorted_classes:
            if cls.is_external or cls.is_template:
                continue
            pf.write_line('{name}.o: {src}/{name}.cpp {inc}/{name}.h'.format(name=cls.base_filename,
                inc=self._includeDir, src=self._srcDir))
            pf.write_line('\t$(CXX) $(CPPFLAGS) -c {src}/{name}.cpp -o {name}.o'.format(src=self._srcDir,
                name=cls.base_filename))
            pf.write_newline()
        for mod in modules:
            if not mod._header:
                pf.write_line('{name}.o: {src}/{source}'.format(name=mod.name,src=self._srcDir,
                    inc=self._includeDir, source=mod._source))
            else:
                pf.write_line('{name}.o: {src}/{source} {inc}/{include}'.format(name=mod.name,src=self._srcDir,
                    inc=self._includeDir, source=mod._source, include=mod._header))
            pf.write_line('\t$(CXX) $(CPPFLAGS) -c {src}/{source} -o {name}.o'.format(
                name=mod.name,src=self._srcDir,
                inc=self._includeDir, source=mod._source))
            pf.write_newline()
        # clean
        pf.write_line('clean:')
        pf.write_line('\trm -f *.o {product}'.format(product=product))
        pf.write_newline()
        pf.write_line('.PHONY: {phony}'.format(phony=phony))
        pf.write_newline()
        f.close()

    @TransactionalMethod('refactor code')
    def refactor_contents(self, old_word, new_word):
        """Do (blind) case-sensitive refactoring changing content.
        This replacemente must be done inside transactional context."""
        import re
        from ._Constructor import Constructor
        from ._Destructor import Destructor
        from ._InitMethod import InitMethod
        from ._ExitMethod import ExitMethod
        from ._IsClassMethod import IsClassMethod
        from ._GetterMethod import GetterMethod
        from ._SetterMethod import SetterMethod
        from ._MemberMethod import MemberMethod
        from ._Function import Function
        expr = re.compile(r'\b{word}\b'.format(word=old_word))
        for element in self._project(Constructor, Destructor, InitMethod, ExitMethod, IsClassMethod,
                                     GetterMethod, SetterMethod, MemberMethod, Function):
            value = element._content
            result = expr.sub(new_word, value)
            if result != value:
                element.save_state()
                element._content=result

