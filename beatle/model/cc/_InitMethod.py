# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:08:46 2013

@author: mel
"""
import wx
import copy
from ._MemberMethod import MemberMethod
from ._Argument import Argument
from ._Type import typeinst


class InitMethod(MemberMethod):
    """Implements is_class method"""
    context_container = True
    argument_container = True

    def __init__(self, **kwargs):
        """Initialization"""
        kwargs['name'] = '__init__'
        kwargs['read_only'] = True
        kwargs['access'] = 'private'
        super(InitMethod, self).__init__(**kwargs)

    def update_code(self):
        """This method updates the init method contents.
        The contents are composed by active semantics and passive ones.
        The active part on what this instance is the owner or parent container is initialised first.
        For the passive part we have to deal with possible initialization order. We found a case
        where an avl tree being using other active identifier as key. Of course that can lead to
        strictly irresolvable situations (cyclic dependencies).

        Note: Yes, cyclic dependencies can be hacked by temporally cross key set/reset values, but
        maybe this is not a good enough solution, mainly because that's an indicator of bad
        design (only God has self dependency) and also requires the same hack on destruction,
        so it will be a mess. (Nevertheless I will revisit this problem on the future)

        Note 2: Having relations with uniqueness based on foreign relation keys is also a delicate
        topic, as when a child is exchanged between parents this key must be updated on the foreign
        relation and even must be able to veto the operation if leads to uniqueness constraint
        violation. An amazing topic that looks more related to a true database operation embedded with
        live code.

        Note 3: Some future extensions could lead to cross-cyclic dependency, we must be aware of that.

        Note 4: Non-aggregated relationship is safe here, but this could populate a harmful behaviour
        later specially if the foreign key is also non-aggregated. Users must be warned.
        """

        from ._Relation import RelationFrom, RelationTo
        parent_class = self.inner_class

        #  active semantics
        to_other = parent_class(RelationTo, filter=lambda x: x.inner_class == parent_class, cut=True )
        init_to_code = '\n'.join(x.INIT_CODE for x in to_other)

        # passive semantics
        from_other = self.inner_class(RelationFrom, filter=lambda x: x.inner_class == parent_class, cut=True)
        # review the relations splitting them into keyed and not keyed
        keyed_from_other = [x for x in from_other if x.key.implementation != 'standard']
        not_keyed_from_other = [x for x in from_other if x not in keyed_from_other]

        # passive semantics without key are safe
        init_from_non_keyed_code = '\n'.join(x.INIT_CODE for x in not_keyed_from_other)

        # non-aggregated keyed relations are safe here (note 4), but we must check if the key is from
        # non-aggregated passive relationship.
        non_aggregated_keyed_from_other = [x for x in keyed_from_other if not x.key.is_aggregate]
        aggregated_keyed_from_other = [x for x in keyed_from_other if x.key.is_aggregate]

        # The x.key.member property is a getter method that access to the key value. Our criteria
        # here is to check the type

        # user warning loop
        for x in non_aggregated_keyed_from_other:
            reference_type = x.key.member.type_instance.type
            for z in from_other:
                if z.key.from_class == reference_type:
                    if not z.key.is_aggregate:
                        wx.LogMessage(
                            f"""DANGEROUS DEPENDENCY DETECTED:
                            You have a relation {x.key.from_alias} -> {x.key.to_alias} that looks key-dependent on
                            the relation {z.key.from_alias} -> {z.key.to_alias}.
                            If so, the order of use of those objects could break relation integrity.
                            Please take in account that this kind of design would be prevented in future Beatle
                            version. YOU HAVE BEEN WARNED.""")
                    else:
                        wx.LogMessage(
                            f"""DANGEROUS DEPENDENCY DETECTED:
                            You have a relation {x.key.from_alias} -> {x.key.to_alias} that looks key-dependent on
                            the aggregated relation {z.key.from_alias} -> {z.key.to_alias}.
                            If so, you must not exchange the ownership from {z.key.from_alias} without previous
                            removal of {x.key.from_alias} relation (you can restore it after), in order to
                            preserve integrity.
                            Future Beatle versions will generate automated handling for those cases. 
                            YOU HAVE BEEN WARNED.""")
                    break

        # ok, generate the code
        non_aggregated_keyed_code = ''
        if len(non_aggregated_keyed_from_other) > 0:
            non_aggregated_keyed_code = '/* POSSIBLE UNSAFE CODE HERE */\n'
            non_aggregated_keyed_code += '\n'.join(x.INIT_CODE for x in non_aggregated_keyed_from_other)

        # ok, now we must examine the aggregated keyed passive relations, and send information to the user.
        # We have the following possibilities:
        # (1) an aggregated relation key that depends on a non aggregated one -> sure crash
        # (2) an aggregated relation key that depends on another -> warn about changes in ownership sides
        # (3) a crossed dependence between aggregates -> sure crash, no way to handle this
        aggregate_dependent = []
        non_aggregate_dependent = []
        free_dependent = []
        non_keyed_dependent = []

        for x in aggregated_keyed_from_other:
            reference_type = x.key.member.type_instance.type
            possible_dependency_found = False
            for z in from_other:
                if z.key.from_class == reference_type:
                    possible_dependency_found = True
                    if z in not_keyed_from_other:  # this the case (2)
                        wx.LogMessage(
                            f"""DANGEROUS DEPENDENCY DETECTED:
                            You have an aggregated relation {x.key.from_alias} -> {x.key.to_alias} that looks
                            key-dependent on the other one {z.key.from_alias} -> {z.key.to_alias}.
                            Beatle has taken account of order of initialization and removal ot these
                            relations, but you must not exchange the ownership from {z.key.from_alias} in order to
                            preserve integrity.
                            Future Beatle versions will generate automated handling for those cases.
                            YOU HAVE BEEN WARNED.""")
                        non_keyed_dependent.append(x)
                    elif not z.key.is_aggregate:  # this the case (1)
                        wx.LogMessage(
                            f"""BAD DEPENDENCY DETECTED:
                            You have a owned relation {x.key.from_alias} -> {x.key.to_alias} that looks 
                            key-dependent on the relation {z.key.from_alias} -> {z.key.to_alias}.
                            If detection is correct, CRASH IS EXPECTED. 
                            Please take in account that this kind of design would be prevented in future Beatle
                            version. 
                            YOU HAVE BEEN WARNED.""")
                        non_aggregate_dependent.append(x)
                    else:
                        aggregate_dependent.append(x)
                    break
            if possible_dependency_found is False:
                free_dependent.append(x)

        # ok, free dependent aggregated keyed is safe
        free_dependent_code = '\n'.join(x.INIT_CODE for x in free_dependent)

        non_keyed_dependent_code = ''
        if len(non_keyed_dependent):
            non_keyed_dependent_code = '/* POSSIBLE UNSAFE CODE HERE */\n'
            non_keyed_dependent_code += '\n'.join(x.INIT_CODE for x in non_keyed_dependent)

        # now, highly crashed code
        non_aggregate_dependent_code = ''
        if len(non_aggregate_dependent) > 0 :
            non_aggregate_dependent_code = '/* POSSIBLE UNSAFE CODE HERE */\n'
            non_aggregate_dependent_code += '\n'.join(x.INIT_CODE for x in non_aggregate_dependent)

        # the remainder relations are processed in iterate and progress way
        # no progress means cyclic dependency (possible). The progress here
        # must report case (2) situations
        to_process = []
        while len(aggregate_dependent) > 0:
            progress_resolution = False
            for x in aggregate_dependent:
                reference_type = x.key.member.type_instance.type
                for z in keyed_from_other:
                    if z.key.from_class == reference_type: # this is a (2) case
                        if z in free_dependent:
                            wx.LogMessage(
                                f"""DANGEROUS DEPENDENCY DETECTED:
                                You have an aggregated relation {x.key.from_alias} -> {x.key.to_alias} that looks 
                                key-dependent on the other one {z.key.from_alias} -> {z.key.to_alias}.
                                Beatle has taken account of order of initialization and removal ot these
                                relations, but you must not exchange the ownership from {z.key.from_alias} in order to
                                preserve integrity.
                                Future Beatle versions will generate automated handling for those cases. 
                                YOU HAVE BEEN WARNED.""")
                            progress_resolution = True
                            to_process.append(z)
                        elif z in non_aggregate_dependent:
                            wx.LogMessage(
                                f"""DANGEROUS DEPENDENCY DETECTED:
                                You have an aggregated relation {x.key.from_alias} -> {x.key.to_alias} that looks 
                                key-dependent on the other one {z.key.from_alias} -> {z.key.to_alias}.
                                Beatle has taken account of order of initialization and removal ot these
                                relations, but you must not exchange the ownership from {z.key.from_alias} in order to
                                preserve integrity.
                                Please take in account that relation {z.key.from_alias} -> {z.key.to_alias}
                                has previously detected as possible unsafe.
                                Future Beatle versions will generate automated handling for those cases. 
                                YOU HAVE BEEN WARNED.""")
                            progress_resolution = True
                            to_process.append(z)
                        elif z not in aggregate_dependent:
                            wx.LogMessage(
                                f"""DANGEROUS DEPENDENCY DETECTED:
                                You have an aggregated relation {x.key.from_alias} -> {x.key.to_alias} that looks 
                                key-dependent on the other one {z.key.from_alias} -> {z.key.to_alias}.
                                Beatle has taken account of order of initialization and removal ot these
                                relations, but you must not exchange the ownership from {z.key.from_alias} in order to
                                preserve integrity.
                                Future Beatle versions will generate automated handling for those cases. 
                                YOU HAVE BEEN WARNED.""")
                            #  this happens when z was in aggregate_dependent but
                            #  was solved in a previous iteration
                            progress_resolution = True
                            to_process.append(z)
                        break
            if progress_resolution is False:
                break
            aggregate_dependent = [x for x in aggregate_dependent if x not in to_process]

        if len(aggregate_dependent) > 0:
            cyclic_dependency = ','.join(f'{x.key.from_alias}' for x in aggregate_dependent)
            wx.LogMessage(
                f"""CYCLIC DEPENDENCY DETECTED (UNSOLVABLE):
                Detected a possible key cyclic dependency between the relations 
                {cyclic_dependency}. 
                If the detection is correct, the code won't work.
                Future Beatle versions will automated block this situations. 
                YOU HAVE BEEN WARNED.""")

        # ok, now we have the relations in the better order as we can have
        to_process_code = ''
        if len(to_process) > 0:
            to_process_code = '/* POSSIBLE UNSAFE CODE HERE */\n'
            to_process_code += '\n'.join(x.INIT_CODE for x in to_process)

        aggregate_dependent_code = ''
        if len(aggregate_dependent) > 0:
            aggregate_dependent_code = '/* POSSIBLE UNSAFE CODE HERE */\n'
            aggregate_dependent_code += '\n'.join(x.INIT_CODE for x in aggregate_dependent)

        self._content = f"""
        //INIT TO OTHERS RELATIONS
        {init_to_code}
        
        //INIT FROM OTHERS RELATIONS
        {init_from_non_keyed_code}
        {non_aggregated_keyed_code}
        {free_dependent_code}
        {non_keyed_dependent_code}
        {non_aggregate_dependent_code}
        {to_process_code}
        {aggregate_dependent_code}
        """

    def update_arguments(self):
        """Actualiza los argumentos"""

        #limpiamos la lista de argumentos previa
        s = copy.copy(self[Argument])
        for arg in s:
            arg.delete()
        from ._Relation import RelationFrom
        # solamente las relaciones 'from' requieren argumentos de inicializacion
        # y, de estas, solamente aquellas que sean owned.
        # Recorremos las relaciones y creamos los argumentos requeridos
        parent_class = self.inner_class
        from_s = parent_class(RelationFrom, filter=lambda x: x.inner_class == parent_class, cut=True )
        for rel in from_s:
            relation = rel.key
            if relation.is_aggregate and not relation.is_static:
                base_type = relation.from_class
                if base_type.is_template:
                    arg_type_instance = typeinst(type=base_type, type_args=base_type.template_args, ptr=True)
                else:
                    arg_type_instance = typeinst(type=base_type, ptr=True)
                Argument(
                    type=arg_type_instance,
                    name=rel.format('ptr_{alias_from}'),
                    parent=self
                    )

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs.update(super(InitMethod, self).get_kwargs())
        return kwargs

    def on_undo_redo_changed(self):
        """Update from app"""
        super(InitMethod, self).on_undo_redo_changed()

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        super(InitMethod, self).on_undo_redo_removing()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(InitMethod, self).on_undo_redo_add()
