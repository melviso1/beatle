import copy
from ._MemberMethod import MemberMethod
from ._Argument import Argument
from ._Type import typeinst
from beatle.lib.utils import cached_type

class SetterMethod(MemberMethod):
    """Implements member method"""
    context_container = True
    argument_container = True

    # visual methods
    def draggable(self):
        """returns info about if the object can be moved"""
        return False

    def __init__(self, **kwargs):
        """Initialization"""
        parent = kwargs['parent']
        kwargs['name'] = 'set_{}'.format(parent.name)
        kwargs['type'] = typeinst(type=cached_type(parent.project,'void'))
        kwargs['inline'] = True 
        kwargs['constmethod'] = False
        kwargs['content'] = '{member}={name};'.format(member=parent.prefixed_name,name=parent.name)
        super(SetterMethod, self).__init__(**kwargs)
        # add argument
        type_instance = parent.type_instance
        type_args = type_instance.get_kwargs()
        complex_type = type_instance.is_ptr or type_instance.is_array or type_instance.is_ref or type_instance.is_rref
        simple_type = (not complex_type) and type_instance.base_type.name in [
            'bool','char', 'short', 'int', 'long', 'float', 'double']
        if parent.volatile or simple_type:
            type_args['ref'] = False
        else:
            type_args['ref'] = True
        if type_args['ptr']:
            type_args['constptr'] = True
        else:
            type_args['const'] = True
        kwargs['type'] = typeinst(**type_args)
        kwargs['name'] = '{name}'.format(name=parent.name)
        kwargs['parent'] = self
        Argument(**kwargs)

