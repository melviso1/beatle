# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""

@author: mel
"""
from ._CCTComponent import CCTComponent
from ._ClassDiagram import ClassDiagram


class Note(CCTComponent):
    """Implements class representation"""
    def __init__(self, **kwargs):
        """Initialization method"""
        kwargs['visibleInTree'] = False
        super(Note, self).__init__(**kwargs)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs.update(super(Note, self).get_kwargs())
        return kwargs

    def delete(self):
        """Delete diagram objects"""
        # process diagrams
        dias = self.project(ClassDiagram)
        for dia in dias:
            # Check if inherit is in
            elem = dia.find_element(self)
            if elem is not None:
                dia.save_state()
                dia.remove_element(elem)
                if hasattr(dia, '_pane') and dia._pane is not None:
                    dia._pane.Refresh()
        super(Note, self).delete()

    def on_undo_redo_changed(self):
        """Update from app"""
        # update class diagrams
        project = self.project
        if project is not None:
            dias = project(ClassDiagram)
            for dia in dias:
                # Check if class is in
                elem = dia.find_element(self)
                if elem is not None:
                    elem.layout()
                    if hasattr(dia, '_pane') and dia._pane is not None:
                        dia._pane.Refresh()
        super(Note, self).on_undo_redo_changed()
