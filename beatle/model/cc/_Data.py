# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 08:28:25 2013

@author: mel
"""


from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject

from ._CCTComponent import CCTComponent
from ._CCContext import ContextDeclaration, ContextImplementation


class Data(CCTComponent):
    """Implements data"""
    context_container = True

    # visual methods
    @TransactionalMethod('move variable {0}')
    def drop(self, to):
        """Drops datamember inside project or another folder """
        target = to.inner_variable_container
        if not target:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        "Initialization"
        self._typei = kwargs['type']
        self._static = kwargs.get('static', False)
        self._volatile = kwargs.get('volatile', False)
        self._default = kwargs.get('default', "")
        super(Data, self).__init__(**kwargs)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def default(self):
        return self._default

    @property
    def static(self):
        return self._static

    @property
    def type_instance(self):
        """return the type instance"""
        return self._typei

    @property
    def volatile(self):
        return self._volatile

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(Data, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'type': self._typei,
            'static': self._static,
            'volatile': self._volatile,
            'default': self._default
        }
        kwargs.update(super(Data, self).get_kwargs())
        return kwargs

    @ContextDeclaration()
    def write_declaration(self, pf):
        """Write the member method declaration"""
        self.write_comment(pf)
        pf.write_line("extern {0};".format(self.label))

    @ContextImplementation()
    def write_code(self, pf):
        """write data definition"""
        s = str(self._typei)
        if self._volatile:
            s = "volatile " + s
        if self._static:
            s = "static " + s
        if self._default:
            pf.write_line(s.format("{self.scope}{self._name} = {self._default};".format(self=self)))
        else:
            pf.write_line(s.format("{self.scope}{self._name};".format(self=self)))

    def get_initializer(self):
        """Return the initializer sequence"""
        if len(self._default) > 0:
            return self._default
        return self._name

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("data")

    def get_static_definition(self):
        """Return global, static definition"""
        if not self._static:
            return ''
        stype = str(self._typei)
        if self._volatile:
            stype = "volatile " + stype
        decl = stype.format(self.scoped)
        return '{decl} = {value}'.format(decl, self._default)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Data, self).on_undo_redo_add()

    def on_undo_redo_changed(self):
        """Make changes in the model as result of change"""
        super(Data, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    @property
    def label(self):
        """Get tree label"""
        s = str(self._typei)
        if self._volatile:
            s = "volatile " + s
        if self._static:
            s = "static " + s
        epi = ""
        return s.format(self._name) + " " + epi

    @property
    def scope(self):
        """Returns the scope string ie <namespace>::<namespace>::...::
           Returns empty string instead of '::' """
        return self.parent.scope

