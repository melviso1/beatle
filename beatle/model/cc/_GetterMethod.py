
import copy
from ._MemberMethod import MemberMethod
from ._Type import typeinst
from beatle.lib.utils import cached_type


class GetterMethod(MemberMethod):
    """Implements member method"""
    context_container = True
    argument_container = True

    # visual methods
    def draggable(self):
        """returns info about if the object can be moved"""
        return False

    def __init__(self, **kwargs):
        """Initialization"""
        parent = kwargs['parent']
        type_instance = parent.type_instance
        type_args = type_instance.get_kwargs()
        complex_type = type_instance.is_ptr or type_instance.is_array or type_instance.is_ref or type_instance.is_rref
        simple_type = (not complex_type) and type_instance.base_type.name in [
            'bool','char', 'short', 'int', 'long', 'float', 'double']
        if parent.volatile or simple_type:
            type_args['ref'] = False
        else:
            type_args['ref'] = True
        if type_args['ptr']:
            type_args['constptr'] = True
        else:
            type_args['const'] = True

        if parent.type_instance.base_type.name == "bool":
            kwargs['name'] = 'is_{}'.format(parent.name)
        else:
            kwargs['name'] = 'get_{}'.format(parent.name)
        kwargs['inline'] = True
        kwargs['constmethod'] = not kwargs.get('static', False)
        kwargs['content'] = 'return {};'.format(parent.prefixed_name)
        kwargs['type'] = typeinst(**type_args)

        super(GetterMethod, self).__init__(**kwargs)

