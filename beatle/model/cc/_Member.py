# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 08:28:25 2013

@author: mel
"""


from ._CCTComponent import CCTComponent


class Member(CCTComponent):
    """Implements member"""

    def draggable(self):
        """returns info about if the object can be moved"""
        return self._draggeable

    def __init__(self, **kwargs):
        "Initialization"
        super(Member, self).__init__(**kwargs)
        parent_class = self.outer_class
        parent_class._lastSrcTime = None
        parent_class._lastHdrTime = None
        self._draggeable = kwargs.get('draggeable', True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        return super(Member, self).get_kwargs()

    def __setstate__(self, d):
        """Load pickle context.
        This will add some missing attributes in previous releases."""
        d['_draggeable'] = d.get('_draggeable', True)
        super(Member, self).__setstate__(d)
