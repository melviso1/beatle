# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 08:28:25 2013

@author: mel
"""
import re
from beatle.lib.tran import TransactionalMethod, TransactionStack, TransactionObject, TransactionalMoveObject
from beatle.lib.utils import cached_type
from ._Type import typeinst
from ._MemberMethod import MemberMethod
from ._MemberData import MemberData
from ._Friendship import Friendship
from ._Argument import Argument
from ._Constructor import Constructor
from ._Destructor import Destructor
from ._ExitMethod import ExitMethod
from ._CCTComponent import CCTComponent
from ._ClassDiagram import ClassDiagram
from ._GetterMethod import GetterMethod

HIDE_TREE = True


def camel_case_to_snake(text):
    """Convert a Camel Case expression to snake one.
    This solution was inspired by https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
    improved after reading some complaints."""
    return re.sub(r'(?<!^)(((?<![A-Z])(?=[A-Z]))|((?<=[A-Z])(?=[A-Z][^A-Z])))', '_', text)


def macro_method(function):
    """method decorator that provides completion of relation templates"""
    def wrap(self, *args, **kwargs):
        """wrapped calls"""
        s = function(self, *args, **kwargs)
        try:
            return self.format(s)
        except KeyError as error:
            raise ValueError('unexpected key {}'.format(error.args[0]))
        except Exception as e:
            import sys
            z = sys.exc_info()[0]
            raise ValueError('macro_method cant be applied here.{e}\n{z}'.format(e=e,z=str(z)))
    return wrap


def macro_method_complete(function):
    """method decorator that extracts content from method doc
    and applies conversion and creation. The decorated method
    must only return the list of kwargs for method and arguments"""
    def wrap(self, *args, **kwargs):
        kwargs_list = function(self, *args, **kwargs)
        if len(kwargs_list) > 0:
            kwargs_method = kwargs_list[0]
            kwargs_method['content'] = self.format(function.__doc__)
            method = MemberMethod(**kwargs_method)
            for kwargs_arg in kwargs_list[1:]:
                kwargs_arg['parent'] = method
                Argument(**kwargs_arg)
    wrap.__doc__ = function.__doc__
    return wrap


def critical_macro_method(function):
    def wrap(self, *args, **kwargs):
        """wrapped calls"""
        try:
            return """
    {{
        {lock}
        {code}
    }}
    """.format(lock=self.CRITICAL_LOCK(), code=function(self, *args, **kwargs))
        except KeyError as error:
            raise ValueError('unexpected key {}'.format(error.args[0]))
        except Exception as e:
            raise ValueError("macro_method can't be applied here.{}".format(e))
    return wrap


def critical_iterator_macro_method(function):
    def wrap(self, *args, **kwargs):
        """wrapped calls"""
        try:
            return """
    {{
        {lock}
        {code}
    }}
    """.format(lock=self.CRITICAL_ITERATOR_LOCK(), code=function(self, *args, **kwargs))
        except KeyError as error:
            raise ValueError('unexpected key {}'.format(error.args[0]))
        except Exception as e:
            raise ValueError("macro_method can't be applied here.{}".format(e))
    return wrap


def critical_static_macro_method(function):
    def wrap(self, *args, **kwargs):
        """wrapped calls"""
        try:
            return """
    {{
        {lock}
        {code}
    }}
    """.format(lock=self.CRITICAL_STATIC_LOCK(), code=function(self, *args, **kwargs))
        except KeyError as error:
            raise ValueError('unexpected key {}'.format(error.args[0]))
        except Exception as e:
            raise ValueError("macro_method can't be applied here.{}".format(e))
    return wrap


def critical_iterator_static_macro_method(function):
    def wrap(self, *args, **kwargs):
        """wrapped calls"""
        try:
            return """
    {{
        {lock}
        {code}
    }}
    """.format(lock=self.CRITICAL_STATIC_ITERATOR_LOCK(), code=function(self, *args, **kwargs))
        except KeyError as error:
            raise ValueError('unexpected key {}'.format(error.args[0]))
        except Exception as e:
            raise ValueError("macro_method can't be applied here.{}".format(e))
    return wrap


class Relation(TransactionObject):
    """Implements a relation object"""
    # The Relation class is the hidden key of the both sides
    # This class is referenced by RelationFrom and RelationTo
    # by his _key attribute
    def __init__(self, **kwargs):
        """Init relation"""
        self._macro_dict = {}
        self._single = kwargs.get('single', False)
        self._aggregate = kwargs.get('aggregate', False)
        self._critical = kwargs.get('critical', False)
        self._static = kwargs.get('static', False)
        self._filter = kwargs.get('filter', False)
        self._unique = kwargs.get('unique', False)
        self._unikey = kwargs.get('key', None)
        self._member = kwargs.get('member', None)
        self._implementation = kwargs.get('implementation', "standard")
        self._note = kwargs.get('note', "")
        self._to_class = kwargs['to_class']
        self._from_class = kwargs['from_class']
        self._project = self._to_class.project
        self._to_alias = kwargs.get('toName', 'to_{}'.format(self._to_class.name))
        self._from_alias = kwargs.get('fromName', 'from_{}'.format(self._from_class.name))
        self._access = kwargs.get('access', 'public')
        self._to_relation = RelationTo(
            name=self._to_alias,
            parent=self._from_class,
            read_only=kwargs.get('read_only', False),
            key=self)
        self._from_relation = RelationFrom(
            name=self._from_alias,
            parent=self._to_class,
            read_only=kwargs.get('read_only', False),
            key=self)
        # add relation to class diagrams
        for dia in self._project(ClassDiagram):
            child = dia.find_element(self._to_class)
            if child is None:
                continue
            parent = dia.find_element(self._from_class)
            if parent is None:
                continue
            dia.save_state()
            dia.add_relation(self, parent, child)
        super(Relation, self).__init__()
        self.build()
        if self._from_class._serial and self._to_class._serial:
            self._from_class.update_serial_methods()

    @property
    def filter(self):
        return self._filter

    @property
    def member(self):
        """The member key for the relation as getter method"""
        return self._member

    @property
    def is_critical(self):
        return self._critical

    @property
    def is_single(self):
        return self._single

    @property
    def is_aggregate(self):
        return self._aggregate

    @property
    def is_static(self):
        return self._static

    @property
    def is_unique(self):
        return self._unique

    @property
    def from_alias(self):
        return self._from_alias

    @from_alias.setter
    def from_alias(self, alias):
        self._from_alias = alias
        self._from_relation._name = alias

    @property
    def to_alias(self):
        return self._to_alias

    @to_alias.setter
    def to_alias(self, alias):
        self._to_alias = alias
        self._to_relation._name = alias

    @property
    def from_class(self):
        return self._from_class

    @property
    def to_class(self):
        return self._to_class

    @property
    def from_relation(self):
        return self._from_relation

    @property
    def to_relation(self):
        return self._to_relation

    @property
    def implementation(self):
        return self._implementation

    @property
    def macro_dict(self):
        return self._macro_dict

    @property
    def implementation(self):
        return self._implementation

    def build_dict(self):
        self._macro_dict = {
            'prefix_to': self._to_class.member_prefix,
            'prefix_from': self._from_class.member_prefix,
            'from': self._from_class.name,
            'to': self._to_class.name,
            'to_type': self._to_class.scope[:-2],
            'from_type': self._from_class.scope[:-2],
            'alias_from': self._from_alias,
            'alias_to': self._to_alias,
            'tree_key': 'undefined',
            'member': (self._member and self._member.call) or '',
            'member_type': self._member and str(self._member.type_instance) or '',
            }

    def rebuild(self):
        self._from_relation.delete_children()
        self._to_relation.delete_children()
        self.build()

    def build(self):
        """Do elements initialization"""
        self.build_dict()
        if self._critical:
            if self._single:
                if self._aggregate:
                    self._to_relation.RELATION_CRITICAL_SINGLE_OWNED_ACTIVE()
                    self._from_relation.RELATION_CRITICAL_SINGLE_OWNED_PASSIVE()
                else:
                    self._to_relation.RELATION_CRITICAL_SINGLE_ACTIVE()
                    self._from_relation.RELATION_CRITICAL_SINGLE_PASSIVE()
            else:
                if self._implementation == 'standard':
                    if self._static:
                        if self._aggregate:
                            self._to_relation.RELATION_CRITICAL_STATIC_MULTIOWNED_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_STATIC_MULTIOWNED_PASSIVE()
                        else:
                            self._to_relation.RELATION_CRITICAL_STATIC_MULTI_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_STATIC_MULTI_PASSIVE()
                    else:
                        if self._aggregate:
                            self._to_relation.RELATION_CRITICAL_MULTIOWNED_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_MULTIOWNED_PASSIVE()
                        else:
                            self._to_relation.RELATION_CRITICAL_MULTI_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_MULTI_PASSIVE()
                elif self._implementation == 'value_tree':
                    if self._unique:
                        if self._aggregate:
                            self._to_relation.RELATION_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_UNIQUEVALUETREE_OWNED_PASSIVE()
                        else:
                            self._to_relation.RELATION_CRITICAL_UNIQUEVALUETREE_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_UNIQUEVALUETREE_PASSIVE()
                    else:
                        if self._aggregate:
                            self._to_relation.RELATION_CRITICAL_VALUETREE_OWNED_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_VALUETREE_OWNED_PASSIVE()
                        else:
                            self._to_relation.RELATION_CRITICAL_VALUETREE_ACTIVE()
                            self._from_relation.RELATION_CRITICAL_VALUETREE_PASSIVE()
                else:
                    if self._aggregate:
                        self._to_relation.RELATION_CRITICAL_AVLTREE_OWNED_ACTIVE()
                        self._from_relation.RELATION_CRITICAL_AVLTREE_OWNED_PASSIVE()
                    else:
                        self._to_relation.RELATION_CRITICAL_AVLTREE_ACTIVE()
                        self._from_relation.RELATION_CRITICAL_AVLTREE_PASSIVE()
        else:
            if self._single:
                if self._aggregate:
                    self._to_relation.RELATION_SINGLE_OWNED_ACTIVE()
                    self._from_relation.RELATION_SINGLE_OWNED_PASSIVE()
                else:
                    self._to_relation.RELATION_SINGLE_ACTIVE()
                    self._from_relation.RELATION_SINGLE_PASSIVE()
            else:
                if self._implementation == 'standard':
                    if self._static:
                        if self._aggregate:
                            self._to_relation.RELATION_STATIC_MULTIOWNED_ACTIVE()
                            self._from_relation.RELATION_STATIC_MULTIOWNED_PASSIVE()
                        else:
                            self._to_relation.RELATION_STATIC_MULTI_ACTIVE()
                            self._from_relation.RELATION_STATIC_MULTI_PASSIVE()
                    else:
                        if self._aggregate:
                            self._to_relation.RELATION_MULTIOWNED_ACTIVE()
                            self._from_relation.RELATION_MULTIOWNED_PASSIVE()
                        else:
                            self._to_relation.RELATION_MULTI_ACTIVE()
                            self._from_relation.RELATION_MULTI_PASSIVE()
                elif self._implementation == 'value_tree':
                    if self._unique:
                        if self._aggregate:
                            self._to_relation.RELATION_UNIQUEVALUETREE_OWNED_ACTIVE()
                            self._from_relation.RELATION_UNIQUEVALUETREE_OWNED_PASSIVE()
                        else:
                            self._to_relation.RELATION_UNIQUEVALUETREE_ACTIVE()
                            self._from_relation.RELATION_UNIQUEVALUETREE_PASSIVE()
                    else:
                        if self._aggregate:
                            self._to_relation.RELATION_VALUETREE_OWNED_ACTIVE()
                            self._from_relation.RELATION_VALUETREE_PASSIVE(True)
                        else:
                            self._to_relation.RELATION_VALUETREE_ACTIVE()
                            self._from_relation.RELATION_VALUETREE_PASSIVE()
                else:
                    if self._aggregate:
                        self._to_relation.RELATION_AVLTREE_OWNED_ACTIVE()
                        self._from_relation.RELATION_AVLTREE_PASSIVE(True)
                    else:
                        self._to_relation.RELATION_AVLTREE_ACTIVE()
                        self._from_relation.RELATION_AVLTREE_PASSIVE()
        self._from_class.update_class_relations()
        self._to_class.update_class_relations()
        self.refresh_diagrams()

    def refresh_diagrams(self):
        for dia in self._project(ClassDiagram):
            relation = dia.find_element(self)
            if relation is not None:
                relation.refresh_changes()
                pane = getattr(dia, '_pane', None)
                if pane is not None:
                    pane.Refresh()

    @property
    def project(self):
        """Gets the project"""
        return self._project

    @property
    def note(self):
        """Gets the comments"""
        return self._note

    @note.setter
    def note(self, value):
        """Sets the note"""
        self._note = value

    def save_state(self):
        """Save the current state in the undo stack"""
        self._from_relation.save_state()
        self._to_relation.save_state()
        super(Relation, self).save_state()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        self.refresh_diagrams()
        super(Relation, self).on_undo_redo_add()


    def on_undo_redo_changed(self):
        """Handles changes"""
        if not TransactionStack.in_undo_redo():
            pass  # pending
        project = self.project
        if project is None:
            return
        self.refresh_diagrams()
        super(Relation, self).on_undo_redo_changed()

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        super(Relation, self).on_undo_redo_removing()
        if not self.in_undo_redo():
            if self._from_class._serial and self._to_class._serial:
                self._from_class.update_serial_methods()

    def delete(self):
        """Transaction delete"""
        self._from_relation.delete()
        self._to_relation.delete()
        project = self.project
        if project is not None:
            # process diagrams
            dias = project.diagrams
            for dia in dias:
                # Check if inherit is in
                elem = dia.find_element(self)
                if elem is not None:
                    dia.save_state()
                    dia.remove_element(elem)
                    pane = getattr(dia, '_pane', None)
                    if pane is not None:
                        pane.Refresh()
        self._from_class.update_class_relations()
        self._to_class.update_class_relations()
        super(Relation, self).delete()


class RelationFrom(CCTComponent):
    """Implements the 'to' side."""
    member_container = True

    @TransactionalMethod('move relation origin {0}')
    def drop(self, to):
        """Drops this inside project or another folder """
        target = to.relation_container and to
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initializes 'from' access inside 'to'"""
        self._key = kwargs['key']
        super(RelationFrom, self).__init__(**kwargs)

    @property
    def tree_label(self):
        return self._key.from_alias

    @property
    def key(self):
        return self._key

    @property
    def note(self):
        return self._key.note

    @note.setter
    def note(self, value):
        self._key.note = value

    def format(self, format_str):
        """Return formatted text with macro replacements"""
        return format_str.format(**self._key.macro_dict)

    def type(self, ref_type, **kwargs):
        """returns type instance constructed from type or from type name,
        with specifiers"""
        if type(ref_type) is str:
            ref_type = cached_type(self.project, ref_type)
        if ref_type.is_template:
            return typeinst(type=ref_type, type_args=ref_type.template_args, **kwargs)
        else:
            return typeinst(type=ref_type, **kwargs)

    @macro_method
    def CRITICAL_LOCK(self):
        return """
        critical_section_lock lock({prefix_to}ref_{alias_from}->get_relation_critical_section_{alias_to}());
        """

    @macro_method
    def CRITICAL_STATIC_LOCK(self):
        return """
        critical_section_lock lock({from}::get_relation_critical_section_{alias_to}());
        """

    def EXIT_AVLTREE_OWNED_PASSIVE(self):
        return self.EXIT_AVLTREE_PASSIVE()

    @macro_method
    def EXIT_AVLTREE_PASSIVE(self):
        return """
    if ({prefix_to}ref_{alias_from})
    {{
        {prefix_to}ref_{alias_from}->remove_{alias_to}(this);
    }}
    """

    @critical_macro_method
    def EXIT_CRITICAL_AVLTREE_OWNED_PASSIVE(self):
        return self.EXIT_AVLTREE_OWNED_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_AVLTREE_PASSIVE(self):
        return self.EXIT_AVLTREE_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_MULTIOWNED_MULTIPARENT_PASSIVE(self):
        return self.EXIT_MULTIOWNED_MULTIPARENT_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_MULTIOWNED_PASSIVE(self):
        return self.EXIT_MULTIOWNED_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_MULTI_PASSIVE(self):
        return self.EXIT_MULTI_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_SINGLE_OWNED_PASSIVE(self):
        return self.EXIT_SINGLE_OWNED_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_SINGLE_PASSIVE(self):
        return self.EXIT_SINGLE_PASSIVE()

    @critical_static_macro_method
    def EXIT_CRITICAL_STATIC_MULTI_PASSIVE(self):
        return self.EXIT_STATIC_MULTI_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_UNIQUEVALUETREE_OWNED_PASSIVE(self):
        return self.EXIT_UNIQUEVALUETREE_OWNED_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_UNIQUEVALUETREE_PASSIVE(self):
        return self.EXIT_UNIQUEVALUETREE_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_VALUETREE_OWNED_PASSIVE(self):
        return self.EXIT_VALUETREE_OWNED_PASSIVE()

    @critical_macro_method
    def EXIT_CRITICAL_VALUETREE_PASSIVE(self):
        return self.EXIT_VALUETREE_PASSIVE()

    @critical_static_macro_method
    def EXIT_CRITICAL_STATIC_MULTIOWNED_PASSIVE(self):
        return self.EXIT_STATIC_MULTIOWNED_PASSIVE()

    @macro_method
    def EXIT_MULTI_MULTIPARENT_PASSIVE(self):
        return """
    {{
        {from_type}* ptr_{alias_from};
        size_t n={prefix_to}{alias_from}_suscriber.GetObjectsCount();
        while(n--)
        {{
            ptr_{alias_from}=({from_type}*){prefix_to}{alias_from}_suscriber.GetObject(n);
            ptr_{alias_from}->{prefix_from}{alias_to}Subscriptor.AwayObject(&{alias_from}_suscriber);
        }}
    }}
    """

    def EXIT_MULTIOWNED_MULTIPARENT_PASSIVE(self):
        return self.EXIT_MULTI_MULTIPARENT_PASSIVE()

    def EXIT_MULTIOWNED_PASSIVE(self):
        return self.EXIT_MULTI_PASSIVE()

    @macro_method
    def EXIT_MULTI_PASSIVE(self):
        return """
    assert(this);
    if ( {prefix_to}ref_{alias_from} != nullptr )
    {{
        {prefix_to}ref_{alias_from}->remove_{alias_to}(this);
    }}
    """

    def EXIT_SINGLE_OWNED_PASSIVE(self):
        return self.EXIT_SINGLE_PASSIVE()

    @macro_method
    def EXIT_SINGLE_PASSIVE(self):
        return """
    assert(this);
    if ({prefix_to}ref_{alias_from})
    {{
        assert({prefix_to}ref_{alias_from}->{prefix_from}ref_{alias_to} == this);
        {prefix_to}ref_{alias_from}->{prefix_from}ref_{alias_to} = nullptr;
        {prefix_to}ref_{alias_from} = nullptr;
    }}
    """

    def EXIT_STATIC_MULTIOWNED_PASSIVE(self):
        return self.EXIT_STATIC_MULTI_PASSIVE()

    @macro_method
    def EXIT_STATIC_MULTI_PASSIVE(self):
        return """
    assert(this);
    if ({prefix_to}prev_{alias_from} || {prefix_to}next_{alias_from} || {from_type}::get_first_{alias_to}() == this)
    {{
        {from_type}::{alias_to}_iterator::check(this);

        {from_type}::{prefix_from}count_{alias_to}--;

        if ({prefix_to}next_{alias_from})
        {{
            {prefix_to}next_{alias_from}->{prefix_to}prev_{alias_from} = {prefix_to}prev_{alias_from};
        }}
        else
        {{
            {from_type}::{prefix_from}last_{alias_to} = {prefix_to}prev_{alias_from};
        }}

        if ({prefix_to}prev_{alias_from})
        {{
            {prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = {prefix_to}next_{alias_from};
        }}
        else
        {{
            {from_type}::{prefix_from}first_{alias_to} = {prefix_to}next_{alias_from};
        }}
        {prefix_to}prev_{alias_from} = nullptr;
        {prefix_to}next_{alias_from} = nullptr;
    }}
    """

    def EXIT_UNIQUEVALUETREE_OWNED_PASSIVE(self):
        return self.EXIT_UNIQUEVALUETREE_PASSIVE()

    @macro_method
    def EXIT_UNIQUEVALUETREE_PASSIVE(self):
        return """
    if ({prefix_to}ref_{alias_from})
    {{
        {prefix_to}ref_{alias_from}->remove_{alias_to}(this);
    }}
    """

    def EXIT_VALUETREE_OWNED_PASSIVE(self):
        return self.EXIT_VALUETREE_PASSIVE()

    @macro_method
    def EXIT_VALUETREE_PASSIVE(self):
        return """
    if ({prefix_to}ref_{alias_from})
    {{
        {prefix_to}ref_{alias_from}->remove_{alias_to}(this);
    }}
    """

    @property
    def EXIT_CODE(self):
        """Returns code"""
        if self._key.is_critical:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.EXIT_CRITICAL_SINGLE_OWNED_PASSIVE()
                else:
                    return self.EXIT_CRITICAL_SINGLE_PASSIVE()
            else:
                if self._key.implementation == 'standard':
                    if self._key.is_aggregate:
                        if self._key.is_static:
                            return self.EXIT_CRITICAL_STATIC_MULTIOWNED_PASSIVE()
                        else:
                            return self.EXIT_CRITICAL_MULTIOWNED_PASSIVE()
                    else:
                        if self._key.is_static:
                            return self.EXIT_CRITICAL_STATIC_MULTI_PASSIVE()
                        else:
                            return self.EXIT_CRITICAL_MULTI_PASSIVE()
                elif self._key.implementation == 'value_tree':
                    if self._key.is_unique:
                        if self._key.is_aggregate:
                            return self.EXIT_CRITICAL_UNIQUEVALUETREE_OWNED_PASSIVE()
                        else:
                            return self.EXIT_CRITICAL_UNIQUEVALUETREE_PASSIVE()
                    else:
                        if self._key.is_aggregate:
                            return self.EXIT_CRITICAL_VALUETREE_OWNED_PASSIVE()
                        else:
                            return self.EXIT_CRITICAL_VALUETREE_PASSIVE()
                else:
                    if self._key.is_aggregate:
                        return self.EXIT_CRITICAL_AVLTREE_OWNED_PASSIVE()
                    else:
                        return self.EXIT_CRITICAL_AVLTREE_PASSIVE()
        else:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.EXIT_SINGLE_OWNED_PASSIVE()
                else:
                    return self.EXIT_SINGLE_PASSIVE()
            else:
                if self._key.implementation == 'standard':
                    if self._key.is_aggregate:
                        if self._key.is_static:
                            return self.EXIT_STATIC_MULTIOWNED_PASSIVE()
                        else:
                            return self.EXIT_MULTIOWNED_PASSIVE()
                    else:
                        if self._key.is_static:
                            return self.EXIT_STATIC_MULTI_PASSIVE()
                        else:
                            return self.EXIT_MULTI_PASSIVE()
                elif self._key.implementation == 'value_tree':
                    if self._key.is_unique:
                        if self._key.is_aggregate:
                            return self.EXIT_UNIQUEVALUETREE_OWNED_PASSIVE()
                        else:
                            return self.EXIT_UNIQUEVALUETREE_PASSIVE()
                    else:
                        if self._key.is_aggregate:
                            return self.EXIT_VALUETREE_OWNED_PASSIVE()
                        else:
                            return self.EXIT_VALUETREE_PASSIVE()
                else:
                    if self._key.is_aggregate:
                        return self.EXIT_AVLTREE_OWNED_PASSIVE()
                    else:
                        return self.EXIT_AVLTREE_PASSIVE()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("parent", 'public')

    @macro_method
    def INIT_AVLTREE_OWNED_PASSIVE(self):
        return """
    //avltree_owned_passive {alias_from} -> {alias_to} 
    {prefix_to}ref_{alias_from} = nullptr;
    {prefix_to}parent_{alias_from} = nullptr;
    {prefix_to}left_{alias_from} = nullptr;
    {prefix_to}right_{alias_from} = nullptr;
    {prefix_to}bal_{alias_from} = 0;
    assert(ptr_{alias_from});
    ptr_{alias_from}->add_{alias_to}(this);
    """

    @macro_method
    def INIT_AVLTREE_PASSIVE(self):
        return """
    //avltree_passive {alias_from} -> {alias_to}
    {prefix_to}ref_{alias_from} = nullptr;
    {prefix_to}parent_{alias_from} = nullptr;
    {prefix_to}left_{alias_from} = nullptr;
    {prefix_to}right_{alias_from} = nullptr;
    {prefix_to}bal_{alias_from} = 0;
    """

    @critical_macro_method
    def INIT_CRITICAL_AVLTREE_OWNED_PASSIVE(self):
        return self.INIT_AVLTREE_OWNED_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_AVLTREE_PASSIVE(self):
        return self.INIT_AVLTREE_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_MULTI_MULTIPARENT_PASSIVE(self):
        return self.INIT_MULTI_MULTIPARENT_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_MULTIOWNED_MULTIPARENT_PASSIVE(self):
        return self.INIT_MULTIOWNED_MULTIPARENT_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_MULTIOWNED_PASSIVE(self):
        return self.INIT_MULTIOWNED_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_MULTI_PASSIVE(self):
        return self.INIT_MULTI_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_SINGLE_OWNED_PASSIVE(self):
        return self.INIT_SINGLE_OWNED_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_SINGLE_PASSIVE(self):
        return self.INIT_SINGLE_PASSIVE

    @critical_static_macro_method
    def INIT_CRITICAL_STATIC_MULTIOWNED_PASSIVE(self):
        return self.INIT_STATIC_MULTIOWNED_PASSIVE()

    @critical_static_macro_method
    def INIT_CRITICAL_STATIC_MULTI_PASSIVE(self):
        return self.INIT_STATIC_MULTI_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_UNIQUEVALUETREE_OWNED_PASSIVE(self):
        return self.INIT_UNIQUEVALUETREE_OWNED_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_UNIQUEVALUETREE_PASSIVE(self):
        return self.INIT_UNIQUEVALUETREE_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_VALUETREE_PASSIVE(self):
        return self.INIT_VALUETREE_PASSIVE()

    @critical_macro_method
    def INIT_CRITICAL_VALUETREE_OWNED_PASSIVE(self):
        return self.INIT_VALUETREE_OWNED_PASSIVE()

    @macro_method
    def INIT_MULTI_MULTIPARENT_PASSIVE(self):
        return """
    {prefix_to}{alias_from}_suscriber.SetOwner(this);
    """

    @macro_method
    def INIT_MULTIOWNED_MULTIPARENT_PASSIVE(self):
        return """
    //multi_multiowned_multiparent_passive {alias_from} -> {alias_to} 
    {{
        assert(this);
        assert(ptr_{alias_from});
        {prefix_to}{alias_from}_suscriber.SetOwner(this);
        ptr_{alias_from}->add_{alias_to}_last(this);
    }}
    """

    @macro_method
    def INIT_MULTIOWNED_PASSIVE(self):
        return """
    //multi_owned_passive {alias_from} -> {alias_to}
    {{
        assert(this);
        assert(ptr_{alias_from});
        ptr_{alias_from}->{prefix_from}count_{alias_to}++;
    
        {prefix_to}ref_{alias_from} = ptr_{alias_from};
    
        if (ptr_{alias_from}->{prefix_from}last_{alias_to})
        {{
            {prefix_to}next_{alias_from} = nullptr;
            {prefix_to}prev_{alias_from} = ptr_{alias_from}->{prefix_from}last_{alias_to};
            {prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = this;
            ptr_{alias_from}->{prefix_from}last_{alias_to} = this;
        }}
        else
        {{
            {prefix_to}prev_{alias_from} = nullptr;
            {prefix_to}next_{alias_from} = nullptr;
            ptr_{alias_from}->{prefix_from}first_{alias_to} = ptr_{alias_from}->{prefix_from}last_{alias_to} = this;
        }}
    }}
    """

    @macro_method
    def INIT_MULTI_PASSIVE(self):
        return """
//multi_passive {alias_from} -> {alias_to}
{prefix_to}ref_{alias_from} = nullptr;
{prefix_to}prev_{alias_from} = nullptr;
{prefix_to}next_{alias_from} = nullptr;
"""

    @macro_method
    def INIT_SINGLE_OWNED_PASSIVE(self):
        return """
//single_owned_passive {alias_from} -> {alias_to}
assert(this);
assert(ptr_{alias_from});
assert(ptr_{alias_from}->{prefix_from}ref_{alias_to} == nullptr);

{prefix_to}ref_{alias_from} = ptr_{alias_from};
ptr_{alias_from}->{prefix_from}ref_{alias_to} = this;
"""

    @macro_method
    def INIT_SINGLE_PASSIVE(self):
        return """
//single_passive {alias_from} -> {alias_to}
{prefix_to}ref_{alias_from} = nullptr;
"""

    @macro_method
    def INIT_STATIC_MULTIOWNED_PASSIVE(self):
        return """
//static multi_owned_passive {alias_from} -> {alias_to}
assert(this);
{from_type}::{prefix_from}count_{alias_to}++;

if ({from_type}::{prefix_from}last_{alias_to})
{{
    {prefix_to}next_{alias_from} = nullptr;
    {prefix_to}prev_{alias_from} = {from_type}::{prefix_from}last_{alias_to};
    {prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = this;
    {from_type}::{prefix_from}last_{alias_to} = this;
}}
else
{{
    {prefix_to}prev_{alias_from} = nullptr;
    {prefix_to}next_{alias_from} = nullptr;
    {from_type}::{prefix_from}first_{alias_to} = {from_type}::{prefix_from}last_{alias_to} = this;
}}
"""

    @macro_method
    def INIT_STATIC_MULTI_PASSIVE(self):
        return """
//static multi_passive {alias_from} -> {alias_to}
assert(this);
{prefix_to}prev_{alias_from} = nullptr;
{prefix_to}next_{alias_from} = nullptr;
"""

    @macro_method
    def INIT_UNIQUEVALUETREE_OWNED_PASSIVE(self):
        return """
//uniquevaluetree_owned_passive {alias_from} -> {alias_to}
assert(this);
{prefix_to}ref_{alias_from} = nullptr;
{prefix_to}parent_{alias_from} = nullptr;
{prefix_to}left_{alias_from} = nullptr;
{prefix_to}right_{alias_from} = nullptr;
assert(ptr_{alias_from});
ptr_{alias_from}->add_{alias_to}(this);
"""

    @macro_method
    def INIT_UNIQUEVALUETREE_PASSIVE(self):
        return """
//valuetree_passive {alias_from} -> {alias_to}
{prefix_to}ref_{alias_from} = nullptr;
{prefix_to}parent_{alias_from} = nullptr;
{prefix_to}left_{alias_from} = nullptr;
{prefix_to}right_{alias_from} = nullptr;
"""

    @macro_method
    def INIT_VALUETREE_OWNED_PASSIVE(self):
        return """
//valuetree_owned_passive {alias_from} -> {alias_to}
{prefix_to}ref_{alias_from} = nullptr;
{prefix_to}parent_{alias_from} = nullptr;
{prefix_to}left_{alias_from} = nullptr;
{prefix_to}right_{alias_from} = nullptr;
{prefix_to}sibling_{alias_from} = nullptr;
assert(ptr_{alias_from});
ptr_{alias_from}->add_{alias_to}(this);
"""

    @macro_method
    def INIT_VALUETREE_PASSIVE(self):
        return """
//valuetree_passive {alias_from} -> {alias_to}
{prefix_to}ref_{alias_from} = nullptr;
{prefix_to}parent_{alias_from} = nullptr;
{prefix_to}left_{alias_from} = nullptr;
{prefix_to}right_{alias_from} = nullptr;
{prefix_to}sibling_{alias_from} = nullptr;
"""

    @property
    def INIT_CODE(self):
        """Returns code"""
        if self._key.is_critical:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.INIT_CRITICAL_SINGLE_OWNED_PASSIVE()
                else:
                    return self.INIT_CRITICAL_SINGLE_PASSIVE()
            else:
                if self._key.implementation == 'standard':
                    if self._key.is_aggregate:
                        if self._key.is_static:
                            return self.INIT_CRITICAL_STATIC_MULTIOWNED_PASSIVE()
                        else:
                            return self.INIT_CRITICAL_MULTIOWNED_PASSIVE()
                    else:
                        if self._key.is_static:
                            return self.INIT_CRITICAL_STATIC_MULTI_PASSIVE()
                        else:
                            return self.INIT_CRITICAL_MULTI_PASSIVE()
                elif self._key.implementation == 'value_tree':
                    if self._key.is_unique:
                        if self._key.is_aggregate:
                            return self.INIT_CRITICAL_UNIQUEVALUETREE_OWNED_PASSIVE()
                        else:
                            return self.INIT_CRITICAL_UNIQUEVALUETREE_PASSIVE()
                    else:
                        if self._key.is_aggregate:
                            return self.INIT_CRITICAL_VALUETREE_OWNED_PASSIVE()
                        else:
                            return self.INIT_CRITICAL_VALUETREE_PASSIVE()
                else:
                    if self._key.is_aggregate:
                        return self.INIT_CRITICAL_AVLTREE_OWNED_PASSIVE()
                    else:
                        return self.INIT_CRITICAL_AVLTREE_PASSIVE()
        else:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.INIT_SINGLE_OWNED_PASSIVE()
                else:
                    return self.INIT_SINGLE_PASSIVE()
            else:
                if self._key.implementation == 'standard':
                    if self._key.is_aggregate:
                        if self._key.is_static:
                            return self.INIT_STATIC_MULTIOWNED_PASSIVE()
                        else:
                            return self.INIT_MULTIOWNED_PASSIVE()
                    else:
                        if self._key.is_static:
                            return self.INIT_STATIC_MULTI_PASSIVE()
                        else:
                            return self.INIT_MULTI_PASSIVE()
                elif self._key.implementation == 'value_tree':
                    if self._key.is_unique:
                        if self._key.is_aggregate:
                            return self.INIT_UNIQUEVALUETREE_OWNED_PASSIVE()
                        else:
                            return self.INIT_UNIQUEVALUETREE_PASSIVE()
                    else:
                        if self._key.is_aggregate:
                            return self.INIT_VALUETREE_OWNED_PASSIVE()
                        else:
                            return self.INIT_VALUETREE_PASSIVE()
                else:
                    if self._key.is_aggregate:
                        return self.INIT_AVLTREE_OWNED_PASSIVE()
                    else:
                        return self.INIT_AVLTREE_PASSIVE()

    def RELATION_AVLTREE_OWNED_PASSIVE(self):
        self.RELATION_AVLTREE_PASSIVE(True)

    def RELATION_AVLTREE_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* {prefix_to}parent_{alias_from};
        #     {to}* _left_{alias_from};
        #     {to}* _right_{alias_from};
        #     int _bal_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.REQUIRED_FROM_FRIENDSHIP()
        parent_ptr = MemberData(
            access='private',
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('ref_{alias_from}'),
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        if owned:
            GetterMethod(parent=parent_ptr, access='public', static=False, visibleInTree=not HIDE_TREE,
                         read_only=self.read_only)
        MemberData(
            access='private',
            type=self.type(self.inner_class, ptr=True),
            name=self.format('parent_{alias_from}'),
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            access='private',
            type=self.type(self.inner_class, ptr=True),
            name=self.format('left_{alias_from}'),
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            access='private',
            type=self.type(self.inner_class, ptr=True),
            name=self.format('right_{alias_from}'),
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            access='private',
            type=self.type('int'),
            name=self.format('bal_{alias_from}'),
            default='0',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberMethod(
            access='public',
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('get_{alias_from}'),
            constmethod=True,
            content=self.format('return  {prefix_to}ref_{alias_from};'),
            parent=self,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_AVLTREE_OWNED_PASSIVE(self):
        self.RELATION_CRITICAL_AVLTREE_PASSIVE(True)

    def RELATION_CRITICAL_AVLTREE_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* {prefix_to}parent_{alias_from};
        #     {to}* _left_{alias_from};
        #     {to}* _right_{alias_from};
        #     int _bal_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.RELATION_AVLTREE_PASSIVE(owned)

    def RELATION_CRITICAL_MULTI_MULTIPARENT_PASSIVE(self):
        #     friend class {from};
        # public:
        #     MultiRelation {alias_from}_suscriber;
        # public:
        #     inline size_t get_{alias_from}_count() const;
        #     inline {from}* get_first_{alias_from}() const;
        #     inline {from}* get_next_{alias_from}({from}* ptr_{alias_from}) const;
        #     inline {from}* get_prev_{alias_from}({from}* ptr_{alias_from}) const;
        #     inline {from}* get_last_{alias_from}() const;
        #     ITERATOR_MULTI_MULTIPARENT_PASSIVE({from},{alias_from},{to},{alias_to})
        pass

    def RELATION_CRITICAL_MULTIOWNED_MULTIPARENT_PASSIVE(self):
        self.RELATION_CRITICAL_MULTI_MULTIPARENT_PASSIVE()

    def RELATION_CRITICAL_MULTIOWNED_PASSIVE(self):
        self.RELATION_CRITICAL_MULTI_PASSIVE(True)

    def RELATION_CRITICAL_MULTI_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* _prev_{alias_from};
        #     {to}* _next_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.REQUIRED_FROM_FRIENDSHIP()
        parent_ptr = MemberData(
            access='private',
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('ref_{alias_from}'),
            default='nullptr',
            parent=self,
            read_only=self.read_only
            )
        if owned:
            GetterMethod(parent=parent_ptr, access='public', static=False, visibleInTree=not HIDE_TREE,
                         read_only=self.read_only)
        MemberData(
            access='private',
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('prev_{alias_from}'),
            default='nullptr',
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            access='private',
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('next_{alias_from}'),
            default='nullptr',
            parent=self,
            read_only=self.read_only
            )
        MemberMethod(
            access='public',
            inline=True,
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('get_{alias_from}'),
            content=self.format('return {prefix_to}ref_{alias_from};'),
            constmethod='True',
            parent=self,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_SINGLE_OWNED_PASSIVE(self):
        self.RELATION_CRITICAL_SINGLE_PASSIVE(True)

    def RELATION_CRITICAL_SINGLE_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.REQUIRED_FROM_FRIENDSHIP()
        parent_ptr = MemberData(
            access='private',
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('ref_{alias_from}'),
            default='nullptr',
            parent=self
        )
        if owned:
            GetterMethod(parent=parent_ptr, access='public', static=False, visibleInTree=not HIDE_TREE)
        MemberMethod(
            access='public',
            inline=True,
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('get_{alias_from}'),
            content=self.format('return {prefix_to}ref_{alias_from};'),
            constmethod='True',
            parent=self,
            read_only=self.read_only
            )
        self.REQUIRED_FROM_FRIENDSHIP()

    def RELATION_CRITICAL_STATIC_MULTIOWNED_PASSIVE(self):
        self. RELATION_CRITICAL_STATIC_MULTI_PASSIVE()

    def RELATION_CRITICAL_STATIC_MULTI_PASSIVE(self):
        # public:
        #     {to}* _prev_{alias_from};
        #     {to}* _next_{alias_from};
        self.RELATION_STATIC_MULTI_PASSIVE()

    def RELATION_CRITICAL_UNIQUEVALUETREE_OWNED_PASSIVE(self):
        self.RELATION_CRITICAL_UNIQUEVALUETREE_PASSIVE(True)

    def RELATION_CRITICAL_UNIQUEVALUETREE_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* {prefix_to}parent_{alias_from};
        #     {to}* _left_{alias_from};
        #     {to}* _right_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.REQUIRED_FROM_FRIENDSHIP()
        self.RELATION_UNIQUEVALUETREE_PASSIVE(owned)

    def RELATION_CRITICAL_VALUETREE_OWNED_PASSIVE(self):
        self.RELATION_CRITICAL_VALUETREE_PASSIVE(True)

    def RELATION_CRITICAL_VALUETREE_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* {prefix_to}parent_{alias_from};
        #     {to}* _left_{alias_from};
        #     {to}* _right_{alias_from};
        #     {to}* _sibling_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.RELATION_VALUETREE_PASSIVE(owned)

    def RELATION_MULTI_MULTIPARENT_PASSIVE(self):
        # public:
        #     friend class {from};
        #     MultiRelation {alias_from}_suscriber;
        #
        # public:
        #     inline size_t get_{alias_from}_count() const
        #     {
        #         return {alias_from}_suscriber.GetObjectsCount();
        #     };
        #     inline {from}* GetFirst{alias_from}() const
        #     {
        #         return ({from}*){alias_from}_suscriber.GetFirstObject();
        #     };
        #     inline {from}* get_next_{alias_from}({from}* ptr_{alias_from}) const
        #     {
        #         return ({from}*){alias_from}_suscriber.GetNextObject(ptr_{alias_from});
        #     };
        #     inline {from}* get_prev_{alias_from}({from}* ptr_{alias_from}) const
        #     {
        #         return ({from}*){alias_from}_suscriber.GetPrevObject(ptr_{alias_from});
        #     };
        #     inline {from}* get_last_{alias_from}() const
        #     {
        #         return ({from}*){alias_from}_suscriber.GetLastObject();
        #     };
        #     ITERATOR_MULTI_MULTIPARENT_PASSIVE({from}, {alias_from}, {to}, {alias_to})
        pass

    def RELATION_MULTIOWNED_MULTIPARENT_PASSIVE(self):
        self.RELATION_MULTI_MULTIPARENT_PASSIVE()

    def RELATION_MULTIOWNED_PASSIVE(self):
        self.RELATION_MULTI_PASSIVE(True)

    def RELATION_MULTI_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* _prev_{alias_from};
        #     {to}* _next_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.REQUIRED_FROM_FRIENDSHIP()
        parent_ptr = MemberData(
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('ref_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        # if the relation is owned, we can add a getter so this could
        # help create avl references based on relations
        if owned:
            GetterMethod(parent=parent_ptr, access='public', static=False, visibleInTree=not HIDE_TREE,
                         read_only=self.read_only)
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('prev_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('next_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberMethod(
            type=self.type(self._key.from_class, ptr=True),
            constmethod=True,
            inline=True,
            name=self.format('get_{alias_from}'),
            access='public',
            content=self.RELATION_SINGLE_PASSIVE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    def RELATION_SINGLE_OWNED_PASSIVE(self):
        self.RELATION_SINGLE_PASSIVE(True)

    def RELATION_SINGLE_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        """Create the weak part of single relation.
         It's required to have friendship to from class
         """
        self.REQUIRED_FROM_FRIENDSHIP()
        parent_ptr = MemberData(
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('ref_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        if owned:
            GetterMethod(parent=parent_ptr, access='public', static=False, visibleInTree=not HIDE_TREE,
                         read_only=self.read_only)
        MemberMethod(
            type=self.type(self._key.from_class, ptr=True),
            constmethod=True,
            inline=True,
            name=self.format('get_{alias_from}'),
            access='public',
            content=self.RELATION_SINGLE_PASSIVE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    @macro_method
    def RELATION_SINGLE_PASSIVE_CONTENT(self):
        return "return {prefix_to}ref_{alias_from};"

    def RELATION_STATIC_MULTIOWNED_PASSIVE(self):
        self.RELATION_STATIC_MULTI_PASSIVE()

    def RELATION_STATIC_MULTI_PASSIVE(self):
        # public:
        #     {to}* _prev_{alias_from};
        #     {to}* _next_{alias_from};
        self.REQUIRED_FROM_FRIENDSHIP()
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('prev_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('next_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_UNIQUEVALUETREE_PASSIVE(self, owned=False):
        # private:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* {prefix_to}parent_{alias_from};
        #     {to}* _left_{alias_from};
        #     {to}* _right_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.REQUIRED_FROM_FRIENDSHIP()
        parent_ptr = MemberData(
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('ref_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=True,
            read_only=self.read_only
            )
        if owned:
            GetterMethod(parent=parent_ptr, access='public', static=False, visibleInTree=not HIDE_TREE,
                         read_only=self.read_only)
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('parent_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=True,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('left_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('right_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberMethod(
            type=self.type(self._key.from_class, ptr=True),
            constmethod=True,
            inline=True,
            name=self.format('get_{alias_from}'),
            access='public',
            content=self.format('return {prefix_to}ref_{alias_from};'),
            parent=self,
            read_only=self.read_only
        )

    def RELATION_UNIQUEVALUETREE_OWNED_PASSIVE(self):
        self.RELATION_UNIQUEVALUETREE_PASSIVE(True)

    def RELATION_VALUETREE_OWNED_PASSIVE(self):
        self.RELATION_VALUETREE_PASSIVE(True)

    def RELATION_VALUETREE_PASSIVE(self, owned=False):
        # public:
        #     {from}* {prefix_to}ref_{alias_from};
        #     {to}* {prefix_to}parent_{alias_from};
        #     {to}* _left_{alias_from};
        #     {to}* _right_{alias_from};
        #     {to}* _sibling_{alias_from};
        #
        # public:
        #     {from}* get_{alias_from}() const { return {prefix_to}ref_{alias_from}; };
        self.REQUIRED_FROM_FRIENDSHIP()
        parent_ptr = MemberData(
            type=self.type(self._key.from_class, ptr=True),
            name=self.format('ref_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        if owned:
            GetterMethod(parent=parent_ptr, access='public', static=False, visibleInTree=not HIDE_TREE,
                         read_only=self.read_only)
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('parent_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('left_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('right_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self.inner_class, ptr=True),
            name=self.format('sibling_{alias_from}'),
            access='private',
            default='nullptr',
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberMethod(
            type=self.type(self._key.from_class, ptr=True),
            constmethod=True,
            inline=True,
            name=self.format('get_{alias_from}'),
            access='private',
            content=self.format('return {prefix_to}ref_{alias_from};'),
            parent=self,
            read_only=self.read_only
        )

    def REQUIRED_FROM_FRIENDSHIP(self):
        if self._key.from_class is not self.inner_class:
            if self._key.from_class not in [x._target for x in self.inner_class.friends]:
                Friendship(
                    target=self._key.from_class,
                    parent=self.inner_class,
                    read_only=self.read_only,
                    visibleInTree=False
                )

    def save_state(self):
        """Internal save state"""
        super(RelationFrom, self).save_state()


class RelationTo(CCTComponent):
    """Implements the 'from' side."""

    @TransactionalMethod('move relation target {0}')
    def drop(self, to):
        """Drops data member inside project or another folder """
        target = to.relation_container and to
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initializes the relation to 'to' inside 'from'"""
        self._key = kwargs['key']
        super(RelationTo, self).__init__(**kwargs)

    @property
    def tree_label(self):
        return self._key.to_alias

    @property
    def key(self):
        return self._key

    @property
    def note(self):
        return self._key.note

    @note.setter
    def note(self, value):
        self._key.note = value

    def format(self, format_str):
        """Return formatted text with macro replacements"""
        return format_str.format(**self._key.macro_dict)

    def type(self, ref_type, **kwargs):
        """Quick access to type instances from type name or base type instance"""
        if type(ref_type) is str:
            ref_type = cached_type(self.project, ref_type)
        if ref_type.is_template:
            return typeinst(type=ref_type, type_args=ref_type.template_args, **kwargs)
        else:
            return typeinst(type=ref_type, **kwargs)

    @macro_method
    def CRITICAL_LOCK(self):
        return """
    critical_section_lock lock({prefix_from}relation_critical_section_{alias_to});
    """

    def CRITICAL_STATIC_LOCK(self):
        return self.CRITICAL_LOCK()

    @macro_method
    def CRITICAL_ITERATOR_LOCK(self):
        return """
    critical_section_lock lock({prefix_from}iter_{alias_from}->get_relation_critical_section_{alias_to}());
    """

    @macro_method
    def CRITICAL_STATIC_ITERATOR_LOCK(self):
        return """
    critical_section_lock lock({from}::get_relation_critical_section_{alias_to}());
    """

    @property
    def EXIT_CODE(self):
        """Returns exit code"""
        if self._key.is_critical:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.EXIT_CRITICAL_SINGLE_OWNED_ACTIVE()
                else:
                    return self.EXIT_CRITICAL_SINGLE_ACTIVE()
            else:
                if self._key.is_aggregate:
                    if self._key.is_static:
                        return self.EXIT_CRITICAL_STATIC_MULTIOWNED_ACTIVE()
                    else:
                        if self._key.implementation == 'standard':
                            return self.EXIT_CRITICAL_MULTIOWNED_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE()
                            else:
                                return self.EXIT_CRITICAL_VALUETREE_OWNED_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE()
                            else:
                                return self.EXIT_CRITICAL_VALUETREE_OWNED_ACTIVE()
                        else:  # avl-tree
                            return self.EXIT_CRITICAL_AVLTREE_OWNED_ACTIVE()
                else:
                    if self._key.is_static:
                        return self.EXIT_CRITICAL_STATIC_MULTI_ACTIVE()
                    else:
                        if self._key.implementation == 'standard':
                            return self.EXIT_CRITICAL_MULTI_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_CRITICAL_UNIQUEVALUETREE_ACTIVE()
                            else:
                                return self.EXIT_CRITICAL_VALUETREE_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_CRITICAL_UNIQUEVALUETREE_ACTIVE()
                            else:
                                return self.EXIT_CRITICAL_VALUETREE_ACTIVE()
                        else:  # avl-tree
                            return self.EXIT_CRITICAL_AVLTREE_ACTIVE()
        else:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.EXIT_SINGLE_OWNED_ACTIVE()
                else:
                    return self.EXIT_SINGLE_ACTIVE()
            else:
                if self._key.is_aggregate:
                    if self._key.is_static:
                        return self.EXIT_STATIC_MULTIOWNED_ACTIVE()
                    else:
                        if self._key.implementation == 'standard':
                            return self.EXIT_MULTIOWNED_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_UNIQUEVALUETREE_OWNED_ACTIVE()
                            else:
                                return self.EXIT_VALUETREE_OWNED_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_UNIQUEVALUETREE_OWNED_ACTIVE()
                            else:
                                return self.EXIT_VALUETREE_OWNED_ACTIVE()
                        else:  # avl-tree
                            return self.EXIT_AVLTREE_OWNED_ACTIVE()
                else:
                    if self._key.is_static:
                        return self.EXIT_STATIC_MULTI_ACTIVE()
                    else:
                        if self._key.implementation == 'standard':
                            return self.EXIT_MULTI_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_UNIQUEVALUETREE_ACTIVE()
                            else:
                                return self.EXIT_VALUETREE_ACTIVE()
                        elif self._key.implementation == 'value_tree':
                            if self._key.is_unique:
                                return self.EXIT_UNIQUEVALUETREE_ACTIVE()
                            else:
                                return self.EXIT_VALUETREE_ACTIVE()
                        else:  # avl-tree
                            return self.EXIT_AVLTREE_ACTIVE()

    @macro_method
    def EXIT_AVLTREE_ACTIVE(self):
        return """
    while ({prefix_from}top_{alias_to})
    {{
        remove_{alias_to}({prefix_from}top_{alias_to});
    }}
    """

    @macro_method
    def EXIT_AVLTREE_OWNED_ACTIVE(self):
        return """
    while ({prefix_from}top_{alias_to})
    {{
        delete {prefix_from}top_{alias_to};
    }}
    """

    @critical_macro_method
    def EXIT_CRITICAL_AVLTREE_ACTIVE(self):
        return self.EXIT_AVLTREE_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_AVLTREE_OWNED_ACTIVE(self):
        return self.EXIT_AVLTREE_OWNED_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_MULTI_ACTIVE(self):
        return self.EXIT_MULTI_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_MULTIOWNED_ACTIVE(self):
        return self.EXIT_MULTIOWNED_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_MULTIOWNED_MULTIPARENT_ACTIVE(self):
        return self.EXIT_MULTIOWNED_MULTIPARENT_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_SINGLE_ACTIVE(self):
        return self.EXIT_SINGLE_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_SINGLE_OWNED_ACTIVE(self):
        return self.EXIT_SINGLE_OWNED_ACTIVE()

    def EXIT_CRITICAL_STATIC_MULTI_ACTIVE(self):
        return ''

    def EXIT_CRITICAL_STATIC_MULTIOWNED_ACTIVE(self):
        return ''

    @critical_macro_method
    def EXIT_CRITICAL_UNIQUEVALUETREE_ACTIVE(self):
        return self.EXIT_UNIQUEVALUETREE_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        return self.EXIT_UNIQUEVALUETREE_OWNED_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_VALUETREE_ACTIVE(self):
        return self.EXIT_VALUETREE_ACTIVE()

    @critical_macro_method
    def EXIT_CRITICAL_VALUETREE_OWNED_ACTIVE(self):
        return self.EXIT_VALUETREE_OWNED_ACTIVE()

    @macro_method
    def EXIT_MULTI_ACTIVE(self):
        return """
    {{
        for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
        {{
            remove_{alias_to}(item);
        }}
    }}
    """

    @macro_method
    def EXIT_MULTI_MULTIPARENT_ACTIVE(self):
        # TO DO
        raise RuntimeError("not yet implemented.")

    @macro_method
    def EXIT_MULTIOWNED_ACTIVE(self):
        return """
    {{
        for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
        {{
            delete item;
        }}
    }}
    """

    def EXIT_MULTIOWNED_MULTIPARENT_ACTIVE(self):
        return self.EXIT_MULTI_MULTIPARENT_ACTIVE()

    @macro_method
    def EXIT_SINGLE_OWNED_MULTIPARENT_ACTIVE(self):
        return """
    {{
        size_t nItem={alias_to}Subscriptor.GetObjectsCount();
        {to_type}* ptr;
        while(nItem--)
        {{
            ptr=({to_type}*){alias_to}Subscriptor.GetObject(nItem);
            {alias_to}Subscriptor.RemoveObject(&ptr->{alias_from}_suscriber);
        }}
    }}
    """

    @macro_method
    def EXIT_SINGLE_ACTIVE(self):
        return """
    if (_ref_{alias_to})
    {{
        clear_{alias_to}(_ref_{alias_to});
    }}
    """

    @macro_method
    def EXIT_SINGLE_OWNED_ACTIVE(self):
        return """
    if (_ref_{alias_to})
    {{
        delete _ref_{alias_to};
    }}
    """

    def EXIT_STATIC_MULTI_ACTIVE(self):
        return ''

    def EXIT_STATIC_MULTIOWNED_ACTIVE(self):
        return ''

    @macro_method
    def EXIT_UNIQUEVALUETREE_ACTIVE(self):
        return """
    {{
        for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
        {{
            remove_{alias_to}(item);
        }} 
    }}
    """

    @macro_method
    def EXIT_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        return """
    {{ 
        for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
        {{
            delete item; 
        }}
    }}
    """

    @macro_method
    def EXIT_VALUETREE_ACTIVE(self):
        return """
    {{
        for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
        {{
            remove_{alias_to}(item);
        }}
    }}
    """

    @macro_method
    def EXIT_VALUETREE_OWNED_ACTIVE(self):
        return """
    {{
        for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
        {{
            delete item;
        }}
    }}
    """

    @macro_method
    def INIT_AVLTREE_ACTIVE(self):
        return """
    {prefix_from}top_{alias_to} = nullptr;
    {prefix_from}count_{alias_to} = 0;"""

    def INIT_AVLTREE_OWNED_ACTIVE(self):
        return self.INIT_AVLTREE_ACTIVE()

    def INIT_CRITICAL_AVLTREE_ACTIVE(self):
        lock = self.CRITICAL_LOCK()
        code = self.INIT_AVLTREE_ACTIVE();
        return """{lock}{code}""".format(lock=lock,code=code)

    def INIT_CRITICAL_AVLTREE_OWNED_ACTIVE(self):
        lock = self.CRITICAL_LOCK()
        code = self.INIT_AVLTREE_OWNED_ACTIVE();
        return """{lock}{code}""".format(lock=lock,code=code)

    def INIT_CRITICAL_MULTI_ACTIVE(self):
        lock = self.CRITICAL_LOCK()
        code = self.INIT_MULTI_ACTIVE();
        return """{lock}{code}""".format(lock=lock,code=code)

    def INIT_CRITICAL_MULTI_MULTIPARENT_ACTIVE(self):
        lock = self.CRITICAL_LOCK()
        code = self.INIT_MULTI_MULTIPARENT_ACTIVE();
        return """{lock}{code}""".format(lock=lock,code=code)

    def INIT_CRITICAL_MULTIOWNED_ACTIVE(self):
        lock = self.CRITICAL_LOCK()
        code = self.INIT_MULTIOWNED_ACTIVE();
        return """{lock}{code}""".format(lock=lock,code=code)

    def INIT_CRITICAL_MULTIOWNED_MULTIPARENT_ACTIVE(self):
        return self.INIT_CRITICAL_MULTI_MULTIPARENT_ACTIVE()

    def INIT_CRITICAL_SINGLE_ACTIVE(self):
        lock = self.CRITICAL_LOCK()
        code = self.INIT_SINGLE_ACTIVE();
        return """{lock}{code}""".format(lock=lock,code=code)

    @critical_macro_method
    def INIT_CRITICAL_SINGLE_OWNED_ACTIVE(self):
        return self.INIT_SINGLE_OWNED_ACTIVE()

    def INIT_CRITICAL_STATIC_MULTI_ACTIVE(self):
        return ''

    def INIT_CRITICAL_STATIC_MULTIOWNED_ACTIVE(self):
        return ''

    @critical_macro_method
    def INIT_CRITICAL_UNIQUEVALUETREE_ACTIVE(self):
        return self.INIT_UNIQUEVALUETREE_ACTIVE()

    @critical_macro_method
    def INIT_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        return self.INIT_UNIQUEVALUETREE_OWNED_ACTIVE()

    @critical_macro_method
    def INIT_CRITICAL_VALUETREE_ACTIVE(self):
        return self.INIT_VALUETREE_ACTIVE()

    @critical_macro_method
    def INIT_CRITICAL_VALUETREE_OWNED_ACTIVE(self):
        return self.INIT_VALUETREE_OWNED_ACTIVE()

    @macro_method
    def INIT_MULTI_ACTIVE(self):
        return """
    {prefix_from}first_{alias_to} = nullptr;
    {prefix_from}last_{alias_to} = nullptr;
    {prefix_from}count_{alias_to} = 0;
    """

    @macro_method
    def INIT_MULTI_MULTIPARENT_ACTIVE(self):
        return """
    {prefix_from}relation_{alias_to}_subscriptor.set_owner(this);
    {prefix_from}relation_{alias_to}_subscriptor.set_delete_method(delete_method({to_type}::static_{to}_dtor));
    """

    def INIT_MULTIOWNED_ACTIVE(self):
        return self.INIT_MULTI_ACTIVE()

    def INIT_MULTIOWNED_MULTIPARENT_ACTIVE(self):
        return self.INIT_MULTI_MULTIPARENT_ACTIVE()

    @macro_method
    def INIT_SINGLE_ACTIVE(self):
        return """
    {prefix_from}ref_{alias_to} =  nullptr;"""

    def INIT_SINGLE_OWNED_ACTIVE(self):
        return self.INIT_SINGLE_ACTIVE()

    def INIT_STATIC_MULTI_ACTIVE(self):
        return ''

    def INIT_STATIC_MULTIOWNED_ACTIVE(self):
        return ''

    @macro_method
    def INIT_UNIQUEVALUETREE_ACTIVE(self):
        return """
    {prefix_from}first_{alias_to} = nullptr;
    {prefix_from}count_{alias_to} = 0;"""

    def INIT_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        return self.INIT_UNIQUEVALUETREE_ACTIVE()

    @macro_method
    def INIT_VALUETREE_ACTIVE(self):
        return """
    {prefix_from}first_{alias_to} = nullptr;
    {prefix_from}count_{alias_to} = 0;"""

    def INIT_VALUETREE_OWNED_ACTIVE(self):
        return self.INIT_VALUETREE_ACTIVE()

    def ITERATOR_MULTI_ACTIVE(self):
        #     class {alias_to}_iterator
        #     {
        #         friend class {from};
        #     private:
        #         {to}* _ref_{alias_to};
        #         {to}* _prev_{alias_to};
        #         {to}* _next_{alias_to};
        #         const {from}* _iter_{alias_from};
        #
        #         {alias_to}_iterator* _prev;
        #         {alias_to}_iterator* _next;
        #
        #         int ({to}::*_method)() const;
        #
        #
        #     public:
        #         {alias_to}_iterator(const {from}* iter_{alias_from},
        #                          int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = 0);
        #         {alias_to}_iterator(const {from}& iter_{alias_from},
        #                          int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = 0);
        #         {alias_to}_iterator(const {alias_to}_iterator& iterator__,
        #                          int ({to}::*method)() const = 0);
        #         ~{alias_to}_iterator();
        #
        #         {alias_to}_iterator& operator= (const {alias_to}_iterator& iterator__)
        #         {
        #             _iter_{alias_from} = iterator__._iter_{alias_from};
        #             _ref_{alias_to} = iterator__._ref_{alias_to};
        #             _prev_{alias_to} = iterator__._prev_{alias_to};
        #             _next_{alias_to} = iterator__._next_{alias_to};
        #             _method = iterator__._method;
        #             return *this;
        #         }
        #         {to}* operator++ ()
        #         {
        #             _next_{alias_to} = _iter_{alias_from}->get_next_{alias_to}(_next_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_next_{alias_to} && !(_next_{alias_to}->*_method)())
        #                     _next_{alias_to} = _iter_{alias_from}->get_next_{alias_to}(_next_{alias_to});
        #             }
        #             _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         {to}* operator-- ()
        #         {
        #             _prev_{alias_to} = _iter_{alias_from}->get_prev_{alias_to}(_prev_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_prev_{alias_to} && !(_prev_{alias_to}->*_method)())
        #                     _prev_{alias_to} = _iter_{alias_from}->get_prev_{alias_to}(_prev_{alias_to});
        #             }
        #             _ref_{alias_to} = _next_{alias_to} = _prev_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         operator {to}*() { return _ref_{alias_to}; }
        #         {to}* operator-> () { return _ref_{alias_to}; }
        #         {to}* get() { return _ref_{alias_to}; }
        #         void reset() { _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ({to}*)0; }
        #
        #         int is_last() { return (_iter_{alias_from}->get_last_{alias_to}() == _ref_{alias_to}); }
        #         int is_first() { return (_iter_{alias_from}->get_first_{alias_to}() == _ref_{alias_to}); }
        #     protected:
        #         void check({to}* item_{alias_to});
        #         void check({to}* item_{alias_to}, {to}* new_item_{alias_to});
        #     };
        from ._Class import Class
        iterator = Class(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            default='nullptr',
            name=self.format('{alias_to}_iterator'),
            prefix=self.inner_class.member_prefix,
            parent=self,
            read_only=self.read_only
            )
        self.REQUIRED_FRIENDSHIP(self._key.from_class, iterator)
        #         {to}* _ref_{alias_to};
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {to}* prev_{alias_to};
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('prev_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {to}* next_{alias_to};
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('next_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         const {from}* _iter_{alias_from};
        MemberData(
            type=self.type(self.inner_class, ptr=True, const=True),
            access='private',
            default='nullptr',
            name=self.format('iter_{alias_from}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {alias_to}_iterator* _prev;
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='prev',
            parent =iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {alias_to}_iterator* _next;
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='next',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         bool ({to}::*_method)() const;
        MemberData(
            type=typeinst(
                type=cached_type(self.project, "bool"),
                funptr=True,
                const_fn=True,
                class_fn=self._key.to_class),
            access='private',
            default='nullptr',
            name='method',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_ITERATOR_MULTI_ACTIVE(iterator)
        return iterator

    def ITERATOR_CRITICAL_MULTI_ACTIVE(self):
        #     class {alias_to}_iterator
        #     {
        #     private:
        #         {to}* _ref_{alias_to};
        #         {to}* _prev_{alias_to};
        #         {to}* _next_{alias_to};
        #         const {from}* _iter_{alias_from};
        #
        #         {alias_to}_iterator* _prev;
        #         {alias_to}_iterator* _next;
        #
        #         int ({to}::*_method)() const;
        #
        #    protected:
        #
        #         void {alias_to}__init__(const {from}* iter_{alias_from},
        #                          int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = 0);
        #         void __exit__();
        #
        #     public:
        #         inline {alias_to}_iterator(const {from}* iter_{alias_from},
        #                          int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = 0);
        #         inline {alias_to}_iterator(const {from}& iter_{alias_from},
        #                          int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = 0);
        #         inline {alias_to}_iterator(const {alias_to}_iterator& iterator__,
        #                          int ({to}::*method)() const = 0);
        #         ~{alias_to}_iterator();
        #
        #         inline {alias_to}_iterator& operator= (const {alias_to}_iterator& iterator__);
        #         {to}* operator++ ()
        #         {
        #             _next_{alias_to} = _iter_{alias_from}->get_next_{alias_to}(_next_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_next_{alias_to} && !(_next_{alias_to}->*_method)())
        #                     _next_{alias_to} = _iter_{alias_from}->get_next_{alias_to}(_next_{alias_to});
        #             }
        #             _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         {to}* operator-- ()
        #         {
        #             _prev_{alias_to} = _iter_{alias_from}->get_prev_{alias_to}(_prev_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_prev_{alias_to} && !(_prev_{alias_to}->*_method)())
        #                     _prev_{alias_to} = _iter_{alias_from}->get_prev_{alias_to}(_prev_{alias_to});
        #             }
        #             _ref_{alias_to} = _next_{alias_to} = _prev_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         operator {to}*() { return _ref_{alias_to}; }
        #         {to}* operator-> () { return _ref_{alias_to}; }
        #         {to}* get() { return _ref_{alias_to}; }
        #         void reset() { _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ({to}*)0; }
        #
        #         int is_last() { return (_iter_{alias_from}->get_last_{alias_to}() == _ref_{alias_to}); }
        #         int is_first() { return (_iter_{alias_from}->get_first_{alias_to}() == _ref_{alias_to}); }
        #     protected:
        #         void check({to}* item_{alias_to});
        #         void check({to}* item_{alias_to}, {to}* new_item_{alias_to});
        #     };
        from ._Class import Class
        iterator = Class(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            default='nullptr',
            name=self.format('{alias_to}_iterator'),
            prefix=self.inner_class.member_prefix,
            parent=self,
            read_only=self.read_only
            )
        #         {to}* _ref_{alias_to};
        self.REQUIRED_FRIENDSHIP(self._key.from_class, iterator)
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {to}* prev_{alias_to};
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('prev_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {to}* next_{alias_to};
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('next_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         const {from}* _iter_{alias_from};
        MemberData(
            type=self.type(self.inner_class, ptr=True, const=True),
            access='private',
            default='nullptr',
            name=self.format('iter_{alias_from}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {alias_to}_iterator* _prev;
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='prev',
            parent =iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         {alias_to}_iterator* _next;
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='next',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #         bool ({to}::*_method)() const;
        MemberData(
            type=typeinst(
                type=cached_type(self.project, "bool"),
                funptr=True,
                const_fn=True,
                class_fn=self._key.to_class),
            access='private',
            default='nullptr',
            name='method',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_ITERATOR_CRITICAL_MULTI_ACTIVE(iterator)
        return iterator

    def ITERATOR_STATIC_MULTI_ACTIVE(self):
        #     class {alias_to}_iterator
        #     {
        #     private:
        #         {to}* _ref_{alias_to};
        #         {to}* _prev_{alias_to};
        #         {to}* _next_{alias_to};
        #
        #         {alias_to}_iterator* _prev;
        #         {alias_to}_iterator* _next;
        #
        #         int ({to}::*_method)() const;
        #
        #     public:
        #         {alias_to}_iterator(int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = ({to}*)0);
        #         {alias_to}_iterator(const {alias_to}_iterator& iterator__,
        #                          int ({to}::*method)() const = 0);
        #         ~{alias_to}_iterator();
        #
        #         {alias_to}_iterator& operator= (const {alias_to}_iterator& iterator__)
        #         {
        #             _ref_{alias_to} = iterator__._ref_{alias_to};
        #             _prev_{alias_to} = iterator__._prev_{alias_to};
        #             _next_{alias_to} = iterator__._next_{alias_to};
        #             _method = iterator__._method;
        #             return *this;
        #         }
        #         {to}* operator++ ()
        #         {
        #             _next_{alias_to} = get_next_{alias_to}(_next_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_next_{alias_to} && !(_next_{alias_to}->*_method)())
        #                     _next_{alias_to} = get_next_{alias_to}(_next_{alias_to});
        #             }
        #             _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         {to}* operator-- ()
        #         {
        #             _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_prev_{alias_to} && !(_prev_{alias_to}->*_method)())
        #                     _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
        #             }
        #             _ref_{alias_to} = _next_{alias_to} = _prev_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         operator {to}*() { return _ref_{alias_to}; }
        #         {to}* operator-> () { return _ref_{alias_to}; }
        #         {to}* get() { return _ref_{alias_to}; }
        #         void reset() { _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ({to}*)0; }
        #
        #         int is_last() { return (get_last_{alias_to}() == _ref_{alias_to}); }
        #         int is_first() { return (get_first_{alias_to}() == _ref_{alias_to}); }
        #
        #         static void check({to}* item_{alias_to});
        #         static void check({to}* item_{alias_to}, {to}* new_item_{alias_to});
        #     };
        from ._Class import Class
        iterator = Class(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            default='nullptr',
            name=self.format('{alias_to}_iterator'),
            prefix=self.inner_class.member_prefix,
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('prev_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('next_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='prev',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='next',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=typeinst(
                type=cached_type(self.project, "bool"),
                funptr=True,
                const_fn=True,
                class_fn=self._key.to_class),
            access='private',
            default='nullptr',
            name='method',
            parent =iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_ITERATOR_STATIC_MULTI_ACTIVE(iterator)
        return iterator

    def ITERATOR_CRITICAL_STATIC_MULTI_ACTIVE(self):
        #     class {alias_to}_iterator
        #     {
        #     private:
        #         {to}* _ref_{alias_to};
        #         {to}* _prev_{alias_to};
        #         {to}* _next_{alias_to};
        #
        #         {alias_to}_iterator* _prev;
        #         {alias_to}_iterator* _next;
        #
        #         bool ({to}::*_method)() const;
        #
        #    protected:
        #
        #         void {alias_to}__init__(int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = 0);
        #         void __exit__();
        #
        #     public:
        #         {alias_to}_iterator(int ({to}::*method)() const = 0,
        #                          {to}* ref_{alias_to} = ({to}*)0);
        #         {alias_to}_iterator(const {alias_to}_iterator& iterator__,
        #                          int ({to}::*method)() const = 0);
        #         ~{alias_to}_iterator();
        #
        #         {alias_to}_iterator& operator= (const {alias_to}_iterator& iterator__)
        #         {
        #             _ref_{alias_to} = iterator__._ref_{alias_to};
        #             _prev_{alias_to} = iterator__._prev_{alias_to};
        #             _next_{alias_to} = iterator__._next_{alias_to};
        #             _method = iterator__._method;
        #             return *this;
        #         }
        #         {to}* operator++ ()
        #         {
        #             _next_{alias_to} = get_next_{alias_to}(_next_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_next_{alias_to} && !(_next_{alias_to}->*_method)())
        #                     _next_{alias_to} = get_next_{alias_to}(_next_{alias_to});
        #             }
        #             _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         {to}* operator-- ()
        #         {
        #             _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_prev_{alias_to} && !(_prev_{alias_to}->*_method)())
        #                     _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
        #             }
        #             _ref_{alias_to} = _next_{alias_to} = _prev_{alias_to};
        #             return _ref_{alias_to};
        #         }
        #         operator {to}*() { return _ref_{alias_to}; }
        #         {to}* operator-> () { return _ref_{alias_to}; }
        #         {to}* get() { return _ref_{alias_to}; }
        #         void reset() { _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ({to}*)0; }
        #
        #         int is_last() { return (get_last_{alias_to}() == _ref_{alias_to}); }
        #         int is_first() { return (get_first_{alias_to}() == _ref_{alias_to}); }
        #
        #         static void check({to}* item_{alias_to});
        #         static void check({to}* item_{alias_to}, {to}* new_item_{alias_to});
        #     };
        from ._Class import Class
        iterator = Class(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            default='nullptr',
            name=self.format('{alias_to}_iterator'),
            prefix=self.inner_class.member_prefix,
            parent=self,
            read_only=self.read_only
        )
        self.REQUIRED_FRIENDSHIP(self._key.from_class, iterator)
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('prev_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('next_{alias_to}'),
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='prev',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type(iterator, ptr=True),
            access='private',
            default='nullptr',
            name='next',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=typeinst(
                type=cached_type(self.project, "bool"),
                funptr=True,
                const_fn=True,
                class_fn=self._key.to_class),
            access='private',
            default='nullptr',
            name='method',
            parent=iterator,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        self.METHODS_ITERATOR_CRITICAL_STATIC_MULTI_ACTIVE(iterator)
        return iterator

    def RELATION_AVLTREE_ACTIVE(self):
        # private:
        #     {to}* _top_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}(); //why two names for the same??
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('top_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_AVLTREE_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_AVLTREE_OWNED_ACTIVE(self):
        # private:
        #     {to}* _top_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        # protected:
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('top_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_AVLTREE_OWNED_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_AVLTREE_ACTIVE(self):
        # private:
        #     {to}* _top_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #     mutable critical_section _critical_section_{alias_to};
        #
        # public:
        #     critical_section& critical_section_{alias_to}() { return _critical_section_{alias_to}; }
        #
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('top_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('int'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_CRITICAL_AVLTREE_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )

    def RELATION_CRITICAL_AVLTREE_OWNED_ACTIVE(self):
        # private:
        #     {to}* _top_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        #     mutable critical_section _critical_section_{alias_to};
        # public:
        #     critical_section& critical_section_{alias_to}() const { return _critical_section_{alias_to}; }
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('top_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('int'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_CRITICAL_AVLTREE_OWNED_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )

    def RELATION_CRITICAL_MULTI_ACTIVE(self):
        # private:
        #     {to}* _first_{alias_to};
        #     {to}* _last_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        #     critical_section _relation_critical_section_{alias_to};
        #
        # public:
        #     static critical_section& get_relation_critical_section_{alias_to}()
        #     {
        #       return _critical_section_{alias_to};
        #     }
        #
        #     void add_{alias_to}_first({to}* item);
        #     void add_{alias_to}_last({to}* item);
        #     void add_{alias_to}_after({to}* item, {to}* pos);
        #     void add_{alias_to}_before({to}* item, {to}* pos);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     void move_{alias_to}_first({to}* item);
        #     void move_{alias_to}_last({to}* item);
        #     void move_{alias_to}_after({to}* item, {to}* pos);
        #     void move_{alias_to}_before({to}* item, {to}* pos);
        #     void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('int'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
            )
        self.METHODS_CRITICAL_MULTI_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_MULTIOWNED_ACTIVE(self):
        # private:
        #     {to}* _first_{alias_to};
        #     {to}* _last_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        #     mutable critical_section _relation_critical_section_{alias_to};
        #
        # public:
        #     critical_section& get_relation_critical_section_{alias_to}() const { return _critical_section_{alias_to}; }
        #
        # protected:
        #     void add_{alias_to}_first({to}* item);
        #     void add_{alias_to}_last({to}* item);
        #     void add_{alias_to}_after({to}* item, {to}* pos);
        #     void add_{alias_to}_before({to}* item, {to}* pos);
        #     void remove_{alias_to}({to}* item);
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #
        # public:
        #     void delete_all_{alias_to}();
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     void move_{alias_to}_first({to}* item);
        #     void move_{alias_to}_last({to}* item);
        #     void move_{alias_to}_after({to}* item, {to}* pos);
        #     void move_{alias_to}_before({to}* item, {to}* pos);
        #     void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        #
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('int'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            access='private',
            mutable=True,
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_CRITICAL_MULTIOWNED_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_MULTIOWNED_MULTIPARENT_ACTIVE(self):
        # public:
        #     friend class {to};
        #     static critical_section _critical_section_{alias_to};
        #     static critical_section& critical_section_{alias_to}() { return _critical_section_{alias_to}; }
        #     MultiRelation {alias_to}Subscriptor;
        # protected:
        #     inline void Unlink{alias_to}({to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.UnlinkObject(ptr_{alias_to});
        #     };
        #     inline void replace_{alias_to}({to}* ptr_{alias_to}Old, {to}* ptr_{alias_to}New)
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.AddObjectAfter(ptr_{alias_to}Old,ptr_{alias_to}New);
        #         {alias_to}Subscriptor.UnlinkObject(ptr_{alias_to}Old);
        #     };
        # public:
        #     inline void add_{alias_to}_first({to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.AddObjectFirst(ptr_{alias_to});
        #         ptr_{alias_to}->{alias_from}_suscriber.AddObjectLast(this);
        #     };
        #     inline void add_{alias_to}_last({to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.AddObjectLast(ptr_{alias_to});
        #         ptr_{alias_to}->{alias_from}_suscriber.AddObjectLast(this);
        #     };
        #     inline void add_{alias_to}_after({to}* ptr_{alias_to}_position, {to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.AddObjectAfter(ptr_{alias_to}_position,ptr_{alias_to});
        #         ptr_{alias_to}->{alias_from}_suscriber.AddObjectLast(this);
        #     };
        #     inline void add_{alias_to}_before({to}* ptr_{alias_to}_position, {to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.AddObjectBefore(ptr_{alias_to}_position,ptr_{alias_to});
        #         ptr_{alias_to}->{alias_from}_suscriber.AddObjectLast(this);
        #     };
        # public:
        #     inline void remove_{alias_to}({to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.RemoveObject(&(ptr_{alias_to}->{alias_from}_suscriber));
        #     };
        #     inline void remove_all_{alias_to}()
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {to}* ptr_{alias_to}=get_last_{alias_to}();
        #         while(ptr_{alias_to})
        #         {
        #             remove_{alias_to}(ptr_{alias_to});
        #             ptr_{alias_to}=get_last_{alias_to}();
        #         }
        #     };
        #     inline void delete_all_{alias_to}()
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {to}* ptr_{alias_to}=get_last_{alias_to}();
        #         while(ptr_{alias_to})
        #         {
        #             {alias_to}Subscriptor.DeleteObject(ptr_{alias_to});
        #             ptr_{alias_to}=get_last_{alias_to}();
        #         }
        #     };
        #     inline {to}* get_first_{alias_to}() const
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         return ({to}*){alias_to}Subscriptor.GetFirstObject();
        #     };
        #     inline {to}* get_last_{alias_to}() const
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         return ({to}*){alias_to}Subscriptor.GetLastObject();
        #     };
        #     inline {to}* get_next_{alias_to}({to}* ptr_{alias_to}) const
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         return ({to}*){alias_to}Subscriptor.GetNextObject(ptr_{alias_to});
        #     };
        #     inline {to}* get_prev_{alias_to}({to}* ptr_{alias_to}) const
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         return ({to}*){alias_to}Subscriptor.GetPrevObject(ptr_{alias_to});
        #     };
        #     inline size_t get_{alias_to}_count() const
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         return (size_t){alias_to}Subscriptor.GetObjectsCount();
        #     };
        #     inline void move_{alias_to}_first({to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.MoveObjectFirst(ptr_{alias_to});
        #     };
        #     inline void move_{alias_to}_last({to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.MoveObjectLast(ptr_{alias_to});
        #     };
        #     inline void move_{alias_to}_after({to}* ptr_{alias_to}_position,{to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.MoveObjectAfter(ptr_{alias_to}_position,ptr_{alias_to});
        #     };
        #     inline void move_{alias_to}_before({to}* ptr_{alias_to}_position,{to}* ptr_{alias_to})
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         {alias_to}Subscriptor.MoveObjectBefore(ptr_{alias_to}_position,ptr_{alias_to});
        #     };
        #     inline void sort_{alias_to}(int (*compare)({to}*, {to}*))
        #     {
        #         critical_section_lock lock(_critical_section_{alias_to});
        #         typedef int (*cmp_fn)(void *,void*);
        #         {alias_to}Subscriptor.HardSort((cmp_fn)comp);
        #     };
        #     ITERATOR_MULTI_MULTIPARENT_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        pass

    def RELATION_CRITICAL_SINGLE_ACTIVE(self):
        # private:
        #     {to}* _ref_{alias_to};
        #
        #     critical_section _critical_section_{alias_to};
        #
        # public:
        #     critical_section& critical_section_{alias_to}() { return _critical_section_{alias_to}; }
        #
        # public:
        #     void set_{alias_to}({to}* item);
        #     void clear_{alias_to}({to}* item);
        #     void move_{alias_to}({to}* item);
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_{alias_to}() const { return _ref_{alias_to}; };
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_SINGLE_ACTIVE()

    def RELATION_CRITICAL_SINGLE_OWNED_ACTIVE(self):
        # private:
        #     {to}* _ref_{alias_to};
        #
        #     static critical_section _critical_section_{alias_to};
        # public:
        #     static critical_section& critical_section_{alias_to}() { return _critical_section_{alias_to}; }
        #
        # protected:
        #     void set_{alias_to}({to}* item);
        #     void clear_{alias_to}({to}* item);
        #     void move_{alias_to}({to}* item);
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #
        # public:
        #     {to}* get_{alias_to}() const {return _ref_{alias_to};};
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_CRITICAL_SINGLE_ACTIVE()

    def RELATION_CRITICAL_STATIC_MULTI_ACTIVE(self):
        # private:
        #     static {to}* _first_{alias_to};
        #     static {to}* _last_{alias_to};
        #     static int {prefix_from}count_{alias_to};
        #
        # public:
        #     static critical_section _critical_section_{alias_to};
        #
        #     static void add_{alias_to}_first({to}* item);
        #     static void add_{alias_to}_last({to}* item);
        #     static void add_{alias_to}_after({to}* item, {to}* pos);
        #     static void add_{alias_to}_before({to}* item, {to}* pos);
        #     static void remove_{alias_to}({to}* item);
        #     static void remove_all_{alias_to}();
        #     static void delete_all_{alias_to}();
        #     static void replace_{alias_to}({to}* item, {to}* new_item);
        #     static {to}* get_first_{alias_to}();
        #     static {to}* get_last_{alias_to}();
        #     static {to}* get_next_{alias_to}({to}* pos);
        #     static {to}* get_prev_{alias_to}({to}* pos);
        #     static int get_{alias_to}_count();
        #     static int includes_{alias_to}({to}* item);
        #     static void move_{alias_to}_first({to}* item);
        #     static void move_{alias_to}_last({to}* item);
        #     static void move_{alias_to}_after({to}* item, {to}* pos);
        #     static void move_{alias_to}_before({to}* item, {to}* pos);
        #     static void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #     ITERATOR_STATIC_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     static {alias_to}_iterator* _first_{alias_to}_iterator;
        #     static {alias_to}_iterator* _last_{alias_to}_iterator;
        #
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            static=True,
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            static=True,
            default='nullptr',
            name=self.format('last_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('int'),
            access='private',
            static=True,
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            static=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            static=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_CRITICAL_STATIC_MULTI_ACTIVE()
        iterator = self.ITERATOR_STATIC_MULTI_ACTIVE()
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_STATIC_MULTIOWNED_ACTIVE(self):
        # private:
        #     static {to}* {prefix_from}first_{alias_to};
        #     static {to}* {prefix_from}last_{alias_to};
        #     static int {prefix_from}count_{alias_to};
        #
        #     static critical_section _critical_section_{alias_to};
        #
        #     static critical_section& get_relation_critical_section_{alias_to}()
        #     {
        #       return _critical_section_{alias_to};
        #     }
        # protected:
        #     static void add_{alias_to}_first({to}* item);
        #     static void add_{alias_to}_last({to}* item);
        #     static void add_{alias_to}_after({to}* item, {to}* pos);
        #     static void add_{alias_to}_before({to}* item, {to}* pos);
        #     static void remove_{alias_to}({to}* item);
        #     static void replace_{alias_to}({to}* item, {to}* new_item);
        #
        #     static void delete_all_{alias_to}();
        #     static {to}* get_first_{alias_to}();
        #     static {to}* get_last_{alias_to}();
        #     static {to}* get_next_{alias_to}({to}* pos);
        #     static {to}* get_prev_{alias_to}({to}* pos);
        #     static int get_{alias_to}_count();
        #     static void move_{alias_to}_first({to}* item);
        #     static void move_{alias_to}_last({to}* item);
        #     static void move_{alias_to}_after({to}* item, {to}* pos);
        #     static void move_{alias_to}_before({to}* item, {to}* pos);
        #     static void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #     ITERATOR_STATIC_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     static {alias_to}_iterator* _first_{alias_to}_iterator;
        #     static {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            static=True,
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            static=True,
            default='nullptr',
            name=self.format('last_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('int'),
            access='private',
            static=True,
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            static=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            static=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
            )
        self.METHODS_CRITICAL_STATIC_MULTIOWNED_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_STATIC_MULTI_ACTIVE()

        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_UNIQUEVALUETREE_ACTIVE(self):
        # private:
        #     {to}* _first_{alias_to};
        #     int _count_{alias_to};
        #
        #     mutable critical_section _critical_section_{alias_to};
        # public:
        #     critical_section& get_relation_critical_section_{alias_to}() const
        #     {
        #       return _critical_section_{alias_to};
        #     }
        #
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
            )
        self.METHODS_CRITICAL_UNIQUEVALUETREE_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        # private:
        #     {to}* _first_{alias_to};
        #     int _count_{alias_to};
        #
        #     critical_section _critical_section_{alias_to};
        # public:
        #     critical_section& get_relation_critical_section_{alias_to}() const
        #     {
        #       return _critical_section_{alias_to};
        #     }
        #
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
            )
        self.METHODS_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_CRITICAL_VALUETREE_ACTIVE(self):
        # private:
        #     {to} _first_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        #     mutable critical_section _critical_section_{alias_to};
        # public:
        #     critical_section& critical_section_{alias_to}() const { return _critical_section_{alias_to}; }
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     size_t get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=False,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=False,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_CRITICAL_VALUETREE_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )

    def RELATION_CRITICAL_VALUETREE_OWNED_ACTIVE(self):
        # private:
        #     {to} _first_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        #     mutable critical_section _critical_section_{alias_to};
        # public:
        #     critical_section& critical_section_{alias_to}() const { return _critical_section_{alias_to}; }
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     size_t get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        self.REQUIRED_CRITICAL_SECTION_IMPLEMENTATION()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=False,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=False,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        MemberData(
            type=self.type('critical_section'),
            mutable=True,
            access='private',
            default='CRITICAL_SECTION_INIT',
            name=self.format('relation_critical_section_{alias_to}'),
            parent=self,
            read_only=self.read_only
        )
        MemberMethod(
            type=self.type('critical_section', ref=True),
            access='public',
            inline=True,
            constmethod=True,
            name=self.format('get_relation_critical_section_{alias_to}'),
            content=self.format("\treturn {prefix_from}relation_critical_section_{alias_to};"),
            parent=self,
            read_only=self.read_only
        )
        self.METHODS_CRITICAL_VALUETREE_OWNED_ACTIVE()
        iterator = self.ITERATOR_CRITICAL_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
        )

    def RELATION_MULTI_ACTIVE(self):
        # public:
        #     {to}* _first_{alias_to};
        #     {to}* _last_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        # public:
        #     void add_{alias_to}_first({to}* item);
        #     void add_{alias_to}_last({to}* item);
        #     void add_{alias_to}_after({to}* item, {to}* pos);
        #     void add_{alias_to}_before({to}* item, {to}* pos);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     void move_{alias_to}_first({to}* item);
        #     void move_{alias_to}_last({to}* item);
        #     void move_{alias_to}_after({to}* item, {to}* pos);
        #     void move_{alias_to}_before({to}* item, {to}* pos);
        #     void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #    ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_MULTI_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_MULTIOWNED_ACTIVE(self):
        # public:
        #     {to}* _first_{alias_to};
        #     {to}* _last_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        # protected:
        #     void add_{alias_to}_first({to}* item);
        #     void add_{alias_to}_last({to}* item);
        #     void add_{alias_to}_after({to}* item, {to}* pos);
        #     void add_{alias_to}_before({to}* item, {to}* pos);
        #     void remove_{alias_to}({to}* item);
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #
        # public:
        #     void delete_all_{alias_to}();
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     void move_{alias_to}_first({to}* item);
        #     void move_{alias_to}_last({to}* item);
        #     void move_{alias_to}_after({to}* item, {to}* pos);
        #     void move_{alias_to}_before({to}* item, {to}* pos);
        #     void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('int'),
            access='private',
            default='0',
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_MULTIOWNED_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_SINGLE_ACTIVE(self):
        """Initialize the class structure"""
        """
        public:
            {to}* _ref_{alias_to};
        
        public:
            void set_{alias_to}({to}* item);
            void clear_{alias_to}({to}* item);
            void replace_{alias_to}({to}* item, {to}* new_item);
            void move_{alias_to}({to}* item);
            {to}* get_{alias_to}() const { return _ref_{alias_to}; };
        """
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_SINGLE_ACTIVE()

    def RELATION_SINGLE_OWNED_ACTIVE(self):
        # public:
        #    {to}* _ref_{alias_to};
        #
        # protected:
        #     void set_{alias_to}({to}* item);
        #     void clear_{alias_to}({to}* item);
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #
        # public:
        #     void move_{alias_to}({to}* item);
        #     {to}* get_{alias_to}() const {return _ref_{alias_to};};
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            name=self.format('ref_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_SINGLE_OWNED_ACTIVE()

    def RELATION_STATIC_MULTI_ACTIVE(self):
        # public:
        #     static {to}* _first_{alias_to};
        #     static {to}* _last_{alias_to};
        #     static int {prefix_from}count_{alias_to};
        #
        # public:
        #     static void add_{alias_to}_first({to}* item);
        #     static void add_{alias_to}_last({to}* item);
        #     static void add_{alias_to}_after({to}* item, {to}* pos);
        #     static void add_{alias_to}_before({to}* item, {to}* pos);
        #     static void remove_{alias_to}({to}* item);
        #     static void remove_all_{alias_to}();
        #     static void delete_all_{alias_to}();
        #     static void replace_{alias_to}({to}* item, {to}* new_item);
        #     static {to}* get_first_{alias_to}();
        #     static {to}* get_last_{alias_to}();
        #     static {to}* get_next_{alias_to}({to}* pos);
        #     static {to}* get_prev_{alias_to}({to}* pos);
        #     static int get_{alias_to}_count();
        #     static int includes_{alias_to}({to}* item);
        #     static void move_{alias_to}_first({to}* item);
        #     static void move_{alias_to}_last({to}* item);
        #     static void move_{alias_to}_after({to}* item, {to}* pos);
        #     static void move_{alias_to}_before({to}* item, {to}* pos);
        #     static void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #     ITERATOR_STATIC_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     static {alias_to}_iterator* _first_{alias_to}_iterator;
        #     static {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=True,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=True,
            name=self.format('last_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=True,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_STATIC_MULTI_ACTIVE()
        iterator = self.ITERATOR_STATIC_MULTI_ACTIVE()
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_STATIC_MULTIOWNED_ACTIVE(self):
        # public:
        #     static {to}* _first_{alias_to};
        #     static {to}* _last_{alias_to};
        #     static int {prefix_from}count_{alias_to};
        #
        # protected:
        #     static void add_{alias_to}_first({to}* item);
        #     static void add_{alias_to}_last({to}* item);
        #     static void add_{alias_to}_after({to}* item, {to}* pos);
        #     static void add_{alias_to}_before({to}* item, {to}* pos);
        #     static void remove_{alias_to}({to}* item);
        #     static void replace_{alias_to}({to}* item, {to}* new_item);
        #
        # public:
        #     static void delete_all_{alias_to}();
        #     static {to}* get_first_{alias_to}();
        #     static {to}* get_last_{alias_to}();
        #     static {to}* get_next_{alias_to}({to}* pos);
        #     static {to}* get_prev_{alias_to}({to}* pos);
        #     static int get_{alias_to}_count();
        #     static void move_{alias_to}_first({to}* item);
        #     static void move_{alias_to}_last({to}* item);
        #     static void move_{alias_to}_after({to}* item, {to}* pos);
        #     static void move_{alias_to}_before({to}* item, {to}* pos);
        #     static void sort_{alias_to}(int (*compare)({to}*, {to}*));
        #     ITERATOR_STATIC_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     static {alias_to}_iterator* _first_{alias_to}_iterator;
        #     static {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=True,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=True,
            name=self.format('last_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=True,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_STATIC_MULTIOWNED_ACTIVE()
        iterator = self.ITERATOR_STATIC_MULTI_ACTIVE()
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            static=True,
            access='private',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_UNIQUEVALUETREE_ACTIVE(self):
        # private:
        #     {to}* _first_{alias_to};
        #     int _count_{alias_to};
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=False,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=False,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_UNIQUEVALUETREE_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        # private:
        #     {to}* _first_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        # protected:
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=False,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=False,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_UNIQUEVALUETREE_OWNED_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_NOFILTER_MULTI_ACTIVE(self):
        pass

    def RELATION_NOFILTER_MULTIOWNED_ACTIVE(self):
        pass

    def RELATION_VALUETREE_ACTIVE(self):
        # private:
        #     {to} _first_{alias_to};
        #     int {prefix_from}count_{alias_to};
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void remove_all_{alias_to}();
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     size_t get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=False,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=False,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        self.METHODS_VALUETREE_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def RELATION_VALUETREE_OWNED_ACTIVE(self):
        # private:
        #     {to}* _first_{alias_to};
        #     int _count_{alias_to};
        #
        #
        # public:
        #     void add_{alias_to}({to}* item);
        #     void remove_{alias_to}({to}* item);
        #     void delete_all_{alias_to}();
        #     void replace_{alias_to}({to}* item, {to}* new_item);
        #     {to}* get_first_{alias_to}() const;
        #     {to}* get_last_{alias_to}() const;
        #     {to}* get_next_{alias_to}({to}* pos) const;
        #     {to}* get_prev_{alias_to}({to}* pos) const;
        #     int get_{alias_to}_count() const;
        #     ITERATOR_MULTI_ACTIVE({from}, {alias_from}, {to}, {alias_to})
        #     friend class {alias_to}_iterator;
        # protected:
        #     mutable {alias_to}_iterator* _first_{alias_to}_iterator;
        #     mutable {alias_to}_iterator* _last_{alias_to}_iterator;
        self.REQUIRED_TO_FRIENDSHIP()
        MemberData(
            type=self.type(self._key.to_class, ptr=True),
            access='private',
            default='nullptr',
            static=False,
            name=self.format('first_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        MemberData(
            type=self.type('size_t'),
            access='private',
            default='0',
            static=False,
            name=self.format('count_{alias_to}'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

        self.METHODS_VALUETREE_OWNED_ACTIVE()
        iterator = self.ITERATOR_MULTI_ACTIVE()
        #         friend class {alias_to}_iterator;
        self.REQUIRED_FRIENDSHIP(iterator)
        #         {alias_to}_iterator* _first;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('first_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )
        #          {alias_to}_iterator* _last;
        MemberData(
            type=self.type(iterator, ptr=True),
            mutable=True,
            access='protected',
            default='nullptr',
            name=self.format('last_{alias_to}_iterator'),
            parent=self,
            visibleInTree=not HIDE_TREE,
            read_only=self.read_only
            )

    def REQUIRED_TO_FRIENDSHIP(self):
        self.REQUIRED_FRIENDSHIP(self._key.to_class)

    def REQUIRED_FRIENDSHIP(self, friend_class, in_class=None):
        """ Add a friendship, checking if it already exists."""
        if in_class is None:
            in_class = self.inner_class
        if friend_class != in_class and friend_class not in [x._target for x in in_class.friends]:
            Friendship(
                target=friend_class,
                parent=in_class,
                read_only=self.read_only,
                visibleInTree=False
            )

    def REQUIRED_CRITICAL_SECTION_IMPLEMENTATION(self):
        """
        This method checks if it exist a project context named CRITICAL_RELATION_DEFINES.
        If this context doesn't exists, creates an enabled basic definitions that
        allow for build using pthreads.
        """
        from ._CCContext import ContextItem
        if TransactionStack.InTransaction():
            self.project.save_state()
        contexts = getattr(self.project, '_contexts', [])
        for context in contexts:
            if context.name == "CRITICAL_RELATION_DEFINES":
                return
        # Ok, the context doesn't exist, so we must create it
        contexts.append(ContextItem(
            name="CRITICAL_RELATION_DEFINES",
            define="""
#define critical_section  pthread_mutex_t
#define CRITICAL_SECTION_INIT PTHREAD_MUTEX_INITIALIZER

#include <pthread.h>

class critical_section_lock
{
pthread_mutex_t  *_pmutex;
public:
    critical_section_lock( pthread_mutex_t &mutex)
    : _pmutex(&mutex)
    {
        pthread_mutex_lock(_pmutex);
    }
    ~critical_section_lock()
    {
        pthread_mutex_unlock(_pmutex);
    }
};
""",
            enable=True,
            note="This context was automatically generated by the use of critical relations."
        ))
        setattr(self.project, '_contexts', contexts)

    def METHODS_AVLTREE_ACTIVE(self):
        self.METHOD_AVLTREE_FIND()
        self.METHOD_AVLTREE_ADD()
        self.METHOD_AVLTREE_REMOVE()
        self.METHOD_AVLTREE_REMOVEALL()
        self.METHOD_AVLTREE_DELETEALL()
        self.METHOD_AVLTREE_REPLACE()
        self.METHOD_AVLTREE_GETFIRST()
        self.METHOD_AVLTREE_GETLAST()
        self.METHOD_AVLTREE_GETNEXT()
        self.METHOD_AVLTREE_GETPREV()
        self.METHOD_AVLTREE_GETCOUNT()

    def METHODS_AVLTREE_OWNED_ACTIVE(self):
        self.METHOD_AVLTREE_FIND()
        self.METHOD_AVLTREE_ADD()
        self.METHOD_AVLTREE_REMOVE()
        self.METHOD_AVLTREE_DELETEALL()
        self.METHOD_AVLTREE_REPLACE()
        self.METHOD_AVLTREE_GETFIRST()
        self.METHOD_AVLTREE_GETLAST()
        self.METHOD_AVLTREE_GETNEXT()
        self.METHOD_AVLTREE_GETPREV()
        self.METHOD_AVLTREE_GETCOUNT()

    def METHODS_CRITICAL_AVLTREE_ACTIVE(self):
        self.METHOD_CRITICAL_AVLTREE_FIND()
        self.METHOD_CRITICAL_AVLTREE_ADD()
        self.METHOD_CRITICAL_AVLTREE_REMOVE()
        self.METHOD_CRITICAL_AVLTREE_REMOVEALL()
        self.METHOD_CRITICAL_AVLTREE_DELETEALL()
        self.METHOD_CRITICAL_AVLTREE_REPLACE()
        self.METHOD_CRITICAL_AVLTREE_GETFIRST()
        self.METHOD_CRITICAL_AVLTREE_GETLAST()
        self.METHOD_CRITICAL_AVLTREE_GETNEXT()
        self.METHOD_CRITICAL_AVLTREE_GETPREV()
        self.METHOD_CRITICAL_AVLTREE_GETCOUNT()

    def METHODS_CRITICAL_AVLTREE_OWNED_ACTIVE(self):
        self.METHODS_CRITICAL_AVLTREE_ACTIVE()

    def METHODS_CRITICAL_MULTI_ACTIVE(self):
        # critical_section {from}::_critical_section_{alias_to};
        self.METHOD_CRITICAL_MULTI_ADDFIRST()
        self.METHOD_CRITICAL_MULTI_ADDLAST()
        self.METHOD_CRITICAL_MULTI_ADDAFTER()
        self.METHOD_CRITICAL_MULTI_ADDBEFORE()
        self.METHOD_CRITICAL_MULTI_REMOVE()
        self.METHOD_CRITICAL_MULTI_REMOVEALL()
        self.METHOD_CRITICAL_MULTI_DELETEALL()
        self.METHOD_CRITICAL_MULTI_REPLACE()
        self.METHOD_CRITICAL_MULTI_GETFIRST()
        self.METHOD_CRITICAL_MULTI_GETLAST()
        self.METHOD_CRITICAL_MULTI_GETNEXT()
        self.METHOD_CRITICAL_MULTI_GETPREV()
        self.METHOD_CRITICAL_MULTI_GETCOUNT()
        self.METHOD_CRITICAL_MULTI_MOVEFIRST()
        self.METHOD_CRITICAL_MULTI_MOVELAST()
        self.METHOD_CRITICAL_MULTI_MOVEAFTER()
        self.METHOD_CRITICAL_MULTI_MOVEBEFORE()
        self.METHOD_CRITICAL_MULTI_SORT()

    def METHODS_CRITICAL_MULTIOWNED_ACTIVE(self):
        self.METHOD_CRITICAL_MULTI_ADDFIRST()
        self.METHOD_CRITICAL_MULTI_ADDLAST()
        self.METHOD_CRITICAL_MULTI_ADDAFTER()
        self.METHOD_CRITICAL_MULTI_ADDBEFORE()
        self.METHOD_CRITICAL_MULTI_REMOVE()
        self.METHOD_CRITICAL_MULTI_DELETEALL()
        self.METHOD_CRITICAL_MULTI_REPLACE()
        self.METHOD_CRITICAL_MULTI_GETFIRST()
        self.METHOD_CRITICAL_MULTI_GETLAST()
        self.METHOD_CRITICAL_MULTI_GETNEXT()
        self.METHOD_CRITICAL_MULTI_GETPREV()
        self.METHOD_CRITICAL_MULTI_GETCOUNT()
        self.METHOD_CRITICAL_MULTI_MOVEFIRST()
        self.METHOD_CRITICAL_MULTI_MOVELAST()
        self.METHOD_CRITICAL_MULTI_MOVEAFTER()
        self.METHOD_CRITICAL_MULTI_MOVEBEFORE()
        self.METHOD_CRITICAL_MULTI_SORT()

    def METHODS_CRITICAL_SINGLE_ACTIVE(self):
        #     critical_section {from}::_critical_section_{alias_to};
        self.METHOD_CRITICAL_SINGLE_SET()
        self.METHOD_CRITICAL_SINGLE_CLEAR()
        self.METHOD_CRITICAL_SINGLE_MOVE()
        self.METHOD_CRITICAL_SINGLE_REPLACE()

    def METHODS_CRITICAL_STATIC_MULTI_ACTIVE(self):
        #     critical_section {from}::_critical_section_{alias_to};
        self.METHOD_CRITICAL_STATIC_MULTI_ADDFIRST()
        self.METHOD_CRITICAL_STATIC_MULTI_ADDLAST()
        self.METHOD_CRITICAL_STATIC_MULTI_ADDAFTER()
        self.METHOD_CRITICAL_STATIC_MULTI_ADDBEFORE()
        self.METHOD_CRITICAL_STATIC_MULTI_REMOVE()
        self.METHOD_CRITICAL_STATIC_MULTI_REMOVEALL()
        self.METHOD_CRITICAL_STATIC_MULTI_DELETEALL()
        self.METHOD_CRITICAL_STATIC_MULTI_REPLACE()
        self.METHOD_CRITICAL_STATIC_MULTI_GETFIRST()
        self.METHOD_CRITICAL_STATIC_MULTI_GETLAST()
        self.METHOD_CRITICAL_STATIC_MULTI_GETNEXT()
        self.METHOD_CRITICAL_STATIC_MULTI_GETPREV()
        self.METHOD_CRITICAL_STATIC_MULTI_GETCOUNT()
        self.METHOD_CRITICAL_STATIC_MULTI_INCLUDES()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVEFIRST()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVELAST()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVEAFTER()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVEBEFORE()
        self.METHOD_CRITICAL_STATIC_MULTI_SORT()

    def METHODS_CRITICAL_STATIC_MULTIOWNED_ACTIVE(self):
        self.METHOD_CRITICAL_STATIC_MULTI_ADDFIRST('protected')
        self.METHOD_CRITICAL_STATIC_MULTI_ADDLAST('protected')
        self.METHOD_CRITICAL_STATIC_MULTI_ADDAFTER('protected')
        self.METHOD_CRITICAL_STATIC_MULTI_ADDBEFORE('protected')
        self.METHOD_CRITICAL_STATIC_MULTI_REMOVE('protected')
        self.METHOD_CRITICAL_STATIC_MULTI_REPLACE('protected')
        self.METHOD_CRITICAL_STATIC_MULTI_DELETEALL()
        self.METHOD_CRITICAL_STATIC_MULTI_GETFIRST()
        self.METHOD_CRITICAL_STATIC_MULTI_GETLAST()
        self.METHOD_CRITICAL_STATIC_MULTI_GETNEXT()
        self.METHOD_CRITICAL_STATIC_MULTI_GETPREV()
        self.METHOD_CRITICAL_STATIC_MULTI_GETCOUNT()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVEFIRST()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVELAST()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVEAFTER()
        self.METHOD_CRITICAL_STATIC_MULTI_MOVEBEFORE()
        self.METHOD_CRITICAL_STATIC_MULTI_SORT()

    def METHODS_ITERATOR_MULTI_ACTIVE(self, iterator):
        self.METHOD_ITERATOR_MULTI_INIT(iterator)
        self.METHOD_ITERATOR_MULTI_EXIT(iterator)
        self.METHOD_ITERATOR_MULTI_CTOR(iterator)
        self.METHOD_ITERATOR_MULTI_CTOR_REF(iterator)
        self.METHOD_ITERATOR_MULTI_CTOR_COPY(iterator)
        self.METHOD_ITERATOR_MULTI_DTOR(iterator)
        #inlines
        self.METHOD_ITERATOR_MULTI_ASSIGN(iterator)
        self.METHOD_ITERATOR_MULTI_NEXT(iterator)
        self.METHOD_ITERATOR_MULTI_PREV(iterator)
        self.METHOD_ITERATOR_MULTI_CAST(iterator)
        self.METHOD_ITERATOR_MULTI_PTR(iterator)
        self.METHOD_ITERATOR_MULTI_GET(iterator)
        self.METHOD_ITERATOR_MULTI_RESET(iterator)
        self.METHOD_ITERATOR_MULTI_ISFIRST(iterator)
        self.METHOD_ITERATOR_MULTI_ISLAST(iterator)
        #protected
        self.METHOD_ITERATOR_MULTI_CHECK_1(iterator)
        self.METHOD_ITERATOR_MULTI_CHECK_2(iterator)

    def METHODS_ITERATOR_CRITICAL_MULTI_ACTIVE(self, iterator):
        self.METHOD_ITERATOR_CRITICAL_MULTI_INIT(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_EXIT(iterator)
        self.METHOD_ITERATOR_MULTI_CTOR(iterator)
        self.METHOD_ITERATOR_MULTI_CTOR_REF(iterator)
        self.METHOD_ITERATOR_MULTI_CTOR_COPY(iterator)
        self.METHOD_ITERATOR_MULTI_DTOR(iterator)
        #inlines
        self.METHOD_ITERATOR_CRITICAL_MULTI_ASSIGN(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_NEXT(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_PREV(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_CAST(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_PTR(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_GET(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_RESET(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_ISFIRST(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_ISLAST(iterator)
        #protected
        self.METHOD_ITERATOR_CRITICAL_MULTI_CHECK_1(iterator)
        self.METHOD_ITERATOR_CRITICAL_MULTI_CHECK_2(iterator)

    def METHODS_ITERATOR_STATIC_MULTI_ACTIVE(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_INIT(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_EXIT(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_CTOR(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_CTOR_COPY(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_DTOR(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_CHECK_1(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_CHECK_2(iterator)
        #inlines
        self.METHOD_ITERATOR_STATIC_MULTI_ASSIGN(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_NEXT(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_PREV(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_CAST(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_PTR(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_GET(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_RESET(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_ISLAST(iterator)
        self.METHOD_ITERATOR_STATIC_MULTI_ISFIRST(iterator)

    def METHODS_ITERATOR_CRITICAL_STATIC_MULTI_ACTIVE(self, iterator):
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_INIT(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_EXIT(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CTOR(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CTOR_COPY(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_DTOR(iterator)
        #inlines
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ASSIGN(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_NEXT(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_PREV(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CAST(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_PTR(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_GET(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_RESET(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ISFIRST(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ISLAST(iterator)
        #protected
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_1(iterator)
        self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_2(iterator)

    def METHODS_MULTI_ACTIVE(self):
        self.METHOD_MULTI_ADDFIRST()
        self.METHOD_MULTI_ADDLAST()
        self.METHOD_MULTI_ADDAFTER()
        self.METHOD_MULTI_ADDBEFORE()
        self.METHOD_MULTI_REMOVE()
        self.METHOD_MULTI_REMOVEALL()
        self.METHOD_MULTI_DELETEALL()
        self.METHOD_MULTI_REPLACE()
        self.METHOD_MULTI_GETFIRST()
        self.METHOD_MULTI_GETLAST()
        self.METHOD_MULTI_GETNEXT()
        self.METHOD_MULTI_GETPREV()
        self.METHOD_MULTI_GETCOUNT()
        self.METHOD_MULTI_MOVEFIRST()
        self.METHOD_MULTI_MOVELAST()
        self.METHOD_MULTI_MOVEAFTER()
        self.METHOD_MULTI_MOVEBEFORE()
        self.METHOD_MULTI_SORT()

    def METHODS_MULTIOWNED_ACTIVE(self):
        self.METHOD_MULTI_ADDFIRST('protected')
        self.METHOD_MULTI_ADDLAST('protected')
        self.METHOD_MULTI_ADDAFTER('protected')
        self.METHOD_MULTI_ADDBEFORE('protected')
        self.METHOD_MULTI_REMOVE('protected')
        self.METHOD_MULTI_REPLACE('protected')
        self.METHOD_MULTI_DELETEALL()
        self.METHOD_MULTI_GETFIRST()
        self.METHOD_MULTI_GETLAST()
        self.METHOD_MULTI_GETNEXT()
        self.METHOD_MULTI_GETPREV()
        self.METHOD_MULTI_GETCOUNT()
        self.METHOD_MULTI_MOVEFIRST()
        self.METHOD_MULTI_MOVELAST()
        self.METHOD_MULTI_MOVEAFTER()
        self.METHOD_MULTI_MOVEBEFORE()
        self.METHOD_MULTI_SORT()

    def METHODS_SINGLE_ACTIVE(self, access='public'):
        self.METHOD_SINGLE_SET(access)
        self.METHOD_SINGLE_CLEAR(access)
        self.METHOD_SINGLE_REPLACE(access)
        self.METHOD_SINGLE_MOVE()
        self.METHOD_SINGLE_GET()

    def METHODS_SINGLE_OWNED_ACTIVE(self):
        self.METHODS_SINGLE_ACTIVE('protected')

    def METHODS_STATIC_MULTI_ACTIVE(self):
        self.METHOD_STATIC_MULTI_ADDFIRST()
        self.METHOD_STATIC_MULTI_ADDLAST()
        self.METHOD_STATIC_MULTI_ADDAFTER()
        self.METHOD_STATIC_MULTI_ADDBEFORE()
        self.METHOD_STATIC_MULTI_REMOVE()
        self.METHOD_STATIC_MULTI_REMOVEALL()
        self.METHOD_STATIC_MULTI_DELETEALL()
        self.METHOD_STATIC_MULTI_REPLACE()
        self.METHOD_STATIC_MULTI_GETFIRST()
        self.METHOD_STATIC_MULTI_GETLAST()
        self.METHOD_STATIC_MULTI_GETNEXT()
        self.METHOD_STATIC_MULTI_GETPREV()
        self.METHOD_STATIC_MULTI_GETCOUNT()
        self.METHOD_STATIC_MULTI_INCLUDES()
        self.METHOD_STATIC_MULTI_MOVEFIRST()
        self.METHOD_STATIC_MULTI_MOVELAST()
        self.METHOD_STATIC_MULTI_MOVEAFTER()
        self.METHOD_STATIC_MULTI_MOVEBEFORE()
        self.METHOD_STATIC_MULTI_SORT()

    def METHODS_STATIC_MULTIOWNED_ACTIVE(self):
        self.METHOD_STATIC_MULTI_ADDFIRST('protected')
        self.METHOD_STATIC_MULTI_ADDLAST('protected')
        self.METHOD_STATIC_MULTI_ADDAFTER('protected')
        self.METHOD_STATIC_MULTI_ADDBEFORE('protected')
        self.METHOD_STATIC_MULTI_REMOVE('protected')
        self.METHOD_STATIC_MULTI_REPLACE('protected')
        self.METHOD_STATIC_MULTI_DELETEALL()
        self.METHOD_STATIC_MULTI_GETFIRST()
        self.METHOD_STATIC_MULTI_GETLAST()
        self.METHOD_STATIC_MULTI_GETNEXT()
        self.METHOD_STATIC_MULTI_GETPREV()
        self.METHOD_STATIC_MULTI_GETCOUNT()
        self.METHOD_STATIC_MULTI_MOVEFIRST()
        self.METHOD_STATIC_MULTI_MOVELAST()
        self.METHOD_STATIC_MULTI_MOVEAFTER()
        self.METHOD_STATIC_MULTI_MOVEBEFORE()
        self.METHOD_STATIC_MULTI_SORT()

    def METHODS_UNIQUEVALUETREE_ACTIVE(self):
        self.METHOD_UNIQUEVALUETREE_ADD()
        self.METHOD_UNIQUEVALUETREE_REMOVE()
        self.METHOD_UNIQUEVALUETREE_REMOVEALL()
        self.METHOD_UNIQUEVALUETREE_DELETEALL()
        self.METHOD_UNIQUEVALUETREE_REPLACE()
        self.METHOD_UNIQUEVALUETREE_GETFIRST()
        self.METHOD_UNIQUEVALUETREE_GETLAST()
        self.METHOD_UNIQUEVALUETREE_GETNEXT()
        self.METHOD_UNIQUEVALUETREE_GETPREV()
        self.METHOD_UNIQUEVALUETREE_GETCOUNT()
        self.METHOD_VALUETREE_FIND()
        #self.METHOD_VALUETREE_FINDREVERSE()

    def METHODS_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        return self.METHODS_UNIQUEVALUETREE_ACTIVE()

    def METHODS_CRITICAL_UNIQUEVALUETREE_ACTIVE(self):
        self.METHOD_CRITICAL_UNIQUEVALUETREE_ADD()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_REMOVE()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_REMOVEALL()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_DELETEALL()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_REPLACE()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETFIRST()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETLAST()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETNEXT()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETPREV()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETCOUNT()
        self.METHOD_CRITICAL_VALUETREE_FIND()

    def METHODS_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE(self):
        self.METHOD_CRITICAL_UNIQUEVALUETREE_ADD('protected')
        self.METHOD_CRITICAL_UNIQUEVALUETREE_REMOVE('protected')
        self.METHOD_CRITICAL_UNIQUEVALUETREE_REMOVEALL('protected')
        self.METHOD_CRITICAL_UNIQUEVALUETREE_DELETEALL()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_REPLACE('protected')
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETFIRST()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETLAST()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETNEXT()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETPREV()
        self.METHOD_CRITICAL_UNIQUEVALUETREE_GETCOUNT()
        self.METHOD_CRITICAL_VALUETREE_FIND()


    def METHODS_VALUETREE_ACTIVE(self):
        self.METHOD_VALUETREE_ADD()
        self.METHOD_VALUETREE_REMOVE()
        self.METHOD_VALUETREE_REMOVEALL()
        self.METHOD_VALUETREE_DELETEALL()
        self.METHOD_VALUETREE_REPLACE()
        self.METHOD_VALUETREE_GETFIRST()
        self.METHOD_VALUETREE_GETLAST()
        self.METHOD_VALUETREE_GETNEXT()
        self.METHOD_VALUETREE_GETPREV()
        self.METHOD_VALUETREE_GETCOUNT()
        self.METHOD_VALUETREE_FIND()
        self.METHOD_VALUETREE_FINDREVERSE()

    def METHODS_VALUETREE_OWNED_ACTIVE(self):
        self.METHODS_VALUETREE_ACTIVE()

    def METHODS_CRITICAL_VALUETREE_ACTIVE(self):
        self.METHOD_CRITICAL_VALUETREE_ADD()
        self.METHOD_CRITICAL_VALUETREE_REMOVE()
        self.METHOD_CRITICAL_VALUETREE_REMOVEALL()
        self.METHOD_CRITICAL_VALUETREE_DELETEALL()
        self.METHOD_CRITICAL_VALUETREE_REPLACE()
        self.METHOD_CRITICAL_VALUETREE_GETFIRST()
        self.METHOD_CRITICAL_VALUETREE_GETLAST()
        self.METHOD_CRITICAL_VALUETREE_GETNEXT()
        self.METHOD_CRITICAL_VALUETREE_GETPREV()
        self.METHOD_CRITICAL_VALUETREE_GETCOUNT()
        self.METHOD_CRITICAL_VALUETREE_FIND()
        self.METHOD_CRITICAL_VALUETREE_FINDREVERSE()

    def METHODS_CRITICAL_VALUETREE_OWNED_ACTIVE(self):
        self.METHOD_CRITICAL_VALUETREE_ADD()
        self.METHOD_CRITICAL_VALUETREE_OWNED_REMOVE()
        self.METHOD_CRITICAL_VALUETREE_OWNED_REMOVEALL()
        self.METHOD_CRITICAL_VALUETREE_DELETEALL()
        self.METHOD_CRITICAL_VALUETREE_OWNED_REPLACE()
        self.METHOD_CRITICAL_VALUETREE_GETFIRST()
        self.METHOD_CRITICAL_VALUETREE_GETLAST()
        self.METHOD_CRITICAL_VALUETREE_GETNEXT()
        self.METHOD_CRITICAL_VALUETREE_GETPREV()
        self.METHOD_CRITICAL_VALUETREE_GETCOUNT()
        self.METHOD_CRITICAL_VALUETREE_FIND()
        self.METHOD_CRITICAL_VALUETREE_FINDREVERSE()

    #{to}* {from}::find_{alias_to}(const {member_type}& value)
    @macro_method_complete
    def METHOD_AVLTREE_FIND(self):
        """
        {to_type}* result = 0;
        if ({prefix_from}top_{alias_to})
        {{
            {to_type}* item = {prefix_from}top_{alias_to};
            while (1)
            {{
                if (item->{member} == value)
                {{
                    result = item;
                    break;
                }}
                if (value <= item->{member})
                {{
                    if (item->{prefix_to}left_{alias_from})
                    {{
                        item = item->{prefix_to}left_{alias_from};
                    }}
                    else
                    {{
                        break;
                    }}
                }}
                else
                {{
                    if (item->{prefix_to}right_{alias_from})
                    {{
                        item = item->{prefix_to}right_{alias_from};
                    }}
                    else
                    {{
                        break;
                    }}
                }}
            }}
        }}
        if (result)
        {{
            {to_type}* prev_{alias_to} = get_prev_{alias_to}(result);
            while (prev_{alias_to} != nullptr && prev_{alias_to}->{member} == value)
            {{
                result = prev_{alias_to};
                prev_{alias_to} = get_prev_{alias_to}(result);
            }}
        }}
        return result;
        """
        return {
                   'type': self.type(self._key.to_class, ptr=True), 'access': 'public',
                   'name': self.format('find_{alias_to}'), 'inline': False, 'constmethod': True, 'parent': self,
                    'read_only': self.read_only
               }, \
               {
                   'type': self._key.member.type_instance, 'name': 'value',
                    'read_only': self.read_only
               }

    @macro_method
    def METHOD_AVLTREE_FIND_CONTENT(self):
        return self.METHOD_AVLTREE_FIND.__doc__

    @macro_method_complete
    def METHOD_AVLTREE_ADD(self):
        """
    assert(this);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == nullptr);

    {prefix_from}count_{alias_to}++;

    item->{prefix_to}ref_{alias_from} = this;

    if ({prefix_from}top_{alias_to})
    {{
        {to_type}* current = {prefix_from}top_{alias_to};
        while (1)
        {{
            if (item->{member} < current->{member})
            {{
                if (current->{prefix_to}left_{alias_from})
                {{
                    current = current->{prefix_to}left_{alias_from};
                }}
                else
                {{
                    current->{prefix_to}left_{alias_from} = item;
                    item->{prefix_to}parent_{alias_from} = current;
                    current->{prefix_to}bal_{alias_from}--;
                    break;
                }}
            }}
            else
            {{
                if (current->{prefix_to}right_{alias_from})
                {{
                    current = current->{prefix_to}right_{alias_from};
                }}
                else
                {{
                    current->{prefix_to}right_{alias_from} = item;
                    item->{prefix_to}parent_{alias_from} = current;
                    current->{prefix_to}bal_{alias_from}++;
                    break;
                }}
            }}
        }}

        {to_type}* parent;
        while (current && current->{prefix_to}bal_{alias_from})
        {{
            parent = current->{prefix_to}parent_{alias_from};
            if (parent)
            {{
                if (parent->{prefix_to}left_{alias_from} == current)
                {{
                    parent->{prefix_to}bal_{alias_from}--;
                }}
                else
                {{
                    parent->{prefix_to}bal_{alias_from}++;
                }}

                if (parent->{prefix_to}bal_{alias_from} == 2)
                {{
                    if (current->{prefix_to}bal_{alias_from} == -1)
                    {{
                        {to_type}* sub = current->{prefix_to}left_{alias_from};
                        parent->{prefix_to}right_{alias_from} = sub->{prefix_to}left_{alias_from};
                        if (sub->{prefix_to}left_{alias_from})
                        {{
                            sub->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                        }}
                        current->{prefix_to}left_{alias_from} = sub->{prefix_to}right_{alias_from};
                        if (sub->{prefix_to}right_{alias_from})
                        {{
                            sub->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = current;
                        }}
                        sub->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                        sub->{prefix_to}left_{alias_from} = parent;
                        parent->{prefix_to}parent_{alias_from} = sub;
                        sub->{prefix_to}right_{alias_from} = current;
                        current->{prefix_to}parent_{alias_from} = sub;
                        if (sub->{prefix_to}parent_{alias_from})
                        {{
                            if (sub->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == parent)
                            {{
                                sub->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = sub;
                            }}
                            else
                            {{
                                sub->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = sub;
                            }}
                        }}
                        else
                        {{
                            {prefix_from}top_{alias_to} = sub;
                        }}
                        parent->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == 1? -1: 0);
                        current->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == -1? 1: 0);
                        sub->{prefix_to}bal_{alias_from} = 0;
                        current = sub;
                    }}
                    else
                    {{
                        parent->{prefix_to}right_{alias_from} = current->{prefix_to}left_{alias_from};
                        if (current->{prefix_to}left_{alias_from})
                        {{
                            current->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                        }}
                        current->{prefix_to}left_{alias_from} = parent;
                        current->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                        parent->{prefix_to}parent_{alias_from} = current;
                        if (current->{prefix_to}parent_{alias_from})
                        {{
                            if (current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == parent)
                            {{
                                current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = current;
                            }}
                            else
                            {{
                                current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = current;
                            }}
                        }}
                        else
                        {{
                            {prefix_from}top_{alias_to} = current;
                        }}
                        parent->{prefix_to}bal_{alias_from} = 0;
                        current->{prefix_to}bal_{alias_from} = 0;
                    }}
                }}
                else if (parent->{prefix_to}bal_{alias_from} == -2)
                {{
                    if (current->{prefix_to}bal_{alias_from} == 1)
                    {{
                        {to_type}* sub = current->{prefix_to}right_{alias_from};
                        parent->{prefix_to}left_{alias_from} = sub->{prefix_to}right_{alias_from};
                        if (sub->{prefix_to}right_{alias_from})
                        {{
                            sub->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                        }}
                        current->{prefix_to}right_{alias_from} = sub->{prefix_to}left_{alias_from};
                        if (sub->{prefix_to}left_{alias_from})
                        {{
                            sub->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = current;
                        }}
                        sub->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                        sub->{prefix_to}right_{alias_from} = parent;
                        parent->{prefix_to}parent_{alias_from} = sub;
                        sub->{prefix_to}left_{alias_from} = current;
                        current->{prefix_to}parent_{alias_from} = sub;
                        if (sub->{prefix_to}parent_{alias_from})
                        {{
                            if (sub->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == parent)
                            {{
                                sub->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = sub;
                            }}
                            else
                            {{
                                sub->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = sub;
                            }}
                        }}
                        else
                        {{
                            {prefix_from}top_{alias_to} = sub;
                        }}
                        parent->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == -1? 1: 0);
                        current->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == 1? -1: 0);
                        sub->{prefix_to}bal_{alias_from} = 0;
                        current = sub;
                    }}
                    else
                    {{
                        parent->{prefix_to}left_{alias_from} = current->{prefix_to}right_{alias_from};
                        if (current->{prefix_to}right_{alias_from})
                        {{
                            current->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                        }}
                        current->{prefix_to}right_{alias_from} = parent;
                        current->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                        parent->{prefix_to}parent_{alias_from} = current;
                        if (current->{prefix_to}parent_{alias_from})
                        {{
                            if (current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == parent)
                            {{
                                current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = current;
                            }}
                            else
                            {{
                                current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = current;
                            }}
                        }}
                        else
                        {{
                            {prefix_from}top_{alias_to} = current;
                        }}
                        parent->{prefix_to}bal_{alias_from} = 0;
                        current->{prefix_to}bal_{alias_from} = 0;
                    }}
                }}
                else
                {{
                    current = parent;
                }}
            }}
            else
            {{
                current = parent;
            }}
        }}
    }}
    else
    {{
        {prefix_from}top_{alias_to} = item;
    }}
    """
        return {
                   'type': self.type('void'), 'access': 'public', 'name': self.format('add_{alias_to}'),
                   'inline': False, 'parent': self,
                    'read_only': self.read_only
               }, \
               {
                   'type' : self.type(self._key.to_class,ptr=True), 'name': 'item',
                    'read_only': self.read_only
               }

    @macro_method
    def METHOD_AVLTREE_ADD_CONTENT(self):
        return self.METHOD_AVLTREE_ADD.__doc__

    # void {from}::remove_{alias_to}({to}* item)
    @macro_method_complete
    def METHOD_AVLTREE_REMOVE(self):
        """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    if(  {prefix_from}first_{alias_to}_iterator != nullptr )
    {{
        {prefix_from}first_{alias_to}_iterator->check(item);
    }}

    {prefix_from}count_{alias_to}--;

    {to_type}* subParent = item->{prefix_to}parent_{alias_from};
    {to_type}* sub = item;
    /*     item               -                                   */
    /*      ^        ==>                                          */
    /*     - -                                                    */
    if (!item->{prefix_to}left_{alias_from} && !item->{prefix_to}right_{alias_from})
    {{
        if (subParent)
        {{
            if (subParent->{prefix_to}left_{alias_from} == item)
            {{
                subParent->{prefix_to}left_{alias_from} = nullptr;
                subParent->{prefix_to}bal_{alias_from}++;
            }}
            else
            {{
                subParent->{prefix_to}right_{alias_from} = nullptr;
                subParent->{prefix_to}bal_{alias_from}--;
            }}
        }}
        else
        {{
            {prefix_from}top_{alias_to} = nullptr;
        }}
    }}
    else
    {{
        if (item->{prefix_to}bal_{alias_from} > 0)
        {{
            sub = item->{prefix_to}right_{alias_from};
            while (sub->{prefix_to}left_{alias_from})
            {{
                sub = sub->{prefix_to}left_{alias_from};
            }}
            subParent = sub->{prefix_to}parent_{alias_from};
            if (subParent != item)
            {{
                subParent->{prefix_to}left_{alias_from} = sub->{prefix_to}right_{alias_from};
                if (subParent->{prefix_to}left_{alias_from})
                {{
                    subParent->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = subParent;
                }}
                subParent->{prefix_to}bal_{alias_from}++;
            }}
            else
            {{
                item->{prefix_to}bal_{alias_from}--;
            }}
        }}
        else
        {{
            sub = item->{prefix_to}left_{alias_from};
            while (sub->{prefix_to}right_{alias_from})
            {{
                sub = sub->{prefix_to}right_{alias_from};
            }}
            subParent = sub->{prefix_to}parent_{alias_from};
            if (subParent != item)
            {{
                subParent->{prefix_to}right_{alias_from} = sub->{prefix_to}left_{alias_from};
                if (subParent->{prefix_to}right_{alias_from})
                {{
                    subParent->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = subParent;
                }}
                subParent->{prefix_to}bal_{alias_from}--;
            }}
            else
            {{
                item->{prefix_to}bal_{alias_from}++;
            }}
        }}
        sub->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        if (item->{prefix_to}parent_{alias_from})
        {{
            if (item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = sub;
            }}
            else
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = sub;
            }}
        }}
        else
        {{
            {prefix_from}top_{alias_to} = sub;
        }}
        if (item->{prefix_to}left_{alias_from} != sub)
        {{
            sub->{prefix_to}left_{alias_from} = item->{prefix_to}left_{alias_from};
            if (item->{prefix_to}left_{alias_from})
            {{
                item->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = sub;
            }}
        }}
        if (item->{prefix_to}right_{alias_from} != sub)
        {{
            sub->{prefix_to}right_{alias_from} = item->{prefix_to}right_{alias_from};
            if (item->{prefix_to}right_{alias_from})
            {{
                item->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = sub;
            }}
        }}
        sub->{prefix_to}bal_{alias_from} = item->{prefix_to}bal_{alias_from};

        if (subParent == item)
        {{
            subParent = sub;
        }}
    }}

    item->{prefix_to}ref_{alias_from} = nullptr;
    item->{prefix_to}parent_{alias_from} = nullptr;
    item->{prefix_to}left_{alias_from} = nullptr;
    item->{prefix_to}right_{alias_from} = nullptr;
    item->{prefix_to}bal_{alias_from} = 0;

    {to_type}* parent = subParent;
    while (parent && parent->{prefix_to}bal_{alias_from} != -1 && parent->{prefix_to}bal_{alias_from} != 1)
    {{
        if (parent->{prefix_to}bal_{alias_from} == 2)
        {{
            {to_type}* current = parent->{prefix_to}right_{alias_from};
            if (current->{prefix_to}bal_{alias_from} == -1)
            {{
                {to_type}* sub = current->{prefix_to}left_{alias_from};
                parent->{prefix_to}right_{alias_from} = sub->{prefix_to}left_{alias_from};
                if (sub->{prefix_to}left_{alias_from})
                {{
                    sub->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                }}
                current->{prefix_to}left_{alias_from} = sub->{prefix_to}right_{alias_from};
                if (sub->{prefix_to}right_{alias_from})
                {{
                    sub->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = current;
                }}
                sub->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                sub->{prefix_to}left_{alias_from} = parent;
                parent->{prefix_to}parent_{alias_from} = sub;
                sub->{prefix_to}right_{alias_from} = current;
                current->{prefix_to}parent_{alias_from} = sub;
                if (sub->{prefix_to}parent_{alias_from})
                {{
                    if (sub->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == parent)
                    {{
                        sub->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = sub;
                    }}
                    else
                    {{
                        sub->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = sub;
                    }}
                }}
                else
                {{
                    {prefix_from}top_{alias_to} = sub;
                }}
                parent->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == 1? -1: 0);
                current->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == -1? 1: 0);
                sub->{prefix_to}bal_{alias_from} = 0;
                parent = sub;
            }}
            else if (current->{prefix_to}bal_{alias_from} == 1)
            {{
                parent->{prefix_to}right_{alias_from} = current->{prefix_to}left_{alias_from};
                if (current->{prefix_to}left_{alias_from})
                {{
                    current->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                }}
                current->{prefix_to}left_{alias_from} = parent;
                current->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                parent->{prefix_to}parent_{alias_from} = current;
                if (current->{prefix_to}parent_{alias_from})
                {{
                    if (current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == parent)
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = current;
                    }}
                    else
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = current;
                    }}
                }}
                else
                {{
                    {prefix_from}top_{alias_to} = current;
                }}
                parent->{prefix_to}bal_{alias_from} = 0;
                current->{prefix_to}bal_{alias_from} = 0;
                parent = current;
            }}
            else
            {{
                parent->{prefix_to}right_{alias_from} = current->{prefix_to}left_{alias_from};
                if (current->{prefix_to}left_{alias_from})
                {{
                    current->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                }}
                current->{prefix_to}left_{alias_from} = parent;
                current->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                parent->{prefix_to}parent_{alias_from} = current;
                if (current->{prefix_to}parent_{alias_from})
                {{
                    if (current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == parent)
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = current;
                    }}
                    else
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = current;
                    }}
                }}
                else
                {{
                    {prefix_from}top_{alias_to} = current;
                }}
                parent->{prefix_to}bal_{alias_from} = 1;
                current->{prefix_to}bal_{alias_from} = -1;
                break;
            }}
        }}
        else if (parent->{prefix_to}bal_{alias_from} == -2)
        {{
            {to_type}* current = parent->{prefix_to}left_{alias_from};
            if (current->{prefix_to}bal_{alias_from} == 1)
            {{
                {to_type}* sub = current->{prefix_to}right_{alias_from};
                parent->{prefix_to}left_{alias_from} = sub->{prefix_to}right_{alias_from};
                if (sub->{prefix_to}right_{alias_from})
                {{
                    sub->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                }}
                current->{prefix_to}right_{alias_from} = sub->{prefix_to}left_{alias_from};
                if (sub->{prefix_to}left_{alias_from})
                {{
                    sub->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = current;
                }}
                sub->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                sub->{prefix_to}right_{alias_from} = parent;
                parent->{prefix_to}parent_{alias_from} = sub;
                sub->{prefix_to}left_{alias_from} = current;
                current->{prefix_to}parent_{alias_from} = sub;
                if (sub->{prefix_to}parent_{alias_from})
                {{
                    if (sub->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == parent)
                    {{
                        sub->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = sub;
                    }}
                    else
                    {{
                        sub->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = sub;
                    }}
                }}
                else
                {{
                    {prefix_from}top_{alias_to} = sub;
                }}
                parent->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == -1? 1: 0);
                current->{prefix_to}bal_{alias_from} = (sub->{prefix_to}bal_{alias_from} == 1? -1: 0);
                sub->{prefix_to}bal_{alias_from} = 0;
                parent = sub;
            }}
            else if (current->{prefix_to}bal_{alias_from} == -1)
            {{
                parent->{prefix_to}left_{alias_from} = current->{prefix_to}right_{alias_from};
                if (current->{prefix_to}right_{alias_from})
                {{
                    current->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                }}
                current->{prefix_to}right_{alias_from} = parent;
                current->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                parent->{prefix_to}parent_{alias_from} = current;
                if (current->{prefix_to}parent_{alias_from})
                {{
                    if (current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == parent)
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = current;
                    }}
                    else
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = current;
                    }}
                }}
                else
                {{
                    {prefix_from}top_{alias_to} = current;
                }}
                parent->{prefix_to}bal_{alias_from} = 0;
                current->{prefix_to}bal_{alias_from} = 0;
                parent = current;
            }}
            else
            {{
                parent->{prefix_to}left_{alias_from} = current->{prefix_to}right_{alias_from};
                if (current->{prefix_to}right_{alias_from})
                {{
                    current->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = parent;
                }}
                current->{prefix_to}right_{alias_from} = parent;
                current->{prefix_to}parent_{alias_from} = parent->{prefix_to}parent_{alias_from};
                parent->{prefix_to}parent_{alias_from} = current;
                if (current->{prefix_to}parent_{alias_from})
                {{
                    if (current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == parent)
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = current;
                    }}
                    else
                    {{
                        current->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = current;
                    }}
                }}
                else
                {{
                    {prefix_from}top_{alias_to} = current;
                }}
                parent->{prefix_to}bal_{alias_from} = -1;
                current->{prefix_to}bal_{alias_from} = 1;
                break;
            }}
        }}

        if (parent->{prefix_to}parent_{alias_from})
        {{
            if (parent->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == parent)
            {{
                parent->{prefix_to}parent_{alias_from}->{prefix_to}bal_{alias_from}++;
            }}
            else
            {{
                parent->{prefix_to}parent_{alias_from}->{prefix_to}bal_{alias_from}--;
            }}
        }}

        parent = parent->{prefix_to}parent_{alias_from};
    }}
    """
        return {
            'type' : self.type('void'),
            'access' : 'public',
            'name' : self.format('remove_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class,ptr=True),
            'name' : self.format('item'),
            'read_only': self.read_only
            }

    @macro_method
    def METHOD_AVLTREE_REMOVE_CONTENT(self):
        return self.METHOD_AVLTREE_REMOVE.__doc__

    # void {from}::remove_all_{alias_to}()
    @macro_method_complete
    def METHOD_AVLTREE_REMOVEALL(self):
        """
    assert(this);
    while ({prefix_from}top_{alias_to})
    {{
        remove_{alias_to}({prefix_from}top_{alias_to});
    }}
    """
        return [{
            'type' : self.type('void'),
            'access' : 'public',
            'name' : self.format('remove_all_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_AVLTREE_REMOVEALL_CONTENT(self):
        return self.METHOD_AVLTREE_REMOVEALL.__doc__

    # void {from}::delete_all_{alias_to}()
    @macro_method_complete
    def METHOD_AVLTREE_DELETEALL(self):
        """
    assert(this);

    while ({prefix_from}top_{alias_to})
    {{
        delete {prefix_from}top_{alias_to};
    }}
    """
        return [{
            'type' : self.type('void'),
            'access' : 'public',
            'name' : self.format('delete_all_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_AVLTREE_DELETEALL_CONTENT(self):
        return self.METHOD_AVLTREE_DELETEALL.__doc__

    # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
    @macro_method_complete
    def METHOD_AVLTREE_REPLACE(self):
        """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    assert(new_item);
    assert(new_item->{prefix_to}ref_{alias_from} == nullptr);

    if (item->{member} == new_item->{member})
    {{
        if(  {prefix_from}first_{alias_to}_iterator != nullptr )
        {{
            {prefix_from}first_{alias_to}_iterator->check(item, new_item);
        }}
        if ({prefix_from}top_{alias_to} == item)
        {{
            {prefix_from}top_{alias_to} = new_item;
        }}
        if (item->{prefix_to}parent_{alias_from})
        {{
            if (item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = new_item;
            }}
            else if (item->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = new_item;
            }}
        }}
        /* fix by mel : missing update side references */
        if (item->{prefix_to}left_{alias_from})
        {{
            item->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = new_item;
        }}
        if (item->{prefix_to}right_{alias_from})
        {{
            item->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = new_item;
        }}
        /* end fix */
        new_item->{prefix_to}ref_{alias_from} = this;
        new_item->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        new_item->{prefix_to}left_{alias_from} = item->{prefix_to}left_{alias_from};
        new_item->{prefix_to}right_{alias_from} = item->{prefix_to}right_{alias_from};
        new_item->{prefix_to}bal_{alias_from} = item->{prefix_to}bal_{alias_from};
        item->{prefix_to}ref_{alias_from} = nullptr;
        item->{prefix_to}parent_{alias_from} = nullptr;
        item->{prefix_to}left_{alias_from} = nullptr;
        item->{prefix_to}right_{alias_from} = nullptr;
        item->{prefix_to}bal_{alias_from} = 0;
    }}
    else
    {{
        if(  {prefix_from}first_{alias_to}_iterator != nullptr )
        {{
            {prefix_from}first_{alias_to}_iterator->check(item);
        }}
        remove_{alias_to}(item);
        add_{alias_to}(new_item);
    }}
    """
        return [{
            'type': self.type('void'),
            'access': 'public',
            'name': self.format('replace_{alias_to}'),
            'inline': False,
            'parent': self,
            'read_only': self.read_only
            }, {
            'type': self.type(self._key.to_class,ptr=True),
            'name': 'item',
            'read_only': self.read_only
            }, {
            'type': self.type(self._key.to_class,ptr=True),
            'name': 'new_item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_AVLTREE_REPLACE_CONTENT(self):
        return self.METHOD_AVLTREE_REPLACE.__doc__

    # {to}* {from}::get_first_{alias_to}() const
    @macro_method_complete
    def METHOD_AVLTREE_GETFIRST(self):
        """
    assert(this);

    {to_type}* result = {prefix_from}top_{alias_to};
    if (result)
    {{
        while (result->{prefix_to}left_{alias_from})
        {{
            result = result->{prefix_to}left_{alias_from};
        }}
    }}

    return result;
    """
        return [{
            'type' : self.type(self._key.to_class, ptr=True),
            'access' : 'public',
            'name' : self.format('get_first_{alias_to}'),
            'constmethod' : True,
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_AVLTREE_GETFIRST_CONTENT(self):
        return self.METHOD_AVLTREE_GETFIRST.__doc__


    # {to}* {from}::get_last_{alias_to}() const
    @macro_method_complete
    def METHOD_AVLTREE_GETLAST(self):
        """
    assert(this);

    {to_type}* result = {prefix_from}top_{alias_to};
    if (result)
    {{
        while (result->{prefix_to}right_{alias_from})
        {{
            result = result->{prefix_to}right_{alias_from};
        }}
    }}

    return result;
    """
        return [{
            'type' : self.type(self._key.to_class, ptr=True),
            'access' : 'public',
            'name' : self.format('get_last_{alias_to}'),
            'constmethod' : True,
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            }, ]

    @macro_method
    def METHOD_AVLTREE_GETLAST_CONTENT(self):
        return self.METHOD_AVLTREE_GETLAST.__doc__

    # {to}* {from}::get_next_{alias_to}({to}* pos) const
    @macro_method_complete
    def METHOD_AVLTREE_GETNEXT(self):
        """
    assert(this);

    {to_type}* result = 0;
    if (pos == nullptr)
    {{
        result = {prefix_from}top_{alias_to};
        if (result)
        {{
            while (result->{prefix_to}left_{alias_from})
            {{
                result = result->{prefix_to}left_{alias_from};
            }}
        }}
    }}
    else
    {{
        assert(pos->{prefix_to}ref_{alias_from} == this);

        if (pos->{prefix_to}right_{alias_from})
        {{
            result = pos->{prefix_to}right_{alias_from};
            while (result->{prefix_to}left_{alias_from})
            {{
                result = result->{prefix_to}left_{alias_from};
            }}
        }}
        else
        {{
            result = pos->{prefix_to}parent_{alias_from};
            while (result && result->{prefix_to}right_{alias_from} == pos)
            {{
                pos = result;
                result = pos->{prefix_to}parent_{alias_from};
            }}
        }}
    }}

    return result;
    """
        return [{
            'type' : self.type(self._key.to_class, ptr=True),
            'access': 'public',
            'name' : self.format('get_next_{alias_to}'),
            'constmethod': True,
            'inline': False,
            'parent': self,
            'read_only': self.read_only
            }, {
            'type': self.type(self._key.to_class,ptr=True),
            'name': 'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_AVLTREE_GETNEXT_CONTENT(self):
        return self.METHOD_AVLTREE_GETNEXT.__doc__

    # {to}* {from}::get_prev_{alias_to}({to}* pos) const
    @macro_method_complete
    def METHOD_AVLTREE_GETPREV(self):
        """
    assert(this);

    {to_type}* result = nullptr;
    if (pos == nullptr)
    {{
        result = {prefix_from}top_{alias_to};
        if (result)
        {{
            while (result->{prefix_to}right_{alias_from})
            {{
                result = result->{prefix_to}right_{alias_from};
            }}
        }}
    }}
    else
    {{
        assert(pos->{prefix_to}ref_{alias_from} == this);

        if (pos->{prefix_to}left_{alias_from})
        {{
            result = pos->{prefix_to}left_{alias_from};
            while (result->{prefix_to}right_{alias_from})
            {{
                result = result->{prefix_to}right_{alias_from};
            }}
        }}
        else
        {{
            result = pos->{prefix_to}parent_{alias_from};
            while (result && result->{prefix_to}left_{alias_from} == pos)
            {{
                pos = result;
                result = pos->{prefix_to}parent_{alias_from};
            }}
        }}
    }}

    return result;
    """
        return [{
            'type': self.type(self._key.to_class, ptr=True),
            'access': 'public',
            'name': self.format('get_prev_{alias_to}'),
            'constmethod': True,
            'inline': False,
            'parent': self,
            'read_only': self.read_only
            } , {
            'type': self.type(self._key.to_class,ptr=True),
            'name': 'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_AVLTREE_GETPREV_CONTENT(self):
        return self.METHOD_AVLTREE_GETPREV.__doc__

    # int {from}::get_{alias_to}_count() const
    @macro_method_complete
    def METHOD_AVLTREE_GETCOUNT(self):
        """
    assert(this);
    return {prefix_from}count_{alias_to};
    """
        return ({
            'type': self.type('size_t'),
            'access': 'public',
            'name': self.format('get_{alias_to}_count'),
            'inline': False,
            'parent': self,
            'read_only': self.read_only,
            'constmethod': True
            }, )

    @macro_method
    def METHOD_AVLTREE_GETCOUNT_CONTENT(self):
        return self.METHOD_AVLTREE_GETCOUNT.__doc__

    def METHOD_CRITICAL_AVLTREE_FIND(self):
        # {to}* {from}::find_{alias_to}(type value) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('find_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_FIND_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self._key.member.type_instance,
            name='value',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_FIND_CONTENT(self):
        return self.METHOD_AVLTREE_FIND_CONTENT()


    def METHOD_CRITICAL_AVLTREE_ADD(self):
        # void {from}::add_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_AVLTREE_ADD({member}, {from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('add_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_ADD_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_ADD_CONTENT(self):
        return self.METHOD_AVLTREE_ADD_CONTENT()

    def METHOD_CRITICAL_AVLTREE_REMOVE(self):
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('remove_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_REMOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_REMOVE_CONTENT(self):
        return self.METHOD_AVLTREE_REMOVE_CONTENT()

    def METHOD_CRITICAL_AVLTREE_REMOVEALL(self):
        MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('remove_all_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_REMOVEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_REMOVEALL_CONTENT(self):
        return self.METHOD_AVLTREE_REMOVEALL_CONTENT()

    def METHOD_CRITICAL_AVLTREE_DELETEALL(self):
        MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('delete_all_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_DELETEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_DELETEALL_CONTENT(self):
        return self.METHOD_AVLTREE_DELETEALL_CONTENT()

    def METHOD_CRITICAL_AVLTREE_REPLACE(self):
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('replace_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_REPLACE_CONTENT(self):
        return self.METHOD_AVLTREE_REPLACE_CONTENT()

    def METHOD_CRITICAL_AVLTREE_GETFIRST(self):
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_first_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_GETFIRST_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_GETFIRST_CONTENT(self):
        return self.METHOD_AVLTREE_GETFIRST_CONTENT()

    def METHOD_CRITICAL_AVLTREE_GETLAST(self):
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_last_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_GETLAST_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_GETLAST_CONTENT(self):
        return self.METHOD_AVLTREE_GETLAST_CONTENT()

    def METHOD_CRITICAL_AVLTREE_GETNEXT(self):
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_next_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_GETNEXT_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_GETNEXT_CONTENT(self):
        return self.METHOD_AVLTREE_GETNEXT_CONTENT()

    def METHOD_CRITICAL_AVLTREE_GETPREV(self):
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_prev_{alias_to}'),
            content=self.METHOD_CRITICAL_AVLTREE_GETPREV_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_GETPREV_CONTENT(self):
        return self.METHOD_AVLTREE_GETPREV_CONTENT()

    def METHOD_CRITICAL_AVLTREE_GETCOUNT(self):
        MemberMethod(
            type=self.type('size_t'),
            access='public',
            name=self.format('get_{alias_to}_count'),
            content=self.METHOD_CRITICAL_AVLTREE_GETCOUNT_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_AVLTREE_GETCOUNT_CONTENT(self):
        return self.METHOD_AVLTREE_GETCOUNT_CONTENT()

    def METHOD_CRITICAL_MULTI_ADDAFTER(self):
        #
        # void {from}::add_{alias_to}_after({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_ADDAFTER({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('add_{alias_to}_after'),
            content=self.METHOD_CRITICAL_MULTI_ADDAFTER_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_ADDAFTER_CONTENT(self):
        return self.METHOD_MULTI_ADDAFTER_CONTENT()

    def METHOD_CRITICAL_MULTI_ADDBEFORE(self):
        #
        # void {from}::add_{alias_to}_before({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_ADDBEFORE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('add_{alias_to}_before'),
            content=self.METHOD_CRITICAL_MULTI_ADDBEFORE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_ADDBEFORE_CONTENT(self):
        return self.METHOD_MULTI_ADDBEFORE_CONTENT()

    def METHOD_CRITICAL_MULTI_DELETEALL(self):
        # void {from}::delete_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_DELETEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('delete_all_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_DELETEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_DELETEALL_CONTENT(self):
        return self.METHOD_MULTI_DELETEALL_CONTENT()

    def METHOD_CRITICAL_MULTI_GETCOUNT(self):
        #
        # size_t {from}::get_{alias_to}_count() const
        # {
        #     METHOD_MULTI_GETCOUNT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        MemberMethod(
            type=self.type('size_t'),
            access='public',
            constmethod=True,
            name=self.format('get_{alias_to}_count'),
            content=self.METHOD_CRITICAL_MULTI_GETCOUNT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_GETCOUNT_CONTENT(self):
        return self.METHOD_MULTI_GETCOUNT_CONTENT()

    def METHOD_CRITICAL_MULTI_GETFIRST(self):
        #
        # {to}* {from}::get_first_{alias_to}(self): const
        # {
        #     METHOD_MULTI_GETFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            constmethod=True,
            name=self.format('get_first_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_GETFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_GETFIRST_CONTENT(self):
        return self.METHOD_MULTI_GETFIRST_CONTENT()

    def METHOD_CRITICAL_MULTI_GETLAST(self):
        # {to}* {from}::get_last_{alias_to}() const
        # {
        #     METHOD_MULTI_GETLAST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            constmethod=True,
            name=self.format('get_last_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_GETLAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_GETLAST_CONTENT(self):
        return self.METHOD_MULTI_GETLAST_CONTENT()

    def METHOD_CRITICAL_MULTI_GETNEXT(self):
        # {to}* {from}::get_next_{alias_to}({to}* pos) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            constmethod=True,
            name=self.format('get_next_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_GETNEXT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_GETNEXT_CONTENT(self):
        return self.METHOD_MULTI_GETNEXT_CONTENT()

    def METHOD_CRITICAL_MULTI_GETPREV(self):
        # {to}* {from}::get_prev_{alias_to}({to}* pos) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_GETPREV({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            constmethod=True,
            name=self.format('get_prev_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_GETPREV_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_GETPREV_CONTENT(self):
        return self.METHOD_MULTI_GETPREV_CONTENT()

    def METHOD_CRITICAL_MULTI_MOVEAFTER(self):
        # void {from}::move_{alias_to}_after({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_MOVEAFTER({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_after'),
            content=self.METHOD_CRITICAL_MULTI_MOVEAFTER_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_MOVEAFTER_CONTENT(self):
        return self.METHOD_MULTI_MOVEAFTER_CONTENT()

    def METHOD_CRITICAL_MULTI_MOVEBEFORE(self):
        # void {from}::move_{alias_to}_before({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_MOVEBEFORE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_before'),
            content=self.METHOD_CRITICAL_MULTI_MOVEBEFORE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_MOVEBEFORE_CONTENT(self):
        return self.METHOD_MULTI_MOVEBEFORE_CONTENT()

    def METHOD_CRITICAL_MULTI_MOVEFIRST(self):
        # void {from}::move_{alias_to}_first({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_MOVEFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_first'),
            content=self.METHOD_CRITICAL_MULTI_MOVEFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_MOVEFIRST_CONTENT(self):
        return self.METHOD_MULTI_MOVEFIRST_CONTENT()

    def METHOD_CRITICAL_MULTI_MOVELAST(self):
        # void {from}::move_{alias_to}_last({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_MOVELAST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_last'),
            content=self.METHOD_CRITICAL_MULTI_MOVELAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_MOVELAST_CONTENT(self):
        return self.METHOD_MULTI_MOVELAST_CONTENT()

    def METHOD_CRITICAL_MULTI_ADDFIRST(self):
        # void {from}::add_{alias_to}_first({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_ADDFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('add_{alias_to}_first'),
            content=self.METHOD_CRITICAL_MULTI_ADDFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_ADDFIRST_CONTENT(self):
        return self.METHOD_MULTI_ADDFIRST_CONTENT()

    def METHOD_CRITICAL_MULTI_ADDLAST(self):
        #
        # void {from}::add_{alias_to}_last({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_ADDLAST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('add_{alias_to}_last'),
            content=self.METHOD_CRITICAL_MULTI_ADDLAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_ADDLAST_CONTENT(self):
        return self.METHOD_MULTI_ADDLAST_CONTENT()

    def METHOD_CRITICAL_MULTI_REMOVE(self):
        # void {from}::remove_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_REMOVE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('remove_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_REMOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_REMOVE_CONTENT(self):
        return self.METHOD_MULTI_REMOVE_CONTENT()

    def METHOD_CRITICAL_MULTI_REMOVEALL(self):
        # void {from}::remove_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('remove_all_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_REMOVEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_REMOVEALL_CONTENT(self):
        return self.METHOD_MULTI_REMOVEALL_CONTENT()

    def METHOD_CRITICAL_MULTI_REPLACE(self):
        #
        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_REPLACE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('replace_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_REPLACE_CONTENT(self):
        return self.METHOD_MULTI_REPLACE_CONTENT()

    def METHOD_CRITICAL_MULTI_SORT(self):
        # void {from}::sort_{alias_to}(int (*compare)({to}*, {to}*))
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_MULTI_SORT({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('sort_{alias_to}'),
            content=self.METHOD_CRITICAL_MULTI_SORT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type('int', funptr=True, argdecl=self.format('({to_type}*,{to_type}*)')),
            name='compare',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_MULTI_SORT_CONTENT(self):
        return self.METHOD_MULTI_SORT_CONTENT()

    def METHOD_CRITICAL_SINGLE_SET(self):
        # void {from}::set_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_SINGLE_ADD({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('set_{alias_to}'),
            content=self.METHOD_CRITICAL_SINGLE_SET_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_SINGLE_SET_CONTENT(self):
        return self.METHOD_SINGLE_SET_CONTENT()

    def METHOD_CRITICAL_SINGLE_MOVE(self):
        # void {from}::move_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_SINGLE_MOVE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}'),
            content=self.METHOD_CRITICAL_SINGLE_MOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_SINGLE_MOVE_CONTENT(self):
        return self.METHOD_SINGLE_MOVE_CONTENT()

    def METHOD_CRITICAL_SINGLE_CLEAR(self):
        # void {from}::clear_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_SINGLE_REMOVE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('clear_{alias_to}'),
            content=self.METHOD_CRITICAL_SINGLE_CLEAR_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_SINGLE_CLEAR_CONTENT(self):
        return self.METHOD_SINGLE_CLEAR_CONTENT()

    def METHOD_CRITICAL_SINGLE_REPLACE(self):
        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_SINGLE_REPLACE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}'),
            content=self.METHOD_CRITICAL_SINGLE_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_SINGLE_REPLACE_CONTENT(self):
        return self.METHOD_SINGLE_REPLACE_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_ADDFIRST(self, access_='public'):
        # void {from}::add_{alias_to}_first({to}* item)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_ADDFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            static=True,
            name=self.format('add_{alias_to}_first'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_ADDFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_ADDFIRST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDFIRST_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_ADDLAST(self, access_='public'):
        # void {from}::add_{alias_to}_last({to}* item)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_ADDLAST({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            static=True,
            name=self.format('add_{alias_to}_last'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_ADDLAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_ADDLAST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDLAST_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_ADDAFTER(self, access_='public'):
        # void {from}::add_{alias_to}_after({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_ADDAFTER({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            static=True,
            name=self.format('add_{alias_to}_after'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_ADDAFTER_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_ADDAFTER_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDAFTER_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_ADDBEFORE(self, access_='public'):
        # void {from}::add_{alias_to}_before({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_ADDBEFORE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            static=True,
            name=self.format('add_{alias_to}_before'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_ADDBEFORE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_ADDBEFORE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDBEFORE_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_REMOVE(self, access_='public'):
        # void {from}::remove_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_REMOVE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=access_,
            name=self.format('remove_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_REMOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_REMOVE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_REMOVE_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_REMOVEALL(self):
        # void {from}::remove_all_{alias_to}(self):
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name=self.format('remove_all{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_REMOVEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_REMOVEALL_CONTENT(self):
        return self.METHOD_STATIC_MULTI_REMOVEALL_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_DELETEALL(self):
        # void {from}::delete_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_DELETEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name=self.format('delete_all{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_DELETEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_DELETEALL_CONTENT(self):
        return self.METHOD_STATIC_MULTI_DELETEALL_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_REPLACE(self, access_='public'):
        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_REPLACE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            static=True,
            name=self.format('replace_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_REPLACE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_REPLACE_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_GETFIRST(self):
        # {to}* {from}::get_first_{alias_to}()
        # {
        #     METHOD_STATIC_MULTI_GETFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            static=True,
            name=self.format('get_first_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_GETFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_GETFIRST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETFIRST_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_GETLAST(self):
        # {to}* {from}::get_last_{alias_to}()
        # {
        #     METHOD_STATIC_MULTI_GETLAST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            static=True,
            name=self.format('get_last_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_GETLAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_GETLAST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETLAST_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_GETNEXT(self):
        # {to}* {from}::get_next_{alias_to}({to}* pos)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            static=True,
            name=self.format('get_next_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_GETNEXT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_GETNEXT_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETNEXT_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_GETPREV(self):
        # {to}* {from}::get_prev_{alias_to}({to}* pos)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_GETPREV({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            static=True,
            name=self.format('get_prev_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_GETPREV_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_GETPREV_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETPREV_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_GETCOUNT(self):
        # size_t {from}::get_{alias_to}_count()
        # {
        #     METHOD_STATIC_MULTI_GETCOUNT({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type('size_t'),
            access='public',
            static=True,
            name=self.format('get_{alias_to}_count'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_GETCOUNT_CONTENT(),
            parent=self,
            read_only=self.read_only,
            constmethod=True
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_GETCOUNT_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETCOUNT_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_INCLUDES(self):
        # bool {from}::includes_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_INCLUDES({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('bool'),
            access='public',
            static=True,
            name=self.format('includes_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_INCLUDES_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_INCLUDES_CONTENT(self):
        return self.METHOD_STATIC_MULTI_INCLUDES_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_MOVEFIRST(self):
        # void {from}::move_{alias_to}_first({to}* item)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_MOVEFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name=self.format('move_{alias_to}_first'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_MOVEFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_MOVEFIRST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVEFIRST_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_MOVELAST(self):
        # void {from}::move_{alias_to}_last({to}* item)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_MOVELAST({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name=self.format('move_{alias_to}_last'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_MOVELAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_MOVELAST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVELAST_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_MOVEAFTER(self):
        # void {from}::move_{alias_to}_after({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_MOVEAFTER({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name=self.format('move_{alias_to}_after'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_MOVEAFTER_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_MOVEAFTER_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVEAFTER_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_MOVEBEFORE(self):
        # void {from}::move_{alias_to}_before({to}* item, {to}* pos)
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_MOVEBEFORE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name=self.format('move_{alias_to}_before'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_MOVEBEFORE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_MOVEBEFORE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVEBEFORE_CONTENT()

    def METHOD_CRITICAL_STATIC_MULTI_SORT(self):
        # void {from}::sort_{alias_to}(int (*compare)({to}*, {to}*))
        # {
        #     critical_section_lock lock({from}::_critical_section_{alias_to});
        #     METHOD_STATIC_MULTI_SORT({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name=self.format('sort_{alias_to}'),
            content=self.METHOD_CRITICAL_STATIC_MULTI_SORT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type('int', funptr=True,argdecl=self.format('({to_type}*,{to_type}*)')),
            name='compare',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_STATIC_MULTI_SORT_CONTENT(self):
        return self.METHOD_STATIC_MULTI_SORT_CONTENT()

    def METHOD_ITERATOR_MULTI_ASSIGN(self, iterator):
        # inline {alias_to}_iterator& operator= (const {alias_to}_iterator& iterator)
        # {
        #   __exit__();
        #   __init__(iterator__._iter_{alias_from}, iterator__._method, iterator__._ref_{alias_to});
        #   return *this;
        # }
        method = MemberMethod(
            type=self.type(iterator,ref=True),
            access='public',
            inline=True,
            name='operator =',
            content=self.METHOD_ITERATOR_MULTI_ASSIGN_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(iterator,ref=True,const=True),
            name='iterator',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_ASSIGN_CONTENT(self):
        return """
    __exit__();
    __init__(iterator.{prefix_from}iter_{alias_from}, iterator.{prefix_from}method, iterator.{prefix_from}ref_{alias_to});
    return *this;
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_ASSIGN(self, iterator):
        self.METHOD_ITERATOR_MULTI_ASSIGN(iterator)

    def METHOD_ITERATOR_MULTI_CAST(self, iterator):
        #         operator {to_type}*() { return _ref_{alias_to}; }
        MemberMethod(
            type=self.type(''),
            access='public',
            inline=True,
            name=self.format('operator {to_type}*'),
            content=self.METHOD_ITERATOR_MULTI_CAST_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_CAST_CONTENT(self):
        return """
    return {prefix_from}ref_{alias_to};
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_CAST(self, iterator):
        self.METHOD_ITERATOR_MULTI_CAST(iterator)

    def METHOD_ITERATOR_MULTI_CHECK_1(self, iterator):
        """This method checks iterators when removing an element"""
        #
        # void {from}::{alias_to}_iterator::check({to}* item_{alias_to})
        # {
        #     for ({alias_to}_iterator* item = _iter_{alias_from}->_first_{alias_to}_iterator; item; item = item->_next)
        #     {
        #         if (item->_prev_{alias_to} == item_{alias_to})
        #         {
        #             item->_prev_{alias_to} = item->_iter_{alias_from}->get_next_{alias_to}(item->_prev_{alias_to});
        #             item->_ref_{alias_to} = 0;
        #         }
        #         if (item->_next_{alias_to} == item_{alias_to})
        #         {
        #             item->_next_{alias_to} = item->_iter_{alias_from}->get_prev_{alias_to}(item->_next_{alias_to});
        #             item->_ref_{alias_to} = 0;
        #         }
        #     }
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='protected',
            name='check',
            content=self.METHOD_ITERATOR_MULTI_CHECK_1_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_CHECK_1_CONTENT(self):
        return """
    for ({alias_to}_iterator* item =  _iter_{alias_from}->{prefix_from}first_{alias_to}_iterator; item; item = item->{prefix_from}next)
    {{
        if (item->{prefix_from}prev_{alias_to} == item_{alias_to})
        {{
            item->{prefix_from}prev_{alias_to} = item->{prefix_from}iter_{alias_from}->get_next_{alias_to}(item->{prefix_from}prev_{alias_to});
            item->{prefix_from}ref_{alias_to} = nullptr;
        }}
        if (item->{prefix_from}next_{alias_to} == item_{alias_to})
        {{
            item->{prefix_from}next_{alias_to} = item->{prefix_from}iter_{alias_from}->get_prev_{alias_to}(item->{prefix_from}next_{alias_to});
            item->{prefix_from}ref_{alias_to} = nullptr;
        }}
    }}
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_CHECK_1(self, iterator):
        method = MemberMethod(
            type=self.type('void'),
            access='protected',
            name='check',
            content=self.METHOD_ITERATOR_CRITICAL_MULTI_CHECK_1_CONTENT(),
            parent=iterator,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
        )

    @critical_iterator_macro_method
    def METHOD_ITERATOR_CRITICAL_MULTI_CHECK_1_CONTENT(self):
        return self.METHOD_ITERATOR_MULTI_CHECK_1_CONTENT()

    def METHOD_ITERATOR_MULTI_CHECK_2(self,iterator):
        #
        # void {from}::{alias_to}_iterator::check({to}* item_{alias_to}, {to}* new_item_{alias_to})
        # {
        #     for ({alias_to}_iterator* item =  _iter_{alias_from}->_first_{alias_to}_iterator; item; item = item->_next)
        #     {
        #         if (item->_ref_{alias_to} == item_{alias_to})
        #         {
        #             item->_ref_{alias_to} = item->_prev_{alias_to} = item->_next_{alias_to} = new_item_{alias_to};
        #         }
        #         if (item->_prev_{alias_to} == item_{alias_to})
        #         {
        #             item->_prev_{alias_to} = new_item_{alias_to};
        #             item->_ref_{alias_to} = 0;
        #         }
        #         if (item->_next_{alias_to} == item_{alias_to})
        #         {
        #             item->_next_{alias_to} = new_item_{alias_to};
        #             item->_ref_{alias_to} = 0;
        #         }
        #     }
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='protected',
            name='check',
            content=self.METHOD_ITERATOR_MULTI_CHECK_2_CONTENT(),
            parent = iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('new_item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_CHECK_2_CONTENT(self):
        return """
    for ({alias_to}_iterator* item = _iter_{alias_from}->{prefix_from}first_{alias_to}_iterator; 
        item != nullptr ; item = item->{prefix_from}next)
    {{
        if (item->{prefix_from}ref_{alias_to} == item_{alias_to})
        {{
            item->{prefix_from}ref_{alias_to} = \
            item->{prefix_from}prev_{alias_to} = \
            item->{prefix_from}next_{alias_to} = new_item_{alias_to};
        }}
        if (item->{prefix_from}prev_{alias_to} == item_{alias_to})
        {{
            item->{prefix_from}prev_{alias_to} = new_item_{alias_to};
            item->{prefix_from}ref_{alias_to} = nullptr;
        }}
        if (item->{prefix_from}next_{alias_to} == item_{alias_to})
        {{
            item->{prefix_from}next_{alias_to} = new_item_{alias_to};
            item->{prefix_from}ref_{alias_to} = nullptr;
        }}
    }}
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_CHECK_2(self, iterator):
        method = MemberMethod(
            type=self.type('void'),
            access='protected',
            name='check',
            content=self.METHOD_ITERATOR_CRITICAL_MULTI_CHECK_2_CONTENT(),
            parent = iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('new_item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )

    @critical_iterator_macro_method
    def METHOD_ITERATOR_CRITICAL_MULTI_CHECK_2_CONTENT(self):
        return self.METHOD_ITERATOR_MULTI_CHECK_2_CONTENT()

    def METHOD_ITERATOR_MULTI_INIT(self, iterator):
        # void {from}::{alias_to}_iterator::__init__(
        #     const {from}* iter_{alias_from},
        #     int ({to}::*method)() const,
        #     {to}* ref_{alias_to})
        # {
        #     assert(iter_{alias_from});
        #
        #     _iter_{alias_from} = iter_{alias_from};
        #     _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ref_{alias_to};
        #     _prev = ({alias_to}_iterator*)0;
        #     _next = ({alias_to}_iterator*)0;
        #     _method = method;
        #     if (_iter_{alias_from}->_last_{alias_to}_iterator)
        #     {
        #         _prev = _iter_{alias_from}->_last_{alias_to}_iterator;
        #         _prev->_next = this;
        #         _iter_{alias_from}->_last_{alias_to}_iterator = this;
        #     }
        #     else
        #         _iter_{alias_from}->_first = _iter_{alias_from}->_last = this;
        # }

        method = MemberMethod(
            type=self.type('void'),
            access='private',
            name='__init__',
            content=self.METHOD_ITERATOR_MULTI_INIT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self.inner_class,ptr=True,const=True),
            name=self.format('iter_{alias_from}'),
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('ref_{alias_to}'),
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_MULTI_INIT_CONTENT(self):
        return """
    assert(iter_{alias_from});
 
    {prefix_from}iter_{alias_from} = iter_{alias_from};
    {prefix_from}ref_{alias_to} = {prefix_from}prev_{alias_to} = {prefix_from}next_{alias_to} = ref_{alias_to};
    {prefix_from}prev = nullptr;
    {prefix_from}next = nullptr;
    {prefix_from}method = method;
    if ( {prefix_from}iter_{alias_from}->{prefix_from}last_{alias_to}_iterator != nullptr )
    {{
        {prefix_from}prev = {prefix_from}iter_{alias_from}->{prefix_from}last_{alias_to}_iterator;
        {prefix_from}prev->{prefix_from}next = this;
        {prefix_from}iter_{alias_from}->{prefix_from}last_{alias_to}_iterator = this;
    }}
    else
    {{
        {prefix_from}iter_{alias_from}->{prefix_from}first_{alias_to}_iterator  = \
{prefix_from}iter_{alias_from}->{prefix_from}last_{alias_to}_iterator  = this;
    }}
    """

    def METHOD_ITERATOR_MULTI_EXIT(self, iterator):
        # void {from}::{alias_to}_iterator::__exit__()
        # {
        #     if (_next)
        #         _next->_prev = _prev;
        #     else
        #         _iter_{alias_from}->_last_{alias_to}_iterator = _prev;
        #
        #     if (_prev)
        #         _prev->_next = _next;
        #     else
        #         _iter_{alias_from}->_first_{alias_to}_iterator = _next;
        # }
        method = ExitMethod(
            type=self.type('void'),
            content=self.METHOD_ITERATOR_MULTI_EXIT_CONTENT(),
            parent = iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_EXIT_CONTENT(self):
        return """
        if ({prefix_from}next)
        {{
            {prefix_from}next->{prefix_from}prev = {prefix_from}prev;
        }}
        else
        {{
             {prefix_from}iter_{alias_from}->{prefix_from}last_{alias_to}_iterator = {prefix_from}prev;
        }}

        if ({prefix_from}prev)
        {{
            {prefix_from}prev->{prefix_from}next = {prefix_from}next;
        }}
        else
        {{
            {prefix_from}iter_{alias_from}->{prefix_from}first_{alias_to}_iterator = {prefix_from}next;
        }}
        """

    def METHOD_ITERATOR_MULTI_CTOR(self, iterator):
        # inline  {from}::{alias_to}_iterator::{alias_to}_iterator(
        #     const {from}* iter_{alias_from},
        #     int ({to}::*method)() const,
        #     {to}* ref_{alias_to})
        # {
        #     __init__(iter_{alias_from}, method, ref_{alias_to});
        # }
        ctor = Constructor(
            parent=iterator,
            access='public',
            inline=True,
            content=self.METHOD_ITERATOR_MULTI_CTOR_CONTENT(),
            note=self.format("""Construct a new iterator {from}->{to}.
            This constructor takes the arguments:
            * {from}* iter_{alias_from} : pointer to the object to be iterated.
            * bool ({to}::*method)() : pointer to member function that acts as filter. Optional.
            * {to}* ref_{alias_to} : pointer to take as start for iterator."""),
            read_only=self.read_only
            )
        Argument(
            type=self.type(self.inner_class,ptr=True,const=True),
            name=self.format('iter_{alias_from}'),
            parent=ctor,
            read_only=self.read_only
        )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('ref_{alias_to}'),
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_MULTI_CTOR_CONTENT(self):
        return """
     __init__(iter_{alias_from}, method, ref_{alias_to});
    """

    def METHOD_ITERATOR_MULTI_CTOR_REF(self, iterator):
        #
        # inline {from}::{alias_to}_iterator::{alias_to}_iterator(
        #     const {from}& iter_{alias_from},
        #     int ({to}::*method)() const,
        #     {to}* ref_{alias_to})
        # {
        #     __init__(&iter_{alias_from}, method, ref_{alias_to});
        # }
        ctor = Constructor(
            parent=iterator,
            access='public',
            inline=True,
            content=self.METHOD_ITERATOR_MULTI_CTOR_REF_CONTENT(),
            read_only=self.read_only
            )
        Argument(
            type=self.type(self.inner_class,ref=True,const=True),
            name=self.format('iter_{alias_from}'),
            parent=ctor,
            read_only=self.read_only
        )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('ref_{alias_to}'),
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_MULTI_CTOR_REF_CONTENT(self):
        return """
         __init__(&iter_{alias_from}, method, ref_{alias_to});
        """

    def METHOD_ITERATOR_MULTI_CTOR_COPY(self, iterator):
        #
        # inline {from}::{alias_to}_iterator::{alias_to}_iterator(
        #     const {alias_to}_iterator& iterator)
        # {
        #     __init__(iterator__._iter_{alias_from}, iterator__._method, iterator__._ref_{alias_to});
        # }
        #
        ctor = Constructor(
            parent=iterator,
            access='public',
            inline=True,
            content=self.METHOD_ITERATOR_MULTI_CTOR_COPY_CONTENT(),
            read_only=self.read_only
            )
        Argument(
            type=self.type(iterator,ref=True, const=True),
            name='iterator',
            parent=ctor,
            read_only=self.read_only
        )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_MULTI_CTOR_COPY_CONTENT(self):
        return """
    __init__(iterator.{prefix_from}iter_{alias_from}, iterator.{prefix_from}method, iterator.{prefix_from}ref_{alias_to});
    """

    def METHOD_ITERATOR_MULTI_DTOR(self, iterator):
        # {from}::{alias_to}_iterator::~{alias_to}_iterator()
        # {
        #   __exit__();
        # }
        Destructor(
            parent=iterator,
            access='public',
            virtual=True,
            content=self.METHOD_ITERATOR_MULTI_DTOR_CONTENT(),
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_MULTI_DTOR_CONTENT(self):
        return """
    __exit__();
    """

    def METHOD_ITERATOR_MULTI_GET(self, iterator):
        #         {to}* get() { return _ref_{alias_to}; }
        MemberMethod(
            type=self.type(self._key.to_class,ptr=True),
            name="get",
            access='public',
            inline=True,
            content=self.METHOD_ITERATOR_MULTI_GET_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_GET_CONTENT(self):
        return """
    return {prefix_from}ref_{alias_to};
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_GET(self, iterator):
        self.METHOD_ITERATOR_MULTI_GET(iterator)

    def METHOD_ITERATOR_MULTI_ISFIRST(self, iterator):
        #         int is_first() { return (_iter_{alias_from}->get_first_{alias_to}() == _ref_{alias_to}); }
        MemberMethod(
            type=self.type('bool'),
            name="is_first",
            access='public',
            inline=True,
            constmethod=True,
            content=self.METHOD_ITERATOR_MULTI_ISFIRST_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_ISFIRST_CONTENT(self):
        return """
    return ({prefix_from}iter_{alias_from}->get_first_{alias_to}() == {prefix_from}ref_{alias_to});
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_ISFIRST(self, iterator):
        self.METHOD_ITERATOR_MULTI_ISFIRST(iterator)

    def METHOD_ITERATOR_MULTI_ISLAST(self, iterator):
        #         int is_last() { return (_iter_{alias_from}->get_last_{alias_to}() == _ref_{alias_to}); }
        MemberMethod(
            type=self.type('bool'),
            name="is_last",
            access='public',
            inline=True,
            constmethod=True,
            content=self.METHOD_ITERATOR_MULTI_ISLAST_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_ISLAST_CONTENT(self):
        return """
    return ({prefix_from}iter_{alias_from}->get_last_{alias_to}() == {prefix_from}ref_{alias_to});
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_ISLAST(self, iterator):
        self.METHOD_ITERATOR_MULTI_ISLAST(iterator)

    def METHOD_ITERATOR_MULTI_NEXT(self,iterator):
        #         {to}* operator++ ()
        #         {
        #             _next_{alias_to} = _iter_{alias_from}->get_next_{alias_to}(_next_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_next_{alias_to} && !(_next_{alias_to}->*_method)())
        #                     _next_{alias_to} = _iter_{alias_from}->get_next_{alias_to}(_next_{alias_to});
        #             }
        #             _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to};
        #             return _ref_{alias_to};
        #         }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            inline=True,
            name='operator ++',
            content=self.METHOD_ITERATOR_MULTI_NEXT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_NEXT_CONTENT(self):
        return """
    {prefix_from}next_{alias_to} = {prefix_from}iter_{alias_from}->get_next_{alias_to}({prefix_from}next_{alias_to});
    if ( {prefix_from}method != nullptr )
    {{
        while ({prefix_from}next_{alias_to} != nullptr && !({prefix_from}next_{alias_to}->*{prefix_from}method)())
        {{
            {prefix_from}next_{alias_to} = {prefix_from}iter_{alias_from}->get_next_{alias_to}({prefix_from}next_{alias_to});
        }}
    }}
    {prefix_from}ref_{alias_to} = {prefix_from}prev_{alias_to} = {prefix_from}next_{alias_to};
    return {prefix_from}ref_{alias_to};
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_NEXT(self, iterator):
        self.METHOD_ITERATOR_MULTI_NEXT(iterator)

    def METHOD_ITERATOR_MULTI_PREV(self, iterator):
        #         {to}* operator-- ()
        #         {
        #             _prev_{alias_to} = _iter_{alias_from}->get_prev_{alias_to}(_prev_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_prev_{alias_to} && !(_prev_{alias_to}->*_method)())
        #                     _prev_{alias_to} = _iter_{alias_from}->get_prev_{alias_to}(_prev_{alias_to});
        #             }
        #             _ref_{alias_to} = _next_{alias_to} = _prev_{alias_to};
        #             return _ref_{alias_to};
        #         }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            inline=True,
            name='operator --',
            content=self.METHOD_ITERATOR_MULTI_PREV_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_PREV_CONTENT(self):
        return """
    {prefix_from}prev_{alias_to} = {prefix_from}iter_{alias_from}->get_prev_{alias_to}({prefix_from}prev_{alias_to});
    if ({prefix_from}method != 0)
    {{
        while ({prefix_from}prev_{alias_to} && !({prefix_from}prev_{alias_to}->*{prefix_from}method)())
        {{
            {prefix_from}prev_{alias_to} = {prefix_from}iter_{alias_from}->get_prev_{alias_to}({prefix_from}prev_{alias_to});
        }}
    }}
    {prefix_from}ref_{alias_to} = {prefix_from}next_{alias_to} = {prefix_from}prev_{alias_to};
    return {prefix_from}ref_{alias_to};
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_PREV(self, iterator):
        self.METHOD_ITERATOR_MULTI_PREV(iterator)

    def METHOD_ITERATOR_MULTI_PTR(self, iterator):
        #         {to}* operator-> () { return _ref_{alias_to}; }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            name="operator ->",
            access="public",
            inline=True,
            content=self.METHOD_ITERATOR_MULTI_PTR_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_PTR_CONTENT(self):
        return """
    return {prefix_from}ref_{alias_to};
    """

    def METHOD_ITERATOR_CRITICAL_MULTI_PTR(self, iterator):
        self.METHOD_ITERATOR_MULTI_PTR(iterator)

    def METHOD_ITERATOR_MULTI_RESET(self, iterator):
        #         void reset() { _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ({to}*)0; }
        MemberMethod(
            type=self.type('void'),
            name="reset",
            access="public",
            inline=True,
            content=self.METHOD_ITERATOR_MULTI_RESET_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_MULTI_RESET_CONTENT(self):
        return """
{prefix_from}ref_{alias_to} = {prefix_from}prev_{alias_to} = {prefix_from}next_{alias_to} = nullptr;
"""


    def METHOD_ITERATOR_CRITICAL_MULTI_RESET(self, iterator):
        self.METHOD_ITERATOR_MULTI_RESET(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_INIT(self, iterator):
        # void {from}::{alias_to}_iterator::__init__(int ({to}::*method)() const, {to}* ref_{alias_to})
        # {
        #     _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ref_{alias_to};
        #     _prev = ({alias_to}_iterator*)0;
        #     _next = ({alias_to}_iterator*)0;
        #     _method = method;
        #     if (_last)
        #     {
        #         _last->_next = this;
        #         _prev = _last;
        #         _last = this;
        #     }
        #     else
        #         _first = _last = this;
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='private',
            name='__init__',
            content=self.METHOD_ITERATOR_STATIC_MULTI_INIT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
        )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('ref_{alias_to}'),
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_INIT_CONTENT(self):
        return """
            {prefix_from}ref_{alias_to} = {prefix_from}prev_{alias_to} = {prefix_from}next_{alias_to} = ref_{alias_to};
            {prefix_from}prev = nullptr;
            {prefix_from}next = nullptr;
            {prefix_from}method = method;
            if ( {from}::{prefix_from}last_{alias_to}_iterator != nullptr )
            {{
                {prefix_from}prev = {from}::{prefix_from}last_{alias_to}_iterator;
                {prefix_from}prev->{prefix_from}next = this;
                {from}::{prefix_from}last_{alias_to}_iterator = this;
            }}
            else
            {{
                {from}::{prefix_from}first_{alias_to}_iterator  = \
                {from}::{prefix_from}last_{alias_to}_iterator  = this;
            }}
            """

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_INIT(self, iterator):
        method = MemberMethod(
            type=self.type('void'),
            access='private',
            name='__init__',
            content=self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_INIT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('ref_{alias_to}'),
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )
        # void {from}::{alias_to}_iterator::__init__(
        #     int ({to}::*method)() const,
        #     {to}* ref_{alias_to})
        # {
        #     assert(iter_{alias_from});
        #
        #     _iter_{alias_from} = iter_{alias_from};
        #     _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ref_{alias_to};
        #     _prev = ({alias_to}_iterator*)0;
        #     _next = ({alias_to}_iterator*)0;
        #     _method = method;
        #     if (_iter_{alias_from}->_last_{alias_to}_iterator)
        #     {
        #         _prev = _iter_{alias_from}->_last_{alias_to}_iterator;
        #         _prev->_next = this;
        #         _iter_{alias_from}->_last_{alias_to}_iterator = this;
        #     }
        #     else
        #         _iter_{alias_from}->_first = _iter_{alias_from}->_last = this;
        # }

    @critical_iterator_static_macro_method
    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_INIT_CONTENT(self):
        return self.METHOD_ITERATOR_STATIC_MULTI_INIT_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_EXIT(self, iterator):
        # void {from}::{alias_to}_iterator::__exit__()
        # {
        #     if (_next)
        #         _next->_prev = _prev;
        #     else
        #         {from}::_last_{alias_to}_iterator = _prev;
        #
        #     if (_prev)
        #         _prev->_next = _next;
        #     else
        #        {from}::_first_{alias_to}_iterator = _next;
        # }
        method = ExitMethod(
            type=self.type('void'),
            content=self.METHOD_ITERATOR_STATIC_MULTI_EXIT_CONTENT(),
            parent = iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_EXIT_CONTENT(self):
        return """
        if ({prefix_from}next)
        {{
            {prefix_from}next->{prefix_from}prev = {prefix_from}prev;
        }}
        else
        {{
             {from}::{prefix_from}last_{alias_to}_iterator = {prefix_from}prev;
        }}

        if ({prefix_from}prev)
        {{
            {prefix_from}prev->{prefix_from}next = {prefix_from}next;
        }}
        else
        {{
            {from}::{prefix_from}first_{alias_to}_iterator = {prefix_from}next;
        }}
        """

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_EXIT(self, iterator):
        ExitMethod(
            type=self.type('void'),
            content=self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_EXIT_CONTENT(),
            parent = iterator,
            read_only=self.read_only
            )

    @critical_iterator_static_macro_method
    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_EXIT_CONTENT(self):
        return self.METHOD_ITERATOR_STATIC_MULTI_EXIT_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_CTOR(self, iterator):
        # {from}::{alias_to}_iterator::{alias_to}_iterator(int ({to}::*method)() const, {to}* ref_{alias_to})
        # {
        #     __init__(method, ref_{alias_to});
        # }
        ctor = Constructor(
            parent=iterator,
            access='public',
            content=self.METHOD_ITERATOR_STATIC_MULTI_CTOR_CONTENT(),
            read_only=self.read_only
            )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('ref_{alias_to}'),
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_CTOR_CONTENT(self):
        return """
    __init__(method, ref_{alias_to});
    """

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CTOR(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_CTOR(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_CTOR_COPY(self, iterator):
        # {from}::{alias_to}_iterator::{alias_to}_iterator(
        #   const {alias_to}_iterator& iterator__, bool ({to}::*method)() const)
        # {
        #       __init__(method, iterator__._ref_{alias_to});
        # }
        ctor = Constructor(
            parent=iterator,
            access='public',
            content=self.METHOD_ITERATOR_STATIC_MULTI_CTOR_COPY_CONTENT(),
            read_only=self.read_only
            )
        Argument(
            type=self.type(iterator,ref=True, const=True),
            name='iterator__',
            parent=ctor,
            read_only=self.read_only
        )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=ctor,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_CTOR_COPY_CONTENT(self):
        return """
         __init__(method, iterator__.{prefix_from}ref_{alias_to});
    """

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CTOR_COPY(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_CTOR_COPY(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_DTOR(self, iterator):
        # {from}::{alias_to}_iterator::~{alias_to}_iterator()
        # {
        #     __exit__();
        # }
        Destructor(
            parent=iterator,
            access='public',
            virtual=True,
            content=self.METHOD_ITERATOR_STATIC_MULTI_DTOR_CONTENT(),
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_DTOR_CONTENT(self):
        return """
    __exit__();
    """

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_DTOR(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_DTOR(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_CHECK_1(self, iterator):
        # void {from}::{alias_to}_iterator::check({to}* item_{alias_to})
        # {
        #     for ({alias_to}_iterator* item = {from}::{prefix_from}first_{alias_to}_iterator; item; item = item->_next)
        #     {
        #         if (item->_prev_{alias_to} == item_{alias_to})
        #         {
        #             item->_prev_{alias_to} = {from}::get_next_{alias_to}(item->_prev_{alias_to});
        #             item->_ref_{alias_to} = 0;
        #         }
        #         if (item->_next_{alias_to} == item_{alias_to})
        #         {
        #             item->_next_{alias_to} = {from}::get_prev_{alias_to}(item->_next_{alias_to});
        #             item->_ref_{alias_to} = 0;
        #         }
        #     }
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name='check',
            content=self.METHOD_ITERATOR_STATIC_MULTI_CHECK_1_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_CHECK_1_CONTENT(self):
        return """
    for ({alias_to}_iterator* item = {from}::{prefix_from}first_{alias_to}_iterator; item; item = item->_next)
    {{
        if (item->_prev_{alias_to} == item_{alias_to})
        {{
            item->_prev_{alias_to} = {from}::get_next_{alias_to}(item->_prev_{alias_to});
            item->_ref_{alias_to} = 0;
        }}
        if (item->_next_{alias_to} == item_{alias_to})
        {{
            item->_next_{alias_to} = {from}::get_prev_{alias_to}(item->_next_{alias_to});
            item->_ref_{alias_to} = 0;
        }}
    }}
    """

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_1(self, iterator):
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name='check',
            content=self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_1_CONTENT(),
            parent=iterator,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
        )

    @critical_iterator_static_macro_method
    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_1_CONTENT(self):
        return self.METHOD_ITERATOR_STATIC_MULTI_CHECK_1_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_CHECK_2(self, iterator):
        # void {from}::{alias_to}_iterator::check({to}* item_{alias_to}, {to}* new_item_{alias_to})
        # {
        #     for ({alias_to}_iterator* item = _first; item; item = item->_next)
        #     {
        #         if (item->_ref_{alias_to} == item_{alias_to})
        #         {
        #             item->_ref_{alias_to} = item->_prev_{alias_to} = item->_next_{alias_to} = new_item_{alias_to};
        #         }
        #         if (item->_prev_{alias_to} == item_{alias_to})
        #         {
        #             item->_prev_{alias_to} = new_item_{alias_to};
        #             item->_ref_{alias_to} = 0;
        #         }
        #         if (item->_next_{alias_to} == item_{alias_to})
        #         {
        #             item->_next_{alias_to} = new_item_{alias_to};
        #             item->_ref_{alias_to} = 0;
        #         }
        #     }
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name='check',
            content=self.METHOD_ITERATOR_STATIC_MULTI_CHECK_2_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('new_item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_CHECK_2_CONTENT(self):
        return """
    for ({alias_to}_iterator* item = {from}::{prefix_from}first_{alias_to}_iterator; item; item = item->_next)
    {{
        if (item->_ref_{alias_to} == item_{alias_to})
        {{
            item->_ref_{alias_to} = item->_prev_{alias_to} = item->_next_{alias_to} = new_item_{alias_to};
        }}
        if (item->_prev_{alias_to} == item_{alias_to})
        {{
            item->_prev_{alias_to} = new_item_{alias_to};
            item->_ref_{alias_to} = 0;
        }}
        if (item->_next_{alias_to} == item_{alias_to})
        {{
            item->_next_{alias_to} = new_item_{alias_to};
            item->_ref_{alias_to} = 0;
        }}
    }}
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_2(self, iterator):
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            static=True,
            name='check',
            content=self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_2_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name=self.format('new_item_{alias_to}'),
            parent=method,
            read_only=self.read_only
            )

    @critical_iterator_static_macro_method
    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CHECK_2_CONTENT(self):
        return self.METHOD_ITERATOR_STATIC_MULTI_CHECK_2_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_ASSIGN(self, iterator):
        #         {alias_to}_iterator& operator= (const {alias_to}_iterator& iterator__)
        #         {
        #             _ref_{alias_to} = iterator__._ref_{alias_to};
        #             _prev_{alias_to} = iterator__._prev_{alias_to};
        #             _next_{alias_to} = iterator__._next_{alias_to};
        #             _method = iterator__._method;
        #             return *this;
        #         }
        method = MemberMethod(
            type=self.type(iterator,ref=True),
            access='public',
            inline=True,
            name='operator =',
            content=self.METHOD_ITERATOR_STATIC_MULTI_ASSIGN_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(iterator,ref=True,const=True),
            name='iterator__',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_ASSIGN_CONTENT(self):
        return """
    _ref_{alias_to} = iterator__._ref_{alias_to};
    _prev_{alias_to} = iterator__._prev_{alias_to};
    _next_{alias_to} = iterator__._next_{alias_to};
    _method = iterator__._method;
    return *this;
    """

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ASSIGN(self, iterator):
        method = MemberMethod(
            type=self.type(iterator,ref=True),
            access='public',
            inline=True,
            name='operator =',
            content=self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ASSIGN_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(iterator,ref=True,const=True),
            name='iterator__',
            parent=method,
            read_only=self.read_only
            )

    @critical_iterator_static_macro_method
    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ASSIGN_CONTENT(self):
        return self.METHOD_ITERATOR_STATIC_MULTI_ASSIGN_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_NEXT(self, iterator):
        #         {to}* operator++ ()
        #         {
        #             _next_{alias_to} = {from}::get_next_{alias_to}(_next_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_next_{alias_to} && !(_next_{alias_to}->*_method)())
        #                     _next_{alias_to} = {from}::get_next_{alias_to}(_next_{alias_to});
        #             }
        #             _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to};
        #             return _ref_{alias_to};
        #         }
        MemberMethod(
            type=self.type(self._key.to_class,ptr=True),
            access='public',
            inline=True,
            name='operator ++',
            content=self.METHOD_ITERATOR_STATIC_MULTI_NEXT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_NEXT_CONTENT(self):
        return """
    _next_{alias_to} = {from}::get_next_{alias_to}(_next_{alias_to});
    if (_method != 0)
    {{
        while (_next_{alias_to} && !(_next_{alias_to}->*_method)())
        {{
            _next_{alias_to} = {from}::get_next_{alias_to}(_next_{alias_to});
        }}
    }}
    _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to};
    return _ref_{alias_to};
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_NEXT(self, iterator):
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            inline=True,
            name='operator ++',
            content=self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_NEXT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
        )

    @critical_iterator_static_macro_method
    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_NEXT_CONTENT(self):
        return self.METHOD_ITERATOR_STATIC_MULTI_NEXT_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_PREV(self, iterator):
        #         {to}* operator-- ()
        #         {
        #             _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
        #             if (_method != 0)
        #             {
        #                 while (_prev_{alias_to} && !(_prev_{alias_to}->*_method)())
        #                     _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
        #             }
        #             _ref_{alias_to} = _next_{alias_to} = _prev_{alias_to};
        #             return _ref_{alias_to};
        #         }
        MemberMethod(
            type=self.type(self._key.to_class,ptr=True),
            access='public',
            inline=True,
            name='operator --',
            content=self.METHOD_ITERATOR_STATIC_MULTI_PREV_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_PREV_CONTENT(self):
        return """
    _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
    if (_method != 0)
    {{
        while (_prev_{alias_to} && !(_prev_{alias_to}->*_method)())
        {{
            _prev_{alias_to} = get_prev_{alias_to}(_prev_{alias_to});
        }}
    }}
    _ref_{alias_to} = _next_{alias_to} = _prev_{alias_to};
    return _ref_{alias_to};
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_PREV(self, iterator):
        MemberMethod(
            type=self.type(self._key.to_class,ptr=True),
            access='public',
            inline=True,
            name='operator --',
            content=self.METHOD_ITERATOR_CRITICAL_STATIC_MULTI_PREV_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @critical_iterator_static_macro_method
    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_PREV_CONTENT(self):
        return self.METHOD_ITERATOR_STATIC_MULTI_PREV_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_CAST(self, iterator):
        #         operator {to_type}*() { return _ref_{alias_to}; }
        MemberMethod(
            type=self.type(''),
            access='public',
            inline=True,
            name=self.format('operator {to_type}*'),
            content=self.METHOD_ITERATOR_STATIC_MULTI_CAST_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_CAST_CONTENT(self):
        return """
    return _ref_{alias_to};
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_CAST(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_CAST(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_PTR(self, iterator):
        #         {to}* operator-> () { return _ref_{alias_to}; }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            name="operator ->",
            access="public",
            inline=True,
            content=self.METHOD_ITERATOR_STATIC_MULTI_PTR_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_PTR_CONTENT(self):
        return """
    return _ref_{alias_to};
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_PTR(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_PTR(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_GET(self, iterator):
        #         {to}* get() { return _ref_{alias_to}; }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            name="get",
            access="public",
            inline=True,
            content=self.METHOD_ITERATOR_STATIC_MULTI_GET_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_GET_CONTENT(self):
        return """
    return _ref_{alias_to};
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_GET(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_GET(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_RESET(self, iterator):
        #         void reset() { _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ({to}*)0; }
        #
        MemberMethod(
            type=self.type('void'),
            access='public',
            inline=True,
            name='reset',
            content=self.METHOD_ITERATOR_STATIC_MULTI_RESET_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_RESET_CONTENT(self):
        return """
    _ref_{alias_to} = _prev_{alias_to} = _next_{alias_to} = ({to_type}*)0;
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_RESET(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_RESET(iterator)

    def METHOD_ITERATOR_CRITICAL_MULTI_INIT(self, iterator):
        method = MemberMethod(
            type=self.type('void'),
            access='private',
            name='__init__',
            content=self.METHOD_ITERATOR_CRITICAL_MULTI_INIT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self.inner_class,ptr=True,const=True),
            name=self.format('iter_{alias_from}'),
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type('bool', funptr=True, const_fn=True, class_fn=self._key.to_class),
            name='method',
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name=self.format('ref_{alias_to}'),
            default='nullptr',
            parent=method,
            read_only=self.read_only
        )

    @critical_iterator_macro_method
    def METHOD_ITERATOR_CRITICAL_MULTI_INIT_CONTENT(self):
        return self.METHOD_ITERATOR_MULTI_INIT_CONTENT()

    def METHOD_ITERATOR_CRITICAL_MULTI_EXIT(self, iterator):
        method = ExitMethod(
            type=self.type('void'),
            content=self.METHOD_ITERATOR_MULTI_EXIT_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @critical_iterator_macro_method
    def METHOD_ITERATOR_CRITICAL_MULTI_EXIT_CONTENT(self):
        return self.METHOD_ITERATOR_MULTI_EXIT_CONTENT()

    def METHOD_ITERATOR_STATIC_MULTI_ISLAST(self, iterator):
        #         int is_last() { return (get_last_{alias_to}() == _ref_{alias_to}); }
        MemberMethod(
            type=self.type('bool'),
            access='public',
            inline=True,
            name='is_last',
            content=self.METHOD_ITERATOR_STATIC_MULTI_ISLAST_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    def METHOD_ITERATOR_STATIC_MULTI_ISLAST(self, iterator):
        #         int is_last() { return (get_last_{alias_to}() == _ref_{alias_to}); }
        MemberMethod(
            type=self.type('bool'),
            access='public',
            inline=True,
            name='is_last',
            content=self.METHOD_ITERATOR_STATIC_MULTI_ISLAST_CONTENT(),
            parent=iterator,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_ISLAST_CONTENT(self):
        return """
    return (get_last_{alias_to}() == _ref_{alias_to});
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ISLAST(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_ISLAST(iterator)

    def METHOD_ITERATOR_STATIC_MULTI_ISFIRST(self, iterator):
        #         int is_first() { return (get_first_{alias_to}() == _ref_{alias_to}); }
        MemberMethod(
            type=self.type('bool'),
            access='public',
            inline=True,
            name='is_first',
            content=self.METHOD_ITERATOR_STATIC_MULTI_ISFIRST_CONTENT(),
            parent=iterator,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_ITERATOR_STATIC_MULTI_ISFIRST_CONTENT(self):
        return """
    return (get_first_{alias_to}() == _ref_{alias_to});
"""

    def METHOD_ITERATOR_CRITICAL_STATIC_MULTI_ISFIRST(self, iterator):
        self.METHOD_ITERATOR_STATIC_MULTI_ISFIRST(iterator)

    def METHOD_MULTI_ADDFIRST(self, access_='public'):
        # void {from}::add_{alias_to}_first({to}* item)
        # {
        #     METHOD_MULTI_ADDFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            name=self.format('add_{alias_to}_first'),
            content=self.METHOD_MULTI_ADDFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_ADDFIRST_CONTENT(self):
        return """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == nullptr);
    {prefix_from}count_{alias_to}++;
    item->{prefix_to}ref_{alias_from} = this;
    if ({prefix_from}first_{alias_to})
    {{
        {prefix_from}first_{alias_to}->{prefix_to}prev_{alias_from} = item;
        item->{prefix_to}next_{alias_from} = {prefix_from}first_{alias_to};
        {prefix_from}first_{alias_to} = item;
    }}
    else
    {{
        {prefix_from}first_{alias_to} = {prefix_from}last_{alias_to} = item;
    }}
"""

    def METHOD_MULTI_ADDLAST(self, access_='public'):
        # void {from}::add_{alias_to}_last({to}* item)
        # {
        #     METHOD_MULTI_ADDLAST({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            name=self.format('add_{alias_to}_last'),
            content=self.METHOD_MULTI_ADDLAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_ADDLAST_CONTENT(self):
        return """
    assert(this);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == nullptr);
    {prefix_from}count_{alias_to}++;
    item->{prefix_to}ref_{alias_from} = this;
    if ({prefix_from}last_{alias_to})
    {{
        {prefix_from}last_{alias_to}->{prefix_to}next_{alias_from} = item;
        item->{prefix_to}prev_{alias_from} = {prefix_from}last_{alias_to};
        {prefix_from}last_{alias_to} = item;
    }}
    else
    {{
       {prefix_from}first_{alias_to} = {prefix_from}last_{alias_to} = item;
    }}
"""

    def METHOD_MULTI_ADDAFTER(self, access_='public'):
        # void {from}::add_{alias_to}_after({to}* item, {to}* pos)
        # {
        #     METHOD_MULTI_ADDAFTER({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            name=self.format('add_{alias_to}_after'),
            content=self.METHOD_MULTI_ADDAFTER_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_ADDAFTER_CONTENT(self):
        return """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == nullptr);

    assert(pos);
    assert(pos->{prefix_to}ref_{alias_from} == this);

    {prefix_from}count_{alias_to}++;

    item->{prefix_to}ref_{alias_from} = this;
    item->{prefix_to}prev_{alias_from} = pos;
    item->{prefix_to}next_{alias_from} = pos->{prefix_to}next_{alias_from};
    pos->{prefix_to}next_{alias_from}  = item;

    if (item->{prefix_to}next_{alias_from})
    {{
        item->{prefix_to}next_{alias_from}->{prefix_to}prev_{alias_from} = item;
    }}
    else
    {{
        {prefix_from}last_{alias_to} = item;
    }}
"""

    def METHOD_MULTI_ADDBEFORE(self, access_='public'):
        # void {from}::add_{alias_to}_before({to}* item, {to}* pos)
        # {
        #     METHOD_MULTI_ADDBEFORE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            name=self.format('add_{alias_to}_before'),
            content=self.METHOD_MULTI_ADDBEFORE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_ADDBEFORE_CONTENT(self):
        return """
    assert( this );

    assert( item );
    assert( item->{prefix_to}ref_{alias_from} == nullptr );

    assert( pos );
    assert(pos->{prefix_to}ref_{alias_from} == this);

    {prefix_from}count_{alias_to}++;

    item->{prefix_to}ref_{alias_from} = this;
    item->{prefix_to}next_{alias_from} = pos;
    item->{prefix_to}prev_{alias_from} = pos->{prefix_to}prev_{alias_from};
    pos->{prefix_to}prev_{alias_from}  = item;

    if (item->{prefix_to}prev_{alias_from})
    {{
        item->{prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = item;
    }}
    else
    {{
        {prefix_from}first_{alias_to} = item;
    }}
"""

    def METHOD_MULTI_REMOVE(self, access_='public'):
        # void {from}::remove_{alias_to}({to}* item)
        # {
        #     METHOD_MULTI_REMOVE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            name=self.format('remove_{alias_to}'),
            content=self.METHOD_MULTI_REMOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_REMOVE_CONTENT(self):
        return """

    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    if(  {prefix_from}first_{alias_to}_iterator != nullptr )
    {{
        {prefix_from}first_{alias_to}_iterator->check(item);
    }}

    {prefix_from}count_{alias_to}--;

    if (item->{prefix_to}next_{alias_from})
    {{
        item->{prefix_to}next_{alias_from}->{prefix_to}prev_{alias_from} = item->{prefix_to}prev_{alias_from};
    }}
    else
    {{
        {prefix_from}last_{alias_to} = item->{prefix_to}prev_{alias_from};
    }}

    if (item->{prefix_to}prev_{alias_from})
    {{
        item->{prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = item->{prefix_to}next_{alias_from};
    }}
    else
    {{
        {prefix_from}first_{alias_to} = item->{prefix_to}next_{alias_from};
    }}

    item->{prefix_to}prev_{alias_from} = nullptr;
    item->{prefix_to}next_{alias_from} = nullptr;
    item->{prefix_to}ref_{alias_from} = nullptr;
"""

    def METHOD_MULTI_REMOVEALL(self):
        # void {from}::remove_all_{alias_to}()
        # {
        #     METHOD_MULTI_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('remove_all_{alias_to}'),
            content=self.METHOD_MULTI_REMOVEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_REMOVEALL_CONTENT(self):
        return """
    assert(this);
    for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
    {{
          remove_{alias_to}(item);
    }}
"""

    def METHOD_MULTI_DELETEALL(self):
        # void {from}::delete_all_{alias_to}()
        # {
        #     METHOD_MULTI_DELETEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('delete_all_{alias_to}'),
            content=self.METHOD_MULTI_DELETEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_DELETEALL_CONTENT(self):
        return """
    assert(this);
    for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
    {{
          delete item;
    }}
"""

    def METHOD_MULTI_REPLACE(self, access_='public'):
        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
        # {
        #     METHOD_MULTI_REPLACE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access_,
            name=self.format('replace_{alias_to}'),
            content=self.METHOD_MULTI_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_REPLACE_CONTENT(self):
        return """
    assert(this);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    assert(new_item);
    assert(new_item->{prefix_to}ref_{alias_from} == nullptr);


    if ( {prefix_from}first_{alias_to}_iterator != nullptr )
    {{
        {prefix_from}first_{alias_to}_iterator->check(item, new_item);
    }}

    if ( item->{prefix_to}next_{alias_from} != nullptr )
    {{
        item->{prefix_to}next_{alias_from}->{prefix_to}prev_{alias_from} = new_item;
    }}
    else
    {{
        {prefix_from}last_{alias_to} = new_item;
    }}
    if ( item->{prefix_to}prev_{alias_from} != nullptr )
    {{
        item->{prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = new_item;
    }}
    else
    {{
        {prefix_from}first_{alias_to} = new_item;
    }}

    new_item->{prefix_to}next_{alias_from} = item->{prefix_to}next_{alias_from};
    new_item->{prefix_to}prev_{alias_from} = item->{prefix_to}prev_{alias_from};
    item->{prefix_to}next_{alias_from} = nullptr;
    item->{prefix_to}prev_{alias_from} = nullptr;

    item->{prefix_to}ref_{alias_from} = nullptr;
    new_item->{prefix_to}ref_{alias_from} = this;
"""

    def METHOD_MULTI_GETFIRST(self):
        # {to}* {from}::get_first_{alias_to}() const
        # {
        #     METHOD_MULTI_GETFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            constmethod=True,
            access='public',
            name=self.format('get_first_{alias_to}'),
            content=self.METHOD_MULTI_GETFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_GETFIRST_CONTENT(self):
        return """
    assert(this);
    return {prefix_from}first_{alias_to};
"""

    def METHOD_MULTI_GETLAST(self):
        # {to}* {from}::get_last_{alias_to}() const
        # {
        #     METHOD_MULTI_GETLAST({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            constmethod=True,
            access='public',
            name=self.format('get_last_{alias_to}'),
            content=self.METHOD_MULTI_GETLAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_GETLAST_CONTENT(self):
        return """
    assert(this);
    return {prefix_from}last_{alias_to};
"""

    def METHOD_MULTI_GETNEXT(self):
        # {to}* {from}::get_next_{alias_to}({to}* pos) const
        # {
        #     METHOD_MULTI_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            constmethod=True,
            access='public',
            name=self.format('get_next_{alias_to}'),
            content=self.METHOD_MULTI_GETNEXT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_GETNEXT_CONTENT(self):
        return """
    assert(this);
    if ( pos == nullptr )
    {{
        return {prefix_from}first_{alias_to};
    }}
    assert(pos);
    assert(pos->{prefix_to}ref_{alias_from} == this);
    return pos->{prefix_to}next_{alias_from};
"""

    def METHOD_MULTI_GETPREV(self):
        # {to}* {from}::get_prev_{alias_to}({to}* pos) const
        # {
        #     METHOD_MULTI_GETPREV({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            constmethod=True,
            access='public',
            name=self.format('get_prev_{alias_to}'),
            content=self.METHOD_MULTI_GETPREV_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_GETPREV_CONTENT(self):
        return """
    assert(this);

    if ( pos == nullptr )
    {{
        return {prefix_from}last_{alias_to};
    }}

    assert(pos);
    assert(pos->{prefix_to}ref_{alias_from} == this);
    return pos->{prefix_to}prev_{alias_from};
"""

    def METHOD_MULTI_GETCOUNT(self):
        # int {from}::get_{alias_to}_count() const
        # {
        #     METHOD_MULTI_GETCOUNT({from}, {alias_from}, {to}, {alias_to})
        # }
        MemberMethod(
            type=self.type('size_t'),
            access='public',
            constmethod=True,
            name=self.format('get_{alias_to}_count'),
            content=self.METHOD_MULTI_GETCOUNT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_GETCOUNT_CONTENT(self):
        return """
    assert(this);
    return {prefix_from}count_{alias_to};
"""

    def METHOD_MULTI_MOVEFIRST(self):
        # void {from}::move_{alias_to}_first({to}* item)
        # {
        #     METHOD_MULTI_MOVEFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_first'),
            content=self.METHOD_MULTI_MOVEFIRST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_MOVEFIRST_CONTENT(self):
        return """
    assert(item);
    assert(item->{prefix_to}ref_{alias_from});
    item->{prefix_to}ref_{alias_from}->remove_{alias_to}(item);
    add_{alias_to}_first(item);
"""

    def METHOD_MULTI_MOVELAST(self):
        # void {from}::move_{alias_to}_last({to}* item)
        # {
        #     METHOD_MULTI_MOVELAST({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_last'),
            content=self.METHOD_MULTI_MOVELAST_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_MOVELAST_CONTENT(self):
        return """
    assert(item);
    assert(item->{prefix_to}ref_{alias_from});
    item->{prefix_to}ref_{alias_from}->remove_{alias_to}(item);
    add_{alias_to}_last(item);
"""

    def METHOD_MULTI_MOVEAFTER(self):
        # void {from}::move_{alias_to}_after({to}* item, {to}* pos)
        # {
        #     METHOD_MULTI_MOVEAFTER({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_after'),
            content=self.METHOD_MULTI_MOVEAFTER_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_MOVEAFTER_CONTENT(self):
        return """
    assert(item);
    assert(item->{prefix_to}ref_{alias_from});
    item->{prefix_to}ref_{alias_from}->remove_{alias_to}(item);
    add_{alias_to}_after(item, pos);
"""

    def METHOD_MULTI_MOVEBEFORE(self):
        # void {from}::move_{alias_to}_before({to}* item, {to}* pos)
        # {
        #     METHOD_MULTI_MOVEBEFORE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('move_{alias_to}_before'),
            content=self.METHOD_MULTI_MOVEBEFORE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_MOVEBEFORE_CONTENT(self):
        return """
    assert(item);
    assert(item->{prefix_to}ref_{alias_from});
    item->{prefix_to}ref_{alias_from}->remove_{alias_to}(item);
    add_{alias_to}_before(item, pos);
    """

    def METHOD_MULTI_SORT(self):
        # void {from}::sort_{alias_to}(int (*compare)({to}*, {to}*))
        # {
        #     METHOD_MULTI_SORT({from}, {alias_from}, {to}, {alias_to})
        # }
        method=MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('sort_{alias_to}'),
            content=self.METHOD_MULTI_SORT_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type('int', funptr=True, argdecl=self.format('({to_type}*,{to_type}*)')),
            name='compare',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_MULTI_SORT_CONTENT(self):
        return """

    for ({to_type}* a = get_first_{alias_to}(); a != nullptr ; a = get_next_{alias_to}(a))
    {{
        {to_type}* b = get_next_{alias_to}(a);

        while ( b != nullptr && compare(a, b) > 0 )
        {{
            {to_type}* c = get_prev_{alias_to}(a);
            while ( c != nullptr  && compare(c, b) > 0 )
                c = get_prev_{alias_to}(c);
            if (c != nullptr )
            {{
                move_{alias_to}_after(b, c);
            }}
            else
            {{
                move_{alias_to}_first(b);
            }}
            b = get_next_{alias_to}(a);
        }}
    }}
"""

    def METHOD_SINGLE_SET(self, access='public'):
        method = MemberMethod(
            type=self.type('void'),
            access=access,
            name=self.format('set_{alias_to}'),
            content=self.METHOD_SINGLE_SET_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_SINGLE_SET_CONTENT(self):
        return """
    assert(this);
    assert(_ref_{alias_to} == nullptr);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == nullptr);

    item->{prefix_to}ref_{alias_from} = this;
    _ref_{alias_to} = item;
"""

    def METHOD_SINGLE_GET(self):
        MemberMethod(
            type=self.type(self._key.to_class,ptr=True),
            access='public',
            inline=True,
            name=self.format('get_{alias_to}'),
            content=self.METHOD_SINGLE_GET_CONTENT(),
            parent= self,
            read_only=self.read_only
        )

    @macro_method
    def METHOD_SINGLE_GET_CONTENT(self):
        return """
    return _ref_{alias_to}; 
"""

    def METHOD_SINGLE_CLEAR(self, access='public'):
        tvoid = cached_type(self.project, 'void')

        method = MemberMethod(
            type=self.type(tvoid),
            access=access,
            name=self.format('clear_{alias_to}'),
            content=self.METHOD_SINGLE_CLEAR_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_SINGLE_CLEAR_CONTENT(self):
        return """
    assert(this);
    assert(_ref_{alias_to} == item);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    item->{prefix_to}ref_{alias_from} = nullptr;
    _ref_{alias_to} = nullptr;
"""

    @macro_method_complete
    def METHOD_SINGLE_MOVE(self):
        """
    assert(item);
    assert(item->{prefix_to}ref_{alias_from});
    item->{prefix_to}ref_{alias_from}->clear_{alias_to}(item);
    set_{alias_to}(item);
"""
        return [{
                'type' : self.type('void'),
                'access' : 'public',
                'name' : self.format('move_{alias_to}'),
                'inline': False,
                'parent' : self,
                'read_only': self.read_only
            },{
                'type' : self.type(self._key.to_class,ptr=True),
                'name' : 'item',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_SINGLE_MOVE_CONTENT(self):
        return self.METHOD_SINGLE_MOVE.__doc__

    def METHOD_SINGLE_REPLACE(self, access='public'):
        tvoid = cached_type(self.project, 'void')
        method = MemberMethod(
            type=self.type(tvoid),
            access=access,
            name=self.format('replace_{alias_to}'),
            content=self.METHOD_SINGLE_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
            )

    @macro_method
    def METHOD_SINGLE_REPLACE_CONTENT(self):
        return """
    assert(this);
    assert({prefix_from}ref_{alias_to} == item);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);
    assert(new_item);
    assert(new_item->{prefix_to}ref_{alias_from} == nullptr);

    item->{prefix_to}ref_{alias_from} = nullptr;
    new_item->{prefix_to}ref_{alias_from} = this;
    {prefix_from}ref_{alias_to} = new_item;
    """

    @macro_method_complete
    def METHOD_STATIC_MULTI_ADDFIRST(self, access='public'):
        """
    assert(item);
    assert(item->{prefix_to}prev_{alias_from} == nullptr &&
             item->{prefix_to}next_{alias_from} == nullptr &&
             ({prefix_from}first_{alias_to} != item));

    {prefix_from}count_{alias_to}++;

    if ({prefix_from}first_{alias_to})
    {{
        {prefix_from}first_{alias_to}->{prefix_to}prev_{alias_from} = item;
        item->{prefix_to}next_{alias_from} = {prefix_from}first_{alias_to};
        {prefix_from}first_{alias_to} = item;
    }}
    else
    {{
         {prefix_from}first_{alias_to} = {prefix_from}last_{alias_to} = item;
    }}
"""
        return [{
            'access' : access,
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('add_{alias_to}_first'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' : 'item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_ADDFIRST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDFIRST.__doc__

    @macro_method_complete
    def METHOD_STATIC_MULTI_ADDLAST(self, access='public'):
        """
    assert(item);
    assert(item->{prefix_to}prev_{alias_from} == nullptr &&
            item->{prefix_to}next_{alias_from} == nullptr &&
            ({prefix_from}first_{alias_to} != item));

    {prefix_from}count_{alias_to}++;

    if ({prefix_from}last_{alias_to})
    {{
        {prefix_from}last_{alias_to}->{prefix_to}next_{alias_from} = item;
        item->{prefix_to}prev_{alias_from} = {prefix_from}last_{alias_to};
        {prefix_from}last_{alias_to} = item;
    }}
    else
    {{
        {prefix_from}first_{alias_to} = {prefix_from}last_{alias_to} = item;
    }}
"""
        return [{
            'access' : access,
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('add_{alias_to}_last'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' : 'item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_ADDLAST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDLAST.__doc__

    # void {from}::add_{alias_to}_after({to}* item, {to}* pos)
    @macro_method_complete
    def METHOD_STATIC_MULTI_ADDAFTER(self, access='public'):
        """
    assert(pos);
    assert((pos->{prefix_to}prev_{alias_from} != nullptr) ||
            (pos->{prefix_to}next_{alias_from} != nullptr) ||
            ({prefix_from}first_{alias_to} == pos));

    assert(item);
    assert(item->{prefix_to}prev_{alias_from} == nullptr &&
            item->{prefix_to}next_{alias_from} == nullptr &&
            ({prefix_from}first_{alias_to} != item));

    {prefix_from}count_{alias_to}++;

    item->{prefix_to}prev_{alias_from} = pos;
    item->{prefix_to}next_{alias_from} = pos->{prefix_to}next_{alias_from};
    pos->{prefix_to}next_{alias_from}  = item;

    if (item->{prefix_to}next_{alias_from})
    {{
        item->{prefix_to}next_{alias_from}->{prefix_to}prev_{alias_from} = item;
    }}
    else
    {{
        {prefix_from}last_{alias_to} = item;
    }}
"""
        return [{
            'access' : access,
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('add_{alias_to}_after'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' : 'item',
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_ADDAFTER_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDAFTER.__doc__

    # void {from}::add_{alias_to}_before({to}* item, {to}* pos)
    @macro_method_complete
    def METHOD_STATIC_MULTI_ADDBEFORE(self, access='public'):
        """
    assert(pos);
    assert((pos->{prefix_to}prev_{alias_from} != nullptr) ||
            (pos->_next_{alias_from} != nullptr) ||
            ({prefix_from}first_{alias_to} == pos));

    assert(item);

    assert(item->{prefix_to}prev_{alias_from} == nullptr &&
            item->{prefix_to}next_{alias_from} == nullptr &&
            ({prefix_from}first_{alias_to} != item));

    {prefix_from}count_{alias_to}++;

    item->{prefix_to}next_{alias_from} = pos;
    item->{prefix_to}prev_{alias_from} = pos->{prefix_to}prev_{alias_from};
    pos->{prefix_to}prev_{alias_from}  = item;

    if (item->{prefix_to}prev_{alias_from})
    {{
        item->{prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = item;
    }}
    else
    {{
        {prefix_from}first_{alias_to} = item;
    }}
"""
        return [{
            'access' : access,
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('add_{alias_to}_before'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' : 'item',
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_ADDBEFORE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_ADDBEFORE.__doc__

    # void {from}::remove_{alias_to}({to}* item)
    @macro_method_complete
    def METHOD_STATIC_MULTI_REMOVE(self, access='public'):
        """
    assert(item);
    assert((item->{prefix_to}prev_{alias_from} != nullptr) ||
            (item->{prefix_to}next_{alias_from} != nullptr) ||
            ({prefix_from}first_{alias_to} == item));

    {from_type}::{alias_to}_iterator::check(item);

    {prefix_from}count_{alias_to}--;

    if (item->{prefix_to}next_{alias_from})
    {{
        item->{prefix_to}next_{alias_from}->{prefix_to}prev_{alias_from} = item->{prefix_to}prev_{alias_from};
    }}
    else
    {{
        {prefix_from}last_{alias_to} = item->{prefix_to}prev_{alias_from};
    }}

    if (item->{prefix_to}prev_{alias_from})
    {{
        item->{prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = item->{prefix_to}next_{alias_from};
    }}
    else
    {{
        {prefix_from}first_{alias_to} = item->{prefix_to}next_{alias_from};
    }}

    item->{prefix_to}prev_{alias_from} = nullptr;
    item->{prefix_to}next_{alias_from} = nullptr;
"""
        return [{
            'access' : access,
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('remove_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_REMOVE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_REMOVE.__doc__

    # void {from}::remove_all_{alias_to}()
    @macro_method_complete
    def METHOD_STATIC_MULTI_REMOVEALL(self):
        """
    for ({to_type}* item = get_first_{alias_to}(); item != nullptr; item = get_first_{alias_to}())
    {{
          remove_{alias_to}(item);
    }}
"""
        return [{
            'access' : 'public',
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('remove_all_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_STATIC_MULTI_REMOVEALL_CONTENT(self):
        return self.METHOD_STATIC_MULTI_REMOVEALL.__doc__

    # void {from}::delete_all_{alias_to}()
    @macro_method_complete
    def METHOD_STATIC_MULTI_DELETEALL(self):
        """
    for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
    {{
          delete item;
    }}
"""
        return [{
            'access' : 'public',
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('delete_all_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_STATIC_MULTI_DELETEALL_CONTENT(self):
        return self.METHOD_STATIC_MULTI_DELETEALL.__doc__

        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
    @macro_method_complete
    def METHOD_STATIC_MULTI_REPLACE(self, access='public'):
        """
    assert(item);
    assert((item->{prefix_to}prev_{alias_from} != nullptr) ||
            (item->{prefix_to}next_{alias_from} != nullptr) ||
            ({prefix_from}first_{alias_to} == item));

    assert(new_item);
    assert(new_item->{prefix_to}prev_{alias_from} == nullptr &&
            new_item->{prefix_to}next_{alias_from} == nullptr &&
            ({prefix_from}first_{alias_to} != new_item));

    {from_type}::{alias_to}_iterator::check(item, new_item);

    if (item->{prefix_to}next_{alias_from})
    {{
        item->{prefix_to}next_{alias_from}->{prefix_to}prev_{alias_from} = new_item;
    }}
    else
    {{
        {prefix_from}last_{alias_to} = new_item;
    }}

    if (item->{prefix_to}prev_{alias_from})
    {{
        item->{prefix_to}prev_{alias_from}->{prefix_to}next_{alias_from} = new_item;
    }}
    else
    {{
       {prefix_from}first_{alias_to} = new_item;
    }}

    new_item->{prefix_to}next_{alias_from} = item->{prefix_to}next_{alias_from};
    new_item->{prefix_to}prev_{alias_from} = item->{prefix_to}prev_{alias_from};
    item->{prefix_to}next_{alias_from} = nullptr;
    item->{prefix_to}prev_{alias_from} = nullptr;
"""
        return [{
            'access' : access,
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('replace_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'item',
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'new_item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_REPLACE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_REPLACE.__doc__

    # {to}* {from}::get_first_{alias_to}()
    @macro_method_complete
    def METHOD_STATIC_MULTI_GETFIRST(self):
        """
    return {prefix_from}first_{alias_to};
"""
        return [{
            'access' : 'public',
            'type' : self.type(self._key.to_class, ptr=True),
            'static' : True,
            'name' : self.format('get_first_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_STATIC_MULTI_GETFIRST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETFIRST.__doc__

    # {to}* {from}::get_last_{alias_to}()
    @macro_method_complete
    def METHOD_STATIC_MULTI_GETLAST(self):
        """
    return {prefix_from}last_{alias_to};
"""
        return [{
            'access' : 'public',
            'type' : self.type(self._key.to_class, ptr=True),
            'static' : True,
            'name' : self.format('get_last_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_STATIC_MULTI_GETLAST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETLAST.__doc__

    # {to}* {from}::get_next_{alias_to}({to}* pos)
    @macro_method_complete
    def METHOD_STATIC_MULTI_GETNEXT(self):
        """
    if (pos == nullptr)
    {{
        return {prefix_from}first_{alias_to};
    }}
    assert((pos->{prefix_to}prev_{alias_from} != nullptr) ||
            (pos->{prefix_to}next_{alias_from} != nullptr) ||
            ({prefix_from}first_{alias_to} == pos));

    return pos->{prefix_to}next_{alias_from};
"""
        return [{
            'access': 'public',
            'type': self.type(self._key.to_class, ptr=True),
            'static': True,
            'name': self.format('get_next_{alias_to}'),
            'inline': False,
            'parent': self,
            'read_only': self.read_only
            }, {
            'type': self.type(self._key.to_class, ptr=True),
            'name': 'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_GETNEXT_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETNEXT.__doc__

    # {to}* {from}::get_prev_{alias_to}({to}* pos)
    @macro_method_complete
    def METHOD_STATIC_MULTI_GETPREV(self):
        """
    if (pos == nullptr)
    {{
        return {prefix_from}last_{alias_to};
    }}
    assert((pos->{prefix_to}prev_{alias_from} != nullptr) ||
            (pos->{prefix_to}next_{alias_from} != nullptr) ||
            ({prefix_from}first_{alias_to} == pos));

    return pos->{prefix_to}prev_{alias_from};
"""
        return [{
            'access' : 'public',
            'type' : self.type(self._key.to_class, ptr=True),
            'static' : True,
            'name' : self.format('get_prev_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_GETPREV_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETPREV.__doc__

    # int {from}::get_{alias_to}_count()
    @macro_method_complete
    def METHOD_STATIC_MULTI_GETCOUNT(self):
        """
    return {prefix_from}count_{alias_to};
"""
        return [{
            'access' : 'public',
            'type' : self.type('size_t'),
            'static' : True,
            'inline' : True,
            'name' : self.format('get_{alias_to}_count'),
            'parent' : self,
            'read_only': self.read_only
            },]

    @macro_method
    def METHOD_STATIC_MULTI_GETCOUNT_CONTENT(self):
        return self.METHOD_STATIC_MULTI_GETCOUNT.__doc__

    # bool {from}::includes_{alias_to}({to}* item)
    @macro_method_complete
    def METHOD_STATIC_MULTI_INCLUDES(self):
        """
    assert(item);
    if (item->_prev_{alias_from} || item->_next_{alias_from}|| {prefix_from}first_{alias_to} == item)
        return true;
    return false;
"""
        return [{
            'access' : 'public',
            'type' : self.type('bool'),
            'static' : True,
            'name' : self.format('includes_{alias_to}'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_INCLUDES_CONTENT(self):
        return self.METHOD_STATIC_MULTI_INCLUDES.__doc__

    # void {from}::move_{alias_to}_first({to}* item)
    @macro_method_complete
    def METHOD_STATIC_MULTI_MOVEFIRST(self):
        """
    remove_{alias_to}(item);
    add_{alias_to}_first(item);
"""
        return [{
            'access' : 'public',
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('move_{alias_to}_first'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' : 'item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_MOVEFIRST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVEFIRST.__doc__

    # void {from}::move_{alias_to}_last({to}* item)
    @macro_method_complete
    def METHOD_STATIC_MULTI_MOVELAST(self):
        """
    remove_{alias_to}(item);
    add_{alias_to}_last(item);
"""
        return [{
            'access' : 'public',
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format('move_{alias_to}_last'),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'item',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_MOVELAST_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVELAST.__doc__

    # void {from}::move_{alias_to}_after({to}* item, {to}* pos)
    @macro_method_complete
    def METHOD_STATIC_MULTI_MOVEAFTER(self):
        """
    remove_{alias_to}(item);
    add_{alias_to}_after(item, pos);
"""
        return [{
            'access' : 'public',
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format("move_{alias_to}_after"),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'item',
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_MOVEAFTER_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVEAFTER.__doc__

    # void {from}::move_{alias_to}_before({to}* item, {to}* pos)
    @macro_method_complete
    def METHOD_STATIC_MULTI_MOVEBEFORE(self):
        """
    remove_{alias_to}(item);
    add_{alias_to}_before(item, pos);
"""
        return [{
            'access' : 'public',
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format("move_{alias_to}_before"),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'item',
            'read_only': self.read_only
            },{
            'type' : self.type(self._key.to_class, ptr=True),
            'name' :'pos',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_MOVEBEFORE_CONTENT(self):
        return self.METHOD_STATIC_MULTI_MOVEBEFORE.__doc__

    # void {from}::add_{alias_to}({to}* item)
    # {
    #     METHOD_UNIQUEVALUETREE_ADD({member}, {from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_ADD(self):
        """
    assert(this);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == nullptr);

    {prefix_from}count_{alias_to}++;

    item->{prefix_to}ref_{alias_from} = this;

    if ({prefix_from}first_{alias_to})
    {{
        {to_type}* current = {prefix_from}first_{alias_to};
        size_t bit = 0x1;
        while (1)
        {{
            assert(current->{member} != item->{member});

            if ((reinterpret_cast<size_t>(current->{member}) & bit) == (reinterpret_cast<size_t>(item->{member}) & bit))
            {{
                if (current->{prefix_to}left_{alias_from})
                {{
                    current = current->{prefix_to}left_{alias_from};
                }}
                else
                {{
                    current->{prefix_to}left_{alias_from} = item;
                    item->{prefix_to}parent_{alias_from} = current;
                    break;
                }}
            }}
            else
            {{
                if (current->{prefix_to}right_{alias_from})
                {{
                    current = current->{prefix_to}right_{alias_from};
                }}
                else
                {{
                    current->{prefix_to}right_{alias_from} = item;
                    item->{prefix_to}parent_{alias_from} = current;
                    break;
                }}
            }}

            bit <<= 1;
        }}
    }}
    else
    {{
        {prefix_from}first_{alias_to} = item;
    }}
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('add_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type'  : self.type(self._key.to_class, ptr=True),
                'name'  : 'item',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_UNIQUEVALUETREE_ADD_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_ADD.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_ADD(self, access='public'):
        # void {from}::add_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_ADD({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access,
            name=self.format('add_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_ADD_CONTENT(),
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_ADD_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_ADD_CONTENT()

    # void {from}::remove_{alias_to}({to}* item)
    # {
    #     METHOD_UNIQUEVALUETREE_REMOVE({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_REMOVE(self):
        """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    if(  {prefix_from}first_{alias_to}_iterator != nullptr )
    {{
        {prefix_from}first_{alias_to}_iterator->check(item);
    }}

    {prefix_from}count_{alias_to}--;

    {to_type}* replacement = 0;
    {to_type}* move = 0;
    if (item->{prefix_to}left_{alias_from})
    {{
        replacement = item->{prefix_to}left_{alias_from};
        replacement->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        move = item->{prefix_to}right_{alias_from};
    }}
    else if (item->{prefix_to}right_{alias_from})
    {{
        replacement = item->{prefix_to}right_{alias_from};
        replacement->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
    }}

    {to_type}* parent = item->{prefix_to}parent_{alias_from};
    if (parent)
    {{
        if (parent->{prefix_to}left_{alias_from} == item)
        {{
            parent->{prefix_to}left_{alias_from} = replacement;
        }}
        else
        {{
            parent->{prefix_to}right_{alias_from} = replacement;
        }}
    }}
    else
    {{
        {prefix_from}first_{alias_to} = replacement;
    }}

    if (replacement)
    {{
        while (1)
        {{
            {to_type}* tmp = replacement->{prefix_to}right_{alias_from};
            replacement->{prefix_to}right_{alias_from} = move;
            if (move)
            {{
                move->{prefix_to}parent_{alias_from} = replacement;
            }}

            if (!replacement->{prefix_to}left_{alias_from})
            {{
                if (tmp)
                {{
                    replacement->{prefix_to}left_{alias_from} = tmp;
                    tmp = 0;
                }}
                else
                {{
                    break;
                }}
            }}
            move = tmp;
            replacement = replacement->{prefix_to}left_{alias_from};
        }}
    }}

    item->{prefix_to}ref_{alias_from} = ({from_type}*)0;
    item->{prefix_to}parent_{alias_from} = ({to_type}*)0;
    item->{prefix_to}left_{alias_from} = ({to_type}*)0;
    item->{prefix_to}right_{alias_from} = ({to_type}*)0;
"""
        return [
            {
                'access': 'public',
                'type': self.type('void'),
                'name': self.format('remove_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type': self.type(self._key.to_class, ptr=True),
                'name': 'item',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_UNIQUEVALUETREE_REMOVE_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_REMOVE.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_REMOVE(self, access='public'):
        # void {from}::remove_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_REMOVE({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access,
            name=self.format('remove_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_REMOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_REMOVE_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_REMOVE_CONTENT()

    # void {from}::remove_all_{alias_to}()
    # {
    #     METHOD_UNIQUEVALUETREE_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_REMOVEALL(self):
        """
    assert(this);

    for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
          remove_{alias_to}(item);
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('remove_all_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_UNIQUEVALUETREE_REMOVEALL_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_REMOVEALL.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_REMOVEALL(self, access='public'):
        # void {from}::remove_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access,
            name=self.format('remove_all_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_REMOVEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_REMOVEALL_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_REMOVEALL_CONTENT()

    # void {from}::delete_all_{alias_to}()
    # {
    #     METHOD_UNIQUEVALUETREE_DELETEALL({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_DELETEALL(self):
        """
    assert(this);

    for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
          delete item;
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('delete_all_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_UNIQUEVALUETREE_DELETEALL_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_DELETEALL.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_DELETEALL(self):
        # void {from}::delete_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_DELETEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('delete_all_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_DELETEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_DELETEALL_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_DELETEALL_CONTENT()

    # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
    # {
    #     METHOD_UNIQUEVALUETREE_REPLACE({member}, {from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_REPLACE(self):
        """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    assert(new_item);
    assert(new_item->{prefix_to}ref_{alias_from} == ({from_type}*)0);

    if (item->{member} == new_item->{member})
    {{
        if(  {prefix_from}first_{alias_to}_iterator != nullptr )
        {{
            {prefix_from}first_{alias_to}_iterator->check(item);
        }}

        if ({prefix_from}first_{alias_to} == item)
        {{
            {prefix_from}first_{alias_to} = new_item;
        }}
        if (item->{prefix_to}parent_{alias_from})
        {{
            if (item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = new_item;
            }}
            else if (item->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = new_item;
            }}
        }}
        new_item->{prefix_to}ref_{alias_from} = this;
        new_item->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        new_item->{prefix_to}left_{alias_from} = item->{prefix_to}left_{alias_from};
        new_item->{prefix_to}right_{alias_from} = item->{prefix_to}right_{alias_from};
        item->{prefix_to}ref_{alias_from} = ({from_type}*)0;
        item->{prefix_to}parent_{alias_from} = ({to_type}*)0;
        item->{prefix_to}left_{alias_from} = ({to_type}*)0;
        item->{prefix_to}right_{alias_from} = ({to_type}*)0;
    }}
    else
    {{
        if(  {prefix_from}first_{alias_to}_iterator != nullptr )
        {{
            {prefix_from}first_{alias_to}_iterator->check(item);
        }}
        remove_{alias_to}(item);
        add_{alias_to}(new_item);
    }}
"""
        return [
            {
                'access': 'public',
                'type': self.type('void'),
                'name': self.format('replace_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type': self.type(self._key.to_class, ptr=True),
                'name': 'item',
                'read_only': self.read_only
            },
            {
                'type': self.type(self._key.to_class, ptr=True),
                'name': 'new_item',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_UNIQUEVALUETREE_REPLACE_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_REPLACE.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_REPLACE(self, access='public'):
        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_REPLACE({member}, {from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access=access,
            name=self.format('replace_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_REPLACE_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_REPLACE_CONTENT()

    # {to}* {from}::get_first_{alias_to}() const
    # {
    #     METHOD_UNIQUEVALUETREE_GETFIRST({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_GETFIRST(self):
        """
    assert(this);
    return {prefix_from}first_{alias_to};
"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'name': self.format('get_first_{alias_to}'),
                'constmethod': True,
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_UNIQUEVALUETREE_GETFIRST_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETFIRST.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_GETFIRST(self):
        # void {from}::get_first_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_GETFIRST({member}, {from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_first_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_GETFIRST_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_GETFIRST_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETFIRST_CONTENT()

    # {to}* {from}::get_last_{alias_to}() const
    # {
    #     METHOD_UNIQUEVALUETREE_GETLAST({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_GETLAST(self):
        """
    assert(this);

    {to_type}* result = {prefix_from}first_{alias_to};
    while (result)
    {{
        while (result->{prefix_to}right_{alias_from})
        {{
            result = result->{prefix_to}right_{alias_from};
        }}

        if (result->{prefix_to}left_{alias_from})
        {{
            result = result->{prefix_to}left_{alias_from};
        }}
        else
        {{
            break;
        }}
    }}

    return result;
"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'name': self.format('get_last_{alias_to}'),
                'constmethod': True,
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_UNIQUEVALUETREE_GETLAST_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETLAST.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_GETLAST(self):
        # void {from}::get_last_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_GETLAST({member}, {from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_last_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_GETLAST_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_GETLAST_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETLAST_CONTENT()

    # {to}* {from}::get_next_{alias_to}({to}* pos) const
    # {
    #     METHOD_UNIQUEVALUETREE_GETNEXT({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_GETNEXT(self):
        """
    assert(this);

    {to_type}* result = 0;
    if (pos == ({to_type}*)0)
        result = {prefix_from}first_{alias_to};
    else
    {{
        assert(pos->{prefix_to}ref_{alias_from} == this);

        if (pos->{prefix_to}left_{alias_from})
        {{
            result = pos->{prefix_to}left_{alias_from};
        }}
        else
        {{
            if (pos->{prefix_to}right_{alias_from})
            {{
                result = pos->{prefix_to}right_{alias_from};
            }}
            else
            {{
                {to_type}* parent = pos->{prefix_to}parent_{alias_from};
                while (parent && (parent->{prefix_to}right_{alias_from} == 0 || parent->{prefix_to}right_{alias_from} == pos))
                {{
                    pos = parent;
                    parent = parent->{prefix_to}parent_{alias_from};
                }}

                if (parent)
                {{
                    result = parent->{prefix_to}right_{alias_from};
                }}
            }}
        }}
    }}

    return result;
"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'name': self.format('get_next_{alias_to}'),
                'constmethod': True,
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type': self.type(self._key.to_class, ptr=True),
                'name': 'pos',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_UNIQUEVALUETREE_GETNEXT_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETNEXT.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_GETNEXT(self):
        # {to}* {from}::get_next_{alias_to}({to}* pos) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_GETNEXT({member}, {from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_next_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_GETNEXT_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_GETNEXT(self):
        return self.METHOD_UNIQUEVALUETREE_GETNEXT()

    # {to}* {from}::get_prev_{alias_to}({to}* pos) const
    # {
    #     METHOD_UNIQUEVALUETREE_GETPREV({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_GETPREV(self):
        """
    assert(this);

    {to_type}* result = 0;
    if (pos == ({to_type}*)0)
    {{
        result = get_last_{alias_to}();
    }}
    else
    {{
        assert(pos->{prefix_to}ref_{alias_from} == this);

        if (pos->{prefix_to}parent_{alias_from})
        {{
            if (pos->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == pos || pos->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == 0)
            {{
                result = pos->{prefix_to}parent_{alias_from};
            }}
            else /* Right branche and valid left branche */
            {{
                result = pos->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from};
                while (1)
                {{
                    while (result->{prefix_to}right_{alias_from})
                    {{
                        result = result->{prefix_to}right_{alias_from};
                    }}

                    if (result->{prefix_to}left_{alias_from})
                    {{
                        result = result->{prefix_to}left_{alias_from};
                    }}
                    else
                    {{
                        break;
                    }}
                }}
            }}
        }}
    }}

    return result;
"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'name': self.format('get_prev_{alias_to}'),
                'constmethod': True,
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type': self.type(self._key.to_class, ptr=True),
                'name': 'pos',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_UNIQUEVALUETREE_GETPREV_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETPREV.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_GETPREV(self):
        # {to}* {from}::get_prev_{alias_to}({to}* pos) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_GETPREV({member}, {from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_prev_{alias_to}'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_GETPREV_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_GETPREV_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETPREV_CONTENT()

    # int {from}::get_{alias_to}_count() const
    # {
    #     METHOD_UNIQUEVALUETREE_GETCOUNT({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_UNIQUEVALUETREE_GETCOUNT(self):
        """
    assert(this);
    return {prefix_from}count_{alias_to};
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('size_t'),
                'name'  : self.format('get_{alias_to}_count'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only,
                'constmethod': True
            }, ]

    @macro_method
    def METHOD_UNIQUEVALUETREE_GETCOUNT_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETCOUNT.__doc__

    def METHOD_CRITICAL_UNIQUEVALUETREE_GETCOUNT(self):
        # int {from}::get_{alias_to}_count() const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_UNIQUEVALUETREE_GETCOUNT({member}, {from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('size_t'),
            access='public',
            constmethod=True,
            name=self.format('get_{alias_to}_count'),
            content=self.METHOD_CRITICAL_UNIQUEVALUETREE_GETCOUNT_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_UNIQUEVALUETREE_GETCOUNT_CONTENT(self):
        return self.METHOD_UNIQUEVALUETREE_GETCOUNT_CONTENT()

    # void {from}::add_{alias_to}({to}* item)
    # {
    #     METHOD_VALUETREE_ADD({member}, {from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_VALUETREE_ADD(self):
        """
    assert(this);
    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == nullptr);

    {prefix_from}count_{alias_to}++;
    item->{prefix_to}ref_{alias_from} = this;

    if ({prefix_from}first_{alias_to} == nullptr)
    {{
        {prefix_from}first_{alias_to} = item;
        return;
    }}
    {to_type}* current = {prefix_from}first_{alias_to};
    size_t bit = 0x1;
    while (1)
    {{
        if(current->{member} != item->{member})
        {{
            if ((reinterpret_cast<size_t>(current->{member}) & bit) == (reinterpret_cast<size_t>(item->{member}) & bit))
            {{
                if (current->{prefix_to}left_{alias_from})
                {{
                    current = current->{prefix_to}left_{alias_from};
                }}
                else
                {{
                    current->{prefix_to}left_{alias_from} = item;
                    item->{prefix_to}parent_{alias_from} = current;
                    break;
                }}
            }}
            else
            {{
                if (current->{prefix_to}right_{alias_from})
                {{
                    current = current->{prefix_to}right_{alias_from};
                }}
                else
                {{
                    current->{prefix_to}right_{alias_from} = item;
                    item->{prefix_to}parent_{alias_from} = current;
                    break;
                }}
            }}
            bit <<= 1;
        }}
        else
        {{
            item->{prefix_to}sibling_{alias_from} = current->{prefix_to}sibling_{alias_from};
            item->{prefix_to}parent_{alias_from} = current;
            current->{prefix_to}sibling_{alias_from} = item;
            if (item->{prefix_to}sibling_{alias_from})
            {{
                item->{prefix_to}sibling_{alias_from}->{prefix_to}parent_{alias_from} = item;
            }}
            break;
        }}
    }}
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('add_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type'  : self.type(self._key.to_class, ptr=True),
                'name'  : 'item',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_VALUETREE_ADD_CONTENT(self):
        return self.METHOD_VALUETREE_ADD.__doc__

    def METHOD_CRITICAL_VALUETREE_ADD(self):
        # void {from}::add_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_ADD({from}, {alias_from}, {to}, {alias_to})
        # }
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('add_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_ADD_CONTENT(),
            parent=self,
            read_only=self.read_only
            )
        Argument(
            type=self.type(self._key.to_class,ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
            )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_ADD_CONTENT(self):
        return self.METHOD_VALUETREE_ADD_CONTENT()

    # void {from}::remove_{alias_to}({to}* item)
    # {
    #     METHOD_VALUETREE_REMOVE({from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_REMOVE(self):
        """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    if(  {prefix_from}first_{alias_to}_iterator != nullptr )
    {{
        {prefix_from}first_{alias_to}_iterator->check(item);
    }}

    {prefix_from}count_{alias_to}--;

    if (item->{prefix_to}parent_{alias_from} && item->{prefix_to}parent_{alias_from}->{prefix_to}sibling_{alias_from} == item)
    {{
        item->{prefix_to}parent_{alias_from}->{prefix_to}sibling_{alias_from} = item->{prefix_to}sibling_{alias_from};
        if (item->{prefix_to}sibling_{alias_from})
        {{
            item->{prefix_to}sibling_{alias_from}->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        }}
    }}
    else if (item->{prefix_to}sibling_{alias_from})
    {{
        {to_type}* replacement = item->{prefix_to}sibling_{alias_from};
        replacement->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        replacement->{prefix_to}left_{alias_from} = item->{prefix_to}left_{alias_from};
        if (item->{prefix_to}left_{alias_from})
        {{
            item->{prefix_to}left_{alias_from}->{prefix_to}parent_{alias_from} = replacement;
        }}
        replacement->{prefix_to}right_{alias_from} = item->{prefix_to}right_{alias_from};
        if (item->{prefix_to}right_{alias_from})
        {{
            item->{prefix_to}right_{alias_from}->{prefix_to}parent_{alias_from} = replacement;
        }}

        {to_type}* parent = item->{prefix_to}parent_{alias_from};
        if (parent)
        {{
            if (parent->{prefix_to}left_{alias_from} == item)
            {{
                parent->{prefix_to}left_{alias_from} = replacement;
            }}
            else
            {{
                parent->{prefix_to}right_{alias_from} = replacement;
            }}
        }}
        else
        {{
            {prefix_from}first_{alias_to} = replacement;
        }}
    }}
    else
    {{
        {to_type}* replacement = nullptr;
        {to_type}* move = nullptr;
        if (item->{prefix_to}left_{alias_from})
        {{
            replacement = item->{prefix_to}left_{alias_from};
            replacement->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
            move = item->{prefix_to}right_{alias_from};
        }}
        else if (item->{prefix_to}right_{alias_from})
        {{
            replacement = item->{prefix_to}right_{alias_from};
            replacement->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        }}

        {to_type}* parent = item->{prefix_to}parent_{alias_from};
        if (parent)
        {{
            if (parent->{prefix_to}left_{alias_from} == item)
            {{
                parent->{prefix_to}left_{alias_from} = replacement;
            }}
            else
            {{
                parent->{prefix_to}right_{alias_from} = replacement;
            }}
        }}
        else
        {{
            {prefix_from}first_{alias_to} = replacement;
        }}

        if (replacement)
        {{
            while (1)
            {{
                {to_type}* tmp = replacement->{prefix_to}right_{alias_from};
                replacement->{prefix_to}right_{alias_from} = move;
                if (move)
                {{
                    move->{prefix_to}parent_{alias_from} = replacement;
                }}

                if (!replacement->{prefix_to}left_{alias_from})
                {{
                    if (tmp)
                    {{
                        replacement->{prefix_to}left_{alias_from} = tmp;
                        tmp = 0;
                    }}
                    else
                    {{
                        break;
                    }}
                }}
                move = tmp;
                replacement = replacement->{prefix_to}left_{alias_from};
            }}
        }}
    }}

    item->{prefix_to}ref_{alias_from} = nullptr;
    item->{prefix_to}parent_{alias_from} = nullptr;
    item->{prefix_to}left_{alias_from} = nullptr;
    item->{prefix_to}right_{alias_from} = nullptr;
    item->{prefix_to}sibling_{alias_from} = nullptr;
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('remove_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type'  : self.type(self._key.to_class, ptr=True),
                'name'  : 'item',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_VALUETREE_REMOVE_CONTENT(self):
        return self.METHOD_VALUETREE_REMOVE.__doc__

    def METHOD_CRITICAL_VALUETREE_REMOVE(self):
        # void {from}::remove_{alias_to}({to}* item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_REMOVE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('remove_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_REMOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_REMOVE_CONTENT(self):
        return self.METHOD_VALUETREE_REMOVE_CONTENT()

    def METHOD_CRITICAL_VALUETREE_OWNED_REMOVE(self):
        method = MemberMethod(
            type=self.type('void'),
            access='protected',
            name=self.format('remove_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_REMOVE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
        )

    # void {from}::remove_all_{alias_to}()
    # {
    #     METHOD_VALUETREE_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_REMOVEALL(self):
        """
    assert(this);

    for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
    {{
          remove_{alias_to}(item);
    }}
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('remove_all_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_VALUETREE_REMOVEALL_CONTENT(self):
        return self.METHOD_VALUETREE_REMOVEALL.__doc__

    def METHOD_CRITICAL_VALUETREE_REMOVEALL(self):
        # void {from}::remove_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('remove_all_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_REMOVEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_REMOVEALL_CONTENT(self):
        return self.METHOD_VALUETREE_REMOVEALL_CONTENT()

    def METHOD_CRITICAL_VALUETREE_OWNED_REMOVEALL(self):
        # void {from}::remove_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_REMOVEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='protected',
            name=self.format('remove_all_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_REMOVEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    # void {from}::delete_all_{alias_to}()
    # {
    #     METHOD_VALUETREE_DELETEALL({from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_DELETEALL(self):
        """
    assert(this);

    for ({to_type}* item = get_first_{alias_to}(); item; item = get_first_{alias_to}())
    {{
          delete item;
    }}
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('delete_all_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_VALUETREE_DELETEALL_CONTENT(self):
        return self.METHOD_VALUETREE_DELETEALL.__doc__

    def METHOD_CRITICAL_VALUETREE_DELETEALL(self):
        # void {from}::delete_all_{alias_to}()
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_DELETEALL({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('delete_all_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_DELETEALL_CONTENT(),
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_DELETEALL_CONTENT(self):
        return self.METHOD_VALUETREE_DELETEALL_CONTENT()

    # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
    # {
    #     METHOD_VALUETREE_REPLACE({member}, {from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_REPLACE(self):
        """
    assert(this);

    assert(item);
    assert(item->{prefix_to}ref_{alias_from} == this);

    assert(new_item);
    assert(new_item->{prefix_to}ref_{alias_from} == ({from_type}*)nullptr);

    if (item->{member} == new_item->{member})
    {{
        if(  {prefix_from}first_{alias_to}_iterator != nullptr )
        {{
            {prefix_from}first_{alias_to}_iterator->check(item, new_item);
        }}
        if ({prefix_from}first_{alias_to} == item)
        {{
            {prefix_from}first_{alias_to} = new_item;
        }}
        if (item->{prefix_to}parent_{alias_from})
        {{
            if (item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} = new_item;
            }}
            else if (item->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}right_{alias_from} = new_item;
            }}
            else if (item->{prefix_to}parent_{alias_from}->{prefix_to}sibling_{alias_from} == item)
            {{
                item->{prefix_to}parent_{alias_from}->{prefix_to}sibling_{alias_from} = new_item;
            }}
        }}
        new_item->{prefix_to}ref_{alias_from} = this;
        new_item->{prefix_to}parent_{alias_from} = item->{prefix_to}parent_{alias_from};
        new_item->{prefix_to}left_{alias_from} = item->{prefix_to}left_{alias_from};
        new_item->{prefix_to}right_{alias_from} = item->{prefix_to}right_{alias_from};
        new_item->{prefix_to}sibling_{alias_from} = item->{prefix_to}sibling_{alias_from};
        item->{prefix_to}ref_{alias_from} = ({from_type}*)0;
        item->{prefix_to}parent_{alias_from} = ({to_type}*)0;
        item->{prefix_to}left_{alias_from} = ({to_type}*)0;
        item->{prefix_to}right_{alias_from} = ({to_type}*)0;
        item->{prefix_to}sibling_{alias_from} = ({to_type}*)0;
    }}
    else
    {{
        if(  {prefix_from}first_{alias_to}_iterator != nullptr )
        {{
            {prefix_from}first_{alias_to}_iterator->check(item);
        }}
        remove_{alias_to}(item);
        add_{alias_to}(new_item);
    }}
"""
        return [
            {
                'access': 'public',
                'type'  : self.type('void'),
                'name'  : self.format('replace_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type'  : self.type(self._key.to_class, ptr=True),
                'name'  : 'item',
                'read_only': self.read_only
            },
            {
                'type'  : self.type(self._key.to_class, ptr=True),
                'name'  : 'new_item',
            }]

    @macro_method
    def METHOD_VALUETREE_REPLACE_CONTENT(self):
        return self.METHOD_VALUETREE_REPLACE.__doc__

    def METHOD_CRITICAL_VALUETREE_REPLACE(self):
        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHODL_VALUETREE_REPLACE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='public',
            name=self.format('delete_all_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_REPLACE_CONTENT(self):
        return self.METHOD_VALUETREE_REPLACE_CONTENT()

    def METHOD_CRITICAL_VALUETREE_OWNED_REPLACE(self):
        # void {from}::replace_{alias_to}({to}* item, {to}* new_item)
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHODL_VALUETREE_REPLACE({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('void'),
            access='protected',
            name=self.format('delete_all_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_REPLACE_CONTENT(),
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='item',
            parent=method,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='new_item',
            parent=method,
            read_only=self.read_only
        )

    # {to}* {from}::get_first_{alias_to}() const
    # {
    #     METHOD_VALUETREE_GETFIRST({from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_GETFIRST(self):
        """
    assert(this);
    return {prefix_from}first_{alias_to};
"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'constmethod': True,
                'name': self.format('get_first_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_VALUETREE_GETFIRST_CONTENT(self):
        return self.METHOD_VALUETREE_GETFIRST.__doc__

    def METHOD_CRITICAL_VALUETREE_GETFIRST(self):
        # {to}* {from}::get_first_{alias_to}() const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETFIRST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_first_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_GETFIRST_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_GETFIRST_CONTENT(self):
        return self.METHOD_VALUETREE_GETFIRST_CONTENT()

    # {to}* {from}::get_last_{alias_to}() const
    # {
    #     METHOD_VALUETREE_GETLAST({from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_GETLAST(self):
        """
    assert(this);

    {to_type}* result = {prefix_from}first_{alias_to};
    while (result)
    {{
        while (result->{prefix_to}right_{alias_from})
        {{
            result = result->{prefix_to}right_{alias_from};
        }}

        if (result->{prefix_to}left_{alias_from})
        {{
            result = result->{prefix_to}left_{alias_from};
        }}
        else
        {{
            break;
        }}
    }}

    if (result)
    {{
        while (result->{prefix_to}sibling_{alias_from})
        {{
            result = result->{prefix_to}sibling_{alias_from};
        }}
    }}

    return result;
"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'constmethod': True,
                'name': self.format('get_last_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },]

    @macro_method
    def METHOD_VALUETREE_GETLAST_CONTENT(self):
        return self.METHOD_VALUETREE_GETLAST.__doc__

    def METHOD_CRITICAL_VALUETREE_GETLAST(self):
        # {to}* {from}::get_last_{alias_to}() const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETLAST({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_last_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_GETLAST_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_GETLAST_CONTENT(self):
        return self.METHOD_VALUETREE_GETLAST_CONTENT()

    # {to}* {from}::get_next_{alias_to}({to}* pos) const
    # {
    #     METHOD_VALUETREE_GETNEXT({from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_GETNEXT(self):
        """
    assert(this);

    {to_type}* result = nullptr;
    if (pos == nullptr)
    {{
        result = {prefix_from}first_{alias_to};
    }}
    else
    {{
        assert(pos->{prefix_to}ref_{alias_from} == this);

        if (pos->{prefix_to}sibling_{alias_from})
        {{
            result = pos->{prefix_to}sibling_{alias_from};
        }}
        else
        {{
            while (pos->{prefix_to}parent_{alias_from} && pos->{prefix_to}parent_{alias_from}->{prefix_to}sibling_{alias_from} == pos)
            {{
                pos = pos->{prefix_to}parent_{alias_from};
            }}
            if (pos->{prefix_to}left_{alias_from})
            {{
                result = pos->{prefix_to}left_{alias_from};
            }}
            else
            {{
                if (pos->{prefix_to}right_{alias_from})
                {{
                    result = pos->{prefix_to}right_{alias_from};
                }}
                else
                {{
                    {to_type}* parent = pos->{prefix_to}parent_{alias_from};
                    while (parent && (parent->{prefix_to}right_{alias_from} == 0 || parent->{prefix_to}right_{alias_from} == pos))
                    {{
                        pos = parent;
                        parent = parent->{prefix_to}parent_{alias_from};
                    }}

                    if (parent)
                    {{
                        result = parent->{prefix_to}right_{alias_from};
                    }}
                }}
            }}
        }}
    }}

    return result;
"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'constmethod': True,
                'name': self.format('get_next_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type': self.type(self._key.to_class, ptr=True),
                'name': 'pos',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_VALUETREE_GETNEXT_CONTENT(self):
        return self.METHOD_VALUETREE_GETNEXT.__doc__

    def METHOD_CRITICAL_VALUETREE_GETNEXT(self):
        # {to}* {from}::get_next_{alias_to}({to}* pos) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_next_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_GETNEXT_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_GETNEXT_CONTENT(self):
        return self.METHOD_VALUETREE_GETNEXT_CONTENT()

    # {to}* {from}::get_prev_{alias_to}({to}* pos) const
    # {
    #     METHOD_VALUETREE_GETPREV({from}, {alias_from}, {to}, {alias_to})
    # }
    #
    @macro_method_complete
    def METHOD_VALUETREE_GETPREV(self):
        """
    assert(this);

    {to_type}* result = nullptr;
    if (pos == nullptr)
    {{
        result = get_last_{alias_to}();
    }}else
    {{
        assert(pos->{prefix_to}ref_{alias_from} == this);

        if (pos->{prefix_to}parent_{alias_from})
        {{
            if (pos->{prefix_to}parent_{alias_from}->{prefix_to}sibling_{alias_from} == pos)
            {{
                result = pos->{prefix_to}parent_{alias_from};
            }}
            else
            {{
                if (pos->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == pos || pos->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from} == 0)
                {{
                    result = pos->{prefix_to}parent_{alias_from};
                }}
                else /* Right branche and valid left branche */
                {{
                    result = pos->{prefix_to}parent_{alias_from}->{prefix_to}left_{alias_from};
                    while (1)
                    {{
                        while (result->{prefix_to}right_{alias_from})
                        {{
                            result = result->{prefix_to}right_{alias_from};
                        }}

                        if (result->{prefix_to}left_{alias_from})
                        {{
                            result = result->{prefix_to}left_{alias_from};
                        }}
                        else
                        {{
                            break;
                        }}
                    }}
                }}
                if (result)
                {{
                    while (result->{prefix_to}sibling_{alias_from})
                    {{
                        result = result->{prefix_to}sibling_{alias_from};
                    }}
                }}
            }}
        }}
    }}
    return result;
"""
        return [
            {
                'access': 'public',
                'type'  : self.type(self._key.to_class, ptr=True),
                'constmethod' : True,
                'name'  : self.format('get_prev_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type'  : self.type(self._key.to_class, ptr=True),
                'name'  : 'pos',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_VALUETREE_GETPREV_CONTENT(self):
        return self.METHOD_VALUETREE_GETPREV.__doc__

    def METHOD_CRITICAL_VALUETREE_GETPREV(self):
        # {to}* {from}::get_prev_{alias_to}({to}* pos) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('get_prev_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_GETPREV_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self.type(self._key.to_class, ptr=True),
            name='pos',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_GETPREV_CONTENT(self):
        return self.METHOD_VALUETREE_GETPREV_CONTENT()

    # int {from}::get_{alias_to}_count() const
    # {
    #     METHOD_VALUETREE_GETCOUNT({from}, {alias_from}, {to}, {alias_to})
    # }
    @macro_method_complete
    def METHOD_VALUETREE_GETCOUNT(self):
        """
    assert(this);
    return {prefix_from}count_{alias_to};
"""
        return [
            {
                'access': 'public',
                'type': self.type('size_t'),
                'constmethod': True,
                'name': self.format('get_{alias_to}_count'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only,
                'constmethod': True
            }, ]

    @macro_method
    def METHOD_VALUETREE_GETCOUNT_CONTENT(self):
        return self.METHOD_VALUETREE_GETCOUNT.__doc__

    def METHOD_CRITICAL_VALUETREE_GETCOUNT(self):
        # int {from}::get_{alias_to}_count() const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETCOUNT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type('size_t'),
            access='public',
            name=self.format('get_{alias_to}_count'),
            content=self.METHOD_CRITICAL_VALUETREE_GETCOUNT_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_GETCOUNT_CONTENT(self):
        return self.METHOD_VALUETREE_GETCOUNT_CONTENT()

    @macro_method_complete
    def METHOD_VALUETREE_FIND(self):
        """
    {to_type}* result = 0;
    if ({prefix_from}first_{alias_to})
    {{
        {to_type}* item = {prefix_from}first_{alias_to};
        size_t bit = 0x1;
        while (1)
        {{
            if (item->{member} == value)
            {{
                result = item;
                break;
            }}

            if ((reinterpret_cast<size_t>(item->{member}) & bit) == (reinterpret_cast<size_t>(value) & bit))
            {{
                if (item->{prefix_to}left_{alias_from})
                {{
                    item = item->{prefix_to}left_{alias_from};
                }}
                else
                {{
                    break;
                }}
            }}
            else
            {{
                if (item->{prefix_to}right_{alias_from})
                {{
                    item = item->{prefix_to}right_{alias_from};
                }}
                else
                {{
                    break;
                }}
            }}

            bit <<= 1;
        }}
    }}
    return result;
"""
        return [
            {
                'access': 'public',
                'type'  : self.type(self._key.to_class, ptr=True),
                'constmethod' : True,
                'name'  : self.format('find_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type'  : self._key.member.type_instance,
                'name'  : 'value',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_VALUETREE_FIND_CONTENT(self):
        return self.METHOD_VALUETREE_FIND.__doc__

    def METHOD_CRITICAL_VALUETREE_FIND(self):
        # {to}* {from}::find_{alias_to}(type value) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('find_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_FIND_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self._key._member._typei,
            name='value',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_FIND_CONTENT(self):
        return self.METHOD_VALUETREE_FIND_CONTENT()

    @macro_method_complete
    def METHOD_VALUETREE_FINDREVERSE(self):
        """
     {to_type}* result = 0;
    if ({prefix_from}first_{alias_to})
    {{
        {to_type}* item = {prefix_from}first_{alias_to};
        size_t bit = 0x1;
        while (1)
        {{
            if (item->{member} == value)
            {{
                result = item;
                break;
            }}

            if ((reinterpret_cast<size_t>(item->{member}) & bit) == (reinterpret_cast<size_t>(value) & bit))
            {{
                if (item->{prefix_to}left_{alias_from})
                {{
                    item = item->{prefix_to}left_{alias_from};
                }}
                else
                {{
                    break;
                }}
            }}
            else
            {{
                if (item->{prefix_to}right_{alias_from})
                {{
                    item = item->{prefix_to}right_{alias_from};
                }}
                else
                {{
                    break;
                }}
            }}

            bit <<= 1;
        }}
    }}
    if (result)
    {{
        while (result->{prefix_to}sibling_{alias_from})
            result = result->{prefix_to}sibling_{alias_from};
    }}
    return result;"""
        return [
            {
                'access': 'public',
                'type': self.type(self._key.to_class, ptr=True),
                'constmethod': True,
                'name': self.format('reverse_find_{alias_to}'),
                'inline': False,
                'parent': self,
                'read_only': self.read_only
            },
            {
                'type'  : self._key.member.type_instance,
                'name'  : 'value',
                'read_only': self.read_only
            }]

    @macro_method
    def METHOD_VALUETREE_FINDREVERSE_CONTENT(self):
        return self.METHOD_VALUETREE_FINDREVERSE.__doc__

    def METHOD_CRITICAL_VALUETREE_FINDREVERSE(self):
        # {to}* {from}::reverse_find_{alias_to}(type value) const
        # {
        #     critical_section_lock lock({from}::critical_section_{alias_to}());
        #     METHOD_VALUETREE_GETNEXT({from}, {alias_from}, {to}, {alias_to})
        # }
        #
        method = MemberMethod(
            type=self.type(self._key.to_class, ptr=True),
            access='public',
            name=self.format('reverse_find_{alias_to}'),
            content=self.METHOD_CRITICAL_VALUETREE_FINDREVERSE_CONTENT(),
            constmethod=True,
            parent=self,
            read_only=self.read_only
        )
        Argument(
            type=self._key._member._typei,
            name='value',
            parent=method,
            read_only=self.read_only
        )

    @critical_macro_method
    def METHOD_CRITICAL_VALUETREE_FINDREVERSE_CONTENT(self):
        return self.METHOD_VALUETREE_FINDREVERSE_CONTENT()

    # void {from}::sort_{alias_to}(int (*compare)({to}*, {to}*))
    @macro_method_complete
    def METHOD_STATIC_MULTI_SORT(self):
        """
    for ({to_type}* a = get_first_{alias_to}(); a; a = get_next_{alias_to}(a))
    {{
        {to_type}* b = get_next_{alias_to}(a);

        while (b && (compare(a, b) > 0))
        {{
            remove_{alias_to}(b);
            {to_type}* c = get_prev_{alias_to}(a);
            while (c && (compare(c, b) > 0))
            {{
                c = get_prev_{alias_to}(c);
            }}

            if (c)
            {{
                add_{alias_to}_after(b, c);
            }}
            else
            {{
                add_{alias_to}_first(b);
            }}

            b = get_next_{alias_to}(a);
        }}
    }}
"""
        return [{
            'access' : 'public',
            'type' : self.type('void'),
            'static' : True,
            'name' : self.format("sort_{alias_to}"),
            'inline': False,
            'parent' : self,
            'read_only': self.read_only
            },{
            'type': self.type('int',funptr=True,argdecl=self.format('({to_type}*,{to_type}*)')),
            'name':'compare',
            'read_only': self.read_only
            }]

    @macro_method
    def METHOD_STATIC_MULTI_SORT_CONTENT(self):
        return self.METHOD_STATIC_MULTI_SORT.__doc__

    @property
    def INIT_CODE(self):
        """Retorn el código que ha de insertarse"""
        if self._key.is_critical:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.INIT_CRITICAL_SINGLE_OWNED_ACTIVE()
                else:
                    return self.INIT_CRITICAL_SINGLE_ACTIVE()
            else:
                if self._key.implementation == 'standard':
                    if self._key.is_aggregate:
                        if self._key.is_static:
                            return self.INIT_CRITICAL_STATIC_MULTIOWNED_ACTIVE()
                        else:
                            return self.INIT_CRITICAL_MULTIOWNED_ACTIVE()
                    else:
                        if self._key.is_static:
                            return self.INIT_CRITICAL_STATIC_MULTI_ACTIVE()
                        else:
                            return self.INIT_CRITICAL_MULTI_ACTIVE()
                elif self._key.implementation == 'value_tree':
                    if self._key.is_unique:
                        if self._key.is_aggregate:
                            return self.INIT_CRITICAL_UNIQUEVALUETREE_OWNED_ACTIVE()
                        else:
                            return self.INIT_CRITICAL_UNIQUEVALUETREE_ACTIVE()
                    else:
                        if self._key.is_aggregate:
                            return self.INIT_CRITICAL_VALUETREE_OWNED_ACTIVE()
                        else:
                            return self.INIT_CRITICAL_VALUETREE_ACTIVE()
                else:
                    if self._key.is_aggregate:
                        return self.INIT_CRITICAL_AVLTREE_OWNED_ACTIVE()
                    else:
                        return self.INIT_CRITICAL_AVLTREE_ACTIVE()
        else:
            if self._key.is_single:
                if self._key.is_aggregate:
                    return self.INIT_SINGLE_OWNED_ACTIVE()
                else:
                    return self.INIT_SINGLE_ACTIVE()
            else:
                if self._key.implementation == 'standard':
                    if self._key.is_aggregate:
                        if self._key.is_static:
                            return self.INIT_STATIC_MULTIOWNED_ACTIVE()
                        else:
                            return self.INIT_MULTIOWNED_ACTIVE()
                    else:
                        if self._key.is_static:
                            return self.INIT_STATIC_MULTI_ACTIVE()
                        else:
                            return self.INIT_MULTI_ACTIVE()
                elif self._key.implementation == 'value_tree':
                    if self._key.is_unique:
                        if self._key.is_aggregate:
                            return self.INIT_UNIQUEVALUETREE_OWNED_ACTIVE()
                        else:
                            return self.INIT_UNIQUEVALUETREE_ACTIVE()
                    else:
                        if self._key.is_aggregate:
                            return self.INIT_VALUETREE_OWNED_ACTIVE()
                        else:
                            return self.INIT_VALUETREE_ACTIVE()
                else:
                    if self._key.is_aggregate:
                        return self.INIT_AVLTREE_OWNED_ACTIVE()
                    else:
                        return self.INIT_AVLTREE_ACTIVE()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("child", 'public')

    def save_state(self):
        """Internal save state"""
        super(RelationTo, self).save_state()

