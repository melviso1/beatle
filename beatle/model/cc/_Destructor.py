# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:08:46 2013

@author: mel
"""
import re

from beatle.lib.api import context
from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject

from ._CCContext import ContextDeclaration, ContextImplementation
from ._Member import Member


class Destructor(Member):
    """Implements ctor method"""
    context_container = True

    # visual methods
    @TransactionalMethod('move destructor ~{0}')
    def drop(self, to):
        """Drops class inside project or another folder """
        target = to.inner_member_container
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        # the name must be allways the same of the class
        kwargs['name'] = kwargs['parent'].inner_class.name
        self._access = kwargs.get('access', "public")
        self._explicit = kwargs.get('explicit', False)
        self._inline = kwargs.get('inline', False)
        self._virtual = kwargs.get('virtual', True)
        self._pure = kwargs.get('pure', False)
        self._calling_convention = kwargs.get('calling_convention', None)
        self._content = kwargs.get('content', "")
        if not hasattr(kwargs, 'note'):
            kwargs['note'] = 'Destructor.'
        super(Destructor, self).__init__(**kwargs)
        self.update_exit_call(False)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def access(self):
        return self._access

    @property
    def static(self):
        return False

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(Destructor, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'access': self._access,
            'explicit': self._explicit,
            'inline': self._inline,
            'virtual': self._virtual,
            'pure': self._pure,
            'calling_convention': self._calling_convention,
            'content': self._content,
        }
        kwargs.update(super(Destructor, self).get_kwargs())
        return kwargs

    def update_exit_call(self, save=True):
        """Updates the call to exit method"""
        if save:
            self.save_state()
        mask = r'\s*__exit__\s*\([^\)]*\);\n'
        if len(self.inner_class.exit_methods) == 0:
            # if there are not exit method, remove the call
            self._content = re.sub(mask, '', self._content)
            return
        # add the call
        if not re.search(mask, self._content):
            self._content = '    __exit__();\n' + self._content

    @ContextDeclaration()
    def write_declaration(self, pf):
        """Write the member method declaration"""
        i = self.inner_class
        i.ensure_access(pf, self._access)
        pf.write_line(self.declare)
        if self._inline or i.is_template:
            self.outer_class._inlines.append(self)

    @property
    def code_lines(self):
        """Return the number of line codes.
        This is used for mapping from file to object.
        """
        return len(self._content.splitlines())

    @ContextImplementation()
    def write_code(self, f):
        """Write code to file"""
        self.write_comment(f)
        f.write_line(self.implement)
        f.open_brace()
        if self._inline:
            setattr(self, 'source_line', -1)  # store start
        else:
            setattr(self, 'source_line', f.line)  # store start
        f.write_line(self._content)
        f.close_brace()

    def open_line(self, line):
        """This is a special method for open until some useful circumstances"""
        frame = context.get_frame()
        book = frame.docBook
        this_pane = getattr(self, '_pane', None)
        if this_pane is None:
            from beatle.activity.models.cc.ui import pane
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            book.AddPage(this_pane, self.tab_label, True, self.bitmap_index)
        else:
            index = book.GetPageIndex(this_pane)
            book.SetSelection(index)
        this_pane.goto_line(line, True)
        return True

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Destructor, self).on_undo_redo_changed()
        if hasattr(self, '_pane') and not self._pane is None:
            book = context.get_frame().docBook
            index = book.GetPageIndex(self._pane)
            book.SetPageText(index, self.name)
            book.SetPageBitmap(index, self.get_tab_bitmap())
            self._pane.m_editor.SetText(self._content)
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            this_pane.pre_delete()    # avoid gtk-critical
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Destructor, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for unload"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Destructor, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Destructor, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from beatle.activity.models.cc.ui import pane
            frame = context.get_frame()
            book = frame.docBook
            this_pane = pane.MethodPane(book, frame, self)
            setattr(self, '_pane', this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')

    def get_tab_bitmap(self):
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc
        return rc.get_bitmap("destructor", self._access)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("destructor", self._access)

    @property
    def tree_label(self):
        """Get tree label"""
        epilog = ""
        if self._pure:
            epilog += "=0"
        prolog = ""
        if self._virtual:
            prolog = "virtual "
        if self._inline:
            prolog = prolog + "inline "
        return prolog + "~" + self._name + "()" + epilog

    @property
    def tab_label(self):
        """Get tree label"""
        epilog = ""
        if self._pure:
            epilog += "=0"
        prolog = ""
        if self._virtual:
            prolog = "virtual "
        if self._inline:
            prolog = prolog + "inline "
        return "~{self.name}()".format(self=self)

    @property
    def declare(self):
        """Get declare string"""
        epilog = ""
        if self._pure:
            epilog += "=0"
        if self._calling_convention is not None:
            epilog += " __attribute__(({self._calling_convention}))".format(self=self)
        prolog = ""
        if self._virtual:
            prolog = "virtual "
        if self._inline:
            prolog = prolog + "inline "
        return "{prolog}~{name}(){epilog};".format(prolog=prolog, name=self._name, epilog=epilog)

    @property
    def implement(self):
        """Gets tab label"""
        cls = self.inner_class
        # we don't handle nested template classes until now
        # for support of relations we add this trick
        template_class = (cls.is_template and cls) or (cls.outer_class.is_template and cls.outer_class) or None
        if template_class is not None:
            return 'template<{cls.clean_template}> {self.scope}~{self._name}()'.format(cls=template_class, self=self)
        else:
            return '{self.scope}~{self._name}()'.format(self=self)

    @property
    def is_inline(self):
        return self._inline

    def exists_named_argument(self, name):
        """Checks about argument existence"""
        return False

    def __setstate__(self, d):
        """Load pickle context.
        This will add some missing attributes in previous releases."""
        if hasattr(d, '_calling'):
            d['_calling_convention'] = d['_callingText']
            del d['_calling']
            del d['_callingText']
        d['_calling_convention'] = d.get('_calling_convention', None)
        super(Destructor, self).__setstate__(d)
