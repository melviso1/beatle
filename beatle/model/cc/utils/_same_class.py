def same_class(cls):
    """Create a TCommon iterator filter"""
    def callback(x):
        return x.parent and x.parent.inner_class == cls
    return callback

