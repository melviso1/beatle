# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""

from math import sqrt
import copy
import wx

from multipledispatch import dispatch
from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject
from beatle.lib.api import volatile
from beatle.lib.api import context
from ._CCTComponent import CCTComponent

NO_WHERE = 0
IN_CLASS = 1
IN_INHERIT = 2
IN_AGGREG = 3
IN_NOTE = 4
IN_LINE = 5
IN_LINE_MARK = 7
IN_SHAPE = 6

BACKGROUND_COLOR = wx.Colour(50, 50, 50)
NORMAL_CLASS_COLOR = wx.Colour(82, 63, 105)
SELECT_CLASS_COLOR = wx.Colour(109, 84,140)
NORMAL_LINE_COLOR = wx.Colour(128, 128, 255)
SELECTED_LINE_COLOR = wx.Colour(192, 192, 255)
NORMAL_NOTE_COLOR = wx.Colour(69, 61,69)
SELECTED_NOTE_COLOR = wx.Colour(113, 100,113)


def quadratic_distance(p, q):
    """returns the power two of the distance"""
    return (p.x-q.x)**2 + (p.y-q.y)**2


def minimal_distance(to_points,from_points):
    """returns the minimal shift size and index i that corresponds to the minimal distance
    between to_points[i] and from_points[i].
    Of course, to_points and from_points are expected to have the same length. If not, only the minimal
    length is used. If that minimal length is zero, asserts an IndexError."""
    length = min(len(to_points), len(from_points))
    if length == 0:
        raise IndexError
    distances = [quadratic_distance(to_points[i], from_points[i]) for i in range(length)]
    min_index = min(range(length), key=lambda i: distances[i])
    return to_points[min_index]-from_points[min_index], min_index


def rect_corners(r):
    """Obtiene los vertices en orden SE NE NW SW"""
    (x, y, w, h) = r.Get()
    p0 = wx.Point(x, y)
    p1 = wx.Point(x, y + h)
    p2 = wx.Point(x + w, y + h)
    p3 = wx.Point(x + w, y)
    return p0, p1, p2, p3


def rect_sides(r):
    """Obtiene los lados de un rectangulo en orden RLBT"""
    (p0, p1, p2, p3) = rect_corners(r)
    return [p2, p3], [p0, p1], [p1, p2], [p0, p3]


def rect_middles(r):
    """Obtiene los puntos centrales de cara de un rectangulo en orden RLBT"""
    return [grid_point(wx.Point((a.x + b.x) // 2,
                                (a.y + b.y) // 2)) for [a, b] in rect_sides(r)]


def grid_point(p):
    """Normaliza a punto de grid"""
    return wx.Point(p.x - p.x % 5, p.y - p.y % 5)


def dot2(v1, v2):
    """Obtiene el producto escalar de dos vectores"""
    return (float(v1.x) * float(v2.x)) + (float(v1.y) * float(v2.y))


def norm(v):
    """obtiene la norma de un vector"""
    return sqrt(dot2(v, v))


def scale(v, a):
    """multiplica un vector por un escalar"""
    x = float(v.x) * a
    y = float(v.y) * a
    return wx.Point(int(x), int(y))


def to_point(x):
    """convert a serial_point to point"""
    if type(x) is serial_point:
        return x.point
    return x


def near_point(r, p):
    """Obtiene el punto mas proximo a p en el borde del rectangulo r
    y el indice de cara en orden RLBT"""
    c = rect_sides(r)
    # print "rect_sides:\n"
    # for side in c:
    #    print("[({x0},{y0}),({x1},{y1})]\n".format(
    #        x0=str(side[0].x), y0=str(side[0].y),
    #        x1=str(side[1].x), y1=str(side[1].y)))
    d2 = None
    ir = None
    m2 = None
    point = None
    for i in range(4):
        segment = c[i]
        # los puntos de la recta son de la forma q(a) = c[0] + a*(c[1]-c[0])
        # el punto mas proximo es aquel para que (p-q(a))*(c[1]-c[0]) = 0, de
        # donde
        # (p-c[0])*(c[1]-c[0]) = a*||c[0]-c[1]||²  Si a esta en el rango
        # [0, 1], la solucion esta en [c[0],c[1]]. En caso contrario, el
        # extremo mas proximo será c[0] si a<0 o c[1] si a>1
        director = segment[1] - segment[0]
        m = dot2(director, director)
        a = dot2(p - segment[0], director) / m
        if a <= 0:
            u = p - segment[0]
            s = dot2(u, u)
            if d2 is None or d2 > s or (d2 == s and m < m2):
                # si la distancia es la primera o es menor que la
                # anteriormente determinada o es igual, pero el lado es
                # mas pequeño ...
                d2 = s
                ir = i
                m2 = m
                point = segment[0]
            continue
        elif a >= 1:
            u = p - segment[1]
            s = dot2(u, u)
            if d2 is None or d2 > s or (d2 == s and m < m2):
                # si la distancia es la primera o es menor que la
                # anteriormente determinada o es igual, pero el lado es
                # mas pequeño ...
                d2 = s
                ir = i
                m2 = m
                point = segment[1]
            continue
        else:
            q = segment[0] + scale(director, a)
            u = p - q
            s = dot2(u, u)
            if d2 is None or d2 > s or (d2 == s and m < m2):
                # si la distancia es la primera o es menor que la
                # anteriormente determinada o es igual, pero el lado es
                # mas pequeño ...
                d2 = s
                ir = i
                m2 = m
                point = q
            continue
    return ir, point


class serial_point(object):  # nopep8
    def __init__(self, x, y):
        self.x = x
        self.y = y
        super(serial_point, self).__init__()

    def __add__(self, other):
        return serial_point(self.x+other. x, self.y+other.y)

    def __sub__(self, other):
        return serial_point(self.x-other.x, self.y-other.y)

    @property
    def point(self):
        return wx.Point(self.x, self.y)


class DiagramElement(object):
    """Base class for diagram elements"""
    def __init__(self, parent, obj, pos):
        """Initialization"""
        self._parent = parent
        self._obj = obj
        self._pos = to_point(pos)
        self._selected = False
        super(DiagramElement, self).__init__()

    @property
    def parent(self):
        return self._parent

    @property
    def object(self):
        """Return the referent object"""
        return self._obj

    @property
    def position(self):
        """Return the object position"""
        return self._pos

    def Draw(self, ctx):
        """Draw element"""
        pass

    def HitTest(self, pos):
        """Check about element near pos"""
        return None, NO_WHERE

    def set_selected(self, value=True):
        """Set selected state"""
        self._selected = value

    @property
    def is_selected(self):
        """Get selected state"""
        return self._selected

    def toggle_selection(self):
        """Toggle selected state"""
        self._selected = not self._selected

    def erase(self, dc):
        """Delete element"""
        pass

    def Remove(self, diag):
        """Remove element from class diagram"""
        pass

    def shift(self, offset):
        """Displaces elements"""
        self._pos += offset

    def layout(self):
        pass


class ClosedShape(DiagramElement):
    """Represents a closed shape"""
    def __init__(self, parent, obj, pos):
        """Initialization"""
        super(ClosedShape, self).__init__(parent, obj, pos)
        self.layout()
        self._rgn = self.region

    def __getstate__(self):
        """Get pickle context"""
        state = dict(self.__dict__)
        if '_rgn' in state:
            del state['_rgn']
        return state

    def __setstate__(self, state):
        """Set pickle context"""
        for attr in state:
            setattr(self, attr, state[attr])
        self._rgn = self.region

    def layout(self):
        """Initialize geometrical properties"""
        pass

    def shift(self, offset):
        """Displaces element"""
        super(ClosedShape, self).shift(offset)
        self._rgn.Offset(offset.x, offset.y)

    def HitTest(self, pos):
        """Check about element near pos"""
        if self.region.ContainsPoint(pos) == wx.InRegion:
            return self, IN_SHAPE
        else:
            return None, NO_WHERE


class RectShape(ClosedShape):
    """Represents a rect, closed shape"""
    def __init__(self, parent, obj, pos, **karg):
        """"""
        if 'size' in karg:
            self._rectSize = karg['size']
        else:
            self._rectSize = None
        super(RectShape, self).__init__(parent, obj, pos)

    @dispatch(wx.Point, int, int)
    def is_inside_rect(self, position, width, height):
        if position.x > self._pos.x \
                or position.y > self._pos.y \
                or position.x+width < self._pos.x + self._rectSize.x \
                or position.y+height < self._pos.y + self._rectSize.y:
            return False
        return True

    @dispatch(int, int, int, int)
    def is_inside_rect(self, x, y, width, height):
        if x > self._pos.x \
                or y > self._pos.y \
                or x+width < self._pos.x + self._rectSize.x \
                or y+height < self._pos.y + self._rectSize.y:
            return False
        return True

    def Rect(self):
        """"""
        return wx.Rect(self._pos.x, self._pos.y, self._rectSize.x, self._rectSize.y)

    @property
    def rect_size(self):
        """Get rectangle"""
        return self._rectSize

    @property
    def region(self):
        """Gets the class region"""
        return wx.Region(self._pos.x, self._pos.y, self._rectSize.x, self._rectSize.y)

    def HitTest(self, pos):
        """Check about element near pos"""
        r = wx.Rect(self._pos, self._rectSize)
        if r.Contains(pos):
            return (self, IN_SHAPE)
        return (None, NO_WHERE)


class NoteElement(RectShape):
    """Represents a text note in diagram"""
    def __init__(self, parent, obj, pos):
        """Initialization"""
        # in order to ensure correct class initialization we need to
        # first define the members, prior to super call
        self._textSize = 0
        self._xOffset = 0
        self._yOffset = 0
        super(NoteElement, self).__init__(parent, obj, pos)

    def layout(self):
        """Update control layout"""
        self.dc_layout(wx.ClientDC(context.get_frame()))

    def dc_layout(self, dc):
        """Update the drawing properties providing specific DC."""
        # When using multimonitor, we found an error due to different
        # screen resolutions. We need to update the parameters each time
        # we draw
        self._rectSize = wx.Size(0, 0)
        (width, height) = dc.GetMultiLineTextExtent(self._obj.note)
        self._textSize = wx.Size(width, height)
        rect_size = wx.Size(((width + 40)//5)*5, ((height + 20)//5)*5)
        if not hasattr(self, '_rectSize'):
            self._rectSize = rect_size
        else:
            if rect_size.x > self.rect_size.x:
                self._rectSize.x = rect_size.x
            if rect_size.y > self._rectSize.y:
                self._rectSize.y = rect_size.y
        self._xOffset = (self._rectSize.x - width) // 2
        self._yOffset = (self._rectSize.y - height) // 2

    def Draw(self, dc):
        """Draw class element"""
        self.dc_layout(dc)
        dc.SetLogicalFunction(wx.COPY)
        pen = wx.WHITE_PEN
        if self.is_selected:
            brush = wx.Brush(SELECTED_NOTE_COLOR)
        else:
            brush = wx.Brush(NORMAL_NOTE_COLOR)
        dc.SetPen(pen)
        dc.SetBrush(brush)
        dc.DrawRectangle(self._pos, self._rectSize)
        dc.SetTextForeground(wx.WHITE)
        dc.SetClippingRegion(self._pos.x + self._xOffset, self._pos.y + self._yOffset,
                             self._textSize.x, self._textSize.y)
        dc.DrawText(self._obj.note, self._pos.x + self._xOffset, self._pos.y + self._yOffset)
        dc.DestroyClippingRegion()

    def erase(self, dc):
        """Erase element"""
        dc.SetLogicalFunction(wx.COPY)
        dc.SetPen(wx.Pen(BACKGROUND_COLOR))
        dc.SetBrush(wx.Brush(BACKGROUND_COLOR))
        dc.DrawRectangle(self._pos, self._rectSize)

    def Remove(self, diag):
        """Remove element from class diagram"""
        pass


class ClassElement(RectShape):
    """Represents a class in diagram"""
    def __init__(self, parent, obj, pos):
        """Initialization"""
        # in order to ensure correct class initialization we need to
        # first define the members, prior to super call
        self._ancestors = []
        self._derivatives = []
        self._fromRelation = []
        self._toRelation = []
        self._textSize = 0
        self._xOffset = 0
        self._yOffset = 0
        super(ClassElement, self).__init__(parent, obj, pos)

    @property
    def ancestors(self):
        return self._ancestors

    def add_ancestor(self, ancestor):
        self._ancestors.append(ancestor)

    @property
    def derivatives(self):
        return self._derivatives

    def add_derivative(self, derivative):
        self._derivatives.append(derivative)

    @property
    def to_relation(self):
        return self._toRelation

    def add_to_relation(self, relation):
        self._toRelation.append(relation)

    def remove_to_relation(self, relation):
        self._toRelation.remove(relation)

    @property
    def from_relation(self):
        return self._fromRelation

    def add_from_relation(self, relation):
        self._fromRelation.append(relation)

    def remove_from_relation(self, relation):
        self._fromRelation.remove(relation)

    @property
    def label(self):
        """Return the label of the element"""
        try:
            scope = self.parent.parent
            return self._obj.local_declaration(scope)[:-1]
        except:
            return self._obj.scoped_declaration[:-1]

    def layout(self):
        """Update some properties"""
        dc = wx.ClientDC(context.get_frame())
        (width, height) = dc.GetTextExtent(self.label)
        self._textSize = wx.Size(width, height)
        rect_size = wx.Size(((width + 40)//5)*5, ((height + 20)//5)*5)
        size_changed = False
        if self._rectSize is None:
            self._rectSize = rect_size
            size_changed = True
        else:
            if rect_size.x > self._rectSize.x:
                self._rectSize.x = rect_size.x
                size_changed = True
            if rect_size.y > self._rectSize.y:
                self._rectSize.y = rect_size.y
                size_changed = True
        self._xOffset = (self._rectSize.x - width) // 2
        self._yOffset = (self._rectSize.y - height) // 2
        if size_changed:
            # se han de redimensionar las relaciones
            for inh in self._ancestors:
                inh.rebuild_segments()
            for inh in self._derivatives:
                inh.rebuild_segments()
            for rel in self._fromRelation:
                rel.rebuild_segments()
            for rel in self._toRelation:
                rel.rebuild_segments()

    def Draw(self, dc):
        """Draw class element"""
        # colors = wx.ColourDatabase()
        dc.SetLogicalFunction(wx.COPY)
        pen = wx.WHITE_PEN
        if self.is_selected:
            brush = wx.Brush(wx.Colour(SELECT_CLASS_COLOR))
        else:
            brush = wx.Brush(wx.Colour(NORMAL_CLASS_COLOR))
        dc.SetPen(pen)
        dc.SetBrush(brush)
        dc.DrawRectangle(self._pos, self._rectSize)
        dc.SetTextForeground(wx.WHITE)
        dc.DrawText(self.label, self._pos.x +
                    self._xOffset, self._pos.y + self._yOffset)

    def erase(self, dc):
        """Erase element"""
        dc.SetLogicalFunction(wx.COPY)
        dc.SetPen(wx.Pen(BACKGROUND_COLOR))
        dc.SetBrush(wx.Brush(BACKGROUND_COLOR))
        dc.DrawRectangle(self._pos, self._rectSize)
        for e in self._ancestors:
            e.erase(dc)
        for e in self._derivatives:
            e.erase(dc)
            e.erase(dc)

    def Remove(self, diag):
        """Remove element from class diagram"""
        k = copy.copy(self._ancestors)
        for inh in k:
            diag.remove_element(inh)
        k = copy.copy(self._derivatives)
        for inh in k:
            diag.remove_element(inh)
        k = copy.copy(self._fromRelation)
        for rel in k:
            diag.remove_element(rel)
        k = copy.copy(self._toRelation)
        for rel in k:
            diag.remove_element(rel)


class ConnectorSegment(object):
    """Represents a single segment"""
    def __init__(self, start, end):
        """Initialization"""
        self._start = to_point(start)
        self._end = to_point(end)
        self._points = [self._start, self._end]
        if self._start.x == self._end.x:
            self._orientation = wx.VERTICAL
        else:
            self._orientation = wx.HORIZONTAL
        self._mark = self._start + self._end
        self._mark.x //= 2
        self._mark.y //= 2
        self._mark.x -= 3
        self._mark.y -= 3

    def is_inside_rect(self, position, width, height):
        if self._start.x < position.x \
                or self._end.x < position.x \
                or self._start.y < position.y \
                or self._end.y < position.y \
                or self._start.x > position.x + width \
                or self._end.x > position.x + width \
                or self._start.y > position.y + height \
                or self._end.y > position.y + height:
            return False
        return True

    @property
    def orientation(self):
        return self._orientation

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        self._start = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        self._end = value

    @property
    def mark(self):
        return self._mark

    @mark.setter
    def mark(self, value):
        self._mark = value

    def get_kwargs(self):
        return {
            'start': serial_point(self._start.x, self._start.y),
            'end': serial_point(self._end.x, self._end.y)
        }

    def HitTest(self, pos, selected):
        """Check about mouse position"""
        if selected:
            # when selected, markers must selected first
            r = wx.Rect(self._mark.x, self._mark.y, 7, 7)
            if r.Contains(pos):
                return self, IN_LINE_MARK
        if self._orientation == wx.HORIZONTAL:
            if (self._start.x - pos.x) * (self._end.x - pos.x) < 0:
                if abs(self._start.y - pos.y) < 4:
                    return self, IN_LINE
        else:
            if (self._start.y - pos.y) * (self._end.y - pos.y) < 0:
                if abs(self._start.x - pos.x) < 4:
                    return self, IN_LINE
        return None, NO_WHERE

    def Draw(self, dc):
        """Draw element"""
        dc.DrawLine(self._start.x, self._start.y, self._end.x, self._end.y)

    def DrawMark(self, dc):
        """Draw selected marks"""
        sz = wx.Size(7, 7)
        dc.DrawRectangle(self._mark, sz)

    def shift(self, offset):
        """Shift position"""
        self._start += offset
        self._end += offset
        self._mark += offset


class ConnectorStartSegment(ConnectorSegment):
    """Represents an start segment"""
    def __init__(self, start, end):
        """Initialization"""
        super(ConnectorStartSegment, self).__init__(start, end)
        if self._orientation is wx.HORIZONTAL:
            self._mark.x = self._start.x - 3
        else:
            self._mark.y = self._start.y - 3
        self._touched = False

    @property
    def min_length(self):
        return 20

    def get_kwargs(self):
        return super(ConnectorStartSegment, self).get_kwargs()

    @property
    def touched(self):
        return self._touched

    @touched.setter
    def touched(self, value):
        self._touched = value


class ArrowStartSegment(ConnectorStartSegment):
    """Represents an start arrow"""
    def __init__(self, start, end):
        """Initialization"""
        super(ArrowStartSegment, self).__init__(start, end)

    def get_kwargs(self):
        return super(ArrowStartSegment, self).get_kwargs()

    def Draw(self, dc):
        """Draw element"""
        p = self._start
        if self._orientation is wx.HORIZONTAL:
            if p.x > self._end.x:
                q = wx.Point(p.x - 10, p.y - 5)
                r = wx.Point(p.x - 10, p.y + 5)
            else:
                q = wx.Point(p.x + 10, p.y - 5)
                r = wx.Point(p.x + 10, p.y + 5)
        elif self._orientation is wx.VERTICAL:
            if p.y > self._end.y:
                q = wx.Point(p.x - 5, p.y - 10)
                r = wx.Point(p.x + 5, p.y - 10)
            else:
                q = wx.Point(p.x - 5, p.y + 10)
                r = wx.Point(p.x + 5, p.y + 10)
        else:
            return
        self._points = [p, q, p, r, p, self._end]
        dc.DrawLines(self._points)


class LanceStartSegment(ConnectorStartSegment):
    """Represents an lance"""
    def __init__(self, start, end):
        """Initialization"""
        super(LanceStartSegment, self).__init__(start, end)

    def get_kwargs(self):
        return super(LanceStartSegment, self).get_kwargs()

    def Draw(self, dc):
        """Draw element"""
        p = self._start
        if self._orientation is wx.HORIZONTAL:
            if p.x > self._end.x:
                q = wx.Point(p.x - 10, p.y - 5)
                r = wx.Point(p.x - 10, p.y + 5)
                s = wx.Point(p.x - 10, p.y)
            else:
                q = wx.Point(p.x + 10, p.y - 5)
                r = wx.Point(p.x + 10, p.y + 5)
                s = wx.Point(p.x + 10, p.y)
        elif self._orientation is wx.VERTICAL:
            if p.y > self._end.y:
                q = wx.Point(p.x - 5, p.y - 10)
                r = wx.Point(p.x + 5, p.y - 10)
                s = wx.Point(p.x, p.y - 10)
            else:
                q = wx.Point(p.x - 5, p.y + 10)
                r = wx.Point(p.x + 5, p.y + 10)
                s = wx.Point(p.x, p.y + 10)
        else:
            return
        self._points = [p, q, s, self._end, s, r, p]
        dc.DrawLines(self._points)


class DiamondStartSegment(ConnectorStartSegment):
    """Represents an diamod <>"""
    def __init__(self, start, end):
        """Initialization"""
        super(DiamondStartSegment, self).__init__(start, end)

    def get_kwargs(self):
        return super(DiamondStartSegment, self).get_kwargs()

    def Draw(self, dc):
        """Draw element"""
        p = self._start
        if self._orientation is wx.HORIZONTAL:
            if p.x > self._end.x:
                q = wx.Point(p.x - 10, p.y - 5)
                r = wx.Point(p.x - 10, p.y + 5)
                s = wx.Point(p.x - 20, p.y)
            else:
                q = wx.Point(p.x + 10, p.y - 5)
                r = wx.Point(p.x + 10, p.y + 5)
                s = wx.Point(p.x + 20, p.y)
        elif self._orientation is wx.VERTICAL:
            if p.y > self._end.y:
                q = wx.Point(p.x - 5, p.y - 10)
                r = wx.Point(p.x + 5, p.y - 10)
                s = wx.Point(p.x, p.y - 20)
            else:
                q = wx.Point(p.x - 5, p.y + 10)
                r = wx.Point(p.x + 5, p.y + 10)
                s = wx.Point(p.x, p.y + 20)
        else:
            return
        self._points = [p, q, s, self._end, s, r, p]
        dc.DrawLines(self._points)


class ConnectorEndSegment(ConnectorSegment):
    """Represents an end segment"""
    def __init__(self, start, end):
        """Initialization"""
        super(ConnectorEndSegment, self).__init__(start, end)
        if self._orientation is wx.HORIZONTAL:
            self._mark.x = self._end.x - 3
        else:
            self._mark.y = self._end.y - 3
        self._touched = False

    @property
    def touched(self):
        return self._touched

    @touched.setter
    def touched(self, value):
        self._touched = value

    @property
    def min_length(self):
        return 20

    def get_kwargs(self):
        return super(ConnectorEndSegment, self).get_kwargs()


class ArrowEndSegment(ConnectorEndSegment):
    """Represents an end arrow"""
    def __init__(self, start, end):
        """Initialization"""
        super(ArrowEndSegment, self).__init__(start, end)

    def get_kwargs(self):
        return super(ArrowEndSegment, self).get_kwargs()

    def Draw(self, dc):
        """Draw element"""
        p = copy.copy(self._end)
        if self._orientation is wx.HORIZONTAL:
            p.x += 1
            if p.x > self._start.x:
                q = wx.Point(p.x - 10, p.y - 5)
                r = wx.Point(p.x - 10, p.y + 5)
            else:
                q = wx.Point(p.x + 10, p.y - 5)
                r = wx.Point(p.x + 10, p.y + 5)
        elif self._orientation is wx.VERTICAL:
            p.y += 1
            if p.y > self._start.y:
                q = wx.Point(p.x - 5, p.y - 10)
                r = wx.Point(p.x + 5, p.y - 10)
            else:
                q = wx.Point(p.x - 5, p.y + 10)
                r = wx.Point(p.x + 5, p.y + 10)
        else:
            return
        self._points = [p, q, p, r, p, self._start]
        dc.DrawLines(self._points)


class DoubleArrowEndSegment(ConnectorEndSegment):
    """Represents an end double arrow (for multiple relationship)"""
    def __init__(self, start, end):
        """Initialization"""
        super(DoubleArrowEndSegment, self).__init__(start, end)

    def get_kwargs(self):
        return super(DoubleArrowEndSegment, self).get_kwargs()

    def Draw(self, dc):
        """Draw element"""
        p = copy.copy(self._end)
        if self._orientation is wx.HORIZONTAL:
            p.x += 1
            if p.x > self._start.x:
                pp = wx.Point(p.x - 5, p.y)
                q = wx.Point(p.x - 10, p.y - 5)
                qq = wx.Point(p.x - 15, p.y - 5)
                r = wx.Point(p.x - 10, p.y + 5)
                rr = wx.Point(p.x - 15, p.y + 5)
            else:
                pp = wx.Point(p.x + 5, p.y)
                q = wx.Point(p.x + 10, p.y - 5)
                qq = wx.Point(p.x + 15, p.y - 5)
                r = wx.Point(p.x + 10, p.y + 5)
                rr = wx.Point(p.x + 15, p.y + 5)
        elif self._orientation is wx.VERTICAL:
            p.y += 1
            if p.y > self._start.y:
                pp = wx.Point(p.x, p.y - 5)
                q = wx.Point(p.x - 5, p.y - 10)
                qq = wx.Point(p.x - 5, p.y - 15)
                r = wx.Point(p.x + 5, p.y - 10)
                rr = wx.Point(p.x + 5, p.y - 15)
            else:
                pp = wx.Point(p.x, p.y + 5)
                q = wx.Point(p.x - 5, p.y + 10)
                qq = wx.Point(p.x - 5, p.y + 15)
                r = wx.Point(p.x + 5, p.y + 10)
                rr = wx.Point(p.x + 5, p.y + 15)
        else:
            return
        self._points = [p, q, p, r, p, pp, qq, pp, rr, pp, self._start]
        dc.DrawLines(self._points)


class LanceEndSegment(ConnectorEndSegment):
    """Represents an lance"""
    def __init__(self, start, end):
        """Initializes lance"""
        super(LanceEndSegment, self).__init__(start, end)

    def get_kwargs(self):
        return super(LanceEndSegment, self).get_kwargs()

    def Draw(self, dc):
        """Draw element"""
        p = self._end
        p = copy.copy(self._end)
        if self._orientation is wx.HORIZONTAL:
            p.x += 1
            if p.x > self._start.x:
                q = wx.Point(p.x - 10, p.y - 5)
                r = wx.Point(p.x - 10, p.y + 5)
            else:
                q = wx.Point(p.x + 10, p.y - 5)
                r = wx.Point(p.x + 10, p.y + 5)
        elif self._orientation is wx.VERTICAL:
            p.y += 1
            if p.y > self._start.y:
                q = wx.Point(p.x - 5, p.y - 10)
                r = wx.Point(p.x + 5, p.y - 10)
            else:
                q = wx.Point(p.x - 5, p.y + 10)
                r = wx.Point(p.x + 5, p.y + 10)
        else:
            return
        self._points = [p, q, r, self._start, r, p]
        dc.DrawLines(self._points)


class DiamondEndSegment(ConnectorEndSegment):
    """Represents an diamod <>"""
    def __init__(self, start, end):
        """Initialization"""
        super(DiamondEndSegment, self).__init__(start, end)

    def get_kwargs(self):
        return super(DiamondEndSegment, self).get_kwargs()

    def Draw(self, dc):
        """Draw element"""
        p = copy.copy(self._end)
        if self._orientation is wx.HORIZONTAL:
            p.x += 1
            if p.x > self._start.x:
                q = wx.Point(p.x - 10, p.y - 5)
                r = wx.Point(p.x - 10, p.y + 5)
                s = wx.Point(p.x - 20, p.y)
            else:
                q = wx.Point(p.x + 10, p.y - 5)
                r = wx.Point(p.x + 10, p.y + 5)
                s = wx.Point(p.x + 20, p.y)
        elif self._orientation is wx.VERTICAL:
            p.y += 1
            if p.y > self._end.y:
                q = wx.Point(p.x - 5, p.y - 10)
                r = wx.Point(p.x + 5, p.y - 10)
                s = wx.Point(p.x, p.y - 20)
            else:
                q = wx.Point(p.x - 5, p.y + 10)
                r = wx.Point(p.x + 5, p.y + 10)
                s = wx.Point(p.x, p.y + 20)
        else:
            return
        self._points = [p, q, s, self._start, s, r, p]
        dc.DrawLines(self._points)


class ConectorElement(DiagramElement):
    """Represents a connector between diagram elements"""
    def __init__(self, parent, obj, from_shape, to_shape):
        """Initialize a connector element"""
        super(ConectorElement, self).__init__(parent, obj, None)
        self._FROM = from_shape
        self._TO = to_shape
        self.segments = []

    def select_from_clients(self):
        """Select element only if both extremes are selected"""
        self.set_selected(self._FROM.is_selected and self._TO.is_selected)

    def draw_lines(self, dc):
        """Draw lines"""
        for segment in self.segments:
            segment.Draw(dc)

    def draw_markers(self, dc):
        """Draw track markers"""
        for segment in self.segments:
            segment.DrawMark(dc)

    def refresh_changes(self):
        """Update extremes segments"""
        if len(self.segments) > 1:
            s = self.segments[0]  # first segment
            e = self.segments[-1]  # last segment
            self.segments[0] = self.new_start_segment(s.start, s.end)
            self.segments[-1] = self.new_end_segment(e.start, e.end)

    def new_start_segment(self, p0, p1):
        """Create a new start segment"""
        return ConnectorStartSegment(p0, p1)

    def is_inside_rect(self, position, width, height):
        for s in self.segments:
            if not s.is_inside_rect(position, width, height):
                return False
        return True

    def __getstate__(self):
        """We have some trouble serializing mixed wx objects"""
        state = dict(self.__dict__)
        state['segments_args'] = [(type(x), x.get_kwargs()) for x in state['segments']]
        del state['segments']
        return state

    def __setstate__(self, state):
        """Set pickle context"""
        if 'segments_args' in state:
            state['segments'] = [x[0](**x[1]) for x in state['segments_args']]
            del state['segments_args']
        self.__dict__ = state

    def new_end_segment(self, p0, p1):
        """Create a new end segment"""
        return ConnectorEndSegment(p0, p1)

    def line_step(self, start_point, target_point, orientation=None, start=False, end=False):
        """Create a line step from p0 to p1"""
        sx = target_point.x - start_point.x
        sy = target_point.y - start_point.y
        if not start and not end:
            if sx == 0 and sy == 0:
                return None
        if orientation is wx.HORIZONTAL or (orientation is None and abs(sx) >= abs(sy)):
            end_point = wx.Point(target_point.x, start_point.y)
        else:
            end_point = wx.Point(start_point.x, target_point.y)
        if start is True:
            return self.new_start_segment(start_point, end_point)
        elif end is True:
            return self.new_end_segment(start_point, end_point)
        else:
            return ConnectorSegment(start_point, end_point)

    def shift_segment(self, index, pos):
        """Desplaza el segmento seleccionado"""
        # Lo primero, es determinar de que segmento se trata
        # print("called shift with pos value ({x},{y})".format(
        #    x=str(pos.x), y=str(pos.y)))
        if index == -1:
            index = len(self.segments) - 1
        segment = self.segments[index]
        if index == 0:
            # se trata de mover el primer segmento. Para esto,
            # se determina primero el lado mas proximo
            side, point = near_point(self._FROM.Rect(), pos)
            # point = grid_point(point)
            # print("near_point devuelve cara {cara} posicion ({x},{y})".format(
            #    x=str(point.x), y=str(point.y), cara=str(side)))
            last = self.segments[-1]
            target = last.start
            self.segments = []
            # add first segment
            q = point  # the target point
            u = side % 4
            # 03/01/2020: Inicialmente la solucion sencilla era añadir un nuevo segmento
            # de desplazamiento hacia el target. Este comportamiento contrasta con el
            # que ocurre cuando el segmento se deplaza de forma individual.
            if u < 2:
                # change is in x
                qq = wx.Point(q.x + (1 - 2 * u) * self.get_start_connector_size().x, q.y)
            else:
                # change is in y
                qq = wx.Point(q.x, q.y + (5 - 2 * u) * self.get_start_connector_size().y)
            ls = self.line_step(q, qq, start=True)
            ls.touched = True
            self.segments.append(ls)
            #new
            self.segments.append(last)
            self.rebuild_segments()
            # q = qq
            # while True:
            #     ls = self.line_step(q, target)
            #     if ls is None:
            #         break
            #     q = ls.end
            #     self.segments.append(ls)
            # # add last segment
            # self.segments.append(last)
            # self.join_ends()
            return 0
        elif index == len(self.segments) - 1:
            # ok, en este caso se trata, reciprocamente, del ultimo segmento
            side, point = near_point(self._TO.Rect(), pos)
            # point = grid_point(point)
            first = self.segments[0]
            target = first.end
            self.segments = []
            q = point
            u = side % 4
            x, y = q.Get()
            if u < 2:
                # change is in x
                qq = wx.Point(x +
                (1 - 2 * u) * self.get_end_connector_size().x, y)
            else:
                # change is in y
                qq = wx.Point(x, y +
                (5 - 2 * u) * self.get_end_connector_size().y)
            ls = self.line_step(qq, q, end=True)
            ls.touched = True
            self.segments.append(first)
            self.segments.append(ls)
            self.rebuild_segments()
            return len(self.segments) - 1
        else:
            # se trata de mover un segmento del medio. En este caso,
            # la simple orientacion determina que es lo que se mueve
            if segment.orientation is wx.HORIZONTAL:
                segment.start.y = pos.y
                segment.end.y = pos.y
                segment.mark.y = pos.y - 3
                self.segments[index - 1].end.y = pos.y
                if index > 1:
                    self.segments[index - 1].mark.y = (
                        self.segments[index - 1].end.y +
                        self.segments[index - 1].start.y) // 2
                self.segments[index + 1].start.y = pos.y
                if index < len(self.segments) - 2:
                    self.segments[index + 1].mark.y = (
                        self.segments[index + 1].end.y +
                        self.segments[index + 1].start.y) // 2
            else:
                segment.start.x = pos.x
                segment.end.x = pos.x
                segment._mark.x = pos.x - 3
                self.segments[index - 1].end.x = pos.x
                if index > 1:
                    self.segments[index - 1].mark.x = (
                        self.segments[index - 1].end.x +
                        self.segments[index - 1].start.x) // 2
                self.segments[index + 1].start.x = pos.x
                if index < len(self.segments) - 2:
                    self.segments[index + 1].mark.x = (
                        self.segments[index + 1].end.x +
                        self.segments[index + 1].start.x) // 2
            return index

    def get_end_connector_size(self):
        """Return the connector size"""
        return wx.Size(20, 20)

    def get_start_connector_size(self):
        """Return the connector size"""
        return wx.Size(20, 20)

    def get_side_positions(self, p, size):
        """Obtiene los puntos centrales de los lados de un cuadrado
        centrado en el punto p"""
        p0 = wx.Point(p.x + size.x, p.y)
        p1 = wx.Point(p.x - size.x, p.y)
        p2 = wx.Point(p.x, p.y + size.y)
        p3 = wx.Point(p.x, p.y - size.y)
        return [p0, p1, p2, p3]

    def join_ends(self):
        """Revisa los extremos, y elimina segmentos adyacentes que solo
        son prolongaciones"""
        l = len(self.segments) - 2
        if l > 0:
            if self.segments[0].orientation == self.segments[1].orientation:
                self.segments[0].end = self.segments[1].end
                self.segments.pop(1)
                l = l - 1
        if l > 0:
            z = l + 1
            if self.segments[l].orientation == self.segments[z].orientation:
                self.segments[z].start = self.segments[l].start
                self.segments.pop(l)

    def clean_touches(self):
        if len(self.segments) > 0:
            self.segments[0].touched = False
            self.segments[-1].touched = False

    def rebuild_segments(self):
        """Create initial lines"""
        start_touched = (len(self.segments) > 0) and self.segments[0].touched
        end_touched = (len(self.segments) > 0) and self.segments[-1].touched
        from_rect = self._FROM.Rect().Inflate(4, 0)
        to_rect = self._TO.Rect().Inflate(4, 0)
        if start_touched:
            starts = [self.segments[0].start] * 4  # hack start as zero size box
        else:
            starts = rect_middles(from_rect)
        if end_touched:
            ends = [self.segments[-1].end] * 4  # hack start as zero size box
        else:
            ends = rect_middles(to_rect)
        # create a combinatorial
        from_points = [starts[i % 4] for i in range(16)]
        to_points = [ends[i // 4] for i in range(16)]
        distance, match_index = minimal_distance(to_points, from_points)

        if start_touched:
            best_from = self.segments[0].start
            from_side_index = near_point(from_rect, best_from)[0]
        else:
            best_from = from_points[match_index]
            from_side_index = match_index % 4  # encode +/- horizontal/vertical flow

        if end_touched:
            best_to = self.segments[-1].end
            to_side_index = near_point(to_rect, best_to)[0]
        else:
            best_to = to_points[match_index]
            to_side_index = match_index // 4

        if to_side_index < 2:
            shift = (1-2*to_side_index)*self.get_end_connector_size().x
            best_to.x += shift
            distance.x += shift
        else:
            shift = (5-2*to_side_index)*self.get_end_connector_size().y
            best_to.y += shift
            distance.y += shift

        # clear segments
        self.segments = []

        # add first segment
        q = copy.copy(best_from)
        start_connector_size = self.get_start_connector_size()
        end_connector_size = self.get_end_connector_size()
        orientation = None
        if from_side_index == 0:  # from right side
            # Our connector starts from right side.
            # If distance.x < 0, our connector will be drawn inside the block. We don't
            # want that!!. We must escape doing a line to the right and then
            # a line with at least the middle height plus the connector size
            if distance.x <= start_connector_size.x+end_connector_size.x:
                if to_side_index == 0:  # the same side
                    shift = max(distance.x, start_connector_size.x)
                else:
                    shift = start_connector_size.x
                q.x += shift
                distance.x -= shift
                segment = self.line_step(best_from, q, orientation=wx.HORIZONTAL, start=True)
                segment.touched = start_touched
                self.segments.append(segment)
                q = copy.copy(segment.end)
                # Now, in order to avoid crossing the box, we need to step upwards or downwards
                if to_side_index == 1 and distance.x < end_connector_size.x:
                    # we do a ugly but easy to understand loop
                    if distance.y >= 0:
                        down_free = from_rect.GetBottom() - best_from.y + start_connector_size.y
                        shift = max(distance.y -2*down_free, 0)//2 + down_free
                    else:
                        up_free = from_rect.GetTop() - best_from.y - start_connector_size.y
                        shift = min(distance.y - 2*up_free, 0)//2 + up_free
                    q.y += shift
                    distance.y -= shift
                    segment = self.line_step(segment.end, q, orientation=wx.VERTICAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    # step horizontally to increase distance as needed
                    n = (end_connector_size.x - distance.x - 1) // end_connector_size.x + 1
                    q.x -= n*end_connector_size.x
                    distance.x += n*end_connector_size.x
                    segment = self.line_step(segment.end, q, orientation=wx.HORIZONTAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    shift = distance.y
                else:
                    down_free = from_rect.GetBottom() - best_from.y + start_connector_size.y
                    shift_down = max(distance.y, down_free)
                    up_free = from_rect.GetTop() - best_from.y - start_connector_size.y
                    shift_up = min(distance.y, up_free)
                    if to_side_index == 2:
                        if up_free >= distance.y:
                            shift = shift_up
                        else:
                            shift = shift_down
                    elif to_side_index == 3:
                        if down_free <= distance.y:
                            shift = shift_down
                        else:
                            shift = shift_up
                    elif distance.y >= 0:
                        shift = shift_down
                    else:
                        shift = shift_up
                q.y += shift
                distance.y -= shift
                segment = self.line_step(segment.end, q, orientation=wx.VERTICAL)
                orientation = wx.HORIZONTAL
            else:
                # Ok, distance is > 0 so we can step. If to_side_index is == 1
                # we must subtract that distance and create our start segment with
                # all the possible size
                if to_side_index == 0 or \
                        (to_side_index == 2 and distance.y <= 0) or \
                        (to_side_index == 3 and distance.y >= 0):  # the same side
                    q.x += max(distance.x, start_connector_size.x+end_connector_size.x)
                else:
                    q.x += max(distance.x//2, start_connector_size.x)
                segment = self.line_step(best_from, q, orientation=wx.HORIZONTAL, start=True)
                segment.touched = start_touched
                orientation = wx.VERTICAL
        elif from_side_index == 1:  # from left side
            if distance.x >= -start_connector_size.x:
                shift = start_connector_size.x
                q.x -= shift
                distance.x += shift
                segment = self.line_step(best_from, q, orientation=wx.HORIZONTAL, start=True)
                segment.touched = start_touched
                self.segments.append(segment)
                q = copy.copy(segment.end)
                # Now, in order to avoid crossing the box, we need to step upwards or downwards
                if to_side_index == 0 : #and distance.x >= -end_connector_size.x:
                    # we do a ugly but easy to understand loop
                    if distance.y >= 0:
                        down_free = from_rect.GetBottom() - best_from.y + start_connector_size.y
                        shift = max(distance.y-2*down_free, 0)//2 + down_free
                    else:
                        up_free = from_rect.GetTop() - best_from.y - start_connector_size.y
                        shift = min(distance.y - 2*up_free, 0)//2 + up_free
                    q.y += shift
                    distance.y -= shift
                    segment = self.line_step(segment.end, q, orientation=wx.VERTICAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    # step horizontally to increase distance as needed
                    n = (end_connector_size.x + distance.x - 1) // end_connector_size.x + 1
                    q.x += n*end_connector_size.x
                    distance.x -= n*end_connector_size.x
                    segment = self.line_step(segment.end, q, orientation=wx.HORIZONTAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    shift = distance.y
                else:
                    down_free = from_rect.GetBottom() - best_from.y + start_connector_size.y
                    shift_down = max(distance.y, down_free)
                    up_free = from_rect.GetTop() - best_from.y - start_connector_size.y
                    shift_up = min(distance.y, up_free)
                    if to_side_index == 2:
                        if up_free >= distance.y:
                            shift = shift_up
                        else:
                            shift = shift_down
                    elif to_side_index == 3:
                        if down_free <= distance.y:
                            shift = shift_down
                        else:
                            shift = shift_up
                    elif distance.y >= 0:
                        shift = shift_down
                    else:
                        shift = shift_up
                q.y += shift
                distance.y -= shift
                segment = self.line_step(segment.end, q, orientation=wx.VERTICAL)
                orientation = wx.HORIZONTAL
            else:
                # Ok, distance is > 0 so we can step. If to_side_index is == 1
                # we must subtract that distance and create our start segment with
                # all the possible size
                if to_side_index == 1 or \
                        (to_side_index == 2 and distance.y <= 0) or \
                        (to_side_index == 3 and distance.y >= 0):
                    q.x -= max(-distance.x, start_connector_size.x+end_connector_size.x)
                else:
                    q.x -= max(-distance.x//2, start_connector_size.x)
                segment = self.line_step(best_from, q, orientation=wx.HORIZONTAL, start=True)
                segment.touched = start_touched
                orientation = wx.VERTICAL
        elif from_side_index == 2:  # from bottom side
            # Our connector starts from bottom side.
            # If distance.y < 0, our connector will be drawn inside the block. We don't
            # want that!!. We must escape doing a line to the right and then
            # a line with at least the middle height plus the connector size
            if distance.y <= start_connector_size.y:
                if to_side_index == 2:  # the same side
                    shift = max(distance.y, start_connector_size.y)
                else:
                    shift = start_connector_size.y
                q.y += shift
                distance.y -= shift
                segment = self.line_step(best_from, q, orientation=wx.VERTICAL, start=True)
                segment.touched = start_touched
                self.segments.append(segment)
                q = copy.copy(segment.end)
                if to_side_index == 3 and distance.y < end_connector_size.y:
                    # we do a ugly but easy to understand loop
                    if distance.x >= 0:
                        right_free = from_rect.GetRight() - best_from.x + start_connector_size.x
                        shift = max(distance.x - 2*right_free, 0)//2 + right_free
                    else:
                        left_free = from_rect.GetLeft() - best_from.x - start_connector_size.x
                        shift = min(distance.x - 2*left_free, 0)//2 + left_free
                    q.x += shift
                    distance.x -= shift
                    segment = self.line_step(segment.end, q, orientation=wx.HORIZONTAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    # step horizontally to increase distance as needed
                    n = (end_connector_size.y - distance.y - 1) // end_connector_size.y + 1
                    q.y -= n*end_connector_size.y
                    distance.y += n*end_connector_size.y
                    segment = self.line_step(segment.end, q,  orientation=wx.VERTICAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    shift = distance.x
                else:
                    right_free = from_rect.GetRight() - best_from.x + start_connector_size.x
                    shift_right = max(distance.x, right_free)
                    left_free = from_rect.GetLeft() - best_from.x - start_connector_size.x
                    shift_left = min(distance.x, left_free)
                    if to_side_index == 0:
                        if left_free >= distance.x:
                            shift = shift_left
                        else:
                            shift = shift_right
                    elif to_side_index == 1:
                        if right_free <= distance.x:
                            shift = shift_right
                        else:
                            shift = shift_left
                    elif distance.x >= 0:
                        shift = shift_right
                    else:
                        shift = shift_left
                q.x += shift
                distance.x -= shift
                segment = self.line_step(segment.end, q, orientation=wx.HORIZONTAL)
                orientation = wx.VERTICAL
            else:
                # Ok, distance.y is > 0 so we can step. If to_side_index is == 0
                # we must subtract that distance and create our start segment with
                # all the possible size
                if to_side_index == 2 or \
                        (to_side_index == 0 and distance.x <= 0) or \
                        (to_side_index == 1 and distance.x >= 0):
                    q.y += max(distance.y, start_connector_size.y+end_connector_size.y)
                else:
                    q.y += max(distance.y//2, start_connector_size.y)
                segment = self.line_step(best_from, q, orientation=wx.VERTICAL, start=True)
                segment.touched = start_touched
                orientation = wx.HORIZONTAL
        elif from_side_index == 3:  # from top side
            # Our connector starts from top side.
            # If distance.y > 0, our connector will be drawn inside the block. We don't
            # want that!!. We must escape doing a line to the right and then
            # a line with at least the middle height plus the connector size
            if distance.y >= -start_connector_size.y:
                shift = start_connector_size.y
                q.y -= shift
                distance.y += shift
                segment = self.line_step(best_from, q, orientation=wx.VERTICAL, start=True)
                segment.touched = start_touched
                self.segments.append(segment)
                q = copy.copy(segment.end)
                if to_side_index == 2: # and distance.y > -end_connector_size.y:
                    # we do a ugly but easy to understand loop
                    if distance.x >= 0:
                        right_free = from_rect.GetRight() - best_from.x + start_connector_size.x
                        shift = max(distance.x - 2*right_free, 0)//2 + right_free
                    else:
                        left_free = from_rect.GetLeft() - best_from.x - start_connector_size.x
                        shift = min(distance.x - 2*left_free, 0)//2 + left_free
                    q.x += shift
                    distance.x -= shift
                    segment = self.line_step(segment.end, q, orientation=wx.HORIZONTAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    # step horizontally to increase distance as needed
                    n = (end_connector_size.y + distance.y - 1) // end_connector_size.y + 1
                    q.y += n*end_connector_size.y
                    distance.y -= n*end_connector_size.y
                    segment = self.line_step(segment.end, q,  orientation=wx.VERTICAL)
                    self.segments.append(segment)
                    q = copy.copy(segment.end)
                    shift = distance.x
                else:
                    right_free = from_rect.GetRight() - best_from.x + start_connector_size.x
                    shift_right = max(distance.x, right_free)
                    left_free = from_rect.GetLeft() - best_from.x - start_connector_size.x
                    shift_left = min(distance.x, left_free)
                    if to_side_index == 0:
                        if left_free >= distance.x:
                            shift = shift_left
                        else:
                            shift = shift_right
                    elif to_side_index == 1:
                        if right_free <= distance.x:
                            shift = shift_right
                        else:
                            shift = shift_left
                    elif distance.x >= 0:
                        shift = shift_right
                    else:
                        shift = shift_left
                q.x += shift
                distance.x -= shift
                segment = self.line_step(segment.end, q, orientation=wx.HORIZONTAL)
                orientation = wx.VERTICAL
            else:
                # Ok, distance.y is > 0 so we can step. If to_side_index is == 0
                # we must subtract that distance and create our start segment with
                # all the possible size
                if to_side_index == 3 or \
                        (to_side_index == 1 and distance.x >= 0) or \
                        (to_side_index == 0 and distance.x <= 0):  # the same side
                    q.y -= max(-distance.y, start_connector_size.y)
                else:
                    q.y += max(distance.y//2, -start_connector_size.y)
                segment = self.line_step(best_from, q, orientation=wx.VERTICAL, start=True)
                segment.touched = start_touched
                orientation = wx.HORIZONTAL
        if segment:
            self.segments.append(segment)
            q = copy.copy(segment.end)
        while True:
            segment = self.line_step(q, best_to, orientation=orientation)
            if segment is None:
                break
            q = segment.end
            self.segments.append(segment)
            if orientation is wx.VERTICAL:
                orientation = wx.HORIZONTAL
            elif orientation is wx.HORIZONTAL:
                orientation = wx.VERTICAL
        # add last line
        q = copy.copy(best_to)

        if to_side_index < 2:
            # change is in x
            q = wx.Point(q.x +
                (2 * to_side_index - 1) * end_connector_size.x, q.y)
        else:
            # change is in y
            q = wx.Point(q.x, q.y +
                (2 * to_side_index - 5) * end_connector_size.y)
        segment = self.line_step(best_to, q, end=True)
        assert( segment is not None)
        self.segments.append(segment)

        # After create the segments, the last step is check
        # for join extremes with adjacent segments
        self.join_ends()
        self.segments[-1].touched=end_touched

        # The connector position is only used as reference
        # when moving mores selected objects so this value
        # can change without special meaning. This method
        # seems a good place to reset it and ensure that
        # dont goes to infinity
        self._pos = wx.Point(0, 0)
        return True

    def selected_pen(self):
        """Returns the selected pen"""
        return wx.Pen(SELECTED_LINE_COLOR)

    def default_pen(self):
        """Returns the default pen for use when no selected"""
        return wx.Pen(NORMAL_LINE_COLOR)

    def Draw(self, dc):
        """Draw class element"""
        dc.SetLogicalFunction(wx.COPY)
        if self.is_selected:
            pen = self.selected_pen()
        else:
            pen = self.default_pen()
        dc.SetPen(pen)
        self.draw_lines(dc)
        if self.is_selected:
            self.draw_markers(dc)

    def erase(self, dc):
        """Draw class element"""
        dc.SetLogicalFunction(wx.COPY)
        pen = wx.Pen(BACKGROUND_COLOR)
        pen.SetWidth(2)
        dc.SetPen(pen)
        self.draw_lines(dc)
        self.draw_markers(dc)

    def HitTest(self, pos):
        """Check about element near pos"""
        for p in self.segments:
            where = p.HitTest(pos, self.is_selected)[1]
            if where is not NO_WHERE:
                return ((self, p), where)
            #if p.orientation == wx.HORIZONTAL:
            #    if (p._start.x - pos.x) * (p._end.x - pos.x) < 0:
            #        if abs(p._start.y - pos.y) < 2:
            #            return (self, IN_LINE)
            #else:
            #    if (p._start.y - pos.y) * (p._end.y - pos.y) < 0:
            #        if abs(p._start.x - pos.x) < 2:
            #            return (self, IN_LINE)
        return (None, NO_WHERE)

    def shift(self, offset):
        """Displaces elements"""
        # We must take caution here: connector segments could share the same
        # physical point object for contact. Due to that, we must do the operation
        # in flat mode.
        super(ConectorElement, self).shift(offset)
        points = [x.start+offset for x in self.segments]
        points.append(self.segments[-1].end+offset)
        for i in range(0, len(self.segments)):
            segment = self.segments[i]
            segment.start = points[i]
            segment.end = points[i+1]
            segment.mark = segment.mark + offset


class RelationElement(ConectorElement):
    """Represents a relationship in diagram"""
    def __init__(self, parent, obj, from_shape, to_shape):
        """Initialization"""
        super(RelationElement, self).__init__(parent, obj, from_shape, to_shape)
        self.rebuild_segments()

    def new_end_segment(self, p0, p1):
        """Get end segment"""
        if self._obj.is_single:
            return ArrowEndSegment(p0, p1)
        else:
            return DoubleArrowEndSegment(p0, p1)

    def new_start_segment(self, p0, p1):
        """Get initial segment"""
        if not self._obj.is_aggregate:
            return super(RelationElement, self).new_start_segment(p0, p1)
        return DiamondStartSegment(p0, p1)

    def get_start_connector_size(self):
        """Return the connector size"""
        return wx.Size(20, 20)

    def Remove(self, diag):
        """Remove element from diagram"""
        self._FROM.remove_to_relation(self)
        self._TO.remove_from_relation(self)

    def Draw(self, dc):
        """Do draw"""
        super(RelationElement, self).Draw(dc)

    def selected_pen(self):
        """return the selected pen"""
        if self._obj.is_critical:
            pen = wx.YELLOW_PEN
        else:
            pen = wx.Pen(SELECTED_LINE_COLOR)
        if self._obj.is_static:
            pen.SetWidth(2)
        else:
            pen.SetWidth(1)
        return pen

    def default_pen(self):
        """return the default pen"""
        if self._obj.is_critical:
            pen = wx.RED_PEN
        else:
            pen = wx.Pen(NORMAL_LINE_COLOR)
        if self._obj.is_static:
            pen.SetWidth(2)
        else:
            pen.SetWidth(1)
        return pen


class InheritanceElement(ConectorElement):
    """Represents a inheritance in diagram"""
    def __init__(self, parent, obj, from_shape, to_shape):
        """Initialization"""
        super(InheritanceElement, self).__init__(parent, obj, from_shape, to_shape)
        self.rebuild_segments()

    def new_start_segment(self, p0, p1):
        """rebuild_segments segment"""
        return LanceStartSegment(p0, p1)

    def get_start_connector_size(self):
        """Return the connector size"""
        return wx.Size(20, 20)

    def Remove(self, diag):
        """Remove element from diagram"""
        self._FROM.derivatives.remove(self)
        self._TO.ancestors.remove(self)

    def shift(self, offset):
        """Displaces elements"""
        super(InheritanceElement, self).shift(offset)

    def default_pen(self):
        """Return default pen"""
        return wx.Pen(NORMAL_LINE_COLOR)


class ClassDiagram(CCTComponent):
    """Implements a class diagram. This is the descriptor for the
    shapes and placements of the diagram."""

    def layout(self):
        for element in self._elements:
            element.layout()

    @TransactionalMethod('move class diagram {0}')
    def drop(self, to):
        """Drops class diagram inside project, class or folder """
        target = to.inner_diagram_container
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        index = 0
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=index)
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        self._elements = kwargs.get('elements', [])
        self._zoom = kwargs.get('zoom', 1.0)
        # A4 300 dpi default size
        self._width = kwargs.get('width', 2480)
        self._height = kwargs.get('height', 3508)
        self._pane = None
        super(ClassDiagram, self).__init__(**kwargs)

    @property
    def height(self):
        """The class diagram height"""
        return self._height

    @property
    def width(self):
        """The class diagram width"""
        return self._width

    @property
    def elements(self):
        """The elements that composed the diagram"""
        return self._elements

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'elements': self._elements,
            'zoom': self._zoom,
            'width': self._width,
            'height': self._height
        }
        kwargs.update(super(ClassDiagram, self).get_kwargs())
        return kwargs

    def add_class(self, cls, pos):
        """Add class element"""
        from beatle import model
        pos.x -= (pos.x % 5)
        pos.y -= (pos.y % 5)
        nwelem = ClassElement(self, cls, pos)

        # add inheritance and derivative links
        self_cls = lambda x: x.inner_class == cls
        inherits = cls(model.cc.Inheritance, filter=self_cls, cut=True)
        for inh in inherits:
            elem = self.find_element(inh._ancestor)
            if elem is not None:
                self.add_inheritance(inh, elem, nwelem)
        for elem in self._elements:
            if type(elem) is ClassElement:
                inherits = elem._obj(model.cc.Inheritance)
                for inh in inherits:
                    if inh._ancestor == cls:
                        self.add_inheritance(inh, nwelem, elem)
                        break
        # add relation links
        self._elements.append(nwelem)
        from_relation = [x.key for x in cls(model.cc.RelationFrom, filter=self_cls, cut=True)]
        to_relation = [x.key for x in cls(model.cc.RelationTo, filter=self_cls, cut=True) if x.key not in from_relation]
        for relation in from_relation:
            element = self.find_element(relation.from_class)
            if element is not None:
                self.add_relation(relation, element, nwelem)
        for relation in to_relation:
            element = self.find_element(relation.to_class)
            if element is not None:
                self.add_relation(relation, nwelem, element)
        return nwelem

    def add_note(self, obj, pos):
        """Add note element"""
        nwelem = NoteElement(self, obj, pos)
        self._elements.append(nwelem)
        return nwelem

    def add_inheritance(self, obj, from_shape, to_shape):
        """Add inheritance element"""
        elem = InheritanceElement(self, obj, from_shape, to_shape)
        from_shape.add_derivative(elem)
        to_shape.add_ancestor(elem)
        self._elements.append(elem)
        return elem

    def add_relation(self, obj, from_shape, to_shape):
        """Add relationship element.
           obj is the key Relation instance"""
        elem = RelationElement(self, obj, from_shape, to_shape)
        from_shape.add_to_relation(elem)
        to_shape.add_from_relation(elem)
        self._elements.append(elem)
        return elem

    @staticmethod
    def move_element(element, pos):
        """Move a element"""
        pos.x -= (pos.x % 5)
        pos.y -= (pos.y % 5)
        offset = pos - element._pos
        element._pos = pos

        if type(element) is ClassElement:
            for e in element.ancestors:
                e.shift(offset)
                e.rebuild_segments()
            for e in element.derivatives:
                e.shift(offset)
                e.rebuild_segments()
            for e in element.from_relation:
                e.shift(offset)
                e.rebuild_segments()
            for e in element.to_relation:
                e.shift(offset)
                e.rebuild_segments()

    def bring_to_front(self, element):
        """Move element to front"""
        if element in self._elements:
            self._elements.remove(element)
        self._elements.append(element)

    def remove_element(self, element):
        """Remove element from class diagram"""
        element.Remove(self)
        if element in self._elements:
            self._elements.remove(element)
        if getattr(self, '_pane', None):
            self._pane.Refresh()

    @property
    def selected_elements(self):
        """Return the list of selected elements"""
        return [x for x in self._elements if x.is_selected]

    def erase_selected(self, dc):
        """Clear selected elements draws"""
        for element in self._elements:
            if element.is_selected:
                element.erase(dc)
        # Erase inheritances
        for element in self._elements:
            if isinstance(element, ConectorElement):
                if not element._FROM.is_selected or not element._TO.is_selected:
                    element.erase(dc)
                elif not element.is_selected:
                    element.erase(dc)

    def shift_selected(self, offset):
        """Displace selected elements"""
        # Select inheritances with both extremes selected
        offset = grid_point(offset)
        for element in self._elements:
            if isinstance(element, ConectorElement):
                element.select_from_clients()
        # do shift
        for element in self._elements:
            if not element.is_selected:
                continue
            element.shift(offset)
            if type(element) is ClassElement:
                for e in element.ancestors:
                    if e.is_selected:
                        continue
                    e.shift_segment(-1, e.segments[-1].end+offset)
                    #e.segments[-1].shift(offset)
                    e.rebuild_segments()
                for e in element.derivatives:
                    if e.is_selected:
                        continue
                    e.shift_segment(0, e.segments[0].start+offset)
                    #e.segments[0].shift(offset)
                    e.rebuild_segments()
                for e in element.from_relation:
                    if e.is_selected:
                        continue
                    e.shift_segment(-1, e.segments[-1].end+offset)
                    #e.segments[-1].shift(offset)
                    e.rebuild_segments()
                for e in element.to_relation:
                    if e.is_selected:
                        continue
                    e.shift_segment(0, e.segments[0].start+offset)
                    #e.segments[0].shift(offset)
                    e.rebuild_segments()

    def on_undo_redo_changed(self):
        """Update from app"""
        if hasattr(self, '_pane'):
            # some bug happens here when we undo open project with open panes
            # and redo open
            from beatle.activity.models.cc.ui import pane
            if type(self._pane) is not pane.ClassDiagramPane:
                delattr(self, '_pane')
            else:
                book = context.get_frame().docBook
                index = book.GetPageIndex(self._pane)
                book.SetPageText(index, self.tab_label)
                book.SetPageBitmap(index, self.get_tab_bitmap())
                self._pane.Refresh()
        #refresh relations
        for element in self._elements:
            if type(element) is InheritanceElement:
                felem = element._FROM
                telem = element._TO
                if felem not in self._elements or telem not in self._elements:
                    print('detected orphan inheritance in diagram')
                if element not in felem.derivatives:
                    felem.derivatives.append(element)
                if element not in telem.ancestors:
                    telem.ancestors.append(element)
            elif type(element) is RelationElement:
                felem = element._FROM
                telem = element._TO
                if felem not in self._elements or telem not in self._elements:
                    print('detected orphan relationship in diagram')
                if element not in felem.to_relation:
                    felem.add_to_relation(element)
                if element not in telem.from_relation:
                    telem.add_from_relation(element)
        super(ClassDiagram, self).on_undo_redo_changed()

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            #focus_wnd = volatile.get('focus')
            book = context.get_frame().docBook
            delattr(self, '_pane')
            this_pane.commit()
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
            #if focus_wnd is not None:
            #    wx.CallAfter(focus_wnd.SetFocus)
        super(ClassDiagram, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for unload"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(ClassDiagram, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(ClassDiagram, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from beatle.activity.models.cc.ui import pane
            book = context.get_frame().docBook
            this_pane = pane.ClassDiagramPane(book, self)
            setattr(self, '_pane',  this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')
            #if focus_wnd is not None:
            #    wx.CallAfter(focus_wnd.SetFocus)

    @staticmethod
    def get_tab_bitmap():
        """Get the bitmap for tab control"""
        from beatle.app import resources as rc
        return rc.get_bitmap('classdiagram')
        # return get_bitmap(4)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('classdiagram')

    def unselect_all(self):
        """Unselect all elements"""
        for element in self._elements:
            element.set_selected(False)

    def draw_class_diagram(self, ctx, skip=None):
        """Draw class diagram"""
        skip = None
        for element in self._elements:
            if element._obj == skip:
                continue
            element.Draw(ctx)

    def __getstate__(self):
        """Set picke context"""
        state = dict(self.__dict__)
        if '_parentIndex' in state:
            del state['_parentIndex']
        if '_pane' in state:
            del state['_pane']
        if '_page_index' in state:
            del state['_page_index']
        return state

    def set_class_position(self, cls, pos):
        """Reposition a class element"""
        self.project.modified = True
        for element in self._elements:
            if element._obj == cls:
                element._pos = pos
                return element
        element = ClassElement(cls, pos)
        self._elements.append(element)
        return element

    def inside_rect(self, pos, width, height):
        return [element for element in self._elements if element.is_inside_rect(pos, width, height)]

    def HitTest(self, pos):
        """Check about element near pos"""
        result = (None, NO_WHERE)
        for element in self._elements:
            (a, b) = element.HitTest(pos)
            if b is not NO_WHERE:
                if element.is_selected:
                    return a, b
                else:
                    result = (a, b)
        return result

    def find_element(self, obj):
        """Finds a class diagram representation for obj"""
        for x in self._elements:
            if x._obj == obj:
                return x
        return None
