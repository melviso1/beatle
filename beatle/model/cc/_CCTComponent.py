'''
Created on 1 sept. 2018

@author: mel
'''

from beatle.model import TComponent


class CCTComponent(TComponent):
    '''
    This class acts as TComponent specialization for C++ datamodel classes.
    The properties that has sense only for C++ are sored here.
    This is critical specially for managed properties of Projects, because
    of the __getattr__ fallback.
    '''

    def __init__(self, **kwargs):
        '''
        Constructor
        '''
        super(CCTComponent, self).__init__(**kwargs)

    @property
    def types(self):
        """This method gets the list of visible types"""
        return (self.outer_class or self.parent).types

    @property
    def level_classes(self):
        """return the list of classes inside this level"""
        from ._Class import Class
        cls = self.outer_class
        return self(Class, filter=lambda x: x.parent.outer_class == cls)

    @property
    def inner_library(self):
        """returns the inner library if any"""
        from ._Library import Library
        return self.inner(Library)

    @property
    def inner_folder(self):
        """returns the innermost folder"""
        from ._Folder import Folder
        return self.inner(Folder)

    @property
    def namespaces(self):
        """returns the list of all namespaces"""
        from ._Namespace import Namespace
        return self(Namespace)

    def write_comment(self, pf):
        """Write the note using the writer"""
        # ok, now we place comments, if any
        s = self._note
        if type(s) is bytes:
            s = s.decode('ascii','replace')
        if len(s) > 0:
            pf.write_line("/**")
            s.replace('\r', '')
            lines = s.split("\n")
            for line in lines:
                line.replace('*/', '* /')
                pf.write_line("* {0}".format(line))
            pf.write_line("**/")


