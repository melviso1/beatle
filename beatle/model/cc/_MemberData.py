# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 08:28:25 2013

@author: mel
"""
from beatle.lib.tran import TransactionalMethod, TransactionStack, TransactionalMoveObject
from ._Member import Member
from ._CCContext import ContextDeclaration, ContextImplementation


class MemberData(Member):
    """Implements member data"""
    context_container = True

    # visual methods
    @TransactionalMethod('move member {0}')
    def drop(self, to):
        """Drops datamember inside project or another folder """
        target = to.inner_member_container
        #if not target or self.inner_class != target.inner_class or self.project != target.project:
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        "Initialization"
        self._typei = kwargs['type']
        self._access = kwargs.get('access', "public")
        self._static = kwargs.get('static', False)
        self._volatile = kwargs.get('volatile', False)
        self._mutable = kwargs.get('mutable', False)
        self._bitField = kwargs.get('bitfield', False)
        self._bitFieldSize = kwargs.get('bitfield_size', 0)
        self._default = kwargs.get('default', "")
        super(MemberData, self).__init__(**kwargs)
        if kwargs.get('update_ctors', True):
            self.inner_class.auto_args()
            self.inner_class.auto_init()
        if self.is_serial:
            # Honor container because pasting
            if not self.inner_class.is_serial:
                self.set_serial(False)
            else:
                self.inner_class.update_serial_methods()
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def default(self):
        return self._default

    @property
    def static(self):
        return self._static

    @property
    def volatile(self):
        return self._volatile

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(MemberData, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs['type'] = self._typei
        kwargs['access'] = self._access
        kwargs['static'] = self._static
        kwargs['volatile'] = self._volatile
        kwargs['mutable'] = self._mutable
        kwargs['bitfield'] = self._bitField
        kwargs['bitfield_size'] = self._bitFieldSize
        kwargs['default'] = self._default
        kwargs.update(super(MemberData, self).get_kwargs())
        return kwargs

    @ContextDeclaration()
    def write_declaration(self, file_writer):
        """Write the member declaration"""
        self.inner_class.ensure_access(file_writer, self._access)
        self.write_comment(file_writer)
        file_writer.write_line("{0};".format(self.label))

    @ContextImplementation()
    def write_code(self, file_writer):
        """Write the member implementation"""
        if self._static:
            file_writer.write_line("{0};".format(self.get_static_definition()))

    def get_initializer(self, serial=False):
        """Return the initializer sequence"""
        from ._Class import Class
        if len(self._default) > 0:
            return self._default
        if serial:
            ti = self.type_instance
            if type(ti.base_type) is Class and ti.base_type.is_serial:
                if not ( ti.is_ptr or ti.is_array or ti.is_ptr_to_ptr):
                    return '_'
            return ''

        return self._name

    @property
    def access(self):
        return self._access

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("member", self._access)

    def get_static_definition(self):
        """Return global, static definition"""
        if not self._static:
            return ""
        cls = self.inner_class
        if cls.is_template:
            template_class = cls
        else:
            outer_class = cls.outer_class
            if outer_class.is_template:
                template_class = outer_class
            else:
                template_class = None
        # type_format = self._typei.scoped_str([self.inner_class.scope])
        type_format = self._typei.scoped_str([])
        if self._volatile:
            type_format = "volatile " + type_format
        # if self._mutable:
        #     type_format = "mutable " + type_format
        # if self._static:
        #     type_format = "static " + type_format
        epigraph = ""
        if self._bitField:
            epigraph = ":" + str(self._bitFieldSize)
        # duplicated code?
        # cls = self.inner_class
        # if cls.is_template:
        #    type_format = "template<{}> ".format(getattr(self.inner_class, '_template')) + type_format
        prefixed_name = cls._memberPrefix + self._name
        if len(self._default.strip()) == 0:
            declaration = type_format.format(
                '{self.scope}{prefixed_name}{epigraph}'.format(
                    self=self, prefixed_name=prefixed_name, epigraph=epigraph))
        else:
            declaration = type_format.format('{self.scope}{prefixed_name}{epigraph}'.format(
                self=self, prefixed_name=prefixed_name, epigraph=epigraph)) + ' = ' + self._default
        if template_class is not None:
            return 'template<{cls._template}> {declaration}'.format(cls=template_class, declaration=declaration)
        else:
            return declaration

    def remove_relations(self):
        cls = self.outer_class
        super(MemberData, self).remove_relations()
        if cls is not None and not TransactionStack.in_undo_redo():
            cls.auto_args()
            cls.auto_init()
            if self._serial and cls._serial:
                cls.update_serial_methods()
            cls.export_code_files(force=True)

    def on_undo_redo_removing(self):
        super(MemberData, self).on_undo_redo_removing()

    def on_undo_redo_add(self):
        super(MemberData, self).on_undo_redo_add()
        if not TransactionStack.in_undo_redo():
            cls = self.outer_class
            if cls is not None:
                cls.auto_args()
                cls.auto_init()
                if self._serial and cls._serial:
                    cls.update_serial_methods()
                cls.export_code_files(force=True)

    def on_undo_redo_changed(self):
        """Make changes in the model as result of change"""
        super(MemberData, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            inner_class = self.inner_class
            if inner_class.is_serial:
                inner_class.update_serial_methods()
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    @property
    def label(self):
        """Get tree label"""
        scope = self.inner_class.scope
        if scope[-3:] == '>::':
            scope=scope[:scope.rfind('<')]+"::"
        s = self._typei.scoped_str([scope])
        if self._volatile:
            s = "volatile " + s
        if self._mutable:
            s = "mutable " + s
        if self._static:
            s = "static " + s
        epigraph = ""
        if self._bitField:
            epigraph = ":" + str(self._bitFieldSize)
        return s.format(self.prefixed_name) + " " + epigraph

    @property
    def prefixed_name(self):
        """local var name, with class prefix"""
        return self.inner_class._memberPrefix + self._name

    @property
    def type_instance(self):
        """return the type instance"""
        return self._typei
