# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""

from beatle.lib.tran import TransactionStack, TransactionalMethod, TransactionalMoveObject
from ._CCTComponent import CCTComponent


class Friendship(CCTComponent):
    """Implements friendship representation"""

    @TransactionalMethod('move friendship {0}')
    def drop(self, to):
        """Drops data member inside project or another folder """
        target = to.friendship_container and to
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """ Initialice the friendship. Required parameters:
            target: the friend class
            parent: the child class
        """
        if 'target' not in kwargs:
            raise ValueError('Missing target key argument at Friendship initialization.')
        if  'parent' not in kwargs:
            raise ValueError('Missing parent key argument at Friendship initialization.')
        self._target = kwargs['target']
        if 'name' not in kwargs:
            kwargs['name'] = self._target._name
        super(Friendship, self).__init__(**kwargs)
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    def delete(self):
        """Handle delete"""
        k = self.outer_class or self.outer_module
        super(Friendship, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs['target'] = self._target
        kwargs.update(super(Friendship, self).get_kwargs())
        return kwargs

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("friend")

    @property
    def label(self):
        """Get tree label"""
        from ._Class import Class
        target = self._target
        if isinstance(target, Class):
            return "friend {kind} {reference}".format(kind=target.kind, reference=target.scope[:-2])
        else:
            reference = target.implement
            if 'template<' in reference:
                reference = reference[reference.find('>')+1:].strip()
            return "friend {reference}".format(reference=reference)

    @property
    def declare(self):
        """Get tree label"""
        from ._Class import Class
        target = self._target
        if isinstance(target, Class):
            return "friend {kind} ::{reference}".format(kind=target.kind, reference=target.scope[:-2])
        else:
            reference = target.implement
            if 'template<' in reference:
                reference = reference[reference.find('>')+1:].strip()
            return "friend ::{reference}".format(reference=reference)

    def on_undo_redo_changed(self):
        """Make changes in the model as result of change"""
        super(Friendship, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

