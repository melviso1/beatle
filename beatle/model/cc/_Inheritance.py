# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
from beatle.lib.tran import TransactionStack
from ._ClassDiagram import ClassDiagram
from ._CCTComponent import CCTComponent


class Inheritance(CCTComponent):
    """Implements class representation"""
    def __init__(self, **kwargs):
        """ Initialice the inheritance. Required parameters:
                ancestor: the parent class
            parent: the child class
            Optional parameters are:
               name: the name of the inheritance. This parameter is
            really not expected, because the inheritance cannot be aliased."""
        assert 'ancestor' in kwargs
        assert 'parent' in kwargs
        self._ancestor = kwargs['ancestor']
        self._access = kwargs.get('access', "public")
        self._virtual = kwargs.get('virtual', False)
        self._template_args = kwargs.get('template_args', '')
        if 'name' not in kwargs:
            scoped = lambda x: (hasattr(x, 'scoped') and x.scoped) or x.name
            kwargs['name'] = scoped(self._ancestor)
        super(Inheritance, self).__init__(**kwargs)
        self._ancestor._deriv.append(self.parent)
        self.parent.auto_init()
        k = self.outer_class or self.outer_module
        if k:
            k.export_code_files(force=True)

    @property
    def ancestor(self):
        return self._ancestor

    @property
    def access(self):
        return self._access

    @property
    def template_args(self):
        return self._template_args

    @property
    def virtual(self):
        return self._virtual

    def delete(self):
        """Handle delete"""
        diagrams = [(x, x.find_element(self)) for x in self.project(ClassDiagram)]
        for diagram, element in diagrams:
            if element:
                diagram.save_state()
                diagram.remove_element(element)
        k = self.outer_class or self.outer_module
        super(Inheritance, self).delete()
        if k:
            k.export_code_files(force=True)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs['ancestor'] = self._ancestor
        kwargs['access'] = self._access
        kwargs['virtual'] = self._virtual
        kwargs['template_args'] = getattr(self, '_template_args', '')
        kwargs.update(super(Inheritance, self).get_kwargs())
        return kwargs

    @property
    def virtual_bases(self):
        """Gets the list of virtual bases through inheritance"""
        r = self._ancestor.virtual_bases
        if self._virtual and self._ancestor not in r:
            r.append(self._ancestor)
        return r

    def set_ancestor(self, ancestor):
        """Update ancestor"""
        if self._ancestor != ancestor:
            if self._ancestor is not None:
                self._ancestor._deriv.remove(self.parent)
        self._ancestor = ancestor
        if self._ancestor is not None:
            self._ancestor._deriv.append(self.parent)
            self._name = self._ancestor._name

    def remove_relations(self):
        """Utility for undo/redo"""
        self._ancestor._deriv.remove(self.parent)
        super(Inheritance, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        self._ancestor._deriv.append(self.parent)
        super(Inheritance, self).restore_relations()

    def on_undo_redo_changed(self):
        """Reflect changes"""
        for x in self.parent._deriv:
            x.on_undo_redo_changed()
        super(Inheritance, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            k = self.outer_class or self.outer_module
            if k:
                k.export_code_files(force=True)

    def on_undo_redo_removing(self):
        super(Inheritance, self).on_undo_redo_removing()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        #self._ancestor._deriv.append(self.parent)
        if not TransactionStack.in_undo_redo():
            # Check class diagrams for update
            if self.project is not None:
                dias = self.project(ClassDiagram)
                for dia in dias:
                    # If inheritance is already, skip
                    if dia.find_element(self) is not None:
                        continue
                    # Check if both classes are represented in
                    parent = dia.find_element(self._ancestor)
                    if parent is None:
                        continue
                    child = dia.find_element(self.inner_class)
                    if child is None:
                        continue
                    # Inheritance must be added to transaction
                    # (even when hidden)
                    TransactionStack.do_save_state(dia)
                    dia.add_inheritance(self, parent, child)
        super(Inheritance, self).on_undo_redo_add()

    def draggable(self):
        """we don't allow this can be moved."""
        return False

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc      
        return rc.get_bitmap_index("inheritance", self._access)

    @property
    def tree_label(self):
        """Get tree label"""
        if len(getattr(self, '_template_args','')) > 0:
            return 'inherits {ancestor}<{args}>'.format(ancestor=self._ancestor.scoped, args=self._template_args)
        else:
            return "inherits " + self._ancestor.scoped


