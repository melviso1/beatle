# -*- coding: utf-8 -*-

"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""

import os
import re
import copy
from beatle.model.writer import Writer
from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject, TransactionStack, DelayedMethod
from beatle.lib.decorators import upgrade_version
from ._CCContext import ContextDeclaration, ContextImplementation
from ._ClassDiagram import ClassDiagram
from ._Folder import Folder
from ._CCTComponent import CCTComponent
from ._Inheritance import Inheritance
from ._SetterMethod import SetterMethod
from ._GetterMethod import GetterMethod
from ._MemberData import MemberData
from ._Argument import Argument
from ._Constructor import Constructor
from ._Destructor import Destructor
from ._Enum import Enum
from ._Type import Type, typeinst
from ._MemberMethod import MemberMethod
from ._InitMethod import InitMethod
from ._ExitMethod import ExitMethod
from ._IsClassMethod import IsClassMethod
from ._Relation import RelationFrom, RelationTo
from ._Function import Function
from ._Data import Data
from ._Friendship import Friendship
from beatle.lib.utils import cached_type


class Class(CCTComponent):
    """Implements c++ class representation"""
    class_container = True
    context_container = True
    folder_container = True
    diagram_container = True
    inheritance_container = True
    member_container = True
    relation_container = True
    enum_container = True
    type_container = True
    friendship_container = True

    # default empty user sections
    user_code_h1 = ''
    user_code_h2 = ''
    user_code_h3 = ''
    user_code_h4 = ''
    user_code_s1 = ''
    user_code_s2 = ''
    user_code_s3 = ''

    # visual methods
    @TransactionalMethod('move class {0}')
    def drop(self, to):
        """Drops class inside project or another folder """
        target = to.inner_class_container
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initialization method"""
        self._is_external = kwargs.get("is_external", False)
        self._transactional = kwargs.get("transactional", False)
        self._is_struct = kwargs.get("is_struct", False)
        self._access = kwargs.get('access', "public")
        self._template = kwargs.get('template', None)
        self._template_types = kwargs.get('template_types', [])
        self._template_arguments = kwargs.get('template_arguments', [])
        self._deriv = kwargs.get('derivatives', [])
        self._memberPrefix = kwargs.get('prefix', '_')
        self._lastHdrTime = None
        self._lastSrcTime = None
        self._header_obj = None  # linked with file
        self._source_obj = None  # linked with file
        self._save_contents = None
        self._load_contents = None
        self._save_method = None
        self._load_method = None
        self._save_relations = None
        self._load_relations = None
        self._get_type_method = None
        self._context_access = None  # temporary var
        self._inlines = None
        self._inheritance_level = 0  # temporary var
        self._export_prefix = kwargs.get('export_prefix', '')
        self._plugin = {}  # external plugin
        super(Class, self).__init__(**kwargs)
        if not self._is_external:
            self.project.export_code_files(force=True)
        if self.is_serial:
            self.add_serialization()

    def open_line(self, line):
        """Open member method based on line, if possible"""
        elements = self(InitMethod,
                        ExitMethod, Constructor, MemberMethod,
                        SetterMethod, GetterMethod, IsClassMethod, Destructor, )
        info_list = [x for x in elements if getattr(x, 'source_line', line + 1) <= line]
        if len(info_list) == 0:
            return False
        candidate = sorted([(x.source_line, x) for x in info_list], key=lambda tup: tup[0])[-1][1]
        if not hasattr(candidate, 'open_line'):
            return False
        # before doing this as good, compute relative offset and check the line is inside
        line_offset = line - candidate.source_line
        if line_offset >= candidate.code_lines:
            return False
        return candidate.open_line(line_offset)

    def set_source(self, source_obj):
        """Sets a vincle with a file"""
        self._source_obj = source_obj

    def set_header(self, header_obj):
        """Sets a vincle with a file"""
        self._header_obj = header_obj

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'is_struct': self._is_struct,
            'access': self._access,
            'template': self._template,
            'template_types': self._template_types,
            'derivatives': self._deriv,
            'prefix': self._memberPrefix,
            'export_prefix': self._export_prefix,
            'is_external': self.is_external,
            'transactional': self.is_transactional,
        }
        kwargs.update(super(Class, self).get_kwargs())
        return kwargs

    @property
    def is_template(self):
        if self._template is not None and len(self._template) > 0:
            return True
        else:
            return False

    @property
    def is_transactional(self):
        return self._transactional

    @property
    def template(self):
        return self._template

    @property
    def is_template(self):
        return self._template is not None

    @property
    def clean_template(self):
        """Return template string without defaults"""
        return re.sub('=[^,]*', '', self._template)

    def lower(self):
        """Criteria for sorting when generating code"""
        return self._inheritance_level

    # backward compatibility code - to simplfy in newer versions
    @property
    def is_external(self):
        """return info about if the class is external"""
        if not hasattr(self, '_is_external'):
            self._is_external = False
        return self._is_external

    @property
    def virtual_bases(self):
        """Get the list of all virtual base classes"""
        # some comments: The inheritance icons will NEVER be hidden
        # by nested folders. It will be ALLWAYS be a direct child of class
        r = []
        for u in self[Inheritance]:
            r.extend([x for x in u.virtual_bases if x not in r])
        return r

    @property
    def direct_bases(self):
        """Get the list of the direct base classes"""
        return [x.ancestor for x in self[Inheritance]]

    @property
    def bases(self):
        """Get the list of direct or indirect bases"""
        r = self.direct_bases
        for u in self[Inheritance]:
            r.extend([x for x in u.ancestor.bases if x not in r])
        return r

    @property
    def member_prefix(self):
        return self._memberPrefix

    @property
    def export_prefix(self):
        return self._export_prefix

    def get_constructor_arguments(self):
        """Get the list of arguments required by the class constructor"""
        # The arguments required by the class are determined following
        # the rules (in this order):
        # 1º Required arguments from virtual base classes
        # 2º Required arguments from normal base classes (not virtual)
        # 3º non-static Members without initialization
        # ** NOT: 4º Required arguments from relation parents...
        #
        # For base classes (both virtual or not), the required arguments
        # are either determined by preferred constructors, either none.
        #
        # If some argument matches in name, the later will take precedence
        #
        # Ok, let's go...
        # 1º & 2º.....
        # UPDATE : we must *not* include serialization constructors in this
        # lookup, for evident reasons.
        virtual_bases = self.virtual_bases
        normal_inheritance = [x.ancestor for x in self[Inheritance] if x.ancestor not in virtual_bases]
        # create a contributor's list
        inheritance = virtual_bases
        inheritance.extend(normal_inheritance)
        inherit_arguments = []
        inherit_argument_names = []
        # append arguments from preferred ctors
        for x in inheritance:
            ctor = x.get_preferred_constructor()
            if ctor is not None:
                for arg in ctor[Argument]:
                    if arg.name not in inherit_argument_names:
                        inherit_arguments.append(arg)
                        inherit_argument_names.append(arg.name)
        # 3º append (without class prefix) non-static uninitialized members
        argument_list = [x for x in
                         self(MemberData, filter=lambda x: x.inner_class == self, cut=True)
                         if x.inner_class is self and x.static is False
                         and len(x.default) == 0]
        argument_name_list = [x.name for x in argument_list]
        argument_list.extend([x for x in inherit_arguments if x.name not in argument_name_list])
        # 4º append relations is not required:
        # It's responsibility of the ctor's, because serial ctor's don't
        # call init methods
        return argument_list

    def update_class_relations(self):
        """This method gets invoked when something changes in class relations specification
        and ensure mainteinance of internal methods and ctors"""
        self.update_init_method()
        self.update_exit_method()
        self.auto_args()
        self.auto_init()
        self.update_init_calls()
        self.update_exit_calls()

    def auto_init(self):
        """Update all init ctors"""
        for x in self(Constructor,
                      filter=lambda z: z.inner_class == self, cut=True):
            x.auto_init()

    def update_init_calls(self):
        """Update the calls to init method"""
        for x in self(Constructor,
                      filter=lambda z: z.inner_class == self, cut=True):
            x.update_init_call()

    def update_exit_calls(self):
        """Update the calls to exit method"""
        for x in self(Destructor, filter=lambda x: x.inner_class == self, cut=True):
            x.update_exit_call()

    def auto_args(self):
        """Update all ctor arguments"""
        for x in self(
                Constructor,
                filter=lambda item: item.inner_class == self, cut=True):
            x.auto_args()

    def on_undo_redo_removing(self):
        """Handle on_undo_redo_removing, prevent generating files"""
        stack = TransactionStack
        method = self.export_code_files.inner
        if method not in stack.delayedCallsFiltered:
            stack.delayedCallsFiltered.append(method)
        super(Class, self).on_undo_redo_removing()

    @DelayedMethod()
    def export_code_files(self, force=False, logger=None):
        """Do code generation"""
        try:
            self.update_sources(force)
            self.update_headers(force)
        #         except:
        #             print 'Failed exporting files for class {0}'.format(self.name)
        except Exception as inst:
            import traceback
            import sys
            traceback.print_exc(file=sys.stdout)
            print(type(inst))  # the exception instance
            print(inst.args)  # arguments stored in .args
            print(inst)

    @property
    def base_filename(self):
        """Return the base filename used for both, header and source name.
        This base filename is with the form <namespace1>.<namespace2>...<namespaceN>.<outerclass>
        """
        from ._Namespace import Namespace
        cls = self.outer_class
        if cls != self:
            return cls.base_filename
        k = [self._name]
        s = self.parent
        while s:
            if type(s) is Namespace:
                k.insert(0, s.name)
            s = s.parent
        return '.'.join(k)

    @property
    def abs_path_source(self):
        """Return the absolute path of source file container
        """
        return os.path.join(self.project.sources_dir, self.base_filename + ".cpp")

    def update_sources(self, force=False):
        """does source generation"""
        if self.is_external or self._template:
            return
        file_name = self.abs_path_source
        regenerate = False
        if force or not os.path.exists(file_name) or self._lastSrcTime is None:
            regenerate = True
        elif os.path.getmtime(file_name) < self._lastSrcTime:
            regenerate = True
        if not regenerate:
            return
        f = open(file_name, 'wt', encoding='ascii', errors='ignore')
        file_writer = Writer.for_file(f)
        # master include file
        project = self.project
        if project._license is not None:
            custom_announce = getattr(self, 'custom_announce', None)
            if custom_announce is not None:
                file_writer.write_line(custom_announce)
            else:
                file_writer.write_line("/*****************************************************************************************")
                file_writer.write_line(" *")
                file_writer.write_line(" * (c) {date} by {author} ".format(date=project._date, author=project._author))
                file_writer.write_line(" *")
                file_writer.write_line(" * This file is {file}.cpp, part of the project {project}.".format(
                    file=self.base_filename, project=project.name))
                file_writer.write_line(" * This project is licensed under the terms of the {license} license.".format(
                    license=project._license))
                file_writer.write_line(" *")
                file_writer.write_line(" * Read the companion LICENSE file for further details.")
                file_writer.write_line(" *")
                file_writer.write_line(" ****************************************************************************************/")
            file_writer.write_newline()
        file_writer.write_line("// {user.before.include.begin}")
        file_writer.write_line(self.user_code_s1)
        file_writer.write_line("// {user.before.include.end}")
        file_writer.write_newline()
        if project.use_master_include:
            file_writer.write_line('#include "{0}"'.format(project.master_include))
        else:
            file_writer.write_line('#include "{0}.h"'.format(self._name))
        file_writer.write_newline()

        # some facility
        file_writer.write_line("#include <assert.h>")
        file_writer.write_newline()

        # We are using full path declarations, we dont need
        # enclosed namespaces in source
        # ns = self.inner_namespace
        # if ns is not None:
        #    file_writer.write_line('namespace {}{{'.format(ns.scoped))
        self.write_code(file_writer)
        # ns = self.inner_namespace
        # if ns is not None:
        #    file_writer.write_line('}}  // end namespace {}'.format(ns.scoped))
        file_writer.write_newline()
        f.close()

    @property
    def kind(self):
        """Returns the class flavor"""
        return (self._is_struct and "struct") or "class"

    @property
    def is_struct(self):
        return self._is_struct;

    @property
    def derivatives(self):
        """Return the list of knows classes that have this as ancestor"""
        return self._deriv

    def ensure_access(self, file_writer, access):
        """utility method that ensures access update when needed"""
        if access != self._context_access:
            self._context_access = access
            file_writer.write_line("{0}:".format(access))

    @ContextDeclaration()
    def write_declaration(self, file_writer):
        """Write the class declaration"""
        # if this is a nested class, ensure access
        pc = self.parent.inner_class
        ns = None
        if pc:
            pc.ensure_access(file_writer, self._access)
        else:
            ns = self.inner_namespace
        ref = self.unscoped_reference
        self._context_access = (self._is_struct and "public") or "private"
        self._inlines = []
        # ok, create user section
        file_writer.write_line("// {{user.before.{0}.begin}}".format(ref))
        file_writer.write_line(self.user_code_h1)
        file_writer.write_line("// {{user.before.{0}.end}}".format(ref))
        if hasattr(self, '_plugin'):
            for element in self._plugin:
                if 'declare_before' in self._plugin[element]:
                    file_writer.write_line(self._plugin[element]['declare_before'])
        file_writer.write_newline()
        # create inheritance strings
        if len(self[Inheritance]):
            s = ": "
            for o in self[Inheritance]:
                ancestor = o.ancestor.scoped
                args = getattr(o, '_template_args', '')
                if args != '':
                    ancestor = '{ancestor}<{args}>'.format(ancestor=ancestor, args=args)
                if o.virtual:
                    s += "{0} virtual {1},".format(o.access, ancestor)
                else:
                    s += "{0} {1},".format(o.access, ancestor)
            s = s[:-1]
        else:
            s = ""
        if ns is not None:
            file_writer.write_line('namespace {}{{'.format(ns.scoped))
        file_writer.write_newline()
        self.write_comment(file_writer)
        # class X
        file_writer.write_line("{ref} {s}".format(ref=ref, s=s))
        file_writer.open_brace()
        file_writer.write_newline()
        file_writer.write_line("// {{user.inside.first.{0}.begin}}".format(ref))
        file_writer.write_line(self.user_code_h2)
        file_writer.write_line("// {{user.inside.first.{0}.end}}".format(ref))
        file_writer.write_newline()
        if hasattr(self, '_plugin'):
            for element in self._plugin:
                if 'declare_inside_first' in self._plugin[element]:
                    file_writer.write_line(self._plugin[element]['declare_inside_first'])

        # write enums first
        def self_element(x):
            return x.parent.inner_class == self

        for element in self(Enum, filter=self_element, cut=True):
            element.write_declaration(file_writer)

        # write types
        _type = {'public': [], 'protected': [], 'private': []}
        f = lambda x: x.inner_class == self and getattr(x, '_definition', 'True').strip()
        for t in self(Type, filter=f, cut=True):
            _type[t.access].append(t)
        for access in _type:
            if not _type[access]:
                continue
            file_writer.write_line('{0}:'.format(access))
            file_writer.write_newline()
            for t in _type[access]:
                file_writer.write_line('//type definition for {0}'.format(t._name))
                file_writer.write_line('{0}'.format(t._definition))
                file_writer.write_newline()

        for element in self(Class,
                            filter=self_element, cut=True):
            element.write_declaration(file_writer)

        # write friend declarations (after nested classes)
        for f in self.friends:
            file_writer.write_line('{0};'.format(f.declare))

        # we will follow and respect the visual order
        # get extended methods with not explicit order
        member_methods_extended = [x for x in self.project.member_method_classes if x not in [
            InitMethod, ExitMethod, IsClassMethod, SetterMethod, GetterMethod]]
        element_classes = [
            MemberData, InitMethod, ExitMethod, Constructor] + member_methods_extended + [
            SetterMethod, GetterMethod, IsClassMethod, Destructor]
        for element in self(*element_classes, filter=self_element, cut=True):
            element.write_declaration(file_writer)

        file_writer.write_line("// {{user.inside.last.{0}.begin}}".format(ref))
        file_writer.write_line(self.user_code_h3)
        file_writer.write_line("// {{user.inside.last.{0}.end}}".format(ref))
        if hasattr(self, '_plugin'):
            for element in self._plugin:
                if 'declare_inside_last' in self._plugin[element]:
                    file_writer.write_line(self._plugin[element]['declare_inside_last'])

        file_writer.close_brace(";")
        file_writer.write_newline()
        if ns is not None:
            file_writer.write_line('}}  // end namespace {}'.format(ns.scoped))
            file_writer.write_newline()

        file_writer.write_line("// {{user.after.{0}.begin}}".format(ref))
        # temp fix
        if not hasattr(self, 'user_code_h4'):
            self.user_code_h4 = ''
        file_writer.write_line(self.user_code_h4)
        file_writer.write_line("// {{user.after.{0}.end}}".format(ref))
        file_writer.write_newline()
        if hasattr(self, '_plugin'):
            for element in self._plugin:
                if 'declare_after' in self._plugin[element]:
                    file_writer.write_line(self._plugin[element]['declare_after'])
        del self._context_access

    @property
    def plugin(self):
        if not hasattr(self, '_plugin'):
            self._plugin = {}
        return self._plugin

    @property
    def abs_path_header(self):
        """Return the absolute path of source"""
        return os.path.join(self.project.headers_dir, self.base_filename + ".h")

    def update_headers(self, force=False):
        """Realiza la generacion de fuentes"""
        if self.is_external:
            return
        file_name = self.abs_path_header
        regenerate = False
        if force or not os.path.exists(file_name) or self._lastHdrTime is None:
            regenerate = True
        elif os.path.getmtime(file_name) < self._lastHdrTime:
            regenerate = True
        if not regenerate:
            return
        with open(file_name, 'wt', encoding='ascii', errors='ignore') as f:
            file_writer = Writer.for_file(f)
            project = self.project

            if project._license is not None:
                custom_announce = getattr(self, 'custom_announce', None)
                if custom_announce is not None:
                    file_writer.write_line(custom_announce)
                else:
                    file_writer.write_line(
                        "/*****************************************************************************************")
                    file_writer.write_line(" *")
                    file_writer.write_line(" * (c) {date} by {author} ".format(date=project._date, author=project._author))
                    file_writer.write_line(" *")
                    file_writer.write_line(" * This file is {file}.h, part of the project {project}.".format(
                        file=self.base_filename, project=project.name))
                    file_writer.write_line(" * This project is licensed under the terms of the {license} license.".format(
                        license=project._license))
                    file_writer.write_line(" *")
                    file_writer.write_line(" * Read the companion LICENSE file for further details.")
                    file_writer.write_line(" *")
                    file_writer.write_line(
                        " ****************************************************************************************/")
                file_writer.write_newline()

            # create safeguard
            safe_name = self._name.upper().replace('.', '_').replace('-', '_')
            safeguard = safe_name + "_H_INCLUDED"
            file_writer.write_line("#if !defined({0})".format(safeguard))
            file_writer.write_line("#define {0}".format(safeguard))
            file_writer.write_newline()

            # we are using master include? if not, include inheritances
            if not project.use_master_include and self.outer_class == self:
                # iterate over inheritances
                if len(self._child[Inheritance]):
                    file_writer.write_line("//base includes")
                    for o in self[Inheritance]:
                        file_writer.write_line("#include \"{0}.h\"".format(o._name))
                    file_writer.write_newline()
                # we need to forward references for relations
                if len(self.child[RelationFrom]):
                    file_writer.write_line("//forward references of parent classes")
                    for relation in self[RelationFrom]:
                        file_writer.write_line(relation.key.from_class.reference)
                if len(self.child[RelationTo]):
                    file_writer.write_line("//forward references of child classes")
                    for relation in self[RelationFrom]:
                        file_writer.write_line(relation.key.to_class.reference)
            file_writer.write_newline()
            self.write_declaration(file_writer)
            # end safeguard
            file_writer.write_line("#endif //{0}".format(safeguard))
            # write inlines or template methods
            file_writer.write_newline()
            if len(self._inlines) > 0 or self._template:
                file_writer.write_line("#if defined(INCLUDE_INLINES)")
                file_writer.write_line("#if !defined(INCLUDE_INLINES_{name})".format(name=self._name.upper()))
                file_writer.write_line("#define INCLUDE_INLINES_{name}".format(name=self._name.upper()))
                file_writer.write_newline()
            # we don't need anymore this namespace closure because we use full-specified names
            # ns = self.inner_namespace
            # if ns is not None:
            #     file_writer.write_line('namespace {}{{'.format(ns.scoped))
            if len(self._inlines) > 0:
                if self._template:
                    self.write_code(file_writer)
                else:
                    for o in self._inlines:
                        o.write_code(file_writer)
            file_writer.write_newline()
            # if ns is not None:
            #    file_writer.write_line('}}  // end namespace {}'.format(ns.scoped))
            #    file_writer.write_newline()
            if len(self._inlines) > 0 or self._template:
                file_writer.write_line("#endif //(INCLUDE_INLINES_{name})".format(name=self._name.upper()))
                file_writer.write_line("#endif //INCLUDE_INLINES")
                file_writer.write_newline()
            if len(self._inlines):
                del self._inlines
            self._lastHdrTime = os.path.getmtime(file_name)
            file_writer.write_newline()

    @ContextImplementation()
    def write_code(self, file_writer):
        """Write code for class"""
        tag = '{self.kind}.{self._name}'.format(self=self)
        file_writer.write_line("// {{user.before.{tag}.begin}}".format(tag=tag))
        file_writer.write_line(self.user_code_s2)
        file_writer.write_line("// {{user.before.{tag}.end}}".format(tag=tag))
        file_writer.write_newline()

        # we have an interesting problem here:
        # From the point of view of easy coding and track context
        # we will follow and respect the visual order
        if self._template:
            declarative = lambda x: getattr(x, '_deleted', False)
        else:
            declarative = lambda x: getattr(x, '_inline', False) or getattr(x, '_deleted', False) or \
                                    getattr(x, '_template', False)
        self_member = lambda x: x.parent.inner_class == self and not declarative(x)
        if self.outer_class == self:
            elements = self(MemberData, filter=lambda x: x.static)
        else:
            elements = []
        # do it with steps for ensuring order.
        # get extended methods with not explicit order
        member_methods_extended = [x for x in self.project.member_method_classes if x not in [
            InitMethod, ExitMethod, IsClassMethod, SetterMethod, GetterMethod]]
        elements += self(InitMethod, filter=self_member, cut=True)
        elements += self(ExitMethod, filter=self_member, cut=True)
        elements += self(Constructor, filter=self_member, cut=True)
        elements += self(IsClassMethod, filter=self_member, cut=True)
        elements += self(*member_methods_extended, filter=self_member, cut=True)
        elements += self(SetterMethod, filter=self_member, cut=True)
        elements += self(GetterMethod, filter=self_member, cut=True)
        elements += self(Destructor, filter=self_member, cut=True)
        elements += self(Class, filter=self_member, cut=True)

        # obtain all the interesting elements and write
        for element in elements:
            element.write_code(file_writer)

        file_writer.write_newline()
        file_writer.write_line("// {{user.after.{tag}.begin}}".format(tag=tag))
        file_writer.write_line(self.user_code_s3)
        file_writer.write_line("// {{user.after.{tag}.end}}".format(tag=tag))
        file_writer.write_newline()

    def get_preferred_constructor(self, serial=False):
        """Get the user set as preferred constructor or None"""
        ctors = self(Constructor, filter=lambda x: x.inner_class == self and x.is_serial == serial)
        for ctor in ctors:
            if ctor.is_preferred():
                return ctor
        # if not preferred, we return the first if any
        if len(ctors):
            return ctors[0]
        return None

    def get_serial_constructor(self):
        """Get the constructor used for class serialization or None"""
        for ctor in self(Constructor, filter=lambda x: x.inner_class == self):
            if ctor.is_serial:
                return ctor
        return None

    @property
    def can_delete(self):
        """Check if class can be deleted"""
        def outer(element):
            """Check that element dont holds self"""
            return self not in element.path

        if len(self.derivatives) > 0:
            return False
        project = self.project
        # get class member list
        if project is not None:
            # check if outer element is using this class
            typed = project(MemberData, MemberMethod,
                            Argument, filter=outer, cut=True)
            typed += project(Function, Data)
            for x in typed:
                if x.type_instance.type == self:
                    return False
        return super(Class, self).can_delete

    def delete(self):
        """Delete related objects"""
        project = self.project
        for dia in self.project(ClassDiagram):
            # Check if inherit is in
            elem = dia.find_element(self)
            if elem is not None:
                dia.save_state()
                dia.remove_element(elem)
                pane = getattr(dia, '_pane', None)
                if pane is not None:
                    pane.Refresh()
        if hasattr(self, '_header_obj') and self._header_obj:
            self._header_obj.delete()
        if hasattr(self, '_source_obj') and self._source_obj:
            self._source_obj.delete()
        friends = self.project(Friendship, filter=lambda x: x._target == self)
        for f in friends:
            f.delete()
        # Relations must be also deleted from here in order to break delete loops
        from_list = self[RelationFrom]
        for f in from_list:
            f.key.delete()
        to_list = self[RelationTo]
        for f in to_list:
            f.key.delete()
        super(Class, self).delete()
        project.export_code_files(force=True)

    def save_state(self):
        """Utility for saving state"""
        for rf in self[RelationFrom]:
            rf.key.save_state()
        for rt in self[RelationTo]:
            rt.key.save_state()
        super(Class, self).save_state()

    def on_undo_redo_changed(self):
        """Update from app"""
        def direct_member(member):
            return member.inner_class == self

        # when the class is updated, ctor's and dtor's, must be updated
        for ctor in self(Constructor, filter=direct_member):
            ctor.name = self._name
            ctor.on_undo_redo_changed()
        for dtor in self(Destructor, filter=direct_member):
            dtor.name = self._name
            dtor.on_undo_redo_changed()
        # inheritance labels must be changed
        for derivative in self._deriv:
            for inheritance in derivative[Inheritance]:
                if inheritance.ancestor == self:
                    inheritance.name = self._name
                    inheritance.on_undo_redo_changed()
        # relation labels must be changed?
        for relation in self[RelationFrom]:
            relation.key.to_relation.name = self._name
            relation.key.to_relation.on_undo_redo_changed()
        for relation in self[RelationTo]:
            relation.key.from_relation.name = self._name
            relation.key.from_relation.on_undo_redo_changed()
        project = self.project
        # update class diagrams
        diagram_list = project(ClassDiagram)
        for diagram in diagram_list:
            # Check if class is in
            elem = diagram.find_element(self)
            if elem is not None:
                elem.layout()
                pane = getattr(diagram, '_pane', None)
                if pane is not None:
                    pane.Refresh()
        # update class usage as type
        typed = project(MemberData, MemberMethod, Argument)
        subject = [x for x in typed if x.type_instance.type == self]
        # before nesting applying changes we must be sure about correct serialization status
        if not TransactionStack.in_undo_redo():
            self.update_class_serialization()
        # iterate applying changes
        for element in subject:
            element.on_undo_redo_changed()
        super(Class, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            project.export_code_files(force=True)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Class, self).on_undo_redo_add()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        if not hasattr(self, '_access'):
            self._access = 'public'
        return rc.get_bitmap_index('class', self._access)

    @property
    def tree_label(self):
        """Get tree label"""
        if self._is_struct:
            cos = 'struct'
        else:
            cos = 'class'
        if self._template:
            return 'template<{self._template}> {self.kind} {self.name}'.format(self=self)
        else:
            if len(self._export_prefix) > 0:
                return '{self.kind} {self._export_prefix} {self.name}'.format(self=self)
            else:
                return '{self.kind} {self.name}'.format(self=self)

    @property
    def declare(self):
        """"""
        if self._is_struct:
            cos = 'struct'
        else:
            cos = 'class'
        if self._template:
            return 'template<{clase._template}> {cos} {clase._name};'.format(cos=cos, clase=self)
        else:
            return '{cos} {clase._name};'.format(cos=cos, clase=self)

    def local_declaration(self, ns):
        """return declarations discarding local scope"""
        if ns is None:
            return self.scoped_declaration
        s = self.scoped_str([ns.scoped])
        if self._template:
            return f'template<{self._template}> {self.kind} {s};'
        return f'{self.kind} {s};'

    @property
    def scoped_declaration(self):
        """return full scoped declaration"""
        return ((self._template and 'template<{self._template}> {self.kind} {self.scoped};') or
                '{self.kind} {self.scoped};').format(self=self)

    def is_ancestor(self, cls):
        """Checks for ancestor relationship"""
        if Inheritance not in cls.children:  # the class has no bases
            return False
        inheritance_list = cls[Inheritance]
        for inheritance in inheritance_list:
            if inheritance.ancestor == self:  # we are base class
                return True
            if self.is_ancestor(inheritance.ancestor):  # we are base class of some base
                return True
        return False

    def update_init_method(self):
        """Update the init methods"""
        methods = self.init_methods
        aggregates = [x for x in self.relations_from if x.key.is_aggregate]
        # if there are no relations we must delete init method
        if len(aggregates) == 0:
            for x in methods:
                x.delete()
            return
        if len(methods) == 0:
            method = InitMethod(parent=self, type=typeinst(type=cached_type(self.project, 'void')))
        else:
            method = methods[0]
            method.save_state()
        method.update_code()
        method.update_arguments()

    def update_exit_method(self):
        """update the exit methods"""
        methods = self.exit_methods
        parent_relations = self.relations_to
        child_relations = self.relations_from
        # if there are no relations we must delete exit methods
        if not parent_relations and not child_relations:
            for x in methods:
                x.delete()
            return
        # ok, if there are no method we must create it and if
        # exists, update it
        if len(methods) == 0:
            method = ExitMethod(parent=self,
                                type=typeinst(type=cached_type(self.project, 'void')))
        else:
            method = methods[0]
            method.save_state()
        method.update_code()

    def add_serialization(self):
        """This method is responsible for adding serialization capabilities to
        the class. Any class with serialization support must inherit the runtime_instance
        base class.
        For convenience purposes, the serialization has some cases:
        - data serialization : This case only serialize class contents (including base classes contents)
        - context serialization : This case also includes the serialization of the class relations.
        - relation local serialization : This case is used to preserve relation information during changes.
        The intended use of data serialization is to do a "local" serialization of the class while the relation
        serialization is a full serialization of the class and related ones.
        The data serialization is intended to be used for preserving internal state of the class while the
        relations with another classes remains intact. The intended usage is saving the class state
        during transactional operations.
        Other operations, like delete or change the relations of classes during transactional operations
        will require an extra careful handling done by relation local serialization.
        data serializations are done by _save_contents and _load_contents
        context serializations are done by _save_method and _load_method
        """
        # we use the serial constructor as flag about if the serialization is already present
        constructor = self.get_serial_constructor()
        if constructor is not None:
            return
        # we use a folder for serialization purposes
        serial_folder = Folder(parent=self, name='serialization', read_only=True)

        # ok, we must add first the inheritance from runtime_instance
        if self._transactional:
            runtime_instance = cached_type(self.project, "trans::transactional_instance")
        else:
            runtime_instance = cached_type(self.project, "serial::runtime_instance")
        type_record = cached_type(self.project, "serial::type_record")
        oxstream = cached_type(self.project, "serial::oxstream")
        ixstream = cached_type(self.project, "serial::ixstream")
        void_type = cached_type(self.project, "void")

        Inheritance(
            parent=self, ancestor=runtime_instance, virtual=False, access='public',
            template_args=self.name, read_only=True, visibleInTree=False)
        # add a serial constructor
        serial_constructor = Constructor(
            parent=serial_folder, access='public', read_only=True, autoargs=False, serial=True, draggeable=False)
        Argument(
            parent=serial_constructor, name='_', type=typeinst(type=type_record, ptr=True, const=True),
            read_only=True)
        serial_constructor.auto_init(force=True)

        # ok, now, we must define two important methods to be used with serialization:
        self._save_method = MemberMethod(
            parent=serial_folder, name="save", virtual=True, pure=False, read_only=True,
            type=typeinst(type=void_type), constmethod=True, draggeable=False,
            note="""
            This method serializes the class and related descent ones. This method is used
            for serializing the class for permanent storage.
            """
        )
        Argument(parent=self._save_method, name="os", type=typeinst(type=oxstream, ref=True), read_only=True)

        self._save_contents = MemberMethod(
            parent=serial_folder, name="save_contents", virtual=True, pure=False, read_only=True,
            type=typeinst(type=void_type), constmethod=True, draggeable=False,
            note="""
            This method serializes only the data members of the class (and bases), not his relations.
            This method is intended to be used for transactional support.
            """
        )
        Argument(parent=self._save_contents, name="os", type=typeinst(type=oxstream, ref=True), read_only=True)

        self._save_relations = MemberMethod(
            parent=serial_folder, name="save_relations", virtual=True, pure=False, read_only=True,
            type=typeinst(type=void_type), constmethod=True, draggeable=False,
            note="""
            This method serializes only the relations of the class (and bases) in memory.
            This method is intended to be used for transactional support.
            """
        )
        Argument(parent=self._save_relations, name="os", type=typeinst(type=oxstream, ref=True), read_only=True)

        self._load_method = MemberMethod(
            parent=serial_folder, name="load", virtual=True, pure=False, read_only=True,
            type=typeinst(type=void_type), draggeable=False,
            note="""
            This method serializes the class and related descent ones. This method is used
            for serializing the class for permanent storage.
            """
        )
        Argument(parent=self._load_method, name="is", type=typeinst(type=ixstream, ref=True), read_only=True)

        self._load_contents = MemberMethod(
            parent=serial_folder, name="load_contents", virtual=True, pure=False, read_only=True,
            type=typeinst(type=void_type), draggeable=False,
            note="""
            This method serializes only the data members of the class (and bases), not his relations.
            This method is intended to be used for transactional support.
            """
        )
        Argument(parent=self._load_contents, name="is", type=typeinst(type=ixstream, ref=True), read_only=True)

        self._load_relations = MemberMethod(
            parent=serial_folder, name="load_relations", virtual=True, pure=False, read_only=True,
            type=typeinst(type=void_type), draggeable=False,
            note="""
            This method serializes only the relations of the class (and bases) in memory.
            This method is intended to be used for transactional support.
            """
        )
        Argument(parent=self._load_relations, name="is", type=typeinst(type=ixstream, ref=True), read_only=True)

        self.update_serial_methods()

    def update_serial_methods(self):
        """This method is brute force for the moment, in order to progress.
        This must be split, we must introduce intelligent data serialization."""

        # One very mutable special requirement is the get_type_method. This method is required whereas
        # the class has some direct or indirect serializable base class. It's required, because
        # the determination of the exact type will fail in the virtual tables tree, if not present in
        # the final class. So we must add or delete it whereas the inheritance change
        serial_bases = [x for x in self.bases if x.is_serial]
        if len(serial_bases) == 0:
            if self._get_type_method is not None:
                self._get_type_method.delete()
                self._get_type_method = None
        elif self._get_type_method is None:
            serial_folder = self._save_method.parent
            type_record = cached_type(self.project, "serial::type_record")
            self._get_type_method = MemberMethod(
                parent=serial_folder, name='get_type',
                type=typeinst(type=type_record, ref=True, const=True),
                virtual=True, constmethod=True,
                content="return serial::runtime_instance<{self.name}>::get_type();".format(self=self),
                note="overload full virtual tables",
                read_only=True)
        # The first step now is to serialize bases first. For doing so,
        # we must prevent double serialization calls because we must be able to
        # call profound serial classes. The mechanism for doing so is to remove
        # classes covered by others. Take in account that some change in the
        # whole hierarchy could break the code, it's better to restart serialization
        # by hand (disable/enable)
        working_copy = copy.copy(serial_bases)
        for x in working_copy:
            local_serial_bases = [y for y in x.bases if y.is_serial]
            for z in local_serial_bases:
                if z in serial_bases:
                    serial_bases.remove(z)

        serializable_members = self(MemberData, filter=lambda x: x.is_serial and x.inner_class == self)
        # now, we need to make distinction between serializable members whose serialization
        # is implemented here and those others that are raw elements. For that, we need to ask
        # if he class serial type
        raw_serializable = [x for x in serializable_members
                            if type(x.type_instance.base_type) is not Class or
                            not x.type_instance.base_type.is_serial]

        inst_serializable = [x for x in serializable_members if x not in raw_serializable]
        ptr_inst_serializable = [x for x in inst_serializable if x.type_instance.is_ptr]
        inst_serializable = [x for x in inst_serializable if x not in ptr_inst_serializable]

        if len(serial_bases) > 0:
            save_content = "//save bases first\n"
            save_data_content = "//save bases first\n"
            save_relations_content = "//save bases first\n"
            save_content += '\n'.join('{}::save(os);'.format(x.scoped) for x in serial_bases)
            save_content += "\n"
            save_data_content += '\n'.join('{}::save_contents(os);'.format(x.scoped) for x in serial_bases)
            save_data_content += "\n"
            save_relations_content += '\n'.join('{}::save_relations(os);'.format(x.scoped) for x in serial_bases)
            save_relations_content += "\n"
        else:
            save_content = ''
            save_data_content = ''
            save_relations_content = ''

        # Now, iterate through the relations
        serializable_relations = self(RelationTo,
                                      filter=lambda x: x.key.to_class.is_serial
                                                       and x.inner_class == self and not x.key.is_static)
        for x in serializable_relations:
            if x.key.is_single:
                save_content += "\n//serialize {to_name} child \n" \
                                "{{\n" \
                                "    auto *ptr = const_cast<{owner}*>(this)->get_{to_name}();\n" \
                                "    size_t {to_name}_count = (ptr == nullptr)? 0: 1;\n" \
                                "    os.write({to_name}_count);\n" \
                                "    if( {to_name}_count == 1 )\n" \
                                "    {{\n" \
                                "        os.save( ptr );\n" \
                                "   }}\n" \
                                "}}\n".format(owner=self.name, to_name=x.key.to_alias)
                # if this relation is an aggregate, there is no need to serialize it inside serialize_relations
                # because if the owner is destroyed, the aggregated will be destroyed first and the relation will
                # be saved from the child
                if not x.key.is_aggregate:
                    save_relations_content += "\n//local serialize {to_name} child \n"\
                                             "{{\n" \
                                             "    auto *ptr = const_cast<{owner}*>(this)->get_{to_name}();\n" \
                                             "    size_t {to_name}_count = (ptr == nullptr)? 0: 1;\n" \
                                             "    os.write({to_name}_count);\n" \
                                             "    if( {to_name}_count == 1 )\n" \
                                             "    {{\n" \
                                             "        os.write( reinterpret_cast<size_t>(ptr) );\n" \
                                             "   }}\n" \
                                             "}}\n".format(owner=self.name, to_name=x.key.to_alias)
            else:
                save_content += "\n//serialize {to_name} child \n" \
                                "{{\n" \
                                "    size_t {to_name}_count = get_{to_name}_count();\n" \
                                "    os.write({to_name}_count);\n" \
                                "    {iterator_name} iter{{this}};\n" \
                                "    while(++iter)\n" \
                                "    {{\n" \
                                "        os.save(iter.get());\n" \
                                "    }}\n" \
                                "}}\n".format(to_name=x.key.to_alias, iterator_name=x(Class)[0].name)
                if not x.key.is_aggregate:
                    save_relations_content += "\n//serialize {to_name} child \n" \
                                    "{{\n" \
                                    "    size_t {to_name}_count = get_{to_name}_count();\n" \
                                    "    os.write({to_name}_count);\n" \
                                    "    {iterator_name} iter{{this}};\n" \
                                    "    while(++iter)\n" \
                                    "    {{\n" \
                                    "        os.write(reinterpret_cast<size_t>(iter.get()));\n" \
                                    "    }}\n" \
                                    "}}\n".format(to_name=x.key.to_alias, iterator_name=x(Class)[0].name)

        save_content += '\n'.join('os.write({});'.format(x.prefixed_name) for x in raw_serializable)
        save_content += '\n'.join('{}.save(os);'.format(x.prefixed_name) for x in inst_serializable)
        save_content += '\n'.join('os.save({});'.format(x.prefixed_name) for x in ptr_inst_serializable)

        save_data_content += '\n'.join('os.write({});'.format(x.prefixed_name) for x in raw_serializable)
        save_data_content += '\n'.join('{}.save(os);'.format(x.prefixed_name) for x in inst_serializable)
        save_data_content += '\n'.join('os.save({});'.format(x.prefixed_name) for x in ptr_inst_serializable)

        self._save_method.content = save_content
        self._save_contents.content = save_data_content
        self._save_relations.content = save_relations_content

        if len(serial_bases) > 0:
            load_content = "//load bases first\n"
            load_data_content = "//load bases first\n"
            load_relations_content = "//load bases first\n"
            load_content += '\n'.join('{}::load(is);'.format(x.scoped) for x in serial_bases)
            load_content += "\n"
            load_data_content += '\n'.join('{}::load_contents(is);'.format(x.scoped) for x in serial_bases)
            load_data_content += "\n"
            load_relations_content += '\n'.join('{}::load_relations(is);'.format(x.scoped) for x in serial_bases)
            load_relations_content += "\n"
        else:
            load_content = ''
            load_data_content = ''
            load_relations_content = ''

        for x in serializable_relations:
            cond = None
            if x.key.implementation != 'standard':
                cond = 'while({to_name}_count--)'.format(to_name=x.key.to_alias)
                add_method_prefix = 'add'
            if x.key.is_single:
                cond = 'if({to_name}_count == 1)'.format(to_name=x.key.to_alias)
                add_method_prefix = 'set'
            if cond is not None:
                load_content += f"""
//serialize {x.key.to_alias} child
{{
    size_t {x.key.to_alias}_count;
    is.read<size_t>({x.key.to_alias}_count);
    {cond}
    {{
        {add_method_prefix}_{x.key.to_alias}(dynamic_cast<{x.key.to_class.name}*>(is.load()));
    }}
}}"""
                load_relations_content += f"""
//serialize {x.key.to_alias} child
{{
    size_t {x.key.to_alias}_count;
    size_t obj_ptr;
    is.read<size_t>({x.key.to_alias}_count);
    {cond}
    {{
        is.read(obj_ptr);
        {add_method_prefix}_{x.key.to_alias}(reinterpret_cast<{x.key.to_class.name}*>(obj_ptr));
    }}
}}"""
            else:
                load_content += "\n//serialize {to_name} child \n" \
                                "{{\n" \
                                "   size_t {to_name}_count;\n" \
                                "   is.read<size_t>({to_name}_count);\n" \
                                "   while({to_name}_count--)\n" \
                                "   {{\n" \
                                "       add_{to_name}_last(dynamic_cast<{toClass}*>(is.load()));\n" \
                                "   }}\n" \
                                "}}\n".format(to_name=x.key.to_alias, toClass=x.key.to_class.name)
                load_relations_content += "\n//serialize {to_name} child \n" \
                                "{{\n" \
                                "   size_t {to_name}_count;\n" \
                                "   size_t obj_ptr;\n" \
                                "   is.read<size_t>({to_name}_count);\n" \
                                "   while({to_name}_count--)\n" \
                                "   {{\n" \
                                "       is.read(obj_ptr);\n" \
                                "       add_{to_name}_last(reinterpret_cast<{toClass}*>(obj_ptr));\n" \
                                "   }}\n" \
                                "}}\n".format(to_name=x.key.to_alias, toClass=x.key.to_class.name)

        load_content += '\n'.join('is.read({0});'.format(x.prefixed_name) for x in raw_serializable)
        load_content += '\n'.join('{}.load(is);'.format(x.prefixed_name) for x in inst_serializable)
        load_content += '\n'.join('{to_name} = dynamic_cast<{toClass}*>(is.load());'.format(
            to_name=x.prefixed_name, toClass=x.type_instance.type.name) for x in ptr_inst_serializable)
        load_data_content += '\n'.join('is.read({0});'.format( x.prefixed_name) for x in raw_serializable)
        load_data_content += '\n'.join('{}.load(is);'.format(x.prefixed_name) for x in inst_serializable)
        load_data_content += '\n'.join('is.read({0});'.format(x.prefixed_name) for x in ptr_inst_serializable)

        self._load_method.content = load_content
        self._load_contents.content = load_data_content
        self._load_relations.content = load_relations_content

    def remove_serialization(self):
        ctor = self.get_serial_constructor()
        if ctor is None:
            return
        # the parent of the ctor is the folder 'serialization' so it's enough to delete it
        ctor.parent.delete()
        for z in self(Inheritance, filter=lambda x: x.ancestor.scoped in [
            "serial::runtime_instance", "trans::transactional_instance"]):
            z.delete()
        self._get_type_method = None
        self._save_method = None
        self._save_contents = None
        self._save_relations = None
        self._load_method = None
        self._load_contents = None
        self._load_relations = None

    def update_class_serialization(self):
        """The mission of this method is to update the serialization
        of the class. First of all, we need to consider that if a class
        is serializable but there are not more serialization at level
        project, it must be removed."""
        if self.project.support_transaction:
            self.remove_serialization()
        if self._serial:
            self.add_serialization()
        else:
            self.remove_serialization()

    @property
    def inner_class(self):
        """Get the inner class container"""
        return self

    @property
    def outer_class(self):
        """Get the outer class container"""
        return (self.parent and self.parent.outer_class) or self

    @property
    def friends(self):
        """returns the list of all friends annotations inside this class"""
        return self(Friendship, filter=lambda x: x.inner_class == self, cut=True)

    @property
    def is_class_methods(self):
        """returns the list of all is_class_methods"""
        return self(IsClassMethod, filter=lambda x: x.inner_class == self, cut=True)

    @property
    def init_methods(self):
        """returns the setup relation method"""
        return self(InitMethod, filter=lambda x: x.inner_class == self, cut=True)

    @property
    def exit_methods(self):
        """returns the clean relations method"""
        return self(ExitMethod,
                    filter=lambda x: x.inner_class == self, cut=True)

    @property
    def nested_classes(self):
        """Returns the list of nested classes (including self)"""
        if type(self.parent) not in [Folder, Class]:
            return [self]
        return self.parent.nested_classes + [self]

    @property
    def nested_types(self):
        """return the visible nested types from here"""
        def is_visible(x):
            return x.parent.inner_class == self or getattr(x, '_access', 'public') == 'public'

        return self(Type, Class, Enum, filter=is_visible, cut=True)

    @property
    def inherited_types(self):
        """return the visible types from base classes"""
        return list(set([t for x in self[Inheritance] if x.access != 'private'
                         for t in x.ancestor.nested_types]))

    @property
    def friend_types(self):
        """return the friend classes types"""
        friends = self.project(Friendship, filter=lambda x: x._target == self)
        return list(set([t for x in friends for t in x.inner_class.nested_types]))

    @property
    def types(self):
        """This method gets the list of visible types"""
        return list(set(self.parent.types) | set(self.friend_types) |
                    set(self.inherited_types) | set(self.nested_types))

    @property
    def template_args(self):
        """Return a string that represents pattern arguments.
        For example, if the template string is 'typename T,class S=x'
        return T,S. The variadic usage is not considered so far"""
        if not self._template:
            return ''
        s = str(self.template_types)[1:-1]  # remove '[' ']'
        s = s.replace("u'", '')
        s = s.replace('u"', '')
        s = s.replace("'", '')
        s = s.replace('"', '')
        return s

    @property
    def template_types(self):
        """Returns the list of nested template types"""
        lt = self._template_types
        nt = super(Class, self).template_types
        lt.extend([x for x in nt if x not in self._template_types])
        return lt

    @property
    def scoped(self):
        """Get the scope"""
        parent_scope = self.parent.scope
        # dependant types required explicit typename keyword
        if '<' in parent_scope:
            parent_scope = 'typename ' + parent_scope
        return '{parent_scope}{self._name}'.format(parent_scope=parent_scope, self=self)

    def scoped_str(self, scope_list, typename_if_required=True):
        """Gets the string representation with minimal relative scope"""
        s = self.scoped
        best_match = 0
        best_scope = None
        bound = s.rfind("::")
        if bound < 0:
            bound = len(s)
        for x in scope_list:
            j = 0
            for j in range(0, min(len(x), bound)):
                if s[j] != x[j]:
                    j = j - 1
                    break
            j = j+1
            if j > best_match:
                if j == len(x) or (j + 2 <= len(x) and x[j:j + 2] == '::'):
                    if j <= len(s) - 2 and s[j:j + 2] == '::':
                        best_match = j
                        best_scope = x[:j]
        if best_scope:
            s = s[best_match:]
            if s[:2] == '::':
                s = s[2:]
        if typename_if_required and '<' in s:
            # if this type is dependant, we need to add a typename spec
            index = s.find('>')
            if index > 0 and index + 1 < len(s):
                if not s.startswith('typename '):
                    return 'typename ' + s
        return s

    @property
    def scope(self):
        """Get the scope"""
        # TODO : remove patch
        if not hasattr(self, '_template_arguments'):
            self._template_arguments = self._template_types
        if self._template:
            tt = ','.join(self._template_arguments)
            return '{self.parent.scope}{self._name}<{tt}>::'.format(self=self, tt=tt)
        parent_scope = self.parent.scope
        return '{parent_scope}{self._name}::'.format(parent_scope=parent_scope, self=self)

    @property
    def reference(self):
        """Gets the forward reference"""
        if self._template:
            return 'template<{self.clean_template}> {self.kind} {self.scoped}'.format(self=self)
        return '{self.kind} {self.scoped}'.format(self=self)

    @property
    def unscoped_reference(self):
        if self._template:
            return 'template<{self._template}> {self.kind} {self._name}'.format(self=self)
        if self._export_prefix:
            return '{self.kind} {self.export_prefix} {self._name}'.format(self=self)
        else:
            return '{self.kind} {self._name}'.format(self=self)

    @property
    def relations_from(self):
        """Gets the relations from"""
        cls_member = lambda x: x.inner_class == self
        return self(RelationFrom, filter=cls_member, cut=True)

    @property
    def relations_to(self):
        """Gets the relations from"""
        cls_member = lambda x: x.inner_class == self
        return self(RelationTo, filter=cls_member, cut=True)

    @property
    def inheritance(self):
        """Get the inheritance list"""
        cls_member = lambda x: x.inner_class == self
        return self(Inheritance, filter=cls_member, cut=True)

    @upgrade_version
    def __setstate__(self, data_dict):
        """While change between versions we need to maintain
        coherence and create missing elements when loading"""
        return {
            'add': {
                '_export_prefix': '',
                '_save_contents': None,
                '_load_contents': None,
                '_save_relations': None,
                '_load_relations': None,
                '_load_method': None,
                '_save_method': None,
                '_get_type_method': None,
                '_child_index': -1,
                '_transactional': False
            }
        }
