# -*- coding: utf-8 -*-
import uuid
from operator import attrgetter
from beatle.lib.api import context
from beatle.lib.tran import TransactionStack
from beatle.lib.decorators import upgrade_version
from beatle.lib.utils import isclass

#import tasks

"""
Created on Sun Dec 15 23:49:51 2013

@author: mel
"""


class TCommon(object):
    """Component class: This class is the base class for datamodel atoms."""
    task_container = False
    project_container = False
    class_container = False
    context_container = False
    folder_container = False
    diagram_container = False
    module_container = False
    namespace_container = False
    type_container = False
    inheritance_container = False
    member_container = False
    argument_container = False
    friendship_container = False
    relation_container = False
    function_container = False
    variable_container = False
    enum_container = False
    import_container = False
    package_container = False
    library_container = False
    repository_container = False
    dir_container = False
    file_container = False
    # database
    schema_container = False
    table_container = False
    field_container = False

    # visual methods
    def draggable(self):
        """returns info about if the object can be moved"""
        return not self._read_only

    def drop(self, to):
        """drop this elemento to another place"""
        return False

    def __init__(self, **kwargs):
        """Initialize a transactional component"""
        self._parent = kwargs.get('parent', None)
        self._name = kwargs.get('name', '')
        self._serial = kwargs.get('serial', False)
        self._note = kwargs.get('note', '')
        self._read_only = kwargs.get('read_only', False)
        self._visibleInTree = kwargs.get('visibleInTree', True)
        self._declare = kwargs.get('declare', True)
        self._implement = kwargs.get('implement', True)
        self._child = {}
        if self._parent is not None:
            self._parent.add_child(self, kwargs.get('prev', None))
        self._uid = uuid.uuid4()
        self._child_index = kwargs.get('child_index', -1)
        super(TCommon, self).__init__(**kwargs)
        # This is not really required because this is updated by the view
        if self._child_index == -1 and isinstance(self.parent, TCommon):
            self._child_index = self.parent.last_child_index+1
        # allow to dynamic children creation through kwargs
        child_kwargs = kwargs.get('child_kwargs', [])
        if len(child_kwargs) > 0:
            for t in child_kwargs:
                for chkwargs in child_kwargs[t]:
                    chkwargs['parent'] = self
                    t(**chkwargs)

    def exchange_index(self, obj):
        """Call it as self.exchange_index(other)"""
        other_index = obj.child_index
        obj.child_index = self._child_index
        self._child_index = other_index
        if hasattr(obj, "parent"):
            try:
                obj.parent.save_state()
            except:
                pass
            if hasattr(obj.parent, '_child'):
                if type(obj) in obj.parent._child.keys():
                    obj.parent._child[type(obj)].sort(key=attrgetter('_child_index'))
                if type(self) == type(obj):
                    return
                if type(self) in obj.parent._child.keys():
                    obj.parent._child[type(self)].sort(key=attrgetter('_child_index'))

    @property
    def child_index(self):
        return self._child_index

    @child_index.setter
    def child_index(self, value):
        self._child_index = value

    @property
    def last_child_index(self):
        a = self.sorted_child
        if len(a) == 0:
            return -1
        return a[-1].child_index

    @property
    def uid(self):
        """Accessor to the uid of the object"""
        return self._uid

    @property
    def is_serial(self):
        return self._serial

    def set_serial(self, value):
        self._serial = value

    @property
    def children(self):
        """Accessor to the children map"""
        return self._child

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        return {
            'parent': self._parent,
            'name': self._name,
            'serial': self._serial,
            'note':  self._note,
            'read_only': self._read_only,
            'visibleInTree': self._visibleInTree,
            'declare': self._declare,
            'implement': self._implement,
            'child_kwargs': dict(
                [(t, [x.get_kwargs() for x in self._child[t]]) for t in self._child.keys()])
        }

    def update_container(self):
        """Update the container info"""
        pass

    def write_contexts_prefix_declaration(self, file_writer):
        """Write the contexts prefixes for declaration"""
        if hasattr(self, '_contexts'):
            for i in range(0, len(self._contexts)):
                item = self._contexts[i]
                if len(item._prefix_declaration):
                    file_writer.write_line('{0}'.format(item._prefix_declaration))

    def write_contexts_suffix_declaration(self, file_writer):
        """Write the contexts suffix for declaration"""
        if hasattr(self, '_contexts'):
            for i in range(len(self._contexts) - 1, -1, -1):
                item = self._contexts[i]
                if len(item._suffix_declaration):
                    file_writer.write_line('{0}'.format(item._suffix_declaration))

    def write_contexts_prefix_implementation(self, file_writer):
        """Write the contexts prefix for implementation"""
        if hasattr(self, '_contexts'):
            for i in range(0, len(self._contexts)):
                item = self._contexts[i]
                if len(item._prefix_implementation):
                    file_writer.write_line('{0}'.format(item._prefix_implementation))

    def write_contexts_suffix_implementation(self, file_writer):
        """Write the contexts suffix for implementation"""
        if hasattr(self, '_contexts'):
            for i in range(len(self._contexts) - 1, -1, -1):
                item = self._contexts[i]
                if len(item._suffix_implementation):
                    file_writer.write_line('{0}'.format(item._suffix_implementation))

    def __call__(self, *args, **kwargs):
        """___call__(type1,...,typeN, filter=lambda x: True, cut=False):
        Returns the list of nested childs of one of the types.

        filter: indicates a boolean filter method that must
                return True for valid elements.

        cut: stops traveling if filter is unsatisfied
        """
        r = []
        fn = kwargs.get('filter', lambda x: True)
        cut = kwargs.get('cut', False)
        for k in self._child:
            seed = self[k]
            approved = []
            if len(args) == 0 or isclass(k, *args):
                approved = list(filter(fn, seed))
            if cut:
                seed = list(filter(fn, seed))
            r.extend(approved)
            for o in seed:
                r.extend(o(*args, **kwargs))
        return r

    @property
    def name(self):
        """Get the name"""
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def note(self):
        """Get the name"""
        return self._note

    @note.setter
    def note(self, note):
        """Set the note"""
        note.replace('\n','')
        self._note = ''.join([i if ord(i)  < 128  else ' ' for i in note])

    @property
    def label(self):
        """Get tree label"""
        return self._name

    @property
    def tree_label(self):
        """Get tree label"""
        return self.label

    @property
    def tab_label(self):
        """Get tree label"""
        return self.label

    # container properties
    @property
    def inner_class_container(self):
        """return the inner class container if any"""
        return (self.class_container and self) or (self.parent and self.parent.inner_class_container)

    @property
    def inner_context_container(self):
        """return the inner context container if any"""
        return (self.context_container and self) or (self.parent and self.parent.inner_context_container)

    @property
    def inner_folder_container(self):
        """return the inner folder container if any"""
        return (self.folder_container and self) or (self.parent and self.parent.inner_folder_container)

    @property
    def inner_repository_container(self):
        """return the inner folder container if any"""
        return (self.repository_container and self) or (self.parent and self.parent.inner_repository_container)

    @property
    def inner_diagram_container(self):
        """return the inner diagram container if any"""
        return (self.diagram_container and self) or (self.parent and self.parent.inner_diagram_container)

    @property
    def inner_module_container(self):
        """return the inner module container if any"""
        return (self.module_container and self) or (self.parent and self.parent.inner_module_container)

    @property
    def inner_task_container(self):
        """return the inner task container if any"""
        return (self.task_container and self) or (self.parent and self.parent.inner_task_container)

    @property
    def inner_type_container(self):
        """return the inner module container if any"""
        return (self.type_container and self) or (self.parent and self.parent.inner_type_container)

    @property
    def inner_enum_container(self):
        """return the inner module container if any"""
        return (self.enum_container and self) or (self.parent and self.parent.inner_enum_container)

    @property
    def inner_namespace_container(self):
        """return the inner namespace container if any"""
        return (self.namespace_container and self) or (self.parent and self.parent.inner_namespace_container)

    @property
    def inner_schema_container(self):
        """return the inner schema container if any"""
        return (self.schema_container and self) or (self.parent and self.parent.inner_schema_container)

    @property
    def inner_table_container(self):
        """return the inner schema container if any"""
        return (self.table_container and self) or (self.parent and self.parent.inner_table_container)

    @property
    def inner_field_container(self):
        """return the inner schema container if any"""
        return (self.field_container and self) or (self.parent and self.parent.inner_field_container)

    @property
    def inner_inheritance_container(self):
        """return the inner ineritance container if any"""
        return (self.inheritance_container and self) or (self.parent and self.parent.inner_inheritance_container)

    @property
    def inner_member_container(self):
        """return the inner member container"""
        return (self.member_container and self) or (self.parent and self.parent.inner_member_container)

    @property
    def inner_argument_container(self):
        """return the inner argument container"""
        return (self.argument_container and self) or (self.parent and self.parent.inner_argument_container)

    @property
    def inner_package_container(self):
        """return the inner package container"""
        return (self.package_container and self) or (self.parent and self.parent.inner_package_container)

    @property
    def inner_project_container(self):
        """return the inner project container"""
        return (self.project_container and self) or (
            self.parent and self.parent.inner_project_container)

    @property
    def inner_relation_container(self):
        """return the inner relation container"""
        return (self.relation_container and self) or (
            self.parent and self.parent.inner_relation_container)

    @property
    def inner_function_container(self):
        """return the inner function container"""
        return (self.function_container and self) or (self.parent and self.parent.inner_function_container)

    @property
    def inner_variable_container(self):
        """return the inner relation container"""
        return (self.variable_container and self) or (self.parent and self.parent.inner_variable_container)

    @property
    def inner_import_container(self):
        """return the inner relation container"""
        return (self.import_container and self) or (self.parent and self.parent.inner_import_container)

    def inner(self, t, self_include=True):
        """Search through ancestors the first ancestor of some type"""
        if self_include:
            if (type(t) is list and isclass(type(self), *t)) or isclass(type(self), t):
                return self
        return self._parent and self._parent.inner(t)

    def inner_true(self, condition=lambda x: False):
        """Search through ancestor the first satisfying condition"""
        return (condition(self) and self) or (self._parent and self._parent.inner_true(condition)) or None

    def inner_false(self, condition=lambda x: False):
        """Search through ancestor the first non-satisfying condition"""
        return (condition(self) is False and self) or (self._parent and self._parent.inner_false(condition)) or None

    @property
    def can_delete(self):
        """Check about if a component can be deleted"""
        return not self._read_only

    @property
    def read_only(self):
        """get read only status"""
        return self._read_only

    @read_only.setter
    def read_only(self, value):
        self._read_only = value

    def __setitem__(self, cls, listobj):
        """sets the list of some class childs"""
        assert type(listobj) is list
        self._child[cls] = listobj

    def __getitem__(self, cls):
        """returns the list of childs with some class"""
        if cls not in self._child:
            self._child[cls] = []
        return self._child[cls]

    @property
    def project(self):
        """return the container project"""
        from beatle.model import Project
        return self.inner(Project)

    @property
    def workspace(self):
        """return the container project"""
        from beatle.model import Workspace
        return self.inner(Workspace)

    @property
    def inner_types_folder(self):
        """return the container types folder"""
        from beatle.model.cc import TypesFolder
        return self.inner(TypesFolder)

    @property
    def classes(self):
        """returns the list of all classes"""
        from beatle.model.cc import Class
        return self(Class)

    @property
    def diagrams(self):
        """returns the list of all diagrams"""
        from beatle.model.cc._ClassDiagram import ClassDiagram
        return self(ClassDiagram)

    @property
    def folders(self):
        """returns the list of all folder"""
        from beatle.model._Folder import Folder
        return self(Folder)


    @property
    def packages(self):
        """returns the list of all packages"""
        from beatle.model.py import Package
        return self(Package)

    @property
    def functions(self):
        """returns the list of all functions"""
        from beatle.model.cc import Function
        return self(Function)

    @property
    def constructors(self):
        """returns the list of all functions"""
        from beatle.model.cc import Constructor
        return self(Constructor)

    @property
    def destructors(self):
        """returns the list of all functions"""
        from beatle.model.cc import Destructor
        return self(Destructor)

    @property
    def methods(self):
        """returns the list of all functions"""
        from beatle.model import cc
        from beatle.model import py
        return self(cc.MemberMethod,py.MemberMethod)

    @property
    def variables(self):
        """returns the list of all variables"""
        from beatle.model.cc import Data
        return self(Data, True)

    @property
    def inner_class(self):
        """returns the innermost class"""
        return self.parent and self.parent.inner_class

    @property
    def inner_namespace(self):
        """returns the innermost class"""
        from beatle.model.cc import Namespace
        return self.inner(Namespace)

    @property
    def inner_folder(self):
        """returns the innermost folder"""
        from beatle.model._Folder import Folder
        return self.inner(Folder)

    @property
    def inner_module(self):
        """returns the innermost class"""
        from beatle.model.cc import Module as ccModule
        from beatle.model.py import Module as pyModule
        return self.inner([ccModule, pyModule])

    @property
    def inner_package(self):
        """returns the innermost class"""
        from beatle.model.py import Package
        return self.inner(Package)

    @property
    def inner_method(self):
        """returns the innermost method"""
        from beatle.model.cc import IsClassMethod
        from beatle.model.cc import MemberMethod
        return self.inner([MemberMethod, IsClassMethod])

    @property
    def inner_function(self):
        """returns the innermost function"""
        from beatle.model.cc import Function
        return self.inner(Function)

    @property
    def inner_constructor(self):
        """returns the innermost constructor"""
        from beatle.model.cc import Constructor
        return self.inner(Constructor)

    @property
    def inner_schema(self):
        """returns the innermost schemar"""
        from beatle.model.database import Schema
        return self.inner(Schema)

    @property
    def inner_table(self):
        """return the innermost table"""
        from beatle.model.database import Table
        return self.inner(Table)

    @property
    def outer_class(self):
        """returns the outermost class"""
        return self.parent and self.parent.outer_class

    @property
    def outer_module(self):
        """returns the outermost module"""
        from beatle.model.cc import Module as ccModule
        from beatle.model.py import Module as pyModule
        return (self.parent and self.parent.outer_module) or ((
            isinstance(self, (ccModule, pyModule)) or None) and self)

    @property
    def outer_namespace(self):
        """returns the outermost namespace"""
        from beatle.model.cc import Namespace
        return (self.parent and self.parent.outer_namespace) or ((
            isinstance(self, Namespace) or None) and self)

    @property
    def outer_folder(self):
        """returns the outermost folder"""
        from beatle.model._Folder import Folder
        return (self.parent and self.parent.outer_folder) or ((
            isinstance(self, Folder) or None) and self)

    @property
    def outer_schema(self):
        """returns the outermost schema"""
        from beatle.model.database import Schema
        return (self.parent and self.parent.outer_schema) or ((
            isinstance(self, Schema) or None) and self)


    @property
    def top_namespaces(self):
        """return the list of top namespaces"""
        return [x for x in self.namespaces if x.outer_namespace == x]

    @property
    def top_folders(self):
        """return the list of top folders"""
        return [x for x in self.folders if x.outer_folder == x]

    @property
    def parent(self):
        """return the parent object"""
        return self._parent

    @property
    def path(self):
        """returns the ordered parents list"""
        return ((self.parent and self.parent.path) or []) + [self]

    @property
    def scope(self):
        """Get the scope"""
        return (self._parent and self._parent.scope) or ''

    @property
    def scoped(self):
        """returns the absolute name, with scope"""
        return '{self.scope}{self._name}'.format(self=self)

    @property
    def template_types(self):
        """Returns the list of nested template types"""
        return (self.parent and self.parent.template_types) or []

    @property
    def visible_in_tree(self):
        return self._visibleInTree

    @visible_in_tree.setter
    def visible_in_tree(self, value):
        self._visibleInTree = value

    def find_child(self, **kwargs):
        """Checks the existence of some child"""
        recurse = kwargs.get('recurse', True)
        obj = kwargs.get('obj', None)
        if obj is not None:
            t = type(obj)
            if not recurse:
                return obj in self[t]
        else:
            t = kwargs.get('type', None)
            name = kwargs.get('name', None)
            if not recurse:
                return name in [x._name for x in self[t]]
        for c, l in self._child.items():
            if hasattr(c, 'find_child'):
                for x in l:
                    if x.find_child(**kwargs):
                        return True
        return False

    def __getstate__(self):
        """Set picke context"""
        state = dict(self.__dict__)
        if '_parentIndex' in state:
            del state['_parentIndex']
        if '_pane' in state:
            del state['_pane']
        if '_page_index' in state:
            del state['_page_index']
        return state

    @property
    def sorted_child(self):
        """Return the list of children sorted by index.
        Foreign child (like autogenerated classes and members) are omitted."""
        return sorted([x for y in self.children for x in self.children[y] if hasattr(x, 'child_index')],
                      key=lambda x: x.child_index)

    def delete(self):
        """Remove all the elements"""
        self.delete_children()
        super(TCommon, self).delete()

    def delete_children(self):
        """Remove all the child elements"""
        items = self._child.keys()
        for cl in items:
            rm = [x for x in self._child[cl]]
            for x in rm:
                x.delete()

    def on_undo_redo_changed(self):
        """Update from app"""
        super(TCommon, self).on_undo_redo_changed()
        self.update_container()
        context.render_undo_redo_changed(self)

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        super(TCommon, self).on_undo_redo_removing()
        children = [item for group in self._child.values() for item in group]
        for child in children:
            child.on_undo_redo_removing()
        context.render_undo_redo_removing(self)

        if not (TransactionStack.in_undo_redo() or TransactionStack.InTransaction()):
            # With careful!! What are us doing using transactional semantics without transactions?
            # I'm unsure about this ... I need to delete undo stack?
            self.remove_relations()

    def remove_relations(self):
        """Utility for undo/redo"""
        if self._parent is not None:
            self._parent.remove_child(self)
            context.render_undo_redo_changed(self._parent)

    def restore_relations(self):
        """Utility for undo/redo"""
        if self._parent is not None:
            self._parent.add_child(self)
            context.render_undo_redo_changed(self._parent)

    def on_undo_redo_add(self):
        """Restore object from undo"""
        if not (self.in_undo_redo() or self.in_transaction()):
            # With careful!! What we are doing using transactional semantics without a transaction?
            # I'm unsure about this ... I need to delete undo stack?
            self.restore_relations()
        context.render_undo_redo_add(self)
        super(TCommon, self).on_undo_redo_add()
        children = [item for group in self._child.values() for item in group]
        children.reverse()
        for child in children:
            child.on_undo_redo_add()

    def on_undo_redo_loaded(self):
        """Restore object after loading"""
        context.render_undo_redo_loaded(self)
        rlc = list(reversed(list(self._child.keys())))
        for c in rlc:
            rlo = list(reversed(self._child[c]))
            for o in rlo:
                o.on_undo_redo_loaded()

    def on_undo_redo_unloaded(self):
        """Destroy object before unloading"""
        rlc = list(reversed(list(self._child.keys())))
        for c in rlc:
            rlo = list(reversed(self._child[c]))
            for o in rlo:
                o.on_undo_redo_unloaded()
        context.render_undo_redo_unloaded(self)

    def index(self, obj):
        """Get the index of an child object inside his group"""
        t = type(obj)
        return ((t in self._child and
                 obj in self._child[t] and
                 self._child[t].index(obj) + 1) or 0) - 1

    def add_child(self, obj, prev=None):
        """Adds a child member to dictionary"""
        cls = type(obj)
        if cls not in self._child:
            self._child[cls] = [obj]
            return
        index = (prev and self.index(prev) + 1) or getattr(obj, '_parentIndex', len(self._child[cls]))
        if index > 0:
            index = min(index, len(self._child[cls]))
        elif index < 0:
            index = len(self._child[cls])
        if obj not in self._child[cls]:
            self._child[cls].insert(index, obj)
        else:
            old_index = self._child[cls].index(obj)
            if index != old_index:
                self._child[cls].pop(old_index)
                self._child[cls].insert(index, obj)

    def remove_child(self, obj):
        """Remove a child member from dictionary"""
        cls = type(obj)
        index = self.index(obj)
        if index >= 0:
            assert obj == self._child[cls].pop(index)
            obj._parentIndex = index

    @upgrade_version
    def __setstate__(self, data_dict):
        return {
            'delete': ['_support_serialization', '_support_undo_redo', '_transactional'],
            'add': {'_read_only': False,  '_child_index': -1},
            'rename': {'_readOnly': '_read_only'},
        }


