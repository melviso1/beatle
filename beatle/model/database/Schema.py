# -*- coding: utf-8 -*-

"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""

from beatle.model import TComponent
from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject
from .Table import Table


class Schema(TComponent):
    """Implements c++ class representation"""
    folder_container = True
    diagram_container = True
    table_container = True

    # visual methods
    @TransactionalMethod('move schema {0}')
    def drop(self, to):
        """Drops schema inside project or another folder """
        return False
        target = to.inner_schema_container
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initialization method"""
        self._memberPrefix = kwargs.get('prefix', '')
        super(Schema, self).__init__(**kwargs)

    def CreateTables(self, conn):
        """Create tables for the schema from database"""
        try:
            conn.query('SHOW TABLES FROM `{0}`'.format(self.name))
            data = conn.store_result()
            if data:
                kwargs = {'parent': self}
                for i in range(0, data.num_rows()):
                    kwargs['name'] = data.fetch_row()[0][0]
                    Table(**kwargs)
                del data
                for table in self[Table]:
                    table.CreateFields(conn)
            return True
        except:
            return False

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {}
        kwargs['prefix'] = self._memberPrefix
        kwargs.update(super(Schema, self).get_kwargs())
        return kwargs

    def on_undo_redo_removing(self):
        """Handle on_undo_redo_removing, prevent generating files"""
        super(Schema, self).on_undo_redo_removing()

    @property
    def can_delete(self):
        """Check abot if class can be deleted"""
        return super(Schema, self).can_delete

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('database_schema')

    @property
    def tree_label(self):
        """Get tree label"""
        return self.name

    @property
    def inner_schema(self):
        """Get the inner schema"""
        return self

    @property
    def outer_schema(self):
        """Get the outer class container"""
        return self

