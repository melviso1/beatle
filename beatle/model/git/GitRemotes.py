# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-

"""Git remote representation"""

from beatle import model
from beatle.app import resources as rc
from .GitRemote import GitRemote


class GitRemotes(model.TComponent):
    """Implements git remotes folder"""

    def __init__(self, **kwargs):
        """Initialization method"""
        self._lastHdrTime = None
        self._lastSrcTime = None
        if 'name' not in kwargs:
            kwargs['name'] = 'remotes'
        self._parent = kwargs['parent']
        super(GitRemotes, self).__init__(**kwargs)
        self.load_remotes()

    def load_remotes(self):
        """Update remotes"""
        kwargs = {'parent': self}
        for remote in self.repo._repo.remotes:
            kwargs['name'] = remote.name
            GitRemote(**kwargs)

    def update_status(self):
        """update the remote status"""
        old_remotes = dict([(x.name, x) for x in self(GitRemote)])
        new_remotes = dict([(x.name, x) for x in self.repo._repo.remotes])
        # remove missing remotes
        todel = [old_remotes[x] for x in old_remotes if x not in new_remotes]
        tonew = [new_remotes[x] for x in new_remotes if x not in old_remotes]
        for x in todel:
            x.delete()
        kwargs = {'parent': self}
        for x in tonew:
            kwargs['name'] = x.name
            GitRemote(**kwargs)

    @property
    def repo(self):
        """get the local repo object"""
        return self.parent.repo

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {'data': self._data}
        kwargs.update(super(GitRemotes, self).get_kwargs())
        return kwargs

    @property
    def can_delete(self):
        """Check abot if class can be deleted"""
        return super(GitRemotes, self).can_delete

    def delete(self):
        """Delete diagram objects"""
        super(GitRemotes, self).delete()

    def remove_relations(self):
        """Utility for undo/redo"""
        super(GitRemotes, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(GitRemotes, self).restore_relations()

    def save_state(self):
        """Utility for saving state"""
        super(GitRemotes, self).save_state()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(GitRemotes, self).on_undo_redo_removing()

    def on_undo_redo_changed(self):
        """Update from app"""
        # when the class is updated, ctor's and dtors, must be updated
        super(GitRemotes, self).on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(GitRemotes, self).on_undo_redo_add()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        return rc.get_bitmap_index('git_repo')

    @property
    def label(self):
        """Get tree label"""
        return '{self._name}'.format(self=self)



