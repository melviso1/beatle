# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
import os.path

import wx

from beatle import model
from beatle.app import resources as rc
from .GitFile import GitFile


class GitDir(model.TComponent):
    """Implements git dir representation"""
    folder_container = True

    def __init__(self, **kwargs):
        """Initialization method"""
        self._lastHdrTime = None
        self._lastSrcTime = None
        self._name = kwargs['name']
        self._parent = kwargs['parent']
        super(GitDir, self).__init__(**kwargs)
        # wx.YieldIfNeeded()
        self.load_dir(kwargs.get('git_context', None))

    def load_dir(self, git_context=None):
        """Recursively descend and analyse existing files
        for representing it even if they are not under git control"""
        hdir = os.path.realpath(self.path)
        if not os.access(hdir, os.R_OK):
            return False
        entries = os.listdir(hdir)
        entries.sort()
        for elem in entries:
            if elem[0] == '.':
                continue
            path = os.path.join(hdir, elem)
            if os.path.isdir(path):
                kwargs = {'name': elem, 'parent': self, 'git_context':git_context}
                GitDir(**kwargs)
            if os.path.isfile(path):
                kwargs = {'name': elem, 'parent': self, 'git_context':git_context}
                GitFile(**kwargs)
            else:
                continue
        return True

    def update_status(self, git_context=None):
        """update the directory status"""
        # wx.YieldIfNeeded()
        hdir = os.path.realpath(self.path)
        if not os.access(hdir, os.R_OK):
            return
        dirs = []
        files = []
        entries = os.listdir(hdir)
        entries.sort()
        for elem in entries:
            path = os.path.join(hdir, elem)
            if elem[0] == '.':
                continue
            if os.path.isdir(path):
                dirs.append(elem)
            if os.path.isfile(path):
                files.append(elem)
            else:
                continue
        # current
        curdirs = self[GitDir]
        curfiles = self[GitFile]
        # new
        newdirs = [x for x in dirs if x not in [y.name for y in curdirs]]
        newfiles = [x for x in files if x not in [y.name for y in curfiles]]
        # delete dirs and files
        deldir = [x for x in curdirs if x.name not in dirs]
        delfile = [x for x in curfiles if x.name not in files]
        for x in deldir:
            x.delete()
        for x in delfile:
            x.delete()
        # update existing
        if git_context is None:
            from .GitRepo import GitContext
            git_context = GitContext(self.repo._repo)
        for d in self[GitDir]:
            d.update_status(git_context)
        for f in self[GitFile]:
            f.update_status(git_context)
            f.on_undo_redo_changed()
        # new files and dirs
        for elem in newdirs:
            GitDir(name=elem, parent=self, git_context=git_context)
        for elem in newfiles:
            GitFile(name=elem, parent=self, git_context=git_context)
    @property
    def repo(self):
        """get the report object"""
        return self.parent.repo

    @property
    def path(self):
        """Access for this element"""
        return os.path.join(self.parent.path, self._name)

    @property
    def rpath(self):
        """Access for this element"""
        return os.path.join(self.parent.rpath, self._name)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {'data': self._data}
        kwargs.update(super(GitDir, self).get_kwargs())
        return kwargs

    @property
    def can_delete(self):
        """Check abot if class can be deleted"""
        return super(GitDir, self).can_delete

    def delete(self):
        """Delete diagram objects"""
        super(GitDir, self).delete()

    def remove_relations(self):
        """Utility for undo/redo"""
        super(GitDir, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(GitDir, self).restore_relations()

    def save_state(self):
        """Utility for saving state"""
        super(GitDir, self).save_state()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(GitDir, self).on_undo_redo_removing()

    def on_undo_redo_changed(self):
        """Update from app"""
        # when the class is updated, ctor's and dtors, must be updated
        super(GitDir, self).on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(GitDir, self).on_undo_redo_add()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        return rc.get_bitmap_index('git_repo')

    @property
    def label(self):
        """Get tree label"""
        return '{self._name}'.format(self=self)



