# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
import copy
import os.path

import git, wx

from beatle.model import TComponent
from beatle.lib.api import context
from beatle.app.ui.wnd import WorkingWindow

from .GitRemotes import GitRemotes
from .GitDir import GitDir
from .GitFile import GitFile

def tracked_files(repo,trees=None):
    """Get tracked files.
    Based on http://eternuement.blogspot.com/2016/05/get-list-of-tracked-files-of-git-by.html"""
    if trees is None:
        trees = [repo.head.commit.tree]
    paths = []
    for tree in trees:
        paths.extend([blob.path for blob in tree.blobs ])
        if tree.trees is not None:
            paths.extend(tracked_files(repo,tree.trees))
    return paths


class GitContext(object):
    """Provides current context for fast operation.
    Some Git operations, like importing a new git repo,
    require checking a bunch of files, while the git
    status is not changing. Even these checks may be
    acceptable for individual checks, massive
    operations are a bottle neck. For this very reason,
    a GitContext is absolutely necessary."""

    def __init__(self, repo):
        """Initialize context
        :param: repo - git repository
        """
        self._file_status = {}
        self._file_status.update((f, 'git_file') for f in tracked_files(repo))

        modified = repo.index.diff(None)
        deleted_files = set()
        modified_files = set()
        stagged_files = set()
        for f in modified:
            if f.b_mode or f.b_blob or f.b_path:
                modified_files.add(f.a_path)
                modified_files.add(f.b_path)
            else:
                deleted_files.add(f.a_path)
                deleted_files.add(f.b_path)
        staged = repo.index.diff('HEAD')
        for f in staged:
            if f.b_mode or f.b_blob or f.b_path:
                stagged_files.add(f.a_path)
                stagged_files.add(f.b_path)
            else:
                deleted_files.add(f.a_path)
                deleted_files.add(f.b_path)
        self._file_status.update((f,'git_file_staged') for f in stagged_files)
        self._file_status.update((f,'git_file_modified') for f in modified_files)
        self._file_status.update((f,'git_file_deleted') for f in deleted_files)
        super(GitContext,self).__init__()

    def status(self, path):
        if path in self._file_status:
            return self._file_status[path]
        return 'file'


class GitRepo(TComponent):
    """Implements git repo representation"""
    folder_container = True

    def __init__(self, **kwargs):
        """Initialization method"""
        from beatle.app.ui import dlg
        self._lastHdrTime = None
        self._lastSrcTime = None
        self._local = kwargs.get('local', True)
        self._uri = kwargs.get('uri', None)
        if not kwargs.get('name', None):
            s = self._uri
            s, t = os.path.split(s)
            if len(t) == 0:
                s, t = os.path.split(s)
            kwargs['name'] = t
        super(GitRepo, self).__init__(**kwargs)
        # The first step is to iterate over the git repository
        # structure to fill it
        working = WorkingWindow(context.get_frame())
        working.Show(True)
        # wx.YieldIfNeeded()
        self._repo = git.Repo(self._uri)
        GitRemotes(parent=self)
        self.load_dir()
        working.Close()

    @property
    def stage_size(self):
        """return the number of elements in stage"""
        return len(self._repo.index.entries)

    def commit(self, message):
        """do a commit"""
        self._repo.index.commit(message)
        self._repo.index.write()
        self.update_status()

    def add(self, element):
        """add element to repository"""
        self._repo.index.add([element.rpath])
        self._repo.index.write()
        self.update_status()

    def supr(self, element):
        """remove element from repository"""
        self._repo.index.remove([element.rpath],working_tree = False)
        self._repo.index.write()
        self.update_status()

    @property
    def repo(self):
        """return the model"""
        return self

    def load_dir(self):
        """Load the head commit and add each entry to repository view"""
        hdir = os.path.realpath(self.path)
        if not os.access(hdir, os.R_OK):
            return False
        kwargs = {'parent': self}
        contents = os.listdir(hdir)
        contents.sort()
        git_context = GitContext(self._repo)  # speed-up
        for elem in contents:
            if elem[0] == '.':
                continue
            path = os.path.join(hdir, elem)
            if os.path.isdir(path):
                kwargs = {'name': elem, 'parent': self, 'git_context':git_context}
                GitDir(**kwargs)
            if os.path.isfile(path):
                kwargs = {'name': elem, 'parent': self, 'git_context':git_context}
                GitFile(**kwargs)
            else:
                continue

    def status(self, path, file_context=None):
        """return the status of the file"""
        if file_context is not None:
            return file_context.status(path)
        status = 'file'
        # check first if the file is under control
        if path in tracked_files(self._repo):
            status = 'git_file'
            modified = self._repo.index.diff(None)
            for f in modified:
                if path not in [f.a_path, f.b_path]:
                    continue
                #ok, the file is modified
                if not f.b_mode and not f.b_blob and not f.b_path:
                    return 'git_file_deleted'
                else:
                    status = 'git_file_modified'
                    break
            staged = self._repo.index.diff('HEAD')
            for f in staged:
                if path not in [f.a_path, f.b_path]:
                    continue
                #ok, the file is modified
                if not f.b_mode and not f.b_blob and not f.b_path:
                    return 'git_file_deleted'
                else:
                    status = 'git_file_staged'
                    break
        return status

    @property
    def path(self):
        """Access for this element"""
        return self._uri

    @property
    def rpath(self):
        """Access for this element"""
        return ''

    def __getstate__(self):
        """Set pickle context"""
        # The repo object has not pickle support ...
        state = copy.copy(self.__dict__)
        del state['_repo']
        return state

    def __setstate__(self, d):
        """Load pickle context"""
        # The repo object has not pickle support ...
        self.__dict__ = d
        try:
            self._repo = git.Repo(self._uri)
        except git.NoSuchPathError as e:
            self._repo = None
        except git.InvalidGitRepositoryError as e:
            self._repo = None

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {
            'local': self._local,
            'uri': self._uri}
        kwargs.update(super(GitRepo, self).get_kwargs())
        return kwargs

    def update_status(self):
        """update the repository status"""
        # wx.YieldIfNeeded()
        git_context = GitContext(self._repo)
        hdir = os.path.realpath(self.path)
        if not os.access(hdir, os.R_OK):
            return
        dirs = []
        files = []
        entries = os.listdir(hdir)
        entries.sort()
        for elem in entries:
            path = os.path.join(hdir, elem)
            if elem[0] == '.':
                continue
            if os.path.isdir(path):
                dirs.append(elem)
            if os.path.isfile(path):
                files.append(elem)
            else:
                continue
        #current
        curdirs = self[GitDir]
        curfiles = self[GitFile]
        #new
        newdirs = [x for x in dirs if x not in [y.name for y in curdirs]]
        newfiles = [x for x in files if x not in [y.name for y in curfiles]]
        # delete dirs and files
        deldir = [x for x in curdirs if x.name not in dirs]
        delfile = [x for x in curfiles if x.name not in files]
        for x in deldir:
            x.delete()
        for x in delfile:
            x.delete()
        # update existing
        for d in self[GitDir]:
            d.update_status(git_context)
        for f in self[GitFile]:
            f.update_status(git_context)
            f.on_undo_redo_changed()
        # new files and dirs
        for elem in newdirs:
            GitDir(name=elem, parent=self, git_context=git_context)
        for elem in newfiles:
            GitFile(name=elem, parent=self, git_context=git_context)
        # update remote status
        for element in self[GitRemotes]:
            element.update_status()

    @property
    def can_delete(self):
        """Check abot if class can be deleted"""
        return super(GitRepo, self).can_delete

    def delete(self):
        """Delete diagram objects"""
        super(GitRepo, self).delete()

    def remove_relations(self):
        """Utility for undo/redo"""
        super(GitRepo, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(GitRepo, self).restore_relations()

    def save_state(self):
        """Utility for saving state"""
        super(GitRepo, self).save_state()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(GitRepo, self).on_undo_redo_removing()

    def on_undo_redo_changed(self):
        """Update from app"""
        super(GitRepo, self).on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(GitRepo, self).on_undo_redo_add()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index('git_repo')

    @property
    def label(self):
        """Get tree label"""
        return 'repository {self._name}'.format(self=self)



