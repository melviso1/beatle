# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

"""
Created on Sun Dec 15 19:22:32 2013

@author: mel
"""
import os.path

import wx

from beatle import model
from beatle.app import resources as rc


class GitFile(model.TComponent):
    """Implements c++ class representation"""
    folder_container = True

    def __init__(self, **kwargs):
        """Initialization method"""
        self._lastHdrTime = None
        self._lastSrcTime = None
        self._name = kwargs['name']
        self._parent = kwargs['parent']
        #Query the status: the file is modified, staged or deleted?
        self._status = self.repo.status(self.rpath, kwargs.get('git_context', None))
        # wx.YieldIfNeeded()
        super(GitFile, self).__init__(**kwargs)
        # iterate over the tree
        # assert len(self._data.hexsha) == 40

    @property
    def repo(self):
        """return the repo"""
        return self.parent.repo

    def update_status(self, git_context=None):
        """Update the file status"""
        # wx.YieldIfNeeded()
        self._status = self.repo.status(self.rpath, git_context)
        self.on_undo_redo_changed()

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {'data': self._data}
        kwargs.update(super(GitFile, self).get_kwargs())
        return kwargs

    @property
    def can_delete(self):
        """Check abot if class can be deleted"""
        return super(GitFile, self).can_delete

    def delete(self):
        """Delete diagram objects"""
        super(GitFile, self).delete()

    def remove_relations(self):
        """Utility for undo/redo"""
        super(GitFile, self).remove_relations()

    def restore_relations(self):
        """Utility for undo/redo"""
        super(GitFile, self).restore_relations()

    def save_state(self):
        """Utility for saving state"""
        super(GitFile, self).save_state()

    def on_undo_redo_removing(self):
        """Prepare object to delete"""
        super(GitFile, self).on_undo_redo_removing()

    def on_undo_redo_changed(self):
        """Update from app"""
        #when the class is updated, ctor's and dtors, must be updated
        super(GitFile, self).on_undo_redo_changed()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(GitFile, self).on_undo_redo_add()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        return rc.get_bitmap_index(self._status)

    @property
    def path(self):
        """Access for this element"""
        return os.path.join(self.parent.path, self._name)

    @property
    def rpath(self):
        """Access for this element"""
        return os.path.join(self.parent.rpath, self._name)

    @property
    def label(self):
        """Get tree label"""
        return '{self._name}'.format(self=self)



