# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 23:24:30 2013

@author: mel
"""
from beatle.lib.tran import TransactionalMethod, TransactionalMoveObject

from ._TComponent import TComponent


class Folder(TComponent):
    """Implements a Folder representation"""
    class_container = True
    context_container = True
    folder_container = True
    diagram_container = True
    module_container = True
    namespace_container = True
    function_container = True
    variable_container = True
    member_container = True
    relation_container = True
    friendship_container = True

    # visual methods
    @TransactionalMethod('move folder {0}')
    def drop(self, to):
        """Drops class inside project or another folder """
        target = to.inner_folder_container
        if not target or self.inner_class != target.inner_class or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        """Initialization"""
        super(Folder, self).__init__(**kwargs)
        self.update_container()

    def update_container(self):
        """Update the container info"""
        self.folder_container = self._parent.folder_container

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        return super(Folder, self).get_kwargs()

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("folder")

    @property
    def bitmap_open_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("folder_open")

