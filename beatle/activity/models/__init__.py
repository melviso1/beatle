# -*- coding: utf-8 -*-

from . import ui
from . import handlers
from . import cc
from . import py


def init():
    """Initialize this activity"""
    ui.init()
    handlers.init()
    cc.init()
    py.init()
