
from . import ui
from . import handlers


def init():
    """Initialize this activity model"""
    ui.init()
    handlers.init()
