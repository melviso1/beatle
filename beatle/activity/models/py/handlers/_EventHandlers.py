'''
Created on 7 sept. 2018

@author: mel
'''

import copy, wx
from wx import aui

from beatle import local_path
from beatle.lib import wxx
from beatle.lib.tran import TransactionalMethod, format_current_transaction_name
from beatle.lib.wxx import CreationDialog
from beatle.lib.ostools import clipboard
from beatle.lib.handlers import identifier, SharedAcceleratorHandler
from beatle.lib.decorators import with_python_export
from beatle.activity.models.ui.view import ModelsView

from ..ui.dlg import PackageDialog, ModuleDialog, ImportDialog,\
    ClassDialog, InheritanceDialog, MemberMethodDialog, ArgumentDialog, \
    MemberDialog, FunctionDialog, VariableDialog, EditDecoratorsDialog

from ..ui.pane import MethodPane, ModulePane

from beatle.model.py import Package, Module, Import, Class, Inheritance, InitMethod, MemberMethod,\
    Function, Argument, ArgsArgument, KwArgsArgument, MemberData, Data, Decorator, ImportsFolder

from beatle.lib.api import context


class EventHandlers(object):

    _add_package_id = identifier("ADD_PYTHON_PACKAGE")
    _add_package_id_aui = identifier("ADD_PYTHON_PACKAGE_AUI")  #
    _add_module_id = identifier("ADD_PYTHON_MODULE")
    _add_module_id_aui = identifier("ADD_PYTHON_MODULE_AUI")  #
    _add_import_folder_id = identifier("ADD_PYTHON_IMPORT_FOLDER")
    _add_import_id = identifier("ADD_PYTHON_IMPORT")
    _add_class_id = identifier("ADD_PYTHON_CLASS")
    _add_class_id_aui = identifier("ADD_PYTHON_CLASS_AUI") #
    _add_inheritance_id = identifier("ADD_PYTHON_INHERITANCE")
    _add_inheritance_id_aui = identifier("ADD_PYTHON_INHERITANCE_AUI")  #
    _add_init_id = identifier("ADD_PYTHON_CLASS_INIT_METHOD")
    _add_init_id_aui = identifier("ADD_PYTHON_CLASS_INIT_METHOD_AUI")  #
    _add_method_id = identifier("ADD_PYTHON_METHOD")
    _add_method_id_aui = identifier("ADD_PYTHON_METHOD_AUI")  #
    _add_argument_id = identifier("ADD_PYTHON_ARGUMENT")
    _add_argument_id_aui = identifier("ADD_PYTHON_ARGUMENT_AUI")  #
    _add_args_id = identifier("ADD_PYTHON_ARGS")
    _add_kwargs_id = identifier("ADD_PYTHON_KWARGS")
    _add_member_id = identifier("ADD_PYTHON_CLASS_MEMBER")
    _add_member_id_aui = identifier("ADD_PYTHON_CLASS_MEMBER_AUI")  #
    _add_function_id = identifier("ADD_PYTHON_FUNCTION")
    _add_function_id_aui = identifier("ADD_PYTHON_FUNCTION_AUI")  #
    _add_variable_id = identifier("ADD_PYTHON_VARIABLE")
    _add_variable_id_aui = identifier("ADD_PYTHON_VARIABLE_AUI")  #
    _edit_decorator_id = identifier("EDIT_PYTHON_DECORATOR")
    _set_main_module_id = identifier("SET_PYTHON_MAIN_MODULE")

    @property
    def selected(self):
        return self._view.selected

    def __init__(self, view):
        self._view = view  # ModelView instance
        ctrl_alt = wx.ACCEL_CTRL + wx.ACCEL_ALT
        ctrl_shf = wx.ACCEL_CTRL + wx.ACCEL_SHIFT
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('C'), self._add_class_id)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('I'), self._add_inheritance_id),
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('M'), self._add_member_id),
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('F'), self._add_method_id)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('F'), self._add_function_id)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('A'), self._add_argument_id)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_alt, ord('M'), self._add_module_id)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('N'), self._add_init_id)

        # register edit handlers
        ModelsView._edit_map.update({
            self.open_module: [Module],
            self.open_method: [MemberMethod, InitMethod, Function],
            })

        super(EventHandlers, self).__init__()

    def _bind_events(self):
        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_package, id=self._add_package_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_package, id=self._add_package_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_package_aui, id=self._add_package_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_module, id=self._add_module_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_module, id=self._add_module_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_module_aui, id=self._add_module_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_import, id=self._add_import_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_import, id=self._add_import_id)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_class, id=self._add_class_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_class, id=self._add_class_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_class_aui, id=self._add_class_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_inheritance, id=self._add_inheritance_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_inheritance, id=self._add_inheritance_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_inheritance_aui, id=self._add_inheritance_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_init, id=self._add_init_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_init, id=self._add_init_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_init_aui, id=self._add_init_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_method, id=self._add_method_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_method, id=self._add_method_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_method_aui, id=self._add_method_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_arg, id=self._add_argument_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_arg, id=self._add_argument_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_arg_aui, id=self._add_argument_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_args, id=self._add_args_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_args, id=self._add_args_id)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_kwargs, id=self._add_kwargs_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_kwargs, id=self._add_kwargs_id)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_member, id=self._add_member_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_member, id=self._add_member_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_member_aui, id=self._add_member_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_function, id=self._add_function_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_function, id=self._add_function_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_function_aui, id=self._add_function_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_variable, id=self._add_variable_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_variable, id=self._add_variable_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_variable_aui, id=self._add_variable_id_aui)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_set_main_module, id=self._set_main_module_id)
        self._view.bind_special(wx.EVT_MENU, self.on_set_main_module, id=self._set_main_module_id)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_edit_decorators, id=self._edit_decorator_id)
        self._view.bind_special(wx.EVT_MENU, self.on_edit_decorators, id=self._edit_decorator_id)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_imports_folder, id=self._add_import_folder_id)
        self._view.bind_special(wx.EVT_MENU, self.on_add_imports_folder, id=self._add_import_folder_id)

    def set_model_menu(self, menu):
        self._menu = wxx.Menu()

        self._menu_add_package = wxx.MenuItem(self._menu, self._add_package_id,
                                              u"package ...\tShift+Ctrl+P", u"Add python package", wx.ITEM_NORMAL)
        self._menu_add_package.SetBitmap(wx.Bitmap(local_path("app/res/py_package.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_package)

        self._menu_add_module = wxx.MenuItem(self._menu, self._add_module_id,
                                             u"Module ...\tShift+Alt+M", u"add module", wx.ITEM_NORMAL)
        self._menu_add_module.SetBitmap(wx.Bitmap(local_path("app/res/py_module.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_module)

        self._menu_add_imports_folder = wxx.MenuItem(self._menu, self._add_import_folder_id,
                                                     u"imports folder", u"add python imports folder", wx.ITEM_NORMAL)
        self._menu_add_imports_folder.SetBitmap(wx.Bitmap(local_path("app/res/folderI.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_imports_folder)

        self._menu_add_import = wxx.MenuItem(self._menu, self._add_import_id,
                                             u"import ...\tCtrl+I", u"add python import", wx.ITEM_NORMAL)
        self._menu_add_import.SetBitmap(wx.Bitmap(local_path("app/res/import.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_import)

        self._menu_add_class = wxx.MenuItem(self._menu, self._add_class_id,
                                            u"Class ...\tCtrl+Shift+C", u"add class", wx.ITEM_NORMAL)
        self._menu_add_class.SetBitmap(wx.Bitmap(local_path("app/res/py_class.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_class)

        self._menu_add_inheritance = wxx.MenuItem(self._menu, self._add_inheritance_id,
                                                  u"Inheritance ...\tCtrl+Shift+I", u"add inheritance", wx.ITEM_NORMAL)
        self._menu_add_inheritance.SetBitmap(wx.Bitmap(local_path("app/res/py_inheritance.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_inheritance)

        self._menu_add_init_method = wxx.MenuItem(self._menu, self._add_init_id,
                                                  u"init\tCtrl+Shift+N", u"add init", wx.ITEM_NORMAL)
        self._menu_add_init_method.SetBitmap(wx.Bitmap(local_path("app/res/py_init.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_init_method)

        self._menu_add_method = wxx.MenuItem(self._menu, self._add_method_id,
                                             u"Method ...\tCtrl+Shift+F", u"add method", wx.ITEM_NORMAL)
        self._menu_add_method.SetBitmap(wx.Bitmap(local_path("app/res/py_method.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_method)

        self._menu_add_argument = wxx.MenuItem(self._menu, self._add_argument_id,
                                               u"Argument ...\tCtrl+Shift+A", u"add argument", wx.ITEM_NORMAL)
        self._menu_add_argument.SetBitmap(wx.Bitmap(local_path("app/res/py_argument.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_argument)

        self._menu_add_args_argument = wxx.MenuItem(self._menu, self._add_args_id,
                                                    u"*args", u"add variable argument list", wx.ITEM_NORMAL)
        self._menu_add_args_argument.SetBitmap(wx.Bitmap(local_path("app/res/py_args.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_args_argument)

        self._menu_add_kwargs_argument = wxx.MenuItem(self._menu, self._add_kwargs_id,
                                                      u"**kwargs", u"add variable argument list", wx.ITEM_NORMAL)
        self._menu_add_kwargs_argument.SetBitmap(wx.Bitmap(local_path("app/res/py_kwargs.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_kwargs_argument)

        self._menu_add_member = wxx.MenuItem(self._menu, self._add_member_id,
                                             u"Member ...\tCtrl+Shift+M", u"add member", wx.ITEM_NORMAL)
        self._menu_add_member.SetBitmap(wx.Bitmap(local_path("app/res/py_member.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_member)

        self._menu_add_function = wxx.MenuItem(self._menu, self._add_function_id,
                                               u"Function ...\tCtrl+Shift+F", u"add function", wx.ITEM_NORMAL)
        self._menu_add_function.SetBitmap(wx.Bitmap(local_path("app/res/py_function.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_function)

        self._menu_add_variable = wxx.MenuItem(self._menu, self._add_variable_id,
                                               u"Variable ...\tCtrl+Alt+V", u"add variable", wx.ITEM_NORMAL)
        self._menu_add_variable.SetBitmap(wx.Bitmap(local_path("app/res/py_variable.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_variable)

        self._menu_edit_decorators = wxx.MenuItem(self._menu, self._edit_decorator_id,
                                                  u"decorators ...", u"edit decorators", wx.ITEM_NORMAL)
        self._menu_edit_decorators.SetBitmap(wx.Bitmap(local_path("app/res/decorator.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_edit_decorators)

        self._menu.AppendSeparator()
        self._menu_set_entry = wxx.MenuItem(self._menu, self._set_main_module_id,
                                            u"entry point", wx.EmptyString, wx.ITEM_CHECK)
        self._menu.AppendItem(self._menu_set_entry)
        self._menu_set_entry.Enable(False)

        menu.AppendSubMenu(self._menu, u"python")

    def _create_toolbars(self):
        """Create the associated toolbars"""
        self._toolbar = aui.AuiToolBar(context.get_frame(), wx.ID_ANY, wx.DefaultPosition,
            wx.Size(16,16), style=aui.AUI_TB_GRIPPER | aui.AUI_TB_HORZ_LAYOUT|aui.AUI_TB_PLAIN_BACKGROUND)
        els = {
            self._add_package_id_aui: self._menu_add_package,
            self._add_module_id_aui: self._menu_add_module,
            self._add_class_id_aui: self._menu_add_class,
            self._add_inheritance_id_aui: self._menu_add_inheritance,
            self._add_member_id_aui: self._menu_add_member,
            self._add_method_id_aui: self._menu_add_method,
            self._add_init_id_aui: self._menu_add_init_method,
            self._add_argument_id_aui: self._menu_add_argument,
            self._add_function_id_aui: self._menu_add_function,
            self._add_variable_id_aui: self._menu_add_variable
        }

        self._toolbar.SetMinSize(wx.Size(24, 24))
        for el in els.keys():
            if els[el] is None:
                self._toolbar.AddSeparator()
            else:
                self._toolbar.AddTool(el, els[el].GetLabel(), els[el].GetBitmap(), els[el].GetHelp())
        self._toolbar.Realize()
        frame = context.get_frame()
        frame.m_mgr.AddPane(self._toolbar, aui.AuiPaneInfo().Name(u"python_toolbar").Top().
            Hide().Caption(u"python").PinButton(True).Gripper().Dock().Resizable().
            DockFixed(False).Row(0).Position(5).Layer(10).ToolbarPane())
        self._view.register_toolbar(self._toolbar)

    def update_menus(self):
        """At some platforms (windows) the procedure of menu item updates didn't work
        fine. This is specially true when the owner menus are collapsed are hidden.
        Also, we found some ramdom break in the menu item refresh iteration.
        As a better alternative, this procedure just do the refresh of the menu items
        status. And also, is faster than the standard way, so no loses here."""
        self._menu_add_package.Enable(self.can_add_package)
        self._menu_add_module.Enable(self.can_add_module)
        self._menu_add_import.Enable(self.can_add_import)
        self._menu_add_class.Enable(self.can_add_class)
        self._menu_add_inheritance.Enable(self.can_add_inheritance)
        self._menu_add_init_method.Enable(self.can_add_init_method)
        self._menu_add_method.Enable(self.can_add_method)
        self._menu_add_argument.Enable(self.can_add_arg)
        self._menu_add_args_argument.Enable(self.can_add_args)
        self._menu_add_kwargs_argument.Enable(self.can_add_kwargs)
        self._menu_add_member.Enable(self.can_add_member)
        self._menu_add_function.Enable(self.can_add_function)
        self._menu_add_variable.Enable(self.can_add_variable)
        self._menu_edit_decorators.Enable(self.can_edit_decorators)
        self._menu_add_imports_folder.Enable(self.can_add_imports_folder)
        enabled = self.can_set_main_module
        self._menu_set_entry.Enable(enabled)
        self._menu_set_entry.Check(enabled and self._view.selected and self._view.selected._entry)

    @property
    def can_paste(self):
        """Indicate if it's possible to paste'"""
        selected = self._view.selected
        if self.selected is None:
            return False
        project = selected.project
        if project is None or getattr(project, '_language', None) != 'python':
            return False
        data = clipboard.data
        if data.lang != 'python':
            return False
        if data.type in [InitMethod, MemberData, MemberMethod]:
            return selected.inner_member_container is not None
        if data.type is Argument:
            return self.can_add_arg
        elif data.type is ArgsArgument:
            return self.can_add_args
        elif data.type is KwArgsArgument:
            return self.can_add_kwargs
        elif data.type is Data:
            return self.can_add_variable
        elif data.type is Function:
            return self.can_add_function
        elif data.type is Import:
            return self.can_add_import
        elif data.type is ImportsFolder:
            return self.can_add_imports_folder
        elif data.type is Inheritance:
            return self.can_add_inheritance
        elif data.type is Module:
            return self.can_add_module
        return False

    @property
    def can_cut(self):
        """return info about the feasibility of cut"""
        obj = self._view.selected
        if getattr(obj, 'read_only', False):
            return False
        if obj is None:
            return False
        project = obj.project
        if project is None or getattr(project, '_language', None) != 'python':
            return False
        return  type(obj) not in [
            Package, Module, Class, InitMethod]

    @property
    def can_copy(self):
        """return info about the feasibility of copy"""
        obj = self._view.selected
        if obj is None:
            return False
        project = obj.project
        if project is None or getattr(project, '_language', None) != 'python':
            return False
        return  type(obj) not in [
            Package, Module, Class, InitMethod]

    @property
    def can_edit_open(self):
        """it's enabled editing?'"""
        obj = self._view.selected
        if obj is None:
            return False
        project = obj.project
        if project is None or getattr(project, '_language', None) != 'python':
            return False
        return  type(obj) in [InitMethod, MemberMethod, Function,Module]

    def on_edit_paste(self, event):
        """Handle paste event"""
        data = clipboard.data
        if data.lang != 'python':
            return False
        if data.type is Argument:
            self.paste_argument()
        elif data.type is MemberData:
            self.paste_member_data()
        elif data.type is InitMethod:
            self.paste_init()
        elif data.type is MemberMethod:
            self.paste_member_method()
        elif data.type is Class:
            self.paste_class()
        elif data.type is Function:
            self.paste_function()
        elif data.type is Import:
            self.paste_import()
        elif data.type is ImportsFolder:
            self.paste_imports_folder()
        elif data.type is Inheritance:
            self.paste_inheritance()
        elif data.type is Module:
            self.paste_module()
        else:
            return False
        return True

    @with_python_export
    @TransactionalMethod('paste python argument')
    def paste_argument(self):
        """Handle paste argument"""
        data = clipboard.data
        name = data.args['name']
        split = min([i for i in range(0, len(name)) if name[i:].isdigit()] + [len(name)])
        pre = name[0:split]
        index = int('0' + name[split:])
        target = self.selected.inner_argument_container
        while True:
            next_name = pre + str(index)
            if next_name in [x.name for x in target[Argument]]:
                index = index + 1
            else:
                break
        kwargs = copy.copy(data.args)
        kwargs['name'] = next_name
        kwargs['parent'] = target
        kwargs['child_index'] = -1
        Argument(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python member')
    def paste_member_data(self):
        """Handle paste member"""
        data = clipboard.data
        name = data.args['name']
        split = min([i for i in range(0, len(name)) if name[i:].isdigit()] + [len(name)])
        pre = name[0:split]
        index = int('0' + name[split:])
        members = self.selected.inner_class(MemberData)
        next_name = pre
        while next_name in [x.name for x in members]:
            index = index + 1
            next_name = pre + str(index)
        kwargs = copy.copy(data.args)
        kwargs['name'] = next_name
        kwargs['parent'] = self.selected.inner_member_container
        MemberData(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python __init__')
    def paste_init(self):
        """Handle paste constructor"""
        # pasting a constructor is easy, we dont need to check anything more
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self.selected.inner_member_container
        InitMethod(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python member method')
    def paste_member_method(self):
        """Handle paste method"""
        # pasting a method is easy, we dont need to check anything more
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self.selected.inner_member_container
        MemberMethod(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python import')
    def paste_import(self):
        """Handle paste import"""
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self.selected.inner_import_container
        Import(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python import folder')
    def paste_imports_folder(self):
        """Handle pasting an import folder"""
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self.selected.inner_import_container
        ImportsFolder(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python inheritance')
    def paste_inheritance(self):
        """Handle pasting an inheritance"""
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self.selected.inner_inheritance_container
        Inheritance(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python class')
    def paste_class(self):
        """Handle paste python class"""
        # pasting a python class has two main scenarios:
        #     a) pasting inside the same project
        #     b) pasting inside other project
        # how to check it?
        # case A:-
        # This cass seems easy: all we need to do is to rename
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['derivatives'] = None
        kwargs['parent'] = self.selected.inner_member_container
        Class(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python function')
    def paste_function(self):
        """Handle pase python function"""
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self.selected.inner_function_container
        Function(**kwargs)
        return True

    @with_python_export
    @TransactionalMethod('paste python module')
    def paste_module(self):
        """Handle paste python module"""
        target = self.selected.inner_module_container
        if target is None:  # last resource check
            return False
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = target
        Module(**kwargs)
        return True

    @property
    def can_add_package(self):
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_package_container)

    def on_update_add_package(self, event):
        """Update add package"""
        enabled = self.can_add_package
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python package {0}')
    @CreationDialog(PackageDialog, Package)
    def on_add_package(self, event):
        """Handle add module method"""
        return context.get_frame(), self._view.selected.inner_module_container

    def on_add_package_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_package_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_module(self):
        """A module can be added here?"""
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_module_container)

    def on_update_add_module(self, event):
        """Update add module"""
        enabled = self.can_add_module
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python module {0}')
    @CreationDialog(ModuleDialog, Module)
    def on_add_module(self, event):
        """Handle add module method"""
        if not self.can_add_module:
            return False
        return context.get_frame(), self._view.selected.inner_module_container

    def on_add_module_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_module_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_import(self):
        """can be added a python import at selected place?"""
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_import_container)

    def on_update_add_import(self, event):
        """Update AddPyImport method"""
        enabled = self.can_add_import
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add phython import {0}')
    @CreationDialog(ImportDialog, Import)
    def on_add_import(self, event):
        """Handle add python __init__ method"""
        return context.get_frame(), self._view.selected.inner_import_container

    @property
    def can_add_class(self):
        """Can be added a class?"""
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_class_container)

    def on_update_add_class(self, event):
        """Updade AddPyClass method"""
        enabled = self.can_add_class
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python class {0}')
    @CreationDialog(ClassDialog, Class)
    def on_add_class(self, event):
        """Handles add pythpn class"""
        if not self.can_add_class:
            return False
        return context.get_frame(), self._view.selected.inner_class_container

    def on_add_class_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_class_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_inheritance(self):
        """can be added a python inheritance at selected place?"""
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_inheritance_container)

    def on_update_add_inheritance(self, event):
        """Handle update add python inheritance"""
        enabled = self.can_add_inheritance
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python {0} inheritance')
    @CreationDialog(InheritanceDialog, Inheritance)
    def on_add_inheritance(self, event):
        """Handle add inheritance command"""
        if not self.can_add_inheritance:
            return False
        return context.get_frame(), self._view.selected.inner_inheritance_container

    def on_add_inheritance_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_inheritance_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_init_method(self):
        v = self._view.selected
        v = v and v.project and v.project.language == 'python' and v.inner_class
        return bool(v and v.project and not v(InitMethod, filter=lambda x: x.inner_class is v, cut=True))

    def on_update_add_init(self, event):
        """Update AddPyInit method"""
        enabled = self.can_add_init_method
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add phython __init__')
    def on_add_init(self, event):
        """Handle add python __init__ method"""
        if not self.can_add_init_method:
            return False
        InitMethod(parent=self._view.selected.inner_member_container)
        return True

    def on_add_init_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_init_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_method(self):
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_member_container)

    def on_update_add_method(self, event):
        """Update add method method"""
        enabled = self.can_add_method
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python method {0}')
    @CreationDialog(MemberMethodDialog, MemberMethod)
    def on_add_method(self, event):
        """Handle add method command"""
        if not self.can_add_method:
            return False
        return context.get_frame(), self._view.selected.inner_member_container

    def on_add_method_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_method_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_arg(self):
        """Can be add an argument?"""
        v = self._view.selected
        return bool(type(v) in [Function, MemberMethod, InitMethod])

    def on_update_add_arg(self, event):
        """Update add argument list method"""
        enabled = self.can_add_arg
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python method argument {0}')
    @CreationDialog(ArgumentDialog, Argument)
    def on_add_arg(self, event):
        """Handle add method command"""
        if not self.can_add_arg:
            return False
        return context.get_frame(), self._view.selected.inner_argument_container

    def on_add_arg_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_argument_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_args(self):
        """Can add an 'args'?"""
        v = self._view.selected
        v = v and v.project and v.inner_argument_container
        return bool(type(v) in [Function, MemberMethod, InitMethod] and not v[ArgsArgument])

    def on_update_add_args(self, event):
        """Update add argument list method"""
        enabled = self.can_add_args
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add *args')
    def on_add_args(self, event):
        """Handle add arg list command"""
        if not self.can_add_args:
            return False
        return ArgsArgument(parent=self._view.selected.inner_argument_container)

    @property
    def can_add_kwargs(self):
        """Can add a kwargs?"""
        v = self._view.selected
        v = v and v.project and v.inner_argument_container
        return bool(type(v) in [Function, MemberMethod, InitMethod] and not v[KwArgsArgument])

    def on_update_add_kwargs(self, event):
        """Update add argument list method"""
        enabled = self.can_add_kwargs
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add *kwargs')
    def on_add_kwargs(self, event):
        """Handle add arg list command"""
        if not self.can_add_kwargs:
            return False
        return KwArgsArgument(parent=self._view.selected.inner_argument_container)

    @property
    def can_add_member(self):
        v = self._view.selected
        return bool(v and v.project and v.project.language=='python' and v.inner_member_container)

    def on_update_add_member(self, event):
        """Update add member"""
        enabled = self.can_add_member
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python {0} class member')
    @CreationDialog(MemberDialog, MemberData)
    def on_add_member(self, event):
        """Handle add member command"""
        if not self.can_add_member:
            return False
        return context.get_frame(), self._view.selected.inner_member_container

    def on_add_member_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_member_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_function(self):
        """Handle add python function"""
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python'
                    and not v.inner_class and v.inner_function_container)

    def on_update_add_function(self, event):
        """Handle update add function method"""
        enabled = self.can_add_function
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python function {0}')
    @CreationDialog(FunctionDialog, Function)
    def on_add_function(self, event):
        """Handle add function command"""
        if not self.can_add_function:
            return False
        return context.get_frame(), self._view.selected.inner_function_container

    def on_add_function_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_function_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_add_variable(self):
        """can add a python variable at selected place?"""
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_variable_container)

    def on_update_add_variable(self, event):
        """Update AddVariable method"""
        enabled = self.can_add_variable
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('add python variable {0}')
    @CreationDialog(VariableDialog, Data)
    def on_add_variable(self, event):
        """Handles add variable method"""
        if not self.can_add_variable:
            return False
        return context.get_frame(), self._view.selected.inner_variable_container

    def on_add_variable_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._add_variable_id)
        wx.PostEvent(context.get_frame().GetEventHandler(), event_new)

    @property
    def can_set_main_module(self):
        """can set python entry here?"""
        v = self._view and self._view.selected
        return bool(type(v) in [Module, Package])

    def on_update_set_main_module(self, event):
        """Update set the main file"""
        enabled = self.can_set_main_module
        event.Enable(enabled)
        checked = enabled and self._view.selected._entry
        event.Check(checked)

    @TransactionalMethod('set {0} as main module')
    def on_set_main_module(self, event):
        """Make a module the run entry  point"""
        if not self.can_set_main_module:
            return False
        project = self._view.selected.project
        project.save_state()
        old = project.main_file
        if old:
            old.save_state()
            old._entry = False
        if old == self.selected:
            project.main_file = None
            format_current_transaction_name('None')
        else:
            self.selected.save_state()
            project.main_file = self.selected
            self.selected._entry = True
            format_current_transaction_name(self.selected.name)
        return True

    @property
    def can_edit_decorators(self):
        v = self._view and self._view.selected
        return type(v) in [MemberMethod, Function, InitMethod]

    def on_update_edit_decorators(self, event):
        """Update decorators edition"""
        enabled = self.can_edit_decorators
        event.Enable(enabled)

    @with_python_export
    @TransactionalMethod('edit {0} decorators')
    @CreationDialog(EditDecoratorsDialog, Decorator)
    def on_edit_decorators(self, event):
        """Edit decorators"""
        if not self.can_edit_decorators:
            return False
        return context.get_frame(), self._view.selected

    @property
    def can_add_imports_folder(self):
        """Can add an imports folder here?"""
        v = self._view.selected
        return bool(v and v.project and v.project.language == 'python' and v.inner_import_container)

    def on_update_add_imports_folder(self, event):
        """Update add imports folder"""
        enabled = self.can_add_imports_folder
        event.Enable(enabled)

    @TransactionalMethod('add python imports folder')
    def on_add_imports_folder(self, event):
        """Add imports folder"""
        if not self.can_add_imports_folder:
            return False
        ImportsFolder(parent=self._view.selected.inner_import_container)
        return True

    def open_method(self, method, view=None):
        """Open method code for editing"""
        if not hasattr(method, '_pane') or method._pane is None:
            frame = self._view.frame
            p = MethodPane(frame.docBook, frame, method)
            method._pane = p
            frame.docBook.AddPage(p, method.tab_label, True, bitmap=method.get_tab_bitmap())

    def open_module(self, module, view=None):
        """Open a module for editing"""
        if getattr(module, '_pane', None):
            raise RuntimeError('Already open pane for module')
        frame = self._view.frame
        p = ModulePane(frame.docBook, frame, module)
        module._pane = p
        frame.docBook.AddPage(p, module.tab_label, True, module.bitmap_index)

