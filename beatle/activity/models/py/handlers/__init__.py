# -*- coding: utf-8 -*-

from ._EditorHandler import EditorHandler
from ._EventHandlers import EventHandlers

def init():
    from beatle.app.ui.tools import MODELVIEW_EVENT_HANDLERS
    MODELVIEW_EVENT_HANDLERS.append(EventHandlers)