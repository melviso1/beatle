# -*- coding: utf-8 -*-

from . import ctrl
from . import dlg
from . import pane


def init():
    """Initialize this activity model ui"""
    from beatle.activity.models.ui.view import ModelsView
    from beatle.model.py import Package, Inheritance, Folder, ImportsFolder, Import, Class, \
        MemberData, InitMethod, MemberMethod, Argument, ArgsArgument, KwArgsArgument, \
        Decorator, Module, Function, Data

    ModelsView.tree_order.extend([
            Package, Inheritance, Folder, ImportsFolder, Import, Class, MemberData,  InitMethod,
            MemberMethod, Argument, ArgsArgument, KwArgsArgument, Decorator, Module, Function, Data, 
            ])

    ModelsView.alias_map.update({
        Folder: "folder {0}",
        Class: "python class {0}",
        Inheritance: "python inheritance from {0}",
        Package: "python package {0}",
        Argument: "python method argument {0}",
        ArgsArgument: "python method argument list",
        KwArgsArgument: "python method argument dict",
        InitMethod: "__init__ method",
        MemberMethod: "python method {0}",
        MemberData: "python member {0}",
        Function: 'python function {0}',
        Module: 'python module "{0}"',
        Data: 'python variable {0}',
        Import: 'python import {0}',
        Decorator: 'python decorator {0}'
        })
    dlg.init()
