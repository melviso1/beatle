# -*- coding: utf-8 -*-

###########################################################################
# Python code generated with wxFormBuilder (version Aug 26 2018)
# http://www.wxformbuilder.org/
#
# PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.richtext
from beatle.lib import wxx


# special import for beatle development

###########################################################################
# Class MethodPaneBase
###########################################################################


class MethodPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(500, 300),
                          style=wx.NO_BORDER | wx.TAB_TRAVERSAL | wx.WANTS_CHARS)
        # self.SetExtraStyle( wx.WS_EX_BLOCK_EVENTS )
        self._mainSizer = wx.FlexGridSizer(2, 1, 0, 0)
        self._mainSizer.AddGrowableCol(0)
        self._mainSizer.AddGrowableRow(1)
        self._mainSizer.SetFlexibleDirection(wx.BOTH)
        self._mainSizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_toolbarSizer = wx.FlexGridSizer(1, 1, 0, 0)
        self.m_toolbarSizer.AddGrowableCol(0)
        self.m_toolbarSizer.AddGrowableRow(0)
        self.m_toolbarSizer.SetFlexibleDirection(wx.BOTH)
        self.m_toolbarSizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self._mainSizer.Add(self.m_toolbarSizer, 1, wx.EXPAND, 5)

        from beatle.activity.models.py.ui.ctrl import Editor
        self.m_editor = Editor(self, **self._editorArgs)
        self._mainSizer.Add(self.m_editor, 1, wx.EXPAND, 5)

        self.SetSizer(self._mainSizer)
        self.Layout()

        # Connect Events
        self.m_editor.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_editor.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnKillFocus(self, event):
        event.Skip()

    def OnGetFocus(self, event):
        event.Skip()


###########################################################################
## Class ModulePaneBase
###########################################################################

class ModulePaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(500, 300),
                          style=wx.NO_BORDER | wx.TAB_TRAVERSAL | wx.WANTS_CHARS)

        # self.SetExtraStyle( wx.WS_EX_BLOCK_EVENTS )

        self._mainSizer = wx.FlexGridSizer(2, 1, 0, 0)
        self._mainSizer.AddGrowableCol(0)
        self._mainSizer.AddGrowableRow(1)
        self._mainSizer.SetFlexibleDirection(wx.BOTH)
        self._mainSizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_toolbarSizer = wx.FlexGridSizer(1, 1, 0, 0)
        self.m_toolbarSizer.AddGrowableCol(0)
        self.m_toolbarSizer.AddGrowableRow(0)
        self.m_toolbarSizer.SetFlexibleDirection(wx.BOTH)
        self.m_toolbarSizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self._mainSizer.Add(self.m_toolbarSizer, 1, wx.EXPAND, 5)

        from beatle.activity.models.py.ui.ctrl import Editor
        self.m_editor = Editor(self, **self._editorArgs)
        self._mainSizer.Add(self.m_editor, 1, wx.EXPAND, 5)

        self.SetSizer(self._mainSizer)
        self.Layout()

        # Connect Events
        self.m_editor.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_editor.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnKillFocus(self, event):
        event.Skip()

    def OnGetFocus(self, event):
        event.Skip()


###########################################################################
## Class ArgumentDialogBase
###########################################################################

class ArgumentDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New argument", pos=wx.DefaultPosition,
                           size=wx.Size(428, 356), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer23 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer23.AddGrowableCol(0)
        fgSizer23.AddGrowableRow(1)
        fgSizer23.SetFlexibleDirection(wx.BOTH)
        fgSizer23.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer24 = wx.FlexGridSizer(2, 4, 0, 0)
        fgSizer24.AddGrowableCol(1)
        fgSizer24.AddGrowableCol(3)
        fgSizer24.SetFlexibleDirection(wx.BOTH)
        fgSizer24.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"&name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer24.Add(self.m_staticText9, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl6 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       0 | wx.SUNKEN_BORDER)
        fgSizer24.Add(self.m_textCtrl6, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText111 = wx.StaticText(self, wx.ID_ANY, u"=", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText111.Wrap(-1)
        fgSizer24.Add(self.m_staticText111, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl8 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer24.Add(self.m_textCtrl8, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer23.Add(fgSizer24, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"note"), wx.VERTICAL)

        self.m_richText1 = wx.richtext.RichTextCtrl(sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer6.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer23.Add(sbSizer6, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer23.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer23)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ClassDialogBase
###########################################################################

class ClassDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New class", pos=wx.DefaultPosition,
                           size=wx.Size(489, 464), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(1)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer7 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer7.AddGrowableCol(1)
        fgSizer7.AddGrowableRow(1)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name :", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)

        self.m_staticText48 = wx.StaticText(self, wx.ID_ANY, u"Members prefix :", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText48.Wrap(-1)
        fgSizer7.Add(self.m_staticText48, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textPrefix = wx.TextCtrl(self, wx.ID_ANY, u"_", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer7.Add(self.m_textPrefix, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer7.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox94 = wx.CheckBox(self, wx.ID_ANY, u"export", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer7.Add(self.m_checkBox94, 0, wx.ALL, 5)

        fgSizer6.Add(fgSizer7, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer6.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ImportDialogBase
###########################################################################

class ImportDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New import",
                           pos=wx.DefaultPosition, size=wx.Size(423, 390), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle(self.GetExtraStyle() | wx.WS_EX_VALIDATE_RECURSIVELY)

        fgSizer54 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer54.AddGrowableCol(0)
        fgSizer54.AddGrowableRow(1)
        fgSizer54.SetFlexibleDirection(wx.BOTH)
        fgSizer54.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer242 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer242.AddGrowableCol(0)
        fgSizer242.AddGrowableRow(0)
        fgSizer242.SetFlexibleDirection(wx.BOTH)
        fgSizer242.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_choicebook5 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT)
        self.m_choicebook5.SetExtraStyle(wx.WS_EX_VALIDATE_RECURSIVELY)

        self.m_panel31 = wx.Panel(self.m_choicebook5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel31.SetExtraStyle(wx.WS_EX_VALIDATE_RECURSIVELY)

        fgSizer243 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer243.AddGrowableCol(0)
        fgSizer243.SetFlexibleDirection(wx.BOTH)
        fgSizer243.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_textCtrl82 = wx.TextCtrl(self.m_panel31, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        fgSizer243.Add(self.m_textCtrl82, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer244 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer244.AddGrowableCol(1)
        fgSizer244.SetFlexibleDirection(wx.BOTH)
        fgSizer244.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox147 = wx.CheckBox(self.m_panel31, wx.ID_ANY, u"as", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer244.Add(self.m_checkBox147, 0, wx.ALL, 5)

        self.m_textCtrl83 = wx.TextCtrl(self.m_panel31, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        self.m_textCtrl83.Enable(False)

        fgSizer244.Add(self.m_textCtrl83, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer243.Add(fgSizer244, 1, wx.EXPAND, 5)

        self.m_panel31.SetSizer(fgSizer243)
        self.m_panel31.Layout()
        fgSizer243.Fit(self.m_panel31)
        self.m_choicebook5.AddPage(self.m_panel31, u"import", True)
        self.m_panel32 = wx.Panel(self.m_choicebook5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel32.SetExtraStyle(wx.WS_EX_VALIDATE_RECURSIVELY)

        fgSizer2431 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer2431.AddGrowableCol(0)
        fgSizer2431.SetFlexibleDirection(wx.BOTH)
        fgSizer2431.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_textCtrl821 = wx.TextCtrl(self.m_panel32, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                         wx.TE_PROCESS_ENTER | wx.WANTS_CHARS)
        fgSizer2431.Add(self.m_textCtrl821, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer2441 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer2441.AddGrowableCol(1)
        fgSizer2441.SetFlexibleDirection(wx.BOTH)
        fgSizer2441.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText134 = wx.StaticText(self.m_panel32, wx.ID_ANY, u"import", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText134.Wrap(-1)
        fgSizer2441.Add(self.m_staticText134, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_comboBox12Choices = [u"caca", u"pis"]
        self.m_comboBox12 = wx.ComboBox(self.m_panel32, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        m_comboBox12Choices, wx.CB_DROPDOWN | wx.WANTS_CHARS)
        fgSizer2441.Add(self.m_comboBox12, 0, wx.ALL | wx.EXPAND, 5)

        self.m_checkBox1471 = wx.CheckBox(self.m_panel32, wx.ID_ANY, u"as", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2441.Add(self.m_checkBox1471, 0, wx.ALL, 5)

        self.m_textCtrl831 = wx.TextCtrl(self.m_panel32, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                         0)
        self.m_textCtrl831.Enable(False)

        fgSizer2441.Add(self.m_textCtrl831, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer2431.Add(fgSizer2441, 1, wx.EXPAND, 5)

        self.m_panel32.SetSizer(fgSizer2431)
        self.m_panel32.Layout()
        fgSizer2431.Fit(self.m_panel32)
        self.m_choicebook5.AddPage(self.m_panel32, u"from", False)
        fgSizer242.Add(self.m_choicebook5, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer54.Add(fgSizer242, 1, wx.EXPAND, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer54.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer43.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_button7 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button7.SetDefault()
        fgSizer43.Add(self.m_button7, 0, wx.ALL, 5)

        fgSizer54.Add(fgSizer43, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer54)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_checkBox147.Bind(wx.EVT_CHECKBOX, self.OnAs)
        self.m_textCtrl821.Bind(wx.EVT_TEXT, self.OnFromChange)
        self.m_comboBox12.Bind(wx.EVT_CHAR, self.OnChangeFor)
        self.m_comboBox12.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.m_checkBox1471.Bind(wx.EVT_CHECKBOX, self.OnAs)
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnAs(self, event):
        event.Skip()

    def OnFromChange(self, event):
        event.Skip()

    def OnChangeFor(self, event):
        event.Skip()

    def OnKeyDown(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class InheritanceDialogBase
###########################################################################

class InheritanceDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New inheritance", pos=wx.DefaultPosition,
                           size=wx.Size(423, 390), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle(self.GetExtraStyle() | wx.WS_EX_VALIDATE_RECURSIVELY)

        fgSizer54 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer54.AddGrowableCol(0)
        fgSizer54.AddGrowableRow(1)
        fgSizer54.SetFlexibleDirection(wx.BOTH)
        fgSizer54.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer55 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer55.AddGrowableCol(1)
        fgSizer55.SetFlexibleDirection(wx.BOTH)
        fgSizer55.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText24 = wx.StaticText(self, wx.ID_ANY, u"from class:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText24.Wrap(-1)
        fgSizer55.Add(self.m_staticText24, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_choice11Choices = []
        self.m_choice11 = wx.ComboBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                      m_choice11Choices, 0)
        fgSizer55.Add(self.m_choice11, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer54.Add(fgSizer55, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer54.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        self.m_info.SetMinSize(wx.Size(32, 32))
        fgSizer43.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_button7 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button7.SetDefault()
        fgSizer43.Add(self.m_button7, 0, wx.ALL, 5)

        fgSizer54.Add(fgSizer43, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer54)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class MemberDialogBase
###########################################################################

class MemberDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New member", pos=wx.DefaultPosition,
                           size=wx.Size(545, 392), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer23 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer23.AddGrowableCol(0)
        fgSizer23.AddGrowableRow(1)
        fgSizer23.SetFlexibleDirection(wx.BOTH)
        fgSizer23.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer24 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer24.AddGrowableCol(0)
        fgSizer24.SetFlexibleDirection(wx.BOTH)
        fgSizer24.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer202 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer202.AddGrowableCol(0)
        fgSizer202.SetFlexibleDirection(wx.BOTH)
        fgSizer202.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer200 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer200.AddGrowableCol(1)
        fgSizer200.SetFlexibleDirection(wx.BOTH)
        fgSizer200.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_ALL)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer200.Add(self.m_staticText9, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.TOP | wx.RIGHT | wx.LEFT,
                       5)

        self.m_textCtrl6 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       0 | wx.SUNKEN_BORDER)
        fgSizer200.Add(self.m_textCtrl6, 0, wx.EXPAND | wx.TOP | wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer202.Add(fgSizer200, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.EXPAND, 5)

        sbSizer52 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"initialization"), wx.VERTICAL)

        self.m_textCtrl8 = wx.TextCtrl(sbSizer52.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                       wx.DefaultSize, wx.TE_MULTILINE)
        sbSizer52.Add(self.m_textCtrl8, 0, wx.EXPAND | wx.TOP | wx.RIGHT, 5)

        fgSizer202.Add(sbSizer52, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer24.Add(fgSizer202, 1, wx.EXPAND, 5)

        m_radioBox1Choices = [u"class variable", u"instance variable"]
        self.m_radioBox1 = wx.RadioBox(self, wx.ID_ANY, u"access", wx.DefaultPosition, wx.DefaultSize,
                                       m_radioBox1Choices, 1, wx.RA_SPECIFY_COLS)
        self.m_radioBox1.SetSelection(1)
        fgSizer24.Add(self.m_radioBox1, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer23.Add(fgSizer24, 1, wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)

        sbSizer6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"notes"), wx.VERTICAL)

        self.m_richText1 = wx.richtext.RichTextCtrl(sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer6.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer23.Add(sbSizer6, 0, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer23.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer23)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class MemberMethodDialogBase
###########################################################################

class MemberMethodDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New method", pos=wx.DefaultPosition,
                           size=wx.Size(453, 392), style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer12 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer12.AddGrowableCol(0)
        fgSizer12.AddGrowableRow(3)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer15 = wx.FlexGridSizer(2, 3, 0, 0)
        fgSizer15.AddGrowableCol(1)
        fgSizer15.SetFlexibleDirection(wx.BOTH)
        fgSizer15.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY, u"method name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText6.Wrap(-1)
        fgSizer15.Add(self.m_staticText6, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_comboBox5Choices = [u"__new__", u"__init__", u"__del__", u"__cmp__", u"__eq__", u"__ne__", u"__lt__",
                              u"__gt__", u"__le__", u"__ge__", u"__pos__", u"__neg__", u"__abs__", u"__invert__",
                              u"__round__", u"__floor__", u"__ceil__", u"__trunc__", u"__add__", u"__sub__", u"__mul__",
                              u"__floordiv__", u"__div__", u"__truediv__", u"__mod__", u"__divmod__", u"__pow__",
                              u"__lshift__", u"__rshift__", u"__and__", u"__or__", u"__xor__", u"__radd__", u"__rsub__",
                              u"__rmul__", u"__rfloordiv__", u"__rdiv__", u"__rtruediv__", u"__rmod__", u"__rdivmod__",
                              u"__rpow__", u"__rlshift__", u"__rrshift__", u"__rand__", u"__ror__", u"__rxor__",
                              u"__iadd__", u"__isub__", u"__imul__", u"__ifloordiv__", u"__idiv__", u"__itruediv__",
                              u"__imod__", u"__ipow__", u"__ilshift__", u"__irshift__", u"__iand__", u"__ior__",
                              u"__ixor__", u"__int__", u"__long__", u"__float__", u"__complex__", u"__oct__",
                              u"__hex__", u"__index__", u"__trunc__", u"__coerce__", u"__str__", u"__repr__",
                              u"__unicode__", u"__format__", u"__hash__", u"__nonzero__", u"__dir__", u"__sizeof__",
                              u"__getattr__", u"__setattr__", u"__delattr__", u"__getattribute__", u"__len__",
                              u"__getitem__", u"__setitem__", u"__delitem__", u"__iter__", u"__reversed__",
                              u"__contains__", u"__missing__", u"__call__", u"__enter__", u"__exit__", u"__get__",
                              u"__set__", u"__delete__", u"__copy__", u"__deepcopy__", u"__getinitargs__",
                              u"__getnewargs__", u"__getstate__", u"__setstate__", u"__reduce__", u"__reduce_ex__"]
        self.m_comboBox5 = wx.ComboBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       m_comboBox5Choices, 0)
        fgSizer15.Add(self.m_comboBox5, 0, wx.ALL | wx.EXPAND, 5)

        self.m_checkBox86 = wx.CheckBox(self, wx.ID_ANY, u"&implement", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox86.SetValue(True)
        fgSizer15.Add(self.m_checkBox86, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer15, 1, wx.EXPAND | wx.ALL, 5)

        sbSizer3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Properties"), wx.VERTICAL)

        fgSizer20 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer20.AddGrowableCol(0)
        fgSizer20.AddGrowableCol(1)
        fgSizer20.AddGrowableCol(2)
        fgSizer20.SetFlexibleDirection(wx.BOTH)
        fgSizer20.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox6 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"@&staticmethod", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox6, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox21 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"@&classmethod", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox21, 0, wx.ALL, 5)

        self.m_checkBox41 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"@&property", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox41, 0, wx.ALL, 5)

        sbSizer3.Add(fgSizer20, 1, wx.EXPAND, 5)

        fgSizer12.Add(sbSizer3, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"Notes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer12.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.m_richText1 = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        fgSizer12.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer21 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer21.AddGrowableCol(0)
        fgSizer21.SetFlexibleDirection(wx.BOTH)
        fgSizer21.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer21.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button1 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer21.Add(self.m_button1, 0, wx.ALL, 5)

        self.m_button2 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button2.SetDefault()
        fgSizer21.Add(self.m_button2, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer21, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer12)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button1.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button2.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class FunctionDialogBase
###########################################################################

class FunctionDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New method", pos=wx.DefaultPosition,
                           size=wx.Size(578, 450), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer12 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer12.AddGrowableCol(0)
        fgSizer12.AddGrowableRow(2)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer15 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer15.AddGrowableCol(1)
        fgSizer15.SetFlexibleDirection(wx.BOTH)
        fgSizer15.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText6.Wrap(-1)
        fgSizer15.Add(self.m_staticText6, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl5 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer15.Add(self.m_textCtrl5, 0, wx.ALL | wx.EXPAND, 5)

        self.m_checkBox86 = wx.CheckBox(self, wx.ID_ANY, u"&implement", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox86.SetValue(True)
        fgSizer15.Add(self.m_checkBox86, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer15, 1, wx.EXPAND | wx.ALL, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"Notes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer12.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.m_richText1 = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        fgSizer12.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer21 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer21.AddGrowableCol(0)
        fgSizer21.SetFlexibleDirection(wx.BOTH)
        fgSizer21.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer21.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button1 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer21.Add(self.m_button1, 0, wx.ALL, 5)

        self.m_button2 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button2.SetDefault()
        fgSizer21.Add(self.m_button2, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer21, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer12)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button1.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button2.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class PackageDialogBase
###########################################################################

class PackageDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New package", pos=wx.DefaultPosition,
                           size=wx.Size(380, 320), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer106 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer106.AddGrowableCol(0)
        fgSizer106.AddGrowableRow(1)
        fgSizer106.SetFlexibleDirection(wx.BOTH)
        fgSizer106.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer40 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, wx.EmptyString), wx.HORIZONTAL)

        fgSizer108 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer108.AddGrowableCol(1)
        fgSizer108.SetFlexibleDirection(wx.HORIZONTAL)
        fgSizer108.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText98 = wx.StaticText(sbSizer40.GetStaticBox(), wx.ID_ANY, u"Name:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText98.Wrap(-1)
        fgSizer108.Add(self.m_staticText98, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_package_name = wx.TextCtrl(sbSizer40.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        fgSizer108.Add(self.m_package_name, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)

        sbSizer40.Add(fgSizer108, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer106.Add(sbSizer40, 1, wx.EXPAND | wx.ALL, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer106.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer157 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer157.AddGrowableCol(1)
        fgSizer157.SetFlexibleDirection(wx.BOTH)
        fgSizer157.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer157.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer9 = wx.StdDialogButtonSizer()
        self.m_sdbSizer9OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer9.AddButton(self.m_sdbSizer9OK)
        self.m_sdbSizer9Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer9.AddButton(self.m_sdbSizer9Cancel)
        m_sdbSizer9.Realize();

        fgSizer157.Add(m_sdbSizer9, 1, wx.EXPAND | wx.ALIGN_RIGHT, 5)

        fgSizer106.Add(fgSizer157, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer106)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer9Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer9OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ModuleDialogBase
###########################################################################

class ModuleDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New module", pos=wx.DefaultPosition,
                           size=wx.Size(401, 357), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer106 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer106.AddGrowableCol(0)
        fgSizer106.AddGrowableRow(1)
        fgSizer106.SetFlexibleDirection(wx.BOTH)
        fgSizer106.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer40 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, wx.EmptyString), wx.HORIZONTAL)

        fgSizer108 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer108.AddGrowableCol(1)
        fgSizer108.SetFlexibleDirection(wx.HORIZONTAL)
        fgSizer108.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText98 = wx.StaticText(sbSizer40.GetStaticBox(), wx.ID_ANY, u"Name:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText98.Wrap(-1)
        fgSizer108.Add(self.m_staticText98, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_moduleName = wx.TextCtrl(sbSizer40.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer108.Add(self.m_moduleName, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)

        sbSizer40.Add(fgSizer108, 1, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer106.Add(sbSizer40, 0, wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        sbSizer6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"notes"), wx.VERTICAL)

        self.m_richText1 = wx.richtext.RichTextCtrl(sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer6.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer106.Add(sbSizer6, 1, wx.EXPAND, 5)

        fgSizer157 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer157.AddGrowableCol(1)
        fgSizer157.SetFlexibleDirection(wx.BOTH)
        fgSizer157.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer157.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer9 = wx.StdDialogButtonSizer()
        self.m_sdbSizer9OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer9.AddButton(self.m_sdbSizer9OK)
        self.m_sdbSizer9Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer9.AddButton(self.m_sdbSizer9Cancel)
        m_sdbSizer9.Realize();

        fgSizer157.Add(m_sdbSizer9, 1, wx.EXPAND | wx.ALIGN_RIGHT, 5)

        fgSizer106.Add(fgSizer157, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer106)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer9OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class VariableDialogBase
###########################################################################

class VariableDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New variable", pos=wx.DefaultPosition,
                           size=wx.Size(483, 409), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer23 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer23.AddGrowableCol(0)
        fgSizer23.AddGrowableRow(1)
        fgSizer23.SetFlexibleDirection(wx.BOTH)
        fgSizer23.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer24 = wx.FlexGridSizer(1, 5, 0, 0)
        fgSizer24.AddGrowableCol(1)
        fgSizer24.AddGrowableCol(3)
        fgSizer24.SetFlexibleDirection(wx.BOTH)
        fgSizer24.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer24.Add(self.m_staticText9, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_textCtrl6 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       0 | wx.SUNKEN_BORDER)
        fgSizer24.Add(self.m_textCtrl6, 0, wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT, 5)

        self.m_staticText11 = wx.StaticText(self, wx.ID_ANY, u"=", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText11.Wrap(-1)
        fgSizer24.Add(self.m_staticText11, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_textCtrl8 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer24.Add(self.m_textCtrl8, 0, wx.EXPAND | wx.TOP | wx.RIGHT, 5)

        self.m_checkBox86 = wx.CheckBox(self, wx.ID_ANY, u"&implement", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox86.SetValue(True)
        fgSizer24.Add(self.m_checkBox86, 0, wx.ALL, 5)

        fgSizer23.Add(fgSizer24, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"notes"), wx.VERTICAL)

        self.m_richText1 = wx.richtext.RichTextCtrl(sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer6.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer23.Add(sbSizer6, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer23.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer23)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class EditDecoratorsDialogBase
###########################################################################

class EditDecoratorsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Edit decorators", pos=wx.DefaultPosition,
                           size=wx.Size(503, 376), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        # self.SetExtraStyle(self.GetExtraStyle() | wx.WS_EX_BLOCK_EVENTS )

        fgSizer229 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer229.AddGrowableCol(0)
        fgSizer229.AddGrowableRow(0)
        fgSizer229.SetFlexibleDirection(wx.BOTH)
        fgSizer229.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer239 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer239.AddGrowableCol(0)
        fgSizer239.AddGrowableRow(0)
        fgSizer239.SetFlexibleDirection(wx.BOTH)
        fgSizer239.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_listCtrl6 = wx.ListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                       wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_SMALL_ICON)
        fgSizer239.Add(self.m_listCtrl6, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer240 = wx.FlexGridSizer(6, 1, 0, 0)
        fgSizer240.AddGrowableCol(0)
        fgSizer240.AddGrowableRow(5)
        fgSizer240.SetFlexibleDirection(wx.BOTH)
        fgSizer240.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton64 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(u"gtk-add", wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer240.Add(self.m_bpButton64, 0, wx.ALL | wx.EXPAND, 5)

        self.m_bpButton65 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(u"gtk-edit", wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_bpButton65.Enable(False)

        fgSizer240.Add(self.m_bpButton65, 0, wx.ALL, 5)

        self.m_bpButton66 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(u"gtk-delete", wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_bpButton66.Enable(False)

        fgSizer240.Add(self.m_bpButton66, 0, wx.ALL, 5)

        self.m_bpButton67 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_bpButton67.Enable(False)

        fgSizer240.Add(self.m_bpButton67, 0, wx.ALL, 5)

        self.m_bpButton68 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_bpButton68.Enable(False)

        fgSizer240.Add(self.m_bpButton68, 0, wx.ALL, 5)

        fgSizer240.Add((40, 0), 1, wx.EXPAND, 5)

        fgSizer239.Add(fgSizer240, 1, wx.EXPAND, 5)

        fgSizer229.Add(fgSizer239, 1, wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)
        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer229.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer229)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_listCtrl6.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.OnUnselectDecorator)
        self.m_listCtrl6.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnSelectDecorator)
        self.m_bpButton64.Bind(wx.EVT_BUTTON, self.OnAddDecorator)
        self.m_bpButton65.Bind(wx.EVT_BUTTON, self.OnEditDecorator)
        self.m_bpButton66.Bind(wx.EVT_BUTTON, self.OnDeleteDecorator)
        self.m_bpButton67.Bind(wx.EVT_BUTTON, self.OnDecoratorUp)
        self.m_bpButton68.Bind(wx.EVT_BUTTON, self.OnDecoratorDown)
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnUnselectDecorator(self, event):
        event.Skip()

    def OnSelectDecorator(self, event):
        event.Skip()

    def OnAddDecorator(self, event):
        event.Skip()

    def OnEditDecorator(self, event):
        event.Skip()

    def OnDeleteDecorator(self, event):
        event.Skip()

    def OnDecoratorUp(self, event):
        event.Skip()

    def OnDecoratorDown(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class DecoratorDialogBase
###########################################################################

class DecoratorDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New decorator", pos=wx.DefaultPosition,
                           size=wx.Size(463, 237), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        # self.SetExtraStyle(self.GetExtraStyle() | wx.WS_EX_BLOCK_EVENTS )

        fgSizer229 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer229.AddGrowableCol(0)
        fgSizer229.AddGrowableRow(0)
        fgSizer229.SetFlexibleDirection(wx.BOTH)
        fgSizer229.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer239 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer239.AddGrowableCol(0)
        fgSizer239.AddGrowableRow(1)
        fgSizer239.SetFlexibleDirection(wx.BOTH)
        fgSizer239.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer250 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer250.AddGrowableCol(1)
        fgSizer250.SetFlexibleDirection(wx.BOTH)
        fgSizer250.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText111 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText111.Wrap(-1)
        fgSizer250.Add(self.m_staticText111, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl79 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer250.Add(self.m_textCtrl79, 0, wx.EXPAND | wx.ALL, 5)

        fgSizer239.Add(fgSizer250, 1, wx.EXPAND, 5)

        sbSizer56 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"declaration"), wx.VERTICAL)

        self.m_textCtrl80 = wx.TextCtrl(sbSizer56.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize, wx.TE_MULTILINE)
        sbSizer56.Add(self.m_textCtrl80, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer239.Add(sbSizer56, 1, wx.EXPAND, 5)

        fgSizer229.Add(fgSizer239, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)
        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)
        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();
        m_sdbSizer12.SetMinSize(wx.Size(-1, 40))

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer229.Add(fgSizer159, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(fgSizer229)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()
