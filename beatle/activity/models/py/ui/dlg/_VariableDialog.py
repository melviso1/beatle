"""Subclass of NewVariable, which is generated by wxFormBuilder."""

import re
import wx

from beatle.lib import wxx
from .._base import VariableDialogBase


class VariableDialog(VariableDialogBase):
    """
    This dialog allows to setup a non-member
    variable.
    """
    @wxx.SetInfo(__doc__)
    def __init__(self, parent, container):
        """Dialog initialization"""
        from beatle.app import resources as rc
        super(VariableDialog, self).__init__(parent)
        self._name = ''
        self._value = None
        self._note = ''
        self._container = container
        self.m_textCtrl6.SetFocus()
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap("py_variable"))
        self.SetIcon(icon)

    def copy_attributes(self, variable):
        """Get the attributes"""
        variable.name = self._name
        variable.value = self._value
        variable.note = self._note

    def set_attributes(self, member):
        """Set the attributes"""
        self.m_textCtrl6.SetValue(member.name)
        self.m_textCtrl8.SetValue(member.value)
        self.m_richText1.SetValue(member.note)
        self.SetTitle("Edit variable")

    def validate(self):
        """Dialog validation"""
        self._name = self.m_textCtrl6.GetValue()
        if len(self._name) == 0:
            wx.MessageBox("Variable name must not be empty", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        if re.match("^[A-Za-z_][0-9A-Za-z_]*$", self._name) is None:
            wx.MessageBox("Variable name contains invalid characters", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self._value = self.m_textCtrl8.GetValue()
        self._note = self.m_richText1.GetValue()
        return True

    def get_kwargs(self):
        """return arguments for object instance"""
        return {'parent': self._container, 'name': self._name,
                'value': self._value, 'note': self._note}

    def on_ok(self, event):
        """ok event handler"""
        if self.validate():
            self.EndModal(wx.ID_OK)

    def on_cancel(self, event):
        """cancel event handler"""
        self.EndModal(wx.ID_CANCEL)

