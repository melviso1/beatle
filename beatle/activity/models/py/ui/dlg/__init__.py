
from ._PackageDialog import PackageDialog
from ._EditDecoratorsDialog import EditDecoratorsDialog
from ._DecoratorDialog import DecoratorDialog
from ._ArgumentDialog import ArgumentDialog
from ._ClassDialog import ClassDialog
from ._FunctionDialog import FunctionDialog
from ._ImportDialog import ImportDialog
from ._InheritanceDialog import InheritanceDialog
from ._MemberDialog import MemberDialog
from ._MemberMethodDialog import MemberMethodDialog
from ._ModuleDialog import ModuleDialog
from ._VariableDialog import VariableDialog


def init():
    """Initialize this activity model ui"""
    from beatle import model
    from beatle.app.ui.dlg import FolderDialog
    from beatle.lib.decorators import with_python_export
    associations = {
        model.py.Folder: (FolderDialog, 'edit folder {0}'),
        model.py.Class: (ClassDialog, 'edit python class {0}'),
        model.py.Argument: (ArgumentDialog, 'edit python method argument {0}'),
        model.py.Inheritance: (InheritanceDialog, 'edit python inheritance {0}'),
        model.py.MemberData: (MemberDialog, 'edit python class member {0}'),
        model.py.Data: (VariableDialog, 'edit python variable {0}'),
        model.py.InitMethod: (MemberMethodDialog, 'edit python __init__ method'),
        model.py.MemberMethod: (MemberMethodDialog, 'edit python method {0}'),
        model.py.Function: (FunctionDialog, 'edit python function {0}'),
        model.py.Module: (ModuleDialog, 'edit python module {0}'),
        model.py.Package: (PackageDialog, 'edit python package {0}'),
        model.py.Import: (ImportDialog, 'edit python import {0}'),
        model.py.Decorator: (DecoratorDialog, 'edit python decorator {0}'),
    }
    from beatle.app.ui.tools import DIALOG_DICT
    DIALOG_DICT.update(associations)

