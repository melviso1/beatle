# -*- coding: utf-8 -*-

import wx.stc
from wx import aui

from beatle import local_path
from beatle.lib.api import context
from beatle.app.ui import pane, dlg
from beatle.lib import wxx
from beatle.lib.tran import TransactionalMethod
from beatle.lib.handlers import identifier
from beatle.lib.decorators import with_python_export

from .._base import MethodPaneBase


class MethodPane(pane.BasePane, MethodPaneBase):
    """Implements method pane editor"""
    # command ids:
    _delete = identifier("ID_DELETE")
    _undo = identifier("ID_UNDO")
    _redo = identifier("ID_REDO")
    _copy = identifier("ID_COPY")
    _cut = identifier("ID_CUT")
    _paste = identifier("ID_PASTE")
    _save = identifier("ID_SAVE")
    _close_active_pane = identifier('ID_CLOSE_ACTIVE_PANE')
    _toggleFullId = identifier('ID_FULL_SCREEN')
    _navigatorId = identifier('ID_NAVIGATOR')

    def _create_toolbars(self):
        """Create custom toolbar"""
        self.m_localToolbar = aui.AuiToolBar(self, wx.ID_ANY, wx.DefaultPosition,
            wx.Size(16,16), style=aui.AUI_TB_HORZ_LAYOUT|aui.AUI_TB_PLAIN_BACKGROUND)
        self.m_save = self.m_localToolbar.AddTool(self._save,
                                                  u"save code",
                                                  wx.ArtProvider.GetBitmap(wx.ART_FILE_SAVE,
                                                                           wx.ART_TOOLBAR),
            wx.NullBitmap, wx.ITEM_NORMAL, u"Save", wx.EmptyString, None)
        self.m_localToolbar.AddSeparator()
        self.m_undo = self.m_localToolbar.AddTool(self._undo,
                                                  u"tool",
                                                  wx.ArtProvider.GetBitmap(wx.ART_UNDO, wx.ART_TOOLBAR),
                                                  wx.NullBitmap, wx.ITEM_NORMAL,
                                                  u"Undo", wx.EmptyString, None)
        self.m_redo = self.m_localToolbar.AddTool(self._redo, u"tool",
                                                  wx.ArtProvider.GetBitmap(wx.ART_REDO, wx.ART_TOOLBAR),
                                                  wx.NullBitmap, wx.ITEM_NORMAL,
                                                  u"Redo", wx.EmptyString, None)
        self.m_localToolbar.AddSeparator()
        self.m_copy = self.m_localToolbar.AddTool(self._copy, u"copy",
                                                  wx.ArtProvider.GetBitmap(wx.ART_COPY, wx.ART_TOOLBAR),
                                                  wx.NullBitmap, wx.ITEM_NORMAL,
                                                  u"Copy", wx.EmptyString, None)
        self.m_cut = self.m_localToolbar.AddTool(self._cut, u"cut",
                                                 wx.ArtProvider.GetBitmap(wx.ART_CUT, wx.ART_TOOLBAR),
                                                 wx.NullBitmap, wx.ITEM_NORMAL, u"Cut", wx.EmptyString, None)
        self.m_paste = self.m_localToolbar.AddTool(self._paste, u"paste",
                                                   wx.ArtProvider.GetBitmap(wx.ART_PASTE, wx.ART_TOOLBAR),
                                                   wx.NullBitmap, wx.ITEM_NORMAL, u"Paste", wx.EmptyString, None)
        self.m_localToolbar.AddSeparator()
        self.m_delete = self.m_localToolbar.AddTool(self._delete, u"delete",
                                                    wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_TOOLBAR),
                                                    wx.NullBitmap, wx.ITEM_NORMAL, u"Delete", wx.EmptyString, None)
        self.m_localToolbar.AddSeparator()
        self.m_fullScreen = self.m_localToolbar.AddTool(self._toggleFullId, u"tool",
                                                        wx.ArtProvider.GetBitmap(u"gtk-fullscreen", wx.ART_TOOLBAR),
                                                        wx.NullBitmap, wx.ITEM_NORMAL, u"Toggle fullscreen",
                                                        wx.EmptyString, None)
        self.m_localToolbar.AddSeparator()
        self.m_navigator = self.m_localToolbar.AddTool(self._navigatorId, u"tool",
                                                       wx.Bitmap(local_path("app/res/method.xpm"), wx.BITMAP_TYPE_ANY),
                                                       wx.NullBitmap, wx.ITEM_NORMAL, u"insert call",
                                                       wx.EmptyString, None)
        self.m_localToolbar.Realize()
        self.m_toolbarSizer.Add(self.m_localToolbar, 0, wx.ALL, 5)
        self.Layout()
        # Connect Events
        self.m_editor.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_editor.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)

    def __init__(self, parent, mainframe, method):
        """Intialization of method editor"""
        from ...handlers import EditorHandler
        self._mainframe = mainframe
        self._object = method
        self._notebook = parent
        read_only = getattr(method, '_read_only_content', method.read_only)
        self._editorArgs = {
            'language': 'python',
            'handler': EditorHandler(
                text=method.content,
                use_bookmarks=False,
                use_breakpoints=False,
                read_only=read_only),
        }
        super(MethodPane, self).__init__(parent)
        self.append_accelerators(
            wx.AcceleratorEntry(wx.ACCEL_ALT | wx.ACCEL_CTRL, ord('F'), self._toggleFullId),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, ord('W'), self._close_active_pane),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, ord('S'), self._save),
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_DELETE, self._delete)
        )
        """
        if method.note:
            self.m_editor.AnnotationSetVisible(wx.stc.STC_ANNOTATION_BOXED)
            self.m_editor.AnnotationSetText(0, method._note)"""
        #self.m_editor.SendMsg(2540, wp="Prueba de anotacion", lp=long(0))
        #self.m_editor.SendMsg(2548, wp=1, lp=long(0))
        self.Layout()
        # self._create_toolbars()

        self.Bind(wx.stc.EVT_STC_CHANGE, self.on_editor_change, id=self.m_editor.GetId())

        # self.Bind(EVT_STC_STYLENEEDED, self.on_style_needed,
        # id=self.m_editor.GetId())
        self.bind_events()
        self.m_editor.popup_handler = self
        self.m_editor.context = self._object
        self._fullScreen = None
        self.bind_accelerators()

    def bind_events(self):
        # bind some messages to this frame
        frame = context.get_frame()
        self.bind_special(wx.EVT_MENU, lambda x: frame.do_close_current_doc_pane(), id=self._close_active_pane)
        self.Bind(wx.EVT_MENU, self.on_delete_code, id=self._delete)
        self.bind_special(wx.EVT_MENU, self.on_undo_code, id=self._undo)
        self.bind_special(wx.EVT_MENU, self.on_redo_code, id=self._redo)
        self.bind_special(wx.EVT_MENU, self.on_copy_code, id=self._copy)
        self.bind_special(wx.EVT_MENU, self.on_paste_code, id=self._paste)
        self.bind_special(wx.EVT_MENU, self.on_cut_code, id=self._cut)
        self.bind_special(wx.EVT_MENU, self.on_save_code, id=self._save)
        self.bind_special(wx.EVT_MENU, self.on_full_screen, id=self._toggleFullId)
        self.bind_special(wx.EVT_MENU, self.on_navigator, id=self._navigatorId)

        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateDeleteCode, id=self._delete)
        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateUndoCode, id=self._undo)
        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateRedoCode, id=self._redo)
        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateCopyCode, id=self._copy)
        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdatePasteCode, id=self._paste)
        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateCutCode, id=self._cut)
        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_save_code, id=self._save)
        super(MethodPane, self).bind_events()

    def update_font(self, font_info):
        self._editorArgs['handler'].update_font(font_info, self.m_editor)

    def Refresh(self):
        """Update editor from external changes (like undo/redo)"""
        if not self.m_editor.modified and self.m_editor.GetText() != self._object.content:
            self.m_editor.ReplaceValue(self._object.content, self._object.read_only)

    def pre_delete(self):
        """Remove toolbar first in order to avoid gtk-collision at close"""
        super(MethodPane, self).pre_delete()

    def popup(self, editor, text=None):
        """Shows a custom popup"""
        pass
        # if text:
        #     # in the future, we need to show a complete floating dialog here
        #     # we receive "this->" or "object." here ..
        #     pass
        # else:
        #     menu.Append(self._mainframe.editProperties.GetId(),
        #     "test menu", "Edit relation properties")
        # pos = editor.GetCurrentPos()
        # p = editor.PointFromPosition(pos)
        # menu.Popup(p, owner=editor)

    def on_navigator(self, event):
        """Navigate throug the class structure"""
        pos = self.m_editor.GetCurrentPos()
        p = self.m_editor.PointFromPosition(pos)
        p = self.m_editor.ClientToScreen(p)
        dialog = dlg.CodeNavigator(self, self._object)
        dialog.Move(p)
        if dialog.ShowModal() != wx.ID_OK:
            return
        pass

    def on_undo_code(self, event):
        """Edit undo"""
        self.m_editor.Undo()

    def on_redo_code(self, event):
        """Edit redo"""
        self.m_editor.Redo()

    def OnGetFocus(self, event):
        """Handle GetFocus event"""
        event.Skip()  # This ensures editor cursor handling
        super(MethodPane, self).OnGetFocus(event)

    def OnKillFocus(self, event):
        """Handle kill focus event"""
        event.Skip()  # This ensures editor cursor handling
        super(MethodPane, self).OnKillFocus(event)

    def OnUpdatePasteCode(self, event):
        """Edit paste?"""
        try:
            if self.m_editor.CanPaste():
                event.Enable(True)
                return
        except:
            event.Enable(False)

    def OnUpdateCopyCode(self, event):
        """Edit copy?"""
        (b, e) = self.m_editor.GetSelection()
        event.Enable(b != e)

    def OnUpdateCutCode(self, event):
        """Edit cut?"""
        if self._object.read_only:
            event.Enable(False)
        else:
            (b, e) = self.m_editor.GetSelection()
            event.Enable(b != e)

    def OnUpdateRedoCode(self, event):
        """Edit redo?"""
        if self.m_editor.CanRedo():
            event.SetText("Redo edit")
            event.Enable(True)
        else:
            event.SetText("Can't redo")
            event.Enable(False)

    def OnUpdateUndoCode(self, event):
        """Edit undo?"""
        if self.m_editor.CanUndo():
            event.SetText("Undo edit")
            event.Enable(True)
        else:
            event.SetText("Can't undo")
            event.Enable(False)

    def OnUpdateDeleteCode(self, event):
        """Edit delete?"""
        event.Enable(not self._object.read_only)

    def Enabled(self, event):
        """Joker event"""
        event.Enable(True)

    def Disabled(self, event):
        """Joker event"""
        event.Enable(False)

    def update_modified_title(self):
        """Update the tile after modifications"""
        if not self._fullScreen:
            i = self._notebook.GetPageIndex(self)
            if i < 0:
                # print("Alert: modified editor not booked")
                return
            s = self._notebook.GetPageText(i)
            m = '[modified] '
            if self.m_editor.modified:
                if s.find(m) == wx.NOT_FOUND:
                    self._notebook.SetPageText(i, m + s)
            else:
                if s.find(m) != wx.NOT_FOUND:
                    s = s.replace(m, '')
                    self._notebook.SetPageText(i, s)

    def on_editor_change(self, event):
        """Called when editor status change"""
        if not self._fullScreen:
            self.update_modified_title()

    @TransactionalMethod('edit method')
    def commit(self):
        """Update document"""
        text = self.m_editor.GetText()
        element = self._object
        if text == element.content:
            return False
        element.save_state()
        element._content = text
        element.project.modified = True
        unit = element.inner_module or element.inner_package
        if unit is not None:
            unit.export_code_files()
        return True

    def on_delete_code(self, event):
        """Handle delete key"""
        (b, e) = self.m_editor.GetSelection()
        if b == e:
            self.m_editor.CharRight()
        self.m_editor.DeleteBack()

    def on_copy_code(self, event):
        """Handle a copy event"""
        self.m_editor.Copy()

    def on_paste_code(self, event):
        """Handle a paste event"""
        self.m_editor.Paste()

    def on_cut_code(self, event):
        """Handle a paste event"""
        self.m_editor.Cut()

    def on_update_save_code(self, event):
        """Handles update event"""
        event.Enable(self.m_editor.modified)

    def on_save_code(self, event):
        """Handles save event"""
        self.commit()
        self.m_editor.ResetModified()
        self.update_modified_title()

    def on_full_screen(self, event):
        """set the pane in fullscreen mode"""
        from beatle.app.ui.wnd import FullScreenWindow
        if self._fullScreen:
            self._fullScreen.leave()
            self.update_modified_title()
            self._fullScreen.Destroy()
            self._fullScreen = None
        else:
            self._fullScreen = FullScreenWindow(self)
        wx.SafeYield()
        self.m_editor.SetFocus()

    def on_style_needed(self, event):
        """Handle scintilla events"""
        wnd = self.m_editor
        pos = wnd.GetCurrentPos()
        if pos == 0:
            return
        if wnd.GetCharAt(pos - 1) == ':':
            line_number = wnd.GetCurrentLine()
            indentation = wnd.GetLineIndentation(line_number - 1)
            wnd.SetLineIndentation(line_number, indentation)

    def goto_line(self, line, select=False):
        """Called for goto to line"""
        self.m_editor.goto_line(line, select)
