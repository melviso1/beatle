# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Aug 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.dataview
from beatle.lib import wxx
from beatle.lib.wxx.agw import TR_WIN_BUTTONS
import wx.richtext

# special import for beatle development
from beatle.lib.handlers import Identifiers
###########################################################################
## Class DatabasePane
###########################################################################

class DatabasePane ( wx.Panel ):

    def __init__(self, parent):
        wx.Panel.__init__ (self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 531,360 ), style = wx.NO_BORDER|wx.TAB_TRAVERSAL )

        self.SetExtraStyle( wx.WS_EX_BLOCK_EVENTS )

        fgSizer190 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer190.AddGrowableCol( 0 )
        fgSizer190.AddGrowableRow( 0 )
        fgSizer190.SetFlexibleDirection( wx.BOTH )
        fgSizer190.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_splitter3 = wx.SplitterWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_BORDER|wx.NO_BORDER )
        # self.m_splitter3.SetSashSize( 12 )  -- deprecated ??
        self.m_splitter3.Bind( wx.EVT_IDLE, self.m_splitter3OnIdle )

        self.m_scrolledWindow1 = wx.ScrolledWindow(self.m_splitter3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.DOUBLE_BORDER|wx.HSCROLL|wx.NO_BORDER|wx.VSCROLL )
        self.m_scrolledWindow1.SetScrollRate( 5, 5 )
        self.m_scrolledWindow1.SetMinSize( wx.Size( 0,0 ) )

        fgSizer191 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer191.AddGrowableCol( 0 )
        fgSizer191.AddGrowableRow( 0 )
        fgSizer191.SetFlexibleDirection( wx.BOTH )
        fgSizer191.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_grid = wx.dataview.DataViewListCtrl(self.m_scrolledWindow1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.dataview.DV_HORIZ_RULES|wx.dataview.DV_MULTIPLE|wx.dataview.DV_ROW_LINES|wx.dataview.DV_VERT_RULES|wx.NO_BORDER|wx.WANTS_CHARS )
        fgSizer191.Add(self.m_grid, 0, wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )


        self.m_scrolledWindow1.SetSizer( fgSizer191 )
        self.m_scrolledWindow1.Layout()
        fgSizer191.Fit(self.m_scrolledWindow1 )
        self.m_panel16 = wx.Panel(self.m_splitter3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CLIP_CHILDREN|wx.SUNKEN_BORDER|wx.WANTS_CHARS )
        self.m_panel16.SetExtraStyle( wx.WS_EX_BLOCK_EVENTS )

        fgSizer195 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer195.AddGrowableCol( 0 )
        fgSizer195.AddGrowableRow( 0 )
        fgSizer195.SetFlexibleDirection( wx.BOTH )
        fgSizer195.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        from beatle.app.ui.ctrl import Editor
        self.m_editor = Editor(self.m_panel16, **self._editorArgs)
        fgSizer195.Add(self.m_editor, 0, wx.ALL|wx.EXPAND, 5 )


        self.m_panel16.SetSizer( fgSizer195 )
        self.m_panel16.Layout()
        fgSizer195.Fit(self.m_panel16 )
        self.m_splitter3.SplitHorizontally(self.m_scrolledWindow1, self.m_panel16, 179 )
        fgSizer190.Add(self.m_splitter3, 1, wx.ALL|wx.EXPAND, 5 )


        self.SetSizer( fgSizer190 )
        self.Layout()

    def __del__(self ):
        pass

    def m_splitter3OnIdle(self, event ):
        self.m_splitter3.SetSashPosition( 179 )
        self.m_splitter3.Unbind( wx.EVT_IDLE )


###########################################################################
## Class ModelsView
###########################################################################

class ModelsView ( wx.Panel ):

    def __init__(self, parent):
        wx.Panel.__init__ (self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ),
                           style= wx.NO_BORDER | wx.TAB_TRAVERSAL | wx.WANTS_CHARS)

        self.SetExtraStyle(wx.WS_EX_PROCESS_UI_UPDATES)
        fgSizer2 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer2.AddGrowableCol( 0 )
        fgSizer2.AddGrowableRow( 0 )
        fgSizer2.SetFlexibleDirection( wx.BOTH )
        fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_tree = wxx.TreeCtrl(
            self, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
            style=wx.TR_LINES_AT_ROOT | wx.TR_HAS_BUTTONS |
                  TR_WIN_BUTTONS | wx.TR_HIDE_ROOT | wx.TR_SINGLE | wx.BORDER_NONE)
        fgSizer2.Add(self.m_tree, 0, wx.EXPAND, 5)


        self.SetSizer( fgSizer2 )
        self.Layout()

    def __del__(self ):
        pass


###########################################################################
## Class GenerateSources
###########################################################################

class GenerateSources ( wxx.Dialog ):

    def __init__(self, parent):
        wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"Generate sources ...", pos = wx.DefaultPosition, size = wx.Size( 629,456 ), style = wx.CLOSE_BOX|wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        fgSizer71 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer71.AddGrowableCol( 0 )
        fgSizer71.AddGrowableRow( 0 )
        fgSizer71.SetFlexibleDirection( wx.BOTH )
        fgSizer71.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_ALL )

        fgSizer72 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer72.AddGrowableCol( 0 )
        fgSizer72.AddGrowableCol( 1 )
        fgSizer72.AddGrowableRow( 0 )
        fgSizer72.SetFlexibleDirection( wx.BOTH )
        fgSizer72.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        fgSizer75 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer75.AddGrowableCol( 0 )
        fgSizer75.AddGrowableRow( 1 )
        fgSizer75.SetFlexibleDirection( wx.BOTH )
        fgSizer75.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        fgSizer74 = wx.FlexGridSizer( 3, 2, 0, 0 )
        fgSizer74.AddGrowableCol( 0 )
        fgSizer74.AddGrowableCol( 1 )
        fgSizer74.SetFlexibleDirection( wx.BOTH )
        fgSizer74.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_staticText37 = wx.StaticText(self, wx.ID_ANY, u"Author", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText37.Wrap( -1 )
        fgSizer74.Add(self.m_staticText37, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )

        self.m_author = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer74.Add(self.m_author, 0, wx.ALL, 5 )

        self.m_staticText38 = wx.StaticText(self, wx.ID_ANY, u"Date", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText38.Wrap( -1 )
        fgSizer74.Add(self.m_staticText38, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_date = wx.adv.DatePickerCtrl(self, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.adv.DP_DEFAULT )
        fgSizer74.Add(self.m_date, 0, wx.ALL, 5 )

        fgSizer75.Add( fgSizer74, 1, wx.EXPAND|wx.TOP, 5 )

        sbSizer20 = wx.StaticBoxSizer( wx.StaticBox(self, wx.ID_ANY, u"Comments" ), wx.VERTICAL )

        self.m_richText15 = wx.richtext.RichTextCtrl( sbSizer20.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0|wx.HSCROLL|wx.VSCROLL|wx.WANTS_CHARS )
        sbSizer20.Add(self.m_richText15, 1, wx.EXPAND |wx.ALL, 5 )


        fgSizer75.Add( sbSizer20, 1, wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )


        fgSizer72.Add( fgSizer75, 1, wx.EXPAND, 5 )

        self.m_report = wx.ListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_ICON|wx.LC_REPORT )
        fgSizer72.Add(self.m_report, 0, wx.ALL|wx.EXPAND, 5 )


        fgSizer71.Add( fgSizer72, 0, wx.EXPAND|wx.TOP, 5 )

        fgSizer73 = wx.FlexGridSizer( 1, 4, 0, 0 )
        fgSizer73.AddGrowableCol( 0 )
        fgSizer73.SetFlexibleDirection( wx.BOTH )
        fgSizer73.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
        fgSizer73.Add(self.m_info, 0, wx.ALL, 5 )

        self.m_overwrite = wx.CheckBox(self, wx.ID_ANY, u"Overwrite &all", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer73.Add(self.m_overwrite, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_button23 = wx.Button(self, wx.ID_ANY, u"&Generate", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer73.Add(self.m_button23, 0, wx.ALL, 5 )

        self.m_button24 = wx.Button(self, wx.ID_CANCEL, u"&Close", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer73.Add(self.m_button24, 0, wx.ALL, 5 )


        fgSizer71.Add( fgSizer73, 1, wx.EXPAND, 5 )


        self.SetSizer( fgSizer71 )
        self.Layout()

        self.Centre( wx.BOTH )

        # Connect Events
        self.m_button23.Bind( wx.EVT_BUTTON, self.OnGenerate)
        self.m_button24.Bind( wx.EVT_BUTTON, self.on_close)

    def __del__(self ):
        pass


    # Virtual event handlers, overide them in your derived class
    def OnGenerate(self, event ):
        event.Skip()


###########################################################################
## Class NewFile
###########################################################################

class NewFile ( wxx.Dialog ):

    def __init__(self, parent):
        wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"New file", pos = wx.DefaultPosition, size = wx.Size( 274,144 ), style = wx.DEFAULT_DIALOG_STYLE )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        fgSizer125 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer125.AddGrowableCol( 0 )
        fgSizer125.AddGrowableRow( 0 )
        fgSizer125.SetFlexibleDirection( wx.BOTH )
        fgSizer125.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        fgSizer126 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer126.AddGrowableCol( 1 )
        fgSizer126.SetFlexibleDirection( wx.BOTH )
        fgSizer126.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_ALL )

        self.m_staticText85 = wx.StaticText(self, wx.ID_ANY, u"File name", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText85.Wrap( -1 )
        fgSizer126.Add(self.m_staticText85, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textCtrl56 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrl56.SetMinSize( wx.Size( 150,-1 ) )

        fgSizer126.Add(self.m_textCtrl56, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )


        fgSizer125.Add( fgSizer126, 0, wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        fgSizer158 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer158.AddGrowableCol( 1 )
        fgSizer158.SetFlexibleDirection( wx.BOTH )
        fgSizer158.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
        fgSizer158.Add(self.m_info, 0, wx.ALL, 5 )

        m_sdbSizer10 = wx.StdDialogButtonSizer()
        self.m_sdbSizer10OK = wx.Button(self, wx.ID_OK )
        m_sdbSizer10.AddButton(self.m_sdbSizer10OK )
        self.m_sdbSizer10Cancel = wx.Button(self, wx.ID_CANCEL )
        m_sdbSizer10.AddButton(self.m_sdbSizer10Cancel )
        m_sdbSizer10.Realize();

        fgSizer158.Add( m_sdbSizer10, 1, wx.EXPAND, 5 )


        fgSizer125.Add( fgSizer158, 1, wx.EXPAND, 5 )


        self.SetSizer( fgSizer125 )
        self.Layout()

        self.Centre( wx.BOTH )

    def __del__(self ):
        pass


###########################################################################
## Class NewFolder
###########################################################################

class NewFolder ( wxx.Dialog ):

    def __init__(self, parent):
        wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"New folder", pos = wx.DefaultPosition, size = wx.Size( 423,390 ), style = wx.DEFAULT_DIALOG_STYLE )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        fgSizer6 = wx.FlexGridSizer( 3, 1, 0, 0 )
        fgSizer6.AddGrowableCol( 0 )
        fgSizer6.AddGrowableRow( 1 )
        fgSizer6.SetFlexibleDirection( wx.BOTH )
        fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        fgSizer7 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer7.AddGrowableCol( 1 )
        fgSizer7.SetFlexibleDirection( wx.BOTH )
        fgSizer7.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText4.Wrap( -1 )
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )


        fgSizer6.Add( fgSizer7, 0, wx.EXPAND|wx.ALL, 5 )

        sbSizer9 = wx.StaticBoxSizer( wx.StaticBox(self, wx.ID_ANY, u"Documentation" ), wx.VERTICAL )

        self.m_richText3 = wx.richtext.RichTextCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0|wx.HSCROLL|wx.SUNKEN_BORDER|wx.VSCROLL|wx.WANTS_CHARS )
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND|wx.ALL, 5 )


        fgSizer6.Add( sbSizer9, 1, wx.EXPAND, 5 )

        fgSizer162 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer162.AddGrowableCol( 1 )
        fgSizer162.SetFlexibleDirection( wx.BOTH )
        fgSizer162.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
        fgSizer162.Add(self.m_info, 0, wx.ALL, 5 )

        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK )
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK )
        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL )
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel )
        m_sdbSizer2.Realize();

        fgSizer162.Add( m_sdbSizer2, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )


        fgSizer6.Add( fgSizer162, 1, wx.EXPAND, 5 )


        self.SetSizer( fgSizer6 )
        self.Layout()

        self.Centre( wx.BOTH )

        # Connect Events
        self.m_sdbSizer2OK.Bind( wx.EVT_BUTTON, self.on_ok )

    def __del__(self ):
        pass


    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event ):
        event.Skip()


###########################################################################
## Class NewNote
###########################################################################

class NewNote ( wxx.Dialog ):

    def __init__(self, parent):
        wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"New note", pos = wx.DefaultPosition, size = wx.Size( 480,299 ), style = wx.DEFAULT_DIALOG_STYLE )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        fgSizer103 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer103.AddGrowableCol( 0 )
        fgSizer103.AddGrowableRow( 0 )
        fgSizer103.SetFlexibleDirection( wx.BOTH )
        fgSizer103.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        sbSizer30 = wx.StaticBoxSizer( wx.StaticBox(self, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

        self.m_text = wx.richtext.RichTextCtrl( sbSizer30.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
        sbSizer30.Add(self.m_text, 1, wx.EXPAND |wx.ALL, 5 )


        fgSizer103.Add( sbSizer30, 1, wx.EXPAND|wx.ALL, 5 )

        fgSizer156 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer156.AddGrowableCol( 1 )
        fgSizer156.SetFlexibleDirection( wx.BOTH )
        fgSizer156.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
        fgSizer156.Add(self.m_info, 0, wx.ALL, 5 )

        m_sdbSizer7 = wx.StdDialogButtonSizer()
        self.m_sdbSizer7OK = wx.Button(self, wx.ID_OK )
        m_sdbSizer7.AddButton(self.m_sdbSizer7OK )
        self.m_sdbSizer7Cancel = wx.Button(self, wx.ID_CANCEL )
        m_sdbSizer7.AddButton(self.m_sdbSizer7Cancel )
        m_sdbSizer7.Realize();

        fgSizer156.Add( m_sdbSizer7, 1, wx.EXPAND, 5 )


        fgSizer103.Add( fgSizer156, 1, wx.EXPAND, 5 )


        self.SetSizer( fgSizer103 )
        self.Layout()

        self.Centre( wx.BOTH )

        # Connect Events
        self.m_sdbSizer7OK.Bind( wx.EVT_BUTTON, self.on_ok )
        self.m_sdbSizer7Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)

    def __del__(self ):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event ):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()
