# -*- coding: utf-8 -*-

from . import dlg
from . import pane
from . import view


def init():
    """Initialize this activity model ui"""
    dlg.init()
