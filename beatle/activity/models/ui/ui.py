import wx

if "__WXMSW__" in wx.PlatformInfo:
    from .ui_win import *
else:
    from .ui_linux import *
