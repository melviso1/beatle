# -*- coding: utf-8 -*-

from .GenerateSources import GenerateSourcesDialog
from .Note import NoteDialog
#from ._ProjectDialog import ProjectDialog
from ._project_dialog import ProjectDialog


def init():
    """Initialize this activity model ui"""
    from beatle import model
    associations = {
#       model.Project: (ProjectDialog, 'edit project {0}'),
        model.Project: (ProjectDialog, 'edit project {0}'),
    }
    from beatle.app.ui.tools import DIALOG_DICT
    DIALOG_DICT.update(associations)

