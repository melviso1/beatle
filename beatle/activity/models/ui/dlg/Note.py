"""Subclass of NewNote, which is generated by wxFormBuilder."""

import wx

from beatle.lib import wxx
from beatle.activity.models.ui import ui


# Implementing NewNote
class NoteDialog(ui.NewNote):
    """
    This is the note dialog, that allows to setup
    a class diagram note.
    """
    @wxx.SetInfo(__doc__)
    def __init__(self, parent, container=None):
        """Initialize dialog"""
        super(NoteDialog, self).__init__(parent)

    # Handlers for NoteDialog events.

    def validate(self):
        """Check dialog"""
        self._note = self.m_text.GetValue()
        return True

    def copy_attributes(self, note):
        """Transfer attributes to folder"""
        note.note = self._note

    def set_attributes(self, note):
        """Transfer attributes from existing constructor"""
        self._note = note.note
        self.m_text.SetValue(self._note)
        self.SetTitle("Edit note")

    def get_kwargs(self):
        """return arguments suitable for object instance"""
        return {'note': self._note}

    def on_ok(self, event):
        """Handles Ok button in dialog"""
        if self.validate():
            self.EndModal(wx.ID_OK)

    def on_cancel(self, event):
        """Handles Cancel button in dialog"""
        self.EndModal(wx.ID_CANCEL)
