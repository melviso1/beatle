# -*- coding: utf-8 -*-
"""
Module containing classes definitions
"""

import wx
import beatle.lib.handlers
import wx.aui
import wx.adv
from beatle.lib import wxx
from beatle.lib.api import context
from beatle.lib.api import volatile
import os
from beatle.lib.api import get_licenses
from beatle.lib.api import add_license


class ProjectDialog(wxx.Dialog):
    """
    This dialog allows to create a new project.
    This dialog is the start point for a new project to be created. \nYou  may select the project type and set his common options. \nThis type can't be changed once the project was created.
    """
    def on_changing_project_nature(self, event):
        """
        Event handler
        """
        if self._veto_change_project_nature:
            event.Veto()
        else:
            event.Skip()

    def on_goto_cpp_templates(self, event):
        """
        Event handler
        """
        self._cpp_project_pages.SetSelection(1)

    def on_goto_cpp_basic_details(self, event):
        """
        Event handler
        """
        self._cpp_project_pages.SetSelection(0)

    def on_init_dialog(self, event):
        """
        Event handler
        """
        from beatle.app import resources as rc
        wk = self._container and self._container.workspace
        if wk is not None:
            self._base = wk.dir
            self._base_directory.Enable(False)
        else:
            self._base = wx.StandardPaths.Get().GetDocumentsDir()
            if len(self._original_project_name) == 0:
                self._base_directory.Enable(True)
        if len(self._original_project_name) == 0:
            self._base_directory.SetPath(self._base)    
        self._project_name.SetFocus()
        icon = wx.Icon()
        if self._nature in [None, 'c++']:
            icon.CopyFromBitmap(rc.get_bitmap('cppproject'))
        elif self._nature == 'python':
            icon.CopyFromBitmap(rc.get_bitmap('python'))
        else:
            icon.CopyFromBitmap(rc.get_bitmap('databases'))
        if not self._licenses_ready:    
            self._license_type.Clear()
            self._license_type.Append(list(get_licenses().keys()))
            self._license_type.SetSelection(0)
            self._licenses_ready = True
        
        self.SetIcon(icon)
        config = beatle.lib.api.context.get_config()
        self._author = config.Read('/last_session/last_author', '')
        self._project_author.SetValue(self._author) 

    def on_cancel(self, event):
        """
        Event handler
        """
        volatile.set(type(self), self.GetRect())
        self.EndModal(wx.ID_CANCEL)

    def on_ok(self, event):
        """
        Event handler
        """
        volatile.set(type(self), self.GetRect())
        if self.validate():
            config = beatle.lib.api.context.get_config()
            config.Write('/last_session/last_author', self._author)
            self.EndModal(wx.ID_OK)

    def on_change_project_name(self, event):
        """
        Event handler
        """
        if self._auto_dir and self._base:
            name = self._project_name.GetValue()
            self._base_directory.SetPath(os.path.join(self._base, name))
            if self._use_cpp_master_include.IsChecked():
                # replace master include
                name = name.replace(" ", "_")
                name += ".h"
                self._cpp_master_include_file.SetValue(name)

    def on_project_nature_changed(self, event):
        """
        Event handler
        """
        from beatle.app import resources as rc
        selection_index = event.GetSelection()
        icon = wx.Icon()
        if selection_index == 0:
            icon.CopyFromBitmap(rc.get_bitmap('cppproject'))
        elif selection_index == 1:
            icon.CopyFromBitmap(rc.get_bitmap('python'))
        elif selection_index == 2:        
            icon.CopyFromBitmap(rc.get_bitmap('databases'))
        self.SetIcon(icon)

    def on_use_license(self, event):
        """
        Event handler
        """
        # write here the event handler
        status = self._use_license.GetValue()
        self._license_type.Enable(status)
        self._new_license.Enable(status)

    def on_new_license(self, event):
        """
        Event handler
        """
        from ._license_dialog import LicenseDialog
        dlg = LicenseDialog(self)
        if dlg.ShowModal() == wx.ID_OK:
            with open(dlg._license_file, 'r') as data:
                if add_license(dlg._license_label, data.read()):
                    self._license_type.Clear()
                    self._license_type.Append(list(get_licenses().keys()))
                    n = self._license_type.FindString(dlg._license_label)
                    if n != wx.NOT_FOUND:
                        self._license_type.SetSelection(n)
        
            

    def on_toggle_cpp_serialization(self, event):
        """
        Event handler
        """
        state = self._cpp_serialization_support.IsChecked()
        self._cpp_undo_redo_support.Enable(state)
        if state is False:
            self._cpp_undo_redo_support.SetValue(False)

    def on_select_python_template(self, event):
        """
        Event handler
        """
        pass

    def on_activate_cpp_template(self, event):
        """
        Event handler
        """
        # This implies the user has double-clicked an template.
        # The process will be a straigforward:
        #    * check the validate
        #    * if ok, use wx.CallAfter for executing the callback after return
        index = self._templates_list.GetFocusedItem()
        if index == wx.NOT_FOUND:
            return
        volatile.set(type(self), self.GetRect())
        if self.validate():
            self._has_template = True
            self._template_init_method = self._cpp_template_list[index].apply_template
            self.EndModal(wx.ID_OK)

    _info = """
        This dialog is the start point for a new project to
        be created. You  may select the project type and
        set his common options. This type can't be changed
        once the project was created.
        """
    def set_attributes(self, project):
        """
        Set dialog attributes based on existing project
        """
        self._project_name.SetValue(project._name)
        self._base_directory.SetPath(project._dir)
        self._project_author.SetValue(project._author)
        date = wx.DateTime()
        date.ParseDate(project._date)
        self._project_date.SetValue(date)
        
        self._license_type.Clear()
        self._license_type.Append(list(get_licenses().keys()))
        self._license_ready = True
        
        use_license = project._license is not None
        self._use_license.SetValue(use_license)
        self._license_type.Enable(use_license)
        self._new_license.Enable(use_license)
        if use_license:
            n = self._license_type.FindString(project._license)
            if n != wx.NOT_FOUND:
                self._license_type.SetSelection(n)
        self._project_description.SetValue(project.note)
        
        self._nature = project.language
        if project.language == 'c++':    
            self._project_choice.SetSelection(0)
            self._cpp_project_type.SetStringSelection(project._type)
            self._cpp_version_0.SetValue(project._version[0])
            self._cpp_version_1.SetValue(project._version[1])
            self._cpp_version_2.SetValue(project._version[2])
            self._cpp_headers_dir.SetValue(project._includeDir)
            self._cpp_sources_dir.SetValue(project._srcDir)
            self._use_cpp_master_include.SetValue(project.use_master_include)
            self._cpp_master_include_file.Enable(project._useMaster)
            self._cpp_master_include_file.SetValue(project.master_include)
            self._cpp_serialization_support.SetValue(project._serial)
            self._cpp_undo_redo_support.Enable(project._serial)
            self._cpp_undo_redo_support.SetValue(project._transactional)
            self._create_cpp_makefile.SetValue(project._useMakefile)
            self._go_templates_btn.Enable(False)            
        elif project.language == 'python':
            self._project_choice.SetSelection(1)
            pass
        elif project.language == 'databases':
            self._project_choice.SetSelection(2)
        #    handler = project._handler
        #    self.m_textCtrl252.SetValue(handler._database_host)
        #    self.m_textCtrl231.SetValue(handler._database_user)
        #    self.m_textCtrl241.SetValue(handler._database_password)
        #    self.m_comboBox1.SetValue(handler._database_default)
        self._original_project_name = project.name
        self.SetTitle("Edit project")
        self._veto_change_project_nature = True

    def copy_attributes(self, project):
        """
        Update project attributes with dialog values.
        """
        project._name = self._name
        project._dir = self._dir
        project._author = self._project_author.GetValue()
        project._date = self._project_date.GetValue().FormatISODate()
        project._license = self._license
        project.note = self._project_description.GetValue()
        
        
        sel = self._project_choice.GetSelection()
        if sel == 0:
            project.language = 'c++'
            project._type = self._cpp_project_type.GetStringSelection()
            project._version = [
                self._cpp_version_0.GetValue(),
                self._cpp_version_1.GetValue(),
                self._cpp_version_2.GetValue()
            ]
            
            project._includeDir = self._cpp_headers_dir.GetValue()
            project._srcDir = self._cpp_sources_dir.GetValue()
            project._useMaster = self._use_cpp_master_include.IsChecked()
            project._masterInclude = self._cpp_master_include_file.GetValue()
            project._serial = self._cpp_serialization_support.GetValue()
            project._transactional = self._cpp_undo_redo_support.IsChecked()
            project._useMakefile = self._create_cpp_makefile.IsChecked()
            if project._useMakefile:
                project.register_output_dirs()
                project.write_makefile()
        
        elif sel == 1:
            project.language = 'python'
        elif sel == 2:
            project.language = 'databases'
            # handler = project.handler
            # handler._database_host = self._database_host
            # handler._database_user = self._database_user
            # handler._database_password = self._database_password
            # handler._database_default = self._database_default
            

    def validate(self):
        """
        Validate dialog
        """
        sel = self._project_choice.GetSelection()
        if sel == 0:
            self._nature = 'c++'
        elif sel == 1:
            self._nature = 'python'
        elif sel == 2:
            self._nature = 'databases'
        self._name = self._project_name.GetValue()
        if len(self._name) == 0:
            wx.MessageBox(
                message="Project name must be non empty",
                caption="Error",
                style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                parent=self)
            return False
        if not self._original_project_name == self._name:
            if context.get_app().exist_project(self._name):
                response = wx.MessageBox(
                    message="Project already exist. Overwrite?",
                    caption="Error",
                    style=wx.YES_NO | wx.CENTER | wx.ICON_ERROR,
                    parent=self)
                if response != wx.YES:
                    return False
        self._dir = self._base_directory.GetPath()
        if len(self._dir) == 0:
            wx.MessageBox(
                message="Target directory must be non empty",
                caption="Error",
                style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                parent=self)
            return False
        if self._nature == 'c++':
            self._useMaster = self._use_cpp_master_include.IsChecked()
            self._masterInclude = self._cpp_master_include_file.GetValue().strip()
            if self._useMaster:
                if len(self._masterInclude) == 0:
                    wx.MessageBox(
                        message="Master include must be specified",
                        caption="Error",
                        style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                    parent=self)
                    return False
            self._includeDir = self._cpp_headers_dir.GetValue()
            self._srcDir = self._cpp_sources_dir.GetValue()
            self._serial = self._cpp_serialization_support.IsChecked()
            self._transactional = self._cpp_undo_redo_support.IsChecked()
            self._author = self._project_author.GetValue().strip()
            self._date = self._project_date.GetValue().FormatISODate()
            if self._use_license.IsChecked():
                if len(self._author) == 0:
                        wx.MessageBox(
                            message="For applying licenses, author must be filled",
                            caption="Error",
                            style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                        parent=self)
                        return False
                self._license = self._license_type.GetStringSelection()
            else:
                self._license = None
            self._type = self._cpp_project_type.GetStringSelection()
            self._version = [
                self._cpp_version_0.GetValue(),
                self._cpp_version_1.GetValue(),
                self._cpp_version_2.GetValue()
            ]
            self._useMakefile = self._create_cpp_makefile.IsChecked()
        #else:
        #    self._database_host = self.m_textCtrl252.GetValue()
        #    self._database_user = self.m_textCtrl231.GetValue()
        #    self._database_password = self.m_textCtrl241.GetValue()
        #    self._database_default = self.m_comboBox1.GetValue()
        #    if not self.test_database_connection(False):
        #        return False
        return True

    def get_kwargs(self):
        """
        Return arguments for instance
        """
        kwargs = {
            'parent': self._container,
            'language': self._nature,
            'name': self._name,
            'dir': self._dir,
        }
        # TODO : Remove custom flavor data
        if self._nature == 'c++':
            kwargs.update({
                'includedir': self._includeDir,
                'sourcedir': self._srcDir,
                'usemaster': self._useMaster,
                'masterinclude': self._masterInclude,
                'author': self._author,
                'date': self._date,
                'license': self._license,
                'type': self._type,
                'version': self._version,
                'usemakefile': self._useMakefile,
                'serial': self._serial,
                'transactional': self._transactional
            })
        #elif self._language == 'databases':
        #    kwargs.update({
        #        'database_host': self._database_host,
        #        'database_user': self._database_user,
        #        'database_password': self._database_password,
        #        'database_default': self._database_default,
        #    })
        return kwargs

    def init_templates(self):
        # initialize first th cpp templates
        from beatle.activity.models.cc.ui.template import load_templates
        
        self._cpp_template_list = load_templates()
        cpp_template_image_list = wx.inmutableImageList(64, 64, mask=True, initialCount=len(self._cpp_template_list))
        for item in self._cpp_template_list:
            cpp_template_image_list.Add(item.bitmap())
            
        self._templates_list.AssignImageList(cpp_template_image_list, wx.IMAGE_LIST_SMALL)
        
        for index, element in enumerate(self._cpp_template_list):
            item = wx.ListItem()
            item.SetId(index)
            item.SetText(element.name())
            item.SetImage(index)
            self._templates_list.InsertItem(item)

    def ShowModal(self):
        self.init_templates()
        return super(ProjectDialog, self).ShowModal()

    def post_process(self, obj):
        """
        This is used for template application
        """
        if self._has_template:
            self._template_init_method(obj)

    @wxx.restore_position
    @beatle.lib.wxx.SetInfo(__doc__)
    def __init__(self, parent, container=None):
        """
        Constructor for the frame window
        """
        self._flex_grid_sizer_1 = None
        self._aui_notebook_1 = None
        self._general_page = None
        self._authoring_page = None
        self._flex_grid_sizer_2 = None
        self._flex_grid_sizer_3 = None
        self._project_choice = None
        self._database_project = None
        self._static_text_1 = None
        self._project_name = None
        self._static_text_2 = None
        self._base_directory = None
        self._cpp_project_pages = None
        self._cpp_basic_details = None
        self._cpp_templates = None
        self._flex_grid_sizer_4 = None
        self._flex_grid_sizer_5 = None
        self._static_text_3 = None
        self._cpp_headers_dir = None
        self._static_text_4 = None
        self._cpp_sources_dir = None
        self._use_cpp_master_include = None
        self._cpp_master_include_file = None
        self._spacer_1 = None
        self._cpp_serialization_support = None
        self._spacer_2 = None
        self._cpp_undo_redo_support = None
        self._spacer_3 = None
        self._go_templates_btn = None
        self._flex_grid_sizer_6 = None
        self._templates_list = None
        self._go_cpp_basic_details = None
        self._flex_grid_sizer_7 = None
        self._flex_grid_sizer_8 = None
        self._static_text_5 = None
        self._project_author = None
        self._static_text_6 = None
        self._project_date = None
        self._use_license = None
        self._static_box_sizer_3 = None
        self._project_description = None
        self._cpp_project_type_lb = None
        self._cpp_project_type = None
        self._spacer_4 = None
        self._create_cpp_makefile = None
        self._static_text_9 = None
        self._flex_grid_sizer_10 = None
        self._flex_grid_sizer_14 = None
        self.m_info = None
        self._cpp_version_0 = None
        self._cpp_version_1 = None
        self._cpp_version_2 = None
        self._cancel = None
        self._ok = None
        self._flex_grid_sizer_15 = None
        self._license_type = None
        self._new_license = None
        self._python_project = None
        self._python_templates = None
        self._flex_grid_sizer_20 = None
        self._python_template_list = None
        self._veto_change_project_nature = False
        self._original_project_name = ''
        self._name = ''
        self._dir = ''
        self._auto_dir = True
        self._container = container
        self._base = None
        self._nature = None
        self._useMaster = True
        self._masterInclude = ''
        self._includeDir = ''
        self._srcDir = ''
        self._serial = False
        self._transactional = False
        self._type = 0
        self._version = []
        self._licenses_ready = False
        self._license = None
        self._cpp_template_list = None
        self._has_template = False
        self._template_init_method = None
        super(ProjectDialog,self).__init__(parent, id=-1, title = 'New project', pos=(-1, -1), size=(600, 800), style=wx.CAPTION|wx.CLOSE_BOX|wx.RESIZE_BORDER|wx.STAY_ON_TOP|wx.CLIP_CHILDREN)
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.wxui_initialize_controls()
        self.wxui_connect_event_handlers()
        self.Layout()

    def wxui_initialize_controls(self):
        """
        This method is responsible for initialize the form layout
        """
        self._flex_grid_sizer_1 = wx.FlexGridSizer(2, 1, 0, 0)
        self._flex_grid_sizer_1.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_1.AddGrowableCol(0)
        self._flex_grid_sizer_1.AddGrowableRow(0)
        self._aui_notebook_1 = wx.aui.AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_TAB_FIXED_WIDTH|wx.CLIP_CHILDREN)
        self._general_page = wx.Panel(self._aui_notebook_1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_2 = wx.FlexGridSizer(2, 1, 0, 0)
        self._flex_grid_sizer_2.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_2.AddGrowableCol(0)
        self._flex_grid_sizer_2.AddGrowableRow(1)
        self._flex_grid_sizer_3 = wx.FlexGridSizer(2, 2, 0, 0)
        self._flex_grid_sizer_3.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_3.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_3.AddGrowableCol(1)
        self._static_text_1 = wx.StaticText(self._general_page, wx.ID_ANY, "&Name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_3.Add(self._static_text_1, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._project_name = wx.TextCtrl(self._general_page, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_3.Add(self._project_name, 0, wx.EXPAND| wx.RIGHT| wx.TOP, 5)
        self._static_text_2 = wx.StaticText(self._general_page, wx.ID_ANY, "&Base directory:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_3.Add(self._static_text_2, 0, wx.ALIGN_CENTER_VERTICAL| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._base_directory = wx.DirPickerCtrl(self._general_page, wx.ID_ANY, wx.EmptyString, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.DIRP_USE_TEXTCTRL|wx.DIRP_DIR_MUST_EXIST|wx.DIRP_SMALL)
        self._base_directory.Enable(False)
        self._flex_grid_sizer_3.Add(self._base_directory, 0, wx.EXPAND| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_2.Add(self._flex_grid_sizer_3, 0, wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.TOP, 5)
        self._project_choice = wx.Choicebook(self._general_page, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT)
        self._cpp_project_pages = wx.Simplebook(self._project_choice, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CLIP_CHILDREN)
        self._cpp_basic_details = wx.Panel(self._cpp_project_pages, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_4 = wx.FlexGridSizer(2, 1, 0, 0)
        self._flex_grid_sizer_4.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_4.AddGrowableCol(0)
        self._flex_grid_sizer_4.AddGrowableRow(0)
        self._flex_grid_sizer_5 = wx.FlexGridSizer(9, 2, 0, 0)
        self._flex_grid_sizer_5.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_5.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_5.AddGrowableCol(1)
        self._flex_grid_sizer_5.AddGrowableRow(8)
        self._cpp_project_type_lb = wx.StaticText(self._cpp_basic_details, wx.ID_ANY, "&Project type:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._cpp_project_type_lb, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.RIGHT| wx.TOP, 5)
        _cpp_project_type_choices = ["executable","shared library","static library","code only"]
        self._cpp_project_type = wx.Choice(self._cpp_basic_details, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, _cpp_project_type_choices, 0)
        self._cpp_project_type.SetSelection(0)
        self._flex_grid_sizer_5.Add(self._cpp_project_type, 0, wx.EXPAND| wx.TOP, 5)
        self._static_text_9 = wx.StaticText(self._cpp_basic_details, wx.ID_ANY, "&Version:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._static_text_9, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_10 = wx.FlexGridSizer(1, 3, 0, 0)
        self._flex_grid_sizer_10.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_10.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_10.AddGrowableCol(0)
        self._flex_grid_sizer_10.AddGrowableCol(1)
        self._flex_grid_sizer_10.AddGrowableCol(2)
        self._cpp_version_0 = wx.SpinCtrl(self._cpp_basic_details, wx.ID_ANY, "1", wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS|wx.TE_PROCESS_ENTER, 0, 99, 1)
        self._cpp_version_0.SetMinSize(wx.Size(50, -1))
        self._flex_grid_sizer_10.Add(self._cpp_version_0, 0, wx.TOP, 5)
        self._cpp_version_1 = wx.SpinCtrl(self._cpp_basic_details, wx.ID_ANY, "0", wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS|wx.TE_PROCESS_ENTER, 0, 99, 0)
        self._cpp_version_1.SetMinSize(wx.Size(50, -1))
        self._flex_grid_sizer_10.Add(self._cpp_version_1, 0, wx.TOP, 5)
        self._cpp_version_2 = wx.SpinCtrl(self._cpp_basic_details, wx.ID_ANY, "0", wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS|wx.TE_PROCESS_ENTER, 0, 99, 0)
        self._cpp_version_2.SetMinSize(wx.Size(50, -1))
        self._flex_grid_sizer_10.Add(self._cpp_version_2, 0, wx.TOP, 5)
        self._flex_grid_sizer_5.Add(self._flex_grid_sizer_10, 1, wx.EXPAND, 5)
        self._static_text_3 = wx.StaticText(self._cpp_basic_details, wx.ID_ANY, "&Headers subdir.:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._static_text_3, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.RIGHT| wx.TOP, 5)
        self._cpp_headers_dir = wx.TextCtrl(self._cpp_basic_details, wx.ID_ANY, "include", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._cpp_headers_dir, 0, wx.EXPAND| wx.TOP, 5)
        self._static_text_4 = wx.StaticText(self._cpp_basic_details, wx.ID_ANY, "&Sources subdir.:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._static_text_4, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.RIGHT| wx.TOP, 5)
        self._cpp_sources_dir = wx.TextCtrl(self._cpp_basic_details, wx.ID_ANY, "src", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._cpp_sources_dir, 0, wx.EXPAND| wx.TOP, 5)
        self._use_cpp_master_include = wx.CheckBox(self._cpp_basic_details, wx.ID_ANY, "&Master include:", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._use_cpp_master_include.SetValue(True)
        self._flex_grid_sizer_5.Add(self._use_cpp_master_include, 0, wx.ALIGN_CENTER_VERTICAL| wx.LEFT| wx.TOP, 5)
        self._cpp_master_include_file = wx.TextCtrl(self._cpp_basic_details, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._cpp_master_include_file, 0, wx.EXPAND| wx.TOP, 5)
        self._spacer_1 = self._flex_grid_sizer_5.Add(wx.Size(0,0), 0, 0, 5)
        self._cpp_serialization_support = wx.CheckBox(self._cpp_basic_details, wx.ID_ANY, "&Serialization support", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._cpp_serialization_support.SetValue(False)
        self._flex_grid_sizer_5.Add(self._cpp_serialization_support, 0, wx.TOP, 5)
        self._spacer_2 = self._flex_grid_sizer_5.Add(wx.Size(0,0), 0, 0, 5)
        self._cpp_undo_redo_support = wx.CheckBox(self._cpp_basic_details, wx.ID_ANY, "&Undo/redo support", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._cpp_undo_redo_support.SetValue(False)
        self._cpp_undo_redo_support.Enable(False)
        self._flex_grid_sizer_5.Add(self._cpp_undo_redo_support, 0, wx.TOP, 5)
        self._spacer_4 = self._flex_grid_sizer_5.Add(wx.Size(0,0), 0, 0, 5)
        self._create_cpp_makefile = wx.CheckBox(self._cpp_basic_details, wx.ID_ANY, "&Create makefile", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._create_cpp_makefile.SetValue(True)
        self._flex_grid_sizer_5.Add(self._create_cpp_makefile, 0, wx.TOP, 5)
        self._spacer_3 = self._flex_grid_sizer_5.Add(wx.Size(0,0), 0, 0, 5)
        self._go_templates_btn = wx.Button(self._cpp_basic_details, wx.ID_ANY, "Templates >>", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_5.Add(self._go_templates_btn, 0, wx.ALIGN_BOTTOM| wx.ALIGN_RIGHT| wx.BOTTOM, 5)
        self._flex_grid_sizer_4.Add(self._flex_grid_sizer_5, 0, wx.EXPAND, 5)
        self._cpp_basic_details.SetSizer(self._flex_grid_sizer_4)
        self._cpp_project_pages.AddPage(self._cpp_basic_details, "a page", False )
        self._cpp_templates = wx.Panel(self._cpp_project_pages, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_6 = wx.FlexGridSizer(2, 1, 0, 0)
        self._flex_grid_sizer_6.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_6.AddGrowableCol(0)
        self._flex_grid_sizer_6.AddGrowableRow(0)
        self._templates_list = wx.ListCtrl(self._cpp_templates, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_SMALL_ICON|wx.LC_AUTOARRANGE|wx.LC_SINGLE_SEL)
        self._flex_grid_sizer_6.Add(self._templates_list, 0, wx.EXPAND| wx.LEFT| wx.RIGHT, 5)
        self._go_cpp_basic_details = wx.Button(self._cpp_templates, wx.ID_ANY, "<< Back", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_6.Add(self._go_cpp_basic_details, 0, wx.BOTTOM| wx.LEFT| wx.TOP, 5)
        self._cpp_templates.SetSizer(self._flex_grid_sizer_6)
        self._cpp_project_pages.AddPage(self._cpp_templates, "a page", False )
        self._project_choice.AddPage(self._cpp_project_pages, "c++ project", False )
        self._python_project = wx.Simplebook(self._project_choice, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self._python_templates = wx.Panel(self._python_project, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_20 = wx.FlexGridSizer(1, 1, 0, 0)
        self._flex_grid_sizer_20.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_20.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_20.AddGrowableCol(0)
        self._flex_grid_sizer_20.AddGrowableRow(0)
        self._python_template_list = wx.ListCtrl(self._python_templates, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_ICON|wx.LC_SINGLE_SEL)
        self._python_template_list.SetMinSize(wx.Size(10, 10))
        self._python_template_list.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_APPWORKSPACE))
        self._flex_grid_sizer_20.Add(self._python_template_list, 1, wx.EXPAND, 5)
        self._python_templates.SetSizer(self._flex_grid_sizer_20)
        self._python_project.AddPage(self._python_templates, "a page", False )
        self._project_choice.AddPage(self._python_project, "python project", False )
        self._database_project = wx.Panel(self._project_choice, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self._project_choice.AddPage(self._database_project, "database project", False )
        self._flex_grid_sizer_2.Add(self._project_choice, 0, wx.EXPAND| wx.LEFT| wx.RIGHT, 5)
        self._general_page.SetSizer(self._flex_grid_sizer_2)
        self._aui_notebook_1.AddPage(self._general_page, "General", False )
        self._authoring_page = wx.Panel(self._aui_notebook_1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_7 = wx.FlexGridSizer(2, 1, 0, 0)
        self._flex_grid_sizer_7.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_7.AddGrowableCol(0)
        self._flex_grid_sizer_7.AddGrowableRow(1)
        self._flex_grid_sizer_8 = wx.FlexGridSizer(3, 2, 0, 0)
        self._flex_grid_sizer_8.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_8.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_8.AddGrowableCol(1)
        self._static_text_5 = wx.StaticText(self._authoring_page, wx.ID_ANY, "&Author:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_8.Add(self._static_text_5, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.RIGHT| wx.TOP, 5)
        self._project_author = wx.TextCtrl(self._authoring_page, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_8.Add(self._project_author, 0, wx.EXPAND| wx.TOP, 5)
        self._static_text_6 = wx.StaticText(self._authoring_page, wx.ID_ANY, "&Date:", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_8.Add(self._static_text_6, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.RIGHT| wx.TOP, 5)
        self._project_date = wx.adv.DatePickerCtrl(self._authoring_page, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.adv.DP_DEFAULT)
        self._flex_grid_sizer_8.Add(self._project_date, 0, wx.EXPAND| wx.TOP, 5)
        self._use_license = wx.CheckBox(self._authoring_page, wx.ID_ANY, "Add license:", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._use_license.SetValue(True)
        self._flex_grid_sizer_8.Add(self._use_license, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_15 = wx.FlexGridSizer(1, 2, 0, 0)
        self._flex_grid_sizer_15.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_15.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_15.AddGrowableCol(0)
        _license_type_choices = []
        self._license_type = wx.Choice(self._authoring_page, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, _license_type_choices, 0)
        self._flex_grid_sizer_15.Add(self._license_type, 0, wx.EXPAND, 5)
        self._new_license = wx.Button(self._authoring_page, wx.ID_ANY, "new...", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_15.Add(self._new_license, 0, wx.LEFT, 5)
        self._flex_grid_sizer_8.Add(self._flex_grid_sizer_15, 0, wx.EXPAND| wx.TOP, 5)
        self._flex_grid_sizer_7.Add(self._flex_grid_sizer_8, 0, wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._static_box_sizer_3 = wx.StaticBoxSizer(wx.StaticBox(self._authoring_page, wx.ID_ANY, " Description"), wx.VERTICAL)
        self._project_description = wx.TextCtrl(self._authoring_page, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE)
        self._static_box_sizer_3.Add(self._project_description, 1, wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_7.Add(self._static_box_sizer_3, 0, wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._authoring_page.SetSizer(self._flex_grid_sizer_7)
        self._aui_notebook_1.AddPage(self._authoring_page, "Authoring", False )
        self._flex_grid_sizer_1.Add(self._aui_notebook_1, 0, wx.EXPAND| wx.LEFT| wx.RIGHT, 5)
        self._flex_grid_sizer_14 = wx.FlexGridSizer(1, 3, 0, 0)
        self._flex_grid_sizer_14.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_14.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_14.AddGrowableCol(0)
        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_14.Add(self.m_info, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_LEFT| wx.LEFT, 5)
        self._cancel = wx.Button(self, wx.ID_CANCEL, "", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_14.Add(self._cancel, 0, wx.LEFT| wx.RIGHT, 5)
        self._ok = wx.Button(self, wx.ID_OK, "", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_14.Add(self._ok, 0, wx.LEFT| wx.RIGHT, 5)
        self._flex_grid_sizer_1.Add(self._flex_grid_sizer_14, 0, wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.TOP, 5)
        self.SetSizer(self._flex_grid_sizer_1)
        self.Layout()

    def wxui_connect_event_handlers(self):
        """
        This method is responsible for connect the event handlers
        """
        self.Bind(wx.EVT_INIT_DIALOG,self.on_init_dialog)
        self._project_name.Bind(wx.EVT_TEXT,self.on_change_project_name)
        self._project_choice.Bind(wx.EVT_CHOICEBOOK_PAGE_CHANGED,self.on_project_nature_changed)
        self._project_choice.Bind(wx.EVT_CHOICEBOOK_PAGE_CHANGING,self.on_changing_project_nature)
        self._cpp_serialization_support.Bind(wx.EVT_CHECKBOX,self.on_toggle_cpp_serialization)
        self._go_templates_btn.Bind(wx.EVT_BUTTON,self.on_goto_cpp_templates)
        self._templates_list.Bind(wx.EVT_LIST_ITEM_ACTIVATED,self.on_activate_cpp_template)
        self._go_cpp_basic_details.Bind(wx.EVT_BUTTON,self.on_goto_cpp_basic_details)
        self._python_template_list.Bind(wx.EVT_LIST_ITEM_SELECTED,self.on_select_python_template)
        self._use_license.Bind(wx.EVT_CHECKBOX,self.on_use_license)
        self._new_license.Bind(wx.EVT_BUTTON,self.on_new_license)
        self._cancel.Bind(wx.EVT_BUTTON,self.on_cancel)
        self._ok.Bind(wx.EVT_BUTTON,self.on_ok)

    def wxui_disconnect_event_handlers(self):
        """
        This method is responsible for disconnect the event handlers
        """
        self.Unbind(wx.EVT_INIT_DIALOG,self.on_init_dialog)
        self._project_name.Unbind(wx.EVT_TEXT,self.on_change_project_name)
        self._project_choice.Unbind(wx.EVT_CHOICEBOOK_PAGE_CHANGED,self.on_project_nature_changed)
        self._project_choice.Unbind(wx.EVT_CHOICEBOOK_PAGE_CHANGING,self.on_changing_project_nature)
        self._cpp_serialization_support.Unbind(wx.EVT_CHECKBOX,self.on_toggle_cpp_serialization)
        self._go_templates_btn.Unbind(wx.EVT_BUTTON,self.on_goto_cpp_templates)
        self._templates_list.Unbind(wx.EVT_LIST_ITEM_ACTIVATED,self.on_activate_cpp_template)
        self._go_cpp_basic_details.Unbind(wx.EVT_BUTTON,self.on_goto_cpp_basic_details)
        self._python_template_list.Unbind(wx.EVT_LIST_ITEM_SELECTED,self.on_select_python_template)
        self._use_license.Unbind(wx.EVT_CHECKBOX,self.on_use_license)
        self._new_license.Unbind(wx.EVT_BUTTON,self.on_new_license)
        self._cancel.Unbind(wx.EVT_BUTTON,self.on_cancel)
        self._ok.Unbind(wx.EVT_BUTTON,self.on_ok)

    def delete(self):
        """
        Destructor for the frame window
        """
        self.wxui_disconnect_event_handlers()


