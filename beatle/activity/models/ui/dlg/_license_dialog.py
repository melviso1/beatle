# -*- coding: utf-8 -*-
"""
Module containing classes definitions
"""

import wx
import beatle.lib.handlers
from beatle.lib import wxx
from beatle.lib.api import get_licenses


class LicenseDialog(wx.Dialog):
    """
    This dialog allows you to add a new copyright file for your product.
    """
    def on_ok(self, event):
        """
        Event handler
        """
        # write here the event handler
        if self.validate():
            self.EndDialog(wx.ID_OK)

    def validate(self):
        """
        This method verifies that user imput is ok.
        """
        self._license_label = self._edit_text_10.GetValue().strip()
        self._license_file = self._file_picker_2.GetPath().strip()
        if len(self._license_label) == 0:
            wx.MessageDialog(self, "The license label bust be non empty", "Error", style=wx.OK | wx.ICON_ERROR).ShowModal()
            return False
            
        if self._license_label in get_licenses():
            wx.MessageDialog(self, f"The license {self._license_label} already exist", "Error", style=wx.OK | wx.ICON_ERROR).ShowModal()
            return False
                
        if len(self._license_file) == 0:
            wx.MessageDialog(self, "Please select a license file", "Error", style=wx.OK | wx.ICON_ERROR).ShowModal()
            return False
            
        return True
            

    @wxx.restore_position
    @wxx.SetInfo(__doc__)
    def __init__(self, parent=None):
        """
        Constructor for the dialog window
        """
        self._flex_grid_sizer_16 = None
        self._flex_grid_sizer_17 = None
        self._static_text_10 = None
        self._edit_text_10 = None
        self._flex_grid_sizer_18 = None
        self._static_text_11 = None
        self._file_picker_2 = None
        self._spacer_5 = None
        self._flex_grid_sizer_19 = None
        self.m_info = None
        self._spacer_6 = None
        self._button_7 = None
        self._button_8 = None
        self._license_label = None
        self._license_file = None
        super(LicenseDialog,self).__init__(parent, id=-1, title = 'New license file', pos=(-1, -1), size=(-1, -1), style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.wxui_initialize_controls()
        self.wxui_connect_event_handlers()
        self.Layout()

    def wxui_initialize_controls(self):
        """
        This method is responsible for initialize the form layout
        """
        self._flex_grid_sizer_16 = wx.FlexGridSizer(4, 1, 0, 0)
        self._flex_grid_sizer_16.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_16.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_16.AddGrowableCol(0)
        self._flex_grid_sizer_16.AddGrowableRow(3)
        self._spacer_5 = self._flex_grid_sizer_16.Add(wx.Size(0,10), 0, 0, 5)
        self._flex_grid_sizer_17 = wx.FlexGridSizer(1, 2, 0, 0)
        self._flex_grid_sizer_17.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_17.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_17.AddGrowableCol(1)
        self._static_text_10 = wx.StaticText(self, wx.ID_ANY, "license label", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_17.Add(self._static_text_10, 0, wx.ALIGN_CENTER_VERTICAL| wx.BOTTOM| wx.LEFT| wx.TOP, 5)
        self._edit_text_10 = wx.TextCtrl(self, wx.ID_ANY, "", wx.DefaultPosition,wx.DefaultSize, 0)
        self._edit_text_10.SetToolTip("place a labe here")
        self._flex_grid_sizer_17.Add(self._edit_text_10, 0, wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_16.Add(self._flex_grid_sizer_17, 0, wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_18 = wx.FlexGridSizer(1, 2, 0, 0)
        self._flex_grid_sizer_18.SetFlexibleDirection(wx.BOTH)
        self._flex_grid_sizer_18.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_18.AddGrowableCol(1)
        self._static_text_11 = wx.StaticText(self, wx.ID_ANY, "license file", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_18.Add(self._static_text_11, 0, wx.ALIGN_CENTER_VERTICAL| wx.LEFT| wx.RIGHT, 5)
        self._file_picker_2 = wx.FilePickerCtrl(self, wx.ID_ANY, wx.EmptyString, wx.EmptyString, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.FLP_USE_TEXTCTRL|wx.FLP_OPEN|wx.FLP_FILE_MUST_EXIST)
        self._flex_grid_sizer_18.Add(self._file_picker_2, 0, wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_16.Add(self._flex_grid_sizer_18, 0, wx.EXPAND| wx.LEFT| wx.RIGHT, 5)
        self._flex_grid_sizer_19 = wx.FlexGridSizer(1, 4, 0, 0)
        self._flex_grid_sizer_19.SetFlexibleDirection(wx.HORIZONTAL)
        self._flex_grid_sizer_19.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._flex_grid_sizer_19.AddGrowableCol(1)
        self._flex_grid_sizer_19.AddGrowableRow(0)
        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_19.Add(self.m_info, 0, wx.ALIGN_BOTTOM| wx.BOTTOM| wx.LEFT| wx.RIGHT, 5)
        self._spacer_6 = self._flex_grid_sizer_19.Add(wx.Size(0,0), 0, 0, 5)
        self._button_8 = wx.Button(self, wx.ID_CANCEL, "&Cancel", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_19.Add(self._button_8, 0, wx.ALIGN_BOTTOM| wx.BOTTOM| wx.LEFT| wx.RIGHT, 5)
        self._button_7 = wx.Button(self, wx.ID_OK, "&Ok", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_19.Add(self._button_7, 0, wx.ALIGN_BOTTOM| wx.BOTTOM| wx.LEFT| wx.RIGHT, 5)
        self._flex_grid_sizer_16.Add(self._flex_grid_sizer_19, 0, wx.ALIGN_BOTTOM| wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.RIGHT, 5)
        self.SetSizer(self._flex_grid_sizer_16)
        self.Layout()

    def wxui_connect_event_handlers(self):
        """
        This method is responsible for connect the event handlers
        """
        self._button_7.Bind(wx.EVT_BUTTON,self.on_ok)

    def wxui_disconnect_event_handlers(self):
        """
        This method is responsible for disconnect the event handlers
        """
        self._button_7.Unbind(wx.EVT_BUTTON,self.on_ok)

    def delete(self):
        """
        Destructor for the frame window
        """
        self.wxui_disconnect_event_handlers()


