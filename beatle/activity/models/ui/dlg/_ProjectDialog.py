
import os, wx

import beatle
from beatle.app.ui import ProjectDialogBase
from beatle.lib.api import context


# Implementing NewProject
class ProjectDialog(ProjectDialogBase):
    """
    This dialog is the start point for a new project to
    be created. You  may select the project type and
    set his common options. This type can't be changed
    once the project was created.
    """
    @beatle.lib.wxx.SetInfo(__doc__)
    @beatle.lib.wxx.restore_position
    def __init__(self, parent, container=None):
        """Initialization"""
        from beatle.app import resources as rc
        super(ProjectDialog, self).__init__(parent)
        self.m_sdbSizer1OK.SetDefault()
        # -- vars --
        self._dir = ''
        self._includeDir = ''
        self._srcDir = ''
        self._useMaster = True
        self._masterInclude = ''
        self._serial = False
        self._transactional = False
        self._author = ''
        self._date = ''
        self._license = ''
        self._type = ''
        self._version = []
        self._useMakefile = True
        self._original = ""
        self._auto_dir = True
        self._veto = False
        self._database_host = ''
        self._database_user = ''
        self._database_password = ''

        self._container = container
        # if this project is inside workspace, the base
        # path must be determined by this workspace
        wk = container.workspace
        if wk is not None:
            self._base = wk.dir
            self.m_bpButton3.Enable(False)
            self.m_textCtrl9.SetEditable(False)
            self.m_bpButton3.Enable(False)
        else:
            self._base = wx.StandardPaths.Get().GetDocumentsDir()
        self.m_textCtrl9.SetValue(self._base)
        self.m_textCtrl1.SetFocus()
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap('cppproject'))
        self.SetIcon(icon)

    def copy_attributes(self, prj):
        """Update project attributes with dialog values"""
        sel = self.m_choicebook1.GetSelection()
        prj._name = self._name
        if sel == 0:
            prj.language = 'c++'
        elif sel == 1:
            prj.language = 'python'
        elif sel == 2:
            prj.language = 'databases'
        prj._dir = self._dir
        if sel in [0, 1]:
            prj._includeDir = self.m_textCtrl23.GetValue()
            prj._srcDir = self.m_textCtrl24.GetValue()
            prj._useMaster = self.m_checkBox52.GetValue()
            prj._masterInclude = self.m_textCtrl25.GetValue()
            if sel == 0:
                prj._serial = self.m_checkBox7.GetValue()
                prj._transactional = self.m_checkBox8.GetValue()
            prj._author = self.m_textCtrl251.GetValue()
            prj._date = self.m_datePicker1.GetValue().FormatISODate()
            if self.m_checkBox53.IsChecked():
                prj._license = self.m_choice16.GetStringSelection()
            else:
                prj._license = None
            prj._type = self.m_choice151.GetStringSelection()
            prj._version = [
                self.m_spinCtrl11.GetValue(),
                self.m_spinCtrl21.GetValue(),
                self.m_spinCtrl31.GetValue()
            ]
            prj._useMakefile = self.m_checkBox54.IsChecked()
            prj.note = self.m_richText13.GetValue()
            if sel == 0:
                if prj._useMakefile:
                    prj.register_output_dirs()
                    prj.write_makefile()
        else:
            handler = prj.handler
            handler._database_host = self._database_host
            handler._database_user = self._database_user
            handler._database_password = self._database_password
            handler._database_default = self._database_default

    def set_attributes(self, prj):
        """Set dialog attributes based on existing project"""
        if prj.language == 'c++':
            self.m_choicebook1.SetSelection(0)
        elif prj.language == 'python':
            self.m_choicebook1.SetSelection(1)
        elif prj.language == 'databases':
            self.m_choicebook1.SetSelection(2)
        self._veto = True
        self.m_textCtrl1.SetValue(prj._name)
        self.m_textCtrl9.SetValue(prj._dir)
        if prj.language == 'c++':
            self.m_textCtrl23.SetValue(prj._includeDir)
            self.m_textCtrl24.SetValue(prj._srcDir)
            self.m_checkBox52.SetValue(prj.use_master_include)
            self.m_textCtrl25.Enable(prj._useMaster)
            self.m_textCtrl25.SetValue(prj.master_include)
            self.m_checkBox7.SetValue(prj._serial)
            self.m_checkBox8.Enable(prj._serial)
            self.m_checkBox8.SetValue(prj._transactional)
            self.m_textCtrl251.SetValue(prj._author)
            date = wx.DateTime()
            date.ParseDate(prj._date)
            self.m_datePicker1.SetValue(date)
            if prj._license is not None:
                self.m_checkBox53.SetValue(True)
                self.m_choice16.SetStringSelection(prj._license)
            else:
                self.m_checkBox53.SetValue(False)
            self.m_choice151.SetStringSelection(prj._type)
            self.m_spinCtrl11.SetValue(prj._version[0])
            self.m_spinCtrl21.SetValue(prj._version[1])
            self.m_spinCtrl31.SetValue(prj._version[2])
            self.m_checkBox54.SetValue(prj._useMakefile)
            self.m_richText13.SetValue(prj.note)
        elif prj.language == 'python':
            self.m_textCtrl251.SetValue(prj._author)
            date = wx.DateTime()
            date.ParseDate(prj._date)
            self.m_datePicker1.SetValue(date)
            if prj._license is not None:
                self.m_checkBox53.SetValue(True)
                self.m_choice16.SetStringSelection(prj._license)
            else:
                self.m_checkBox53.SetValue(False)
            self.m_choice151.SetStringSelection(prj._type)
            self.m_spinCtrl11.SetValue(prj._version[0])
            self.m_spinCtrl21.SetValue(prj._version[1])
            self.m_spinCtrl31.SetValue(prj._version[2])
            self.m_richText13.SetValue(prj.note)
        elif prj.language == 'databases':
            handler = prj._handler
            self.m_textCtrl252.SetValue(handler._database_host)
            self.m_textCtrl231.SetValue(handler._database_user)
            self.m_textCtrl241.SetValue(handler._database_password)
            self.m_comboBox1.SetValue(handler._database_default)
        self._original = prj.name
        self.SetTitle("Edit project")

    def on_page_changed(self, event):
        """Show/Hide some controls from project type"""
        from beatle.app import resources as rc
        selection_index = event.GetSelection()
        icon = wx.Icon()
        if selection_index == 2:
            # This is a database project
            index = self.m_auinotebook2.GetPageIndex(self.m_panel6)
            if index != wx.NOT_FOUND:
                self.m_auinotebook2.RemovePage(index)
            index = self.m_auinotebook2.GetPageIndex(self.m_panel7)
            if index != wx.NOT_FOUND:
                self.m_auinotebook2.RemovePage(index)
            icon.CopyFromBitmap(rc.get_bitmap('databases'))
        else:
            if self.m_auinotebook2.GetPageIndex(self.m_panel6) == wx.NOT_FOUND:
                self.m_auinotebook2.AddPage(self.m_panel6, u"Authoring", False)
                self.m_auinotebook2.AddPage(self.m_panel7, u"Generation", False)
            if selection_index == 0:
                icon.CopyFromBitmap(rc.get_bitmap('cppproject'))
            elif selection_index == 1:
                icon.CopyFromBitmap(rc.get_bitmap('python'))
        self.fgSizer4.Layout()
        self.fgSizer65.Layout()
        self.SetIcon(icon)

    def on_page_changing(self, event):
        """Checks if project type can be changed"""
        if self._veto:
            event.Veto()
        else:
            event.Skip()

    def on_change_project_name(self, event):
        """Process change text event"""
        if self._auto_dir:
            name = self.m_textCtrl1.GetValue()
            # create a custom subdirectory
            self.m_textCtrl9.SetValue(os.path.join(self._base, name))
            if self.m_checkBox52.GetValue():
                # replace master include
                name = name.replace(" ", "_")
                name += ".h"
                self.m_textCtrl25.SetValue(name)

    def on_choose_dir(self, event):
        """Launch sub-dialog for choosing dir"""
        dialog = wx.DirDialog(
            self, "Select directory",
            self.m_textCtrl9.GetValue(), wx.DD_DIR_MUST_EXIST)
        if dialog.ShowModal() == wx.ID_OK:
            self.m_textCtrl9.SetValue(dialog.GetPath())
            self._auto_dir = False

    def on_toggle_master_include(self, event):
        """Toggle using master include file"""
        if self.m_checkBox52.GetValue():
            self.m_textCtrl25.Enable(True)
            self.m_textCtrl25.SetValue(self.m_textCtrl1.GetValue() + ".h")
        else:
            self.m_textCtrl25.Enable(False)

    def on_serialization_support(self, event):
        """Toggle serialization support"""
        if self.m_checkBox7.GetValue():
            self.m_checkBox8.Enable(True)   # Enable undo/redo
        else:
            self.m_checkBox8.SetValue(False)
            self.m_checkBox8.Enable(False)

    def on_undo_redo_support(self, event):
        event.Skip()

    # Database project handlers
    def on_mysql_host_change(self, event):
        """Handle database host change"""
        self._database_host = self.m_textCtrl252.GetValue()
        self.update_test_database_connection()

    def on_mysql_user_change(self, event):
        """Handle user host change"""
        self._database_user = self.m_textCtrl231.GetValue()
        self.update_test_database_connection()

    def on_mysql_password_change(self, event):
        """Handle user password change"""
        self._database_password = self.m_textCtrl241.GetValue()
        self.update_test_database_connection()

    def update_test_database_connection(self):
        """Update the enabled/disabled status of the test connection button"""
        if len(self._database_host) > 0 and len(self._database_user) > 0 and len(self._database_password) > 0:
            self.m_button3.Enable(True)
        else:
            self.m_button3.Enable(False)

    def on_test_database_connection(self, event):
        """Test the database connection"""
        if self.test_database_connection():
            self.update_schemas()

    def get_mysql_driver(self):
        """Attempt to import MySQLdb and return status"""
        from beatle.lib.utils import import_once
        try:
            value = import_once("MySQLdb")
        except:
            return False
        return value

    def test_database_connection(self, verbose=True):
        """Check the database connectivity"""
        retry = False
        frame = context.get_frame()
        driver = self.get_mysql_driver()
        if not driver:
            reponse = wx.MessageBox(
                message="\nThe mysql-python package is not installed.\n\n"
                "You need have installed python-dev and libmysqlclient-dev \n"
                "and mysql-python package. For example doing:\n\n"
                " > sudo apt-get install python-dev libmysqlclient-dev \n"
                " > (sudo) pip install mysql-python.\n\n"
                "Do you want we attempt to install mysql-python for you?",
                caption="Error",
                style=wx.YES_NO | wx.CENTER | wx.ICON_ERROR,
                parent=frame)
            if reponse != wx.YES:
                return
            if os.system("gksudo 'apt-get -y install python-dev libmysqlclient-dev'"):
                wx.MessageBox(
                    message="Install failed!",
                    caption="Error",
                    style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                    parent=frame)
                return
            if os.system("pip install mysql-python"):
                # Ok, now with sudo
                if os.system("gksudo 'pip install mysql-python'"):
                    wx.MessageBox(
                        message="Install failed!",
                        caption="Error",
                        style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                        parent=frame)
                    return
            retry = True
        if retry:
            driver = self.get_mysql_driver()
            if driver:
                wx.MessageBox(
                    message="The install seems ok, but import mysql-python still fails.\n"
                            "Try to close and reope beatle.\n"
                            "Sorry for the inconvenience",
                    caption="Error",
                    style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                    parent=frame)
                return
        try:
            conn = driver.connect(host=self._database_host,
                        user=self._database_user,
                        passwd=self._database_password)
        except driver.Error:
            wx.MessageBox(message="Connection failed.",
                          caption="Error",
                          style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                          parent=frame)
            return False
        conn.close()
        if verbose:
            wx.MessageBox(
                message="Connection success.",
                caption="Error",
                style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                parent=frame)
        return True

    def update_schemas(self):
        """Update the schema list"""
        driver = self.get_mysql_driver()
        conn = driver.connect(host=self._database_host, user=self._database_user, passwd=self._database_password)
        conn.query('SHOW SCHEMAS')
        data = conn.store_result()
        schemas = []
        for i in range(0, data.num_rows()):
            schemas.append(data.fetch_row()[0][0])
        del data
        conn.close()
        self.m_comboBox1.Clear()
        self.m_comboBox1.AppendItems(schemas)

    def validate(self):
        """validate dialog"""
        sel = self.m_choicebook1.GetSelection()
        if sel == 0:
            self._language = 'c++'
        elif sel == 1:
            self._language = 'python'
        elif sel == 2:
            self._language = 'databases'
        self._name = self.m_textCtrl1.GetValue()
        if len(self._name) == 0:
            wx.MessageBox(
                message="Project name must be non empty",
                caption="Error",
                style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                parent=self)
            return False
        if not self._original == self._name:
            if context.get_app().exist_project(self._name):
                response = wx.MessageBox(
                    message="Project already exist. Overwrite?",
                    caption="Error",
                    style=wx.YES_NO | wx.CENTER | wx.ICON_ERROR,
                    parent=self)
                if response != wx.YES:
                    return False
        self._dir = self.m_textCtrl9.GetValue()
        if len(self._dir) == 0:
            wx.MessageBox(
                message="Target directory must be non empty",
                caption="Error",
                style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                parent=self)
            return False
        if sel in [0, 1]:
            # python and c++
            if self.m_checkBox52.GetValue():
                if len(self.m_textCtrl25.GetValue()) == 0:
                    wx.MessageBox(
                        message="Master include must be non empty",
                        caption="Error",
                        style=wx.OK | wx.CENTER | wx.ICON_ERROR,
                        parent=self)
                    return False
            self._includeDir = self.m_textCtrl23.GetValue()
            self._srcDir = self.m_textCtrl24.GetValue()
            self._useMaster = self.m_checkBox52.GetValue()
            self._masterInclude = self.m_textCtrl25.GetValue()
            if sel == 0:
                self._serial = self.m_checkBox7.GetValue()
                self._transactional = self.m_checkBox8.GetValue()
            self._author = self.m_textCtrl251.GetValue()
            self._date = self.m_datePicker1.GetValue().FormatISODate()
            self._license = self.m_choice16.GetStringSelection()
            self._type = self.m_choice151.GetStringSelection()
            self._version = []
            self._version = [
                self.m_spinCtrl11.GetValue(),
                self.m_spinCtrl21.GetValue(),
                self.m_spinCtrl31.GetValue()]
            self._useMakefile = True
        else:
            self._database_host = self.m_textCtrl252.GetValue()
            self._database_user = self.m_textCtrl231.GetValue()
            self._database_password = self.m_textCtrl241.GetValue()
            self._database_default = self.m_comboBox1.GetValue()
            if not self.test_database_connection(False):
                return False
        return True

    def get_kwargs(self):
        """returns arguments for instance... pending"""
        kwargs = {
            'parent': self._container,
            'language': self._language,
            'name': self._name,
            'dir': self._dir,
        }
        # TODO : Remove custom flavor data
        if self._language in ['c++', 'python']:
            kwargs.update({
                'includedir': self._includeDir,
                'sourcedir': self._srcDir,
                'usemaster': self._useMaster,
                'masterinclude': self._masterInclude,
                'author': self._author,
                'date': self._date,
                'license': self._license,
                'type': self._type,
                'version': self._version,
                'usemakefile': self._useMakefile
            })
            if self._language == 'c++':
                kwargs.update({
                    'serial': self._serial,
                    'transactional': self._transactional
                })
        elif self._language == 'databases':
            kwargs.update({
                'database_host': self._database_host,
                'database_user': self._database_user,
                'database_password': self._database_password,
                'database_default': self._database_default,
            })
        return kwargs

    # Handlers for NewProject events.
    @beatle.lib.wxx.save_position
    def on_ok(self, event):
        """Handle ok button"""
        if self.validate():
            self.EndModal(wx.ID_OK)

    @beatle.lib.wxx.save_position
    def on_cancel(self, event):
        self.EndModal(wx.ID_CANCEL)
