'''
Created on 7 sept. 2018

@author: mel
'''
import copy, wx
from wx import aui

from beatle import local_path
from beatle.lib import wxx
from beatle.lib.tran import TransactionalMethod, format_current_transaction_name
from beatle.lib.wxx import CreationDialog, EditionDialog
from beatle.lib.ostools import clipboard
from beatle.lib.handlers import identifier, SharedAcceleratorHandler
from beatle.activity.models.ui.view import ModelsView

from ..ui.dlg import ModuleDialog, NamespaceDialog, TypeDialog, \
    EnumDialog, InheritanceDialog, SelectFriendsDialog, MemberDialog, ConstructorDialog, \
    MemberMethodDialog, DestructorDialog, MethodDialog, VariableDialog, \
    VirtualMethodsDialog, IsClassMethodsDialog, RelationDialog, \
    SelectContextsDialog, ContextItemsDialog, UserSectionsDialog, \
    ClassDiagramDialog, ClassDialog, ArgumentDialog  # , SelectLibrariesDialog

from ..ui.pane import ConstructorPane, MethodPane, ClassDiagramPane, CCBuildSettingsPane

from ._PreferencesHandler import PreferencesHandler

# from beatle.activity.models.cc.ui.dlg import ClassDiagramDialog, ClassDialog, ArgumentDialog

from beatle.model.cc import ClassDiagram, Module, Namespace, Type, Enum, Class, Inheritance, Friendship, \
    MemberData, Constructor, MemberMethod, Destructor, Argument, Function, Data, IsClassMethod, Relation, \
    typeinst, LibrariesFolder, TypesFolder, Library, InitMethod, ExitMethod, SetterMethod, GetterMethod, \
    RelationFrom, RelationTo, CCProject

from beatle.model import Project

from beatle.lib.api import context


class EventHandlers(object):
    _addClassDiagramId = identifier("ADD_CLASS_DIAGRAM")
    _addModuleId = identifier("ADD_CPP_MODULE")
    _addNamespaceId = identifier("ADD_CPP_NAMESPACE")
    _addTypeId = identifier("ADD_CPP_TYPE")
    _addEnumId = identifier("ADD_CPP_ENUM")
    _addClassId = identifier("ADD_CPP_CLASS")
    _addInheritanceId = identifier("ADD_CPP_INHERITANCE")
    _setFriendshipId = identifier("SET_CPP_FRIENDSHIP")
    _addMemberId = identifier("ADD_CPP_MEMBER")
    _addConstructorId = identifier("ADD_CPP_CONSTRUCTOR")
    _addMethodId = identifier("ADD_CPP_METHOD")
    _addDestructorId = identifier("ADD_CPP_DESTRUCTOR")
    _addArgumentId = identifier("ADD_CPP_ARGUMENT")
    _addFunctionId = identifier("ADD_CPP_FUNCTION")
    _addVariableId = identifier("ADD_CPP_VARIABLE")
    _addVirtualMethodId = identifier("ADD_CPP_VIRTUAL_OVERRIDE")
    _addIsclassMethodId = identifier("ADD_CPP_IS_CLASS")
    _applyRule3Id = identifier("APPLY_CPP_RULEOF3")
    _setPreferredCtorId = identifier("SET_CPP_PREFERRED_CTOR")
    _projectContextsId = identifier("CPP_PROJECT_CONTEXTS")
    _projectLibrariesId = identifier("CPP_PROJECT_LIBRARIES")
    _addRelationId = identifier("CPP_ADD_RELATION")
    _editContextId = identifier("ID_EDIT_CONTEXT")
    _editUserSections = identifier("ID_EDIT_USER_SECTIONS")

    def __init__(self, view):
        self._view = view  # ModelView instance
        # register accelerators
        ctrl_alt = wx.ACCEL_CTRL + wx.ACCEL_ALT
        ctrl_shf = wx.ACCEL_CTRL + wx.ACCEL_SHIFT
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('C'), self._addClassId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('I'), self._addInheritanceId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('M'), self._addMemberId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('F'), self._addMethodId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('F'), self._addFunctionId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('A'), self._addArgumentId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_alt, ord('M'), self._addModuleId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('T'), self._addTypeId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('N'), self._addConstructorId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('D'), self._addDestructorId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('R'), self._addRelationId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('U'), self._editUserSections)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_alt, ord('V'), self._addVariableId)
        SharedAcceleratorHandler.add_accelerator('models view', ctrl_shf, ord('V'), self._addVirtualMethodId)

        # register edit handlers
        ModelsView._edit_map.update({
            self.open_constructor: [Constructor],
            self.open_method: [
                MemberMethod, IsClassMethod, InitMethod, ExitMethod, Destructor,
                SetterMethod, GetterMethod, Function],
            self.open_class_diagram: [ClassDiagram]})

        super(EventHandlers, self).__init__()

    def _bind_events(self):
        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_project_contexts, id=self._projectContextsId)
        self._view.bind_special(wx.EVT_MENU, self.on_project_contexts, id=self._projectContextsId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_class_diagram, id=self._addClassDiagramId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_class_diagram, id=self._addClassDiagramId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_module, id=self._addModuleId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_module, id=self._addModuleId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_namespace, id=self._addNamespaceId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_namespace, id=self._addNamespaceId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_type, id=self._addTypeId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_type, id=self._addTypeId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_enum, id=self._addEnumId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_enum, id=self._addEnumId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_class, id=self._addClassId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_class, id=self._addClassId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_inheritance, id=self._addInheritanceId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_inheritance, id=self._addInheritanceId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_select_friends, id=self._setFriendshipId)
        self._view.bind_special(wx.EVT_MENU, self.on_select_friends, id=self._setFriendshipId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_member, id=self._addMemberId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_member, id=self._addMemberId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_constructor, id=self._addConstructorId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_constructor, id=self._addConstructorId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_method, id=self._addMethodId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_method, id=self._addMethodId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_destructor, id=self._addDestructorId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_destructor, id=self._addDestructorId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_argument, id=self._addArgumentId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_argument, id=self._addArgumentId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_function, id=self._addFunctionId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_function, id=self._addFunctionId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_variable, id=self._addVariableId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_variable, id=self._addVariableId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_virtual_methods, id=self._addVirtualMethodId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_virtual_methods, id=self._addVirtualMethodId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_is_class_methods, id=self._addIsclassMethodId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_is_class_methods, id=self._addIsclassMethodId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_relation, id=self._addRelationId)
        self._view.bind_special(wx.EVT_MENU, self.on_add_relation, id=self._addRelationId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_apply_rule_of_three, id=self._applyRule3Id)
        self._view.bind_special(wx.EVT_MENU, self.on_rule_of_three, id=self._applyRule3Id)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_set_preferred_constructor, id=self._setPreferredCtorId)
        self._view.bind_special(wx.EVT_MENU, self.on_set_preferred_constructor, id=self._setPreferredCtorId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_edit_context, id=self._editContextId)
        self._view.bind_special(wx.EVT_MENU, self.on_edit_context, id=self._editContextId)

        self._view.bind_special(wx.EVT_UPDATE_UI, self.on_update_edit_user_sections, id=self._editUserSections)
        self._view.bind_special(wx.EVT_MENU, self.on_edit_user_sections, id=self._editUserSections)

        # disable until mature
        # self._view.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateProjectLibraries, id=self._projectLibrariesId)
        # self._view.bind_special(wx.EVT_MENU, self.OnProjectLibraries, id=self._projectLibrariesId)

    def set_model_menu(self, menu):
        self._menu = wxx.Menu()

        self._menu_add_class_diagram = wxx.MenuItem(self._menu, self._addClassDiagramId,
                                                    u"Class diagram ...", u"add class diagram", wx.ITEM_NORMAL)
        self._menu_add_class_diagram.SetBitmap(wx.Bitmap(local_path("app/res/classdiagram.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_class_diagram)

        self._menu_add_module = wxx.MenuItem(self._menu, self._addModuleId,
                                             u"Module ...\tCtrl+Alt+M", u"add module", wx.ITEM_NORMAL)
        self._menu_add_module.SetBitmap(wx.Bitmap(local_path("app/res/module.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_module)

        self._menu_add_namespace = wxx.MenuItem(self._menu, self._addNamespaceId,
                                                u"Namespace ...\tCtrl+Shift+N", u"add namespace", wx.ITEM_NORMAL)
        self._menu_add_namespace.SetBitmap(wx.Bitmap(local_path("app/res/namespace.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_namespace)

        self._menu_add_type = wxx.MenuItem(self._menu, self._addTypeId,
                                           u"Type ...\tCtrl+Shift+T", u"add type", wx.ITEM_NORMAL)
        self._menu_add_type.SetBitmap(wx.Bitmap(local_path("app/res/type.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_type)

        self._menu_add_enum = wxx.MenuItem(self._menu, self._addEnumId,
                                           u"Enum...\tCtrl+Shift+E", u"add enum", wx.ITEM_NORMAL)
        self._menu_add_enum.SetBitmap(wx.Bitmap(local_path("app/res/enum.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_enum)

        self._menu_add_class = wxx.MenuItem(self._menu, self._addClassId,
                                            u"Class ...\tCtrl+Shift+C", u"add class", wx.ITEM_NORMAL)
        self._menu_add_class.SetBitmap(wx.Bitmap(local_path("app/res/class.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_class)

        self._menu_add_inheritance = wxx.MenuItem(self._menu, self._addInheritanceId,
                                                  u"Inheritance ...\tCtrl+Shift+I", u"add inheritance", wx.ITEM_NORMAL)
        self._menu_add_inheritance.SetBitmap(wx.Bitmap(local_path("app/res/inheritance.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_inheritance)

        self._menu_select_friends = wxx.MenuItem(self._menu, self._setFriendshipId,
                                                 u"set friends ...", u"Select friend classes", wx.ITEM_NORMAL)
        self._menu_select_friends.SetBitmap(wx.Bitmap(local_path("app/res/friend.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_select_friends)

        self._menu_add_member = wxx.MenuItem(self._menu, self._addMemberId,
                                             u"Member ...\tCtrl+Shift+M", u"add member", wx.ITEM_NORMAL)
        self._menu_add_member.SetBitmap(wx.Bitmap(local_path("app/res/argument.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_member)

        self._menu_add_constructor = wxx.MenuItem(self._menu, self._addConstructorId,
                                                  u"Constructor\tCtrl+Shift+N", u"add constructor", wx.ITEM_NORMAL)
        self._menu_add_constructor.SetBitmap(wx.Bitmap(local_path("app/res/constructor.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_constructor)

        self._menu_add_method = wxx.MenuItem(self._menu, self._addMethodId,
                                             u"Method ...\tCtrl+Shift+F", u"add method", wx.ITEM_NORMAL)
        self._menu_add_method.SetBitmap(wx.Bitmap(local_path("app/res/method.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_method)

        self._menu_add_destructor = wxx.MenuItem(self._menu, self._addDestructorId,
                                                 u"Destructor ...\tCtrl+Shift+D", u"add destructor", wx.ITEM_NORMAL)
        self._menu_add_destructor.SetBitmap(wx.Bitmap(local_path("app/res/destructor.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_destructor)

        self._menu_add_argument = wxx.MenuItem(self._menu, self._addArgumentId,
                                               u"Argument ...\tCtrl+Shift+A", u"add argument", wx.ITEM_NORMAL)
        self._menu_add_argument.SetBitmap(wx.Bitmap(local_path("app/res/argument.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_argument)

        self._menu_add_function = wxx.MenuItem(self._menu, self._addFunctionId,
                                               u"Function ...\tCtrl+Shift+F", u"add function", wx.ITEM_NORMAL)
        self._menu_add_function.SetBitmap(wx.Bitmap(local_path("app/res/function.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_function)

        self._menu_add_variable = wxx.MenuItem(self._menu, self._addVariableId,
                                               u"Variable ...\tCtrl+Alt+V", u"add variable", wx.ITEM_NORMAL)
        self._menu_add_variable.SetBitmap(wx.Bitmap(local_path("app/res/data.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_variable)

        self._menu_add_virtual_methods = wxx.MenuItem(self._menu, self._addVirtualMethodId,
                                                      u"Virtual methods ...\tCtrl+Shift+V",
                                                      u"show virtual methods dialog", wx.ITEM_NORMAL)
        self._menu_add_virtual_methods.SetBitmap(wx.Bitmap(local_path("app/res/virtual.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_add_virtual_methods)

        self._submenu_generators = wxx.Menu()
        self._menu_add_is_class_methods = wxx.MenuItem(self._submenu_generators, self._addIsclassMethodId,
                                                       u"is_class methods ...\tCtrl+Alt+I",
                                                       u"show is_class methods dialog", wx.ITEM_NORMAL)
        self._menu_add_is_class_methods.SetBitmap(wx.Bitmap(local_path("app/res/is_class.xpm"), wx.BITMAP_TYPE_ANY))
        self._submenu_generators.AppendItem(self._menu_add_is_class_methods)

        self._menu_add_relation = wxx.MenuItem(self._submenu_generators, self._addRelationId,
                                               u"Relation ...\tCtrl+Shift+R", u"add relation", wx.ITEM_NORMAL)
        self._menu_add_relation.SetBitmap(wx.Bitmap(local_path("app/res/relation.xpm"), wx.BITMAP_TYPE_ANY))
        self._submenu_generators.AppendItem(self._menu_add_relation)

        self._menu_apply_rule_of_three = wxx.MenuItem(self._submenu_generators, self._applyRule3Id,
                                                      u"Rule of three\tCtrl+3", u"apply rule of three", wx.ITEM_NORMAL)
        self._menu_apply_rule_of_three.SetBitmap(wx.Bitmap(local_path("app/res/tres.xpm"), wx.BITMAP_TYPE_ANY))
        self._submenu_generators.AppendItem(self._menu_apply_rule_of_three)

        self._menu.AppendSubMenu(self._submenu_generators, u"Generators")

        self._menu_set_preferred_constructor = wxx.MenuItem(self._menu, self._setPreferredCtorId,
                                                            u"Set preferred ctor", u"set preferred constructor",
                                                            wx.ITEM_CHECK)
        self._menu.AppendItem(self._menu_set_preferred_constructor)

        self._menu.AppendSeparator()
        # project contexts
        self._menu_edit_project_contexts = wxx.MenuItem(self._menu, self._projectContextsId,
                                                        u"Project contexts", u"setup project contexts", wx.ITEM_NORMAL)
        self._menu_edit_project_contexts.SetBitmap(wx.Bitmap(local_path("app/res/context.xpm"), wx.BITMAP_TYPE_ANY))
        self._menu.AppendItem(self._menu_edit_project_contexts)
        # project libraries : disable until mature
        # self._projectLibraries = wxx.MenuItem(self._menu, self._projectLibrariesId,
        #     u"Project libraries", u"add/remove project libraries", wx.ITEM_NORMAL)
        # self._projectLibraries.SetBitmap(wx.Bitmap(local_path("app/res/library.xpm"), wx.BITMAP_TYPE_ANY))
        # self._menu.Append(self._projectLibraries)

        menu.AppendSubMenu(self._menu, u"c++")

    def _create_toolbars(self):
        """Create the associated toolbars"""
        self._toolbar = aui.AuiToolBar(context.get_frame(), wx.ID_ANY, wx.DefaultPosition,
                                       wx.Size(16, 16),
                                       style=aui.AUI_TB_GRIPPER | aui.AUI_TB_HORZ_LAYOUT | aui.AUI_TB_PLAIN_BACKGROUND)
        els = [
            self._menu_add_argument,
            self._menu_add_namespace,
            self._menu_add_type,
            self._menu_add_enum,
            self._menu_add_class,
            self._menu_add_inheritance,
            self._menu_select_friends,
            self._menu_add_member,
            self._menu_add_constructor,
            self._menu_add_method,
            self._menu_add_destructor,
            self._menu_add_argument,
            self._menu_add_function,
            self._menu_add_variable,
            None,
            self._menu_add_relation,
            self._menu_edit_project_contexts,
            # self._projectLibraries
        ]

        self._toolbar.SetMinSize(wx.Size(24, 24))
        for el in els:
            if el is None:
                self._toolbar.AddSeparator()
            else:
                self._toolbar.AddTool(el.GetId(), el.GetLabel(), el.GetBitmap())
        self._toolbar.Realize()
        frame = context.get_frame()
        frame.m_mgr.AddPane(self._toolbar, aui.AuiPaneInfo().Name(u"cpp_toolbar").Top().
                            Hide().Caption(u"c++").PinButton(True).Gripper().Dock().Resizable().
                            DockFixed(False).Row(0).Position(7).Layer(10).ToolbarPane())
        self._view.register_toolbar(self._toolbar)

    def update_menus(self):
        self._menu_edit_project_contexts.Enable(self.can_edit_contexts)
        self._menu_add_class_diagram.Enable(self.can_add_class_diagram)
        self._menu_add_module.Enable(self.can_add_module)
        self._menu_add_namespace.Enable(self.can_add_namespace)
        self._menu_add_type.Enable(self.can_add_type)
        self._menu_add_enum.Enable(self.can_add_enum)
        self._menu_add_class.Enable(self.can_add_class)
        self._menu_add_inheritance.Enable(self.can_add_inheritance)
        self._menu_select_friends.Enable(self.can_select_friends)
        self._menu_add_member.Enable(self.can_add_member)
        self._menu_add_constructor.Enable(self.can_add_constructor)
        self._menu_add_method.Enable(self.can_add_method)
        self._menu_add_destructor.Enable(self.can_add_destructor)
        self._menu_add_argument.Enable(self.can_add_argument)
        self._menu_add_function.Enable(self.can_add_function)
        self._menu_add_variable.Enable(self.can_add_variable)
        self._menu_add_virtual_methods.Enable(self.can_add_virtual_methods)
        self._menu_add_is_class_methods.Enable(self.can_add_is_class_methods)
        self._menu_add_relation.Enable(self.can_add_relation)
        self._menu_apply_rule_of_three.Enable(self.can_apply_rule_of_three)
        self._menu_set_preferred_constructor.Enable(self.can_set_preferred_constructor)
        enabled = self.can_set_preferred_constructor
        self._menu_set_preferred_constructor.Enable(enabled)
        self._menu_set_preferred_constructor.Check(enabled and self.selected_is_preferred_constructor)
        self._menu_edit_project_contexts.Enable(self.can_edit_contexts)
        # TODO : review this
        #self._view.menu_edit_user_sections.Enable(self.can_edit_user_sections)

    @property
    def can_paste(self):
        """Indicate if it's possible to paste'"""
        selected = self._view.selected
        if selected is None:
            return False
        project = selected.project
        if project is None or getattr(project, '_language', None) != 'c++':
            return False
        data = clipboard.data
        if data.lang != 'c++':
            return False
        s_type = data.args.get('type', None)
        if data.type in [Argument, MemberData]:
            if not s_type:
                return False  # may be an exception, because that's not allowd
            # kwnow types
            if s_type._type.scoped in [x.scoped for x in selected.types]:
                return True
            # kwnow types??
            if s_type.type_name in selected.template_types:
                return True
            return False
        elif data.type is Constructor:
            parent = data.args.get('parent', None)
            if parent is not None:
                if parent == selected or parent.inner_class == selected.inner_class:
                    return True
            return False
        elif data.type in [MemberMethod, IsClassMethod]:
            if not selected.inner_member_container:
                return False
            if not s_type:
                return False  # may be an exception, because that's not allowd
            s_typenames = [x.name for x in selected.types]
            if s_type.type_name in s_typenames:
                return True
            t_typenames = selected.template_types
            if s_type.type_name in selected.template_types:
                return True
            # check again the arguments
            childs = data.args.get('child_kwargs', {})
            if Argument in childs:
                for arg in childs[Argument]:
                    s_type = arg.get('type', None)
                    if not s_type:
                        return False
                    if s_type.type_name in s_typenames:
                        continue
                    if s_type.type_name in t_typenames:
                        continue
                    return False
            return True
        elif data.type in [Function]:
            if not selected.inner_function_container:
                return False
            if not s_type:
                return False
            s_typenames = [x.name for x in selected.types]
            if s_type.type_name in s_typenames:
                return True
            t_typenames = selected.template_types
            if s_type.type_name in t_typenames:
                return True
            # check again the arguments
            childs = data.args.get('child_kwargs', {})
            if Argument in childs:
                for arg in childs[Argument]:
                    s_type = arg.get('type', None)
                    if not s_type:
                        return False
                    if s_type.type_name in s_typenames:
                        continue
                    if s_type.type_name in t_typenames:
                        continue
                    return False
            return True
        elif data.type in [Type]:
            container = selected.inner_type_container
            if not container:
                return False
            return data.args.get('name', 'int') not in [x.name for x in container.types]
        elif data.type is Module:
            if len(data.args.get('child_kwargs', [])) == 0:
                return True
        return False

    @property
    def can_cut(self):
        """return info about the feasibility of cut"""
        obj = self._view.selected
        if obj is None:
            return False
        if getattr(obj, '_read_only', False):
            return False
        project = obj.project
        if project is None or getattr(project, '_language', None) != 'c++':
            return False
        return type(obj) not in [
            # LibrariesFolder,
            TypesFolder, Inheritance, RelationFrom, RelationTo, Class]

    @property
    def can_copy(self):
        """Inticate if it's possible to copy'"""
        obj = self._view.selected
        if obj is None:
            return False
        project = obj.project
        if project is None or getattr(project, '_language', None) != 'c++':
            return False
        return type(obj) not in [
            # LibrariesFolder,
            TypesFolder, Inheritance, RelationFrom, RelationTo, Class]

    @property
    def can_edit_open(self):
        """it's enabled editing?'"""
        obj = self._view.selected
        if obj is None:
            return False
        project = obj.project
        if project is None or getattr(project, '_language', None) != 'c++':
            return False
        if type(obj) in [Constructor, Destructor, Function, MemberMethod, SetterMethod, GetterMethod,
                         InitMethod, ExitMethod, IsClassMethod, ClassDiagram]:
            return True
        return False

    @property
    def aggregated_preferences(self):
        obj = self._view.selected
        if obj is not None:
            project = obj.project
            if project and project.language == 'c++':
                from beatle.activity.models.cc.handlers import PreferencesHandler
                return PreferencesHandler(project)
        return False

    def on_edit_paste(self, event):
        """Handle paste event"""
        data = clipboard.data
        if getattr(data, 'lang', None) != 'c++':
            return False
        # The paste event must check about collision
        if data.type is Argument:
            self.paste_argument()
        elif data.type is MemberData:
            self.paste_member_data()
        elif data.type is Constructor:
            self.paste_constructor()
        elif data.type in [MemberMethod, IsClassMethod]:
            self.paste_member_method()
        elif data.type is Function:
            self.paste_function()
        elif data.type is Type:
            self.paste_type()
        elif data.type is Module:
            self.paste_module()
        else:
            return False
        return True

    @TransactionalMethod('paste c++ argument')
    def paste_argument(self):
        """Handle paste argument"""
        test = True
        data = clipboard.data
        name = data.args['name']
        target = self._view.selected.inner_argument_container
        next_name = None
        if name in [x.name for x in target[Argument]]:
            split = min([i for i in range(0, len(name)) if name[i:].isdigit()] + [len(name)])
            pre = name[0:split]
            index = int('0' + name[split:]) + 1
            while test:
                next_name = pre + str(index)
                if next_name in [x._name for x in target[Argument]]:
                    index = index + 1
                else:
                    test = False
            name = next_name
        kwargs = copy.copy(data.args)
        kwargs['name'] = name
        kwargs['parent'] = target
        kwargs['child_index'] = -1
        Argument(**kwargs)
        return True

    @TransactionalMethod('paste c++ member')
    def paste_member_data(self):
        """Handle paste member"""
        data = clipboard.data
        name = data.args['name']
        members = self._view.selected.inner_class(MemberData)
        # check if that name is already in the target
        if name in [x.name for x in members]:
            split = min([i for i in range(0, len(name)) if name[i:].isdigit()] + [len(name)])
            pre = name[0:split]
            index = int('0' + name[split:])
            name = pre
            while name in [x.name for x in members]:
                index = index + 1
                name = pre + str(index)
        kwargs = copy.copy(data.args)
        kwargs['name'] = name
        kwargs['parent'] = self._view.selected.inner_member_container
        MemberData(**kwargs)
        return True

    @TransactionalMethod('paste c++ constructor')
    def paste_constructor(self):
        """Handle paste constructor"""
        # pasting a constructor is easy, we dont need to check anything more
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['autoargs'] = False  # prevent argument deduction
        kwargs['parent'] = self._view.selected.inner_member_container
        Constructor(**kwargs)
        return True

    @TransactionalMethod('paste c++ member method')
    def paste_member_method(self):
        """Handle paste method"""
        # pasting a method is easy, we dont need to check anything more
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self._view.selected.inner_member_container
        MemberMethod(**kwargs)
        return True

    @TransactionalMethod('paste c++ function')
    def paste_function(self):
        """Handle paste function"""
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self._view.selected.inner_function_container
        Function(**kwargs)
        return True

    @TransactionalMethod('paste c++ type')
    def paste_type(self):
        """Handle paste function"""
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self._view.selected.inner_type_container
        Type(**kwargs)
        return True

    @TransactionalMethod('paste c++ module')
    def paste_module(self):
        """This can be only done when the module
        is naked, that is, there are no other contents that
        user sections."""
        data = clipboard.data
        kwargs = copy.copy(data.args)
        kwargs['parent'] = self._view.selected.inner_module_container
        Module(**kwargs)
        return True

    def on_delete(self, event):
        """delete element"""
        obj = self._view.selected
        if type(obj) in [RelationFrom, RelationTo]:
            # if delete some side of relation, we must delete both sides
            # then, redirecting to hidden parent is what we do
            obj.key.delete()
            return True
        return False

    @property
    def can_add_class_diagram(self):
        v = self._view.selected
        return bool(v and getattr(v.project, '_language', None) == 'c++' and v.inner_diagram_container)

    def on_update_add_class_diagram(self, event):
        """Update add class diagram"""
        event.Enable(self.can_add_class_diagram)

    @TransactionalMethod('add class diagram {0}')
    @CreationDialog(ClassDiagramDialog, ClassDiagram)
    def on_add_class_diagram(self, event):
        """add class diagram"""
        return context.get_frame(), self._view.selected.inner_diagram_container

    @property
    def can_add_module(self):
        v = self._view.selected
        return bool(v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_module_container)

    def on_update_add_module(self, event):
        """Update add module"""
        event.Enable(self.can_add_module)

    @TransactionalMethod('add c++ module {0}')
    @CreationDialog(ModuleDialog, Module)
    def on_add_module(self, event):
        """Handle add module method"""
        if not self.can_add_module:
            return False
        return context.get_frame(), self._view.selected.inner_module_container

    @property
    def can_add_namespace(self):
        v = self._view.selected
        return bool(v and getattr(v.project, '_language', None) == 'c++' and v.inner_namespace_container)

    def on_update_add_namespace(self, event):
        """Update Add namespace method"""
        event.Enable(self.can_add_namespace)

    @TransactionalMethod('add namespace {0}')
    @CreationDialog(NamespaceDialog, Namespace)
    def on_add_namespace(self, event):
        """Handles add namespace method"""
        return context.get_frame(), self._view.selected.inner_namespace_container

    @property
    def can_add_type(self):
        v = self._view.selected
        return bool(v and v.inner_type_container)

    def on_update_add_type(self, event):
        """Update AddType method"""
        event.Enable(self.can_add_type)

    @TransactionalMethod('add type {0}')
    @CreationDialog(TypeDialog, Type)
    def on_add_type(self, event):
        """Handles add type method"""
        return context.get_frame(), self._view.selected.inner_type_container

    @property
    def can_add_enum(self):
        v = self._view.selected
        return bool(v and getattr(v.project, '_language', None) == 'c++' and v.inner_enum_container)

    def on_update_add_enum(self, event):
        """Update AddType method"""
        event.Enable(self.can_add_enum)

    @TransactionalMethod('add enum {0}')
    @CreationDialog(EnumDialog, Enum)
    def on_add_enum(self, event):
        """Handles add enum method"""
        return context.get_frame(), self._view.selected.inner_enum_container

    @property
    def can_add_class(self):
        v = self._view.selected
        return bool(v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_class_container)

    def on_update_add_class(self, event):
        """Updade AddClass method"""
        event.Enable(self.can_add_class)

    @TransactionalMethod('add c++ class {0}')
    @CreationDialog(ClassDialog, Class)
    def on_add_class(self, event):
        """Handles add class method"""
        if not self.can_add_class:
            return False
        return context.get_frame(), self._view.selected.inner_class_container

    @property
    def can_add_inheritance(self):
        v = self._view.selected
        return bool(
            v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_inheritance_container)

    def on_update_add_inheritance(self, event):
        """Handle update add c++ inheritance"""
        event.Enable(self.can_add_inheritance)

    @TransactionalMethod('add c++ {0} inheritance')
    @CreationDialog(InheritanceDialog, Inheritance)
    def on_add_inheritance(self, event):
        """Handle add inheritance command"""
        if not self.can_add_inheritance:
            return False
        return context.get_frame(), self._view.selected.inner_inheritance_container

    @property
    def can_select_friends(self):
        v = self._view.selected
        return bool(v and getattr(v.project, '_language', None) == 'c++' and v.inner_class)

    def on_update_select_friends(self, event):
        """Handle update select friends event"""
        event.Enable(self.can_select_friends)

    @TransactionalMethod('select friends')
    @CreationDialog(SelectFriendsDialog, Friendship)
    def on_select_friends(self, event):
        """Handle select friends"""
        return context.get_frame(), self._view.selected

    @property
    def can_add_member(self):
        v = self._view.selected
        return bool(v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_member_container)

    def on_update_add_member(self, event):
        """Update add member"""
        event.Enable(self.can_add_member)

    @TransactionalMethod('add c++ {0} class member')
    @CreationDialog(MemberDialog, MemberData)
    def on_add_member(self, event):
        """Handle add member command"""
        if not self.can_add_member:
            return False
        return context.get_frame(), self._view.selected.inner_member_container

    @property
    def can_add_constructor(self):
        v = self._view.selected
        return bool(v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_member_container)

    def on_update_add_constructor(self, event):
        """Update Add constructor method"""
        event.Enable(self.can_add_constructor)

    @TransactionalMethod('add {0} constructor')
    @CreationDialog(ConstructorDialog, Constructor)
    def on_add_constructor(self, event):
        """Handle add constructor command"""
        if not self.can_add_constructor:
            return False
        return context.get_frame(), self._view.selected.inner_member_container

    @property
    def can_add_method(self):
        v = self._view.selected
        return bool(v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_member_container)

    def on_update_add_method(self, event):
        """Update add method """
        event.Enable(self.can_add_method)

    @TransactionalMethod('add c++ method {0}')
    @CreationDialog(MemberMethodDialog, MemberMethod)
    def on_add_method(self, event):
        """Handle add method command"""
        if not self.can_add_method:
            return False
        return context.get_frame(), self._view.selected.inner_member_container

    @property
    def can_add_destructor(self):
        v = self._view.selected
        v = v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_class
        return bool(v and not v(Destructor, filter=lambda x: x.inner_class == v, cut=True))

    def on_update_add_destructor(self, event):
        """Update Add destructor method"""
        event.Enable(self.can_add_destructor)

    @TransactionalMethod('add {0} destructor')
    @CreationDialog(DestructorDialog, Destructor)
    def on_add_destructor(self, event):
        """Handle add destructor command"""
        if not self.can_add_destructor:
            return False
        return context.get_frame(), self._view.selected.inner_member_container

    @property
    def can_add_argument(self):
        v = self._view.selected
        return bool(v and not v.read_only and v.project and
                    getattr(v.project, '_language', None) == 'c++' and v.inner_argument_container)

    def on_update_add_argument(self, event):
        """Update add argument method"""
        event.Enable(self.can_add_argument)

    @TransactionalMethod('add c++ method argument {0}')
    @CreationDialog(ArgumentDialog, Argument)
    def on_add_argument(self, event):
        """Handle add method command"""
        if not self.can_add_argument:
            return False
        return context.get_frame(), self._view.selected.inner_argument_container

    @property
    def can_add_function(self):
        v = self._view.selected
        return bool(v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_function_container)

    def on_update_add_function(self, event):
        """Handle update add function method"""
        event.Enable(self.can_add_function)

    @TransactionalMethod('add c++ method {0}')
    @CreationDialog(MethodDialog, Function)
    def on_add_function(self, event):
        """Handle add function command"""
        if not self.can_add_function:
            return False
        return context.get_frame(), self._view.selected.inner_function_container

    @property
    def can_add_variable(self):
        v = self._view.selected
        return bool(v and getattr(v.project, '_language', None) == 'c++' and v.inner_variable_container)

    def on_update_add_variable(self, event):
        """Update AddVariable method"""
        event.Enable(self.can_add_variable)

    @TransactionalMethod('add c++ variable {0}')
    @CreationDialog(VariableDialog, Data)
    def on_add_variable(self, event):
        """Handles add variable method"""
        return context.get_frame(), self._view.selected.inner_variable_container

    @property
    def can_add_virtual_methods(self):
        v = self._view.selected
        return bool(v and getattr(v.project, '_language', None) == 'c++' and v.inner_member_container)

    def on_update_add_virtual_methods(self, event):
        """Update AddVariable method"""
        event.Enable(self.can_add_virtual_methods)

    @TransactionalMethod('add virtual methods')
    @CreationDialog(VirtualMethodsDialog, MemberMethod)
    def on_add_virtual_methods(self, event):
        """Handles add variable method"""
        return context.get_frame(), self._view.selected.inner_member_container

    @property
    def can_add_is_class_methods(self):
        v = self._view.selected
        return bool(v and getattr(v.project, '_language', None) == 'c++'
                    and v.inner_class and len(v.inner_class._deriv) > 0)

    def on_update_add_is_class_methods(self, event):
        """Update on_update_add_is_class_methods method"""
        event.Enable(self.can_add_is_class_methods)

    @TransactionalMethod('add is_class methods')
    @CreationDialog(IsClassMethodsDialog, IsClassMethod)
    def on_add_is_class_methods(self, event):
        """Handle add is_class method"""
        return context.get_frame(), self._view.selected.inner_member_container

    @property
    def can_add_relation(self):
        v = self._view.selected
        return bool(v and v.project and getattr(v.project, '_language', None) == 'c++' and v.inner_relation_container)

    def update_element(self, obj):
        """Do cosmetic updates to object after inserted"""
        if type(obj) is Constructor:
            self._view.m_tree.SetItemBold(obj, bold=obj.is_preferred())

    def on_update_add_relation(self, event):
        """Update add relation method"""
        event.Enable(self.can_add_relation)

    def on_add_relation(self, event):
        """Handle add relation method"""
        if not self.can_add_relation:
            return False
        self.add_relation(self._view.selected.inner_relation_container)

    @TransactionalMethod('add relation')
    @CreationDialog(RelationDialog, Relation)
    def add_relation(self, _from, _to=None):
        """Handle add relation method"""
        return context.get_frame(), _from, _to

    @property
    def can_apply_rule_of_three(self):
        # we need to have class
        v = self._view.selected
        cls = None
        if v is not None and getattr(v.project, '_language', None) == 'c++':
            cls = v.inner_class
        if cls is None:
            return False
        # ok, rule of three is applicable only if not yet applied

        def self_class(x):
            return x.inner_class == cls

        dtor = cls(Destructor, filter=self_class, cut=True)
        ctor = cls(Constructor, filter=self_class, cut=True)
        copy_op = cls(MemberMethod, filter=lambda x: self_class(x) and x.name == 'operator =')
        has_dtor = (len(dtor) > 0)
        has_ctor = False
        for t in ctor:
            args = t[Argument]
            if len(args) != 1:
                continue
            ti = args[0].type_instance
            if ti.type == cls and ti.is_const and ti.is_ref and not ti.is_ptr and not ti.is_array:
                has_ctor = True
                break
        has_copy_op = False
        for t in copy_op:
            args = t[Argument]
            if len(args) != 1:
                continue
            ti = args[0].type_instance
            if ti.type == cls and ti.is_const and ti.is_ref and not ti.is_ptr and not ti.is_array:
                has_copy_op = True
                break
        return bool(cls and (has_ctor or has_dtor or has_copy_op) and not (has_ctor and has_dtor and has_copy_op))

    def on_update_apply_rule_of_three(self, event):
        """Update rule of three"""
        # we need to have class
        event.Enable(self.can_apply_rule_of_three)

    @TransactionalMethod('apply rule of three')
    def on_rule_of_three(self, event):
        """Apply rule of three"""
        v = self._view.selected
        cls = v and v.inner_class
        if not cls:
            return
        # ok, rule of three is applicable only if not yet applied

        def self_class(x):
            return x.inner_class == cls

        dtor = cls(Destructor, filter=self_class, cut=True)
        ctor = cls(Constructor, filter=self_class, cut=True)
        copy_op = cls(MemberMethod, filter=lambda x: self_class(x) and x.name == 'operator =')
        if len(dtor) == 0:
            # Create new destructor
            Destructor(parent=cls, serial=cls._serial, note="Destructor created by rule of three")
        has_ctor = False
        for t in ctor:
            args = t[Argument]
            if len(args) != 1:
                continue
            ti = args[0].type_instance
            if ti.type == cls and ti.is_const and ti.is_ref and not ti.is_ptr and not ti.is_array:
                has_ctor = True
                break
        if not has_ctor:
            # Create new constructor
            ctor = Constructor(parent=cls, name=cls._name, autoargs=False,
                               note="Constructor created by rule of three")
            Argument(parent=ctor,
                     type=typeinst(type=cls, ref=True, const=True),
                     name='ref{0}'.format(cls._name))
        has_copy_op = False
        for t in copy_op:
            args = t[Argument]
            if len(args) != 1:
                continue
            ti = args[0].type_instance
            if ti.type == cls and ti.is_const and ti.is_ref and not ti.is_ptr and not ti.is_array:
                has_copy_op = True
                break
        if not has_copy_op:
            # create copy assignment
            # travel through members and copy them
            code = '    //Autogenerated copy\n'
            # Travel through bases with copy operator (not virtual)
            bases = [x.ancestor for x in cls(Inheritance,
                                             filter=lambda x: self_class(x) and not x.virtual)]
            for base in bases:
                candidates = base(MemberMethod,
                                  filter=lambda x: x.inner_class == base and x.name == 'operator =')
                for copy_op in candidates:
                    args = copy_op[Argument]
                    if len(args) != 1:
                        continue
                    ti = args[0].type_instance
                    if ti.type != base or not ti.is_const or not ti.is_ref or ti.is_ptr or ti.is_array:
                        continue
                    # Ok, this base has copy operator
                    code += '    {base.scope}{base._name}(ref{cls._name});\n'.format(base=base, cls=cls)
                    break
            # ok now do the same for members
            for member in cls(MemberData, filter=lambda x: self_class(x) and not x.static):
                code += '    {v} = ref{c}.{v};\n'.format(v=member.prefixed_name, c=cls._name)
            code += '\treturn *this;'
            copy_op = MemberMethod(parent=cls,
                                   name='operator =',
                                   type=typeinst(type=cls, ref=True, const=True),
                                   content=code,
                                   note="Copy assignment created by rule of three")
            Argument(parent=copy_op,
                     type=typeinst(type=cls, ref=True, const=True), name='ref{0}'.format(cls.name))
        return True

    @property
    def can_set_preferred_constructor(self):
        v = self._view.selected
        return bool(type(v) is Constructor and v)

    @property
    def selected_is_preferred_constructor(self):
        v = self._view.selected
        v = type(v) is Constructor and v
        return bool(v and v._preferred)

    def on_update_set_preferred_constructor(self, event):
        """Update set preferred ctor"""
        enabled = self.can_set_preferred_constructor
        event.Enable(enabled)
        event.Check(enabled and self.selected_is_preferred_constructor)

    @TransactionalMethod('Set preferred constructor')
    def on_set_preferred_constructor(self, event):
        """Handle set preferred ctor command"""
        v = self._view.selected
        pref = v.inner_class.get_preferred_constructor()
        if pref is not None:
            if pref.is_preferred():
                pref.save_state()
                pref.set_preferred(False)
                self._view.update_element(pref)
            else:
                pref = None
        if v is not pref:
            v.save_state()
            v.set_preferred(True)
            self._view.update_element(v)
        return True

    @property
    def can_edit_context(self):
        """Is context editable?"""
        selected_object = self._view.selected
        if selected_object:
            if getattr(selected_object.project, '_language', None) == 'c++':
                if type(selected_object) not in [TypesFolder, Type, Namespace, Argument, Project]:
                    return True
        return False

    def on_update_edit_context(self, event):
        """Handle update edit context"""
        event.Enable(self.can_edit_context)

    @TransactionalMethod('apply contexts to {0}')
    def on_edit_context(self, event):
        """Handle edit context event"""
        if not self.can_edit_context:
            return False
        selected_object = self._view.selected
        dialog = SelectContextsDialog(context.get_frame(), selected_object)
        if dialog.ShowModal() != wx.ID_OK:
            return False
        selected_object.save_state()
        dialog.copy_attributes(selected_object)
        selected_object.project.modified = True
        # Many kind of transactional actions allow to
        # include the name of an object over what
        # the transactional action operates. For
        # example, in "delete class A" the 'A' is
        # the name of the object deleted by a
        # transactional delete.
        # These kind of transactional operations
        # are frequently labeled with a format
        # string, like "delete class {name}"
        format_current_transaction_name(selected_object.name)
        return True

    @property
    def can_edit_contexts(self):
        v = self._view.selected
        return bool(v and v.project and v.project and v.project.language == 'c++')

    def on_update_project_contexts(self, event):
        """Update project contexts if any"""
        event.Enable(self.can_edit_contexts)

    @TransactionalMethod('edit contexts')
    def on_project_contexts(self, event):
        """Handle project contexts"""
        if not self.can_edit_contexts:
            return False
        project = self._view.selected.project
        d = ContextItemsDialog(context.get_frame(), project)
        if d.ShowModal() != wx.ID_OK:
            return False
        project.save_state()
        if project.use_master_include:
            try:
                project.write_master_header(project.sorted_classes, True)
            except AssertionError as e:
                wx.MessageBox(f'error {str(e)}')
                return False
        d.copy_attributes(project)
        project.modified = True
        return True

    @property
    def can_edit_user_sections(self):
        """The user sections is editable for selected object?"""
        selected = self._view.selected
        if getattr(selected, 'read_only', True) is True:
            return False
        return type(selected) is Class or type(selected) in CCProject.module_classes

    def on_update_edit_user_sections(self, event):
        """Update edit user sections method"""
        event.Enable(self.can_edit_user_sections)

    def on_edit_user_sections(self, event):
        """Edit user sections in a class"""
        # some accelerator have detected to reach here even if not allowed
        if self.can_edit_user_sections:
            self.do_edit_user_sections()

    @TransactionalMethod('edit {0} user sections')
    @EditionDialog(UserSectionsDialog)
    def do_edit_user_sections(self):
        """Edit user sections in class"""
        return context.get_frame(), self._view.selected

    #     def OnUpdateProjectLibraries(self, event):
    #         """Update libraries"""
    #         obj = self._view.selected
    #         obj = obj and obj.project
    #         event.Enable(bool(obj and obj._language == 'c++'))

    #     @TransactionalMethod('select libraries')
    #     @CreationDialog(SelectLibrariesDialog, Library)
    #     def OnProjectLibraries(self, event):
    #         """Handle select libraries event"""
    #         return (context.get_frame(), self._view.selected.project)

    def on_tree_selection_changed(self, event):
        if not wx.GetKeyState(wx.WXK_CONTROL):
            return
        prev_selected = self._view.selected
        if type(prev_selected) is not Class:
            return
        selected = self._view.m_tree.GetSelection()
        if type(selected) is not Class:
            return
        if selected.project == prev_selected.project:
            self.add_relation(prev_selected, selected)

    def open_constructor(self, constructor, view=None):
        """Open constructor code for editing"""
        assert (not hasattr(constructor, '_pane') or constructor._pane is None)
        frame = self._view.frame
        this_pane = ConstructorPane(frame.docBook, frame, constructor)
        constructor._pane = this_pane
        frame.docBook.AddPage(this_pane, constructor.tab_label, True, bitmap=constructor.get_tab_bitmap())

    def open_method(self, method, view=None):
        """Open method code for editing"""
        assert (not hasattr(method, '_pane') or method._pane is None)
        frame = self._view.frame
        this_pane = MethodPane(frame.docBook, frame, method)
        method._pane = this_pane
        frame.docBook.AddPage(this_pane, method.tab_label, True, bitmap=method.get_tab_bitmap())

    def open_class_diagram(self, diagram):
        """Open the class diagram for show"""
        assert not hasattr(diagram, '_pane') or diagram._pane is None
        frame = self._view.frame
        this_pane = ClassDiagramPane(frame.docBook, diagram)
        diagram._pane = this_pane
        frame.docBook.AddPage(this_pane, diagram.tab_label, True, diagram.bitmap_index)
        # diag._pane.notify_show()
