import wx


class PreferencesHandler(object):
    """This class handles the preferences for a C++ project"""
    def __init__(self, project):
        self._build_settings = None
        self._debug_settings = None
        self._project = project

    def add_pages(self, book):
        """Required method. The book argument is a wx.Treebook where
        add the required preferences"""
        from ..ui.pane import CCBuildSettingsPane, CCDebugSettingsPane
        self._build_settings = CCBuildSettingsPane(book, self._project)
        self._debug_settings = CCDebugSettingsPane(book, self._project)
        book.AddPage(self._build_settings, u"C++ build settings", False, wx.NOT_FOUND)
        book.AddPage(self._debug_settings, u"C++ debug settings", False, wx.NOT_FOUND)

    def save(self):
        self._build_settings.save()
        self._debug_settings.save()
        self._project.modified = True
        return True
