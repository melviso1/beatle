# -*- coding: utf-8 -*-
# by mel

# Custom editor wrapper for C++ with  wx.Scintilla control
import re

import wx
import wx.stc as stc

from beatle.lib.api import context
from beatle.lib import wxx
from beatle.lib.handlers import identifier

MARGIN_LINE_NUMBERS = 0
MARK_MARGIN = 1
MARGIN_FOLD = 2

stc.STC_MASK_MARKERS = ~stc.STC_MASK_FOLDERS

MARK_BOOKMARK = 0
MARK_BREAKPOINT_ENABLED = 1
MARK_BREAKPOINT_DISABLED = 2
MARK_BREAKPOINT_DELETED = 3  # This is not a real market but a commodity for status
DEBUG_CURRENT = 32

#missing indicators
STC_INDICATOR_FULL_BOX = 16

#undefined wrappers
SCI_ANNOTATIONSETTEXT = 2540
SCI_ANNOTATIONGETTEXT = 2541
SCI_ANNOTATION_SET_STYLE = 2542
SCI_ANNOTATION_GET_STYLE = 2543
SCI_ANNOTATION_SET_STYLES = 2544
SCI_ANNOTATION_GET_STYLES = 2545
SCI_ANNOTATION_GET_LINES = 2546
SCI_ANNOTATION_CLEAR_ALL = 2547
ANNOTATION_HIDDEN = 0
ANNOTATION_STANDARD = 1
ANNOTATION_BOXED = 2
SCI_ANNOTATION_SET_VISIBLE = 2548
SCI_ANNOTATION_GET_VISIBLE = 2549
SCI_ANNOTATION_SET_STYLE_OFFSET = 2550
SCI_ANNOTATION_GET_STYLE_OFFSET = 2551

SCI_STYLE_SET_CHANGEABLE = 2099
STC_FIND_MATCH_CASE = 4
STC_FIND_POSIX = 4194304
STC_FIND_REGEXP = 2097152
STC_FIND_WHOLE_WORD = 2
STC_FIND_WORD_START = 1048576


BOOKMARK_TOGGLE  = 30000
BOOKMARK_UP      = 30001
BOOKMARK_DOWN    = 30002
BREAKPOINT_TOGGLE= 30003
FIND = 30004
FIND_NEXT = 30005
FIND_PREV = 30006

ITERATOR_WIZARD = identifier("ID_ITERATOR_WIZARD")
ID_UNDO = identifier('ID_UNDO')
ID_REDO = identifier('ID_REDO')
ID_COPY = identifier('ID_COPY')
ID_CUT = identifier('ID_CUT')
ID_PASTE = identifier('ID_PASTE')
ID_DELETE = identifier('ID_DELETE')


class Editor(stc.StyledTextCtrl):
    """Editor class"""

    _toggleBreakpointId = identifier("ID_TOGGLE_BREAKPOINT")

    def __init__(self, parent, id=wx.ID_ANY, **kwargs):
        """Initialize control"""
        style = kwargs.get('style', wx.BORDER_NONE)
        super(Editor, self).__init__(parent, id, wx.DefaultPosition, wx.DefaultSize, style)
        self._auto = kwargs.get('auto', '')
        self._keywords = ''
        self.search_text = ''
        self._types = ''
        self._bookmarks = kwargs.get('bookmarks', {})
        self._breakpoints = kwargs.get('breakpoints', {})
        self._enable_breakpoints = False
        self._enable_bookmarks = False
        self.popup_handler = None
        self.parent = parent
        self.handler = kwargs.get('handler', None)
        self.context = None
        self._last_char = None
        if self.handler:
            # New version, handler carry specialized setup
            self.handler.initialize(self)
        self.SetUseHorizontalScrollBar(False)
        self._horizontal_scrollbar_visible = False
        self._last_horizontal_line = -1
        self._last_horizontal_line_length = 0

    def update_horizontal_scrollbar(self):
        """Do a automatic scroll"""
        line = self.GetCurrentLine()
        length = self.GetLineLength(line)
        if line == self._last_horizontal_line:
            if length == self._last_horizontal_line_length:
                return
            if length > self._last_horizontal_line_length:
                if self._horizontal_scrollbar_visible:
                    return
            elif not self._horizontal_scrollbar_visible:
                return
        else:
            self._last_horizontal_line = line
        self._last_horizontal_line_length = length
        if length < 1:
            if self._horizontal_scrollbar_visible:
                self.SetUseHorizontalScrollBar(False)
                self._horizontal_scrollbar_visible = False
            return
        text = self.GetLineText(line)
        width = self.TextWidth(self.GetStyleAt(self.GetCurrentPos()), text)
        if width >= self.GetClientSize()[0]:
            if not self._horizontal_scrollbar_visible:
                self._horizontal_scrollbar_visible = True
                self.SetUseHorizontalScrollBar(True)
        else:
            if self._horizontal_scrollbar_visible:
                self.SetUseHorizontalScrollBar(False)
                self._horizontal_scrollbar_visible = False

    def CanPaste(self):
        # This cause hang
        # if wx.TheClipboard.IsOpened():
        #    wx.TheClipboard.Close()
        return super(Editor, self).CanPaste()

    def EnableBreakpoints(self, enable=True):
        """Control if breakpoints are enabled"""
        if self._enable_breakpoints:
            if not enable:
                for line in self._breakpoints:
                    self.MarkerDelete(line, self._breakpoints[line])
        else:
            if enable:
                for line in self._breakpoints:
                    self.MarkerAdd(line, self._breakpoints[line])
        self._enable_breakpoints = enable

    def EnableBookmarks(self, enable=True):
        """Control if bookmarks are enabled"""
        if self._enable_bookmarks:
            if not enable:
                for br in self._bookmarks:
                    self.MarkerDelete(br, MARK_BOOKMARK)
        else:
            if enable:
                for br in self._bookmarks:
                    self.MarkerAdd(br, MARK_BOOKMARK)
        self._enable_bookmarks = enable

    def HandleBreakpoints(self, breakpoints):
        """This method can be only used while the breakpoints
        are disabled and is used for replacing the breakpoint dictionnary
        whith the project dictionnary."""
        if  not self._enable_breakpoints:
            self._breakpoints = breakpoints

    def HandleBookmarks(self, bookmarks):
        """This method can be only used while the bookmarks
        are disabled and is used for replacing the bookmarks dictionnary
        whith the project dictionnary."""
        if not self._enable_bookmarks:
            self._bookmarks = bookmarks

    # def SetBreakpoints(self, breakpoints):
    #     """Set the breakpoints"""
    #     if self._enable_breakpoints:
    #         remove = [br for br in self._breakpoints if br not in breakpoints]
    #         add = [br for br in breakpoints if br not in self._breakpoints]
    #         for br in remove:
    #             self.MarkerDelete(br, MARK_BREAKPOINT_ENABLED)
    #         for br in add:
    #             self.MarkerAdd(br, MARK_BREAKPOINT_ENABLED)
    #     self._breakpoints = breakpoints

    def GetLineStartPosition(self, line):
        """Get the starting position of the given line
        @param line: int
        @return: int
        """
        if line > 0:
            spos = self.GetLineEndPosition(line - 1)
            if self.GetLine(line).endswith("\r\n"):
                spos += 2
            else:
                spos += 1
        else:
            spos = 0
        return spos

    def goto_line(self, line, select=False):
        """Called for goto to line"""
        super(Editor, self).GotoLine(line)
        if select:
            pos = self.GetLineEndPosition(line)
            if line > 0:
                start = self.GetLineEndPosition(line - 1) + 1
            else:
                start = 0
            self.SetSelectionStart(start)
            self.SetSelectionEnd(pos)

    def Select(self, sline, scol, eline, ecol):
        """Select a range"""
        super(Editor, self).GotoLine(sline)
        start = self.GetLineStartPosition(sline) + scol
        end = self.GetLineStartPosition(eline) + ecol
        self.SetSelectionStart(start)
        self.SetSelectionEnd(end)

    @property
    def modified(self):
        """Emulate unmodifiable flag using undo stack"""
        return self.CanUndo()

    def ResetModified(self):
        """Emulate unmodifiable flag using undo stack"""
        #p = self.GetCurrentPos()
        #self.SetSavePoint()
        #self.GotoPos(p)
        self.EmptyUndoBuffer()

    def ReplaceValue(self, content, read_only=False):
        """Replace the content of the control, while
        attempts to maintain minimal visual changes"""
        self.Freeze()
        line = self.GetFirstVisibleLine()
        pos = self.GetInsertionPoint()
        self.SetReadOnly(False)
        self.ChangeValue(content)
        self.SetReadOnly(read_only)
        if line < self.GetLineCount():
            self.SetFirstVisibleLine(line)
        else:
            pos = self.GetLastPosition()
        self.SetSelection(pos, pos)
        self.SetInsertionPoint(pos)
        self.Thaw()
        self.ResetModified()
        self.SetModified(False)

    def initialize(self, types=tuple()):
        """Initialize editor"""
        # a sample list of keywords,
        if self.handler:
            self.SetKeyWords(0, self.handler.keywords)
        self._keywords = "alignas alignof and and_eq asm auto "\
            "bitand bitor break case catch class "\
            "compl const constexpr const_cast continue decltype default "\
            "delete do dynamic_cast else enum explicit export extern "\
            "false for final friend goto if inline mutable namespace "\
            "new noexcept not not_eq nullptr operator or or_eq private "\
            " protected public register reinterpret_cast return signed "\
            "sizeof static static_assert static_cast struct switch template "\
            "this thread_local throw true try typedef typeid typename "\
            "union unsigned using virtual void volatile wchar_t while "\
            "xor xor_eq"
        self.SetKeyWords(0, self._keywords)
        self._types = ""

        for k in types:
            if "unsigned" in k:
                continue
            if "long" in k:
                continue
            self._types += k + " "
        self._types += "long unsigned "
        s = (self._types + self._keywords).split(' ')
        s.sort()
        self._auto = ""
        for k in s:
            self._auto += " " + k
        self.SetKeyWords(1, self._types)

        self.EmptyUndoBuffer()
        self.SetSavePoint()
        self.SetUndoCollection(True)
        self.Bind(stc.EVT_STC_MARGINCLICK, self.on_margin_click)
        self.Bind(wx.EVT_KEY_UP, self.on_key)
        self.Bind(stc.EVT_STC_UPDATEUI, self.on_update_edit_ui)

    @property
    def breakpoint(self):
        """check if the line holds breakpoint"""
        if not self._enable_breakpoints:
            return None
        line = self.GetCurrentLine()
        if line in self._breakpoints:
            return self._breakpoints[line]
        else:
            return None

    @breakpoint.setter
    def breakpoint(self, value):
        if not self._enable_breakpoints:
            return
        line = self.GetCurrentLine()
        if line not in self._breakpoints:
            self.MarkerAdd(line, MARK_BREAKPOINT_ENABLED)
        self._breakpoints[line] = value

    def toggle_breakpoint(self, arg):
        """Toggle breakpoint at some line"""
        if not self._enable_breakpoints:
            return None
        if isinstance(arg, wx.Event):
            line = self.GetCurrentLine()
        else:
            line = arg or self.GetCurrentLine()
        if line in self._breakpoints:
            if self._breakpoints[line] == MARK_BREAKPOINT_ENABLED:  # The breakpoint is enabled
                self._breakpoints[line] = MARK_BREAKPOINT_DISABLED
                self.MarkerDelete(line, MARK_BREAKPOINT_ENABLED)
                self.MarkerAdd(line, MARK_BREAKPOINT_DISABLED)
                return MARK_BREAKPOINT_DISABLED
            else:  # The breakpojnt is disable, then we must remove it
                del self._breakpoints[line]
                self.MarkerDelete(line, MARK_BREAKPOINT_DISABLED)
                self.MarkerAdd(line, MARK_BREAKPOINT_DELETED)
                self.MarkerDelete(line, MARK_BREAKPOINT_DELETED)
                return MARK_BREAKPOINT_DELETED
        else:
            self._breakpoints[line] = MARK_BREAKPOINT_ENABLED
            self.MarkerAdd(line, MARK_BREAKPOINT_ENABLED)
            return MARK_BREAKPOINT_ENABLED

    def toggle_bookmark(self, arg):
        """Toggle the bookmark at some line"""
        if not self._enable_bookmarks:
            return
        if isinstance(arg, wx.Event):
            line = self.GetCurrentLine()
        else:
            line = arg or self.GetCurrentLine()
        if line in self._bookmarks:
            del self._bookmarks[line]
            if self.MarkerGet(line):
                self.MarkerDelete(line, MARK_BOOKMARK)
        else:
            self._bookmarks[line] = '<<describe the bookmark here>>'
            self.MarkerAdd(line, MARK_BOOKMARK)

    def NextBookmark(self, event):
        """Goto next bookmark"""
        line = self.GetCurrentLine()
        if self.MarkerGet(line):
                line += 1
        mark = self.MarkerNext(line, 1)
        if mark == wx.NOT_FOUND:
            mark = self.MarkerNext(0, 1)
        if mark != wx.NOT_FOUND:
            self.goto_line(mark)

    def PrevBookmark(self, event):
        """Goto previous bookmark"""
        line = self.GetCurrentLine()
        if self.MarkerGet(line):
            line -= 1
        mark = self.MarkerPrevious(line, 1)
        if mark == wx.NOT_FOUND:
            mark = self.MarkerPrevious(self.GetLineCount(), 1)
        if mark != wx.NOT_FOUND:
            self.goto_line(mark)

    def Find(self, event):
        """Find command"""
        # note: search flags
        # SCFIND_MATCHCASE	A match only occurs with text that matches the case of the search string.
        # SCFIND_WHOLEWORD	A match only occurs if the characters before and after are not word characters.
        # SCFIND_WORDSTART	A match only occurs if the character before is not a word character.
        # SCFIND_REGEXP	The search string should be interpreted as a regular expression.
        # SCFIND_POSIX Treat regular expression in a more POSIX compatible manner by interpreting bare
        #              ( and ) for tagged sections rather than \( and \).
        # SCFIND_CXX11REGEX Replaces scintilla regex with <regex> when compiled with CX11_REGEX
        #                   (goto http://www.scintilla.org/ScintillaDoc.html#searchFlags for details)
        from beatle.app.ui.dlg import FindTextDialog
        text = self.GetSelectedText()
        dlg = FindTextDialog(context.get_frame(), text)
        if dlg.ShowModal() == wx.ID_OK:
            self.search_text = dlg.seach_text
            self.goto_line(0)
            self.SearchAnchor()
            self.find_next(event)

    def iterator_wizard(self, event):
        """c++ iterator wizard"""
        if self.context:
            from beatle.activity.models.cc.ui.dlg import IteratorWizardDialog
            dlg = IteratorWizardDialog(context.get_frame(), self.context)
            if dlg.ShowModal() == wx.ID_OK:
                self.AddText(dlg.code)

    def find_next(self, event):
        """Find next command"""
        if self.search_text:
            pos = self.SearchNext(STC_FIND_MATCH_CASE, self.search_text)
            if pos != wx.NOT_FOUND:
                epos = pos + len(self.search_text)
                self.GotoPos(pos + 1)
                self.SearchAnchor()
                self.SetSelection(pos, epos)

    def find_previous(self, event):
        """Find previous command"""
        if self.search_text:
            pos = self.SearchPrev(STC_FIND_MATCH_CASE, self.search_text)
            if pos != wx.NOT_FOUND:
                epos = pos + len(self.search_text)
                self.GotoPos(pos + 1)
                self.SearchAnchor()
                self.SetSelection(pos, epos)

    def delete_all_bookmarks(self):
        """Remove all book marks"""
        self.MarkerDeleteAll(MARK_MARGIN)

    def on_margin_click(self, event):
        """Handles margin click"""
        if event.GetMargin() == MARGIN_FOLD:
            # fold or unfold
            line_click = self.LineFromPosition(event.GetPosition())
            levelClick = self.GetFoldLevel(line_click)
            if levelClick & stc.STC_FOLDLEVELHEADERFLAG:
                self.ToggleFold(line_click)
            return
        # toggle bookmark
        if self._enable_breakpoints:
            line_click = self.LineFromPosition(event.GetPosition())
            self.GotoLine(line_click)
            wx.PostEvent(context.get_current_view(), wx.CommandEvent(wx.EVT_MENU.typeId, self._toggleBreakpointId))
        else:
            line_click = self.LineFromPosition(event.GetPosition())
            self.toggle_bookmark(line_click)

    def on_popup_menu(self, event):
        """This replaces popup menu"""
        menu = wxx.Menu()
        menu.AppendItem(wxx.MenuItem(menu, ID_UNDO, u"Undo", u"Undoes last edit", wx.ITEM_NORMAL))
        menu.AppendItem(wxx.MenuItem(menu, ID_REDO, u"Redo", u"Redoes last edit", wx.ITEM_NORMAL))
        menu.AppendItem(wxx.MenuItem(menu, ID_CUT, u"Cut", u"Cut selected text", wx.ITEM_NORMAL))
        menu.AppendItem(wxx.MenuItem(menu, ID_COPY, u"Copy", u"Copy selected text", wx.ITEM_NORMAL))
        menu.AppendItem(wxx.MenuItem(menu, ID_PASTE, u"Paste", u"Paste clipboard content", wx.ITEM_NORMAL))
        menu.AppendItem(wxx.MenuItem(menu, ID_DELETE, u"Delete", u"Delete selected text", wx.ITEM_NORMAL))
        menu.AppendSeparator()
        menu.AppendItem(
            wxx.MenuItem(menu, ITERATOR_WIZARD, u"Iterator wizard...", u"Show the iterator wizard.", wx.ITEM_NORMAL))
        menu.Popup(event.GetPosition(), owner=self)

    def on_update_edit_ui(self, event):
        """Handles pos changed"""
        self.update_horizontal_scrollbar()
        brace_at_caret = -1
        opposite_brace = -1
        char_before = None
        style_before = None
        caret_position = self.GetCurrentPos()
        if caret_position > 0:
            char_before = self.GetCharAt(caret_position - 1)
            style_before = self.GetStyleAt(caret_position - 1)
        # check before
        if char_before and chr(char_before) in "[]{}()":
            if style_before == stc.STC_P_OPERATOR:
                brace_at_caret = caret_position - 1
        # check after
        if brace_at_caret < 0:
            char_after = self.GetCharAt(caret_position)
            style_after = self.GetStyleAt(caret_position)
            if char_after and chr(char_after) in "[]{}()":
                if style_after == stc.STC_P_OPERATOR:
                    brace_at_caret = caret_position
        if brace_at_caret >= 0:
            opposite_brace = self.BraceMatch(brace_at_caret)
        if brace_at_caret != -1 and opposite_brace == -1:
            self.BraceBadLight(brace_at_caret)
        else:
            self.BraceHighlight(brace_at_caret, opposite_brace)

    def auto_indent(self, intro):
        """Compute autoindent after key"""
        caret_position = self.GetCurrentPos()
        lnno = self.GetCurrentLine()
        if caret_position == 0 or lnno == 0:
            return
        if intro:
            lnpp = self.GetLineEndPosition(lnno - 1)
            iden = self.GetLineIndentation(lnno - 1)
            pre = self.GetCharAt(lnpp - 1)
            if chr(pre) in "[{(:":
                iden = iden + self.GetTabWidth()
            # prevent popup intro propagation
            if self.GetLineIndentation(lnno) != iden:
                pre = self.GetLineIndentPosition(lnno)
                self.SetLineIndentation(lnno, iden)
                pos = self.GetLineIndentPosition(lnno)
                self.GotoPos(caret_position+(pos-pre))
        else:
            pre = self.GetCharAt(caret_position - 1)
            if chr(pre) in "]})":
                brace_at_caret = caret_position - 1
                opposite_brace = self.BraceMatch(brace_at_caret)
                if opposite_brace != wx.NOT_FOUND:
                    iden = self.GetLineIndentation(
                        self.LineFromPosition(opposite_brace))
                    if iden != self.GetLineIndentation(lnno):
                        self.SetLineIndentation(lnno, iden)

    def on_key(self, event):
        """Handle a scintilla event"""
        k = event.GetKeyCode()
        if k == wx.WXK_SPACE and event.ControlDown():
            self.popup_handler and self.popup_handler.popup(self)
            return
        else:
            (start, end) = self.GetSelection()
            if start < end:
                return
            if k in [wx.WXK_NUMPAD_ENTER, wx.WXK_RETURN]:
                self.auto_indent(True)
                return
            elif k in [wx.WXK_DELETE, wx.WXK_NUMPAD_DELETE, wx.WXK_CLEAR]:
                return
        if len(self._auto) == 0 or k in [wx.WXK_ESCAPE, wx.WXK_TAB]:
            event.Skip()
            return
        elif self.AutoCompActive():
            return
        #filter popup on non printable keys
        if event.GetUnicodeKey() ==  wx.WXK_NONE:
            return
        # check join letters for popup
        k = self._last_char
        if k is None:
            return
        caret_position = self.GetCurrentPos()
        if caret_position > 2:
            style_before = self.GetStyleAt(caret_position - 1)
            if style_before in [stc.STC_C_COMMENT, stc.STC_C_NUMBER, stc.STC_C_COMMENTLINE, stc.STC_C_COMMENTDOC,
                               stc.STC_C_PREPROCESSORCOMMENT, stc.STC_C_REGEX,
                               stc.STC_C_STRINGEOL, stc.STC_C_STRINGRAW, stc.STC_C_UUID]:
                return
            if k in ':{}()[]':
                self.auto_indent(False)
                return
            text, pos = self.GetCurLine()
            if len(text)>pos and not text[pos].isspace():
                return
            text = text[:pos]
            rtext = text[::-1]
            match = re.search(r'(?P<text>(\.|-|>-)?[_A-Za-z0-9]*)\s*', rtext)
            if match:
                s = match.group('text')[::-1]
                if len(s) == 0:
                    return
                if self.popup_handler and k in ['.><']:
                    if s[-1] == '.':
                        self.popup_handler.popup(self, s[:-1])
                        return
                    if len(s) < 2:
                        return
                    if s[-2:] == '->':
                        self.popup_handler.popup(self, s[:-2])
                        return
                if len(s) > 2:
                    self.AutoCompShow(len(s), self._auto)

    def OnChar(self, event):
        """Handle a scintilla event"""
        self._last_char = chr(event.GetKey())

