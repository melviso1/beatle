import wx

if "__WXMSW__" in wx.PlatformInfo:
    from ._base_win import *
else:
    from ._base_linux import *
