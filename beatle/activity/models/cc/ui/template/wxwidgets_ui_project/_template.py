import os
import wx
from beatle.lib.api import context
from beatle.lib.tran import TransactionalMethod
from beatle.lib.utils import cached_type
from .._base_template import BaseTemplate
from beatle.model import cc
from beatle.plugin.tools._wxUI import model as ui_model


class Template(BaseTemplate):

    def __init__(self):
        super(Template, self).__init__()

    def bitmap(self):
        cwd = os.path.realpath(os.path.abspath(os.path.dirname(__file__)))
        return wx.Bitmap(os.path.join(cwd, u"icon64.xpm"), wx.BITMAP_TYPE_ANY)

    def name(self):
        """Return the template displayed name"""
        return "wxWidgets UI project"

    def priority(self):
        """Return a value suitable for ordering templates"""
        return 3

    @TransactionalMethod('Apply wxWidgets UI project template')
    def apply_template(self, project):
        if project is None:
            # we don't report any error, because if the project didn't exist
            # this could imply some error on another side that has already been reported
            return False
        project.save_state()
        project._type = 'executable'
        project.current_build_config._target._type = project._type
        if "__WXMSW__" in wx.PlatformInfo:
            project.current_build_config._target._extension = '.exe'
        else:
            project.current_build_config._target._extension = ''
        project.current_build_config._target._prefix = ''
        # Ok, lets create a module for the main method

        # get the wxWidgets build flags
        stream = os.popen('wx-config --cflags', mode='r', buffering=-1)
        result = stream.read()
        split_list = [s.strip() for s in result.split(' ')]
        includes = [s[2:] for s in split_list if s[:2] == '-I']
        defines = [s[2:] for s in split_list if s[:2] == '-D']
        pthread = '-pthread' in result
        includes.append('include')
        project.current_build_config._cpp_compiler._include_paths = includes
        project.current_build_config._cpp_compiler._defined_symbols = defines
        project.current_build_config._cpp_compiler._pthread_support = pthread

        stream = os.popen('wx-config --libs', mode='r', buffering=-1)
        result = stream.read()
        split_list = [s.strip() for s in result.split(' ')]
        library_paths = [s[2:] for s in split_list if s[:2] == '-L']
        libraries = [s[2:] for s in split_list if s[:2] == '-l']
        pthread = '-pthread' in result

        project.current_build_config._linker._pthreads = pthread
        project.current_build_config._linker._lib_path = library_paths
        project.current_build_config._linker._libs = libraries

        bool_t = cached_type(project, 'bool')
        char_t = cached_type(project, 'char')

        external_definitions_folder = cc.Folder(parent=project, name='external')

        base_app_class = cc.Class(parent=external_definitions_folder,name='wxApp', is_external=True)

        app_class = cc.Class(parent=project, name='App')
        cc.Inheritance(parent=app_class, ancestor=base_app_class)
        app_class.user_code_h1 = '#include <wx/wx.h>'
        app_class.user_code_s2 = 'wxIMPLEMENT_APP(App);'

        cc.MemberMethod(parent=app_class, name='OnInit', virtual=True,
                        type=cc.typeinst(type=bool_t),
                        content="""
    auto *frame = new ApplicationFrame(nullptr);
    frame->Show(true);
    return true;
""")
        # Ok, now we create the environment
        model = ui_model.Design(parent=project, name='UI Design')
        frame = ui_model.Frame(parent=model, name='ApplicationFrame', title='wxWidgets UI Application')
        return True
