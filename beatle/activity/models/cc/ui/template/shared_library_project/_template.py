import os
import wx
from beatle.lib.api import context
from beatle.lib.tran import TransactionalMethod
from .._base_template import BaseTemplate


class Template(BaseTemplate):

    def __init__(self):
        super(Template, self).__init__()

    def bitmap(self):
        cwd = os.path.realpath(os.path.abspath(os.path.dirname(__file__)))
        return wx.Bitmap(os.path.join(cwd, u"icon64.xpm"), wx.BITMAP_TYPE_ANY)

    def name(self):
        """Return the template displayed name"""
        return "shared library project"

    def priority(self):
        """Return a value suitable for ordering templates"""
        return 1

    @TransactionalMethod('Apply c++ shaed library project template')
    def apply_template(self, project):
        if project is None:
            # we don't report any error, because if the project didn't exist
            # this could imply some error on another side that has already been reported
            return False
        project.save_state()
        project._type = 'shared library'
        project.current_build_config._target._type = project._type
        project.current_build_config._target._extension = '.so'
        project.current_build_config._target._prefix = 'lib'
        return True
