import os

import wx


class BaseTemplate(object):
    """This class setup the base of the template projects used for C++"""

    def __init__(self):
        """Constructor"""
        super(BaseTemplate, self).__init__()

    def bitmap(self):
        """Return a suitable icon for the template.
        We choose a 64x64 images
        This must be implemented on each template"""
        assert False

    def name(self):
        """Return the template displayed name"""
        assert False

    def priority(self):
        """Return a value suitable for ordering templates"""
        return 0

    def apply_template(self, project_name):
        dialog = wx.MessageDialog(None,
                                  f"Missing template build method for project {project_name}",
                                  "Error",
                                  wx.OK | wx.ICON_ERROR)
        dialog.ShowModal()
        dialog.Destroy()
        return False


def load_templates():
    """This method loads the existing templates and return a list
    containing the loaded templates."""
    cwd = os.path.realpath(os.path.abspath(os.path.dirname(__file__)))
    dirs = [x for x in os.listdir(cwd) if x[0] not in  ['_', '.'] and os.path.isdir(os.path.join(cwd, x))]
    for d in dirs:
        exec(f"from  . import {d}", globals())
    # create a list of template projects
    unsorted_list = [eval(x+".Template()", globals(), locals()) for x in dirs]
    return sorted(unsorted_list, key=lambda template: template.priority())
