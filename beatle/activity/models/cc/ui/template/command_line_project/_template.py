import os
import wx
from beatle.lib.api import context
from beatle.lib.tran import TransactionalMethod
from beatle.lib.utils import cached_type
from .._base_template import BaseTemplate


class Template(BaseTemplate):

    def __init__(self):
        super(Template, self).__init__()

    def bitmap(self):
        cwd = os.path.realpath(os.path.abspath(os.path.dirname(__file__)))
        return wx.Bitmap(os.path.join(cwd, u"icon64.xpm"), wx.BITMAP_TYPE_ANY)

    def name(self):
        """Return the template displayed name"""
        return "command line project"

    def priority(self):
        """Return a value suitable for ordering templates"""
        return 0

    @TransactionalMethod('Apply c++ commandline project template')
    def apply_template(self, project):
        from beatle.model import cc
        if project is None:
            # we don't report any error, because if the project didn't exist
            # this could imply some error on another side that has already been reported
            return False
        project.save_state()
        project._type = 'executable'
        project.current_build_config._target._type = project._type
        if "__WXMSW__" in wx.PlatformInfo:
            project.current_build_config._target._extension = '.exe'
        else:
            project.current_build_config._target._extension = ''
        project.current_build_config._target._prefix = ''

        # Ok, lets create a module for the main method
        int_t = cached_type(project, 'int')
        char_t = cached_type(project, 'char')

        module = cc.Module(parent=project, name='main', header='main.h', source='main.cpp',
                           user_code_s1="#include <iostream>")

        main = cc.Function(parent=module, name='main',
                           type=cc.typeinst(type=int_t),
                           content=r"""
    std::cout << "Hello word!\n";
    return 0;
    """)
        cc.Argument(parent=main, name='argc', type=cc.typeinst(type=int_t))
        cc.Argument(parent=main, name='argv', type=cc.typeinst(type=char_t, ptr=True, array=True, arraysize=''))
        return True
