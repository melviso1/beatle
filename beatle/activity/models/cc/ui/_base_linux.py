# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Aug 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.richtext
from beatle.lib import wxx
from wx import aui


class ConstructorPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(500, 300),
                          style=wx.BORDER_NONE | wx.TAB_TRAVERSAL | wx.WANTS_CHARS)
        dbc = wx.TheColourDatabase
        fore = dbc.Find('LIGHT GREY')
        back = dbc.Find('DARK GREY')
        self.SetForegroundColour(fore)
        self.SetBackgroundColour(back)

        self.SetExtraStyle(wx.WS_EX_BLOCK_EVENTS)

        fgSizer28 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer28.AddGrowableCol(0)
        fgSizer28.AddGrowableRow(1)
        fgSizer28.SetFlexibleDirection(wx.BOTH)
        fgSizer28.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_toolbarSizer = wx.FlexGridSizer(1, 1, 0, 0)
        self.m_toolbarSizer.AddGrowableCol(0)
        self.m_toolbarSizer.AddGrowableRow(0)
        self.m_toolbarSizer.SetFlexibleDirection(wx.BOTH)
        self.m_toolbarSizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer28.Add(self.m_toolbarSizer, 1, wx.EXPAND, 0)

        self.m_splitter2 = wx.SplitterWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                             wx.SP_3D | wx.NO_BORDER)
        self.m_splitter2.Bind(wx.EVT_IDLE, self.m_splitter2OnIdle)
        self.m_splitter2.SetMinimumPaneSize(60)

        self.m_panel3 = wx.Panel(self.m_splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER)
        self.m_panel3.SetMinSize(wx.Size(-1, 60))

        fgSizer60 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer60.AddGrowableCol(0)
        fgSizer60.AddGrowableRow(0)
        fgSizer60.SetFlexibleDirection(wx.BOTH)
        fgSizer60.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        from beatle.activity.models.cc.ui.ctrl import Editor
        self.m_init = Editor(self.m_panel3, **self._editorArgsInit)

        self.m_init.SetMinSize(wx.Size(-1, 60))

        fgSizer60.Add(self.m_init, 0, wx.EXPAND | wx.BOTTOM, 0)

        self.m_panel3.SetSizer(fgSizer60)
        self.m_panel3.Layout()
        fgSizer60.Fit(self.m_panel3)
        self.m_panel4 = wx.Panel(self.m_splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER)
        fgSizer601 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer601.AddGrowableCol(0)
        fgSizer601.AddGrowableRow(0)
        fgSizer601.SetFlexibleDirection(wx.BOTH)
        fgSizer601.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_editor = Editor(self.m_panel4, **self._editorArgs)
        fgSizer601.Add(self.m_editor, 1, wx.EXPAND | wx.TOP, 0)

        self.m_panel4.SetSizer(fgSizer601)
        self.m_panel4.Layout()
        fgSizer601.Fit(self.m_panel4)
        self.m_splitter2.SplitHorizontally(self.m_panel3, self.m_panel4, 83)
        fgSizer28.Add(self.m_splitter2, 1, wx.EXPAND, 0)

        self.SetSizer(fgSizer28)
        self.Layout()

        # Connect Events
        self.Bind(wx.EVT_INIT_DIALOG, self.OnInitPane)
        self.m_init.Bind(wx.EVT_KILL_FOCUS, self.OnInitLeaveFocus)
        self.m_init.Bind(wx.EVT_SET_FOCUS, self.OnInitGetFocus)
        self.m_editor.Bind(wx.EVT_KILL_FOCUS, self.OnEditorLeaveFocus)
        self.m_editor.Bind(wx.EVT_SET_FOCUS, self.OnEditorGetFocus)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnInitPane(self, event):
        event.Skip()

    def OnInitLeaveFocus(self, event):
        event.Skip()

    def OnInitGetFocus(self, event):
        event.Skip()

    def OnEditorLeaveFocus(self, event):
        event.Skip()

    def OnEditorGetFocus(self, event):
        event.Skip()

    def m_splitter2OnIdle(self, event):
        self.m_splitter2.SetSashPosition(83)
        self.m_splitter2.Unbind(wx.EVT_IDLE)


###########################################################################
## Class ClassDiagramPaneBase
###########################################################################

class ClassDiagramPaneBase(wx.ScrolledCanvas):

    def __init__(self, parent):
        super(ClassDiagramPaneBase, self).__init__(parent,
                                                   size=wx.Size(500, 300),
                                                   style=wx.NO_BORDER | wx.TAB_TRAVERSAL | wx.WANTS_CHARS)
        dbc = wx.TheColourDatabase

        self.SetExtraStyle(wx.WS_EX_BLOCK_EVENTS | wx.WS_EX_PROCESS_UI_UPDATES)
        self.SetBackgroundColour(wx.Colour(255, 255, 255))

        # Connect Events
        self.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.Bind(wx.EVT_LEFT_DOWN, self.on_mouse_down)
        self.Bind(wx.EVT_LEFT_UP, self.on_mouse_up)
        self.Bind(wx.EVT_MOTION, self.on_mouse_move)
        self.Bind(wx.EVT_PAINT, self.on_paint_class_diagram)
        self.Bind(wx.EVT_RIGHT_DOWN, self.on_diagram_menu)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnKillFocus(self, event):
        event.Skip()

    def on_mouse_down(self, event):
        event.Skip()

    def on_mouse_up(self, event):
        event.Skip()

    def on_mouse_move(self, event):
        event.Skip()

    def on_paint_class_diagram(self, event):
        event.Skip()

    def on_diagram_menu(self, event):
        event.Skip()

    def OnGetFocus(self, event):
        event.Skip()


###########################################################################
## Class MethodPaneBase
###########################################################################

class MethodPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self,
                          parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(500, 300),
                          style=wx.BORDER_NONE | wx.TAB_TRAVERSAL | wx.WANTS_CHARS)

        self.SetExtraStyle(wx.WS_EX_BLOCK_EVENTS)
        fgSizer28 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer28.AddGrowableCol(0)
        fgSizer28.AddGrowableRow(1)
        fgSizer28.SetFlexibleDirection(wx.BOTH)
        fgSizer28.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_toolbarSizer = wx.FlexGridSizer(1, 1, 0, 0)
        self.m_toolbarSizer.AddGrowableCol(0)
        self.m_toolbarSizer.AddGrowableRow(0)
        self.m_toolbarSizer.SetFlexibleDirection(wx.BOTH)
        self.m_toolbarSizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer28.Add(self.m_toolbarSizer, 1, wx.EXPAND, 0)

        from beatle.activity.models.cc.ui.ctrl import Editor
        self.m_editor = Editor(self, **self._editorArgs)
        fgSizer28.Add(self.m_editor, 0, wx.EXPAND, 0)

        self.SetSizer(fgSizer28)
        self.Layout()

        # Connect Events
        self.m_editor.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_editor.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnKillFocus(self, event):
        event.Skip()

    def OnGetFocus(self, event):
        event.Skip()


###########################################################################
## Class ContextItemsDialogBase
###########################################################################

class ContextItemsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Edit context items", pos=wx.DefaultPosition,
                           size=wx.Size(662, 664),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer97 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer97.AddGrowableCol(0)
        fgSizer97.AddGrowableRow(1)
        fgSizer97.AddGrowableRow(2)
        fgSizer97.SetFlexibleDirection(wx.BOTH)
        fgSizer97.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer100 = wx.FlexGridSizer(2, 3, 0, 0)
        fgSizer100.AddGrowableCol(1)
        fgSizer100.SetFlexibleDirection(wx.BOTH)
        fgSizer100.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText64 = wx.StaticText(self, wx.ID_ANY, u"name: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText64.Wrap(-1)
        fgSizer100.Add(self.m_staticText64, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl40 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.TE_PROCESS_ENTER)
        fgSizer100.Add(self.m_textCtrl40, 0, wx.ALL | wx.EXPAND , 5)

        self.m_button30 = wx.Button(self, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer100.Add(self.m_button30, 0, wx.ALL, 5)

        fgSizer100.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox64 = wx.CheckBox(self, wx.ID_ANY, u"Enabled", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer100.Add(self.m_checkBox64, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_button31 = wx.Button(self, wx.ID_ANY, u"Remove", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button31.Enable(False)

        fgSizer100.Add(self.m_button31, 0, wx.ALL, 5)

        fgSizer97.Add(fgSizer100, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_splitter3 = wx.SplitterWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D)
        # self.m_splitter3.SetSashSize(5)  -- deprecated ??
        self.m_splitter3.Bind(wx.EVT_IDLE, self.m_splitter3OnIdle)

        self.m_panel16 = wx.Panel(self.m_splitter3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                  wx.NO_BORDER | wx.TAB_TRAVERSAL)
        sbSizer23 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel16, wx.ID_ANY, u"Contextuals"), wx.VERTICAL)

        m_listBox1Choices = []
        self.m_listBox1 = wx.ListBox(sbSizer23.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                     m_listBox1Choices, 0 | wx.HSCROLL | wx.NO_BORDER)
        sbSizer23.Add(self.m_listBox1, 1, wx.EXPAND, 5)

        self.m_panel16.SetSizer(sbSizer23)
        self.m_panel16.Layout()
        sbSizer23.Fit(self.m_panel16)
        self.m_panel17 = wx.Panel(self.m_splitter3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer99 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer99.AddGrowableCol(0)
        fgSizer99.AddGrowableRow(1)
        fgSizer99.AddGrowableRow(2)
        fgSizer99.SetFlexibleDirection(wx.BOTH)
        fgSizer99.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer101 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer101.AddGrowableCol(0)
        fgSizer101.AddGrowableRow(0)
        fgSizer101.SetFlexibleDirection(wx.BOTH)
        fgSizer101.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer31 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel17, wx.ID_ANY, u"definition"), wx.VERTICAL)

        self.m_definition = wx.richtext.RichTextCtrl(sbSizer31.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                     wx.DefaultPosition, wx.DefaultSize,
                                                     0 | wx.HSCROLL | wx.NO_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_definition.SetMargins(left=10, top=10)
        self.m_definition.SetMinSize(wx.Size(-1, 120))
        self.m_definition.SetMaxSize(wx.Size(-1, 120))

        sbSizer31.Add(self.m_definition, 1, wx.EXPAND, 5)

        fgSizer101.Add(sbSizer31, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer99.Add(fgSizer101, 1, wx.EXPAND, 5)

        gSizer2 = wx.GridSizer(2, 2, 0, 0)

        sbSizer25 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel17, wx.ID_ANY, u"declaration prefix"), wx.VERTICAL)

        self.m_decl_prefix = wx.richtext.RichTextCtrl(sbSizer25.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize,
                                                      0 | wx.HSCROLL | wx.NO_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_decl_prefix.SetMargins(left=10, top=10)
        sbSizer25.Add(self.m_decl_prefix, 1, wx.EXPAND, 5)

        gSizer2.Add(sbSizer25, 1, wx.EXPAND | wx.LEFT, 5)

        sbSizer27 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel17, wx.ID_ANY, u"implementation prefix"), wx.VERTICAL)

        self.m_impl_prefix = wx.richtext.RichTextCtrl(sbSizer27.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize,
                                                      0 | wx.HSCROLL | wx.NO_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_impl_prefix.SetMargins(left=10, top=10)
        sbSizer27.Add(self.m_impl_prefix, 1, wx.EXPAND, 5)

        gSizer2.Add(sbSizer27, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer28 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel17, wx.ID_ANY, u"declaration suffix"), wx.VERTICAL)

        self.m_decl_suffix = wx.richtext.RichTextCtrl(sbSizer28.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize,
                                                      0 | wx.HSCROLL | wx.NO_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_decl_suffix.SetMargins(left=10, top=10)
        sbSizer28.Add(self.m_decl_suffix, 1, wx.EXPAND, 5)

        gSizer2.Add(sbSizer28, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer29 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel17, wx.ID_ANY, u"implementation suffix"), wx.VERTICAL)

        self.m_impl_suffix = wx.richtext.RichTextCtrl(sbSizer29.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize,
                                                      0 | wx.HSCROLL | wx.NO_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_impl_suffix.SetMargins(left=10, top=10)
        sbSizer29.Add(self.m_impl_suffix, 1, wx.EXPAND, 5)

        gSizer2.Add(sbSizer29, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer99.Add(gSizer2, 1, wx.EXPAND, 5)

        self.m_panel17.SetSizer(fgSizer99)
        self.m_panel17.Layout()
        fgSizer99.Fit(self.m_panel17)
        self.m_splitter3.SplitVertically(self.m_panel16, self.m_panel17, 171)
        fgSizer97.Add(self.m_splitter3, 1, wx.EXPAND | wx.LEFT, 5)

        sbSizer30 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Note"), wx.VERTICAL)

        self.m_note = wx.richtext.RichTextCtrl(sbSizer30.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.DefaultSize,
                                               0 | wx.HSCROLL | wx.NO_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_note.SetMargins(left=10, top=10)
        sbSizer30.Add(self.m_note, 1, wx.EXPAND, 5)

        fgSizer97.Add(sbSizer30, 2, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer154 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer154.AddGrowableCol(1)
        fgSizer154.SetFlexibleDirection(wx.BOTH)
        fgSizer154.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer154.Add(self.m_info, 0, wx.ALL, 5)

        m_std_buttons = wx.StdDialogButtonSizer()
        self.m_std_buttonsOK = wx.Button(self, wx.ID_OK)

        m_std_buttons.AddButton(self.m_std_buttonsOK)

        self.m_std_buttonsCancel = wx.Button(self, wx.ID_CANCEL)

        m_std_buttons.AddButton(self.m_std_buttonsCancel)
        m_std_buttons.Realize();

        fgSizer154.Add(m_std_buttons, 1, wx.EXPAND, 5)

        fgSizer97.Add(fgSizer154, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer97)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_textCtrl40.Bind(wx.EVT_TEXT, self.OnChangeName)
        self.m_button30.Bind(wx.EVT_BUTTON, self.OnAddNewContext)
        self.m_checkBox64.Bind(wx.EVT_CHECKBOX, self.OnEnabledChanged)
        self.m_button31.Bind(wx.EVT_BUTTON, self.OnRemoveContext)
        self.m_listBox1.Bind(wx.EVT_LISTBOX_DCLICK, self.on_select_item)
        self.m_definition.Bind(wx.EVT_TEXT, self.OnChangeDefinition)
        self.m_decl_prefix.Bind(wx.EVT_CHAR, self.OnDeclarationPrefixChar)
        self.m_decl_prefix.Bind(wx.EVT_TEXT, self.OnChangeDeclarationPrefix)
        self.m_impl_prefix.Bind(wx.EVT_CHAR, self.OnImplementationPrefixChar)
        self.m_impl_prefix.Bind(wx.EVT_TEXT, self.OnChangeImplementationPrefix)
        self.m_decl_suffix.Bind(wx.EVT_CHAR, self.OnDeclarationSuffixChar)
        self.m_decl_suffix.Bind(wx.EVT_TEXT, self.OnChangeDeclarationSuffix)
        self.m_impl_suffix.Bind(wx.EVT_CHAR, self.OnImplementationSuffixChar)
        self.m_impl_suffix.Bind(wx.EVT_TEXT, self.OnChangeImplementationSuffix)

        self.m_decl_prefix.Bind(wx.EVT_CHAR_HOOK, self.OnDeclarationPrefixCharHook)
        self.m_impl_prefix.Bind(wx.EVT_CHAR_HOOK, self.OnImplementationPrefixCharHook)
        self.m_decl_suffix.Bind(wx.EVT_CHAR_HOOK, self.OnDeclarationSuffixCharHook)
        self.m_impl_suffix.Bind(wx.EVT_CHAR_HOOK, self.OnImplementationSuffixCharHook)

        self.m_note.Bind(wx.EVT_TEXT, self.OnChangeNote)
        self.m_std_buttonsCancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_std_buttonsOK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    def OnDeclarationPrefixCharHook(self, event):
        event.Skip()

    def OnImplementationPrefixCharHook(self, event):
        event.Skip()

    def OnDeclarationSuffixCharHook(self, event):
        event.Skip()

    def OnImplementationSuffixCharHook(self, event):
        event.Skip()

    # Virtual event handlers, overide them in your derived class
    def OnChangeName(self, event):
        event.Skip()

    def OnAddNewContext(self, event):
        event.Skip()

    def OnEnabledChanged(self, event):
        event.Skip()

    def OnRemoveContext(self, event):
        event.Skip()

    def on_select_item(self, event):
        event.Skip()

    def OnChangeDefinition(self, event):
        event.Skip()

    def OnDeclarationPrefixChar(self, event):
        event.Skip()

    def OnChangeDeclarationPrefix(self, event):
        event.Skip()

    def OnImplementationPrefixChar(self, event):
        event.Skip()

    def OnChangeImplementationPrefix(self, event):
        event.Skip()

    def OnDeclarationSuffixChar(self, event):
        event.Skip()

    def OnChangeDeclarationSuffix(self, event):
        event.Skip()

    def OnImplementationSuffixChar(self, event):
        event.Skip()

    def OnChangeImplementationSuffix(self, event):
        event.Skip()

    def OnChangeNote(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()

    def m_splitter3OnIdle(self, event):
        self.m_splitter3.SetSashPosition(171)
        self.m_splitter3.Unbind(wx.EVT_IDLE)


###########################################################################
## Class ArgumentDialogBase
###########################################################################

class ArgumentDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New argument", pos=wx.DefaultPosition,
                           size=wx.Size(750, 570), style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizerNA = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizerNA.AddGrowableCol(0)
        fgSizerNA.AddGrowableRow(2)
        fgSizerNA.SetFlexibleDirection(wx.BOTH)
        fgSizerNA.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizerNA1 = wx.FlexGridSizer(4, 4, 0, 0)
        fgSizerNA1.AddGrowableCol(2)
        fgSizerNA1.SetFlexibleDirection(wx.BOTH)
        fgSizerNA1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText10 = wx.StaticText(self, wx.ID_ANY, u"argument &type:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText10.Wrap(-1)
        fgSizerNA1.Add(self.m_staticText10, 0, wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizerNA1.Add((0, 0), 1, wx.EXPAND, 5)

        m_typeChoices = []
        self.m_type = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_typeChoices,
                                wx.CB_SORT | wx.WANTS_CHARS)
        self.m_type.SetSelection(0)
        self.m_type.Refresh()

        fgSizerNA1.Add(self.m_type, 1, wx.EXPAND | wx.BOTTOM, 5)

        fgSizerNA1.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"argument &name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizerNA1.Add(self.m_staticText9, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        fgSizerNA1.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_name = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                  wx.TE_PROCESS_ENTER | wx.SUNKEN_BORDER)
        self.m_name.SetMinSize(wx.Size(180, -1))

        fgSizerNA1.Add(self.m_name, 0, wx.ALIGN_CENTER_VERTICAL | wx.EXPAND | wx.BOTTOM, 5)

        fgSizerNA1.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText91 = wx.StaticText(self, wx.ID_ANY, u"template re&ference:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText91.Wrap(-1)
        fgSizerNA1.Add(self.m_staticText91, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_staticText67 = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText67.Wrap(-1)
        self.m_staticText67.Enable(False)

        fgSizerNA1.Add(self.m_staticText67, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.RIGHT | wx.LEFT, 5)

        self.m_template_args = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TE_PROCESS_ENTER)
        self.m_template_args.Enable(False)

        fgSizerNA1.Add(self.m_template_args, 0, wx.EXPAND | wx.BOTTOM, 5)

        self.m_staticText68 = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText68.Wrap(-1)
        self.m_staticText68.Enable(False)

        fgSizerNA1.Add(self.m_staticText68, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText111 = wx.StaticText(self, wx.ID_ANY, u"default &value:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText111.Wrap(-1)
        fgSizerNA1.Add(self.m_staticText111, 0, wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizerNA1.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_textCtrl8 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        fgSizerNA1.Add(self.m_textCtrl8, 0, wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM, 5)

        fgSizerNA.Add(fgSizerNA1, 1, wx.EXPAND | wx.ALL, 10)

        sbSizerNA2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"properties"), wx.VERTICAL)

        fgSizer25 = wx.FlexGridSizer(4, 4, 0, 0)
        fgSizer25.AddGrowableCol(0)
        fgSizer25.AddGrowableCol(1)
        fgSizer25.AddGrowableCol(2)
        fgSizer25.AddGrowableCol(3)
        fgSizer25.SetFlexibleDirection(wx.BOTH)
        fgSizer25.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_const = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"&const", wx.DefaultPosition, wx.DefaultSize,
                                   0)
        fgSizer25.Add(self.m_const, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_ptr = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"&pointer", wx.DefaultPosition, wx.DefaultSize,
                                 0)
        fgSizer25.Add(self.m_ptr, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_reference = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"&reference", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer25.Add(self.m_reference, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_rvalue_reference = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"rvalue reference",
                                              wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_rvalue_reference.Enable(False)

        fgSizer25.Add(self.m_rvalue_reference, 0, wx.ALL, 5)

        self.m_volatile = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"volatile", wx.DefaultPosition,
                                      wx.DefaultSize, 0)
        self.m_volatile.Enable(False)

        fgSizer25.Add(self.m_volatile, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_constptr = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"c&onst pointer", wx.DefaultPosition,
                                      wx.DefaultSize, 0)
        self.m_constptr.Enable(False)

        fgSizer25.Add(self.m_constptr, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_array = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"&array", wx.DefaultPosition, wx.DefaultSize,
                                   0)
        fgSizer25.Add(self.m_array, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl7 = wx.TextCtrl(sbSizerNA2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                       wx.DefaultSize, wx.TE_PROCESS_ENTER)
        self.m_textCtrl7.Enable(False)
        self.m_textCtrl7.Hide()

        fgSizer25.Add(self.m_textCtrl7, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 0)

        self.m_checkBox50 = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"volatile pointer", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox50.Enable(False)

        fgSizer25.Add(self.m_checkBox50, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_pptr = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"po&inter/pointer", wx.DefaultPosition,
                                  wx.DefaultSize, 0)
        self.m_pptr.Enable(False)

        fgSizer25.Add(self.m_pptr, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox51 = wx.CheckBox(sbSizerNA2.GetStaticBox(), wx.ID_ANY, u"bit field", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox51.Enable(False)
        self.m_checkBox51.Hide()

        fgSizer25.Add(self.m_checkBox51, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_textCtrl39 = wx.TextCtrl(sbSizerNA2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize, wx.TE_PROCESS_ENTER)
        self.m_textCtrl39.Enable(False)
        self.m_textCtrl39.Hide()

        fgSizer25.Add(self.m_textCtrl39, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)
        fgSizer25.AddSpacer(10)

        sbSizerNA2.Add(fgSizer25, 1,  wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT, 5)

        fgSizerNA.Add(sbSizerNA2, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 10)

        sbSizerNA3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"note"), wx.VERTICAL)

        self.m_richText1 = wx.richtext.RichTextCtrl(sbSizerNA3.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_richText1.SetMargins(left=20, top=20)
        sbSizerNA3.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizerNA.Add(sbSizerNA3, 1, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 10)

        fgSizerNA4 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizerNA4.AddGrowableCol(0)
        fgSizerNA4.SetFlexibleDirection(wx.BOTH)
        fgSizerNA4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizerNA4.Add(self.m_info, 0, wx.ALL, 5)

        m_std_buttons = wx.StdDialogButtonSizer()
        self.m_std_buttonsOK = wx.Button(self, wx.ID_OK)

        m_std_buttons.AddButton(self.m_std_buttonsOK)

        self.m_std_buttonsCancel = wx.Button(self, wx.ID_CANCEL)

        m_std_buttons.AddButton(self.m_std_buttonsCancel)

        m_std_buttons.Realize();

        fgSizerNA4.Add(m_std_buttons, 1, wx.EXPAND, 5)

        fgSizerNA.Add(fgSizerNA4, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        self.SetSizer(fgSizerNA)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_type.Bind(wx.EVT_CHAR, self.OnChar)
        self.m_type.Bind(wx.EVT_CHOICE, self.on_type_changed)
        self.m_const.Bind(wx.EVT_CHECKBOX, self.OnToggleConst)
        self.m_ptr.Bind(wx.EVT_CHECKBOX, self.OnTogglePtr)
        self.m_reference.Bind(wx.EVT_CHECKBOX, self.OnToggleReference)
        self.m_rvalue_reference.Bind(wx.EVT_CHECKBOX, self.OnToggleRValueReference)
        self.m_constptr.Bind(wx.EVT_CHECKBOX, self.OnToggleConstPtr)
        self.m_array.Bind(wx.EVT_CHECKBOX, self.OnToggleArray)
        self.m_pptr.Bind(wx.EVT_CHECKBOX, self.OnTogglePointerPointer)
        self.m_checkBox51.Bind(wx.EVT_CHECKBOX, self.OnToggleBitFiled)
        self.m_std_buttonsCancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_std_buttonsOK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnChar(self, event):
        event.Skip()

    def on_type_changed(self, event):
        event.Skip()

    def OnToggleConst(self, event):
        event.Skip()

    def OnTogglePtr(self, event):
        event.Skip()

    def OnToggleReference(self, event):
        event.Skip()

    def OnToggleRValueReference(self, event):
        event.Skip()

    def OnToggleConstPtr(self, event):
        event.Skip()

    def OnToggleArray(self, event):
        event.Skip()

    def OnTogglePointerPointer(self, event):
        event.Skip()

    def OnToggleBitFiled(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ClassDialogBase
###########################################################################

class ClassDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New class", pos=wx.DefaultPosition,
                           size=wx.Size(595,552), style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(1)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer7 = wx.FlexGridSizer(5, 2, 0, 0)
        fgSizer7.AddGrowableCol(1)
        fgSizer7.AddGrowableRow(0)
        fgSizer7.AddGrowableRow(2)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox90 = wx.CheckBox(self, wx.ID_ANY, u"Extern class", wx.DefaultPosition, wx.DefaultSize,
                                        wx.ALIGN_RIGHT)
        fgSizer7.Add(self.m_checkBox90, 0, wx.ALL, 5)

        fgSizer7.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name :", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)

        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL | wx.EXPAND, 5)

        self.m_staticText48 = wx.StaticText(self, wx.ID_ANY, u"Members prefix :", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText48.Wrap(-1)
        fgSizer7.Add(self.m_staticText48, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textPrefix = wx.TextCtrl(self, wx.ID_ANY, u"_", wx.DefaultPosition, wx.DefaultSize,
                                        wx.TE_PROCESS_ENTER)
        fgSizer7.Add(self.m_textPrefix, 1, wx.ALL | wx.EXPAND, 5)

        sbSizer57 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"serialization" ), wx.VERTICAL )

        fgSizer204 = wx.FlexGridSizer( 2, 2, 0, 0 )
        fgSizer204.SetFlexibleDirection( wx.BOTH )
        fgSizer204.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox60 = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"serialize", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer204.Add(self.m_checkBox60, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox127 = wx.CheckBox( sbSizer57.GetStaticBox(), wx.ID_ANY, u"undo/redo support", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_checkBox127.Enable( False )

        fgSizer204.Add( self.m_checkBox127, 0, wx.ALL, 5 )

        self.m_checkBox126 = wx.CheckBox( sbSizer57.GetStaticBox(), wx.ID_ANY, u"component", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_checkBox126.Enable( False )

        fgSizer204.Add( self.m_checkBox126, 0, wx.ALL, 5 )

        m_choice28Choices = []
        self.m_choice28 = wx.Choice( sbSizer57.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice28Choices, 0 )
        self.m_choice28.SetSelection( 0 )
        fgSizer204.Add( self.m_choice28, 0, wx.ALL, 5 )

        sbSizer57.Add( fgSizer204, 1, wx.EXPAND, 5 )

        fgSizer7.Add(sbSizer57, 1, wx.EXPAND, 5)

        fgSizer134 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer134.AddGrowableCol(1)
        fgSizer134.AddGrowableCol(2)
        fgSizer134.SetFlexibleDirection(wx.BOTH)
        fgSizer134.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox83 = wx.CheckBox(self, wx.ID_ANY, u"struct", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer134.Add(self.m_checkBox83, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText88 = wx.StaticText(self, wx.ID_ANY, u"access", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText88.Wrap(-1)
        self.m_staticText88.Enable(False)

        fgSizer134.Add(self.m_staticText88, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        m_choice2Choices = [u"public", u"protected", u"private"]
        self.m_choice2 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice2Choices, 0)
        self.m_choice2.SetSelection(0)
        self.m_choice2.Refresh()
        self.m_choice2.Enable(False)

        fgSizer134.Add(self.m_choice2, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer7.Add(fgSizer134, 1, wx.EXPAND, 5)

        self.m_checkBox61 = wx.CheckBox(self, wx.ID_ANY, u"template", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer7.Add(self.m_checkBox61, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer93 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer93.AddGrowableCol(1)
        fgSizer93.AddGrowableRow(0)
        fgSizer93.SetFlexibleDirection(wx.BOTH)
        fgSizer93.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_stStartTemplate = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stStartTemplate.Wrap(-1)
        self.m_stStartTemplate.Enable(False)

        fgSizer93.Add(self.m_stStartTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_template = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                      wx.TE_PROCESS_ENTER)
        self.m_template.Enable(False)

        fgSizer93.Add(self.m_template, 0, wx.ALL | wx.EXPAND, 5)

        self.m_stEndTemplate = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stEndTemplate.Wrap(-1)
        self.m_stEndTemplate.Enable(False)

        fgSizer93.Add(self.m_stEndTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer7.Add(fgSizer93, 1, wx.EXPAND, 5)

        fgSizer6.Add(fgSizer7, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer6.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_checkBox90.Bind(wx.EVT_CHECKBOX, self.on_toggle_external_class)
        self.m_checkBox61.Bind(wx.EVT_CHECKBOX, self.on_toggle_template)
        self.m_template.Bind(wx.EVT_TEXT, self.on_change_template_specification)
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_toggle_external_class(self, event):
        event.Skip()

    def on_toggle_template(self, event):
        event.Skip()

    def on_change_template_specification(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ClassDiagramDialogBase
###########################################################################

class ClassDiagramDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"new class diagram", pos=wx.DefaultPosition,
                           size=wx.Size(423, 390),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(1)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer7 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer7.AddGrowableCol(1)
        fgSizer7.AddGrowableRow(0)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer6.Add(fgSizer7, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer9, 1, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer6.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ConstructorDialogBase
###########################################################################

class ConstructorDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New constructor", pos=wx.DefaultPosition,
                           size=wx.Size(546, 483),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.TAB_TRAVERSAL | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer12 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer12.AddGrowableCol(0)
        fgSizer12.AddGrowableRow(2)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Properties"), wx.VERTICAL)

        fgSizer20 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer20.AddGrowableCol(0)
        fgSizer20.AddGrowableCol(1)
        fgSizer20.SetFlexibleDirection(wx.BOTH)
        fgSizer20.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer16 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer16.AddGrowableCol(1)
        fgSizer16.SetFlexibleDirection(wx.BOTH)
        fgSizer16.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText8 = wx.StaticText(sbSizer3.GetStaticBox(), wx.ID_ANY, u"access", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText8.Wrap(-1)
        fgSizer16.Add(self.m_staticText8, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.RIGHT | wx.LEFT, 5)

        m_choice2Choices = [u"public", u"protected", u"private"]
        self.m_choice2 = wx.Choice(sbSizer3.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                   m_choice2Choices, 0)
        self.m_choice2.SetSelection(0)
        self.m_choice2.Refresh()
        fgSizer16.Add(self.m_choice2, 0, wx.EXPAND | wx.ALL, 5)

        self.m_checkBox6 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&calling", wx.DefaultPosition,
                                       wx.DefaultSize, wx.ALIGN_RIGHT)
        fgSizer16.Add(self.m_checkBox6, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT | wx.ALIGN_RIGHT, 5)

        m_comboBox1Choices = [u"cdecl", u"stdcall", u"fastcall", u"thiscall", u"naked", u"ms_abi", u"sysv_abi",
                              u"interrupt"]
        self.m_comboBox1 = wx.ComboBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"cdecl", wx.DefaultPosition, wx.DefaultSize,
                                       m_comboBox1Choices, 0)
        self.m_comboBox1.Enable(False)

        fgSizer16.Add(self.m_comboBox1, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer20.Add(fgSizer16, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer141 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer141.AddGrowableCol(0)
        fgSizer141.AddGrowableCol(1)
        fgSizer141.SetFlexibleDirection(wx.BOTH)
        fgSizer141.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox31 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&inline", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer141.Add(self.m_checkBox31, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox41 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&explicit", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer141.Add(self.m_checkBox41, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox92 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&declare", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox92.SetValue(True)
        fgSizer141.Add(self.m_checkBox92, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox93 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"i&mplement", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox93.SetValue(True)
        fgSizer141.Add(self.m_checkBox93, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBoxPreferred = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&preferred", wx.DefaultPosition,
                                               wx.DefaultSize, 0)
        fgSizer141.Add(self.m_checkBoxPreferred, 0, wx.ALL, 5)

        self.m_checkBoxDeleted = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"de&leted", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        fgSizer141.Add(self.m_checkBoxDeleted, 0, wx.ALL, 5)

        fgSizer20.Add(fgSizer141, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer3.Add(fgSizer20, 1, wx.EXPAND, 5)

        fgSizer12.Add(sbSizer3, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"Notes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer12.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.m_richText1 = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                    wx.SIMPLE_BORDER | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)

        fgSizer12.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)
        self.m_richText1.SetMargins(left=20, top=20)
        fgSizer21 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer21.AddGrowableCol(0)
        fgSizer21.SetFlexibleDirection(wx.BOTH)
        fgSizer21.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer21.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button1 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer21.Add(self.m_button1, 0, wx.ALL, 5)

        self.m_button2 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button2.SetDefault()
        fgSizer21.Add(self.m_button2, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer21, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer12)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_checkBox6.Bind(wx.EVT_CHECKBOX, self.on_toggle_calling_convention)
        self.m_button1.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button2.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_toggle_calling_convention(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class DestructorDialogBase
###########################################################################

class DestructorDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New destructor", pos=wx.DefaultPosition,
                           size=wx.Size(546, 483),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.TAB_TRAVERSAL | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer12 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer12.AddGrowableCol(0)
        fgSizer12.AddGrowableRow(2)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Properties"), wx.VERTICAL)

        fgSizer20 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer20.AddGrowableCol(0)
        fgSizer20.AddGrowableCol(1)
        fgSizer20.AddGrowableRow(0)
        fgSizer20.SetFlexibleDirection(wx.BOTH)
        fgSizer20.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer16 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer16.AddGrowableCol(1)
        fgSizer16.SetFlexibleDirection(wx.BOTH)
        fgSizer16.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText8 = wx.StaticText(sbSizer3.GetStaticBox(), wx.ID_ANY, u"access", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText8.Wrap(-1)
        fgSizer16.Add(self.m_staticText8, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.RIGHT | wx.LEFT, 5)

        m_choice2Choices = [u"public", u"protected", u"private"]
        self.m_choice2 = wx.Choice(sbSizer3.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                   m_choice2Choices, 0)
        self.m_choice2.SetSelection(0)
        self.m_choice2.Refresh()
        fgSizer16.Add(self.m_choice2, 0, wx.EXPAND | wx.ALL, 5)

        self.m_checkBox6 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&calling", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer16.Add(self.m_checkBox6, 0, wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        m_comboBox1Choices = [u"cdecl", u"stdcall", u"fastcall", u"thiscall", u"naked", u"ms_abi", u"sysv_abi",
                              u"interrupt"]
        self.m_comboBox1 = wx.ComboBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"cdecl", wx.DefaultPosition, wx.DefaultSize,
                                       m_comboBox1Choices, 0)
        self.m_comboBox1.Enable(False)

        fgSizer16.Add(self.m_comboBox1, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer20.Add(fgSizer16, 1, wx.RIGHT | wx.LEFT | wx.EXPAND, 5)

        fgSizer141 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer141.AddGrowableCol(0)
        fgSizer141.AddGrowableCol(1)
        fgSizer141.SetFlexibleDirection(wx.BOTH)
        fgSizer141.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox31 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&inline", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer141.Add(self.m_checkBox31, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        fgSizer141.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox41 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&virtual", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox41.SetValue(True)
        fgSizer141.Add(self.m_checkBox41, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox104 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&pure", wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        fgSizer141.Add(self.m_checkBox104, 0, wx.ALL, 5)

        self.m_checkBox92 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&declare", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox92.SetValue(True)
        fgSizer141.Add(self.m_checkBox92, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox93 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"i&mplement", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox93.SetValue(True)
        fgSizer141.Add(self.m_checkBox93, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        fgSizer20.Add(fgSizer141, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer3.Add(fgSizer20, 1, wx.EXPAND, 5)

        fgSizer12.Add(sbSizer3, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"Notes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer12.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.m_richText1 = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                    wx.SIMPLE_BORDER | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_richText1.SetMargins(left=20, top=20)
        fgSizer12.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer21 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer21.AddGrowableCol(0)
        fgSizer21.SetFlexibleDirection(wx.BOTH)
        fgSizer21.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer21.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button1 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer21.Add(self.m_button1, 0, wx.ALL, 5)

        self.m_button2 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button2.SetDefault()
        fgSizer21.Add(self.m_button2, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer21, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer12)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button1.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button2.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class EnumDialogBase
###########################################################################

class EnumDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New enum", pos=wx.DefaultPosition,
                           size=wx.Size(613, 528),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer135 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer135.AddGrowableCol(0)
        fgSizer135.AddGrowableRow(1)
        fgSizer135.AddGrowableRow(2)
        fgSizer135.SetFlexibleDirection(wx.BOTH)
        fgSizer135.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer136 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer136.AddGrowableCol(1)
        fgSizer136.AddGrowableCol(2)
        fgSizer136.SetFlexibleDirection(wx.BOTH)
        fgSizer136.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText89 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText89.Wrap(-1)
        fgSizer136.Add(self.m_staticText89, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl55 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.TE_PROCESS_ENTER)
        fgSizer136.Add(self.m_textCtrl55, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer16 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer16.AddGrowableCol(1)
        fgSizer16.SetFlexibleDirection(wx.BOTH)
        fgSizer16.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText8 = wx.StaticText(self, wx.ID_ANY, u"access", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText8.Wrap(-1)
        self.m_staticText8.Enable(False)

        fgSizer16.Add(self.m_staticText8, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.RIGHT | wx.LEFT, 5)

        m_choice2Choices = [u"public", u"protected", u"private"]
        self.m_choice2 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice2Choices, 0)
        self.m_choice2.SetSelection(0)
        self.m_choice2.Refresh()
        self.m_choice2.Enable(False)

        fgSizer16.Add(self.m_choice2, 0, wx.EXPAND | wx.ALL, 5)

        fgSizer136.Add(fgSizer16, 1, wx.EXPAND, 5)

        fgSizer135.Add(fgSizer136, 1, wx.EXPAND | wx.TOP | wx.LEFT, 5)

        fgSizer137 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer137.AddGrowableCol(0)
        fgSizer137.AddGrowableRow(0)
        fgSizer137.SetFlexibleDirection(wx.BOTH)
        fgSizer137.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_listCtrl5 = wx.ListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                       wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.m_listCtrl5.Refresh()
        self.m_listCtrl5.SetToolTip(u"click for select item\ndouble click or enter for edit")

        fgSizer137.Add(self.m_listCtrl5, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer138 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer138.AddGrowableCol(0)
        fgSizer138.AddGrowableRow(4)
        fgSizer138.SetFlexibleDirection(wx.BOTH)
        fgSizer138.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer138.SetMinSize(wx.Size(32, -1))
        self.m_bpButton11 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(u"gtk-add", wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer138.Add(self.m_bpButton11, 0, wx.ALL, 5)

        self.m_bpButton12 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(u"gtk-go-up", wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_bpButton12.Enable(False)

        fgSizer138.Add(self.m_bpButton12, 0, wx.ALL, 5)

        self.m_bpButton13 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(u"gtk-go-down", wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_bpButton13.Enable(False)

        fgSizer138.Add(self.m_bpButton13, 0, wx.ALL, 5)

        self.m_bpButton14 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(u"gtk-delete", wx.ART_BUTTON),
                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_bpButton14.Enable(False)

        fgSizer138.Add(self.m_bpButton14, 0, wx.ALL, 5)

        fgSizer137.Add(fgSizer138, 1, wx.EXPAND, 5)

        fgSizer135.Add(fgSizer137, 0, wx.EXPAND, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.HORIZONTAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer135.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer43.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_button7 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button7.SetDefault()
        fgSizer43.Add(self.m_button7, 0, wx.ALL, 5)

        fgSizer135.Add(fgSizer43, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer135)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_listCtrl5.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnEditEnumItem)
        self.m_listCtrl5.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.OnDeselectEnumItem)
        self.m_listCtrl5.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnSelectEnumItem)
        self.m_bpButton11.Bind(wx.EVT_BUTTON, self.OnAddEnumItem)
        self.m_bpButton12.Bind(wx.EVT_BUTTON, self.OnEnumItemUp)
        self.m_bpButton13.Bind(wx.EVT_BUTTON, self.OnEnumItemDown)
        self.m_bpButton14.Bind(wx.EVT_BUTTON, self.OnRemoveEnumItem)
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnEditEnumItem(self, event):
        event.Skip()

    def OnDeselectEnumItem(self, event):
        event.Skip()

    def OnSelectEnumItem(self, event):
        event.Skip()

    def OnAddEnumItem(self, event):
        event.Skip()

    def OnEnumItemUp(self, event):
        event.Skip()

    def OnEnumItemDown(self, event):
        event.Skip()

    def OnRemoveEnumItem(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class EnumItemDialogBase
###########################################################################

class EnumItemDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Enum item", pos=wx.DefaultPosition,
                           size=wx.Size(339, 200),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer142 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer142.AddGrowableCol(0)
        fgSizer142.AddGrowableRow(0)
        fgSizer142.SetFlexibleDirection(wx.BOTH)
        fgSizer142.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer149 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer149.AddGrowableCol(0)
        fgSizer149.SetFlexibleDirection(wx.BOTH)
        fgSizer149.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer143 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer143.AddGrowableCol(1)
        fgSizer143.SetFlexibleDirection(wx.BOTH)
        fgSizer143.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer143.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer143.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText90 = wx.StaticText(self, wx.ID_ANY, u"label", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText90.Wrap(-1)
        fgSizer143.Add(self.m_staticText90, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl56 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.TE_PROCESS_ENTER)
        fgSizer143.Add(self.m_textCtrl56, 1, wx.ALL | wx.EXPAND , 5)

        self.m_staticText91 = wx.StaticText(self, wx.ID_ANY, u"value", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText91.Wrap(-1)
        fgSizer143.Add(self.m_staticText91, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_comboBox6Choices = []
        self.m_comboBox6 = wx.ComboBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       m_comboBox6Choices, 0)
        self.m_comboBox6.Refresh()
        fgSizer143.Add(self.m_comboBox6, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer149.Add(fgSizer143, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizer142.Add(fgSizer149, 1, wx.EXPAND | wx.TOP, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer43.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_button7 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button7.SetDefault()
        fgSizer43.Add(self.m_button7, 0, wx.ALL, 5)

        fgSizer142.Add(fgSizer43, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer142)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class FolderDialogBase
###########################################################################

class FolderDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New folder", pos=wx.DefaultPosition,
                           size=wx.Size(423, 390),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(1)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer7 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer7.AddGrowableCol(1)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer6.Add(fgSizer7, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer162 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer162.AddGrowableCol(1)
        fgSizer162.SetFlexibleDirection(wx.BOTH)
        fgSizer162.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer162.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK)

        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel)
        m_sdbSizer2.Realize();

        fgSizer162.Add(m_sdbSizer2, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer6.Add(fgSizer162, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer2Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer2OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class IncludeDialogBase
###########################################################################

class IncludeDialogBase(wxx.Dialog):

    def __init__(self, parent):
        super(IncludeDialogBase, self).__init__(parent, id=wx.ID_ANY, title=u"New include", pos=wx.DefaultPosition,
                                                size=wx.Size(366, 143),
                                                style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(0)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer210 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer210.AddGrowableCol(0)
        fgSizer210.AddGrowableRow(0)
        fgSizer210.AddGrowableRow(2)
        fgSizer210.SetFlexibleDirection(wx.BOTH)
        fgSizer210.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer210.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_dirPicker = wx.DirPickerCtrl(self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition,
                                            wx.DefaultSize, wx.DIRP_DEFAULT_STYLE | wx.DIRP_USE_TEXTCTRL)
        fgSizer210.Add(self.m_dirPicker, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer210.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer6.Add(fgSizer210, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer162 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer162.AddGrowableCol(1)
        fgSizer162.SetFlexibleDirection(wx.BOTH)
        fgSizer162.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer162.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK)

        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel)
        m_sdbSizer2.Realize();

        fgSizer162.Add(m_sdbSizer2, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer6.Add(fgSizer162, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer2Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer2OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class InheritanceDialogBase
###########################################################################

class InheritanceDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New inheritance", pos=wx.DefaultPosition,
                           size=wx.Size(593, 493),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer54 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer54.AddGrowableCol(0)
        fgSizer54.AddGrowableRow(1)
        fgSizer54.SetFlexibleDirection(wx.BOTH)
        fgSizer54.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer55 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer55.AddGrowableCol(1)
        fgSizer55.SetFlexibleDirection(wx.BOTH)
        fgSizer55.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText24 = wx.StaticText(self, wx.ID_ANY, u"from class:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText24.Wrap(-1)
        fgSizer55.Add(self.m_staticText24, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_base_classChoices = []
        self.m_base_class = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_base_classChoices, 0)
        self.m_base_class.SetSelection(0)
        self.m_base_class.Refresh()
        fgSizer55.Add(self.m_base_class, 1, wx.ALL | wx.EXPAND, 5)

        self.m_staticText25 = wx.StaticText(self, wx.ID_ANY, u"access:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText25.Wrap(-1)
        fgSizer55.Add(self.m_staticText25, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_accessChoices = [u"public", u"protected", u"private"]
        self.m_access = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_accessChoices, 0)
        self.m_access.SetSelection(0)
        self.m_access.Refresh()
        fgSizer55.Add(self.m_access, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer55.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox51 = wx.CheckBox(self, wx.ID_ANY, u"&virtual", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer55.Add(self.m_checkBox51, 0, wx.ALL, 5)

        self.m_staticText109 = wx.StaticText(self, wx.ID_ANY, u"template :", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText109.Wrap(-1)
        self.m_staticText109.Enable(False)

        fgSizer55.Add(self.m_staticText109, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        fgSizer93 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer93.AddGrowableCol(1)
        fgSizer93.AddGrowableRow(0)
        fgSizer93.SetFlexibleDirection(wx.BOTH)
        fgSizer93.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_stStartTemplate = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stStartTemplate.Wrap(-1)
        self.m_stStartTemplate.Enable(False)

        fgSizer93.Add(self.m_stStartTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_template = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                      wx.TE_PROCESS_ENTER)
        self.m_template.Enable(False)

        fgSizer93.Add(self.m_template, 0, wx.ALL | wx.EXPAND, 5)

        self.m_stEndTemplate = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stEndTemplate.Wrap(-1)
        self.m_stEndTemplate.Enable(False)

        fgSizer93.Add(self.m_stEndTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer55.Add(fgSizer93, 1, wx.EXPAND, 5)
        fgSizer54.Add(fgSizer55, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer54.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer43.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_button7 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button7.SetDefault()
        fgSizer43.Add(self.m_button7, 0, wx.ALL, 5)

        fgSizer54.Add(fgSizer43, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer54)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_base_class.Bind(wx.EVT_CHAR, self.OnChar)
        self.m_template.Bind(wx.EVT_TEXT, self.on_change_template_specification)
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnChar(self, event):
        event.Skip()

    def on_change_template_specification(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class MemberDialogBase
###########################################################################

class MemberDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New member", pos=wx.DefaultPosition,
                           size=wx.Size(584, 702),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        dbc = wx.TheColourDatabase

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer202 = wx.FlexGridSizer(6, 1, 0, 0)
        fgSizer202.AddGrowableCol(0)
        fgSizer202.AddGrowableRow(4)
        fgSizer202.SetFlexibleDirection(wx.BOTH)
        fgSizer202.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer203 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer203.AddGrowableCol(1)
        fgSizer203.SetFlexibleDirection(wx.BOTH)
        fgSizer203.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText94 = wx.StaticText(self, wx.ID_ANY, u"member &type:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText94.Wrap(-1)
        fgSizer203.Add(self.m_staticText94, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_typeChoices = []
        self.m_type = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_typeChoices,
                                wx.CB_SORT | wx.WANTS_CHARS)
        self.m_type.SetSelection(0)
        self.m_type.Refresh()

        fgSizer203.Add(self.m_type, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText95 = wx.StaticText(self, wx.ID_ANY, u"member na&me:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText95.Wrap(-1)
        fgSizer203.Add(self.m_staticText95, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_name = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                  wx.TE_PROCESS_ENTER | wx.SUNKEN_BORDER)
        fgSizer203.Add(self.m_name, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText96 = wx.StaticText(self, wx.ID_ANY, u"template re&ference:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText96.Wrap(-1)
        fgSizer203.Add(self.m_staticText96, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_template_args = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TE_PROCESS_ENTER)
        self.m_template_args.Enable(False)

        fgSizer203.Add(self.m_template_args, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer202.Add(fgSizer203, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer57 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"type properties"), wx.VERTICAL)

        fgSizer204 = wx.FlexGridSizer(3, 5, 0, 0)
        fgSizer204.AddGrowableCol(0)
        fgSizer204.AddGrowableCol(1)
        fgSizer204.AddGrowableCol(2)
        fgSizer204.AddGrowableCol(3)
        fgSizer204.AddGrowableCol(4)
        fgSizer204.SetFlexibleDirection(wx.BOTH)
        fgSizer204.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_const = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"c&onst", wx.DefaultPosition, wx.DefaultSize,
                                   0)
        fgSizer204.Add(self.m_const, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_reference = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"&reference", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer204.Add(self.m_reference, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizer204.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_array = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"arra&y", wx.DefaultPosition, wx.DefaultSize,
                                   0)
        fgSizer204.Add(self.m_array, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_textCtrl7 = wx.TextCtrl(sbSizer57.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                       wx.DefaultSize, wx.TE_PROCESS_ENTER)
        self.m_textCtrl7.Enable(False)

        fgSizer204.Add(self.m_textCtrl7, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox48 = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"m&utable", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer204.Add(self.m_checkBox48, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_ptr = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"&pointer", wx.DefaultPosition, wx.DefaultSize,
                                 0)
        fgSizer204.Add(self.m_ptr, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_constptr = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"con&st pointer", wx.DefaultPosition,
                                      wx.DefaultSize, 0)
        self.m_constptr.Enable(False)

        fgSizer204.Add(self.m_constptr, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox51 = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"&bit field", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer204.Add(self.m_checkBox51, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_textCtrl39 = wx.TextCtrl(sbSizer57.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize, wx.TE_PROCESS_ENTER)
        self.m_textCtrl39.Enable(False)

        fgSizer204.Add(self.m_textCtrl39, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox49 = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"&volatile", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer204.Add(self.m_checkBox49, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        self.m_pptr = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"poi&nter/pointer", wx.DefaultPosition,
                                  wx.DefaultSize, 0)
        self.m_pptr.Enable(False)

        fgSizer204.Add(self.m_pptr, 0, wx.ALL, 5)

        self.m_checkBox50 = wx.CheckBox(sbSizer57.GetStaticBox(), wx.ID_ANY, u"vo&latile pointer", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox50.Enable(False)

        fgSizer204.Add(self.m_checkBox50, 0, wx.ALL, 5)

        sbSizer57.Add(fgSizer204, 1, wx.EXPAND, 5)

        fgSizer202.Add(sbSizer57, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 10)

        fgSizer205 = wx.FlexGridSizer(1, 4, 0, 0)
        fgSizer205.AddGrowableCol(0)
        fgSizer205.AddGrowableCol(1)
        fgSizer205.AddGrowableCol(2)
        fgSizer205.AddGrowableCol(3)
        fgSizer205.AddGrowableRow(0)
        fgSizer205.SetFlexibleDirection(wx.BOTH)
        fgSizer205.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer58 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"properties"), wx.VERTICAL)

        self.m_checkBox129 = wx.CheckBox(sbSizer58.GetStaticBox(), wx.ID_ANY, u"&delete", wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        self.m_checkBox129.Enable(False)

        sbSizer58.Add(self.m_checkBox129, 0, wx.ALL, 5)

        self.m_checkBox130 = wx.CheckBox(sbSizer58.GetStaticBox(), wx.ID_ANY, u"seriali&ze", wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        self.m_checkBox130.Enable(False)

        sbSizer58.Add(self.m_checkBox130, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer205.Add(sbSizer58, 1, wx.EXPAND | wx.RIGHT, 5)

        sbSizer60 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"&access"), wx.VERTICAL)

        self.m_checkBox105 = wx.CheckBox(sbSizer60.GetStaticBox(), wx.ID_ANY, u"s&tatic", wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        sbSizer60.Add(self.m_checkBox105, 0, wx.ALL, 5)

        m_choice2Choices = [u"public", u"protected", u"private"]
        self.m_choice2 = wx.Choice(sbSizer60.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                   m_choice2Choices, 0)
        self.m_choice2.SetSelection(0)
        self.m_choice2.Refresh()
        sbSizer60.Add(self.m_choice2, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer205.Add(sbSizer60, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer61 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"&getter"), wx.VERTICAL)

        m_choice21Choices = [u"none", u"public", u"protected", u"private"]
        self.m_choice21 = wx.Choice(sbSizer61.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                    m_choice21Choices, 0)
        self.m_choice21.SetSelection(0)
        self.m_choice21.Refresh()
        sbSizer61.Add(self.m_choice21, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer205.Add(sbSizer61, 1, wx.EXPAND, 5)

        sbSizer62 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"s&etter"), wx.VERTICAL)

        m_choice22Choices = [u"none", u"public", u"protected", u"private"]
        self.m_choice22 = wx.Choice(sbSizer62.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                    m_choice22Choices, 0)
        self.m_choice22.SetSelection(0)
        self.m_choice22.Refresh()
        sbSizer62.Add(self.m_choice22, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer205.Add(sbSizer62, 1, wx.EXPAND, 5)

        fgSizer202.Add(fgSizer205, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 10)

        fgSizer206 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer206.AddGrowableCol(1)
        fgSizer206.AddGrowableRow(0)
        fgSizer206.SetFlexibleDirection(wx.BOTH)
        fgSizer206.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText97 = wx.StaticText(self, wx.ID_ANY, u"&initialization:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText97.Wrap(-1)
        fgSizer206.Add(self.m_staticText97, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl8 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        fgSizer206.Add(self.m_textCtrl8, 0, wx.ALL | wx.EXPAND, 5)

        self.m_button57 = wx.Button(self, wx.ID_ANY, u"...", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer206.Add(self.m_button57, 0, wx.ALL, 5)

        fgSizer202.Add(fgSizer206, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"notes"), wx.VERTICAL)

        self.m_richText1 = wx.richtext.RichTextCtrl(sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    wx.SIMPLE_BORDER | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_richText1.SetMargins(left=20, top=20)
        sbSizer6.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer202.Add(sbSizer6, 1, wx.EXPAND, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer202.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer202)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_staticText94.Bind(wx.EVT_SET_FOCUS, self.OnFocusType)
        self.m_type.Bind(wx.EVT_CHAR, self.OnChar)
        self.m_type.Bind(wx.EVT_CHOICE, self.on_type_changed)
        self.m_staticText95.Bind(wx.EVT_SET_FOCUS, self.OnFocusName)
        self.m_name.Bind(wx.EVT_TEXT_ENTER, self.OnEnterName)
        self.m_staticText96.Bind(wx.EVT_SET_FOCUS, self.OnFocusReference)
        self.m_const.Bind(wx.EVT_CHECKBOX, self.OnConstToggle)
        self.m_reference.Bind(wx.EVT_CHECKBOX, self.OnReferenceToggle)
        self.m_array.Bind(wx.EVT_CHECKBOX, self.OnToggleArray)
        self.m_ptr.Bind(wx.EVT_CHECKBOX, self.on_toggle_pointer)
        self.m_constptr.Bind(wx.EVT_CHECKBOX, self.OnConstPtrToggle)
        self.m_checkBox51.Bind(wx.EVT_CHECKBOX, self.OnToggleBitField)
        self.m_checkBox49.Bind(wx.EVT_CHECKBOX, self.OnVolatileToggle)
        self.m_pptr.Bind(wx.EVT_CHECKBOX, self.OnPointerPointerToggle)
        self.m_checkBox50.Bind(wx.EVT_CHECKBOX, self.OnVolatilePointerToggle)
        self.m_checkBox105.Bind(wx.EVT_CHECKBOX, self.OnToggleStatic)
        self.m_staticText97.Bind(wx.EVT_SET_FOCUS, self.OnFocusInitialization)
        self.m_button57.Bind(wx.EVT_BUTTON, self.OnEditInitialValue)
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnFocusType(self, event):
        event.Skip()

    def OnChar(self, event):
        event.Skip()

    def on_type_changed(self, event):
        event.Skip()

    def OnFocusName(self, event):
        event.Skip()

    def OnEnterName(self, event):
        event.Skip()

    def OnFocusReference(self, event):
        event.Skip()

    def OnConstToggle(self, event):
        event.Skip()

    def OnReferenceToggle(self, event):
        event.Skip()

    def OnToggleArray(self, event):
        event.Skip()

    def on_toggle_pointer(self, event):
        event.Skip()

    def OnConstPtrToggle(self, event):
        event.Skip()

    def OnToggleBitField(self, event):
        event.Skip()

    def OnVolatileToggle(self, event):
        event.Skip()

    def OnPointerPointerToggle(self, event):
        event.Skip()

    def OnVolatilePointerToggle(self, event):
        event.Skip()

    def OnToggleStatic(self, event):
        event.Skip()

    def OnFocusInitialization(self, event):
        event.Skip()

    def OnEditInitialValue(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class MemberMethodDialogBase
###########################################################################

class MemberMethodDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New method", pos=wx.DefaultPosition,
                           size=wx.Size(598, 644),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer12 = wx.FlexGridSizer(6, 1, 0, 0)
        fgSizer12.AddGrowableCol(0)
        fgSizer12.AddGrowableRow(3)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer121 = wx.FlexGridSizer(4, 5, 0, 0)
        fgSizer121.AddGrowableCol(2)
        fgSizer121.SetFlexibleDirection(wx.BOTH)
        fgSizer121.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.method_type_lb = wx.StaticText(self, wx.ID_ANY, u"method type:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.method_type_lb.Wrap(-1)
        fgSizer121.Add(self.method_type_lb, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        fgSizer121.Add((0, 0), 1, wx.EXPAND, 5)

        m_typeChoices = []
        self.m_type = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_typeChoices,
                                wx.CB_SORT | wx.WANTS_CHARS)
        self.m_type.SetSelection(0)
        self.m_type.Refresh()

        fgSizer121.Add(self.m_type, 0, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer121.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox85 = wx.CheckBox(self, wx.ID_ANY, u"&declare", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox85.SetValue(True)
        fgSizer121.Add(self.m_checkBox85, 0, wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText92 = wx.StaticText(self, wx.ID_ANY, u"type args:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText92.Wrap(-1)
        self.m_staticText92.Hide()

        fgSizer121.Add(self.m_staticText92, 0, wx.ALL, 5)

        self.m_staticText65 = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText65.Wrap(-1)
        self.m_staticText65.Enable(False)
        self.m_staticText65.Hide()

        fgSizer121.Add(self.m_staticText65, 0, wx.ALL, 5)

        self.m_template_args = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TE_PROCESS_ENTER)
        self.m_template_args.Enable(False)
        self.m_template_args.Hide()

        fgSizer121.Add(self.m_template_args, 0, wx.BOTTOM | wx.EXPAND, 5)

        self.m_staticText66 = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText66.Wrap(-1)
        self.m_staticText66.Enable(False)
        self.m_staticText66.Hide()

        fgSizer121.Add(self.m_staticText66, 0, wx.ALL, 5)

        fgSizer121.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY, u"method name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText6.Wrap(-1)
        fgSizer121.Add(self.m_staticText6, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        fgSizer121.Add((0, 0), 1, wx.EXPAND, 5)

        m_comboBox5Choices = [u"operator =", u"operator ==", u"operator !=", u"operator <", u"operator >",
                              u"operator <=", u"operator >=", u"operator +", u"operator -", u"operator *",
                              u"operator /", u"operator %", u"operator +=", u"operator -=", u"operator *=",
                              u"operator /=", u"operator %=", u"operator !", u"operator &&", u"operator ||",
                              u"operator ++", u"operator --", u"operator &", u"operator |", u"operator ^",
                              u"operator ~", u"operator &=", u"operator |=", u"operator ^=", u"operator <<",
                              u"operator >>", u"operator <<=", u"operator >>=", u"operator ->", u"operator ()",
                              u"operator []", u"operator new", u"operator delete", u"operator bool", u"operator int"]
        self.m_comboBox5 = wx.ComboBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       m_comboBox5Choices, wx.CB_SORT)
        fgSizer121.Add(self.m_comboBox5, 0, wx.EXPAND | wx.BOTTOM, 5)

        fgSizer121.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox86 = wx.CheckBox(self, wx.ID_ANY, u"&implement", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox86.SetValue(True)
        fgSizer121.Add(self.m_checkBox86, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, 5)

        self.m_checkBox63 = wx.CheckBox(self, wx.ID_ANY, u"template", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer121.Add(self.m_checkBox63, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_stStartTemplate = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stStartTemplate.Wrap(-1)
        self.m_stStartTemplate.Enable(False)

        fgSizer121.Add(self.m_stStartTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_template = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                      wx.TE_PROCESS_ENTER)
        self.m_template.Enable(False)

        fgSizer121.Add(self.m_template, 0, wx.EXPAND | wx.BOTTOM, 5)

        self.m_stEndTemplate = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stEndTemplate.Wrap(-1)
        self.m_stEndTemplate.Enable(False)

        fgSizer121.Add(self.m_stEndTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox126 = wx.CheckBox(self, wx.ID_ANY, u"deleted", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer121.Add(self.m_checkBox126, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer121, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer97 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer97.AddGrowableCol(0)
        fgSizer97.AddGrowableCol(1)
        fgSizer97.SetFlexibleDirection(wx.BOTH)
        fgSizer97.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Type modifiers"), wx.VERTICAL)

        sbSizer2.SetMinSize(wx.Size(-1, 200))
        fgSizer13 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer13.AddGrowableCol(0)
        fgSizer13.SetFlexibleDirection(wx.BOTH)
        fgSizer13.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox1 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"cons&t", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer13.Add(self.m_checkBox1, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox2 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"&reference", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer13.Add(self.m_checkBox2, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox3 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"&pointer", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer13.Add(self.m_checkBox3, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox4 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"&to pointer", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        self.m_checkBox4.Enable(False)

        fgSizer13.Add(self.m_checkBox4, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox5 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"c&onst ptr", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        self.m_checkBox5.Enable(False)

        fgSizer13.Add(self.m_checkBox5, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        sbSizer2.Add(fgSizer13, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer97.Add(sbSizer2, 0, wx.ALL | wx.EXPAND, 5)

        sbSizer3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Properties"), wx.VERTICAL)

        sbSizer3.SetMinSize(wx.Size(-1, 200))
        fgSizer20 = wx.FlexGridSizer(5, 2, 0, 0)
        fgSizer20.AddGrowableCol(0)
        fgSizer20.AddGrowableCol(1)
        fgSizer20.SetFlexibleDirection(wx.BOTH)
        fgSizer20.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText8 = wx.StaticText(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&access", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText8.Wrap(-1)
        fgSizer20.Add(self.m_staticText8, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.BOTTOM | wx.RIGHT | wx.LEFT,
                      5)

        m_choice2Choices = [u"public", u"protected", u"private"]
        self.m_choice2 = wx.Choice(sbSizer3.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                   m_choice2Choices, 0)
        self.m_choice2.SetSelection(0)
        self.m_choice2.Refresh()
        fgSizer20.Add(self.m_choice2, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox61 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"callin&g", wx.DefaultPosition,
                                        wx.DefaultSize, wx.ALIGN_RIGHT)
        fgSizer20.Add(self.m_checkBox61, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL,
                      5)

        m_comboBox1Choices = [u"cdecl", u"stdcall", u"fastcall", u"thiscall", u"naked", u"ms_abi", u"sysv_abi",
                              u"interrupt"]
        self.m_comboBox1 = wx.ComboBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"cdecl", wx.DefaultPosition, wx.DefaultSize,
                                       m_comboBox1Choices, 0)
        self.m_comboBox1.Enable(False)

        fgSizer20.Add(self.m_comboBox1, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox6 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&static", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox6, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox11 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&virtual", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox11, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox21 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"p&ure", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox21, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)
        self.m_checkBox21.Enable(False)

        self.m_checkBox31 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&inline", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox31, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox41 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"co&nst method", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer20.Add(self.m_checkBox41, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        sbSizer3.Add(fgSizer20, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer97.Add(sbSizer3, 0, wx.EXPAND | wx.ALL, 5)

        fgSizer12.Add(fgSizer97, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"N&otes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer12.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.m_richText1 = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                    wx.HSCROLL | wx.SIMPLE_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_richText1.SetMargins(left=20, top=20)
        fgSizer12.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer21 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer21.AddGrowableCol(0)
        fgSizer21.SetFlexibleDirection(wx.BOTH)
        fgSizer21.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer21.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button1 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer21.Add(self.m_button1, 0, wx.ALL, 5)

        self.m_button2 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button2.SetDefault()
        fgSizer21.Add(self.m_button2, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer21, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer12)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_type.Bind(wx.EVT_CHAR, self.OnChar)
        self.m_type.Bind(wx.EVT_CHOICE, self.on_type_changed)
        self.m_checkBox63.Bind(wx.EVT_CHECKBOX, self.on_toggle_template)
        self.m_template.Bind(wx.EVT_TEXT, self.on_change_template_specification)
        self.m_checkBox3.Bind(wx.EVT_CHECKBOX, self.on_toggle_pointer)
        self.m_checkBox61.Bind(wx.EVT_CHECKBOX, self.on_toggle_calling_convention)
        self.m_button1.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button2.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnChar(self, event):
        event.Skip()

    def on_type_changed(self, event):
        event.Skip()

    def on_toggle_template(self, event):
        event.Skip()

    def on_change_template_specification(self, event):
        event.Skip()

    def on_toggle_pointer(self, event):
        event.Skip()

    def on_toggle_calling_convention(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class MethodDialogBase
###########################################################################

class MethodDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New method", pos=wx.DefaultPosition,
                           size=wx.Size(578, 591),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer12 = wx.FlexGridSizer(6, 1, 0, 0)
        fgSizer12.AddGrowableCol(0)
        fgSizer12.AddGrowableRow(3)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer15 = wx.FlexGridSizer(4, 5, 0, 0)
        fgSizer15.AddGrowableCol(2)
        fgSizer15.SetFlexibleDirection(wx.BOTH)
        fgSizer15.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.method_type_lb = wx.StaticText(self, wx.ID_ANY, u"method type:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.method_type_lb.Wrap(-1)
        fgSizer15.Add(self.method_type_lb, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 5)

        fgSizer15.Add((0, 0), 1, wx.EXPAND, 5)

        m_typeChoices = []
        self.m_type = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_typeChoices,
                                wx.CB_SORT | wx.WANTS_CHARS)
        self.m_type.SetSelection(0)
        self.m_type.Refresh()
        fgSizer15.Add(self.m_type, 0, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer15.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox85 = wx.CheckBox(self, wx.ID_ANY, u"&declare", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox85.SetValue(True)
        self.m_checkBox85.Enable(False)

        fgSizer15.Add(self.m_checkBox85, 0, wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText92 = wx.StaticText(self, wx.ID_ANY, u"type args:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText92.Wrap(-1)
        self.m_staticText92.Hide()

        fgSizer15.Add(self.m_staticText92, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText651 = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText651.Wrap(-1)
        self.m_staticText651.Enable(False)
        self.m_staticText651.Hide()

        fgSizer15.Add(self.m_staticText651, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_template_args = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TE_PROCESS_ENTER)
        self.m_template_args.Enable(False)
        self.m_template_args.Hide()

        fgSizer15.Add(self.m_template_args, 0, wx.EXPAND | wx.BOTTOM, 5)

        self.m_staticText661 = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText661.Wrap(-1)
        self.m_staticText661.Enable(False)
        self.m_staticText661.Hide()

        fgSizer15.Add(self.m_staticText661, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        fgSizer15.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY, u"method name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText6.Wrap(-1)
        fgSizer15.Add(self.m_staticText6, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 5)

        fgSizer15.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_textCtrl5 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)

        fgSizer15.Add(self.m_textCtrl5, 0, wx.EXPAND | wx.BOTTOM, 5)

        fgSizer15.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox86 = wx.CheckBox(self, wx.ID_ANY, u"&implement", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox86.SetValue(True)
        self.m_checkBox86.Enable(False)

        fgSizer15.Add(self.m_checkBox86, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, 5)

        self.m_checkBox63 = wx.CheckBox(self, wx.ID_ANY, u"template", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer15.Add(self.m_checkBox63, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_stStartTemplate = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stStartTemplate.Wrap(-1)
        self.m_stStartTemplate.Enable(False)

        fgSizer15.Add(self.m_stStartTemplate, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_template = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                      wx.TE_PROCESS_ENTER)
        self.m_template.Enable(False)

        fgSizer15.Add(self.m_template, 0, wx.EXPAND  | wx.BOTTOM, 5)

        self.m_stEndTemplate = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stEndTemplate.Wrap(-1)
        self.m_stEndTemplate.Enable(False)

        fgSizer15.Add(self.m_stEndTemplate, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        fgSizer12.Add(fgSizer15, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer97 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer97.AddGrowableCol(1)
        fgSizer97.SetFlexibleDirection(wx.BOTH)
        fgSizer97.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Type"), wx.VERTICAL)

        fgSizer14 = wx.FlexGridSizer(2, 3, 0, 0)
        fgSizer14.SetFlexibleDirection(wx.BOTH)
        fgSizer14.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox1 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"&const", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer14.Add(self.m_checkBox1, 0, wx.ALL, 5)

        self.m_checkBox2 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"&reference", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer14.Add(self.m_checkBox2, 0, wx.ALL, 5)

        self.m_checkBox3 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"&pointer", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer14.Add(self.m_checkBox3, 0, wx.ALL, 5)

        self.m_checkBox4 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"&to pointer", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        self.m_checkBox4.Enable(False)

        fgSizer14.Add(self.m_checkBox4, 0, wx.ALL, 5)

        self.m_checkBox5 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"c&onst ptr", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        self.m_checkBox5.Enable(False)

        fgSizer14.Add(self.m_checkBox5, 0, wx.ALL, 5)

        sbSizer2.Add(fgSizer14, 1, wx.EXPAND, 5)

        fgSizer97.Add(sbSizer2, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Properties"), wx.VERTICAL)

        fgSizer20 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer20.AddGrowableCol(0)
        fgSizer20.AddGrowableCol(1)
        fgSizer20.AddGrowableRow(0)
        fgSizer20.AddGrowableRow(2)
        fgSizer20.SetFlexibleDirection(wx.BOTH)
        fgSizer20.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox6 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&static", wx.DefaultPosition,
                                       wx.DefaultSize, wx.ALIGN_RIGHT)
        fgSizer20.Add(self.m_checkBox6, 0, wx.ALL | wx.ALIGN_BOTTOM | wx.ALIGN_RIGHT, 5)

        self.m_checkBox31 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&inline", wx.DefaultPosition,
                                        wx.DefaultSize, wx.ALIGN_RIGHT)
        fgSizer20.Add(self.m_checkBox31, 0, wx.ALL | wx.ALIGN_BOTTOM, 5)

        self.m_checkBox61 = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"&calling", wx.DefaultPosition,
                                        wx.DefaultSize, wx.ALIGN_RIGHT)
        fgSizer20.Add(self.m_checkBox61, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        m_comboBox1Choices = [u"cdecl", u"stdcall", u"fastcall", u"thiscall", u"naked", u"ms_abi", u"sysv_abi",
                              u"interrupt"]
        self.m_comboBox1 = wx.ComboBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u"cdecl", wx.DefaultPosition, wx.DefaultSize,
                                       m_comboBox1Choices, 0)
        self.m_comboBox1.Enable(False)

        fgSizer20.Add(self.m_comboBox1, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer20.Add((0, 0), 1, wx.EXPAND, 5)

        sbSizer3.Add(fgSizer20, 1, wx.EXPAND | wx.BOTTOM | wx.RIGHT, 5)

        fgSizer97.Add(sbSizer3, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer12.Add(fgSizer97, 1, wx.EXPAND, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"Notes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer12.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.m_richText1 = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                    wx.SIMPLE_BORDER | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_richText1.SetMargins(left=20, top=20)
        fgSizer12.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer21 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer21.AddGrowableCol(0)
        fgSizer21.SetFlexibleDirection(wx.BOTH)
        fgSizer21.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer21.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button1 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer21.Add(self.m_button1, 0, wx.ALL, 5)

        self.m_button2 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button2.SetDefault()
        fgSizer21.Add(self.m_button2, 0, wx.ALL, 5)

        fgSizer12.Add(fgSizer21, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer12)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_type.Bind(wx.EVT_CHAR, self.OnChar)
        self.m_type.Bind(wx.EVT_CHOICE, self.on_type_changed)
        self.m_checkBox63.Bind(wx.EVT_CHECKBOX, self.on_toggle_template)
        self.m_template.Bind(wx.EVT_TEXT, self.on_change_template_specification)
        self.m_checkBox3.Bind(wx.EVT_CHECKBOX, self.on_toggle_pointer)
        self.m_checkBox61.Bind(wx.EVT_CHECKBOX, self.on_toggle_calling_convention)
        self.m_button1.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button2.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnChar(self, event):
        event.Skip()

    def on_type_changed(self, event):
        event.Skip()

    def on_toggle_template(self, event):
        event.Skip()

    def on_change_template_specification(self, event):
        event.Skip()

    def on_toggle_pointer(self, event):
        event.Skip()

    def on_toggle_calling_convention(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ModuleDialogBase
###########################################################################

class ModuleDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New module", pos=wx.DefaultPosition,
                           size=wx.Size(346, 247),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer106 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer106.AddGrowableCol(0)
        fgSizer106.AddGrowableRow(0)
        fgSizer106.SetFlexibleDirection(wx.BOTH)
        fgSizer106.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer40 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, wx.EmptyString), wx.HORIZONTAL)

        fgSizer108 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer108.AddGrowableCol(1)
        fgSizer108.SetFlexibleDirection(wx.HORIZONTAL)
        fgSizer108.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText98 = wx.StaticText(sbSizer40.GetStaticBox(), wx.ID_ANY, u"Name:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText98.Wrap(-1)
        fgSizer108.Add(self.m_staticText98, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_moduleName = wx.TextCtrl(sbSizer40.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer108.Add(self.m_moduleName, 1, wx.ALL | wx.EXPAND, 5)

        self.m_staticText70 = wx.StaticText(sbSizer40.GetStaticBox(), wx.ID_ANY, u"Header:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText70.Wrap(-1)
        fgSizer108.Add(self.m_staticText70, 0,
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.BOTTOM | wx.RIGHT, 5)

        self.m_choiceHeader = wx.TextCtrl(sbSizer40.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer108.Add(self.m_choiceHeader, 1, wx.EXPAND | wx.ALL, 5)

        self.m_staticText71 = wx.StaticText(sbSizer40.GetStaticBox(), wx.ID_ANY, u"Source:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText71.Wrap(-1)
        fgSizer108.Add(self.m_staticText71, 0,
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.BOTTOM | wx.RIGHT, 5)

        self.m_choiceSource = wx.TextCtrl(sbSizer40.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer108.Add(self.m_choiceSource, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer40.Add(fgSizer108, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer106.Add(sbSizer40, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer157 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer157.AddGrowableCol(1)
        fgSizer157.SetFlexibleDirection(wx.BOTH)
        fgSizer157.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer157.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer9 = wx.StdDialogButtonSizer()
        self.m_sdbSizer9OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer9.AddButton(self.m_sdbSizer9OK)

        self.m_sdbSizer9Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer9.AddButton(self.m_sdbSizer9Cancel)
        m_sdbSizer9.Realize();

        fgSizer157.Add(m_sdbSizer9, 1, wx.EXPAND , 5)

        fgSizer106.Add(fgSizer157, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer106)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_moduleName.Bind(wx.EVT_TEXT, self.OnNameChanged)
        self.m_sdbSizer9Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer9OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnNameChanged(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class NoteDialogBase
###########################################################################

class NoteDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New note", pos=wx.DefaultPosition,
                           size=wx.Size(480, 299),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer103 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer103.AddGrowableCol(0)
        fgSizer103.AddGrowableRow(0)
        fgSizer103.SetFlexibleDirection(wx.BOTH)
        fgSizer103.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer30 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, wx.EmptyString), wx.VERTICAL)

        self.m_text = wx.richtext.RichTextCtrl(sbSizer30.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.DefaultSize,
                                               0 | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)

        sbSizer30.Add(self.m_text, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer103.Add(sbSizer30, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer156 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer156.AddGrowableCol(1)
        fgSizer156.SetFlexibleDirection(wx.BOTH)
        fgSizer156.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer156.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer7 = wx.StdDialogButtonSizer()
        self.m_sdbSizer7OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer7.AddButton(self.m_sdbSizer7OK)

        self.m_sdbSizer7Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer7.AddButton(self.m_sdbSizer7Cancel)
        m_sdbSizer7.Realize();

        fgSizer156.Add(m_sdbSizer7, 1, wx.EXPAND, 5)

        fgSizer103.Add(fgSizer156, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer103)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer7Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer7OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class NamespaceDialogBase
###########################################################################

class NamespaceDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New namespace", pos=wx.DefaultPosition,
                           size=wx.Size(423, 390),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(1)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer7 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer7.AddGrowableCol(1)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer6.Add(fgSizer7, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer163 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer163.AddGrowableCol(1)
        fgSizer163.SetFlexibleDirection(wx.BOTH)
        fgSizer163.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer163.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK)

        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel)
        m_sdbSizer2.Realize();

        fgSizer163.Add(m_sdbSizer2, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer6.Add(fgSizer163, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer2Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer2OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class RelationDialogBase
###########################################################################

class RelationDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(
            self, parent,
            id=wx.ID_ANY,
            title=u"New relation",
            pos=wx.DefaultPosition,
            size=wx.Size(662, 1024),
            style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.BORDER_THEME)
        # dbc = wx.TheColourDatabase
        # fore = dbc.Find('LIGHT GREY')
        # back = dbc.Find('DARK GREY')
        # self.SetForegroundColour(fore)
        # self.SetBackgroundColour(back)

        self.SetSizeHints(wx.Size(600, 400), wx.DefaultSize)

        fgSizer190 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer190.AddGrowableCol(0)
        fgSizer190.AddGrowableRow(3)
        fgSizer190.SetFlexibleDirection(wx.BOTH)
        fgSizer190.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer51 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"&From"), wx.VERTICAL)

        fgSizer191 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer191.AddGrowableCol(1)
        fgSizer191.SetFlexibleDirection(wx.BOTH)
        fgSizer191.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText85 = wx.StaticText(sbSizer51.GetStaticBox(), wx.ID_ANY, u"class:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText85.Wrap(-1)
        fgSizer191.Add(self.m_staticText85, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_choice21Choices = []
        self.m_choice21 = wx.Choice(sbSizer51.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                    m_choice21Choices, 0)
        self.m_choice21.SetSelection(0)
        self.m_choice21.Refresh()
        fgSizer191.Add(self.m_choice21, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText86 = wx.StaticText(sbSizer51.GetStaticBox(), wx.ID_ANY, u"alias:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText86.Wrap(-1)
        fgSizer191.Add(self.m_staticText86, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl60 = wx.TextCtrl(sbSizer51.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer191.Add(self.m_textCtrl60, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        sbSizer51.Add(fgSizer191, 1, wx.EXPAND, 5)

        fgSizer190.Add(sbSizer51, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizer192 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer192.AddGrowableCol(0)
        fgSizer192.AddGrowableCol(1)
        fgSizer192.AddGrowableCol(2)
        fgSizer192.SetFlexibleDirection(wx.BOTH)
        fgSizer192.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer61 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Association type"), wx.VERTICAL)

        self.m_radioBtn5 = wx.RadioButton(sbSizer61.GetStaticBox(), wx.ID_ANY, u"&Single", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        sbSizer61.Add(self.m_radioBtn5, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_radioBtn6 = wx.RadioButton(sbSizer61.GetStaticBox(), wx.ID_ANY, u"&Multi", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        sbSizer61.Add(self.m_radioBtn6, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_radioBtn7 = wx.RadioButton(sbSizer61.GetStaticBox(), wx.ID_ANY, u"Static m&ulti", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        sbSizer61.Add(self.m_radioBtn7, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer192.Add(sbSizer61, 1, wx.EXPAND | wx.TOP | wx.RIGHT, 5)

        sbSizer53 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Association properties"), wx.VERTICAL)

        fgSizer193 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer193.SetFlexibleDirection(wx.BOTH)
        fgSizer193.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox92 = wx.CheckBox(sbSizer53.GetStaticBox(), wx.ID_ANY, u"&Aggregation", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer193.Add(self.m_checkBox92, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox93 = wx.CheckBox(sbSizer53.GetStaticBox(), wx.ID_ANY, u"Multi&ple", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer193.Add(self.m_checkBox93, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox94 = wx.CheckBox(sbSizer53.GetStaticBox(), wx.ID_ANY, u"&Critical", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer193.Add(self.m_checkBox94, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer193.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox96 = wx.CheckBox(sbSizer53.GetStaticBox(), wx.ID_ANY, u"Fi&lter", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox96.Enable(False)

        fgSizer193.Add(self.m_checkBox96, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        sbSizer53.Add(fgSizer193, 1, wx.EXPAND, 5)

        fgSizer192.Add(sbSizer53, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer54 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Implementation"), wx.VERTICAL)

        fgSizer194 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer194.SetFlexibleDirection(wx.BOTH)
        fgSizer194.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_radioBtn1 = wx.RadioButton(sbSizer54.GetStaticBox(), wx.ID_ANY, u"Standar&d", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        fgSizer194.Add(self.m_radioBtn1, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizer194.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_radioBtn3 = wx.RadioButton(sbSizer54.GetStaticBox(), wx.ID_ANY, u"&Value tree", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.m_radioBtn3.Enable(False)

        fgSizer194.Add(self.m_radioBtn3, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox97 = wx.CheckBox(sbSizer54.GetStaticBox(), wx.ID_ANY, u"Uni&que", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox97.Enable(False)

        fgSizer194.Add(self.m_checkBox97, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)

        self.m_radioBtn4 = wx.RadioButton(sbSizer54.GetStaticBox(), wx.ID_ANY, u"AV&L tree", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.m_radioBtn4.Enable(False)

        fgSizer194.Add(self.m_radioBtn4, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        sbSizer54.Add(fgSizer194, 1, wx.EXPAND, 5)

        fgSizer192.Add(sbSizer54, 1, wx.EXPAND | wx.TOP | wx.LEFT, 5)

        fgSizer190.Add(fgSizer192, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer511 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"&To"), wx.VERTICAL)

        fgSizer1911 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer1911.AddGrowableCol(1)
        fgSizer1911.SetFlexibleDirection(wx.BOTH)
        fgSizer1911.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText851 = wx.StaticText(sbSizer511.GetStaticBox(), wx.ID_ANY, u"class:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText851.Wrap(-1)
        fgSizer1911.Add(self.m_staticText851, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_choice211Choices = []
        self.m_choice211 = wx.Choice(sbSizer511.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                     m_choice211Choices, 0)
        self.m_choice211.SetSelection(0)
        self.m_choice211.Refresh()
        fgSizer1911.Add(self.m_choice211, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText861 = wx.StaticText(sbSizer511.GetStaticBox(), wx.ID_ANY, u"alias:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText861.Wrap(-1)
        fgSizer1911.Add(self.m_staticText861, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl601 = wx.TextCtrl(sbSizer511.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer1911.Add(self.m_textCtrl601, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText91 = wx.StaticText(sbSizer511.GetStaticBox(), wx.ID_ANY, u"member:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText91.Wrap(-1)
        self.m_staticText91.Enable(False)
        self.m_staticText91.SetMinSize(wx.Size(-1, 24))

        fgSizer1911.Add(self.m_staticText91, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_choice24Choices = []
        self.m_choice24 = wx.Choice(sbSizer511.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                    m_choice24Choices, 0)
        self.m_choice24.SetSelection(0)
        self.m_choice24.Refresh()
        self.m_choice24.Enable(False)
        # self.m_choice24.Hide()

        fgSizer1911.Add(self.m_choice24, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        sbSizer511.Add(fgSizer1911, 0, wx.EXPAND, 5)

        fgSizer190.Add(sbSizer511, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)
        sbSizer9.SetMinSize(wx.Size(62, 200))

        fgSizer190.Add(sbSizer9, 1, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP | wx.BOTTOM, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.HORIZONTAL)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        sbSizer9.SetMinSize(wx.Size(62, 50))

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer43.Add(self.m_info, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_button7 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button7.SetDefault()
        fgSizer43.Add(self.m_button7, 0, wx.ALL, 5)

        fgSizer190.Add(fgSizer43, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(fgSizer190)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_choice21.Bind(wx.EVT_CHOICE, self.OnChangeFrom)
        self.m_radioBtn5.Bind(wx.EVT_RADIOBUTTON, self.OnSingle)
        self.m_radioBtn6.Bind(wx.EVT_RADIOBUTTON, self.OnMulti)
        self.m_radioBtn7.Bind(wx.EVT_RADIOBUTTON, self.OnStaticMulti)
        self.m_radioBtn1.Bind(wx.EVT_RADIOBUTTON, self.OnStandard)
        self.m_radioBtn3.Bind(wx.EVT_RADIOBUTTON, self.OnValueTree)
        self.m_radioBtn4.Bind(wx.EVT_RADIOBUTTON, self.OnAVLTree)
        self.m_choice211.Bind(wx.EVT_CHOICE, self.OnChangeTo)
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnChangeFrom(self, event):
        event.Skip()

    def OnSingle(self, event):
        event.Skip()

    def OnMulti(self, event):
        event.Skip()

    def OnStaticMulti(self, event):
        event.Skip()

    def OnStandard(self, event):
        event.Skip()

    def OnValueTree(self, event):
        event.Skip()

    def OnAVLTree(self, event):
        event.Skip()

    def OnChangeTo(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class IteratorWizardDialogBase
###########################################################################

class IteratorWizardDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Iterator wizard", pos=wx.DefaultPosition,
                           size=wx.Size(730, 454),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer197 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer197.AddGrowableCol(0)
        fgSizer197.AddGrowableRow(0)
        fgSizer197.SetFlexibleDirection(wx.BOTH)
        fgSizer197.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer198 = wx.FlexGridSizer(3, 3, 0, 0)
        fgSizer198.AddGrowableCol(0)
        fgSizer198.AddGrowableCol(1)
        fgSizer198.AddGrowableCol(2)
        fgSizer198.AddGrowableRow(1)
        fgSizer198.SetFlexibleDirection(wx.BOTH)
        fgSizer198.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText91 = wx.StaticText(self, wx.ID_ANY, u"&source:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText91.Wrap(-1)
        fgSizer198.Add(self.m_staticText91, 0, wx.ALL | wx.ALIGN_BOTTOM, 5)

        self.m_staticText92 = wx.StaticText(self, wx.ID_ANY, u"&relation:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText92.Wrap(-1)
        fgSizer198.Add(self.m_staticText92, 0, wx.ALL | wx.ALIGN_BOTTOM, 5)

        self.m_staticText93 = wx.StaticText(self, wx.ID_ANY, u"&filter:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText93.Wrap(-1)
        fgSizer198.Add(self.m_staticText93, 0, wx.ALL | wx.ALIGN_BOTTOM, 5)

        self.m_source_tree = wxx.TreeCtrl(self, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                                          style=wx.TR_DEFAULT_STYLE | wx.TR_HIDE_ROOT | wx.TR_SINGLE | wx.SUNKEN_BORDER)

        fgSizer198.Add(self.m_source_tree, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        m_relation_listChoices = []
        self.m_relation_list = wx.ListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_relation_listChoices,
                                          wx.LB_SINGLE)
        fgSizer198.Add(self.m_relation_list, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        m_filter_listChoices = []
        self.m_filter_list = wx.ListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_filter_listChoices, 0)
        fgSizer198.Add(self.m_filter_list, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer199 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer199.AddGrowableCol(1)
        fgSizer199.SetFlexibleDirection(wx.BOTH)
        fgSizer199.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText94 = wx.StaticText(self, wx.ID_ANY, u"&iterator name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText94.Wrap(-1)
        fgSizer199.Add(self.m_staticText94, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl62 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.TE_PROCESS_ENTER)
        fgSizer199.Add(self.m_textCtrl62, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer198.Add(fgSizer199, 1, wx.EXPAND, 5)

        fgSizer200 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer200.SetFlexibleDirection(wx.BOTH)
        fgSizer200.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox97 = wx.CheckBox(self, wx.ID_ANY, u"rese&t", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer200.Add(self.m_checkBox97, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox98 = wx.CheckBox(self, wx.ID_ANY, u"&backward", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer200.Add(self.m_checkBox98, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer198.Add(fgSizer200, 1, wx.EXPAND, 5)

        fgSizer197.Add(fgSizer198, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer43.Add(self.m_info, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_ok = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_ok.SetDefault()
        self.m_ok.Enable(False)

        fgSizer43.Add(self.m_ok, 0, wx.ALL, 5)

        fgSizer197.Add(fgSizer43, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer197)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_source_tree.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnChangedSource)
        self.m_relation_list.Bind(wx.EVT_LISTBOX, self.OnChangedRelation)
        self.m_filter_list.Bind(wx.EVT_LISTBOX, self.OnChangedFilter)
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_ok.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnChangedSource(self, event):
        event.Skip()

    def OnChangedRelation(self, event):
        event.Skip()

    def OnChangedFilter(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class TypeDialogBase
###########################################################################

class TypeDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New type", pos=wx.DefaultPosition,
                           size=wx.Size(546, 562),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(1)
        fgSizer6.AddGrowableRow(2)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer7 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer7.AddGrowableCol(1)
        fgSizer7.AddGrowableRow(0)
        fgSizer7.AddGrowableRow(1)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL | wx.EXPAND, 5)

        self.m_checkBoxTemplate = wx.CheckBox(self, wx.ID_ANY, u"template", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer7.Add(self.m_checkBoxTemplate, 0, wx.ALL, 5)

        fgSizer93 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer93.AddGrowableCol(1)
        fgSizer93.AddGrowableRow(0)
        fgSizer93.SetFlexibleDirection(wx.BOTH)
        fgSizer93.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_stStartTemplate = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stStartTemplate.Wrap(-1)
        self.m_stStartTemplate.Enable(False)

        fgSizer93.Add(self.m_stStartTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_template = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                      wx.TE_PROCESS_ENTER)
        self.m_template.Enable(False)

        fgSizer93.Add(self.m_template, 0, wx.ALL | wx.EXPAND, 5)

        self.m_stEndTemplate = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_stEndTemplate.Wrap(-1)
        self.m_stEndTemplate.Enable(False)

        fgSizer93.Add(self.m_stEndTemplate, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer7.Add(fgSizer93, 1, wx.EXPAND, 5)

        self.m_accessLb = wx.StaticText(self, wx.ID_ANY, u"access", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_accessLb.Wrap(-1)
        self.m_accessLb.Enable(False)

        fgSizer7.Add(self.m_accessLb, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        m_accessChoices = [u"public", u"protected", u"private"]
        self.m_access = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_accessChoices, 0)
        self.m_access.SetSelection(0)
        self.m_access.Refresh()
        self.m_access.Enable(False)

        fgSizer7.Add(self.m_access, 0, wx.ALL, 5)

        fgSizer6.Add(fgSizer7, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer20 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Declaration"), wx.VERTICAL)

        self.m_richText2 = wx.richtext.RichTextCtrl(sbSizer20.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer20.Add(self.m_richText2, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer20, 1, wx.EXPAND, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)

        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer6.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_checkBoxTemplate.Bind(wx.EVT_CHECKBOX, self.on_toggle_template)
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_toggle_template(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class VariableDialogBase
###########################################################################

class VariableDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New variable", pos=wx.DefaultPosition,
                           size=wx.Size(483, 409),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer23 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer23.AddGrowableCol(0)
        fgSizer23.AddGrowableRow(2)
        fgSizer23.SetFlexibleDirection(wx.BOTH)
        fgSizer23.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer24 = wx.FlexGridSizer(2, 5, 0, 0)
        fgSizer24.AddGrowableCol(1)
        fgSizer24.AddGrowableCol(3)
        fgSizer24.SetFlexibleDirection(wx.BOTH)
        fgSizer24.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer24.Add(self.m_staticText9, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_textCtrl6 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER | wx.SUNKEN_BORDER)
        fgSizer24.Add(self.m_textCtrl6, 0, wx.EXPAND | wx.TOP | wx.RIGHT, 5)

        self.m_staticText11 = wx.StaticText(self, wx.ID_ANY, u"=", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText11.Wrap(-1)
        fgSizer24.Add(self.m_staticText11, 0, wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_textCtrl8 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        fgSizer24.Add(self.m_textCtrl8, 0, wx.EXPAND | wx.TOP | wx.RIGHT, 5)

        fgSizer24.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText10 = wx.StaticText(self, wx.ID_ANY, u"type", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText10.Wrap(-1)
        fgSizer24.Add(self.m_staticText10, 0, wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.RIGHT | wx.LEFT,
                      5)

        m_choice1Choices = []
        self.m_choice1 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice1Choices,
                                   wx.CB_SORT | wx.WANTS_CHARS)
        self.m_choice1.SetSelection(0)
        self.m_choice1.Refresh()
        fgSizer24.Add(self.m_choice1, 0, wx.EXPAND | wx.TOP | wx.RIGHT, 5)

        self.m_staticText67 = wx.StaticText(self, wx.ID_ANY, u"<", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText67.Wrap(-1)
        self.m_staticText67.Enable(False)

        fgSizer24.Add(self.m_staticText67, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_template_args = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TE_PROCESS_ENTER)
        self.m_template_args.Enable(False)

        fgSizer24.Add(self.m_template_args, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText68 = wx.StaticText(self, wx.ID_ANY, u">", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText68.Wrap(-1)
        self.m_staticText68.Enable(False)

        fgSizer24.Add(self.m_staticText68, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer23.Add(fgSizer24, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer5 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"properties"), wx.VERTICAL)

        fgSizer25 = wx.FlexGridSizer(3, 4, 0, 0)
        fgSizer25.SetFlexibleDirection(wx.BOTH)
        fgSizer25.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_checkBox105 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"&static", wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        fgSizer25.Add(self.m_checkBox105, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox13 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"&pointer", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer25.Add(self.m_checkBox13, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox11 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"&const", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer25.Add(self.m_checkBox11, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox12 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"&reference", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer25.Add(self.m_checkBox12, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_checkBox49 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"&volatile", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer25.Add(self.m_checkBox49, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox14 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"c&onst pointer", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox14.Enable(False)

        fgSizer25.Add(self.m_checkBox14, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox50 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"vo&latile pointer", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox50.Enable(False)

        fgSizer25.Add(self.m_checkBox50, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_checkBox15 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"po&inter/pointer", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox15.Enable(False)

        fgSizer25.Add(self.m_checkBox15, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer25.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox17 = wx.CheckBox(sbSizer5.GetStaticBox(), wx.ID_ANY, u"a&rray", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer25.Add(self.m_checkBox17, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl7 = wx.TextCtrl(sbSizer5.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                       wx.DefaultSize, wx.TE_PROCESS_ENTER)
        self.m_textCtrl7.Enable(False)
        self.m_textCtrl7.Hide()

        fgSizer25.Add(self.m_textCtrl7, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer25.Add((0, 0), 1, wx.EXPAND, 5)

        sbSizer5.Add(fgSizer25, 1, wx.EXPAND, 5)

        fgSizer23.Add(sbSizer5, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        sbSizer6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"notes"), wx.VERTICAL)

        self.m_richText1 = wx.richtext.RichTextCtrl(sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    wx.SIMPLE_BORDER | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_richText1.SetMargins(left=20, top=20)
        sbSizer6.Add(self.m_richText1, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer23.Add(sbSizer6, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer23.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer23)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_choice1.Bind(wx.EVT_CHOICE, self.on_type_changed)
        self.m_choice1.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.m_checkBox105.Bind(wx.EVT_CHECKBOX, self.OnToggleStatic)
        self.m_checkBox13.Bind(wx.EVT_CHECKBOX, self.on_toggle_pointer)
        self.m_checkBox17.Bind(wx.EVT_CHECKBOX, self.OnToggleArray)
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_type_changed(self, event):
        event.Skip()

    def OnKeyDown(self, event):
        event.Skip()

    def OnToggleStatic(self, event):
        event.Skip()

    def on_toggle_pointer(self, event):
        event.Skip()

    def OnToggleArray(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class SelectClassesDialogBase
###########################################################################

class SelectClassesDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Select Classes", pos=wx.DefaultPosition,
                           size=wx.Size(551, 310),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer63 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer63.AddGrowableCol(0)
        fgSizer63.AddGrowableRow(1)
        fgSizer63.SetFlexibleDirection(wx.BOTH)
        fgSizer63.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer63.Add(self.m_info, 0, wx.ALIGN_RIGHT, 5)

        m_classChecklistChoices = []
        self.m_classChecklist = wx.CheckListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                m_classChecklistChoices, 0)
        self.m_classChecklist.SetMinSize(wx.Size(-1, 200))
        self.m_classChecklist.SetMaxSize(wx.Size(-1, 200))

        fgSizer63.Add(self.m_classChecklist, 1, wx.EXPAND | wx.ALL, 5)

        self._sizer_buttons = wx.FlexGridSizer(1, 3, 0, 0)
        self._sizer_buttons.AddGrowableCol(0)
        self._sizer_buttons.SetFlexibleDirection(wx.BOTH)
        self._sizer_buttons.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self._sizer_buttons.SetMinSize(wx.Size(62, 42))

        self._sizer_buttons.Add((0, 42), 1, wx.EXPAND, 5)

        self.m_button21 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        self._sizer_buttons.Add(self.m_button21, 0, wx.ALL, 5)

        self.m_button22 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button22.SetDefault()
        self._sizer_buttons.Add(self.m_button22, 0, wx.ALL, 5)

        fgSizer63.Add(self._sizer_buttons, 0, wx.EXPAND, 5)

        self.SetSizer(fgSizer63)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.Bind(wx.EVT_INIT_DIALOG, self.OnInitDialog)
        self.m_button21.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button22.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnInitDialog(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class SelectContextsDialogBase
###########################################################################

class SelectContextsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Select contexts", pos=wx.DefaultPosition,
                           size=wx.Size(736, 428),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer126 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer126.AddGrowableCol(0)
        fgSizer126.AddGrowableRow(0)
        fgSizer126.SetFlexibleDirection(wx.BOTH)
        fgSizer126.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer127 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer127.AddGrowableCol(0)
        fgSizer127.AddGrowableRow(0)
        fgSizer127.SetFlexibleDirection(wx.BOTH)
        fgSizer127.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer128 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer128.AddGrowableCol(0)
        fgSizer128.AddGrowableCol(1)
        fgSizer128.AddGrowableRow(0)
        fgSizer128.SetFlexibleDirection(wx.BOTH)
        fgSizer128.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_ALL)

        sbSizer37 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Available contexts"), wx.VERTICAL)

        self.m_listCtrl5 = wx.ListCtrl(sbSizer37.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(150, -1),
                                       wx.LC_NO_HEADER | wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.m_listCtrl5.Refresh()
        sbSizer37.Add(self.m_listCtrl5, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer128.Add(sbSizer37, 1, wx.EXPAND, 5)

        sbSizer38 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Assigned contexts"), wx.VERTICAL)

        self.m_listCtrl4 = wx.ListCtrl(sbSizer38.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(150, -1),
                                       wx.LC_NO_HEADER | wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.m_listCtrl4.Refresh()
        sbSizer38.Add(self.m_listCtrl4, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer128.Add(sbSizer38, 1, wx.EXPAND, 5)

        fgSizer127.Add(fgSizer128, 1, wx.EXPAND, 5)

        fgSizer129 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer129.AddGrowableCol(0)
        fgSizer129.AddGrowableRow(2)
        fgSizer129.SetFlexibleDirection(wx.BOTH)
        fgSizer129.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_button31 = wx.Button(self, wx.ID_ANY, u"Move up", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button31.Enable(False)

        fgSizer129.Add(self.m_button31, 0, wx.ALL | wx.EXPAND, 5)

        self.m_button32 = wx.Button(self, wx.ID_ANY, u"Move down", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button32.Enable(False)

        fgSizer129.Add(self.m_button32, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer127.Add(fgSizer129, 1, wx.EXPAND, 5)

        fgSizer126.Add(fgSizer127, 1, wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)

        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer126.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer126)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_listCtrl5.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnAddContext)
        self.m_listCtrl4.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnRemoveContext)
        self.m_listCtrl4.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnSelectAssigned)
        self.m_button31.Bind(wx.EVT_BUTTON, self.OnMoveUp)
        self.m_button32.Bind(wx.EVT_BUTTON, self.OnMoveDown)
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnAddContext(self, event):
        event.Skip()

    def OnRemoveContext(self, event):
        event.Skip()

    def OnSelectAssigned(self, event):
        event.Skip()

    def OnMoveUp(self, event):
        event.Skip()

    def OnMoveDown(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class IsClassMethodsDialogBase
###########################################################################

class IsClassMethodsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"is_class methods", pos=wx.DefaultPosition,
                           size=wx.Size(441, 320),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer154 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer154.AddGrowableCol(0)
        fgSizer154.AddGrowableRow(0)
        fgSizer154.SetFlexibleDirection(wx.BOTH)
        fgSizer154.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_checkList2Choices = []
        self.m_checkList2 = wx.CheckListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_checkList2Choices, 0)
        fgSizer154.Add(self.m_checkList2, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)

        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer154.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer154)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class VirtualMethodsDialogBase
###########################################################################

class VirtualMethodsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"virtual methods", pos=wx.DefaultPosition,
                           size=wx.Size(441, 320),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer154 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer154.AddGrowableCol(0)
        fgSizer154.AddGrowableRow(0)
        fgSizer154.SetFlexibleDirection(wx.BOTH)
        fgSizer154.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_checkList2Choices = []
        self.m_checkList2 = wx.CheckListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_checkList2Choices, 0)
        fgSizer154.Add(self.m_checkList2, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_std_buttons = wx.StdDialogButtonSizer()
        self.m_std_buttonsOK = wx.Button(self, wx.ID_OK)
        m_std_buttons.AddButton(self.m_std_buttonsOK)

        self.m_std_buttonsCancel = wx.Button(self, wx.ID_CANCEL)
        m_std_buttons.AddButton(self.m_std_buttonsCancel)
        m_std_buttons.Realize();

        fgSizer159.Add(m_std_buttons, 1, wx.EXPAND, 5)

        fgSizer154.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer154)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_std_buttonsCancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_std_buttonsOK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class SelectFriendsDialogBase
###########################################################################

class SelectFriendsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"select friends", pos=wx.DefaultPosition,
                           size=wx.Size(441, 320),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer154 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer154.AddGrowableCol(0)
        fgSizer154.AddGrowableRow(0)
        fgSizer154.SetFlexibleDirection(wx.BOTH)
        fgSizer154.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_checkList2Choices = []
        self.m_checkList2 = wx.CheckListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_checkList2Choices,
                                            wx.LB_ALWAYS_SB)
        self.m_checkList2.SetMinSize(wx.Size(-1, 240))
        self.m_checkList2.SetMaxSize(wx.Size(-1, 240))

        fgSizer154.Add(self.m_checkList2, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer159.SetMinSize(wx.Size(-1, 42))
        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        self.m_info.SetMinSize(wx.Size(32, 32))

        fgSizer159.Add(self.m_info, 0, wx.ALL | wx.FIXED_MINSIZE, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)

        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();
        m_sdbSizer12.SetMinSize(wx.Size(-1, 32))

        fgSizer159.Add(m_sdbSizer12, 0, wx.EXPAND, 5)

        fgSizer154.Add(fgSizer159, 0, wx.EXPAND | wx.FIXED_MINSIZE, 5)

        self.SetSizer(fgSizer154)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class UserSectionsDialogBase
###########################################################################

class UserSectionsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"user sections", pos=wx.DefaultPosition,
                           size=wx.Size(764, 423),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer158 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer158.AddGrowableCol(0)
        fgSizer158.AddGrowableRow(0)
        fgSizer158.SetFlexibleDirection(wx.BOTH)
        fgSizer158.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_sections_book = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT)
        self.m_panel22 = wx.Panel(self.m_sections_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer162 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer162.AddGrowableCol(0)
        fgSizer162.AddGrowableRow(0)
        fgSizer162.SetFlexibleDirection(wx.BOTH)
        fgSizer162.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        from beatle.activity.models.cc.ui.ctrl import Editor
        self.m_h1 = Editor(self.m_panel22, **self._editorArgs)
        fgSizer162.Add(self.m_h1, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel22.SetSizer(fgSizer162)
        self.m_panel22.Layout()
        fgSizer162.Fit(self.m_panel22)
        self.m_sections_book.AddPage(self.m_panel22, u"before class declaration", False)
        self.m_panel23 = wx.Panel(self.m_sections_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer1621 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer1621.AddGrowableCol(0)
        fgSizer1621.AddGrowableRow(0)
        fgSizer1621.SetFlexibleDirection(wx.BOTH)
        fgSizer1621.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_h2 = Editor(self.m_panel23, **self._editorArgs)
        fgSizer1621.Add(self.m_h2, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel23.SetSizer(fgSizer1621)
        self.m_panel23.Layout()
        fgSizer1621.Fit(self.m_panel23)
        self.m_sections_book.AddPage(self.m_panel23, u"inside class declaration (first)", False)
        self.m_panel231 = wx.Panel(self.m_sections_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                   wx.TAB_TRAVERSAL)
        fgSizer16211 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer16211.AddGrowableCol(0)
        fgSizer16211.AddGrowableRow(0)
        fgSizer16211.SetFlexibleDirection(wx.BOTH)
        fgSizer16211.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_h3 = Editor(self.m_panel231, **self._editorArgs)
        fgSizer16211.Add(self.m_h3, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel231.SetSizer(fgSizer16211)
        self.m_panel231.Layout()
        fgSizer16211.Fit(self.m_panel231)
        self.m_sections_book.AddPage(self.m_panel231, u"inside class declaration (last)", False)
        self.m_panel24 = wx.Panel(self.m_sections_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer1622 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer1622.AddGrowableCol(0)
        fgSizer1622.AddGrowableRow(0)
        fgSizer1622.SetFlexibleDirection(wx.BOTH)
        fgSizer1622.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_h4 = Editor(self.m_panel24, **self._editorArgs)
        fgSizer1622.Add(self.m_h4, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel24.SetSizer(fgSizer1622)
        self.m_panel24.Layout()
        fgSizer1622.Fit(self.m_panel24)
        self.m_sections_book.AddPage(self.m_panel24, u"after class declaration", False)
        self.m_panel25 = wx.Panel(self.m_sections_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer1623 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer1623.AddGrowableCol(0)
        fgSizer1623.AddGrowableRow(0)
        fgSizer1623.SetFlexibleDirection(wx.BOTH)
        fgSizer1623.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_s1 = Editor(self.m_panel25, **self._editorArgs)
        fgSizer1623.Add(self.m_s1, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel25.SetSizer(fgSizer1623)
        self.m_panel25.Layout()
        fgSizer1623.Fit(self.m_panel25)
        self.m_sections_book.AddPage(self.m_panel25, u"before implementation includes", False)
        self.m_panel26 = wx.Panel(self.m_sections_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer1624 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer1624.AddGrowableCol(0)
        fgSizer1624.AddGrowableRow(0)
        fgSizer1624.SetFlexibleDirection(wx.BOTH)
        fgSizer1624.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_s2 = Editor(self.m_panel26, **self._editorArgs)
        fgSizer1624.Add(self.m_s2, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel26.SetSizer(fgSizer1624)
        self.m_panel26.Layout()
        fgSizer1624.Fit(self.m_panel26)
        self.m_sections_book.AddPage(self.m_panel26, u"before implementation", False)
        self.m_panel27 = wx.Panel(self.m_sections_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer1625 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer1625.AddGrowableCol(0)
        fgSizer1625.AddGrowableRow(0)
        fgSizer1625.SetFlexibleDirection(wx.BOTH)
        fgSizer1625.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_s3 = Editor(self.m_panel27, **self._editorArgs)
        fgSizer1625.Add(self.m_s3, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel27.SetSizer(fgSizer1625)
        self.m_panel27.Layout()
        fgSizer1625.Fit(self.m_panel27)
        self.m_sections_book.AddPage(self.m_panel27, u"after implementation", False)
        fgSizer158.Add(self.m_sections_book, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)

        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer158.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer158)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_h1.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_h1.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.m_h2.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_h2.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.m_h3.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_h3.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.m_h4.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_h4.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.m_s1.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_s1.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.m_s2.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_s2.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.m_s3.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        self.m_s3.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnKillFocus(self, event):
        event.Skip()

    def OnGetFocus(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class SelectLibrariesDialogBase
###########################################################################

class SelectLibrariesDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Select libraries  ...", pos=wx.DefaultPosition,
                           size=wx.Size(383, 351),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        dbc = wx.TheColourDatabase

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer178 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer178.AddGrowableCol(0)
        fgSizer178.AddGrowableRow(0)
        fgSizer178.SetFlexibleDirection(wx.BOTH)
        fgSizer178.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_choicebook4 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT)
        self.m_panel29 = wx.Panel(self.m_choicebook4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer183 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer183.AddGrowableCol(0)
        fgSizer183.AddGrowableRow(0)
        fgSizer183.SetFlexibleDirection(wx.BOTH)
        fgSizer183.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_checkList6Choices = []
        self.m_checkList6 = wx.CheckListBox(self.m_panel29, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            m_checkList6Choices, 0)
        fgSizer183.Add(self.m_checkList6, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel29.SetSizer(fgSizer183)
        self.m_panel29.Layout()
        fgSizer183.Fit(self.m_panel29)
        self.m_choicebook4.AddPage(self.m_panel29, u"builtin libraries", False)
        self.m_panel28 = wx.Panel(self.m_choicebook4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer182 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer182.SetFlexibleDirection(wx.BOTH)
        fgSizer182.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_panel28.SetSizer(fgSizer182)
        self.m_panel28.Layout()
        fgSizer182.Fit(self.m_panel28)
        self.m_choicebook4.AddPage(self.m_panel28, u"custom libraries", False)
        fgSizer178.Add(self.m_choicebook4, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)

        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer178.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer178)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class NavigatorDialogBase
###########################################################################

class NavigatorDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                           size=wx.Size(409, 580),
                           style=0 | wx.NO_BORDER | wx.WANTS_CHARS)
        dbc = wx.TheColourDatabase

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle(self.GetExtraStyle() | wx.WS_EX_BLOCK_EVENTS)

        fgSizer189 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer189.AddGrowableCol(0)
        fgSizer189.AddGrowableRow(0)
        fgSizer189.SetFlexibleDirection(wx.BOTH)
        fgSizer189.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_tree = wxx.TreeCtrl(self, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                                   style=wx.TR_DEFAULT_STYLE | wx.TR_FULL_ROW_HIGHLIGHT |
                                          wx.TR_HIDE_ROOT | wx.TR_SINGLE | wx.NO_BORDER)
        fgSizer189.Add(self.m_tree, 0, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(fgSizer189)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.Bind(wx.EVT_KILL_FOCUS, self.OnLoseFocus)
        self.m_tree.Bind(wx.EVT_ERASE_BACKGROUND, self.OnEraseBrackground)
        self.m_tree.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.OnChoice)
        self.m_tree.Bind(wx.EVT_TREE_ITEM_EXPANDED, self.on_expand_item)
        self.m_tree.Bind(wx.EVT_TREE_SEL_CHANGED, self.on_select_element)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnLoseFocus(self, event):
        event.Skip()

    def OnEraseBrackground(self, event):
        event.Skip()

    def OnChoice(self, event):
        event.Skip()

    def on_expand_item(self, event):
        event.Skip()

    def on_select_element(self, event):
        event.Skip()


###########################################################################
## Class CCBuildSettingsPaneBase
###########################################################################

class CCBuildSettingsPaneBase(wx.Panel):

    def __init__(self, parent):
        super(CCBuildSettingsPaneBase, self).__init__(parent, size=wx.Size(719, 520),
                                                      style=wx.FULL_REPAINT_ON_RESIZE | wx.TAB_TRAVERSAL)
        dbc = wx.TheColourDatabase

        fgSizer200 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer200.AddGrowableCol(0)
        fgSizer200.AddGrowableRow(3)
        fgSizer200.SetFlexibleDirection(wx.BOTH)
        fgSizer200.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText92 = wx.StaticText(self, wx.ID_ANY, u"C++ Build Settings", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText92.Wrap(-1)
        self.m_staticText92.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))

        fgSizer200.Add(self.m_staticText92, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticline1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        fgSizer200.Add(self.m_staticline1, 0, wx.EXPAND | wx.ALL, 5)

        fgSizer201 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer201.AddGrowableCol(1)
        fgSizer201.SetFlexibleDirection(wx.BOTH)
        fgSizer201.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText93 = wx.StaticText(self, wx.ID_ANY, u"configuration", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText93.Wrap(-1)
        fgSizer201.Add(self.m_staticText93, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_build_configsChoices = [u"default"]
        self.m_build_configs = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_build_configsChoices, 0)
        self.m_build_configs.SetSelection(0)
        self.m_build_configs.Refresh()
        fgSizer201.Add(self.m_build_configs, 0, wx.ALL | wx.EXPAND, 5)

        self.m_button58 = wx.Button(self, wx.ID_ANY, u"configurations ...", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer201.Add(self.m_button58, 0, wx.ALL, 5)

        fgSizer200.Add(fgSizer201, 1, wx.EXPAND, 5)

        self.m_auinotebook1 = wxx.AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              0 | wx.FULL_REPAINT_ON_RESIZE)
        self.m_panel45 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer268 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer268.AddGrowableCol(0)
        fgSizer268.AddGrowableRow(1)
        fgSizer268.SetFlexibleDirection(wx.BOTH)
        fgSizer268.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer269 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer269.AddGrowableCol(1)
        fgSizer269.SetFlexibleDirection(wx.BOTH)
        fgSizer269.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText129 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"type:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText129.Wrap(-1)
        fgSizer269.Add(self.m_staticText129, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_cpp_build_typeChoices = []
        self.m_cpp_build_type = wx.Choice(self.m_panel45, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                          m_cpp_build_typeChoices, 0)
        self.m_cpp_build_type.SetSelection(0)
        self.m_cpp_build_type.Refresh()
        fgSizer269.Add(self.m_cpp_build_type, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText131 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText131.Wrap(-1)
        fgSizer269.Add(self.m_staticText131, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_build_name = wx.TextCtrl(self.m_panel45, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer269.Add(self.m_cpp_build_name, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText132 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"extension:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText132.Wrap(-1)
        fgSizer269.Add(self.m_staticText132, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_build_extension = wx.TextCtrl(self.m_panel45, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                 wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer269.Add(self.m_cpp_build_extension, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText133 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"prefix:", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText133.Wrap(-1)
        fgSizer269.Add(self.m_staticText133, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_build_prefix = wx.TextCtrl(self.m_panel45, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer269.Add(self.m_cpp_build_prefix, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer268.Add(fgSizer269, 1, wx.EXPAND, 5)

        self.m_panel45.SetSizer(fgSizer268)
        self.m_panel45.Layout()
        fgSizer268.Fit(self.m_panel45)
        self.m_auinotebook1.AddPage(self.m_panel45, u"target type", True)
        self.m_panel17 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer202 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer202.AddGrowableCol(0)
        fgSizer202.AddGrowableRow(0)
        fgSizer202.SetFlexibleDirection(wx.BOTH)
        fgSizer202.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_splitter5 = wx.SplitterWindow(self.m_panel17, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D)
        self.m_splitter5.Bind(wx.EVT_IDLE, self.m_splitter5OnIdle)
        self.m_splitter5.SetMinimumPaneSize(20)

        self.m_panel18 = wx.Panel(self.m_splitter5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer203 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer203.AddGrowableCol(0)
        fgSizer203.AddGrowableRow(0)
        fgSizer203.SetFlexibleDirection(wx.BOTH)
        fgSizer203.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_treeTools = wx.TreeCtrl(self.m_panel18, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TR_HAS_BUTTONS | wx.TR_HIDE_ROOT | wx.TR_SINGLE)
        fgSizer203.Add(self.m_treeTools, 0, wx.EXPAND, 5)

        self.m_panel18.SetSizer(fgSizer203)
        self.m_panel18.Layout()
        fgSizer203.Fit(self.m_panel18)
        self.m_book_container = wx.Panel(self.m_splitter5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                         wx.TAB_TRAVERSAL)
        fgSizer204 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer204.AddGrowableCol(0)
        fgSizer204.AddGrowableRow(0)
        fgSizer204.SetFlexibleDirection(wx.BOTH)
        fgSizer204.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer204.SetMinSize(wx.Size(200, 100))
        self.m_book = wxx.Simplebook(self.m_book_container, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_archiver_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer205 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer205.AddGrowableCol(1)
        fgSizer205.AddGrowableRow(1)
        fgSizer205.AddGrowableRow(3)
        fgSizer205.SetFlexibleDirection(wx.BOTH)
        fgSizer205.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText94 = wx.StaticText(self.m_archiver_pane, wx.ID_ANY, u"command:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText94.Wrap(-1)
        fgSizer205.Add(self.m_staticText94, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_archiver_command = wx.TextCtrl(self.m_archiver_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer205.Add(self.m_archiver_command, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText95 = wx.StaticText(self.m_archiver_pane, wx.ID_ANY, u"options:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText95.Wrap(-1)
        fgSizer205.Add(self.m_staticText95, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.m_archiver_options = wx.TextCtrl(self.m_archiver_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, wx.TE_MULTILINE | wx.TE_READONLY)
        fgSizer205.Add(self.m_archiver_options, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText97 = wx.StaticText(self.m_archiver_pane, wx.ID_ANY, u"pattern:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText97.Wrap(-1)
        fgSizer205.Add(self.m_staticText97, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_archiver_pattern = wx.TextCtrl(self.m_archiver_pane, wx.ID_ANY,
                                              u"${COMMAND} ${FLAGS} ${OUTPUT_FLAG} ${OUTPUT_PREFIX}${OUTPUT} ${INPUTS}",
                                              wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer205.Add(self.m_archiver_pattern, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer205.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_archiver_pane.SetSizer(fgSizer205)
        self.m_archiver_pane.Layout()
        fgSizer205.Fit(self.m_archiver_pane)
        self.m_book.AddPage(self.m_archiver_pane, u"a page", False)
        self.m_archiver_flags_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              wx.TAB_TRAVERSAL)
        fgSizer206 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer206.AddGrowableCol(1)
        fgSizer206.AddGrowableRow(1)
        fgSizer206.SetFlexibleDirection(wx.BOTH)
        fgSizer206.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText971 = wx.StaticText(self.m_archiver_flags_pane, wx.ID_ANY, u"flags:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText971.Wrap(-1)
        fgSizer206.Add(self.m_staticText971, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_archiver_flags = wx.TextCtrl(self.m_archiver_flags_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer206.Add(self.m_archiver_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer206.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_archiver_flags_pane.SetSizer(fgSizer206)
        self.m_archiver_flags_pane.Layout()
        fgSizer206.Fit(self.m_archiver_flags_pane)
        self.m_book.AddPage(self.m_archiver_flags_pane, u"a page", False)
        self.m_cpp_compiler_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.TAB_TRAVERSAL)
        fgSizer2051 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer2051.AddGrowableCol(1)
        fgSizer2051.AddGrowableRow(1)
        fgSizer2051.AddGrowableRow(3)
        fgSizer2051.SetFlexibleDirection(wx.BOTH)
        fgSizer2051.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText941 = wx.StaticText(self.m_cpp_compiler_pane, wx.ID_ANY, u"command:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText941.Wrap(-1)
        fgSizer2051.Add(self.m_staticText941, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_cpp_compiler_command = wx.TextCtrl(self.m_cpp_compiler_pane, wx.ID_ANY, wx.EmptyString,
                                                  wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer2051.Add(self.m_cpp_compiler_command, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText951 = wx.StaticText(self.m_cpp_compiler_pane, wx.ID_ANY, u"options:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText951.Wrap(-1)
        fgSizer2051.Add(self.m_staticText951, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_compiler_options = wx.TextCtrl(self.m_cpp_compiler_pane, wx.ID_ANY, wx.EmptyString,
                                                  wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE | wx.TE_READONLY)
        fgSizer2051.Add(self.m_cpp_compiler_options, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText972 = wx.StaticText(self.m_cpp_compiler_pane, wx.ID_ANY, u"pattern:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText972.Wrap(-1)
        fgSizer2051.Add(self.m_staticText972, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_compiler_pattern = wx.TextCtrl(self.m_cpp_compiler_pane, wx.ID_ANY,
                                                  u"${COMMAND} ${FLAGS} ${OUTPUT_FLAG} ${OUTPUT_PREFIX}${OUTPUT} ${INPUTS}",
                                                  wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer2051.Add(self.m_cpp_compiler_pattern, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer2051.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_cpp_compiler_pane.SetSizer(fgSizer2051)
        self.m_cpp_compiler_pane.Layout()
        fgSizer2051.Fit(self.m_cpp_compiler_pane)
        self.m_book.AddPage(self.m_cpp_compiler_pane, u"a page", False)
        self.m_cpp_version_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer210 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer210.AddGrowableCol(1)
        fgSizer210.AddGrowableRow(2)
        fgSizer210.SetFlexibleDirection(wx.BOTH)
        fgSizer210.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText104 = wx.StaticText(self.m_cpp_version_pane, wx.ID_ANY, u"language flawor:",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText104.Wrap(-1)
        fgSizer210.Add(self.m_staticText104, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_cpp_compiler_versionChoices = [wx.EmptyString, u"ISO C++ 98 (-std=c++98)", u"ISO C++ 03 (-std=c++03)",
                                         u"ISO C++ 11 (-std=c++0x)", u"ISO C++ 14 (-std=c++0y)",
                                         u"ISO C++ 17 (-std=c++1z)", u"ISO C++ 20 (-std=c++2a)"]
        self.m_cpp_compiler_version = wx.Choice(self.m_cpp_version_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                m_cpp_compiler_versionChoices, 0)
        self.m_cpp_compiler_version.SetSelection(0)
        self.m_cpp_compiler_version.Refresh()
        fgSizer210.Add(self.m_cpp_compiler_version, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText105 = wx.StaticText(self.m_cpp_version_pane, wx.ID_ANY, u"extra flawor flags:",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText105.Wrap(-1)
        fgSizer210.Add(self.m_staticText105, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_compiler_version_flags = wx.TextCtrl(self.m_cpp_version_pane, wx.ID_ANY, wx.EmptyString,
                                                        wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer210.Add(self.m_cpp_compiler_version_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer210.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_cpp_version_pane.SetSizer(fgSizer210)
        self.m_cpp_version_pane.Layout()
        fgSizer210.Fit(self.m_cpp_version_pane)
        self.m_book.AddPage(self.m_cpp_version_pane, u"a page", False)
        self.m_cpp_preprocessor_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                wx.TAB_TRAVERSAL)
        fgSizer211 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer211.AddGrowableCol(0)
        fgSizer211.AddGrowableRow(1)
        fgSizer211.AddGrowableRow(2)
        fgSizer211.SetFlexibleDirection(wx.BOTH)
        fgSizer211.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer199 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer199.AddGrowableCol(0)
        fgSizer199.AddGrowableCol(1)
        fgSizer199.SetFlexibleDirection(wx.BOTH)
        fgSizer199.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_cpp_compiler_exclude_std_includes = wx.CheckBox(self.m_cpp_preprocessor_pane, wx.ID_ANY,
                                                               u"exclude standard includes", wx.DefaultPosition,
                                                               wx.DefaultSize, 0)
        fgSizer199.Add(self.m_cpp_compiler_exclude_std_includes, 0, wx.ALL, 5)

        self.m_cpp_compiler_preprocess_only = wx.CheckBox(self.m_cpp_preprocessor_pane, wx.ID_ANY, u"preprocess only",
                                                          wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer199.Add(self.m_cpp_compiler_preprocess_only, 0, wx.ALL, 5)

        fgSizer211.Add(fgSizer199, 1, wx.EXPAND, 5)

        sbSizer59 = wx.StaticBoxSizer(wx.StaticBox(self.m_cpp_preprocessor_pane, wx.ID_ANY, u"define symbols"),
                                      wx.VERTICAL)

        fgSizer212 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer212.AddGrowableCol(0)
        fgSizer212.AddGrowableRow(0)
        fgSizer212.SetFlexibleDirection(wx.BOTH)
        fgSizer212.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_cpp_compiler_defined_symbolsChoices = []
        self.m_cpp_compiler_defined_symbols = wx.ListBox(sbSizer59.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                         wx.Size(-1, -1), m_cpp_compiler_defined_symbolsChoices, 0)
        self.m_cpp_compiler_defined_symbols.SetToolTip(u"double click over item for editing ")

        fgSizer212.Add(self.m_cpp_compiler_defined_symbols, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer216 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer216.AddGrowableRow(4)
        fgSizer216.SetFlexibleDirection(wx.BOTH)
        fgSizer216.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_add_cpp_preprocessor_defined_symbol = wx.BitmapButton(sbSizer59.GetStaticBox(), wx.ID_ANY,
                                                                     wx.ArtProvider.GetBitmap(wx.ART_NEW,
                                                                                              wx.ART_BUTTON),
                                                                     wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer216.Add(self.m_add_cpp_preprocessor_defined_symbol, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_cpp_preprocessor_defined_symbol = wx.BitmapButton(sbSizer59.GetStaticBox(), wx.ID_ANY,
                                                                            wx.ArtProvider.GetBitmap(wx.ART_DELETE,
                                                                                                     wx.ART_BUTTON),
                                                                            wx.DefaultPosition, wx.DefaultSize,
                                                                            wx.BU_AUTODRAW)
        self.m_btn_delete_cpp_preprocessor_defined_symbol.Enable(False)

        fgSizer216.Add(self.m_btn_delete_cpp_preprocessor_defined_symbol, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_cpp_preprocessor_defined_symbol = wx.BitmapButton(sbSizer59.GetStaticBox(), wx.ID_ANY,
                                                                             wx.ArtProvider.GetBitmap(wx.ART_GO_UP,
                                                                                                      wx.ART_BUTTON),
                                                                             wx.DefaultPosition, wx.DefaultSize,
                                                                             wx.BU_AUTODRAW)
        self.m_btn_move_up_cpp_preprocessor_defined_symbol.Enable(False)

        fgSizer216.Add(self.m_btn_move_up_cpp_preprocessor_defined_symbol, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_cpp_preprocessor_defined_symbol = wx.BitmapButton(sbSizer59.GetStaticBox(), wx.ID_ANY,
                                                                               wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN,
                                                                                                        wx.ART_BUTTON),
                                                                               wx.DefaultPosition, wx.DefaultSize,
                                                                               wx.BU_AUTODRAW)
        self.m_btn_move_down_cpp_preprocessor_defined_symbol.Enable(False)

        fgSizer216.Add(self.m_btn_move_down_cpp_preprocessor_defined_symbol, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer212.Add(fgSizer216, 1, wx.EXPAND, 5)

        sbSizer59.Add(fgSizer212, 1, wx.EXPAND, 5)

        fgSizer211.Add(sbSizer59, 1, wx.EXPAND, 5)

        sbSizer60 = wx.StaticBoxSizer(wx.StaticBox(self.m_cpp_preprocessor_pane, wx.ID_ANY, u"undefine symbols"),
                                      wx.VERTICAL)

        fgSizer2121 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2121.AddGrowableCol(0)
        fgSizer2121.AddGrowableRow(0)
        fgSizer2121.SetFlexibleDirection(wx.BOTH)
        fgSizer2121.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_cpp_compiler_undefined_symbolsChoices = []
        self.m_cpp_compiler_undefined_symbols = wx.ListBox(sbSizer60.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                           wx.DefaultSize, m_cpp_compiler_undefined_symbolsChoices, 0)
        self.m_cpp_compiler_undefined_symbols.SetToolTip(u"double click over item for editing ")

        fgSizer2121.Add(self.m_cpp_compiler_undefined_symbols, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer2161 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer2161.AddGrowableRow(4)
        fgSizer2161.SetFlexibleDirection(wx.BOTH)
        fgSizer2161.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_btn_add_cpp_preprocessor_undefined_symbol = wx.BitmapButton(sbSizer60.GetStaticBox(), wx.ID_ANY,
                                                                           wx.ArtProvider.GetBitmap(wx.ART_NEW,
                                                                                                    wx.ART_BUTTON),
                                                                           wx.DefaultPosition, wx.DefaultSize,
                                                                           wx.BU_AUTODRAW)
        fgSizer2161.Add(self.m_btn_add_cpp_preprocessor_undefined_symbol, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_cpp_preprocessor_undefined_symbol = wx.BitmapButton(sbSizer60.GetStaticBox(), wx.ID_ANY,
                                                                              wx.ArtProvider.GetBitmap(wx.ART_DELETE,
                                                                                                       wx.ART_BUTTON),
                                                                              wx.DefaultPosition, wx.DefaultSize,
                                                                              wx.BU_AUTODRAW)
        self.m_btn_delete_cpp_preprocessor_undefined_symbol.Enable(False)

        fgSizer2161.Add(self.m_btn_delete_cpp_preprocessor_undefined_symbol, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_cpp_preprocessor_undefined_symbol = wx.BitmapButton(sbSizer60.GetStaticBox(), wx.ID_ANY,
                                                                               wx.ArtProvider.GetBitmap(wx.ART_GO_UP,
                                                                                                        wx.ART_BUTTON),
                                                                               wx.DefaultPosition, wx.DefaultSize,
                                                                               wx.BU_AUTODRAW)
        self.m_btn_move_up_cpp_preprocessor_undefined_symbol.Enable(False)

        fgSizer2161.Add(self.m_btn_move_up_cpp_preprocessor_undefined_symbol, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_cpp_preprocessor_undefined_symbol = wx.BitmapButton(sbSizer60.GetStaticBox(), wx.ID_ANY,
                                                                                 wx.ArtProvider.GetBitmap(
                                                                                     wx.ART_GO_DOWN, wx.ART_BUTTON),
                                                                                 wx.DefaultPosition, wx.DefaultSize,
                                                                                 wx.BU_AUTODRAW)
        self.m_btn_move_down_cpp_preprocessor_undefined_symbol.Enable(False)

        fgSizer2161.Add(self.m_btn_move_down_cpp_preprocessor_undefined_symbol, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer2121.Add(fgSizer2161, 1, wx.EXPAND, 5)

        sbSizer60.Add(fgSizer2121, 1, wx.EXPAND, 5)

        fgSizer211.Add(sbSizer60, 1, wx.EXPAND, 5)

        self.m_cpp_preprocessor_pane.SetSizer(fgSizer211)
        self.m_cpp_preprocessor_pane.Layout()
        fgSizer211.Fit(self.m_cpp_preprocessor_pane)
        self.m_book.AddPage(self.m_cpp_preprocessor_pane, u"a page", False)
        self.m_cpp_includes_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.TAB_TRAVERSAL)
        fgSizer214 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer214.AddGrowableCol(0)
        fgSizer214.AddGrowableRow(0)
        fgSizer214.AddGrowableRow(1)
        fgSizer214.SetFlexibleDirection(wx.BOTH)
        fgSizer214.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer61 = wx.StaticBoxSizer(wx.StaticBox(self.m_cpp_includes_pane, wx.ID_ANY, u"paths"), wx.VERTICAL)

        fgSizer2122 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2122.AddGrowableCol(0)
        fgSizer2122.AddGrowableRow(0)
        fgSizer2122.SetFlexibleDirection(wx.BOTH)
        fgSizer2122.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_cpp_compiler_include_pathsChoices = []
        self.m_cpp_compiler_include_paths = wx.ListBox(sbSizer61.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                       wx.DefaultSize, m_cpp_compiler_include_pathsChoices, 0)
        self.m_cpp_compiler_include_paths.SetToolTip(u"double click over item for editing ")

        fgSizer2122.Add(self.m_cpp_compiler_include_paths, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer2162 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer2162.AddGrowableRow(4)
        fgSizer2162.SetFlexibleDirection(wx.VERTICAL)
        fgSizer2162.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_NONE)

        self.m_bpButton532 = wx.BitmapButton(sbSizer61.GetStaticBox(), wx.ID_ANY,
                                             wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                             wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2162.Add(self.m_bpButton532, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_cpp_include_path = wx.BitmapButton(sbSizer61.GetStaticBox(), wx.ID_ANY,
                                                             wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                             wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_cpp_include_path.Enable(False)

        fgSizer2162.Add(self.m_btn_delete_cpp_include_path, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_include_path = wx.BitmapButton(sbSizer61.GetStaticBox(), wx.ID_ANY,
                                                          wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                          wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_include_path.Enable(False)

        fgSizer2162.Add(self.m_btn_move_up_include_path, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_include_path = wx.BitmapButton(sbSizer61.GetStaticBox(), wx.ID_ANY,
                                                            wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_include_path.Enable(False)

        fgSizer2162.Add(self.m_btn_move_down_include_path, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer2122.Add(fgSizer2162, 0, wx.EXPAND, 5)

        sbSizer61.Add(fgSizer2122, 1, wx.EXPAND, 5)

        fgSizer214.Add(sbSizer61, 1, wx.EXPAND, 5)

        sbSizer63 = wx.StaticBoxSizer(wx.StaticBox(self.m_cpp_includes_pane, wx.ID_ANY, u"includes"), wx.VERTICAL)

        fgSizer2123 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2123.AddGrowableCol(0)
        fgSizer2123.AddGrowableRow(0)
        fgSizer2123.SetFlexibleDirection(wx.BOTH)
        fgSizer2123.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_cpp_compiler_includesChoices = []
        self.m_cpp_compiler_includes = wx.ListBox(sbSizer63.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                  wx.DefaultSize, m_cpp_compiler_includesChoices, 0)
        self.m_cpp_compiler_includes.SetToolTip(u"double click over item for editing ")

        fgSizer2123.Add(self.m_cpp_compiler_includes, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer2163 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer2163.AddGrowableRow(4)
        fgSizer2163.SetFlexibleDirection(wx.VERTICAL)
        fgSizer2163.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_NONE)

        self.m_bpButton533 = wx.BitmapButton(sbSizer63.GetStaticBox(), wx.ID_ANY,
                                             wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                             wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2163.Add(self.m_bpButton533, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_cpp_include = wx.BitmapButton(sbSizer63.GetStaticBox(), wx.ID_ANY,
                                                        wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                        wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_cpp_include.Enable(False)

        fgSizer2163.Add(self.m_btn_delete_cpp_include, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_cpp_include = wx.BitmapButton(sbSizer63.GetStaticBox(), wx.ID_ANY,
                                                         wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                         wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_cpp_include.Enable(False)

        fgSizer2163.Add(self.m_btn_move_up_cpp_include, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_cpp_include = wx.BitmapButton(sbSizer63.GetStaticBox(), wx.ID_ANY,
                                                           wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                                           wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_cpp_include.Enable(False)

        fgSizer2163.Add(self.m_btn_move_down_cpp_include, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer2123.Add(fgSizer2163, 0, wx.EXPAND, 5)

        sbSizer63.Add(fgSizer2123, 1, wx.EXPAND, 5)

        fgSizer214.Add(sbSizer63, 1, wx.EXPAND, 5)

        self.m_cpp_includes_pane.SetSizer(fgSizer214)
        self.m_cpp_includes_pane.Layout()
        fgSizer214.Fit(self.m_cpp_includes_pane)
        self.m_book.AddPage(self.m_cpp_includes_pane, u"a page", False)
        self.m_cpp_optimization_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                wx.TAB_TRAVERSAL)
        fgSizer223 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer223.AddGrowableCol(1)
        fgSizer223.AddGrowableRow(2)
        fgSizer223.SetFlexibleDirection(wx.BOTH)
        fgSizer223.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText103 = wx.StaticText(self.m_cpp_optimization_pane, wx.ID_ANY, u"level:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText103.Wrap(-1)
        fgSizer223.Add(self.m_staticText103, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_cpp_compiler_levelChoices = [u"none", u"basic", u"more", u"expensive", u"size"]
        self.m_cpp_compiler_level = wx.Choice(self.m_cpp_optimization_pane, wx.ID_ANY, wx.DefaultPosition,
                                              wx.DefaultSize, m_cpp_compiler_levelChoices, 0)
        self.m_cpp_compiler_level.SetSelection(0)
        self.m_cpp_compiler_level.Refresh()
        fgSizer223.Add(self.m_cpp_compiler_level, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText1041 = wx.StaticText(self.m_cpp_optimization_pane, wx.ID_ANY, u"other flags:",
                                              wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1041.Wrap(-1)
        fgSizer223.Add(self.m_staticText1041, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_cpp_compiler_level_flags = wx.TextCtrl(self.m_cpp_optimization_pane, wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer223.Add(self.m_cpp_compiler_level_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer223.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_cpp_optimization_pane.SetSizer(fgSizer223)
        self.m_cpp_optimization_pane.Layout()
        fgSizer223.Fit(self.m_cpp_optimization_pane)
        self.m_book.AddPage(self.m_cpp_optimization_pane, u"a page", False)
        self.m_cpp_debug_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer224 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer224.AddGrowableCol(0)
        fgSizer224.AddGrowableRow(2)
        fgSizer224.SetFlexibleDirection(wx.BOTH)
        fgSizer224.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer225 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer225.AddGrowableCol(1)
        fgSizer225.SetFlexibleDirection(wx.BOTH)
        fgSizer225.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1051 = wx.StaticText(self.m_cpp_debug_pane, wx.ID_ANY, u"level:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText1051.Wrap(-1)
        fgSizer225.Add(self.m_staticText1051, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_cpp_compiler_debug_levelChoices = [u"none", u"minimal", u"default", u"maximum"]
        self.m_cpp_compiler_debug_level = wx.Choice(self.m_cpp_debug_pane, wx.ID_ANY, wx.DefaultPosition,
                                                    wx.DefaultSize, m_cpp_compiler_debug_levelChoices, 0)
        self.m_cpp_compiler_debug_level.SetSelection(0)
        self.m_cpp_compiler_debug_level.Refresh()
        fgSizer225.Add(self.m_cpp_compiler_debug_level, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText107 = wx.StaticText(self.m_cpp_debug_pane, wx.ID_ANY, u"other flags:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText107.Wrap(-1)
        fgSizer225.Add(self.m_staticText107, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_compiler_debug_flags = wx.TextCtrl(self.m_cpp_debug_pane, wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer225.Add(self.m_cpp_compiler_debug_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer224.Add(fgSizer225, 1, wx.EXPAND, 5)

        fgSizer226 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer226.AddGrowableCol(0)
        fgSizer226.SetFlexibleDirection(wx.BOTH)
        fgSizer226.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_cpp_compiler_debug_prof = wx.CheckBox(self.m_cpp_debug_pane, wx.ID_ANY, u"prof information",
                                                     wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer226.Add(self.m_cpp_compiler_debug_prof, 0, wx.ALL | wx.EXPAND, 5)

        self.m_cpp_compiler_debug_gprof = wx.CheckBox(self.m_cpp_debug_pane, wx.ID_ANY, u"gprof information",
                                                      wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer226.Add(self.m_cpp_compiler_debug_gprof, 0, wx.ALL, 5)

        self.m_cpp_compiler_debug_gcov = wx.CheckBox(self.m_cpp_debug_pane, wx.ID_ANY, u"gcov information",
                                                     wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer226.Add(self.m_cpp_compiler_debug_gcov, 0, wx.ALL, 5)

        fgSizer224.Add(fgSizer226, 1, wx.EXPAND, 5)

        fgSizer224.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_cpp_debug_pane.SetSizer(fgSizer224)
        self.m_cpp_debug_pane.Layout()
        fgSizer224.Fit(self.m_cpp_debug_pane)
        self.m_book.AddPage(self.m_cpp_debug_pane, u"a page", False)
        self.m_cpp_warnings_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.TAB_TRAVERSAL)
        fgSizer227 = wx.FlexGridSizer(9, 1, 0, 0)
        fgSizer227.SetFlexibleDirection(wx.BOTH)
        fgSizer227.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_cpp_compiler_warnings_syntax_only = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY, u"syntax only",
                                                               wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_syntax_only, 0, wx.ALL, 5)

        self.m_cpp_compiler_warnings_pedantic = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY, u"pedantic",
                                                            wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_pedantic, 0, wx.ALL, 5)

        self.m_cpp_compiler_warnings_pedantic_are_errors = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY,
                                                                       u"pedantic warnings are errors",
                                                                       wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_pedantic_are_errors, 0, wx.ALL, 5)

        self.m_cpp_compiler_warnings_disable = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY, u"disable warnings",
                                                           wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_disable, 0, wx.ALL, 5)

        self.m_cpp_compiler_warnings_all = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY, u"all warnings",
                                                       wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_all, 0, wx.ALL, 5)

        self.m_cpp_compiler_warnings_extra = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY, u"extra warnings",
                                                         wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_extra, 0, wx.ALL, 5)

        self.m_cpp_compiler_warnings_are_errors = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY,
                                                              u"warnings are errors", wx.DefaultPosition,
                                                              wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_are_errors, 0, wx.ALL, 5)

        self.m_cpp_compiler_warnings_conversions = wx.CheckBox(self.m_cpp_warnings_pane, wx.ID_ANY,
                                                               u"conversions warning", wx.DefaultPosition,
                                                               wx.DefaultSize, 0)
        fgSizer227.Add(self.m_cpp_compiler_warnings_conversions, 0, wx.ALL, 5)

        fgSizer227.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_cpp_warnings_pane.SetSizer(fgSizer227)
        self.m_cpp_warnings_pane.Layout()
        fgSizer227.Fit(self.m_cpp_warnings_pane)
        self.m_book.AddPage(self.m_cpp_warnings_pane, u"a page", False)
        self.m_cpp_other_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer228 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer228.AddGrowableCol(0)
        fgSizer228.SetFlexibleDirection(wx.BOTH)
        fgSizer228.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer229 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer229.AddGrowableCol(1)
        fgSizer229.SetFlexibleDirection(wx.BOTH)
        fgSizer229.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText108 = wx.StaticText(self.m_cpp_other_pane, wx.ID_ANY, u"other flags:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText108.Wrap(-1)
        fgSizer229.Add(self.m_staticText108, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_cpp_compiler_other_flags = wx.TextCtrl(self.m_cpp_other_pane, wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer229.Add(self.m_cpp_compiler_other_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer228.Add(fgSizer229, 1, wx.EXPAND, 5)

        fgSizer230 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer230.SetFlexibleDirection(wx.BOTH)
        fgSizer230.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_cpp_compiler_verbose = wx.CheckBox(self.m_cpp_other_pane, wx.ID_ANY, u"verbose", wx.DefaultPosition,
                                                  wx.DefaultSize, 0)
        fgSizer230.Add(self.m_cpp_compiler_verbose, 0, wx.ALL | wx.EXPAND, 5)

        self.m_cpp_compiler_position_independent_code = wx.CheckBox(self.m_cpp_other_pane, wx.ID_ANY,
                                                                    u"position independent code", wx.DefaultPosition,
                                                                    wx.DefaultSize, 0)
        fgSizer230.Add(self.m_cpp_compiler_position_independent_code, 0, wx.ALL | wx.EXPAND, 5)

        self.m_cpp_compiler_pthread_support = wx.CheckBox(self.m_cpp_other_pane, wx.ID_ANY, u"pthread support",
                                                          wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer230.Add(self.m_cpp_compiler_pthread_support, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer228.Add(fgSizer230, 1, wx.EXPAND, 5)

        fgSizer228.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_cpp_other_pane.SetSizer(fgSizer228)
        self.m_cpp_other_pane.Layout()
        fgSizer228.Fit(self.m_cpp_other_pane)
        self.m_book.AddPage(self.m_cpp_other_pane, u"a page", False)
        self.m_c_compiler_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_c_compiler_pane.Enable(False)
        self.m_c_compiler_pane.Hide()

        fgSizer20511 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer20511.AddGrowableCol(1)
        fgSizer20511.AddGrowableRow(1)
        fgSizer20511.AddGrowableRow(3)
        fgSizer20511.SetFlexibleDirection(wx.BOTH)
        fgSizer20511.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText9411 = wx.StaticText(self.m_c_compiler_pane, wx.ID_ANY, u"command:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9411.Wrap(-1)
        fgSizer20511.Add(self.m_staticText9411, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_c_compiler_command = wx.TextCtrl(self.m_c_compiler_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer20511.Add(self.m_c_compiler_command, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText9511 = wx.StaticText(self.m_c_compiler_pane, wx.ID_ANY, u"options:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9511.Wrap(-1)
        fgSizer20511.Add(self.m_staticText9511, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.m_c_compiler_options = wx.TextCtrl(self.m_c_compiler_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, wx.TE_MULTILINE | wx.TE_READONLY)
        fgSizer20511.Add(self.m_c_compiler_options, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText9721 = wx.StaticText(self.m_c_compiler_pane, wx.ID_ANY, u"pattern:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9721.Wrap(-1)
        fgSizer20511.Add(self.m_staticText9721, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_c_compiler_pattern = wx.TextCtrl(self.m_c_compiler_pane, wx.ID_ANY,
                                                u"${COMMAND} ${FLAGS} ${OUTPUT_FLAG} ${OUTPUT_PREFIX}${OUTPUT} ${INPUTS}",
                                                wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer20511.Add(self.m_c_compiler_pattern, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer20511.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer20511.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_c_compiler_pane.SetSizer(fgSizer20511)
        self.m_c_compiler_pane.Layout()
        fgSizer20511.Fit(self.m_c_compiler_pane)
        self.m_book.AddPage(self.m_c_compiler_pane, u"a page", False)
        self.m_c_version_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_c_version_pane.Enable(False)
        self.m_c_version_pane.Hide()

        fgSizer2101 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer2101.AddGrowableCol(1)
        fgSizer2101.AddGrowableRow(2)
        fgSizer2101.SetFlexibleDirection(wx.BOTH)
        fgSizer2101.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1042 = wx.StaticText(self.m_c_version_pane, wx.ID_ANY, u"language flawor:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText1042.Wrap(-1)
        fgSizer2101.Add(self.m_staticText1042, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_c_compiler_versionChoices = [wx.EmptyString, u"ISO C 90", u"ISO C 99", u"ISO C 11"]
        self.m_c_compiler_version = wx.Choice(self.m_c_version_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              m_c_compiler_versionChoices, 0)
        self.m_c_compiler_version.SetSelection(0)
        self.m_c_compiler_version.Refresh()
        fgSizer2101.Add(self.m_c_compiler_version, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText1052 = wx.StaticText(self.m_c_version_pane, wx.ID_ANY, u"extra flawor flags:",
                                              wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1052.Wrap(-1)
        fgSizer2101.Add(self.m_staticText1052, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_c_compiler_version_flags = wx.TextCtrl(self.m_c_version_pane, wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer2101.Add(self.m_c_compiler_version_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer2101.Add((10, 10), 1, wx.EXPAND, 5)

        self.m_c_version_pane.SetSizer(fgSizer2101)
        self.m_c_version_pane.Layout()
        fgSizer2101.Fit(self.m_c_version_pane)
        self.m_book.AddPage(self.m_c_version_pane, u"a page", False)
        self.m_c_preprocessor_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              wx.TAB_TRAVERSAL)
        self.m_c_preprocessor_pane.Enable(False)
        self.m_c_preprocessor_pane.Hide()

        fgSizer2111 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer2111.AddGrowableCol(0)
        fgSizer2111.AddGrowableRow(1)
        fgSizer2111.AddGrowableRow(2)
        fgSizer2111.SetFlexibleDirection(wx.BOTH)
        fgSizer2111.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer2001 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2001.SetFlexibleDirection(wx.BOTH)
        fgSizer2001.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_c_compiler_exclude_std_includes = wx.CheckBox(self.m_c_preprocessor_pane, wx.ID_ANY,
                                                             u"exclude standard includes", wx.DefaultPosition,
                                                             wx.DefaultSize, 0)
        fgSizer2001.Add(self.m_c_compiler_exclude_std_includes, 0, wx.ALL, 5)

        self.m_c_compiler_preprocess_only = wx.CheckBox(self.m_c_preprocessor_pane, wx.ID_ANY, u"preprocess only",
                                                        wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2001.Add(self.m_c_compiler_preprocess_only, 0, wx.ALL, 5)

        fgSizer2111.Add(fgSizer2001, 1, wx.EXPAND, 5)

        sbSizer591 = wx.StaticBoxSizer(wx.StaticBox(self.m_c_preprocessor_pane, wx.ID_ANY, u"define symbols"),
                                       wx.VERTICAL)

        fgSizer2124 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2124.AddGrowableCol(0)
        fgSizer2124.AddGrowableRow(0)
        fgSizer2124.SetFlexibleDirection(wx.BOTH)
        fgSizer2124.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_c_compiler_defined_symbolsChoices = []
        self.m_c_compiler_defined_symbols = wx.ListBox(sbSizer591.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                       wx.DefaultSize, m_c_compiler_defined_symbolsChoices, 0)
        fgSizer2124.Add(self.m_c_compiler_defined_symbols, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer2164 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer2164.AddGrowableRow(4)
        fgSizer2164.SetFlexibleDirection(wx.BOTH)
        fgSizer2164.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton534 = wx.BitmapButton(sbSizer591.GetStaticBox(), wx.ID_ANY,
                                             wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                             wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2164.Add(self.m_bpButton534, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton544 = wx.BitmapButton(sbSizer591.GetStaticBox(), wx.ID_ANY,
                                             wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON), wx.DefaultPosition,
                                             wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2164.Add(self.m_bpButton544, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton554 = wx.BitmapButton(sbSizer591.GetStaticBox(), wx.ID_ANY,
                                             wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON), wx.DefaultPosition,
                                             wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2164.Add(self.m_bpButton554, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton564 = wx.BitmapButton(sbSizer591.GetStaticBox(), wx.ID_ANY,
                                             wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                             wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2164.Add(self.m_bpButton564, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer2124.Add(fgSizer2164, 1, wx.EXPAND, 5)

        sbSizer591.Add(fgSizer2124, 1, wx.EXPAND, 5)

        fgSizer2111.Add(sbSizer591, 1, wx.EXPAND, 5)

        sbSizer601 = wx.StaticBoxSizer(wx.StaticBox(self.m_c_preprocessor_pane, wx.ID_ANY, u"undefine symbols"),
                                       wx.VERTICAL)

        fgSizer21211 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21211.AddGrowableCol(0)
        fgSizer21211.AddGrowableRow(0)
        fgSizer21211.SetFlexibleDirection(wx.BOTH)
        fgSizer21211.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_c_compiler_undefined_symbolsChoices = []
        self.m_c_compiler_undefined_symbols = wx.ListBox(sbSizer601.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                         wx.DefaultSize, m_c_compiler_undefined_symbolsChoices, 0)
        fgSizer21211.Add(self.m_c_compiler_undefined_symbols, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer21611 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21611.AddGrowableRow(4)
        fgSizer21611.SetFlexibleDirection(wx.BOTH)
        fgSizer21611.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5311 = wx.BitmapButton(sbSizer601.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21611.Add(self.m_bpButton5311, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5411 = wx.BitmapButton(sbSizer601.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21611.Add(self.m_bpButton5411, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5511 = wx.BitmapButton(sbSizer601.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21611.Add(self.m_bpButton5511, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5611 = wx.BitmapButton(sbSizer601.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21611.Add(self.m_bpButton5611, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21211.Add(fgSizer21611, 1, wx.EXPAND, 5)

        sbSizer601.Add(fgSizer21211, 1, wx.EXPAND, 5)

        fgSizer2111.Add(sbSizer601, 1, wx.EXPAND, 5)

        self.m_c_preprocessor_pane.SetSizer(fgSizer2111)
        self.m_c_preprocessor_pane.Layout()
        fgSizer2111.Fit(self.m_c_preprocessor_pane)
        self.m_book.AddPage(self.m_c_preprocessor_pane, u"a page", False)
        self.m_c_includes_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_c_includes_pane.Enable(False)
        self.m_c_includes_pane.Hide()

        fgSizer2141 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer2141.AddGrowableCol(0)
        fgSizer2141.AddGrowableRow(0)
        fgSizer2141.AddGrowableRow(1)
        fgSizer2141.SetFlexibleDirection(wx.BOTH)
        fgSizer2141.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer611 = wx.StaticBoxSizer(wx.StaticBox(self.m_c_includes_pane, wx.ID_ANY, u"paths"), wx.VERTICAL)

        fgSizer21221 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21221.AddGrowableCol(0)
        fgSizer21221.AddGrowableRow(0)
        fgSizer21221.SetFlexibleDirection(wx.BOTH)
        fgSizer21221.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_c_compiler_include_pathsChoices = []
        self.m_c_compiler_include_paths = wx.ListBox(sbSizer611.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                     wx.DefaultSize, m_c_compiler_include_pathsChoices, 0)
        fgSizer21221.Add(self.m_c_compiler_include_paths, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer21621 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21621.AddGrowableRow(4)
        fgSizer21621.SetFlexibleDirection(wx.BOTH)
        fgSizer21621.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5321 = wx.BitmapButton(sbSizer611.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21621.Add(self.m_bpButton5321, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5421 = wx.BitmapButton(sbSizer611.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21621.Add(self.m_bpButton5421, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5521 = wx.BitmapButton(sbSizer611.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21621.Add(self.m_bpButton5521, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5621 = wx.BitmapButton(sbSizer611.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21621.Add(self.m_bpButton5621, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21221.Add(fgSizer21621, 1, wx.EXPAND, 5)

        sbSizer611.Add(fgSizer21221, 1, wx.EXPAND, 5)

        fgSizer2141.Add(sbSizer611, 1, wx.EXPAND, 5)

        sbSizer631 = wx.StaticBoxSizer(wx.StaticBox(self.m_c_includes_pane, wx.ID_ANY, u"includes"), wx.VERTICAL)

        fgSizer21231 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21231.AddGrowableCol(0)
        fgSizer21231.AddGrowableRow(0)
        fgSizer21231.SetFlexibleDirection(wx.BOTH)
        fgSizer21231.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_c_compiler_includesChoices = []
        self.m_c_compiler_includes = wx.ListBox(sbSizer631.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                wx.DefaultSize, m_c_compiler_includesChoices, 0)
        fgSizer21231.Add(self.m_c_compiler_includes, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer21631 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21631.AddGrowableRow(4)
        fgSizer21631.SetFlexibleDirection(wx.BOTH)
        fgSizer21631.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5331 = wx.BitmapButton(sbSizer631.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21631.Add(self.m_bpButton5331, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5431 = wx.BitmapButton(sbSizer631.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21631.Add(self.m_bpButton5431, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5531 = wx.BitmapButton(sbSizer631.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21631.Add(self.m_bpButton5531, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5631 = wx.BitmapButton(sbSizer631.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21631.Add(self.m_bpButton5631, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21231.Add(fgSizer21631, 1, wx.EXPAND, 5)

        sbSizer631.Add(fgSizer21231, 1, wx.EXPAND, 5)

        fgSizer2141.Add(sbSizer631, 1, wx.EXPAND, 5)

        self.m_c_includes_pane.SetSizer(fgSizer2141)
        self.m_c_includes_pane.Layout()
        fgSizer2141.Fit(self.m_c_includes_pane)
        self.m_book.AddPage(self.m_c_includes_pane, u"a page", False)
        self.m_c_optimization_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              wx.TAB_TRAVERSAL)
        self.m_c_optimization_pane.Enable(False)
        self.m_c_optimization_pane.Hide()

        fgSizer2231 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer2231.AddGrowableCol(1)
        fgSizer2231.AddGrowableRow(2)
        fgSizer2231.SetFlexibleDirection(wx.BOTH)
        fgSizer2231.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1031 = wx.StaticText(self.m_c_optimization_pane, wx.ID_ANY, u"level:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText1031.Wrap(-1)
        fgSizer2231.Add(self.m_staticText1031, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_c_compiler_levelChoices = [u"none", u"level 1", u"level 2", u"level 3", u"for size", u"expensive", u"more",
                                     u"basic", u"size"]
        self.m_c_compiler_level = wx.Choice(self.m_c_optimization_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            m_c_compiler_levelChoices, 0)
        self.m_c_compiler_level.SetSelection(0)
        self.m_c_compiler_level.Refresh()
        fgSizer2231.Add(self.m_c_compiler_level, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText10411 = wx.StaticText(self.m_c_optimization_pane, wx.ID_ANY, u"other flags:",
                                               wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText10411.Wrap(-1)
        fgSizer2231.Add(self.m_staticText10411, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_c_compiler_level_flags = wx.TextCtrl(self.m_c_optimization_pane, wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer2231.Add(self.m_c_compiler_level_flags, 0, wx.ALL | wx.EXPAND, 5)

        self.m_c_optimization_pane.SetSizer(fgSizer2231)
        self.m_c_optimization_pane.Layout()
        fgSizer2231.Fit(self.m_c_optimization_pane)
        self.m_book.AddPage(self.m_c_optimization_pane, u"a page", False)
        self.m_c_debug_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_c_debug_pane.Enable(False)
        self.m_c_debug_pane.Hide()

        fgSizer2241 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer2241.AddGrowableCol(0)
        fgSizer2241.AddGrowableRow(2)
        fgSizer2241.SetFlexibleDirection(wx.BOTH)
        fgSizer2241.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer2251 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer2251.AddGrowableCol(1)
        fgSizer2251.SetFlexibleDirection(wx.BOTH)
        fgSizer2251.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText10511 = wx.StaticText(self.m_c_debug_pane, wx.ID_ANY, u"level:", wx.DefaultPosition,
                                               wx.DefaultSize, 0)
        self.m_staticText10511.Wrap(-1)
        fgSizer2251.Add(self.m_staticText10511, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_c_compiler_debug_levelChoices = [u"none", u"min", u"default", u"max"]
        self.m_c_compiler_debug_level = wx.Choice(self.m_c_debug_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                  m_c_compiler_debug_levelChoices, 0)
        self.m_c_compiler_debug_level.SetSelection(0)
        self.m_c_compiler_debug_level.Refresh()
        fgSizer2251.Add(self.m_c_compiler_debug_level, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText1071 = wx.StaticText(self.m_c_debug_pane, wx.ID_ANY, u"other flags:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText1071.Wrap(-1)
        fgSizer2251.Add(self.m_staticText1071, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_c_compiler_debug_flags = wx.TextCtrl(self.m_c_debug_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                    wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer2251.Add(self.m_c_compiler_debug_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer2241.Add(fgSizer2251, 1, wx.EXPAND, 5)

        fgSizer2261 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer2261.AddGrowableCol(0)
        fgSizer2261.SetFlexibleDirection(wx.BOTH)
        fgSizer2261.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_c_compiler_debug_prof = wx.CheckBox(self.m_c_debug_pane, wx.ID_ANY, u"prof information",
                                                   wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2261.Add(self.m_c_compiler_debug_prof, 0, wx.ALL | wx.EXPAND, 5)

        self.m_c_compiler_debug_gprof = wx.CheckBox(self.m_c_debug_pane, wx.ID_ANY, u"gprof information",
                                                    wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2261.Add(self.m_c_compiler_debug_gprof, 0, wx.ALL, 5)

        self.m_c_compiler_debug_gcov = wx.CheckBox(self.m_c_debug_pane, wx.ID_ANY, u"gcov information",
                                                   wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2261.Add(self.m_c_compiler_debug_gcov, 0, wx.ALL, 5)

        fgSizer2241.Add(fgSizer2261, 1, wx.EXPAND, 5)

        self.m_c_debug_pane.SetSizer(fgSizer2241)
        self.m_c_debug_pane.Layout()
        fgSizer2241.Fit(self.m_c_debug_pane)
        self.m_book.AddPage(self.m_c_debug_pane, u"a page", False)
        self.m_c_warnings_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_c_warnings_pane.Enable(False)
        self.m_c_warnings_pane.Hide()

        fgSizer2271 = wx.FlexGridSizer(9, 1, 0, 0)
        fgSizer2271.SetFlexibleDirection(wx.BOTH)
        fgSizer2271.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_c_compiler_warning_syntax_only = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY, u"syntax only",
                                                            wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_syntax_only, 0, wx.ALL, 5)

        self.m_c_compiler_warning_pedantic = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY, u"pedantic",
                                                         wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_pedantic, 0, wx.ALL, 5)

        self.m_c_compiler_warning_pedantic_are_errors = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY,
                                                                    u"pedantic warnings are errors", wx.DefaultPosition,
                                                                    wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_pedantic_are_errors, 0, wx.ALL, 5)

        self.m_c_compiler_warning_disable = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY, u"disable warnings",
                                                        wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_disable, 0, wx.ALL, 5)

        self.m_c_compiler_warning_all = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY, u"all warnings",
                                                    wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_all, 0, wx.ALL, 5)

        self.m_c_compiler_warning_extra = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY, u"extra warnings",
                                                      wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_extra, 0, wx.ALL, 5)

        self.m_c_compiler_warning_are_errors = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY, u"warnings are errors",
                                                           wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_are_errors, 0, wx.ALL, 5)

        self.m_c_compiler_warning_conversions = wx.CheckBox(self.m_c_warnings_pane, wx.ID_ANY, u"conversion warning",
                                                            wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2271.Add(self.m_c_compiler_warning_conversions, 0, wx.ALL, 5)

        self.m_c_warnings_pane.SetSizer(fgSizer2271)
        self.m_c_warnings_pane.Layout()
        fgSizer2271.Fit(self.m_c_warnings_pane)
        self.m_book.AddPage(self.m_c_warnings_pane, u"a page", False)
        self.m_c_other_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_c_other_pane.Enable(False)
        self.m_c_other_pane.Hide()

        fgSizer2281 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer2281.AddGrowableCol(0)
        fgSizer2281.SetFlexibleDirection(wx.BOTH)
        fgSizer2281.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer2291 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2291.AddGrowableCol(1)
        fgSizer2291.SetFlexibleDirection(wx.BOTH)
        fgSizer2291.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1081 = wx.StaticText(self.m_c_other_pane, wx.ID_ANY, u"other flags:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText1081.Wrap(-1)
        fgSizer2291.Add(self.m_staticText1081, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_c_compiler_other_flags = wx.TextCtrl(self.m_c_other_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                    wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer2291.Add(self.m_c_compiler_other_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer2281.Add(fgSizer2291, 1, wx.EXPAND, 5)

        fgSizer2301 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer2301.SetFlexibleDirection(wx.BOTH)
        fgSizer2301.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_c_compiler_verbose = wx.CheckBox(self.m_c_other_pane, wx.ID_ANY, u"verbose", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        fgSizer2301.Add(self.m_c_compiler_verbose, 0, wx.ALL | wx.EXPAND, 5)

        self.m_c_compiler_position_independent_code = wx.CheckBox(self.m_c_other_pane, wx.ID_ANY,
                                                                  u"position independent code", wx.DefaultPosition,
                                                                  wx.DefaultSize, 0)
        fgSizer2301.Add(self.m_c_compiler_position_independent_code, 0, wx.ALL | wx.EXPAND, 5)

        self.m_c_compiler_pthread_support = wx.CheckBox(self.m_c_other_pane, wx.ID_ANY, u"pthread support",
                                                        wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2301.Add(self.m_c_compiler_pthread_support, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer2281.Add(fgSizer2301, 1, wx.EXPAND, 5)

        self.m_c_other_pane.SetSizer(fgSizer2281)
        self.m_c_other_pane.Layout()
        fgSizer2281.Fit(self.m_c_other_pane)
        self.m_book.AddPage(self.m_c_other_pane, u"a page", False)
        self.m_assembler_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer20512 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer20512.AddGrowableCol(1)
        fgSizer20512.AddGrowableRow(1)
        fgSizer20512.AddGrowableRow(3)
        fgSizer20512.SetFlexibleDirection(wx.BOTH)
        fgSizer20512.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText9412 = wx.StaticText(self.m_assembler_pane, wx.ID_ANY, u"command:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9412.Wrap(-1)
        fgSizer20512.Add(self.m_staticText9412, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_assembler_command = wx.TextCtrl(self.m_assembler_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer20512.Add(self.m_assembler_command, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText9512 = wx.StaticText(self.m_assembler_pane, wx.ID_ANY, u"options:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9512.Wrap(-1)
        fgSizer20512.Add(self.m_staticText9512, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.m_assembler_options = wx.TextCtrl(self.m_assembler_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.DefaultSize, wx.TE_MULTILINE | wx.TE_READONLY)
        fgSizer20512.Add(self.m_assembler_options, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText9722 = wx.StaticText(self.m_assembler_pane, wx.ID_ANY, u"pattern:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9722.Wrap(-1)
        fgSizer20512.Add(self.m_staticText9722, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_assembler_pattern = wx.TextCtrl(self.m_assembler_pane, wx.ID_ANY,
                                               u"${COMMAND} ${FLAGS} ${OUTPUT_FLAG} ${OUTPUT_PREFIX}${OUTPUT} ${INPUTS}",
                                               wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer20512.Add(self.m_assembler_pattern, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer20512.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_assembler_pane.SetSizer(fgSizer20512)
        self.m_assembler_pane.Layout()
        fgSizer20512.Fit(self.m_assembler_pane)
        self.m_book.AddPage(self.m_assembler_pane, u"a page", False)
        self.m_assembler_general_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                 wx.TAB_TRAVERSAL)
        fgSizer273 = wx.FlexGridSizer(3, 1, 0, 0)

        fgSizer273.AddGrowableCol(0)
        fgSizer273.AddGrowableRow(1)
        fgSizer273.SetFlexibleDirection(wx.BOTH)
        fgSizer273.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer274 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer274.AddGrowableCol(1)
        fgSizer274.SetFlexibleDirection(wx.BOTH)
        fgSizer274.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText135 = wx.StaticText(self.m_assembler_general_pane, wx.ID_ANY, u"flags:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText135.Wrap(-1)
        fgSizer274.Add(self.m_staticText135, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_assembler_flags = wx.TextCtrl(self.m_assembler_general_pane, wx.ID_ANY, wx.EmptyString,
                                             wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer274.Add(self.m_assembler_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer273.Add(fgSizer274, 1, wx.EXPAND, 5)

        sbSizer632 = wx.StaticBoxSizer(wx.StaticBox(self.m_assembler_general_pane, wx.ID_ANY, u"includes"), wx.VERTICAL)

        fgSizer21232 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21232.AddGrowableCol(0)
        fgSizer21232.AddGrowableRow(0)
        fgSizer21232.SetFlexibleDirection(wx.BOTH)
        fgSizer21232.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_assembler_includesChoices = []
        self.m_assembler_includes = wx.ListBox(sbSizer632.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                               m_assembler_includesChoices, 0)
        fgSizer21232.Add(self.m_assembler_includes, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer21632 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21632.AddGrowableRow(4)
        fgSizer21632.SetFlexibleDirection(wx.BOTH)
        fgSizer21632.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5332 = wx.BitmapButton(sbSizer632.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21632.Add(self.m_bpButton5332, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5432 = wx.BitmapButton(sbSizer632.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21632.Add(self.m_bpButton5432, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5532 = wx.BitmapButton(sbSizer632.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21632.Add(self.m_bpButton5532, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_bpButton5632 = wx.BitmapButton(sbSizer632.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21632.Add(self.m_bpButton5632, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21232.Add(fgSizer21632, 1, wx.EXPAND, 5)

        sbSizer632.Add(fgSizer21232, 1, wx.EXPAND, 5)

        fgSizer273.Add(sbSizer632, 1, wx.EXPAND, 5)

        fgSizer284 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer284.AddGrowableCol(0)
        fgSizer284.SetFlexibleDirection(wx.BOTH)
        fgSizer284.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_assembler_suppres_warnings = wx.CheckBox(self.m_assembler_general_pane, wx.ID_ANY, u"supress warnings",
                                                        wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer284.Add(self.m_assembler_suppres_warnings, 0, wx.ALL, 5)

        self.m_assembler_announce_version = wx.CheckBox(self.m_assembler_general_pane, wx.ID_ANY, u"announce version",
                                                        wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer284.Add(self.m_assembler_announce_version, 0, wx.ALL, 5)

        fgSizer273.Add(fgSizer284, 1, wx.EXPAND, 5)

        self.m_assembler_general_pane.SetSizer(fgSizer273)
        self.m_assembler_general_pane.Layout()
        fgSizer273.Fit(self.m_assembler_general_pane)
        self.m_book.AddPage(self.m_assembler_general_pane, u"a page", False)
        self.m_linker_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer20513 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer20513.AddGrowableCol(1)
        fgSizer20513.AddGrowableRow(1)
        fgSizer20513.AddGrowableRow(3)
        fgSizer20513.SetFlexibleDirection(wx.BOTH)
        fgSizer20513.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText9413 = wx.StaticText(self.m_linker_pane, wx.ID_ANY, u"command:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9413.Wrap(-1)
        fgSizer20513.Add(self.m_staticText9413, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_linker_command = wx.TextCtrl(self.m_linker_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer20513.Add(self.m_linker_command, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText9513 = wx.StaticText(self.m_linker_pane, wx.ID_ANY, u"options:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9513.Wrap(-1)
        fgSizer20513.Add(self.m_staticText9513, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.m_linker_options = wx.TextCtrl(self.m_linker_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, wx.TE_READONLY)
        fgSizer20513.Add(self.m_linker_options, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        self.m_staticText9723 = wx.StaticText(self.m_linker_pane, wx.ID_ANY, u"pattern:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText9723.Wrap(-1)
        fgSizer20513.Add(self.m_staticText9723, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_linker_pattern = wx.TextCtrl(self.m_linker_pane, wx.ID_ANY,
                                            u"${COMMAND} ${FLAGS} ${OUTPUT_FLAG} ${OUTPUT_PREFIX}${OUTPUT} ${INPUTS}",
                                            wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer20513.Add(self.m_linker_pattern, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer20513.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_linker_pane.SetSizer(fgSizer20513)
        self.m_linker_pane.Layout()
        fgSizer20513.Fit(self.m_linker_pane)
        self.m_book.AddPage(self.m_linker_pane, u"a page", False)
        self.m_linker_general_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              wx.TAB_TRAVERSAL)
        fgSizer288 = wx.FlexGridSizer(6, 1, 0, 0)
        fgSizer288.AddGrowableCol(0)
        fgSizer288.AddGrowableRow(5)
        fgSizer288.SetFlexibleDirection(wx.BOTH)
        fgSizer288.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_linker_no_standard_start_files = wx.CheckBox(self.m_linker_general_pane, wx.ID_ANY,
                                                            u"don't use standard start files", wx.DefaultPosition,
                                                            wx.DefaultSize, 0)
        fgSizer288.Add(self.m_linker_no_standard_start_files, 0, wx.ALL, 5)

        self.m_linker_no_default_libraries = wx.CheckBox(self.m_linker_general_pane, wx.ID_ANY,
                                                         u"don't use default libraries", wx.DefaultPosition,
                                                         wx.DefaultSize, 0)
        fgSizer288.Add(self.m_linker_no_default_libraries, 0, wx.ALL, 5)

        self.m_linker_no_standard_libraries = wx.CheckBox(self.m_linker_general_pane, wx.ID_ANY,
                                                          u"don't use standard libraries", wx.DefaultPosition,
                                                          wx.DefaultSize, 0)
        fgSizer288.Add(self.m_linker_no_standard_libraries, 0, wx.ALL, 5)

        self.m_linker_strip_symbols = wx.CheckBox(self.m_linker_general_pane, wx.ID_ANY, u"strip symbol information",
                                                  wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer288.Add(self.m_linker_strip_symbols, 0, wx.ALL, 5)

        self.m_linker_pthreads = wx.CheckBox(self.m_linker_general_pane, wx.ID_ANY, u"support for pthreads",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer288.Add(self.m_linker_pthreads, 0, wx.ALL, 5)

        self.m_linker_general_pane.SetSizer(fgSizer288)
        self.m_linker_general_pane.Layout()
        fgSizer288.Fit(self.m_linker_general_pane)
        self.m_book.AddPage(self.m_linker_general_pane, u"a page", False)
        self.m_linker_libs_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer2142 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer2142.AddGrowableCol(0)
        fgSizer2142.AddGrowableRow(0)
        fgSizer2142.AddGrowableRow(1)
        fgSizer2142.SetFlexibleDirection(wx.BOTH)
        fgSizer2142.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer612 = wx.StaticBoxSizer(wx.StaticBox(self.m_linker_libs_pane, wx.ID_ANY, u"libraries"), wx.VERTICAL)

        fgSizer21222 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21222.AddGrowableCol(0)
        fgSizer21222.AddGrowableRow(0)
        fgSizer21222.SetFlexibleDirection(wx.BOTH)
        fgSizer21222.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_linker_libsChoices = []
        self.m_linker_libs = wx.ListBox(sbSizer612.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                        m_linker_libsChoices, wx.LB_SINGLE)
        self.m_linker_libs.SetToolTip(u"double click over item for editing ")

        fgSizer21222.Add(self.m_linker_libs, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer21622 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21622.AddGrowableRow(4)
        fgSizer21622.SetFlexibleDirection(wx.BOTH)
        fgSizer21622.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5322 = wx.BitmapButton(sbSizer612.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21622.Add(self.m_bpButton5322, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_linker_library = wx.BitmapButton(sbSizer612.GetStaticBox(), wx.ID_ANY,
                                                           wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                           wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_linker_library.Enable(False)

        fgSizer21622.Add(self.m_btn_delete_linker_library, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_linker_library = wx.BitmapButton(sbSizer612.GetStaticBox(), wx.ID_ANY,
                                                            wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_linker_library.Enable(False)

        fgSizer21622.Add(self.m_btn_move_up_linker_library, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_linker_library = wx.BitmapButton(sbSizer612.GetStaticBox(), wx.ID_ANY,
                                                              wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_linker_library.Enable(False)

        fgSizer21622.Add(self.m_btn_move_down_linker_library, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21222.Add(fgSizer21622, 1, wx.EXPAND, 5)

        sbSizer612.Add(fgSizer21222, 1, wx.EXPAND, 5)

        fgSizer2142.Add(sbSizer612, 1, wx.EXPAND, 5)

        sbSizer633 = wx.StaticBoxSizer(wx.StaticBox(self.m_linker_libs_pane, wx.ID_ANY, u"search paths"), wx.VERTICAL)

        fgSizer21233 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21233.AddGrowableCol(0)
        fgSizer21233.AddGrowableRow(0)
        fgSizer21233.SetFlexibleDirection(wx.BOTH)
        fgSizer21233.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_linker_lib_pathChoices = []
        self.m_linker_lib_path = wx.ListBox(sbSizer633.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            m_linker_lib_pathChoices, wx.LB_SINGLE)
        self.m_linker_lib_path.SetToolTip(u"double click over item for editing ")

        fgSizer21233.Add(self.m_linker_lib_path, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer21633 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21633.AddGrowableRow(4)
        fgSizer21633.SetFlexibleDirection(wx.BOTH)
        fgSizer21633.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5333 = wx.BitmapButton(sbSizer633.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21633.Add(self.m_bpButton5333, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_linker_library_path = wx.BitmapButton(sbSizer633.GetStaticBox(), wx.ID_ANY,
                                                                wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                                wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_linker_library_path.Enable(False)

        fgSizer21633.Add(self.m_btn_delete_linker_library_path, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_linker_library_path = wx.BitmapButton(sbSizer633.GetStaticBox(), wx.ID_ANY,
                                                                 wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                                 wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_linker_library_path.Enable(False)

        fgSizer21633.Add(self.m_btn_move_up_linker_library_path, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_linker_library_path = wx.BitmapButton(sbSizer633.GetStaticBox(), wx.ID_ANY,
                                                                   wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN,
                                                                                            wx.ART_BUTTON),
                                                                   wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_linker_library_path.Enable(False)

        fgSizer21633.Add(self.m_btn_move_down_linker_library_path, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21233.Add(fgSizer21633, 1, wx.EXPAND, 5)

        sbSizer633.Add(fgSizer21233, 1, wx.EXPAND, 5)

        fgSizer2142.Add(sbSizer633, 1, wx.EXPAND, 5)

        self.m_linker_libs_pane.SetSizer(fgSizer2142)
        self.m_linker_libs_pane.Layout()
        fgSizer2142.Fit(self.m_linker_libs_pane)
        self.m_book.AddPage(self.m_linker_libs_pane, u"a page", False)
        self.m_linker_other_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.TAB_TRAVERSAL)
        fgSizer299 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer299.AddGrowableCol(0)
        fgSizer299.AddGrowableRow(1)
        fgSizer299.AddGrowableRow(2)
        fgSizer299.SetFlexibleDirection(wx.BOTH)
        fgSizer299.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer300 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer300.AddGrowableCol(1)
        fgSizer300.SetFlexibleDirection(wx.BOTH)
        fgSizer300.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText142 = wx.StaticText(self.m_linker_other_pane, wx.ID_ANY, u"flags:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText142.Wrap(-1)
        fgSizer300.Add(self.m_staticText142, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_linker_flags = wx.TextCtrl(self.m_linker_other_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer300.Add(self.m_linker_flags, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer299.Add(fgSizer300, 1, wx.EXPAND, 5)

        sbSizer613 = wx.StaticBoxSizer(wx.StaticBox(self.m_linker_other_pane, wx.ID_ANY, u"options"), wx.VERTICAL)

        fgSizer21223 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21223.AddGrowableCol(0)
        fgSizer21223.AddGrowableRow(0)
        fgSizer21223.SetFlexibleDirection(wx.BOTH)
        fgSizer21223.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_linker_other_optionsChoices = []
        self.m_linker_other_options = wx.ListBox(sbSizer613.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                 wx.DefaultSize, m_linker_other_optionsChoices, 0)
        fgSizer21223.Add(self.m_linker_other_options, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer21623 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21623.AddGrowableRow(4)
        fgSizer21623.SetFlexibleDirection(wx.BOTH)
        fgSizer21623.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5323 = wx.BitmapButton(sbSizer613.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21623.Add(self.m_bpButton5323, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_linker_option = wx.BitmapButton(sbSizer613.GetStaticBox(), wx.ID_ANY,
                                                          wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                          wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_linker_option.Enable(False)

        fgSizer21623.Add(self.m_btn_delete_linker_option, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_linker_option = wx.BitmapButton(sbSizer613.GetStaticBox(), wx.ID_ANY,
                                                           wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                           wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_linker_option.Enable(False)

        fgSizer21623.Add(self.m_btn_move_up_linker_option, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_linker_option = wx.BitmapButton(sbSizer613.GetStaticBox(), wx.ID_ANY,
                                                             wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                                             wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_linker_option.Enable(False)

        fgSizer21623.Add(self.m_btn_move_down_linker_option, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21223.Add(fgSizer21623, 1, wx.EXPAND, 5)

        sbSizer613.Add(fgSizer21223, 1, wx.EXPAND, 5)

        fgSizer299.Add(sbSizer613, 1, wx.EXPAND, 5)

        sbSizer634 = wx.StaticBoxSizer(wx.StaticBox(self.m_linker_other_pane, wx.ID_ANY, u"extra objects"), wx.VERTICAL)

        fgSizer21234 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer21234.AddGrowableCol(0)
        fgSizer21234.AddGrowableRow(0)
        fgSizer21234.SetFlexibleDirection(wx.BOTH)
        fgSizer21234.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_linker_extra_objectsChoices = []
        self.m_linker_extra_objects = wx.ListBox(sbSizer634.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                 wx.DefaultSize, m_linker_extra_objectsChoices, 0)
        fgSizer21234.Add(self.m_linker_extra_objects, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer21634 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer21634.AddGrowableRow(4)
        fgSizer21634.SetFlexibleDirection(wx.BOTH)
        fgSizer21634.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_bpButton5334 = wx.BitmapButton(sbSizer634.GetStaticBox(), wx.ID_ANY,
                                              wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON), wx.DefaultPosition,
                                              wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer21634.Add(self.m_bpButton5334, 0, wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_btn_delete_linker_extra_object = wx.BitmapButton(sbSizer634.GetStaticBox(), wx.ID_ANY,
                                                                wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                                wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_linker_extra_object.Enable(False)

        fgSizer21634.Add(self.m_btn_delete_linker_extra_object, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_up_linker_extra_object = wx.BitmapButton(sbSizer634.GetStaticBox(), wx.ID_ANY,
                                                                 wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                                 wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_linker_extra_object.Enable(False)

        fgSizer21634.Add(self.m_btn_move_up_linker_extra_object, 0, wx.RIGHT | wx.LEFT, 5)

        self.m_btn_move_down_linker_extra_object = wx.BitmapButton(sbSizer634.GetStaticBox(), wx.ID_ANY,
                                                                   wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN,
                                                                                            wx.ART_BUTTON),
                                                                   wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_linker_extra_object.Enable(False)

        fgSizer21634.Add(self.m_btn_move_down_linker_extra_object, 0, wx.RIGHT | wx.LEFT, 5)

        fgSizer21234.Add(fgSizer21634, 1, wx.EXPAND, 5)

        sbSizer634.Add(fgSizer21234, 1, wx.EXPAND, 5)

        fgSizer299.Add(sbSizer634, 1, wx.EXPAND, 5)

        self.m_linker_other_pane.SetSizer(fgSizer299)
        self.m_linker_other_pane.Layout()
        fgSizer299.Fit(self.m_linker_other_pane)
        self.m_book.AddPage(self.m_linker_other_pane, u"a page", False)
        self.m_linker_shared_pane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                             wx.TAB_TRAVERSAL)
        fgSizer309 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer309.AddGrowableCol(0)
        fgSizer309.AddGrowableRow(2)
        fgSizer309.SetFlexibleDirection(wx.BOTH)
        fgSizer309.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_linker_shared = wx.CheckBox(self.m_linker_shared_pane, wx.ID_ANY, u"shared", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        fgSizer309.Add(self.m_linker_shared, 0, wx.ALL, 5)

        fgSizer310 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer310.AddGrowableCol(1)
        fgSizer310.SetFlexibleDirection(wx.BOTH)
        fgSizer310.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText143 = wx.StaticText(self.m_linker_shared_pane, wx.ID_ANY, u"shared name:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText143.Wrap(-1)
        fgSizer310.Add(self.m_staticText143, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_linker_shared_name = wx.TextCtrl(self.m_linker_shared_pane, wx.ID_ANY, wx.EmptyString,
                                                wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer310.Add(self.m_linker_shared_name, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText144 = wx.StaticText(self.m_linker_shared_pane, wx.ID_ANY, u"import library:",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText144.Wrap(-1)
        fgSizer310.Add(self.m_staticText144, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_linker_import_library = wx.TextCtrl(self.m_linker_shared_pane, wx.ID_ANY, wx.EmptyString,
                                                   wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer310.Add(self.m_linker_import_library, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText145 = wx.StaticText(self.m_linker_shared_pane, wx.ID_ANY, u"def file:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText145.Wrap(-1)
        fgSizer310.Add(self.m_staticText145, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_linker_default_file = wx.TextCtrl(self.m_linker_shared_pane, wx.ID_ANY, wx.EmptyString,
                                                 wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer310.Add(self.m_linker_default_file, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer309.Add(fgSizer310, 1, wx.EXPAND, 5)

        self.m_linker_shared_pane.SetSizer(fgSizer309)
        self.m_linker_shared_pane.Layout()
        fgSizer309.Fit(self.m_linker_shared_pane)
        self.m_book.AddPage(self.m_linker_shared_pane, u"a page", False)

        fgSizer204.Add(self.m_book, 1, wx.EXPAND | wx.ALL, 5)

        self.m_book_container.SetSizer(fgSizer204)
        self.m_book_container.Layout()
        fgSizer204.Fit(self.m_book_container)
        self.m_splitter5.SplitVertically(self.m_panel18, self.m_book_container, 181)
        fgSizer202.Add(self.m_splitter5, 1, wx.EXPAND, 5)

        self.m_panel17.SetSizer(fgSizer202)
        self.m_panel17.Layout()
        fgSizer202.Fit(self.m_panel17)
        self.m_auinotebook1.AddPage(self.m_panel17, u"tools", False)
        self.m_panel46 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer272 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer272.AddGrowableCol(0)
        fgSizer272.AddGrowableRow(0)
        fgSizer272.AddGrowableRow(1)
        fgSizer272.SetFlexibleDirection(wx.BOTH)
        fgSizer272.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer592 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel46, wx.ID_ANY, u"before build step"), wx.VERTICAL)

        fgSizer2125 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2125.AddGrowableCol(0)
        fgSizer2125.AddGrowableRow(0)
        fgSizer2125.SetFlexibleDirection(wx.BOTH)
        fgSizer2125.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_cpp_before_build_stepsChoices = []
        self.m_cpp_before_build_steps = wx.ListBox(sbSizer592.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                   wx.DefaultSize, m_cpp_before_build_stepsChoices, 0)
        fgSizer2125.Add(self.m_cpp_before_build_steps, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer2165 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer2165.AddGrowableRow(4)
        fgSizer2165.SetFlexibleDirection(wx.BOTH)
        fgSizer2165.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_btn_add_before_build_step = wx.BitmapButton(sbSizer592.GetStaticBox(), wx.ID_ANY,
                                                           wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON),
                                                           wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2165.Add(self.m_btn_add_before_build_step, 0, wx.ALL, 5)

        self.m_btn_delete_before_build_step = wx.BitmapButton(sbSizer592.GetStaticBox(), wx.ID_ANY,
                                                              wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_before_build_step.Enable(False)

        fgSizer2165.Add(self.m_btn_delete_before_build_step, 0, wx.ALL, 5)

        self.m_btn_move_up_before_build_step = wx.BitmapButton(sbSizer592.GetStaticBox(), wx.ID_ANY,
                                                               wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                               wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_before_build_step.Enable(False)

        fgSizer2165.Add(self.m_btn_move_up_before_build_step, 0, wx.ALL, 5)

        self.m_btn_move_down_before_build_step = wx.BitmapButton(sbSizer592.GetStaticBox(), wx.ID_ANY,
                                                                 wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN,
                                                                                          wx.ART_BUTTON),
                                                                 wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_before_build_step.Enable(False)

        fgSizer2165.Add(self.m_btn_move_down_before_build_step, 0, wx.ALL, 5)

        fgSizer2125.Add(fgSizer2165, 1, wx.EXPAND, 5)

        sbSizer592.Add(fgSizer2125, 1, wx.EXPAND, 5)

        fgSizer272.Add(sbSizer592, 1, wx.EXPAND, 5)

        sbSizer593 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel46, wx.ID_ANY, u"after build step"), wx.VERTICAL)

        fgSizer2126 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer2126.AddGrowableCol(0)
        fgSizer2126.AddGrowableRow(0)
        fgSizer2126.SetFlexibleDirection(wx.BOTH)
        fgSizer2126.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_cpp_after_build_stepsChoices = []
        self.m_cpp_after_build_steps = wx.ListBox(sbSizer593.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition,
                                                  wx.DefaultSize, m_cpp_after_build_stepsChoices, 0)
        fgSizer2126.Add(self.m_cpp_after_build_steps, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer2166 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer2166.AddGrowableRow(4)
        fgSizer2166.SetFlexibleDirection(wx.BOTH)
        fgSizer2166.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_btn_add_after_build_step = wx.BitmapButton(sbSizer593.GetStaticBox(), wx.ID_ANY,
                                                          wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON),
                                                          wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer2166.Add(self.m_btn_add_after_build_step, 0, wx.ALL, 5)

        self.m_btn_delete_after_build_step = wx.BitmapButton(sbSizer593.GetStaticBox(), wx.ID_ANY,
                                                             wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON),
                                                             wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete_after_build_step.Enable(False)

        fgSizer2166.Add(self.m_btn_delete_after_build_step, 0, wx.ALL, 5)

        self.m_btn_move_up_after_build_step = wx.BitmapButton(sbSizer593.GetStaticBox(), wx.ID_ANY,
                                                              wx.ArtProvider.GetBitmap(wx.ART_GO_UP, wx.ART_BUTTON),
                                                              wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_up_after_build_step.Enable(False)

        fgSizer2166.Add(self.m_btn_move_up_after_build_step, 0, wx.ALL, 5)

        self.m_btn_move_down_after_build_step = wx.BitmapButton(sbSizer593.GetStaticBox(), wx.ID_ANY,
                                                                wx.ArtProvider.GetBitmap(wx.ART_GO_DOWN, wx.ART_BUTTON),
                                                                wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_move_down_after_build_step.Enable(False)

        fgSizer2166.Add(self.m_btn_move_down_after_build_step, 0, wx.ALL, 5)

        fgSizer2126.Add(fgSizer2166, 1, wx.EXPAND, 5)

        sbSizer593.Add(fgSizer2126, 1, wx.EXPAND, 5)

        fgSizer272.Add(sbSizer593, 1, wx.EXPAND, 5)

        self.m_panel46.SetSizer(fgSizer272)
        self.m_panel46.Layout()
        fgSizer272.Fit(self.m_panel46)
        self.m_auinotebook1.AddPage(self.m_panel46, u"build steps", False)

        fgSizer200.Add(self.m_auinotebook1, 1, wx.ALL | wx.EXPAND, 5)

        self.SetSizer(fgSizer200)
        self.Layout()

        # Connect Events
        self.m_button58.Bind(wx.EVT_BUTTON, self.OnManageConfigs)
        self.m_cpp_build_type.Bind(wx.EVT_CHOICE, self.on_change_build_type)
        self.m_treeTools.Bind(wx.EVT_TREE_SEL_CHANGED, self.on_select_tree_tool)
        self.m_cpp_compiler_version.Bind(wx.EVT_CHOICE, self.OnChangeCPPCompilerVersion)
        self.m_cpp_compiler_version_flags.Bind(wx.EVT_TEXT_ENTER, self.OnChangeCPPCompilerVersion)
        self.m_cpp_compiler_exclude_std_includes.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerPreprocessor)
        self.m_cpp_compiler_preprocess_only.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerPreprocessor)
        self.m_cpp_compiler_defined_symbols.Bind(wx.EVT_LISTBOX, self.OnSelectCPPCompilerPreprocessorDefinedSymbol)
        self.m_cpp_compiler_defined_symbols.Bind(wx.EVT_LISTBOX_DCLICK, self.OnEditCPPCompilerPreprocessorDefinedSymbol)
        self.m_add_cpp_preprocessor_defined_symbol.Bind(wx.EVT_BUTTON, self.on_add_cpp_preprocessor_defined_symbol)
        self.m_btn_delete_cpp_preprocessor_defined_symbol.Bind(wx.EVT_BUTTON,
                                                               self.on_delete_cpp_preproprocessor_defined_symbol)
        self.m_btn_move_up_cpp_preprocessor_defined_symbol.Bind(wx.EVT_BUTTON,
                                                                self.OnMoveUpCPPPreprocessorDefinedSymbol)
        self.m_btn_move_down_cpp_preprocessor_defined_symbol.Bind(wx.EVT_BUTTON,
                                                                  self.OnMoveDownCPPPreprocessorDefinedSymbol)
        self.m_cpp_compiler_undefined_symbols.Bind(wx.EVT_LISTBOX, self.OnSelectCPPCompilerPreprocessorUndefinedSymbol)
        self.m_cpp_compiler_undefined_symbols.Bind(wx.EVT_LISTBOX_DCLICK,
                                                   self.OnEditCPPCompilerPreprocessorUndefinedSymbol)
        self.m_btn_add_cpp_preprocessor_undefined_symbol.Bind(wx.EVT_BUTTON, self.OnAddCPPPreprocessorUndefinedSymbol)
        self.m_btn_delete_cpp_preprocessor_undefined_symbol.Bind(wx.EVT_BUTTON,
                                                                 self.OnDeleteCPPPreprocessorUndefinedSymbol)
        self.m_btn_move_up_cpp_preprocessor_undefined_symbol.Bind(wx.EVT_BUTTON,
                                                                  self.OnMoveUpCPPPreprocessorUndefinedSymbol)
        self.m_btn_move_down_cpp_preprocessor_undefined_symbol.Bind(wx.EVT_BUTTON,
                                                                    self.OnMoveDownCPPPreprocessorUndefinedSymbol)
        self.m_cpp_compiler_include_paths.Bind(wx.EVT_LISTBOX, self.OnSelectCPPIncludePath)
        self.m_cpp_compiler_include_paths.Bind(wx.EVT_LISTBOX_DCLICK, self.OnEditCPPIncludePath)
        self.m_bpButton532.Bind(wx.EVT_BUTTON, self.OnAddCPPIncludePath)
        self.m_btn_delete_cpp_include_path.Bind(wx.EVT_BUTTON, self.OnDeleteCPPIncludePath)
        self.m_btn_move_up_include_path.Bind(wx.EVT_BUTTON, self.OnMoveUpCPPIncludePath)
        self.m_btn_move_down_include_path.Bind(wx.EVT_BUTTON, self.OnMoveDownCPPIncludePath)
        self.m_cpp_compiler_includes.Bind(wx.EVT_LISTBOX, self.OnSelectCPPInclude)
        self.m_cpp_compiler_includes.Bind(wx.EVT_LISTBOX_DCLICK, self.OnEditCPPInclude)
        self.m_bpButton533.Bind(wx.EVT_BUTTON, self.OnAddCPPInclude)
        self.m_btn_delete_cpp_include.Bind(wx.EVT_BUTTON, self.OnDeleteCPPInclude)
        self.m_btn_move_up_cpp_include.Bind(wx.EVT_BUTTON, self.OnMoveUpCPPInclude)
        self.m_btn_move_down_cpp_include.Bind(wx.EVT_BUTTON, self.OnMoveDownCPPInclude)
        self.m_cpp_compiler_level.Bind(wx.EVT_CHOICE, self.OnChangeCPPCompilerOptimization)
        self.m_cpp_compiler_level_flags.Bind(wx.EVT_TEXT_ENTER, self.OnChangeCPPCompilerOptimization)
        self.m_cpp_compiler_debug_level.Bind(wx.EVT_CHOICE, self.OnChangeCPPCompilerDebug)
        self.m_cpp_compiler_debug_flags.Bind(wx.EVT_TEXT_ENTER, self.OnChangeCPPCompilerDebug)
        self.m_cpp_compiler_debug_prof.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerDebug)
        self.m_cpp_compiler_debug_gprof.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerDebug)
        self.m_cpp_compiler_debug_gcov.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerDebug)
        self.m_cpp_compiler_warnings_syntax_only.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_warnings_pedantic.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_warnings_pedantic_are_errors.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_warnings_disable.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_warnings_all.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_warnings_extra.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_warnings_are_errors.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_warnings_conversions.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerWarnings)
        self.m_cpp_compiler_other_flags.Bind(wx.EVT_TEXT_ENTER, self.OnChangeCPPCompilerOther)
        self.m_cpp_compiler_verbose.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerOther)
        self.m_cpp_compiler_position_independent_code.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerOther)
        self.m_cpp_compiler_pthread_support.Bind(wx.EVT_CHECKBOX, self.OnChangeCPPCompilerOther)
        self.m_linker_libs.Bind(wx.EVT_LISTBOX, self.OnSelectLinkerLibrary)
        self.m_linker_libs.Bind(wx.EVT_LISTBOX_DCLICK, self.OnEditLinkerLibrary)
        self.m_bpButton5322.Bind(wx.EVT_BUTTON, self.OnAddLinkerLibrary)
        self.m_btn_delete_linker_library.Bind(wx.EVT_BUTTON, self.OnDeleteLinkerLibrary)
        self.m_btn_move_up_linker_library.Bind(wx.EVT_BUTTON, self.OnMoveUpLinkerLibrary)
        self.m_btn_move_down_linker_library.Bind(wx.EVT_BUTTON, self.OnMoveDownLinkerLibrary)
        self.m_linker_lib_path.Bind(wx.EVT_LISTBOX, self.OnSelectLinkerLibraryPath)
        self.m_linker_lib_path.Bind(wx.EVT_LISTBOX_DCLICK, self.OnEditLinkerLibraryPath)
        self.m_bpButton5333.Bind(wx.EVT_BUTTON, self.OnAddLinkerLibraryPath)
        self.m_btn_delete_linker_library_path.Bind(wx.EVT_BUTTON, self.OnRemoveLinkerLibraryPath)
        self.m_btn_move_up_linker_library_path.Bind(wx.EVT_BUTTON, self.OnMoveUpLinkerLibraryPath)
        self.m_btn_move_down_linker_library_path.Bind(wx.EVT_BUTTON, self.OnMoveDownLinkerLibraryPath)
        self.m_linker_other_options.Bind(wx.EVT_LISTBOX, self.OnSelectLinkerOptions)
        self.m_bpButton5323.Bind(wx.EVT_BUTTON, self.OnAddLinkerOption)
        self.m_btn_delete_linker_option.Bind(wx.EVT_BUTTON, self.OnDeleteLinkerOption)
        self.m_btn_move_up_linker_option.Bind(wx.EVT_BUTTON, self.OnMoveUpLinkerOption)
        self.m_btn_move_down_linker_option.Bind(wx.EVT_BUTTON, self.OnMoveDownLinkerOption)
        self.m_linker_extra_objects.Bind(wx.EVT_LISTBOX, self.OnSelectLinkerExtraObjects)
        self.m_bpButton5334.Bind(wx.EVT_BUTTON, self.OnAddLinkerExtraObject)
        self.m_btn_delete_linker_extra_object.Bind(wx.EVT_BUTTON, self.OnDeleteLinkerExtraObject)
        self.m_btn_move_up_linker_extra_object.Bind(wx.EVT_BUTTON, self.OnMoveUpLinkerExtraObject)
        self.m_btn_move_down_linker_extra_object.Bind(wx.EVT_BUTTON, self.OnMoveDownLinkerExtraObject)
        self.m_cpp_before_build_steps.Bind(wx.EVT_LISTBOX, self.OnSelectBeforeBuildStep)
        self.m_cpp_before_build_steps.Bind(wx.EVT_LISTBOX_DCLICK, self.OnEditBeforeBuildStep)
        self.m_btn_add_before_build_step.Bind(wx.EVT_BUTTON, self.OnAddCPPBeforeBuildStep)
        self.m_btn_delete_before_build_step.Bind(wx.EVT_BUTTON, self.OnDeleteCPPBeforeBuildStep)
        self.m_btn_move_up_before_build_step.Bind(wx.EVT_BUTTON, self.OnMoveUpCPPBeforeBuildStep)
        self.m_btn_move_down_before_build_step.Bind(wx.EVT_BUTTON, self.OnMoveDownCPPBeforeBuildStep)
        self.m_cpp_after_build_steps.Bind(wx.EVT_LISTBOX, self.OnSelectAfterBuildStep)
        self.m_cpp_after_build_steps.Bind(wx.EVT_LISTBOX_DCLICK, self.OnEditAfterBuildStep)
        self.m_btn_add_after_build_step.Bind(wx.EVT_BUTTON, self.OnAddCPPAfterBuildStep)
        self.m_btn_delete_after_build_step.Bind(wx.EVT_BUTTON, self.OnDeleteCPPAfterBuildStep)
        self.m_btn_move_up_after_build_step.Bind(wx.EVT_BUTTON, self.OnMoveUpCPPAfterBuildStep)
        self.m_btn_move_down_after_build_step.Bind(wx.EVT_BUTTON, self.OnMoveDownCPPAfterBuildStep)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnManageConfigs(self, event):
        event.Skip()

    def on_change_build_type(self, event):
        event.Skip()

    def on_select_tree_tool(self, event):
        event.Skip()

    def OnChangeCPPCompilerVersion(self, event):
        event.Skip()

    def OnChangeCPPCompilerPreprocessor(self, event):
        event.Skip()

    def OnSelectCPPCompilerPreprocessorDefinedSymbol(self, event):
        event.Skip()

    def OnEditCPPCompilerPreprocessorDefinedSymbol(self, event):
        event.Skip()

    def on_add_cpp_preprocessor_defined_symbol(self, event):
        event.Skip()

    def on_delete_cpp_preproprocessor_defined_symbol(self, event):
        event.Skip()

    def OnMoveUpCPPPreprocessorDefinedSymbol(self, event):
        event.Skip()

    def OnMoveDownCPPPreprocessorDefinedSymbol(self, event):
        event.Skip()

    def OnSelectCPPCompilerPreprocessorUndefinedSymbol(self, event):
        event.Skip()

    def OnEditCPPCompilerPreprocessorUndefinedSymbol(self, event):
        event.Skip()

    def OnAddCPPPreprocessorUndefinedSymbol(self, event):
        event.Skip()

    def OnDeleteCPPPreprocessorUndefinedSymbol(self, event):
        event.Skip()

    def OnMoveUpCPPPreprocessorUndefinedSymbol(self, event):
        event.Skip()

    def OnMoveDownCPPPreprocessorUndefinedSymbol(self, event):
        event.Skip()

    def OnSelectCPPIncludePath(self, event):
        event.Skip()

    def OnEditCPPIncludePath(self, event):
        event.Skip()

    def OnAddCPPIncludePath(self, event):
        event.Skip()

    def OnDeleteCPPIncludePath(self, event):
        event.Skip()

    def OnMoveUpCPPIncludePath(self, event):
        event.Skip()

    def OnMoveDownCPPIncludePath(self, event):
        event.Skip()

    def OnSelectCPPInclude(self, event):
        event.Skip()

    def OnEditCPPInclude(self, event):
        event.Skip()

    def OnAddCPPInclude(self, event):
        event.Skip()

    def OnDeleteCPPInclude(self, event):
        event.Skip()

    def OnMoveUpCPPInclude(self, event):
        event.Skip()

    def OnMoveDownCPPInclude(self, event):
        event.Skip()

    def OnChangeCPPCompilerOptimization(self, event):
        event.Skip()

    def OnChangeCPPCompilerDebug(self, event):
        event.Skip()

    def OnChangeCPPCompilerWarnings(self, event):
        event.Skip()

    def OnChangeCPPCompilerOther(self, event):
        event.Skip()

    def OnSelectLinkerLibrary(self, event):
        event.Skip()

    def OnEditLinkerLibrary(self, event):
        event.Skip()

    def OnAddLinkerLibrary(self, event):
        event.Skip()

    def OnDeleteLinkerLibrary(self, event):
        event.Skip()

    def OnMoveUpLinkerLibrary(self, event):
        event.Skip()

    def OnMoveDownLinkerLibrary(self, event):
        event.Skip()

    def OnSelectLinkerLibraryPath(self, event):
        event.Skip()

    def OnEditLinkerLibraryPath(self, event):
        event.Skip()

    def OnAddLinkerLibraryPath(self, event):
        event.Skip()

    def OnRemoveLinkerLibraryPath(self, event):
        event.Skip()

    def OnMoveUpLinkerLibraryPath(self, event):
        event.Skip()

    def OnMoveDownLinkerLibraryPath(self, event):
        event.Skip()

    def OnSelectLinkerOptions(self, event):
        event.Skip()

    def OnAddLinkerOption(self, event):
        event.Skip()

    def OnDeleteLinkerOption(self, event):
        event.Skip()

    def OnMoveUpLinkerOption(self, event):
        event.Skip()

    def OnMoveDownLinkerOption(self, event):
        event.Skip()

    def OnSelectLinkerExtraObjects(self, event):
        event.Skip()

    def OnAddLinkerExtraObject(self, event):
        event.Skip()

    def OnDeleteLinkerExtraObject(self, event):
        event.Skip()

    def OnMoveUpLinkerExtraObject(self, event):
        event.Skip()

    def OnMoveDownLinkerExtraObject(self, event):
        event.Skip()

    def OnSelectBeforeBuildStep(self, event):
        event.Skip()

    def OnEditBeforeBuildStep(self, event):
        event.Skip()

    def OnAddCPPBeforeBuildStep(self, event):
        event.Skip()

    def OnDeleteCPPBeforeBuildStep(self, event):
        event.Skip()

    def OnMoveUpCPPBeforeBuildStep(self, event):
        event.Skip()

    def OnMoveDownCPPBeforeBuildStep(self, event):
        event.Skip()

    def OnSelectAfterBuildStep(self, event):
        event.Skip()

    def OnEditAfterBuildStep(self, event):
        event.Skip()

    def OnAddCPPAfterBuildStep(self, event):
        event.Skip()

    def OnDeleteCPPAfterBuildStep(self, event):
        event.Skip()

    def OnMoveUpCPPAfterBuildStep(self, event):
        event.Skip()

    def OnMoveDownCPPAfterBuildStep(self, event):
        event.Skip()

    def m_splitter5OnIdle(self, event):
        self.m_splitter5.SetSashPosition(181)
        self.m_splitter5.Unbind(wx.EVT_IDLE)

###########################################################################
## Class SettingsDialogBase
###########################################################################


class SettingsDialogBase ( wxx.Dialog ):
    
    def __init__( self, parent ):
        wxx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Build settings", pos = wx.DefaultPosition, size = wx.Size( 767,700 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )
        #dbc = wx.TheColourDatabase
        #fore = dbc.Find('LIGHT GREY')
        #back = dbc.Find('DARK GREY')
        #self.SetForegroundColour(fore)
        #self.SetBackgroundColour(back)
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        from .pane._CCBuildSettingsPane import CCBuildSettingsPane
        fgSizer154 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer154.AddGrowableCol( 0 )
        fgSizer154.AddGrowableRow( 0 )
        fgSizer154.SetFlexibleDirection( wx.BOTH )
        fgSizer154.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self._build_settings = CCBuildSettingsPane(self, self._project)
        fgSizer154.Add( self._build_settings, 1, wx.EXPAND |wx.ALL, 5 )
        
        fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer159.AddGrowableCol( 1 )
        fgSizer159.SetFlexibleDirection( wx.BOTH )
        fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        fgSizer159.SetMinSize( wx.Size( -1,42 ) ) 
        self.m_info = wx.BitmapButton( self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
        self.m_info.SetMinSize( wx.Size( 32,32 ) )
        
        fgSizer159.Add( self.m_info, 0, wx.ALL|wx.FIXED_MINSIZE, 5 )
        
        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button( self, wx.ID_OK )
        m_sdbSizer12.AddButton( self.m_sdbSizer12OK )
        self.m_sdbSizer12Cancel = wx.Button( self, wx.ID_CANCEL )
        m_sdbSizer12.AddButton( self.m_sdbSizer12Cancel )
        m_sdbSizer12.Realize();
        m_sdbSizer12.SetMinSize( wx.Size( -1,32 ) ) 
        
        fgSizer159.Add( m_sdbSizer12, 0, wx.EXPAND, 5 )
        
        
        fgSizer154.Add( fgSizer159, 0, wx.EXPAND|wx.FIXED_MINSIZE, 5 )
        
        
        self.SetSizer( fgSizer154 )
        self.Layout()
        
        self.Centre( wx.BOTH )
        
        # Connect Events
        self.m_sdbSizer12Cancel.Bind( wx.EVT_BUTTON, self.on_cancel )
        self.m_sdbSizer12OK.Bind( wx.EVT_BUTTON, self.on_ok )
    
    def __del__( self ):
        pass
    
    
    # Virtual event handlers, overide them in your derived class
    def on_cancel( self, event ):
        event.Skip()
    
    def on_ok( self, event ):
        event.Skip()
    

