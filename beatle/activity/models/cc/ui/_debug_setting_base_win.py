# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Aug 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.aui
from beatle.lib import wxx

# special import for beatle development
from beatle.lib.handlers import Identifiers


###########################################################################
## Class CCDebugSettingsPaneBase
###########################################################################

class CCDebugSettingsPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(555, 477),
                          style=wx.TAB_TRAVERSAL)

        fgSizer200 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer200.AddGrowableCol(0)
        fgSizer200.AddGrowableRow(3)
        fgSizer200.SetFlexibleDirection(wx.BOTH)
        fgSizer200.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText92 = wx.StaticText(self, wx.ID_ANY, u"C++ Debug Settings", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText92.Wrap(-1)
        self.m_staticText92.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))

        fgSizer200.Add(self.m_staticText92, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)

        self.m_staticline1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        fgSizer200.Add(self.m_staticline1, 0, wx.EXPAND | wx.ALL, 5)

        fgSizer201 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer201.AddGrowableCol(1)
        fgSizer201.SetFlexibleDirection(wx.BOTH)
        fgSizer201.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText93 = wx.StaticText(self, wx.ID_ANY, u"configuration", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText93.Wrap(-1)
        fgSizer201.Add(self.m_staticText93, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_build_configsChoices = [u"default"]
        self.m_build_configs = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_build_configsChoices, 0)
        self.m_build_configs.SetSelection(0)
        fgSizer201.Add(self.m_build_configs, 0, wx.ALL | wx.EXPAND, 5)

        self.m_button58 = wx.Button(self, wx.ID_ANY, u"configurations ...", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button58.Enable(False)

        fgSizer201.Add(self.m_button58, 0, wx.ALL, 5)

        fgSizer200.Add(fgSizer201, 1, wx.EXPAND, 5)

        self.m_auinotebook1 = wxx.AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              wx.FULL_REPAINT_ON_RESIZE)
        self.m_panel45 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer268 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer268.AddGrowableCol(0)
        fgSizer268.AddGrowableRow(1)
        fgSizer268.SetFlexibleDirection(wx.BOTH)
        fgSizer268.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer269 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer269.AddGrowableCol(1)
        fgSizer269.SetFlexibleDirection(wx.BOTH)
        fgSizer269.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText129 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"type:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText129.Wrap(-1)
        fgSizer269.Add(self.m_staticText129, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_debug_typeChoices = [u"local", u"remote"]
        self.m_debug_type = wx.Choice(self.m_panel45, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                      m_debug_typeChoices, 0)
        self.m_debug_type.SetSelection(0)
        fgSizer269.Add(self.m_debug_type, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText131 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"local file:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText131.Wrap(-1)
        fgSizer269.Add(self.m_staticText131, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_local_file = wx.FilePickerCtrl(self.m_panel45, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*",
                                              wx.DefaultPosition, wx.DefaultSize,
                                              wx.FLP_DEFAULT_STYLE | wx.FLP_USE_TEXTCTRL)
        fgSizer269.Add(self.m_local_file, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText132 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"source dir:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText132.Wrap(-1)
        fgSizer269.Add(self.m_staticText132, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_local_dir = wx.DirPickerCtrl(self.m_panel45, wx.ID_ANY, wx.EmptyString, u"Select a folder",
                                            wx.DefaultPosition, wx.DefaultSize,
                                            wx.DIRP_DEFAULT_STYLE | wx.DIRP_DIR_MUST_EXIST | wx.DIRP_USE_TEXTCTRL)
        fgSizer269.Add(self.m_local_dir, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText12 = wx.StaticText(self.m_panel45, wx.ID_ANY, u"parameters:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText12.Wrap(-1)
        fgSizer269.Add(self.m_staticText12, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_debug_arguments = wx.TextCtrl(self.m_panel45, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize,
                                             wx.TE_PROCESS_ENTER)
        fgSizer269.Add(self.m_debug_arguments, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer268.Add(fgSizer269, 1, wx.EXPAND, 5)

        self.m_panel45.SetSizer(fgSizer268)
        self.m_panel45.Layout()
        fgSizer268.Fit(self.m_panel45)
        self.m_auinotebook1.AddPage(self.m_panel45, u"target", True)
        self.m_panel17 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel17.Enable(False)

        fgSizer269 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer269.AddGrowableCol(0)
        fgSizer269.AddGrowableRow(0)
        fgSizer269.SetFlexibleDirection(wx.BOTH)
        fgSizer269.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer87 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel17, wx.ID_ANY, u"connection"), wx.VERTICAL)

        fgSizer362 = wx.FlexGridSizer(4, 2, 0, 0)
        fgSizer362.AddGrowableCol(1)
        fgSizer362.SetFlexibleDirection(wx.BOTH)
        fgSizer362.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText198 = wx.StaticText(sbSizer87.GetStaticBox(), wx.ID_ANY, u"host:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText198.Wrap(-1)
        fgSizer362.Add(self.m_staticText198, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_remote_host = wx.TextCtrl(sbSizer87.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer362.Add(self.m_remote_host, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText199 = wx.StaticText(sbSizer87.GetStaticBox(), wx.ID_ANY, u"port:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText199.Wrap(-1)
        fgSizer362.Add(self.m_staticText199, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_remote_port = wx.TextCtrl(sbSizer87.GetStaticBox(), wx.ID_ANY, u"22", wx.DefaultPosition, wx.DefaultSize,
                                         wx.TE_PROCESS_ENTER)
        fgSizer362.Add(self.m_remote_port, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText200 = wx.StaticText(sbSizer87.GetStaticBox(), wx.ID_ANY, u"user:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText200.Wrap(-1)
        fgSizer362.Add(self.m_staticText200, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_remote_user = wx.TextCtrl(sbSizer87.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, wx.TE_PROCESS_ENTER)
        fgSizer362.Add(self.m_remote_user, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText201 = wx.StaticText(sbSizer87.GetStaticBox(), wx.ID_ANY, u"password:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText201.Wrap(-1)
        fgSizer362.Add(self.m_staticText201, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_remote_password = wx.TextCtrl(sbSizer87.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, wx.TE_PASSWORD | wx.TE_PROCESS_ENTER)
        fgSizer362.Add(self.m_remote_password, 0, wx.ALL | wx.EXPAND, 5)

        sbSizer87.Add(fgSizer362, 1, wx.EXPAND, 5)

        self.m_button_test_connection = wx.Button(sbSizer87.GetStaticBox(), wx.ID_ANY, u"verify connection",
                                                  wx.DefaultPosition, wx.DefaultSize, 0)
        sbSizer87.Add(self.m_button_test_connection, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        fgSizer269.Add(sbSizer87, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer363 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer363.AddGrowableCol(1)
        fgSizer363.SetFlexibleDirection(wx.BOTH)
        fgSizer363.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText203 = wx.StaticText(self.m_panel17, wx.ID_ANY, u"remote file:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText203.Wrap(-1)
        fgSizer363.Add(self.m_staticText203, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_remote_file = wx.TextCtrl(self.m_panel17, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize,
                                         wx.TE_PROCESS_ENTER)
        fgSizer363.Add(self.m_remote_file, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText204 = wx.StaticText(self.m_panel17, wx.ID_ANY, u"remote sources dir:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText204.Wrap(-1)
        fgSizer363.Add(self.m_staticText204, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_remote_dir = wx.TextCtrl(self.m_panel17, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize,
                                        wx.TE_PROCESS_ENTER)
        fgSizer363.Add(self.m_remote_dir, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer269.Add(fgSizer363, 1, wx.EXPAND | wx.BOTTOM, 5)

        self.m_panel17.SetSizer(fgSizer269)
        self.m_panel17.Layout()
        fgSizer269.Fit(self.m_panel17)
        self.m_auinotebook1.AddPage(self.m_panel17, u"remote", False)
        self.m_panel3 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer8 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer8.AddGrowableCol(0)
        fgSizer8.AddGrowableRow(0)
        fgSizer8.SetFlexibleDirection(wx.BOTH)
        fgSizer8.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_environment_varsChoices = []
        self.m_environment_vars = wx.CheckListBox(self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                  m_environment_varsChoices, 0)
        fgSizer8.Add(self.m_environment_vars, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer9 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer9.SetFlexibleDirection(wx.BOTH)
        fgSizer9.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_btn_add = wx.BitmapButton(self.m_panel3, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_BUTTON),
                                         wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer9.Add(self.m_btn_add, 0, wx.ALL, 5)

        self.m_btn_delete = wx.BitmapButton(self.m_panel3, wx.ID_ANY,
                                            wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_BUTTON), wx.DefaultPosition,
                                            wx.DefaultSize, wx.BU_AUTODRAW)
        self.m_btn_delete.Enable(False)

        fgSizer9.Add(self.m_btn_delete, 0, wx.ALL, 5)

        fgSizer8.Add(fgSizer9, 1, wx.EXPAND, 5)

        self.m_panel3.SetSizer(fgSizer8)
        self.m_panel3.Layout()
        fgSizer8.Fit(self.m_panel3)
        self.m_auinotebook1.AddPage(self.m_panel3, u"environment", False)

        fgSizer200.Add(self.m_auinotebook1, 1, wx.ALL | wx.EXPAND, 5)

        self.SetSizer(fgSizer200)
        self.Layout()

        # Connect Events
        self.m_button58.Bind(wx.EVT_BUTTON, self.on_manage_configs)
        self.m_debug_type.Bind(wx.EVT_CHOICE, self.on_debug_type_changed)
        self.m_local_file.Bind(wx.EVT_FILEPICKER_CHANGED, self.on_local_file_changed)
        self.m_local_dir.Bind(wx.EVT_DIRPICKER_CHANGED, self.on_local_dir_changed)
        self.m_debug_arguments.Bind(wx.EVT_TEXT, self.on_debug_arguments_changed)
        self.m_debug_arguments.Bind(wx.EVT_TEXT_ENTER, self.on_debug_arguments_changed)
        self.m_remote_host.Bind(wx.EVT_TEXT, self.on_host_changed)
        self.m_remote_host.Bind(wx.EVT_TEXT_ENTER, self.on_host_enter)
        self.m_remote_port.Bind(wx.EVT_TEXT, self.on_port_changed)
        self.m_remote_port.Bind(wx.EVT_TEXT_ENTER, self.on_port_enter)
        self.m_remote_user.Bind(wx.EVT_TEXT, self.on_user_changed)
        self.m_remote_user.Bind(wx.EVT_TEXT_ENTER, self.on_user_enter)
        self.m_remote_password.Bind(wx.EVT_TEXT, self.on_password_changed)
        self.m_remote_password.Bind(wx.EVT_TEXT_ENTER, self.on_password_enter)
        self.m_button_test_connection.Bind(wx.EVT_BUTTON, self.on_verify_connection)
        self.m_remote_file.Bind(wx.EVT_TEXT, self.on_remote_file_changed)
        self.m_remote_file.Bind(wx.EVT_TEXT_ENTER, self.on_remote_file_enter)
        self.m_remote_dir.Bind(wx.EVT_TEXT, self.on_remote_dir_changed)
        self.m_remote_dir.Bind(wx.EVT_TEXT_ENTER, self.on_remote_dir_enter)
        self.m_environment_vars.Bind(wx.EVT_LISTBOX, self.on_environment_selected)
        self.m_environment_vars.Bind(wx.EVT_LISTBOX_DCLICK, self.on_edit_environment)
        self.m_btn_add.Bind(wx.EVT_BUTTON, self.on_add_environment)
        self.m_btn_delete.Bind(wx.EVT_BUTTON, self.on_delete_environment)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_manage_configs(self, event):
        event.Skip()

    def on_debug_type_changed(self, event):
        event.Skip()

    def on_local_file_changed(self, event):
        event.Skip()

    def on_local_dir_changed(self, event):
        event.Skip()

    def on_debug_arguments_changed(self, event):
        event.Skip()

    def on_host_changed(self, event):
        event.Skip()

    def on_host_enter(self, event):
        event.Skip()

    def on_port_changed(self, event):
        event.Skip()

    def on_port_enter(self, event):
        event.Skip()

    def on_user_changed(self, event):
        event.Skip()

    def on_user_enter(self, event):
        event.Skip()

    def on_password_changed(self, event):
        event.Skip()

    def on_password_enter(self, event):
        event.Skip()

    def on_verify_connection(self, event):
        event.Skip()

    def on_remote_file_changed(self, event):
        event.Skip()

    def on_remote_file_enter(self, event):
        event.Skip()

    def on_remote_dir_changed(self, event):
        event.Skip()

    def on_remote_dir_enter(self, event):
        event.Skip()

    def on_environment_selected(self, event):
        event.Skip()

    def on_edit_environment(self, event):
        event.Skip()

    def on_add_environment(self, event):
        event.Skip()

    def on_delete_environment(self, event):
        event.Skip()


###########################################################################
## Class EnvironmentDialogBase
###########################################################################

class EnvironmentDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"new environment", pos=wx.DefaultPosition,
                           size=wx.Size(350, 196), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer10 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer10.AddGrowableCol(0)
        fgSizer10.AddGrowableRow(0)
        fgSizer10.SetFlexibleDirection(wx.BOTH)
        fgSizer10.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer12 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer12.AddGrowableCol(1)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText13 = wx.StaticText(self, wx.ID_ANY, u"name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText13.Wrap(-1)
        fgSizer12.Add(self.m_staticText13, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl8 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer12.Add(self.m_textCtrl8, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText14 = wx.StaticText(self, wx.ID_ANY, u"value:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText14.Wrap(-1)
        fgSizer12.Add(self.m_staticText14, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl9 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer12.Add(self.m_textCtrl9, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer10.Add(fgSizer12, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizerNA4 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizerNA4.AddGrowableCol(0)
        fgSizerNA4.SetFlexibleDirection(wx.BOTH)
        fgSizerNA4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_HELP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizerNA4.Add(self.m_info, 0, wx.ALL, 5)

        m_std_buttons = wx.StdDialogButtonSizer()
        self.m_std_buttonsOK = wx.Button(self, wx.ID_OK)
        m_std_buttons.AddButton(self.m_std_buttonsOK)
        self.m_std_buttonsCancel = wx.Button(self, wx.ID_CANCEL)
        m_std_buttons.AddButton(self.m_std_buttonsCancel)
        m_std_buttons.Realize();

        fgSizerNA4.Add(m_std_buttons, 1, wx.EXPAND, 5)

        fgSizer10.Add(fgSizerNA4, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer10)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_std_buttonsCancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_std_buttonsOK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()
