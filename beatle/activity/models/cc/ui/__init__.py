# -*- coding: utf-8 -*-
from . import dlg
from . import pane
from . import ctrl


def init():
    """Initialize this activity model ui"""
    from beatle.activity.models.ui.view import ModelsView
    from beatle.model.cc import Inheritance, Friendship, RelationFrom, RelationTo, ClassDiagram,\
        Type, Folder, Library, Namespace, Class, Enum, MemberData, InitMethod, ExitMethod,\
        IsClassMethod, Constructor, MemberMethod, GetterMethod, SetterMethod, Argument, Destructor,\
        Module, Function, Data, TypesFolder

    ModelsView.tree_order.extend([
            Inheritance, Friendship, RelationFrom, RelationTo,
            ClassDiagram, Type, Folder, Library, Namespace, Class, Enum, MemberData,
            InitMethod, ExitMethod, IsClassMethod, Constructor, MemberMethod,  
            GetterMethod, SetterMethod, Argument, Destructor, Module, Function,
            Data, 
            # LibrariesFolder,   # disable the insertion of na�f, inmature
            TypesFolder])

    ModelsView.alias_map.update({
        Class: "c++ class {0}", 
        Inheritance: "c++ inheritance {0}",
        Argument: "c++ method argument {0}", 
        Constructor: "c++ constructor {0}",
        Destructor: "c++ destructor {0}", 
        IsClassMethod: "c++ method {0}",
        Friendship: "friend {0}", 
        MemberData: "member {0}",
        MemberMethod: "c++ method {0}", 
        SetterMethod: "c++ setter method {0}",             
        GetterMethod: "c++ getter method {0}",  
        ClassDiagram: "c++ class diagram {0}",
        Folder: "folder {0}", 
        RelationFrom: "c++ relation {0}", 
        RelationTo: "c++ relation {0}",
        Namespace: "c++ namespace {0}", 
        Type: "c++ type {0}",
        Enum: "c++ enum {0}", 
        Module: 'c++ module "{0}"',
        Function: "c++ method {0}", 
        #Library: "c++ library {0}",  # disable the insertion of na�f, inmature 
        Data: 'c++ variable {0}'})
    dlg.init()
