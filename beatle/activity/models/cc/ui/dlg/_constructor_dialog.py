# -*- coding: utf-8 -*-
"""
Module containing classes definitions
"""

import wx
import beatle.lib.handlers
from beatle.lib import wxx
import wx.richtext
from beatle.lib.api import volatile


class ConstructorDialog(wxx.Dialog):
    """
    This dialog allows to create a new class constructor and\nhis attributes. The preferred constructor means that this\nconstructor will be chosen by default for the list of the\nrequired arguments of derived classes and creating the\ninit sections of these classes.
    """
    def on_init_dialog(self, event):
        """
        Event handler
        """
        from beatle.app import resources as rc
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap("constructor"))
        self.SetIcon(icon)

    def on_toggle_calling_convention(self, event):
        """
        Event handler
        """
        self._use_calling_convention = self._check_box_calling_convention.IsChecked()
        self._combo_box_calling_convention.Enable(self._use_calling_convention)

    def on_toggle_template(self, event):
        """
        Event handler
        """
        if self._check_box_template.IsChecked():
            self._static_text_template_open.Enable(True)
            self._edit_text_template.Enable(True)
            self._static_text_template_close.Enable(True)
            self._check_box_preferred.SetValue(False)
            self._check_box_preferred.Enable(False)
        else:
            self._static_text_template_open.Enable(False)
            self._edit_text_template.Enable(False)
            self._static_text_template_close.Enable(False)
            self._check_box_preferred.Enable(True)
        event.Skip()

    def on_change_template_specification(self, event):
        """
        Event handler
        """
        text = self._edit_text_template.GetValue()
        template_items = text.split(',')
        prologues = ['class', 'typename']
        template_types = []
        for u in template_items:
            v = u.split('=')[0].strip()
            for p in prologues:
                if v.find(p) == 0:
                    v = v.replace(p, '').strip()
                    if len(v) and v not in self._nested_template_types:
                        template_types.append(v)
                    break
        if len(template_types) > 0:
            self._template_types = template_types
        event.Skip()

    def on_toggle_deleted(self, event):
        """
        Event handler
        """
        if self._check_box_deleted.IsChecked():
            self._check_box_implement.SetValue(False)
            self._check_box_implement.Enable(False)
        else:
            self._check_box_implement.Enable(True)

    def on_ok(self, event):
        """
        Event handler
        """
        if self.validate():
            volatile.set(type(self), self.GetRect())
            self.EndModal(wx.ID_OK)

    def on_cancel(self, event):
        """
        Event handler
        """
        volatile.set(type(self), self.GetRect())
        self.EndModal(wx.ID_CANCEL)

    def copy_attributes(self, ctor):
        """
        Transfer attributes to ctor
        """
        ctor._name = self._name
        ctor._access = self._access
        ctor._explicit = self._explicit
        ctor._inline = self._inline
        ctor._declare = self._declare
        ctor._implement = self._implement
        ctor._deleted = self._deleted
        ctor._calling_convention = self._calling_convention
        ctor.note = self._note
        ctor._template = self._template
        ctor._template_types = self._template_types
        ctor.set_preferred(self._preferred)

    def set_attributes(self, ctor):
        """
        Transfer attributes from existing constructor
        """
        self._container = ctor.parent
        self._class = ctor.inner_class
        self._name = ctor.name
        self._access = ctor.access
        iSel = self._choice_access.FindString(self._access)
        self._choice_access.Select(iSel)
        self._explicit = ctor._explicit
        self._check_box_explicit.SetValue(self._explicit)
        self._declare = ctor._declare
        self._check_box_declare.SetValue(self._declare)
        self._implement = ctor._implement
        self._check_box_implement.SetValue(self._implement)
        self._inline = ctor._inline
        self._check_box_inline.SetValue(self._inline)
        self._calling_convention = ctor._calling_convention
        self._use_calling_convention = self._calling_convention is not None
        self._check_box_calling_convention.SetValue(self._use_calling_convention)
        self._combo_box_calling_convention.Enable(self._use_calling_convention)
        if self._use_calling_convention:
            self._combo_box_calling_convention.SetValue(self._calling_convention)
        self._deleted = getattr(ctor,'_deleted', False)
        self._check_box_deleted.SetValue(self._deleted)  # to reduce in next releases
        self._note = ctor.note
        self._rich_text_note.SetValue(self._note)
        self._preferred = ctor.is_preferred()
        self._template = ctor._template
        self._template_types = ctor._template_types
        pref = self._class.get_preferred_constructor()
        if pref is None or pref is ctor:
            self._check_box_preferred.Enable(True)
        self._check_box_preferred.SetValue(self._preferred)
        if self._template :
            self._check_box_template.SetValue(True)
            self._static_text_template_open.Enable(True)
            self._edit_text_template.Enable(True)
            self._static_text_template_close.Enable(True)
            self._check_box_preferred.SetValue(False)
            self._check_box_preferred.Enable(False)
            self._edit_text_template.SetValue(self._template)
        
        
        self.SetTitle("Edit constructor")

    def validate(self):
        """
        Validation
        """
        self._name = self._class.name
        access_index = self._choice_access.GetSelection()
        self._access = self._choice_access.GetString(access_index)
        self._declare = self._check_box_declare.IsChecked()
        self._explicit = self._check_box_explicit.IsChecked()
        self._inline = self._check_box_inline.IsChecked()
        self._implement = self._check_box_implement.IsChecked()
        self._deleted = self._check_box_deleted.IsChecked()
        if self._use_calling_convention:
            self._calling_convention = self._combo_box_calling_convention.GetStringSelection()
        else:
            self._calling_convention = None
        if self._check_box_template.IsChecked():
            self._template = self._edit_text_template.GetValue()
        else:
            self._template = None
        self._note = self._rich_text_note.GetValue()
        self._preferred = self._check_box_preferred.IsChecked()
        return True

    def get_kwargs(self):
        """
        return arguments suitable for creation
        """
        return {
            'parent': self._container,
            'name': self._name,
            'access': self._access,
            'explicit': self._explicit,
            'inline': self._inline,
            'declare': self._declare,
            'implement': self._implement,
            'calling_convention': self._calling_convention,
            'note': self._note,
            'preferred': self._preferred,
            'autoargs': True,
            'deleted': self._deleted,
            'template': self._template,
            'template_types': self._template_types
        }

    def get_kwargs(self):
        """
        return arguments suitable for creation
        """
        return {
            'parent': self._container,
            'name': self._name,
            'access': self._access,
            'explicit': self._explicit,
            'inline': self._inline,
            'declare': self._declare,
            'implement': self._implement,
            'calling_convention': self._calling_convention,
            'note': self._note,
            'preferred': self._preferred,
            'autoargs': True,
            'deleted': self._deleted,
            'template': self._template,
            'template_types': self._template_types
        }

    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, container):
        """
        Constructor for the dialog window
        """
        self._flex_grid_sizer_1 = None
        self._static_box_sizer_1 = None
        self._static_box_sizer_2 = None
        self._flex_grid_sizer_2 = None
        self.m_info = None
        self._flex_grid_sizer_3 = None
        self._flex_grid_sizer_4 = None
        self._static_text_1 = None
        self._choice_access = None
        self._check_box_calling_convention = None
        self._combo_box_calling_convention = None
        self._check_box_template = None
        self._flex_grid_sizer_5 = None
        self._static_text_template_open = None
        self._edit_text_template = None
        self._static_text_template_close = None
        self._flex_grid_sizer_6 = None
        self._check_box_inline = None
        self._check_box_explicit = None
        self._check_box_declare = None
        self._check_box_implement = None
        self._check_box_preferred = None
        self._check_box_deleted = None
        self._cancel_button = None
        self._ok_button = None
        self._rich_text_note = None
        self._use_calling_convention = False
        self._calling_convention = None
        self._container = container
        self._class = container.inner_class
        self._template = None
        self._template_types = []
        self._nested_template_types = container.template_types
        super(ConstructorDialog,self).__init__(parent, id=-1, title = 'New constructor', pos=(-1, -1), size=(600, 500), style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER)
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.wxui_initialize_controls()
        self.wxui_connect_event_handlers()
        self.Layout()

    def wxui_initialize_controls(self):
        """
        This method is responsible for initialize the form layout
        """
        self._flex_grid_sizer_1 = wx.FlexGridSizer(3, 1, 0, 0)
        self._flex_grid_sizer_1.AddGrowableCol(0)
        self._flex_grid_sizer_1.AddGrowableRow(1)
        self._static_box_sizer_1 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Properties "), wx.VERTICAL)
        self._flex_grid_sizer_3 = wx.FlexGridSizer(1, 2, 0, 0)
        self._flex_grid_sizer_3.AddGrowableCol(0)
        self._flex_grid_sizer_4 = wx.FlexGridSizer(3, 2, 0, 0)
        self._flex_grid_sizer_4.AddGrowableCol(1)
        self._static_text_1 = wx.StaticText(self, wx.ID_ANY, "access", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_4.Add(self._static_text_1, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.LEFT| wx.TOP, 5)
        _choice_access_choices = ["public","protected","private"]
        self._choice_access = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, _choice_access_choices, 0)
        self._choice_access.SetSelection(0)
        self._flex_grid_sizer_4.Add(self._choice_access, 0, wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._check_box_calling_convention = wx.CheckBox(self, wx.ID_ANY, "calling", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE|wx.ALIGN_RIGHT)
        self._check_box_calling_convention.SetValue(False)
        self._flex_grid_sizer_4.Add(self._check_box_calling_convention, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.LEFT| wx.TOP, 5)
        self._combo_box_calling_convention = wx.ComboBox(self, wx.ID_ANY, "", wx.DefaultPosition,wx.DefaultSize, ["cdecl", "stdcall", "fastcall", "thiscall", "naked", "ms_abi", "sysv_abi", "interrupt"],  0)
        self._combo_box_calling_convention.Enable(False)
        self._flex_grid_sizer_4.Add(self._combo_box_calling_convention, 0, wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._check_box_template = wx.CheckBox(self, wx.ID_ANY, "template", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE|wx.ALIGN_RIGHT)
        self._check_box_template.SetValue(False)
        self._flex_grid_sizer_4.Add(self._check_box_template, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_RIGHT| wx.BOTTOM| wx.LEFT| wx.TOP, 5)
        self._flex_grid_sizer_5 = wx.FlexGridSizer(1, 3, 0, 0)
        self._flex_grid_sizer_5.AddGrowableCol(1)
        self._static_text_template_open = wx.StaticText(self, wx.ID_ANY, "<", wx.DefaultPosition,wx.DefaultSize, 0)
        self._static_text_template_open.Enable(False)
        self._flex_grid_sizer_5.Add(self._static_text_template_open, 0, wx.ALIGN_CENTER_VERTICAL| wx.BOTTOM| wx.RIGHT| wx.TOP, 5)
        self._edit_text_template = wx.TextCtrl(self, wx.ID_ANY, "", wx.DefaultPosition,wx.DefaultSize, 0)
        self._edit_text_template.Enable(False)
        self._flex_grid_sizer_5.Add(self._edit_text_template, 0, wx.ALIGN_CENTER_VERTICAL| wx.BOTTOM| wx.EXPAND| wx.TOP, 5)
        self._static_text_template_close = wx.StaticText(self, wx.ID_ANY, ">", wx.DefaultPosition,wx.DefaultSize, 0)
        self._static_text_template_close.Enable(False)
        self._flex_grid_sizer_5.Add(self._static_text_template_close, 0, wx.ALIGN_CENTER_VERTICAL| wx.ALIGN_LEFT| wx.BOTTOM| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_4.Add(self._flex_grid_sizer_5, 0, wx.EXPAND| wx.RIGHT, 5)
        self._flex_grid_sizer_3.Add(self._flex_grid_sizer_4, 0, wx.EXPAND, 5)
        self._flex_grid_sizer_6 = wx.FlexGridSizer(3, 2, 0, 0)
        self._check_box_inline = wx.CheckBox(self, wx.ID_ANY, "inline", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._check_box_inline.SetValue(False)
        self._flex_grid_sizer_6.Add(self._check_box_inline, 0, wx.TOP, 5)
        self._check_box_explicit = wx.CheckBox(self, wx.ID_ANY, "explicit", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._check_box_explicit.SetValue(False)
        self._flex_grid_sizer_6.Add(self._check_box_explicit, 0, wx.RIGHT| wx.TOP, 5)
        self._check_box_declare = wx.CheckBox(self, wx.ID_ANY, "declare", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._check_box_declare.SetValue(True)
        self._flex_grid_sizer_6.Add(self._check_box_declare, 0, wx.TOP, 5)
        self._check_box_implement = wx.CheckBox(self, wx.ID_ANY, "implement", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._check_box_implement.SetValue(True)
        self._flex_grid_sizer_6.Add(self._check_box_implement, 0, wx.RIGHT| wx.TOP, 5)
        self._check_box_preferred = wx.CheckBox(self, wx.ID_ANY, "preferred", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._check_box_preferred.SetValue(False)
        self._flex_grid_sizer_6.Add(self._check_box_preferred, 0, wx.TOP, 5)
        self._check_box_deleted = wx.CheckBox(self, wx.ID_ANY, "deleted", wx.DefaultPosition,wx.DefaultSize, wx.CHK_2STATE)
        self._check_box_deleted.SetValue(False)
        self._flex_grid_sizer_6.Add(self._check_box_deleted, 0, wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_3.Add(self._flex_grid_sizer_6, 0, 0, 5)
        self._static_box_sizer_1.Add(self._flex_grid_sizer_3, 0, wx.EXPAND, 5)
        self._flex_grid_sizer_1.Add(self._static_box_sizer_1, 0, wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 10)
        self._static_box_sizer_2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Notes"), wx.VERTICAL)
        self._rich_text_note = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.richtext.RE_CENTER_CARET|wx.HSCROLL| wx.NO_BORDER| wx.VSCROLL| wx.WANTS_CHARS)
        self._static_box_sizer_2.Add(self._rich_text_note, 1, wx.BOTTOM| wx.EXPAND| wx.RIGHT, 5)
        self._flex_grid_sizer_1.Add(self._static_box_sizer_2, 0, wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 10)
        self._flex_grid_sizer_2 = wx.FlexGridSizer(1, 3, 0, 0)
        self._flex_grid_sizer_2.AddGrowableCol(0)
        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_2.Add(self.m_info, 0, wx.BOTTOM| wx.LEFT| wx.TOP, 5)
        self._cancel_button = wx.Button(self, wx.ID_CANCEL, "", wx.DefaultPosition,wx.DefaultSize, 0)
        self._flex_grid_sizer_2.Add(self._cancel_button, 0, wx.BOTTOM| wx.TOP, 5)
        self._ok_button = wx.Button(self, wx.ID_OK, "", wx.DefaultPosition,wx.DefaultSize, 0)
        self._ok_button.SetDefault()
        self._flex_grid_sizer_2.Add(self._ok_button, 0, wx.BOTTOM| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self._flex_grid_sizer_1.Add(self._flex_grid_sizer_2, 0, wx.BOTTOM| wx.EXPAND| wx.LEFT| wx.RIGHT| wx.TOP, 5)
        self.SetSizer(self._flex_grid_sizer_1)
        self.Layout()

    def wxui_connect_event_handlers(self):
        """
        This method is responsible for connect the event handlers
        """
        self.Bind(wx.EVT_INIT_DIALOG,self.on_init_dialog)
        self._check_box_calling_convention.Bind(wx.EVT_CHECKBOX,self.on_toggle_calling_convention)
        self._check_box_template.Bind(wx.EVT_CHECKBOX,self.on_toggle_template)
        self._edit_text_template.Bind(wx.EVT_TEXT,self.on_change_template_specification)
        self._check_box_deleted.Bind(wx.EVT_CHECKBOX,self.on_toggle_deleted)
        self._cancel_button.Bind(wx.EVT_BUTTON,self.on_cancel)
        self._ok_button.Bind(wx.EVT_BUTTON,self.on_ok)

    def wxui_disconnect_event_handlers(self):
        """
        This method is responsible for disconnect the event handlers
        """
        self.Unbind(wx.EVT_INIT_DIALOG,self.on_init_dialog)
        self._check_box_calling_convention.Unbind(wx.EVT_CHECKBOX,self.on_toggle_calling_convention)
        self._check_box_template.Unbind(wx.EVT_CHECKBOX,self.on_toggle_template)
        self._edit_text_template.Unbind(wx.EVT_TEXT,self.on_change_template_specification)
        self._check_box_deleted.Unbind(wx.EVT_CHECKBOX,self.on_toggle_deleted)
        self._cancel_button.Unbind(wx.EVT_BUTTON,self.on_cancel)
        self._ok_button.Unbind(wx.EVT_BUTTON,self.on_ok)

    def delete(self):
        """
        Destructor for the frame window
        """
        self.wxui_disconnect_event_handlers()


