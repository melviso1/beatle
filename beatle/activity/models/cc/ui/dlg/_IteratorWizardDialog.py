import wx

from beatle import model
from beatle.lib import wxx
from beatle.lib.utils import cached_type
from .._base import IteratorWizardDialogBase


# Implementing IteratorWizardDialog
class IteratorWizardDialog(IteratorWizardDialogBase):
    """This wizard allows to generate fast code for iteration over known relations in the model."""

    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, context):
        import beatle.app.resources as rc
        self._context = context  # provide information for iterators
        self._source_class = None
        self._source_relation = None
        self._filter_method = None
        super(IteratorWizardDialog, self).__init__(parent)
        self.image_list = rc.get_bitmap_image_list()
        self.m_source_tree.SetImageList(self.image_list)
        self.FillSource()

    @property
    def selected_relation(self):
        index = self.m_relation_list.GetSelection()
        if index == wx.NOT_FOUND:
            return None
        return self.m_relation_list.GetClientData(index)

    @property
    def selected_filter(self):
        index = self.m_filter_list.GetSelection()
        if index == wx.NOT_FOUND:
            return None
        return self.m_filter_list.GetClientData(index)

    def FillSource(self):
        """Fills the tree with info about relations"""
        from beatle.model.cc import RelationTo, Member
        self._root = self.m_source_tree.AddRoot(wx.EmptyString)
        if isinstance(self._context, Member):  # realmente solo puede ser ctor, dtor o metodo
            if not getattr(self._context, '_static', False):
                # we must insert this element
                parent_class = self._context.inner_class
                treeid = self.m_source_tree.AppendItem(self._root, "this", image=parent_class.bitmap_index)
                self.m_source_tree.SetItemData(treeid, wxx.TreeItemData(parent_class))
        # self.m_source_tree.SetItemHasChildren(treeid, True)
        # check about project classes with static from side
        if hasattr(self._context, 'project'):
            statics = [x for x in self._context.project.level_classes if len(x(RelationTo,
                                                                               filter=lambda
                                                                                   z: z.inner_class == x and z._key._static and not z._key._single)) > 0]
            for element in statics:
                treeid = self.m_source_tree.AppendItem(self._root, "{}::".format(element.name),
                                                       image=element.bitmap_index)
                self.m_source_tree.SetItemData(treeid, wxx.TreeItemData(element))

    # Handlers for NewIteratorWizard events.
    def OnChangedSource(self, event):
        from beatle.model.cc import RelationTo
        treeitem = event.GetItem()  # the new tree item
        # lo primero que hay que hacer es verificar si existen relaciones en el elemento
        # seleccionado y rellenar el listbox de relaciones
        self.m_relation_list.Clear()
        self.m_ok.Disable()
        self.m_textCtrl62.SetValue(wx.EmptyString)
        self._source_class = self.m_source_tree.GetItemData(treeitem).get_data()

        def instance_relation_filter(class_instance):
            """Return a instance relation filter constructor"""

            def filter_builder(relation_to):
                """filter constructor"""
                return not relation_to.key.is_single and relation_to.inner_class is class_instance

            return filter_builder

        def static_relation_filter(class_instance):
            """Return a static relation filter constructor"""

            def filter_builder(relation_to):
                """filter constructor"""
                return relation_to.key.is_static and (not relation_to.key.is_single) \
                       and (relation_to.inner_class is class_instance)

            return filter_builder

        if self.m_source_tree.GetItemText(treeitem) == 'this':
            assert (self._source_class is self._context.inner_class)
            relation_filter = instance_relation_filter
        else:
            relation_filter = static_relation_filter

        # first, add bases
        for base in self._source_class.bases:
            relations = base(RelationTo, filter=relation_filter(base))
            for item in relations:
                self.m_relation_list.Append(item.name, item)
        # now, the proper instance
        relations = self._source_class(RelationTo, filter=relation_filter(self._source_class))
        for item in relations:
            self.m_relation_list.Append(item.name, item)

    def OnChangedRelation(self, event):
        """
        This is called when any relation is selected. Then, the filter scan
        must fill the available filter list.
        """
        # A bug for fixing there(?):
        # When selected a relation, the filters are extracted from the
        # associated target class. We must also seach for the base clases
        # Improvements
        # Esto es mas para las relaciones que para aqui: La implementacion de C++
        # de las relaciones es absolutamente penosa a nivel de herencia.
        # A una relacion ya establecida se le debe de poder añadir un sistema
        # de "mascarade" para representar restricciones basadas en ella.
        # Esto nos ofrece un sistema con economía relacional y la posiblidad
        # de implementar condiciones no triviales en las relaciones:
        # Por ejemplo, sería posible definir tipos de relaciones viculadas
        # como A->B <=> C->B
        # Este tipo de restricciones pueden ser muy utilies en AIRD

        self.m_filter_list.Clear()
        self._source_relation = self.selected_relation
        if self._source_relation is not None:
            target_class = self._source_relation.key.to_class
            # we must iterate over all the member methods that are type bool
            # and also const methods, without any argument
            bool_type = cached_type(target_class.project, 'bool')

            def filter_method(x):
                return x.is_const_method and x.type_instance.type is bool_type and len(x[model.cc.Argument]) == 0

            method_classes = (model.cc.MemberMethod, model.cc.IsClassMethod, model.cc.GetterMethod)
            available_filters = target_class(*method_classes, filter=filter_method)
            for callback in available_filters:
                self.m_filter_list.Append('{base}::{method}()'.format(
                    base=target_class.scoped, method=callback.name), callback)

        self.m_textCtrl62.SetValue('iter_{}'.format(self._source_relation.name))
        self.m_ok.Enable()

    def OnChangedFilter(self, event):
        """Called when the user selects a filter"""
        self._filter_method = self.selected_filter

    def validate(self):
        iterators = self._source_relation(model.cc.Class)
        if len(iterators) < 1:
            return False
        self.iterator_class = iterators[0]
        self.iterator_name = self.m_textCtrl62.GetValue().strip()
        if ' ' in self.iterator_name or '\t' in self.iterator_name:
            return False
        return True

    @property
    def code(self):
        """Return code for insert"""
        if self.m_checkBox97.GetValue():
            result = '{}.reset();\n'.format(self.iterator_name)
        else:
            if self._source_relation.key.is_static:
                result = '{cls}::{it} {name};\n'.format(
                    cls=self._source_class.name,
                    it=self.iterator_class.name,
                    name=self.iterator_name)
            else:
                if self._filter_method is None:
                    result = '{it} {name} = {it}(this);\n'.format(
                        cls=self._source_class.name,
                        it=self.iterator_class.name,
                        name=self.iterator_name)
                else:
                    filter_decl = '{fn.parent.scope}{fn._name}'.format(fn=self._filter_method)
                    result = '{it} {name} = {it}(this, {filter});\n'.format(
                        cls=self._source_class.name,
                        it=self.iterator_class.name,
                        name=self.iterator_name,
                        filter=filter_decl)
        if self.m_checkBox98.GetValue():
            result += 'while(--{name})\n{{\n    //put code here\n}}\n'.format(name=self.iterator_name)
        else:
            result += 'while(++{name})\n{{\n    //put code here\n}}\n'.format(name=self.iterator_name)
        return result

    @wxx.save_position
    def on_ok(self, event):
        # TODO: Implement on_ok
        if self.validate():
            self.EndModal(wx.ID_OK)

    @wxx.save_position
    def on_cancel(self, event):
        self.EndModal(wx.ID_CANCEL)

