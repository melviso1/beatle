"""Subclass of VirtualMethods, which is generated by wxFormBuilder."""

import wx

from beatle import model
from beatle.lib import wxx
from .._base import VirtualMethodsDialogBase


# Implementing VirtualMethods
class VirtualMethodsDialog(VirtualMethodsDialogBase ):
    """
    This dialog allows to add inherited virtual methods.
    """
    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, container):
        "initialization"
        super(VirtualMethodsDialog, self).__init__(parent)
        self.m_std_buttonsOK.SetDefault()
        # container es la clase destino
        self.container = container
        self.parent_class = container.inner_class
        # create a map of feasible casts
        self._classes = []
        self._methods = []
        for k in self.parent_class.inheritance:
            self.visit_class(k._ancestor)
        # scan the available virtual methods
        virtual = lambda x: x._virtual
        for cls in self._classes:
            members = lambda x: x.inner_class == cls
            self._methods.extend(filter(virtual,
                cls(model.cc.MemberMethod, filter=members, cut=True)))
        # create a map from method names to implementations
        self._map = {}
        members = lambda x: x.inner_class == self.parent_class
        for k in self._methods:
            s = k.label
            override = lambda x: x._virtual and x.label == s
            if s not in self._map:
                #find the method inside the class
                self._map[s] = (k,
                    list(filter(override, self.parent_class(model.cc.MemberMethod,
                        members, True))))
        # ok, do insertion remebering implementations
        pos = 0
        for k in self._map:
            v = self._map[k]
            self.m_checkList2.Insert(v[0].scoped_declaration, pos, v)
            if len(v[1]) > 0:
                self.m_checkList2.Check(pos)
            pos = pos + 1
        aTable = wx.AcceleratorTable([
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_RETURN, wx.ID_OK),
        #tree navigation facilities
        ])
        self.SetAcceleratorTable(aTable)

    def visit_class(self, k):
        """Add ancestor branch"""
        self._classes.append(k)
        for l in k.inheritance:
            #paranoid prevent cyclic inheritance
            if l._ancestor not in self._classes:
                self.visit_class(l._ancestor)

    def get_kwargs(self):
        """Returns kwargs dictionary suitable for objects creation"""
        kwargs_list = []
        for item in range(0, self.m_checkList2.GetCount()):
            v = self.m_checkList2.GetClientData(item)
            c = self.m_checkList2.IsChecked(item)
            if  (c and v[1]) or (not c and not v[1]):
                continue
            if c:
                kwargs = {}
                kwargs = v[0].get_kwargs()
                kwargs['parent'] = self.container
                kwargs['pure'] = False
                kwargs['declare'] = True
                kwargs['implement'] = True
                kwargs['read_only'] = False
                kwargs_list.append(kwargs)
            else:
                for m in v[1]:
                    m.delete()
        return kwargs_list

    @wxx.save_position
    def on_ok(self, event):
        """ok event handler"""
        self.EndModal(wx.ID_OK)

    @wxx.save_position
    def on_cancel(self, event):
        """cancel event handler"""
        self.EndModal(wx.ID_CANCEL)

