"""Subclass of EnvironmentDialogBase, which is generated by wxFormBuilder."""

import wx
from beatle.lib import wxx
from .._debug_setting_base import EnvironmentDialogBase


class EnvironmentDialog(EnvironmentDialogBase):
	"""This class implements a small dialog for environment edition"""

	@wxx.SetInfo(__doc__)
	@wxx.restore_position
	def __init__(self, parent, project):
		self._project = project
		self._name = ''
		self._value = ''
		super(EnvironmentDialog, self).__init__(parent)

	def set_attributes(self, obj):
		"""Transfer attributes to dialog"""
		self.SetTitle("Edit environment")
		self._name = obj.name
		self._value = obj.value
		self.m_textCtrl8.SetValue(self._name)
		self.m_textCtrl9.SetValue(self._value)

	def get_kwargs(self):
		"""return arguments suitable for object instance"""
		return {'name': self._name, 'value': self._value}

	def copy_attributes(self, obj):
		"""Copy attributes to environment"""
		obj.name = self._name
		obj.value = self._value

	def validate(self):
		self._name = self.m_textCtrl8.GetValue().strip()
		self._value = self.m_textCtrl9.GetValue().strip()
		if len(self._name) == 0:
			wx.MessageBox(
				"Environment name must not be blank or empty", "Error", wx.OK | wx.CENTER | wx.ICON_ERROR, self)
			return False
		return True

	@wxx.save_position
	def on_ok(self, event):
		if self.validate():
			self.EndModal(wx.ID_OK)

	@wxx.save_position
	def on_cancel(self, event):
		self.EndModal(wx.ID_CANCEL)


