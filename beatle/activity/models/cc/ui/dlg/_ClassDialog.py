# -*- coding: utf-8 -*-
"""
Module containing classes definitions
"""

import wx
import wx.richtext
from beatle.lib import wxx
from beatle import model
import re
from beatle.app import resources as rc
from beatle.lib import wxx


class ClassDialog(wxx.Dialog):
    """
    This dialog allows to create a new class or struct. Even as top
    element or nested in another project container. You can specify
    access for child classes and structs. Serialize flag indicates
    the creation and maintenance of serialization methods for the
    class.
    """

    @wxx.save_position
    def on_ok(self, event):
        """
        event handler
        """
        if self.validate():
            self.EndModal(wx.ID_OK)

    @wxx.save_position
    def on_cancel(self, event):
        """
        event handler
        """
        self.EndModal(wx.ID_CANCEL)

    def on_toggle_template(self, event):
        """
        event handler
        """
        if self._chk_is_template.GetValue():
            self._open_template_spec.Enable(True)
            self._edt_template_spec.Enable(True)
            self._close_template_spec.Enable(True)
            # for the moment, a template class is not serializable
            self._serialize_class = False
            self._transactional_class = False
            self._chk_serialize.SetValue(False)
            self._chk_serialize.Enable(False)
        else:
            self._open_template_spec.Enable(False)
            self._edt_template_spec.SetValue('')
            self._edt_template_spec.Enable(False)
            self._close_template_spec.Enable(False)
            # for the moment, only non-template classes can not serializable
            self._chk_serialize.Enable(True)
        event.Skip()

    def on_change_template_specification(self, event):
        """
        event handler
        """
        text = self._edt_template_spec.GetValue()
        terms = text.split(',')
        prologs = ['class', 'typename']
        r = []
        s = []
        for u in terms:
            v = u.split('=')[0].strip()
            added = False
            for p in prologs:
                if v.find(p) == 0:
                    v = v.replace(p, '').strip()
                    if len(v):
                        v = v.strip()
                        r.append(v)
                        s.append(v)
                        added = True
                    break
            if added:
                continue
            v = v.strip()
            v = v.split(' ')
            while v[-1] in ['const', 'volatile']:
                v = v[:-1]
            if len(v) < 2:  # must be a syntactic error
                continue
            v = ''.join(v[1:])
            # we must extract the argument name for a function pointer
            # specified as (*fn)(args)
            t = self.fn_template_arg.match(v)
            if t:
                s.append(t.group('name'))
                continue
            t = self.ptr_arg.match(v)
            if t:
                s.append(t.group('name'))
                continue

        # set templates to available types
        self._template_types = r
        self._template_arguments = s
        event.Skip()

    def on_toggle_external_class(self, event):
        """
        event handler
        """
        self._is_external = self._chk_is_external.GetValue()
        self._chk_is_external.Enable(not self._is_external)
        event.Skip()

    def on_serialize(self, event):
        """
        event handler
        """
        self._serialize_class = self._chk_serialize.GetValue()
        if self._serialize_class is False:
            self._chk_undo_redo.Enable(False)
        else:
            self._chk_undo_redo.Enable(self.project.support_transaction)

    def on_toogle_component(self, event):
        """
        event handler
        """
        # write here the event handler

    def on_choice_component(self, event):
        """
        event handler
        """
        # write here the event handler

    def on_toggle_enable_export_prefix(self, event):
        """
        event handler
        """
        self._edt_export_prefix.Enable(self._chk_enable_export_prefix.GetValue())

    def on_undo_redo(self, event):
        """
        event handler
        """
        self._transactional_class = self._chk_undo_redo.GetValue()

    def on_init_dialog(self, event):
        """
        event handler
        """
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap("class"))
        self.SetIcon(icon)
        self._edt_class_name.SetFocus()
        if self._container.inner_class:
            self._static_text_7.Enable()
            self._cmb_access.Enable()
        self._chk_serialize.Enable(self.project.support_serialization)
        self._chk_undo_redo.Enable(self.project.support_transaction)

    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, container):
        """
        Constructor for the frame window
        """
        self._flex_grid_sizer_2 = None
        self._flex_grid_sizer_3 = None
        self._static_box_sizer_1 = None
        self._static_text_1 = None
        self._edt_class_name = None
        self._edt_members_prefix = None
        self._cmb_access = None
        self._chk_is_template = None
        self._flex_grid_sizer_8 = None
        self._edt_template_spec = None
        self._close_template_spec = None
        self._txt_comments = None
        self._flex_grid_sizer_9 = None
        self.m_info = None
        self._button_1 = None
        self._button_2 = None
        self._open_template_spec = None
        self._chk_is_external = None
        self._flex_grid_sizer_14 = None
        self._static_box_sizer_6 = None
        self._static_box_sizer_7 = None
        self._flex_grid_sizer_20 = None
        self._chk_serialize = None
        self._chk_component = None
        self._chk_undo_redo = None
        self._chc_component = None
        self._flex_grid_sizer_21 = None
        self._chk_enable_export_prefix = None
        self._edt_export_prefix = None
        self._grid_sizer_5 = None
        self._chk_is_struct = None
        self._chk_replace_constructor = None
        self._static_text_7 = None
        self._static_text_3 = None
        self._container = container
        self.project = container.project
        self._original = ""
        self._template = None
        self._template_types = []
        self._template_arguments = []
        self._is_external = False
        self._name = ''
        self._memberPrefix = '_'
        self._export_prefix = ''
        self._serialize_class = False
        self._transactional_class = False
        self._note = '<Place a description here>'
        self._is_struct = False
        self._access = 'public'
        self.fn_template_arg = re.compile(r'\((\*+)(?P<name>[^\)]+)\)\s*\([^\)]*\)')
        self.ptr_arg = re.compile(r'(\*+)(?P<name>[^\s]+).*')
        super(ClassDialog, self).__init__(parent, id=-1, title='New class', pos=wx.DefaultPosition,
                                          size=wx.Size(595, 650), style=wx.CAPTION | wx.DEFAULT_DIALOG_STYLE)
        self.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT))
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.wxui_initialize_controls()
        self.wxui_connect_event_handlers()
        self.Centre(wx.BOTH)

    def wxui_initialize_controls(self):
        """
        This method is responsible for initialize the form layout
        """
        self._flex_grid_sizer_2 = wx.FlexGridSizer(4, 1, 0, 0)
        self._flex_grid_sizer_2.AddGrowableCol(0)
        self._flex_grid_sizer_2.AddGrowableRow(2)
        self._flex_grid_sizer_3 = wx.FlexGridSizer(5, 2, 0, 0)
        self._flex_grid_sizer_3.AddGrowableCol(1)
        self._chk_is_external = wx.CheckBox(self, wx.ID_ANY, "e&xtern", wx.DefaultPosition, wx.DefaultSize,
                                            wx.CHK_2STATE)
        self._chk_is_external.SetValue(False)
        self._chk_is_external.SetToolTip("don't generate sources")
        self._flex_grid_sizer_3.Add(self._chk_is_external, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 5)
        self._grid_sizer_5 = wx.GridSizer(1, 2, 0, 0)
        self._chk_is_struct = wx.CheckBox(self, wx.ID_ANY, "&struct", wx.DefaultPosition, wx.DefaultSize, wx.CHK_2STATE)
        self._chk_is_struct.SetValue(False)
        self._chk_is_struct.SetToolTip("declare as struct")
        self._grid_sizer_5.Add(self._chk_is_struct, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        self._chk_replace_constructor = wx.CheckBox(self, wx.ID_ANY, "&replace", wx.DefaultPosition, wx.DefaultSize,
                                                    wx.CHK_2STATE)
        self._chk_replace_constructor.SetValue(False)
        self._chk_replace_constructor.SetToolTip("create replacement constructor")
        self._chk_replace_constructor.Enable(False)
        self._grid_sizer_5.Add(self._chk_replace_constructor, 0, 0, 5)
        self._flex_grid_sizer_3.Add(self._grid_sizer_5, 0, wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)
        self._static_text_1 = wx.StaticText(self, wx.ID_ANY, "name :", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_3.Add(self._static_text_1, 0,
                                    wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP,
                                    5)
        self._edt_class_name = wx.TextCtrl(self, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self._edt_class_name.SetToolTip("name of the class")
        self._flex_grid_sizer_3.Add(self._edt_class_name, 0,
                                    wx.ALIGN_TOP | wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 5)
        self._chk_is_template = wx.CheckBox(self, wx.ID_ANY, "template", wx.DefaultPosition, wx.DefaultSize,
                                            wx.CHK_2STATE)
        self._chk_is_template.SetValue(False)
        self._chk_is_template.SetToolTip("declare a template")
        self._flex_grid_sizer_3.Add(self._chk_is_template, 0,
                                    wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 5)
        self._flex_grid_sizer_8 = wx.FlexGridSizer(1, 3, 0, 0)
        self._flex_grid_sizer_8.AddGrowableCol(1)
        self._open_template_spec = wx.StaticText(self, wx.ID_ANY, "<", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_8.Add(self._open_template_spec, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)
        self._edt_template_spec = wx.TextCtrl(self, wx.ID_ANY, "text", wx.DefaultPosition, wx.DefaultSize, 0)
        self._edt_template_spec.SetToolTip("template arguments")
        self._edt_template_spec.Enable(False)
        self._flex_grid_sizer_8.Add(self._edt_template_spec, 1, wx.ALIGN_CENTER_VERTICAL | wx.EXPAND | wx.LEFT, 5)
        self._close_template_spec = wx.StaticText(self, wx.ID_ANY, " >", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_8.Add(self._close_template_spec, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT, 5)
        self._flex_grid_sizer_3.Add(self._flex_grid_sizer_8, 0,
                                    wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM | wx.EXPAND | wx.RIGHT | wx.TOP, 5)
        self._static_text_3 = wx.StaticText(self, wx.ID_ANY, "members prefix :", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_3.Add(self._static_text_3, 0,
                                    wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP,
                                    5)
        self._edt_members_prefix = wx.TextCtrl(self, wx.ID_ANY, "_", wx.DefaultPosition, wx.DefaultSize, 0)
        self._edt_members_prefix.SetToolTip("prefix for members")
        self._flex_grid_sizer_3.Add(self._edt_members_prefix, 0,
                                    wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 5)
        self._static_text_7 = wx.StaticText(self, wx.ID_ANY, "access :", wx.DefaultPosition, wx.DefaultSize, 0)
        self._static_text_7.Enable(False)
        self._flex_grid_sizer_3.Add(self._static_text_7, 0,
                                    wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP,
                                    5)
        _cmb_access_choices = ["public", "protected", "private"]
        self._cmb_access = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, _cmb_access_choices, 0)
        self._cmb_access.SetSelection(0)
        self._cmb_access.SetToolTip("access (only inner classes)")
        self._cmb_access.Enable(False)
        self._flex_grid_sizer_3.Add(self._cmb_access, 0,
                                    wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 5)
        self._flex_grid_sizer_2.Add(self._flex_grid_sizer_3, 0, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 5)
        self._flex_grid_sizer_14 = wx.FlexGridSizer(1, 2, 0, 0)
        self._flex_grid_sizer_14.AddGrowableCol(0)
        self._flex_grid_sizer_14.AddGrowableCol(1)
        self._flex_grid_sizer_14.AddGrowableRow(0)
        self._static_box_sizer_6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "serialization"), wx.VERTICAL)
        self._flex_grid_sizer_20 = wx.FlexGridSizer(2, 2, 0, 0)
        self._flex_grid_sizer_20.AddGrowableCol(1)
        self._chk_serialize = wx.CheckBox(self, wx.ID_ANY, "serialize", wx.DefaultPosition, wx.DefaultSize,
                                          wx.CHK_2STATE)
        self._chk_serialize.SetValue(False)
        self._chk_serialize.SetToolTip("make this class serializable")
        self._chk_serialize.Enable(False)
        self._flex_grid_sizer_20.Add(self._chk_serialize, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        self._chk_undo_redo = wx.CheckBox(self, wx.ID_ANY, "undo/redo", wx.DefaultPosition, wx.DefaultSize,
                                          wx.CHK_2STATE)
        self._chk_undo_redo.SetValue(False)
        self._chk_undo_redo.SetToolTip("make this class transactional")
        self._chk_undo_redo.Enable(False)
        self._flex_grid_sizer_20.Add(self._chk_undo_redo, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        self._chk_component = wx.CheckBox(self, wx.ID_ANY, "doc component :", wx.DefaultPosition, wx.DefaultSize,
                                          wx.CHK_2STATE)
        self._chk_component.SetValue(False)
        self._chk_component.SetToolTip("declare this class as document element")
        self._chk_component.Enable(False)
        self._flex_grid_sizer_20.Add(self._chk_component, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        _chc_component_choices = []
        self._chc_component = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, _chc_component_choices, 0)
        self._chc_component.SetToolTip("choose document")
        self._flex_grid_sizer_20.Add(self._chc_component, 0, wx.EXPAND, 5)
        self._static_box_sizer_6.Add(self._flex_grid_sizer_20, 0, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP,
                                     5)
        self._flex_grid_sizer_14.Add(self._static_box_sizer_6, 1, wx.EXPAND | wx.LEFT | wx.RIGHT, 5)
        self._static_box_sizer_7 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "export prefix"), wx.VERTICAL)
        self._flex_grid_sizer_21 = wx.FlexGridSizer(2, 1, 0, 0)
        self._chk_enable_export_prefix = wx.CheckBox(self, wx.ID_ANY, "use export prefix", wx.DefaultPosition,
                                                     wx.DefaultSize, wx.CHK_2STATE)
        self._chk_enable_export_prefix.SetValue(False)
        self._chk_enable_export_prefix.SetToolTip("enable export prefix")
        self._flex_grid_sizer_21.Add(self._chk_enable_export_prefix, 0, 0, 5)
        self._edt_export_prefix = wx.TextCtrl(self, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self._edt_export_prefix.Enable(False)
        self._flex_grid_sizer_21.Add(self._edt_export_prefix, 0, wx.EXPAND, 5)
        self._static_box_sizer_7.Add(self._flex_grid_sizer_21, 0, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP,
                                     5)
        self._flex_grid_sizer_14.Add(self._static_box_sizer_7, 0, wx.EXPAND | wx.LEFT | wx.RIGHT, 5)
        self._flex_grid_sizer_2.Add(self._flex_grid_sizer_14, 0, wx.EXPAND | wx.LEFT | wx.RIGHT, 5)
        self._static_box_sizer_1 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Documentation"), wx.VERTICAL)
        self._txt_comments = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                      wx.DefaultSize,
                                                      wx.richtext.RE_CENTER_CARET | wx.HSCROLL | wx.NO_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        self._txt_comments.SetToolTip("class information")
        self._static_box_sizer_1.Add(self._txt_comments, 1, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 5)
        self._flex_grid_sizer_2.Add(self._static_box_sizer_1, 0, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP,
                                    10)
        self._flex_grid_sizer_9 = wx.FlexGridSizer(1, 3, 0, 0)
        self._flex_grid_sizer_9.AddGrowableCol(0)
        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_9.Add(self.m_info, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP,
                                    5)
        self._button_2 = wx.Button(self, wx.ID_CANCEL, "&Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        self._flex_grid_sizer_9.Add(self._button_2, 0, wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP, 5)
        self._button_1 = wx.Button(self, wx.ID_OK, "Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self._button_1.SetDefault()
        self._flex_grid_sizer_9.Add(self._button_1, 0, wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP, 5)
        self._flex_grid_sizer_2.Add(self._flex_grid_sizer_9, 1, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT, 5)
        self.SetSizer(self._flex_grid_sizer_2)
        self.Layout()

    def wxui_connect_event_handlers(self):
        """
        This method is responsible for connect the event handlers
        """
        self.Bind(wx.EVT_INIT_DIALOG, self.on_init_dialog)
        self._chk_is_external.Bind(wx.EVT_CHECKBOX, self.on_toggle_external_class)
        self._chk_is_template.Bind(wx.EVT_CHECKBOX, self.on_toggle_template)
        self._edt_template_spec.Bind(wx.EVT_TEXT, self.on_change_template_specification)
        self._chk_serialize.Bind(wx.EVT_CHECKBOX, self.on_serialize)
        self._chk_undo_redo.Bind(wx.EVT_CHECKBOX, self.on_undo_redo)
        self._chk_component.Bind(wx.EVT_CHECKBOX, self.on_toogle_component)
        self._chc_component.Bind(wx.EVT_CHOICE, self.on_choice_component)
        self._chk_enable_export_prefix.Bind(wx.EVT_CHECKBOX, self.on_toggle_enable_export_prefix)
        self._button_2.Bind(wx.EVT_BUTTON, self.on_cancel)
        self._button_1.Bind(wx.EVT_BUTTON, self.on_ok)

    def wxui_disconnect_event_handlers(self):
        """
        This method is responsible for disconnect the event handlers
        """
        self.Unbind(wx.EVT_INIT_DIALOG, self.on_init_dialog)
        self._chk_is_external.Unbind(wx.EVT_CHECKBOX, self.on_toggle_external_class)
        self._chk_is_template.Unbind(wx.EVT_CHECKBOX, self.on_toggle_template)
        self._edt_template_spec.Unbind(wx.EVT_TEXT, self.on_change_template_specification)
        self._chk_serialize.Unbind(wx.EVT_CHECKBOX, self.on_serialize)
        self._chk_undo_redo.Unbind(wx.EVT_CHECKBOX, self.on_undo_redo)
        self._chk_component.Unbind(wx.EVT_CHECKBOX, self.on_toogle_component)
        self._chc_component.Unbind(wx.EVT_CHOICE, self.on_choice_component)
        self._chk_enable_export_prefix.Unbind(wx.EVT_CHECKBOX, self.on_toggle_enable_export_prefix)
        self._button_2.Unbind(wx.EVT_BUTTON, self.on_cancel)
        self._button_1.Unbind(wx.EVT_BUTTON, self.on_ok)

    def delete(self):
        """
        Destructor for the frame window
        """
        self.wxui_disconnect_event_handlers();

    def validate(self):
        """
        validate the dialog before end modal ok
        """
        self._name = self._edt_class_name.GetValue()
        if len(self._name) == 0:
            wx.MessageBox("Class name must be non empty", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        if self._chk_is_template.GetValue():
            self._template = self._edt_template_spec.GetValue()
        else:
            self._template = None
            self._template_types = []
            self._template_arguments = []
        if self._template:
            if re.match(r"^[A-Za-z_][0-9A-Za-z_<>\b,]*$", self._name) is None:
                wx.MessageBox("class name contains invalid characters", "Error",
                              wx.OK | wx.CENTER | wx.ICON_ERROR, self)
                return False
        else:
            if re.match(r"^[A-Za-z_][0-9A-Za-z_]*$", self._name) is None:
                wx.MessageBox("class name contains invalid characters", "Error",
                              wx.OK | wx.CENTER | wx.ICON_ERROR, self)
                return False
        if not self._original == self._name:
            # Check name collision taking care of ambit
            owner_class = self._container.inner_class
            if owner_class is not None:
                level_classes = owner_class.level_classes
            else:
                # if our class is inside a namespace, we must check only direct namespace classes
                namespace = self._container.inner_namespace
                if namespace is not None:
                    level_classes = namespace.level_classes
                else:
                    level_classes = [x for x in self.project.level_classes if x.inner_namespace is None]
            if self._name in [x._name for x in level_classes]:
                wx.MessageBox("Class already exist", "Error",
                              wx.OK | wx.CENTER | wx.ICON_ERROR, self)
                return False
        self._is_struct = self._chk_is_struct.GetValue()
        self._note = self._txt_comments.GetValue()
        self._memberPrefix = self._edt_members_prefix.GetValue()
        if self._chk_enable_export_prefix.GetValue():
            self._export_prefix = self._edt_export_prefix.GetValue()
        else:
            self._export_prefix = ''
        self._access = self._cmb_access.GetStringSelection()
        return True

    def copy_attributes(self, cls):
        """
        Copy dialog selections to object.
        This method is only invoked when editing an existing class.
        """
        if not cls._is_external and self._is_external:
            if hasattr(cls, '_header_obj') and cls._header_obj:
                cls._header_obj.delete()
            if hasattr(cls, '_source_obj') and cls._source_obj:
                cls._source_obj.delete()
            cls._is_external = True
        else:
            cls._is_external = self._is_external
        if cls._name != self._name:
            # refactoring is needed
            cls.project.refactor_contents(cls._name, self._name)
        cls._name = self._name
        cls._is_struct = self._is_struct
        cls._note = self._note
        cls._memberPrefix = self._memberPrefix
        cls._export_prefix = self._export_prefix
        cls._template = self._template
        cls._template_types = self._template_types
        cls._template_arguments = self._template_arguments
        cls._access = self._access
        cls._serial = self._serialize_class
        cls._transactional = self._transactional_class

    def set_attributes(self, cls):
        """
        Copy attributes from object to dialog.
        """
        self.SetTitle("Edit class/struct")
        # backward compatibility code - to simplfy in newer versions
        if not hasattr(cls, '_is_external'):
            cls._is_external = False
        self._is_external = cls._is_external
        self._original = cls.name
        self._name = cls.name
        self._note = cls.note
        self._is_struct = cls.is_struct
        self._serialize_class = cls.is_serial
        self._transactional_class = cls.is_transactional
        self._memberPrefix = cls.member_prefix
        self._export_prefix = cls.export_prefix
        self._template = cls.template
        self._template_types = cls.template_types
        self._template_arguments = getattr(cls, '_template_arguments', cls._template_types)
        self._edt_class_name.SetValue(self._name)
        self._chk_is_struct.SetValue(self._is_struct)
        self._chk_serialize.SetValue(self._serialize_class)
        self._chk_undo_redo.SetValue(self._transactional_class)
        self._edt_members_prefix.SetValue(self._memberPrefix)
        self._edt_export_prefix.SetValue(self._export_prefix)
        self._chk_enable_export_prefix.SetValue(len(self._export_prefix) > 0)
        self._edt_export_prefix.Enable(len(self._export_prefix) > 0)
        self._txt_comments.SetValue(self._note)
        self._access = cls._access
        if cls.parent.inner_class:
            self._static_text_7.Enable()
            self._cmb_access.Enable()
            i = self._cmb_access.FindString(self._access)
            self._cmb_access.Select(i)

        if cls._template is not None:
            self._open_template_spec.Enable(True)
            self._edt_template_spec.Enable(True)
            self._close_template_spec.Enable(True)
            self._chk_is_template.SetValue(True)
            self._edt_template_spec.SetValue(self._template)
            # if template is specified and some contained method /argument holds
            # some of the template types, the template specification cannot be changed
            self.SetTitle("Edit class")
            for method in cls(model.cc.MemberMethod):
                if method._typei._type_alias in cls._template_types:
                    self._chk_is_template.Enable(False)
                    self._chk_is_template.SetToolTip(
                        'Template cannot be edited because some method is using this template types.')
                    self._open_template_spec.Enable(False)
                    self._edt_template_spec.Enable(False)
                    self._close_template_spec.Enable(False)
                    return
            for arg in cls(model.cc.Argument):
                if arg._typei._type_alias in cls._template_types:
                    self._chk_is_template.Enable(False)
                    self._chk_is_template.SetToolTip(
                        'Template cannot be edited because some argument is using this template types.')
                    self._open_template_spec.Enable(False)
                    self._edt_template_spec.Enable(False)
                    self._close_template_spec.Enable(False)
                    return
        self._chk_is_external.SetValue(self._is_external)

    def get_kwargs(self):
        """
        Returns suitable arguments for creating object
        """
        return {
            'parent': self._container,
            'name': self._name,
            'is_struct': self._is_struct,
            'serial': self._serialize_class,
            'transactional': self._transactional_class,
            'access': self._access,
            'template': self._template,
            'template_types': self._template_types,
            'template_arguments': self._template_arguments,
            'prefix': self._memberPrefix,
            'export_prefix': self._export_prefix,
            'note': self._note,
            'is_external': self._is_external
        }


