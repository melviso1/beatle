import copy, re

import wx

from beatle.lib import wxx
from beatle import model
from beatle.lib.api import context
from .._base import MemberMethodDialogBase


# Implementing MemberMethodDialog
class MemberMethodDialog(MemberMethodDialogBase):
    """
    This dialog allows to setup the attributes
    of a member function of struct or class.
    You can configure the method for not write
    his declaration and/or his implementation.
    """
    @wxx.restore_position
    @wxx.SetInfo(__doc__)
    def __init__(self, parent, container):
        """Initialization"""
        from beatle.app import resources as rc
        super(MemberMethodDialog, self).__init__(parent)
        self.m_checkBox11.Bind(wx.EVT_CHECKBOX, self.on_toggle_virtual)
        self.m_checkBox21.Bind(wx.EVT_CHECKBOX, self.on_toggle_pure)
        self.m_checkBox126.Bind(wx.EVT_CHECKBOX, self.on_toggle_deleted)
        self.container = container
        self._name = ''
        self._template = None
        self._typei = None
        self._access = 'public'
        self._static = False
        self._virtual = False
        self._pure = False
        self._inline = False
        self._const_method = False
        self._note = ''
        self._declare = True
        self._implement = True
        self._use_calling_convention = False
        self._calling_convention = None
        self._types = dict([getattr(x, 'scoped', x.name), x] for x in container.types)
        self.sorted_types = list(self._types.keys())
        self.sorted_types.sort()
        # add types but not on-the-fly template type
        self.m_type.AppendItems([x for x in self.sorted_types if x != '@'])

        self._template_types = []
        # we need to add types from template nested classes
        self._nested_template_types = container.template_types
        if len(self._nested_template_types) > 0:
            self.m_type.AppendItems(self._nested_template_types)
        self.m_type.SetSelection(self.m_type.FindString(''))
        self.choice_string = ""
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap("method"))
        self.SetIcon(icon)
        info = context.get_info('c++ member method dialog')
        if info:
            # Repeat user choices
            access = info.get('access', None)
            if access is not None:
                access_index = self.m_choice2.FindString(access)
                self.m_choice2.SetSelection(access_index)
        # attempt to adjust choice for bigger types
        type_width = self.m_type.GetRect().GetWidth()
        self.m_type.InvalidateBestSize()
        best_type_size = self.m_type.GetBestSize()
        if best_type_size.GetWidth() > type_width:
            offset = best_type_size.GetWidth() - type_width
            from beatle.lib.api import volatile
            rect = self.GetRect()
            rect.Inflate(offset, 0)
            self.SetRect(rect)
            self.m_type.SetSize(best_type_size)
            self.Layout()
            volatile.set(type(self), rect)

        wx.YieldIfNeeded()
        self.m_type.SetFocus()
        self.m_type.SetSelection(0)

    def on_toggle_virtual(self, event):
        """Handle toggle virtual"""
        if self.m_checkBox11.IsChecked():
            self.m_checkBox21.Enable(True)
        else:
            if self.m_checkBox21.IsChecked():
                self.m_checkBox86.SetValue(True)
                self.m_checkBox21.SetValue(False)
            self.m_checkBox21.Enable(False)

    def on_toggle_pure(self, event):
        """Handle toggle pure"""
        # Although a base class can implement pure methods,
        # we preselect default behaviour (not implemented)
        if self.m_checkBox21.IsChecked():
            self.m_checkBox86.SetValue(False)
        else:
            self.m_checkBox86.SetValue(True)

    def on_toggle_deleted(self, event):
        """Handle toggle deleted method"""
        status = not self.m_checkBox126.IsChecked()
        self.m_checkBox86.SetValue(status)
        self.m_checkBox86.Enable(status)

    def on_toggle_template(self, event):
        """Handles toggle event"""
        if self.m_checkBox63.GetValue():
            self.m_stStartTemplate.Enable(True)
            self.m_template.Enable(True)
            self.m_stEndTemplate.Enable(True)
            # templates cannot be virtual
            self.m_checkBox11.SetValue(False)
            self.m_checkBox11.Enable(False)
            self.m_checkBox21.SetValue(False)
            self.m_checkBox21.Enable(False)
            if not self.m_checkBox86.IsEnabled():
                self.m_checkBox86.Enable(True)
                self.m_checkBox86.SetValue(True)
        else:
            self.m_stStartTemplate.Enable(False)
            self.m_template.SetValue('')
            self.m_template.Enable(False)
            self.m_stEndTemplate.Enable(False)
            # non-templates can be virtual
            self.m_checkBox11.Enable(True)
            self.m_checkBox21.Enable(True)
        event.Skip()

    def remove_template_types(self):
        """Remove template generated types from combo. This method only removes
        the in-place template types created by function template declaration."""
        # get current value
        type_index = self.m_type.GetCurrentSelection()
        if type_index != wx.NOT_FOUND:
            s = self.m_type.GetString(type_index)
        else:
            s = None
        for x in self._template_types:
            type_index = self.m_type.FindString(x)
            if type_index != wx.NOT_FOUND:
                self.m_type.Delete(type_index)
        if s is not None:
            self.m_type.SetStringSelection(s)

    def on_change_template_specification(self, event):
        """Handles change text event"""
        # When the template specification changes, virtual types must be added
        text = self.m_template.GetValue()
        template_items = text.split(',')
        prologues = ['class', 'typename']
        template_types = []
        for u in template_items:
            v = u.split('=')[0].strip()
            for p in prologues:
                if v.find(p) == 0:
                    v = v.replace(p, '').strip()
                    if len(v) and v not in self._nested_template_types:
                        template_types.append(v)
                    break
        # remove previous template types
        self.remove_template_types()
        # Append templates to available types
        if len(template_types) > 0:
            self.m_type.AppendItems(template_types)
            self._template_types = template_types
        event.Skip()

    def update_type_context(self):
        type_index = self.m_type.GetCurrentSelection()
        selected_type = self._types.get(self.m_type.GetString(type_index), None)
        template_args = False
        if selected_type is not None:
            if selected_type.is_template:
                template_args = True
        if template_args is True:
            self.m_staticText65.Enable(True)
            self.m_template_args.Enable(True)
            self.m_staticText66.Enable(True)
            self.m_staticText92.Show(True)
            self.m_staticText65.Show(True)
            self.m_template_args.Show(True)
            self.m_staticText66.Show(True)
        else:
            self.m_staticText92.Show(False)
            self.m_staticText65.Show(False)
            self.m_template_args.Show(False)
            self.m_staticText66.Show(False)
            self.m_staticText65.Enable(False)
            self.m_template_args.Enable(False)
            self.m_staticText66.Enable(False)
            self.m_template_args.SetValue('')
        self.Layout()

    def on_type_changed(self, event):
        """This event happens when the return type is changed. The main goal
        of this callback is handling template types for argument specification"""
        self.update_type_context()
        event.Skip()

    def validate(self):
        """Validation"""
        self._name = self.m_comboBox5.GetValue()
        if len(self._name) == 0:
            wx.MessageBox(
                "Method name must not be empty", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        label = '[A-Za-z_]((::)?[0-9A-Za-z_]+)*'
        operators = [
            '=', '==', '!=',
            '<', '>', '<=', '>=', r'\+', '-', r'\*', '/', '%', r'\+=', '-=', r'\*=', '/=', '%=',
            '!', '&&', r'\|\|', r'\+\+', '--', '&', r'\|', r'\^', '~', '&=', r'\|=', r'\^=',
            '<<', '>>', '<<=', '>>=', '->', r'\(\)',
            r'\[\]', r'(const\s+)?{label}(\s+int)?\s*(\*|&)?'.format(label=label),
            r'\[\]', r'(const\s+)?((unsigned|signed)\s+)?(long\s+)?{label}(\s+int)?\s*(\*|&)?'.format(label=label)]
        operator = r'operator\s+({0})'.format('|'.join(operators))
        expression = "^({label}|{operator})$".format(label=label, operator=operator)
        if re.match(expression, self._name) is None:
            wx.MessageBox(
                "Method name contains invalid characters", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        type_index = self.m_type.GetCurrentSelection()
        if type_index == wx.NOT_FOUND:
            wx.MessageBox(
                "Invalid type", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        typename = self.m_type.GetString(type_index)
        if self.m_checkBox63.IsChecked():
            self._template = self.m_template.GetValue()
        else:
            self._template = None
        # filter template types
        template_types = self._template_types + self._nested_template_types
        if typename in template_types:
            self._typei = model.cc.typeinst(
                type=self._types['@'],
                type_alias=typename,
                const=self.m_checkBox1.IsChecked(),
                ptr=self.m_checkBox3.IsChecked(),
                ref=self.m_checkBox2.IsChecked(),
                ptrptr=self.m_checkBox4.IsChecked(),
                constptr=self.m_checkBox5.IsChecked(),
                )
        else:
            selected_type = self._types[typename]
            if selected_type.is_template:
                # we construct type instance with explicit arguments
                type_args = self.m_template_args.GetValue()
                self._typei = model.cc.typeinst(
                    type=selected_type,
                    const=self.m_checkBox1.IsChecked(),
                    ptr=self.m_checkBox3.IsChecked(),
                    ref=self.m_checkBox2.IsChecked(),
                    ptrptr=self.m_checkBox4.IsChecked(),
                    constptr=self.m_checkBox5.IsChecked(),
                    type_args=type_args
                    )
            else:
                self._typei = model.cc.typeinst(
                    type=selected_type,
                    const=self.m_checkBox1.IsChecked(),
                    ptr=self.m_checkBox3.IsChecked(),
                    ref=self.m_checkBox2.IsChecked(),
                    ptrptr=self.m_checkBox4.IsChecked(),
                    constptr=self.m_checkBox5.IsChecked(),
                    )
        access_index = self.m_choice2.GetCurrentSelection()
        self._access = self.m_choice2.GetString(access_index)
        self._static = self.m_checkBox6.IsChecked()
        self._virtual = self.m_checkBox11.IsChecked()
        self._pure = self.m_checkBox21.IsChecked()
        self._inline = self.m_checkBox31.IsChecked()
        self._const_method = self.m_checkBox41.IsChecked()
        self._note = self.m_richText1.GetValue()
        self._declare = self.m_checkBox85.IsChecked()
        self._implement = self.m_checkBox86.IsChecked()
        self._deleted = self.m_checkBox126.IsChecked()
        if self._use_calling_convention:
            self._calling_convention = self.m_comboBox1.GetStringSelection()
        else:
            self._calling_convention = None
        return True

    def get_kwargs(self):
        """Returns kwargs dictionary suitable for object creation"""
        kwargs = {
            'parent': self.container,
            'name': self._name,
            'type': self._typei,
            'access': self._access,
            'static': self._static,
            'virtual': self._virtual,
            'pure': self._pure,
            'inline': self._inline,
            'template': self._template,
            'template_types': self._template_types,
            'note': self._note,
            'declare': self._declare,
            'implement': self._implement,
            'constmethod': self._const_method,
            'calling_convention': self._calling_convention,
            'deleted': self._deleted,
        }
        return kwargs

    def copy_attributes(self, method):
        """Copy attributes to method"""
        method._name = self._name
        method._typei = copy.copy(self._typei)
        method._access = self._access
        method._static = self._static
        method._virtual = self._virtual
        method._pure = self._pure
        method._inline = self._inline
        method._const_method = self._const_method
        method.note = self._note
        method._template = self._template
        method._template_types = self._template_types
        method._declare = self._declare
        method._implement = self._implement
        method._deleted = self._deleted
        method._calling_convention = self._calling_convention


    def set_attributes(self, method):
        """Setup attributes for editing already method"""
        self.m_comboBox5.SetValue(method.name)
        access_index = self.m_choice2.FindString(method._access)
        self.m_choice2.SetSelection(access_index)
        self.m_checkBox6.SetValue(method.static)
        self.m_checkBox11.SetValue(method._virtual)
        self.m_checkBox21.Enable(method._virtual)
        self.m_checkBox21.SetValue(method._pure)
        self.m_checkBox31.SetValue(method._inline)
        self.m_checkBox41.SetValue(method._const_method)
        self.m_richText1.SetValue(method.note)
        self.SetTitle("Edit method")
        if method.is_template:
            self.m_checkBox63.SetValue(True)
            self.m_stStartTemplate.Enable(True)
            self.m_template.Enable(True)
            self.m_stEndTemplate.Enable(True)
            self.m_template.SetValue(method.template)
            # if template is specified and the method holds arguments
            # using that types, the template specification cannot be changed
            for arg in method[model.cc.Argument]:
                if arg.type_instance.type_alias in method.template_types:
                    self.m_checkBox63.Enable(False)
                    self.m_checkBox63.SetToolTip(
                        'Template cannot be edited because arguments using template types exist.')
                    self.m_stStartTemplate.Enable(False)
                    self.m_template.Enable(False)
                    self.m_stEndTemplate.Enable(False)
                    break
        type_instance = method.type_instance
        type_index = self.m_type.FindString(type_instance.type_name)
        self.m_type.SetSelection(type_index)
        self.m_checkBox1.SetValue(type_instance.is_const)
        self.m_checkBox3.SetValue(type_instance.is_ptr)
        self.m_checkBox2.SetValue(type_instance.is_ref)
        if type_instance.is_ptr:
            self.m_checkBox4.Enable(True)
            self.m_checkBox5.Enable(True)
        self.m_checkBox4.SetValue(type_instance.is_ptr_to_ptr)
        self.m_checkBox5.SetValue(type_instance.is_const_ptr)
        if type_instance.type_args is not None:
            self.m_staticText65.Enable(True)
            self.m_template_args.Enable(True)
            self.m_staticText66.Enable(True)
            self.m_template_args.SetValue(type_instance.type_args)
        self.m_checkBox85.SetValue(method._declare)
        self.m_checkBox86.SetValue(method._implement)
        self._deleted = getattr(method,'_deleted', False)
        self.m_checkBox126.SetValue(self._deleted)  # to reduce in next releases
        self._calling_convention = getattr(method, '_calling_convention', None)
        self._use_calling_convention = self._calling_convention is not None
        self.m_checkBox61.SetValue(self._use_calling_convention)
        self.m_comboBox1.Enable(self._use_calling_convention)
        if self._use_calling_convention:
            self.m_comboBox1.SetValue(self._calling_convention)
        self.update_type_context()

    def on_toggle_pointer(self, event):
        """ptr toggle gui"""
        if self.m_checkBox3.IsChecked():
            self.m_checkBox4.Enable(True)
            self.m_checkBox5.Enable(True)
        else:
            self.m_checkBox4.SetValue(False)
            self.m_checkBox5.SetValue(False)
            self.m_checkBox4.Enable(False)
            self.m_checkBox5.Enable(False)
        event.Skip()

    def OnChar(self, event):
        # print 'received onchar'
        if not event.AltDown():
            key_code = event.GetUnicodeKey()
            if key_code == wx.WXK_NONE:
                key_code = event.GetKeyCode()
            try:
                key_char = chr(key_code)
                if key_char.isalnum() or key_code is wx.WXK_SPACE or key_char in '<>_:':
                    self.choice_string += key_char.lower()
                    for t in self.sorted_types:
                        tl = t.lower()
                        if tl.find(self.choice_string) == 0:
                            if self.m_type.SetStringSelection(t):
                                self.update_type_context()
                                return
            except ValueError:
                if key_code == wx.WXK_RIGHT:
                    # accept current show text as choice for extension_
                    self.choice_string = self.m_type.GetStringSelection().lower()
                    return
        self.choice_string = ""
        event.Skip()

    def on_toggle_calling_convention(self, event):
        self._use_calling_convention = self.m_checkBox61.IsChecked()
        self.m_comboBox1.Enable(self._use_calling_convention)

    @wxx.save_position
    def on_cancel(self, event):
        """cancel event handler"""
        self.EndModal(wx.ID_CANCEL)

    @wxx.save_position
    def on_ok(self, event):
        """ok event handler"""
        if self.validate():
            context.set_info(
                'c++ member method dialog',
                {'access': self._access})
            self.EndModal(wx.ID_OK)


