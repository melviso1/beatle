
import wx
from beatle.lib import wxx
from beatle.model.cc import CCProject
from .._base import UserSectionsDialogBase


class UserSectionsDialog( UserSectionsDialogBase ):
    """
    This dialog allows to edit user sections and put
    custom code inside generated code.
    """
    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, container):
        """initialization"""
        from ...handlers import EditorHandler
        self._types = {}
        container_cls = container.inner_class
        if container_cls:
            s = [container_cls.scoped]

            def scoped(x):
                if hasattr(x, 'scoped_str'):
                    return x.scoped_str(s), x
                if hasattr(x, 'scoped'):
                    return x.scoped, x
                return x.name, x

            self._types = dict(scoped(x) for x in container.types)
        else:

            def scoped(x):
                if hasattr(x, 'scoped'):
                    return x.scoped, x
                return x.name, x

            self._types = dict(scoped(x) for x in container.types)

        if '@' in self._types:
            del self._types['@']
        keywords2 = [x for x in self._types.keys() if ' ' not in x]
        self._editorArgs = {
            'language': 'c++',
            'handler': EditorHandler(
                text='',
                read_only=False,
                use_bookmarks=False,
                use_breakpoints=False,
                keywords2=keywords2
            )
        }
        super(UserSectionsDialog, self).__init__(parent)
        self.container = container

    # Handlers for UserSections events.
    def OnKillFocus(self, event):
        event.Skip()

    def OnGetFocus(self, event):
        event.Skip()

    def copy_attributes(self, obj):
        """Copy attributes to class"""
        obj.user_code_h1 = self.m_h1.GetText()
        if type(obj) not in CCProject.module_classes:
            obj.user_code_h2 = self.m_h2.GetText()
            obj.user_code_h3 = self.m_h3.GetText()
        obj.user_code_h4 = self.m_h4.GetText()
        obj.user_code_s1 = self.m_s1.GetText()
        obj.user_code_s2 = self.m_s2.GetText()
        obj.user_code_s3 = self.m_s3.GetText()

    def set_attributes(self, obj):
        """Setup attributes for editing already class"""
        self.m_h1.SetText(getattr(obj, "user_code_h1",""))
        if type(obj) not in CCProject.module_classes:
            self.m_h2.SetText(getattr(obj, "user_code_h2",""))
            self.m_h3.SetText(getattr(obj, "user_code_h3",""))
        else:
            self.m_sections_book.SetPageText(3,'after declarations')
            self.m_sections_book.DeletePage(2)
            self.m_sections_book.DeletePage(1)
            self.m_sections_book.SetPageText(0,'before declarations')
        self.m_h4.SetText(getattr(obj, "user_code_h4", ""))
        self.m_s1.SetText(getattr(obj, "user_code_s1", ""))
        self.m_s2.SetText(getattr(obj, "user_code_s2", ""))
        self.m_s3.SetText(getattr(obj, "user_code_s3", ""))

    def validate(self):
        return True

    @wxx.save_position
    def on_cancel(self, event):
        """cancel event handler"""
        self.EndModal(wx.ID_CANCEL)

    @wxx.save_position
    def on_ok(self, event):
        """Handle Ok button event"""
        if self.validate():
            self.EndModal(wx.ID_OK)
