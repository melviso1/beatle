"""Subclass of NewConstructor, which is generated by wxFormBuilder."""

import wx

from beatle.lib import wxx
from .._base import ConstructorDialogBase


# Implementing ConstructorDialog
class ConstructorDialog(ConstructorDialogBase):
    """
    This dialog allows to create a new class constructor and
    his attributes. The preferred constructor means that this
    constructor will be chosen by default for the list of
    the required arguments of derived classes and creating the
    init sections of these classes.
    """
    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, container):
        """Initialize dialog"""
        from beatle.app import resources as rc
        super(ConstructorDialog, self).__init__(parent)
        self._use_calling_convention = False
        self._calling_convention = None
        self._container = container
        self._class = container.inner_class
        self.m_choice2.SetFocus()
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap("constructor"))
        self.SetIcon(icon)
        self.m_button2.SetFocus()

    def copy_attributes(self, ctor):
        """Transfer attributes to ctor"""
        ctor._name = self._name
        ctor._access = self._access
        ctor._explicit = self._explicit
        ctor._inline = self._inline
        ctor._declare = self._declare
        ctor._implement = self._implement
        ctor._deleted = self._deleted
        ctor._calling_convention = self._calling_convention
        ctor.note = self._note
        ctor.set_preferred(self._preferred)

    def set_attributes(self, ctor):
        """Transfer attributes from existing constructor"""
        self._container = ctor.parent
        self._class = ctor.inner_class
        self._name = ctor.name
        self._access = ctor.access
        iSel = self.m_choice2.FindString(self._access)
        self.m_choice2.Select(iSel)
        self._explicit = ctor._explicit
        self.m_checkBox41.SetValue(self._explicit)
        self._declare = ctor._declare
        self.m_checkBox92.SetValue(self._declare)
        self._implement = ctor._implement
        self.m_checkBox93.SetValue(self._implement)
        self._inline = ctor._inline
        self.m_checkBox31.SetValue(self._inline)
        self._calling_convention = ctor._calling_convention
        self._use_calling_convention = self._calling_convention is not None
        self.m_checkBox6.SetValue(self._use_calling_convention)
        self.m_comboBox1.Enable(self._use_calling_convention)
        if self._use_calling_convention:
            self.m_comboBox1.SetValue(self._calling_convention)
        self._deleted = getattr(ctor,'_deleted', False)
        self.m_checkBoxDeleted.SetValue(self._deleted)  # to reduce in next releases
        self._note = ctor.note
        self.m_richText1.SetValue(self._note)
        self._preferred = ctor.is_preferred()
        pref = self._class.get_preferred_constructor()
        if pref is None or pref is ctor:
            self.m_checkBoxPreferred.Enable(True)
        self.m_checkBoxPreferred.SetValue(self._preferred)
        self.SetTitle("Edit constructor")

    def validate(self):
        """Validation"""
        self._name = self._class.name
        access_index = self.m_choice2.GetSelection()
        self._access = self.m_choice2.GetString(access_index)
        self._declare = self.m_checkBox92.IsChecked()
        self._explicit = self.m_checkBox41.IsChecked()
        self._inline = self.m_checkBox31.IsChecked()
        self._implement = self.m_checkBox93.IsChecked()
        self._deleted = self.m_checkBoxDeleted.IsChecked()
        if self._use_calling_convention:
            self._calling_convention = self.m_comboBox1.GetStringSelection()
        else:
            self._calling_convention = None
        self._note = self.m_richText1.GetValue()
        self._preferred = self.m_checkBoxPreferred.IsChecked()
        return True

    def get_kwargs(self):
        """return arguments suitable for creation"""
        return {
            'parent': self._container,
            'name': self._name,
            'access': self._access,
            'explicit': self._explicit,
            'inline': self._inline,
            'declare': self._declare,
            'implement': self._implement,
            'calling_convention': self._calling_convention,
            'note': self._note,
            'preferred': self._preferred,
            'autoargs': True,
            'deleted': self._deleted
        }

    def on_toggle_calling_convention(self, event):
        """Implements calling toggle"""
        self._use_calling_convention = self.m_checkBox6.IsChecked()
        self.m_comboBox1.Enable(self._use_calling_convention)

    def on_toggle_deleted(self, event):
        """Handle toggle deleted method"""
        status = not self.m_checkBoxDeleted.IsChecked()
        self.m_checkBox93.SetValue(status)
        self.m_checkBox93.Enable(status)

    @wxx.save_position
    def on_ok(self, event):
        """On Ok"""
        if self.validate():
            self.EndModal(wx.ID_OK)

    @wxx.save_position
    def on_cancel(self, event):
        """on_cancel"""
        self.EndModal(wx.ID_CANCEL)


