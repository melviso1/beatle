# -*- coding: utf-8 -*-

from ._ArgumentDialog import ArgumentDialog
from ._ClassDialog import ClassDialog
from ._ClassDiagramDialog import ClassDiagramDialog
from ._ConstructorDialog import ConstructorDialog
from ._DestructorDialog import DestructorDialog
from ._EnumDialog import EnumDialog
from ._EnumItemDialog import EnumItemDialog
from ._EnvironmentDialog import EnvironmentDialog
from ._MethodDialog import MethodDialog
from ._InheritanceDialog import InheritanceDialog
from ._IsClassMethodsDialog import IsClassMethodsDialog
from ._MemberDialog import MemberDialog
from ._MemberMethodDialog import MemberMethodDialog
from ._ModuleDialog import ModuleDialog
from ._NamespaceDialog import NamespaceDialog
from ._RelationDialog import RelationDialog
from ._SelectClassesDialog import SelectClassesDialog
from ._SelectContextsDialog import SelectContextsDialog
from ._SelectFriendsDialog import SelectFriendsDialog
from ._SelectLibrariesDialog import SelectLibrariesDialog
from ._TypeDialog import TypeDialog
from ._UserSectionsDialog import UserSectionsDialog
from ._VariableDialog import VariableDialog
from ._VirtualMethodsDialog import VirtualMethodsDialog
from ._NavigatorDialog import NavigatorDialog
from ._IteratorWizardDialog import IteratorWizardDialog
from ._ContextItemsDialog import ContextItemsDialog
from ._IncludeDialog import IncludeDialog
from ._SettingsDialog import SettingsDialog


def init():
    """Initialize this activity model ui"""
    from beatle import model
    from beatle.app.ui.dlg import FolderDialog
    from beatle.activity.models.ui.dlg import GenerateSourcesDialog, NoteDialog 

    associations = {
        model.cc.Namespace: (NamespaceDialog, 'edit namespace {0}'),
        model.cc.Folder: (FolderDialog, 'edit folder {0}'),
        model.cc.Type: (TypeDialog, 'edit type {0}'),
        model.cc.Class: (ClassDialog, 'edit c++ class {0}'),
        model.cc.Argument: (ArgumentDialog, 'edit c++ method argument {0}'),
        model.cc.ClassDiagram: (ClassDiagramDialog, 'edit class diagram {0}'),
        model.cc.Inheritance: (InheritanceDialog, 'edit c++ inheritance {0}'),
        model.cc.MemberData: (MemberDialog, 'edit c++ class member {0}'),
        model.cc.Data: (VariableDialog, 'edit c++ variable {0}'),
        model.cc.Constructor: (ConstructorDialog, 'edit c++ {0} constructor'),
        model.cc.MemberMethod: (MemberMethodDialog, 'edit c++ method {0}'),
        model.cc.GetterMethod: (MemberMethodDialog, 'edit c++ getter method {0}'),
        model.cc.SetterMethod: (MemberMethodDialog, 'edit c++ setter method {0}'),
        model.cc.IsClassMethod: (MemberMethodDialog, 'edit c++ method {0}'),
        model.cc.Destructor: (DestructorDialog, 'edit c++ destructor {0}'),
        model.cc.Module: (ModuleDialog, 'edit c++ module "{0}"'),
        model.cc.Function: (MethodDialog, 'edit c++ method {0}'),
        model.cc.Note: (NoteDialog, 'edit note'),
        model.cc.Enum: (EnumDialog, 'edit c++ enum {0}'),
        model.cc.RelationFrom: (RelationDialog, 'edit relation'),
        model.cc.RelationTo: (RelationDialog, 'edit relation'),
    }
    from beatle.app.ui.tools import DIALOG_DICT
    DIALOG_DICT.update(associations)

