import wx

if "__WXMSW__" in wx.PlatformInfo:
    from ._debug_setting_base_win import *
else:
    from ._debug_setting_base_linux import *