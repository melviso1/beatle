
import wx

from beatle.activity.arch.ui import ui as ui


class DebugConsolePane(ui.DebugConsolePaneBase):
    """aux pane for debug command"""

    def __init__(self, parent, handler):
        """init"""
        super(DebugConsolePane, self).__init__(parent)
        self._handler = handler

    def update_data(self, response):
        """Add data from debugger"""
        # response = response.strip()
        # if len(response) == 0:
        #     return
        # if response[-1] != '\n':
        #    response = response+'\n'
        if self.m_debugOutput.GetNumberOfLines() > 500:
            text = self.m_debugOutput.GetValue()
            text = '\n'.join(text.splitlines()[50:])
            self.m_debugOutput.Clear()
            self.m_debugOutput.AppendText(text)
        # self._need_scroll = need_scroll
        self.m_debugOutput.AppendText(response)
        self.m_debugOutput.MoveEnd()
        self.m_debugOutput.ShowPosition(self.m_debugOutput.LastPosition)

    def on_text_changed(self, event):
        pass
        # self.m_debugOutput.LayoutContent()
        # self.m_debugOutput.MoveEnd()
        # self.m_debugOutput.MoveToLineStart()
        # self.m_debugOutput.ShowPosition(self.m_debugOutput.GetCaretPosition())

    def on_enter(self, event):
        """Send command to debugger"""
        cmd = self.m_debugCommand.GetValue().strip()
        self.m_debugCommand.SetValue(wx.EmptyString)
        self._handler.send_user_command(cmd)
