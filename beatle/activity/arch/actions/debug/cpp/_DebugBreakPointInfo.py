

class DebugBreakPointInfo(object):
    """Represent current breakpoints"""

    def __init__(self, number, **kwargs):
        self._number = number
        self._attributes = kwargs

    def __str__(self):
        return 'breakpoint {number}:    {description}'.format(
            number=self._number, description=','.join('{name}="{value}"'.format(
                name=x, value=self._attributes[x]) for x in self._attributes))

    @property
    def number(self):
        """Return the number of breakpoint."""
        return self._number

    @property
    def file(self):
        return self._attributes.get('file', None)

    @property
    def line(self):
        return self._attributes.get('line', None)

    @property
    def attributes(self):
        return self._attributes

    @attributes.setter
    def attributes(self, value):
        self._attributes = value

    def __getitem__(self, key):
        """
        Compatibility with trepan3k debugger:
        Until we do some deep changes, the trepan3k breakpoints
        are single stored as a tuple and index 0 and 1
        are, respectively, source and line.
        """
        key = int(key)
        if key == 0:
            return self.file
        if key == 1:
            return self.line
        return None

