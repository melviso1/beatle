

class DebugLibraryInfo(object):
    """Represents loaded libraries"""

    def __init__(self, id_, target_name, host_name):
        super(DebugLibraryInfo, self).__init__()
        self._identifier = id_
        self._target_name = target_name
        self._host_name = host_name
        self._thread_group = None
        self._from_address = None
        self._to_address = None

    def __str__(self):
        s = self._identifier
        if self._from_address is not None and self._to_address is not None:
            s += ' ({self._from_address}:{self._to_address})'.format(self=self)
        if self._thread_group is not None:
            s += ' [{self._thread_group}]'.format(self=self)
        return s

    @property
    def identifier(self):
        return self._identifier

    @property
    def target_name(self):
        return self._target_name

    @property
    def host_name(self):
        return self._host_name

    @property
    def thread_group(self):
        return self._thread_group

    @thread_group.setter
    def thread_group(self, value):
        self._thread_group = value

    @property
    def from_address(self):
        return self._from_address

    @from_address.setter
    def from_address(self, value):
        self._from_address = value

    @property
    def to_address(self):
        return self._to_address

    @to_address.setter
    def to_address(self, value):
        self._to_address = value

