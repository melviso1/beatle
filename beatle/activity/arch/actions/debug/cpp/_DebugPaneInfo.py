import wx
from beatle.lib import wxx


class DebugPaneInfo(object):
    """A tiny wrapper for fast operation"""
    _instances = 0  # singleton counter

    def __init__(self, class_, id_, name_, help_string=''):
        self._class = class_
        self._id = id_
        self._name = name_
        self._help_string = help_string
        self._instance = None
        self._session = None
        self._container = None
        self._mgr = None
        self._shown = False
        self._index = DebugPaneInfo._instances
        DebugPaneInfo._instances = DebugPaneInfo._instances + 1

    @property
    def instance(self):
        return self._instance

    @property
    def name(self):
        return self._name

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        self._index = value

    @property
    def shown(self):
        return self._shown

    def init_pane(self, session, show=True):
        self._session = session
        self._container = session.m_debug_info
        self._mgr = session.frame.m_mgr
        if show:
            self.create_pane()

    def uninit_pane(self):
        self._instance = None
        self._session = None

    def create_pane(self, selected=False):
        if self._session is None:
            return False
        self._instance = self._class(self._container, self._session)
        if not hasattr(self._instance, 'update_data'):
            # this could happen ...
            def update_data(this: object):
                return this

            bind_update_data = update_data.__get__(self._instance, None)
            setattr(self._instance, 'update_data', bind_update_data)
        self._container.AddPage(self._instance, self._name, selected, wx.NOT_FOUND)
        self._shown = True
        return True

    def add_menu_item(self, menu):
        """Add a menu entry for this pane"""
        menu.AppendItem(
            wxx.MenuItem(menu, self._id, self._name,
                         self._help_string, wx.ITEM_CHECK))

    def bind_events(self):
        if self._session is None:
            return False
        frame = self._session.frame
        frame.Bind(wx.EVT_UPDATE_UI, self.on_update_toggle, id=self._id)
        frame.Bind(wx.EVT_MENU, lambda x: self.do_toggle(), id=self._id)
        return True

    def unbind_events(self):
        if self._session is None:
            return False
        frame = self._session.frame
        frame.Unbind(wx.EVT_UPDATE_UI, id=self._id)
        frame.Unbind(wx.EVT_MENU, id=self._id)
        return True

    def update_data(self):
        """refresh visualization"""
        if self._shown and self._instance is not None:
            self._instance.update_data()

    def on_update_toggle(self, event):
        event.Check(self._shown)

    def do_toggle(self):
        if self._session is None:
            return
        if self._instance is None:
            self.create_pane(True)
            self.update_data()
            if self._container.GetPageCount() == 1:
                self._mgr.GetPane('watch_book').Show()
            self._mgr.Update()
            return
        index = self._container.GetPageIndex(self._instance)
        if index != wx.NOT_FOUND:
            self._shown = False
            self._container.RemovePage(index)
            if self._container.GetPageCount() == 0:
                self._mgr.GetPane('watch_book').Hide()
            self._mgr.Update()

            # temp workaround: it seems that aui touch the window
            # making it not very usable at all
            if self._instance is not None:
                # hard way: maybe the window is closed
                self._instance.Destroy()
                self._instance = None
        else:
            if self._shown:
                print('Alert! page thinks is shown!!')
            self._shown = True
            self._container.AddPage(self._instance, self._name, True, self._id)
            if self._container.GetPageCount() == 1:
                self._mgr.GetPane('watch_book').Show()
            self._mgr.Update()
            self.update_data()

    def close(self):
        """Handles direct page close

        Unfortunately, it seems that direct user close
        keeps the page in bad state for reusing it later,
        so the better and safe is to destroy it.
        """
        self._shown = False
        index = self._container.GetPageIndex(self._instance)
        if index != wx.NOT_FOUND:
            self._container.RemovePage(index)
            if self._container.GetPageCount() == 0:
                self._mgr.GetPane('watch_book').Hide()
            self._mgr.Update()
        else:
            print('Alert!!! closed page not found!!')
        if self._instance is not None:
            # hard way: maybe the window is closed
            self._instance.Destroy()
            self._instance = None

