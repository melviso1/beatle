

class DebugThreadInfo(object):
    """small class for thread information"""

    def __init__(self, thread_group, id_):
        """init"""
        super(DebugThreadInfo, self).__init__()
        self._identifier = id_
        self._thread_group = thread_group
        self._running = True
        self._is_current = False
        thread_group.threads[id_] = self

    def __str__(self):
        group = self._thread_group.identifier
        if self._running:
            status = 'running'
        else:
            status = 'stopped'
        return '{group}:{id} {status}'.format(
            group=group, id=self._identifier, status=status)

    @property
    def group(self):
        return self._thread_group

    @property
    def is_current(self):
        return self._is_current

    @property
    def identifier(self):
        return self._identifier

    @property
    def running(self):
        return self._running

    @running.setter
    def running(self, value):
        self._running = value

    def delete(self):
        self._thread_group.remove(self._identifier)
        del self

