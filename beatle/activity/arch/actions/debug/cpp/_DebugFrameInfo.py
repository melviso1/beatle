

class DebugFrameInfo(object):
    """Represents a debug frame"""

    def __init__(self, **kwargs):
        self._level = kwargs['level']
        self._address = kwargs['address']
        self._func = kwargs['func']
        self._file = kwargs['file']
        self._fullname = kwargs['fullname']
        self._line = kwargs['line']
        self._from = kwargs.get('from', None)
        self._arch = kwargs.get('arch', None)

    @property
    def better_file(self):
        """return absolute file if exists and  relative if not"""
        if self._fullname:
            return self._fullname
        return self._file

    @property
    def file(self):
        return self._file

    @property
    def line(self):
        return self._line

    def __str__(self):
        return '{self._level}: {self._func} at {self._file}:{self._line}'.format(self=self)

