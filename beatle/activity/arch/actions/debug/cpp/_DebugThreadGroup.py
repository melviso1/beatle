

class DebugThreadGroup(object):
    """This represents a thread group"""

    def __init__(self, group_identifier):
        super(DebugThreadGroup, self).__init__()
        self._identifier = group_identifier
        self._pid = None
        self._started = False
        self._exit_code = None
        self._threads = {}

    def remove(self, thread_identifier):
        """remove a thread from the group"""
        if thread_identifier in self._threads:
            del self._threads[thread_identifier]

    @property
    def identifier(self):
        return self._identifier

    @property
    def pid(self):
        return self._pid

    @pid.setter
    def pid(self, pid_):
        self._pid = pid_

    @property
    def started(self):
        return self._started

    @started.setter
    def started(self, started_):
        self._started = started_

    @property
    def exit_code(self):
        return self._exit_code

    @exit_code.setter
    def exit_code(self, value):
        self._exit_code = value

    @property
    def threads(self):
        return self._threads

    def delete(self):
        while len(self._threads) > 0:
            self._threads[-1].delete()
        del self

