from ._DebugLocalInfo import DebugLocalInfo


class DebugArgInfo(DebugLocalInfo):
    """Represents a stack frame argument"""

    def __init__(self, **kwargs):
        super(DebugArgInfo, self).__init__(**kwargs)

