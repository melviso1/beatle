

class DebugLocalInfo(object):
    """Represents a stack frame var"""

    def __init__(self, **kwargs):
        super(DebugLocalInfo, self).__init__()
        # mandatory arguments
        self._name = kwargs['name']
        self._order = kwargs['order']
        if 'value' in kwargs:
            self._has_value = True
            self._value = kwargs['value']
        else:
            self._has_value = False
            self._value = None

    @property
    def name(self):
        return self._name

    @property
    def order(self):
        return self._order

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        # TO-DO : dispatch command
        self._value = value

