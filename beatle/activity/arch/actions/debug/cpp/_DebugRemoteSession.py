import signal
import os
import re
import threading
import platform

import wx
from wx import aui

from beatle.lib import wxx
from beatle.lib.handlers import identifier
from beatle.lib.api import context
from beatle.activity.arch.ui import pane
from beatle.app.ui import tools


from ._DebugPaneInfo import DebugPaneInfo
from ._DebugArgInfo import DebugArgInfo
from ._DebugLocalInfo import DebugLocalInfo
from ._DebugThreadInfo import DebugThreadInfo
from ._WatchExpression import WatchExpression
from ._DebugFrameInfo import DebugFrameInfo
from ._DebugThreadGroup import DebugThreadGroup
from ._DebugLibraryInfo import DebugLibraryInfo
from ._DebugBreakPointInfo import DebugBreakPointInfo


class DebugRemoteSession(object):
    """Handles interaction with debugger"""

    _show_threads = identifier('ID_SHOW_DEBUG_THREADS')
    _show_locals = identifier('ID_SHOW_DEBUG_LOCALS')
    _show_expressions = identifier('ID_SHOW_DEBUG_EXPRESSIONS')
    _show_breakpoints = identifier('ID_SHOW_DEBUG_BREAKPOINTS')

    def __init__(self, handler,
                 local_source_dir, host,
                 port, user,
                 password, remote_executable, remote_source_dir,
                 arguments=None, attach=False):
        """Init"""
        self._handler = handler
        self._frame = context.get_frame()
        self._menu = None
        self._old_perspective = None
        self._perspective = None
        self.menu_pos = None
        self.done = False
        self._paused = False
        self.m_stack = None
        self.m_command = None
        self.m_debug_info = None

        self._debug_panes = [
            DebugPaneInfo(pane.Locals,
                          self._show_locals, u"Locals", u"show frame locals."),
            DebugPaneInfo(pane.Expressions,
                          self._show_expressions, u"Expressions", u"show watch expressions."),
            DebugPaneInfo(pane.Breakpoints,
                          self._show_breakpoints, u"Breakpoints", u"show breakpoints."),
            DebugPaneInfo(pane.Threads,
                          self._show_threads, u"Threads", u"show threads of current target."),
        ]

        self._local_source_dir = local_source_dir
        if platform.system() == 'Windows':
            if self._local_source_dir[-1] != '\\':
                self._local_source_dir += '\\'
        elif self._local_source_dir[-1] != '/':
            self._local_source_dir += '/'
        self._host = host
        self._port = port
        self._user = user
        self._password = self.translate_string(password)
        self._executable = remote_executable
        self._source_dir = remote_source_dir
        if platform.system() == 'Windows':
            if self._source_dir[-1] != '\\':
                self._source_dir += '\\'
        elif self._source_dir[-1] != '/':
            self._source_dir += '/'
        self._program_arguments = arguments
        self._attach = attach

        # Regex expressions
        # -------------------------------------------
        #
        #  gdb status changes regular expressions
        #
        # -------------------------------------------

        thread_running = r'running,thread-id="(?P<id>[^"]+)".*'
        stopped = r'stopped,(?P<info>.*)'

        self._on_gdb_status_changes_map = {
            re.compile(thread_running, re.DOTALL): self.on_thread_running,
            re.compile(stopped, re.DOTALL): self.on_stopped,
        }

        # ---------------------------------------
        #
        #  run status changes regular expressions
        #
        # -----------------------------------------

        thread_group_added = r'thread-group-(added|created),id="(?P<id>[^"]+)".*'
        thread_group_removed = r'thread-group-removed,id="(?P<id>[^"]+)".*'
        thread_group_started = r'thread-group-started,id="(?P<id>[^"]+)",pid="(?P<pid>[^"]+)".*'
        thread_group_exited = r'thread-group-exited,id="(?P<id>[^"]+)"(,exit-code="(?P<code>[^"]+)")?.*'
        thread_created = r'thread-created,id="(?P<id>[^"]+)",group-id="(?P<gid>[^"]+)".*'
        thread_exited = r'thread-exited,id="(?P<id>[^"]+)",group-id="(?P<gid>[^"]+)".*'
        thread_selected = r'thread-selected,id="(?P<id>[^"]+)".*'
        library_loaded = r'library-loaded,id="(?P<id>[^"]+)",target-name="(?P<target_name>[^"]+)",' + \
                         r'host-name="(?P<host_name>[^"]+)",symbols-loaded="([^"]+)"' + \
                         r'(,thread-group="(?P<thread_group_id>[^"]+)")?' + \
                         r'(,ranges=\[\{from="(?P<from_address>[^"]+)",to="(?P<to_address>[^"]+)"\}\])?.*'
        library_unloaded = r'library-unloaded,target-name="(?P<target_name>[^"]+)",' + \
                           r'host-name="(?P<host_name>[^"]+)"(,thread-group="(?P<thread_group_id>[^"]+)")?'
        breakpoint_created = r'breakpoint-created,bkpt=\{(?P<breakpoint_info>[^\}]+)\}.*'
        breakpoint_modified = r'breakpoint-modified,bkpt=\{(?P<breakpoint_info>[^\}]+)\}.*'
        breakpoint_deleted = r'breakpoint-deleted,bkpt=\{(?P<breakpoint_info>[^\}]+)\}.*'
        breakpoint_deleted2 = r'breakpoint-deleted,(?P<breakpoint_info>.*)'

        self._on_run_status_changes_map = {
            re.compile(thread_group_added, re.DOTALL): self.on_thread_group_add,
            re.compile(thread_group_removed, re.DOTALL): self.on_thread_group_removed,
            re.compile(thread_group_started, re.DOTALL): self.on_thread_group_started,
            re.compile(thread_group_exited, re.DOTALL): self.on_thread_group_exited,
            re.compile(thread_created, re.DOTALL): self.on_thread_created,
            re.compile(thread_exited, re.DOTALL): self.on_thread_exited,
            re.compile(thread_selected, re.DOTALL): self.on_thread_selected,
            re.compile(library_loaded, re.DOTALL): self.on_library_loaded,
            re.compile(library_unloaded, re.DOTALL): self.on_library_unloaded,
            re.compile(breakpoint_created, re.DOTALL): self.on_breakpoint_created,
            re.compile(breakpoint_modified, re.DOTALL): self.on_breakpoint_modified,
            re.compile(breakpoint_deleted, re.DOTALL): self.on_breakpoint_deleted,
            re.compile(breakpoint_deleted2, re.DOTALL): self.on_breakpoint_deleted
        }

        # ----------------------------------
        #
        #   result status regular expression
        #
        # ----------------------------------

        done_breakpoint_add = r'done,bkpt=[{](?P<breakpoint_info>[^\}]+)[}].*'
        done_ = r'done(,(?P<results>.+))?'
        running = r'running(,(?P<results>.+))?'
        connected = r'connected'
        error = r'error,(?P<message>.+)'
        exit_ = r'exit'

        # -- we use a list because order is important here
        self._on_result_status_map = [
            (re.compile(done_breakpoint_add, re.DOTALL), self.on_breakpoint_created),
            (re.compile(done_, re.DOTALL), self.on_done),
            (re.compile(running, re.DOTALL), self.on_done),
            (re.compile(connected, re.DOTALL), lambda x: self.on_connected()),
            (re.compile(error, re.DOTALL), self.on_error),
            (re.compile(exit_, re.DOTALL), lambda x: self.on_exit()),
        ]

        # ----------------------------------
        #
        #   result done regular expressions
        #
        # ----------------------------------

        # -- parsing stack frames info
        self._stack_frame_list = re.compile(r'stack=\[(?P<stack_frame_list>[^\]]*)\]')
        self._stack_frame_entry = re.compile(
            r'frame=[{]level="(?P<num>[^"]+)",addr="(?P<address>[^"]+)",func="(?P<func>[^"]+)",' +
            r'file="(?P<file>[^"]+)"(,fullname="(?P<fullname>[^"]+)")?,line="(?P<line>[^"]+)"' +
            r'(,from="(?P<from>[^"]+)")?(,arch="(?P<arch>[^"]+)")?\}(,(?P<stack_frame_list>.*))?')

        # -- parsing stack frame arguments
        self._stack_arg_list = re.compile(r'stack-args=\[(?P<stack_arg_list_mixin>.*)\]')
        self._stack_arg_list_mixin = re.compile(
            r'frame=[{]level="(?P<num>[^"]+)",args=\[(?P<arg_list_mixin>.*)')
        self._arg_list_mixin = re.compile(
            r'([{])?name="(?P<name>[^"]+)"(?(1),value="(?P<value>((\\[."\'])|[^"])*)"[}])' +
            r'((,(?P<arg_list_mixin>.*))|(\]\}(,(?P<stack_arg_list_mixin>.*))?))?')

        # -- parsing stack frame locals
        self._stack_locals_list = re.compile(r'locals=\[(?P<stack_local_list_mixin>.*)\]')
        self._stack_local_list_mixin = re.compile(
            r'([{])?name="(?P<name>[^"]+)"(?(1),value="(?P<value>.*)"\})' +
            r'(,(?P<stack_local_list_mixin>.*))?')

        self._on_done_results_map = [
            (self._stack_frame_list, self.on_stack_frame),
            (self._stack_arg_list, self.on_stack_arguments),
            (self._stack_locals_list, self.on_stack_locals),
        ]

        # ----------------------------
        #
        #   auxiliary reg expressions
        #
        # -----------------------------

        self._break_point_fields = {
            'number': re.compile(r'(id|number)="(?P<value>[^"]+)"'),
            'type': re.compile(r'type="(?P<value>[\"]+)"'),
            'disposition': re.compile(r'disp="(?P<value>[^"]+)"'),
            'enabled': re.compile(r'enabled="(?P<value>[yn])"'),
            'address': re.compile(r'addr="(?P<value>(0x)?[0-9A-F]+)"', re.IGNORECASE),
            'func': re.compile(r'func="(?P<value>[^"]+)"'),
            'file': re.compile(r'file="(?P<value>[^"]+)"'),
            'fullname': re.compile(r'fullname="(?P<value>[^"]+)"'),
            'line': re.compile(r'line="(?P<value>(0X)?[0-9A-F]+)"', re.IGNORECASE),
            'thread': re.compile(r'thread="(?P<value>[^"]+)"'),
            'times': re.compile(r'times="(?P<value>(0X)?[0-9A-F]*)', re.IGNORECASE),
        }

        self._stopped_info_fields = {
            'reason': re.compile(r'reason="(?P<value>[^"]+)"'),
            'disposition': re.compile(r'disp="(?P<value>[^"]+)"'),
            'breakpoint_number': re.compile(r'bkptno="(?P<value>[^"]+)"'),
            'frame': re.compile(r'addr="(?P<value>[^"]+)"'),
            'func': re.compile(r'func="(?P<value>[^"]+)"'),
            'args': re.compile(r'args=\[(?P<value>[^\]]+)\]'),
            'file': re.compile(r'file="(?P<value>[^"]+)"'),
            'fullname': re.compile(r'fullname="(?P<value>[^"]+)"'),
            'line': re.compile(r'line="(?P<value>[^"]+)"'),
            'thread-id': re.compile(r'thread-id="(?P<value>[^"]+)"'),
            'stopped-threads': re.compile(r'stopped-threads="(?P<value>[^"]+)"'),
            'core': re.compile(r'core="(?P<value>[^"]+)"'),
        }

        self._threads = {}
        self._thread_groups = {}
        self._libraries = {}
        self._selected_thread = None
        self._first_running = False  # false before some line is executed

        # retained info
        self._filter_gdb_output = re.compile(r'(\(gdb\).*)', re.DOTALL)
        self._newline = re.compile(r'(?<!\\)\\n')
        self._quoted = re.compile(r'(?<!\\)\\"')
        self._tab = re.compile(r'(?<!\\)\\t')
        self.info = {}
        self._process = None
        self._receiver = None
        self._message = []
        self._stepping = False
        self._locals = {}
        self._watches = []
        self._watch_count = 0
        self._breakpoints = {}
        self._stack_frames = []
        self._current_frame = 0
        self._semaphore = threading.Semaphore(0)  # command dispatch lock

        self._translate_map = {
            '^': self._translate_result,
            '~': self._translate_cli_command_response,
            '@': self._translate_output,
            '&': self._translate_internal_msg,
            '*': self._translate_gdb_status_changes,
            '=': self._translate_run_status_changes,
        }

        super(DebugRemoteSession, self).__init__()
        if self._attach:
            self.debug_attach_remote(self._executable)
        else:
            self.debug_remote(self._executable)

    @staticmethod
    def translate_string(text):
        result = ''
        for x in text:
            result += chr(1024 - ord(x))
        return result

    @property
    def is_paused(self):
        return self._paused

    @property
    def is_stepping(self):
        return self._stepping

    def register_stack(self, info):
        """add a new stack frame entry.
        The calls for this method are expected to be
        additive from topmost to bottommost entries."""
        self._stack_frames.append(info)

    def register_argument(self, name, value, order):
        """add a new frame argument.
        If the argument already exists, only the value is
        replaced.
        If if exist a local with the same name, it will be
        respected, because locals override (and hide) arguments
        Return True if the arg was registered
        """
        if name in self._locals:
            element = self._locals[name]
            if type(element) is DebugArgInfo:
                element._value = value  # don't use setter because trigger side effects
                element._order = order
                return True
            else:
                return False
        else:
            self._locals[name] = DebugArgInfo(name=name, value=value, order=order)
        return True

    def register_local(self, name, value, order):
        """add a new frame local.
        If the local already exists, only the value is
        replaced.
        This method always success
        """
        if name in self._locals:
            element = self._locals[name]
            element._value = value  # don't use setter because it would trigger side effects
            element._order = order
        else:
            self._locals[name] = DebugLocalInfo(name=name, value=value, order=order)

    def clean_locals(self):
        """remove locals
        This method only remove locals (not args) for
        current stack frame"""
        names = [x for x in self._locals.keys() if type(self._locals[x]) is DebugLocalInfo]
        for name in names:
            del self._locals[name]

    def clean_args(self):
        """remove args
        This method only remove args (not locals) for
        current stack frame"""
        names = [x for x in self._locals.keys() if type(self._locals[x]) is DebugArgInfo]
        for name in names:
            del self._locals[name]

    def clean_stack(self):
        """remove stack frames"""
        while len(self._stack_frames) > 0:
            del self._stack_frames[0]
        self._current_frame = 0

    @property
    def stack(self):
        """return stack frame"""
        return self._stack_frames

    @property
    def selected_thread(self):
        return self._selected_thread

    @selected_thread.setter
    def selected_thread(self, value):
        if self._selected_thread != value:
            if type(self._selected_thread) is DebugThreadInfo:
                self._selected_thread._is_current = False
            self._selected_thread = value
            if type(self._selected_thread) is DebugThreadInfo:
                self._selected_thread._is_current = True

    def post_debugger_info(self, msg):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.DEBUGGER_INFO, message=msg))

    def post_unknown_debug_info(self, msg):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UNKNOWN_DEBUG_INFO, msg))

    def post_debug_end(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.DEBUG_ENDED))

    def post_update_breakpoints(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_BREAKPOINTS_INFO))

    def post_update_threads(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_THREADS_INFO))

    def post_update_locals(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_LOCALS_INFO))

    def post_update_stack_frame(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_STACK_FRAME))

    def post_user_command_response(self, msg):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.USER_COMMAND_RESPONSE, msg + "\n"))

    def post_file_line(self, file_, line_):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.FILE_LINE_INFO, (file_, int(line_))))

    def post_output(self, message):
        handler = self._frame.GetEventHandler()
        handler.AddPendingEvent(wxx.OutputEvent(message + "\n"))

    @property
    def frame(self):
        return self._frame

    @property
    def threads(self):
        """return the thread dictionary"""
        return self._threads

    @property
    def locals(self):
        """return an ordered list of frame locals corresponding to localInfo.
        The returned list excludes argList"""
        return sorted((x for x in self._locals.values() if type(x) is DebugLocalInfo), key=lambda x: x.order)

    @property
    def args(self):
        """return an ordered list of frame args corresponding to localInfo.
        The returned list excludes argList"""
        return sorted((x for x in self._locals.values() if type(x) is DebugArgInfo), key=lambda x: x.order)

    @property
    def watches(self):
        """return a list of watch expressions"""
        return self._watches

    @property
    def breakpoints(self):
        """return the dict of breakpoints"""
        return self._breakpoints

    def add_watch_expression(self, expression):
        self._watch_count += 1
        name = 'var_{}'.format(self._watch_count)
        w = WatchExpression(name, expression)
        self._watches.append(w)
        self.do_add_watch(w)
        self.do_query_watch(w)

    def activate_ui(self):
        """Activate the panes"""
        config = context.get_config()
        self._old_perspective = self._frame.m_mgr.SavePerspective()
        self._perspective = config.Read('/debuggers/gdb/perspectives/watch_book', '')
        status = {}
        for x in self._debug_panes:
            status[x.name] = config.ReadBool('/debuggers/gdb/panes/{}/visible'.format(x.name), True)
            x.index = config.ReadInt('/debuggers/gdb/panes/{}/index'.format(x.name), wx.NOT_FOUND)
        selected_pane = config.ReadInt('/debuggers/gdb/panes/selected', wx.NOT_FOUND)
        self._frame.clean_output_pane()
        self.add_debug_menu()
        self.add_stack_pane()
        self.add_debug_console_pane()
        self.activate_output_pane()
        self.activate_debug_watch_book(status)
        self.bind_events()
        if len(self._perspective):
            this_pane = self._frame.m_mgr.GetPane('watch_book')
            self._frame.m_mgr.LoadPaneInfo(self._perspective, this_pane)
        if selected_pane != wx.NOT_FOUND:
            self.m_debug_info.ChangeSelection(selected_pane)
        self._frame.m_mgr.Update()

    def bind_events(self):
        for element in self._debug_panes:
            element.bind_events()
        self._frame.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE,
                         self.on_debug_pane_closed, self.m_debug_info)
        self._frame.m_mgr.Bind(aui.EVT_AUI_PANE_CLOSE, handler=self.on_debug_watch_book_closed)

    def on_debug_pane_closed(self, event):
        event.Skip()
        index = self.m_debug_info.GetSelection()
        if index != wx.NOT_FOUND:
            try:
                instance = self.m_debug_info.GetPage(index)
                page = next(x for x in self._debug_panes if x.instance == instance)
                page.close()
                event.Veto()
            except StopIteration:
                pass

    def on_debug_watch_book_closed(self, event):
        """Process a close book event"""
        event.Skip()
        if event.GetPane().name == 'watch_book':
            for x in self._debug_panes:
                if x.instance is not None:
                    x.close()

    def unbind_events(self):
        self._frame.m_mgr.Unbind(aui.EVT_AUI_PANE_CLOSE, handler=self.on_debug_watch_book_closed)
        self._frame.Unbind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.m_debug_info)
        for element in self._debug_panes:
            element.unbind_events()

    def deactivate_ui(self):
        """Deactivate the ui"""
        element = self._frame.m_mgr.GetPane('watch_book')
        self._perspective = self._frame.m_mgr.SavePaneInfo(element)
        config = context.get_config()
        config.Write('/debuggers/gdb/perspectives/watch_book', self._perspective)
        for x in self._debug_panes:
            if x.instance is not None:
                x.index = self.m_debug_info.GetPageIndex(x.instance)
            config.WriteBool('/debuggers/gdb/panes/{}/visible'.format(x.name), x.shown)
            config.WriteInt('/debuggers/gdb/panes/{}/index'.format(x.name), x.index)
        if self.m_debug_info.GetPageCount() > 0:
            selection = self.m_debug_info.GetSelection()
        else:
            selection = wx.NOT_FOUND
        config.WriteInt('/debuggers/gdb/panes/selected', selection)
        self.unbind_events()
        self.deactivate_debug_watch_book()
        self.remove_debug_stack_pane()
        self.remove_debug_console_pane()
        self.delete_debug_menu()
        self._frame.m_mgr.LoadPerspective(self._old_perspective)
        self._frame.m_mgr.Update()

    def activate_output_pane(self):
        """Activate the output pane"""
        if self._frame.m_aux_panes == self._frame.m_aux_output_pane:
            return True
        candidate = [wx.NOT_FOUND]
        candidate.extend(i for i in range(0, self._frame.m_aux_panes.GetPageCount())
                         if self._frame.m_aux_panes.GetPage(i) == self._frame.m_aux_output_pane)
        i = max(candidate)
        if i == wx.NOT_FOUND:
            return False
        self._frame.m_aux_panes.SetSelection(i)
        return True

    def remove_debug_stack_pane(self):
        """Remove the stack pane"""
        book = self._frame.m_aux_panes
        for i in range(0, book.GetPageCount()):
            if book.GetPage(i) == self.m_stack:
                book.DeletePage(i)
                return

    def remove_debug_console_pane(self):
        """Remove the debug command pane"""
        book = self._frame.m_aux_panes
        for i in range(0, book.GetPageCount()):
            if book.GetPage(i) == self.m_command:
                book.DeletePage(i)
                return

    def add_debug_menu(self):
        """Add normal debug menu to main window"""
        bar = self._frame.m_menubar1
        help_ = self._frame.menuHelp
        self.menu_pos = bar.FindMenu(help_.menu_bar_title)
        if self.menu_pos == wx.NOT_FOUND:
            # couldn't add menu!!
            self._menu = None
            return
        self._menu = wxx.Menu()
        submenu_windows = wxx.Menu()
        for element in self._debug_panes:
            element.add_menu_item(submenu_windows)
        self._menu.AppendSubMenu(submenu_windows, u"Debug info panels")
        self._menu.AppendSeparator()
        # Populate debug menu
        tools.clone_mnu(self._handler.debug_menu, self._menu)
        self._handler.register_menu('Debug', self._menu)
        self._frame.add_main_menu("Debug", self._menu, pos=self.menu_pos)

    def delete_debug_menu(self):
        """Remove normal debug menu from main window"""
        self._handler.unregister_menu('Debug')
        if self._menu is not None:
            self._frame.remove_main_menu("Debug", self._menu)
            self._menu.Destroy()
            self._menu = None

    def add_stack_pane(self):
        """Add stack frame pane"""
        self.m_stack = pane.StackFrame(self._frame.m_aux_panes, self)
        self._frame.m_aux_panes.AddPage(self.m_stack, u"stack frame", False)

    def add_debug_console_pane(self):
        """Add debug command pane"""
        self.m_command = pane.DebugConsolePane(self._frame.m_aux_panes, self)
        self._frame.m_aux_panes.AddPage(self.m_command, u"debug console", False)

    def activate_debug_watch_book(self, status):
        """Activate the watch panel"""
        self.m_debug_info = wxx.AuiNotebook(self._frame, wx.ID_ANY, wx.DefaultPosition,
                                            wx.Size(200, -1), aui.AUI_NB_DEFAULT_STYLE)
        pane_info = aui.AuiPaneInfo().Name('watch_book').Right().PinButton(True).Dock().Resizable()
        pane_info.FloatingSize(wx.Size(120, -1)).DockFixed(False).Row(1).Layer(4)
        self._frame.m_mgr.AddPane(self.m_debug_info, pane_info)
        sorted_panes = sorted(self._debug_panes, key=lambda x: x.index)
        for element in sorted_panes:
            element.init_pane(self, status[element.name])

    def deactivate_debug_watch_book(self):
        self._frame.m_mgr.DetachPane(self.m_debug_info)
        for element in self._debug_panes:
            element.uninit_pane()
        self.m_debug_info.Destroy()

    def update_debug_locals_ui(self):
        """Update the visual info"""
        self._debug_panes[0].update_data()

    def update_debug_stack_ui(self):
        """Update the visual info"""
        self.m_stack.update_data()

    def update_debug_breakpoints_ui(self):
        """Update the visual info"""
        self._debug_panes[2].update_data()

    def update_debug_console_pane_ui(self, response):
        """Update debug command pane"""
        self.m_command.update_data(response)

    def update_debug_threads_ui(self):
        """Update the visual info"""
        self._debug_panes[3].update_data()

    def _translate_result(self, text):
        """
        In addition to a number of out-of-band notifications, the response to a gdb/mi command
        includes one of the following result indications:

        "^done" [ "," results ]
            The synchronous operation was successful, results are the return values.
        "^running"
            This result record is equivalent to `^done'. Historically, it was output instead of `^done'
            if the command has resumed the target. This behaviour is maintained for backward
            compatibility, but all front-ends should treat `^done' and `^running' identically and
            rely on the `*running' output record to determine which threads are resumed.
        "^connected"
            gdb has connected to a remote target.
        "^error" "," c-string
            The operation failed. The c-string contains the corresponding error message.
        "^exit"
            gdb has terminated.
        ----
        text : received message without first marker (^).
        """
        if len(text) > 0:
            for r_expr, dispatch in self._on_result_status_map:
                match = r_expr.match(text)
                if match is not None:
                    dispatch(match)
                    return True
            self.post_unknown_debug_info(
                'Error: cannot find syntax matching result status "{text}".'.format(text=text))
        return True

    def on_done(self, match):
        """
        Interesting information received with done results:

            stack-args=[<frame-info>(,<frame-info>)]

        where frame-info is of the form:

            frame={level="<num>",args=[<arg_descriptor>(,<arg_descriptor>)*]

        where arg_descriptor is

            name="<name>"|{name="<name>,value="<value>"}

        """
        results = match.group('results')
        print(results)
        if results is not None and len(results) > 0:
            for r_expr, dispatch in self._on_done_results_map:
                match = r_expr.match(results)
                if match is not None:
                    dispatch(match)
                    return True
        pass

    def on_stack_frame(self, match):
        """Process the info about the stack frame list info.
        This info is routine query at breakpoint."""
        stack_frame_list = match.group('stack_frame_list')
        while stack_frame_list is not None:
            match = self._stack_frame_entry.match(stack_frame_list)
            stack_frame_list = match and match.group('stack_frame_list')
            if match is not None:
                kwargs = {
                    'level': match.group('num'),
                    'address': match.group('address'),
                    'func': match.group('func'),
                    'line': match.group('line'),
                    'from': match.group('from'),
                    'arch': match.group('arch'),
                    'fullname': '',
                    'file': ''
                }
                full_name = match.group('fullname')
                file = match.group('file')
                if full_name is not None:
                    kwargs['fullname'] = self.translate_remote_to_local_file(full_name)
                if file:
                    kwargs['file'] = self.translate_remote_to_local_file(file)
                self._stack_frames.append(DebugFrameInfo(**kwargs))
        self.post_update_stack_frame()

    def on_stack_arguments(self, match):
        """
        Process the info about current thread stack frame arguments.
        The frame level 0 is for current stack frame, and level n
        is for n outer deep frame.
        Normal result is to have only the current level info.

        regular expressions (very simplified, only for illustration, some escape removed):

            _stack_arg_list -> 'frame=[{]level="(?P<num>[^"]+)",args=[(?P<arg_list>.*)]}(,(?P<stack_arg_list>.*))?'
            _arg_list -> r'([{])?name="(?P<name>[^"]+)"(?(1),value="(?P<value>[^"]*)"[}])(,(?P<arg_list>.*))?'

        The regular expression cannot be parsed as expressed, it's only a simplification that don't take care of
        values interaction. The expression must be parsed carefully by step and mixing's

            _stack_arg_list -> r'stack-args=[(?P<stack_arg_list_mixin>.*)]'
            _stack_arg_list_mixin -> r'frame={level="(?P<num>[^"]+)",args=[(?P<arg_list_mixin>.*)'
            _arg_list_mixin  ->
                r'([{])?name="(?P<name>[^"]+)"(?(1),value="(?P<value>((\\[."\'])|[^"])*)"[}])
                ((,(?P<arg_list_mixin>.*))|(]}(,(?P<stack_arg_list_mixin>.*))?))'

        """

        stack_arg_list_mixin = match.group('stack_arg_list_mixin') or ''
        changes = False
        while len(stack_arg_list_mixin):
            match = self._stack_arg_list_mixin.match(stack_arg_list_mixin)
            if match is None:
                self.post_unknown_debug_info(
                    'Error: failed to parse stack arg list mixin :{}.'.format(stack_arg_list_mixin))
                return

            level = int(match.group('num'))
            arg_list_mixin = match.group('arg_list_mixin') or ''
            order = 0
            while len(arg_list_mixin) > 0:
                match = self._arg_list_mixin.match(arg_list_mixin)
                if match is None:
                    self.post_unknown_debug_info('Error: failed to parse arg list mixin :{}.'.format(arg_list_mixin))
                    return
                # We actually only requests arguments for the current stack frame
                # for these values, we add to arguments
                if level == 0:
                    name = match.group('name')
                    value = match.group('value')
                    if self.register_argument(name, self.decode_msg(value), order):
                        changes = True
                arg_list_mixin = match.group('arg_list_mixin') or ''
                stack_arg_list_mixin = match.group('stack_arg_list_mixin') or ''
                order = order + 1
        if changes:
            self.post_update_locals()

    def on_stack_locals(self, match):
        """
        Process the info about current thread stack locals.

        The regular expression cannot be parsed as expressed, it's only a simplification that don't take care of
        values interaction, for example. The expression must be parsed carefully by step and mixing's



        _stack_locals_list -> 'locals=[(?P<stack_local_list_mixin>.*)]'
        _stack_local_list_mixin -> '({)?name="(?P<name>[^"]+)"(?(1),value="(?P<value>((\\[."\'])|[^"])*)"})
            (,(?P<stack_local_list_mixin>.*))?'

        """
        stack_local_list_mixin = match.group('stack_local_list_mixin') or ''
        order = 0
        while len(stack_local_list_mixin) > 0:
            match = self._stack_local_list_mixin.match(stack_local_list_mixin)
            if match is None:
                self.post_unknown_debug_info('Error: failed to parse stack local list mixin :{} ({}).'.format(
                    stack_local_list_mixin, list(stack_local_list_mixin)))
                return
            name = match.group('name')
            value = match.group('value')
            self.register_local(name, self.decode_msg(value), order)
            stack_local_list_mixin = match.group('stack_local_list_mixin') or ''
            order = order + 1
        if order > 0:
            self.post_update_locals()

    def on_connected(self):
        self.post_debugger_info('Remote target connected.')

    def on_error(self, match):
        self.post_debugger_info('Error: {}.'.format(match.group('message')))

    def on_exit(self):
        self.post_debugger_info('Debugger session exited.')

    def _translate_cli_command_response(self, text):
        """
        "~" string-output
        The console output stream contains text that should be displayed in the CLI console window.
        It contains the textual responses to CLI commands.
        ----
        text : received message without first marker (~).
        """
        self.post_user_command_response(self.decode_msg(text))
        return True

    def _translate_output(self, text):
        """
        "@" string-output
        The target output stream contains any textual output from the running target.
        This is only present when gdb event loop is truly asynchronous, what's only true for remote targets.
        ----
        text : received message without first marker (@).
        """
        self.post_output(text)
        return True

    def _translate_internal_msg(self, text):
        """
        "&" string-output
        The log stream contains debugging messages being produced by gdb internals.
        ----
        text : received message without first marker (&).
        """
        self.post_user_command_response(self.decode_msg(text))
        return True

    def _translate_gdb_status_changes(self, text):
        """
        Async records are used to notify the gdb/mi client of additional changes that have occurred.
        Those changes can either be a consequence of gdb/mi commands (e.g., a breakpoint modified)
        or a result of target activity (e.g., target stopped).

        The following is the list of possible async records related to running status:

        *running,thread-id="thread"
            (see on_thread_running for details)

        *stopped,reason="reason",thread-id="id",stopped-threads="stopped",core="core"
            (see on_stopped for details)

        ----
        text : received message without first marker (*).
        """
        if len(text) > 0:
            for reg_expr in self._on_gdb_status_changes_map:
                match = reg_expr.match(text)
                if match is not None:
                    self._on_gdb_status_changes_map[reg_expr](match)
                    return True
        return True

    def on_thread_running(self, match):
        """
        running,thread-id="thread"

            The target is now running. The thread field tells which specific thread is now running,
            and can be `all' if all threads are running. The frontend should assume that no interaction
            with a running thread is possible after this notification is produced.
            The frontend should not assume that this notification is output only once for any command.
            gdb may emit this notification several times, either for different threads, because it cannot
            resume all threads together, or even for a single thread, if the thread must be stepped
            though some code before letting it run freely.
        """
        # thread_running = r'running,thread-id="(?P<id>[^"]+)".*'
        if not self._first_running:
            self._first_running = True
            self._translate_output('\n')  # because informational messages
        thread_identifier = match.group('id')
        if thread_identifier == 'all':
            for id_ in self._threads:
                self._threads[id_].running = True
            self._stepping = False
            self.post_debugger_info('all threads running')
            return
        if thread_identifier not in self._threads:
            self.post_debugger_info(
                'Error: unknown thread {} running.'.format(thread_identifier))
            return
        self._threads[thread_identifier].running = True
        if self._selected_thread.identifier == thread_identifier:
            self._stepping = False
        self.post_update_threads()
        self.post_debugger_info('thread {} running'.format(thread_identifier))

    def _stopped_info(self, info):
        """
        the stopped info is variable and looks
        not fully explained in documentation.
        We use the same strategy as for breakpoint info
        for extracting all information as we can.
        """
        r = {}
        for x in self._stopped_info_fields:
            t = self._stopped_info_fields[x].search(info)
            if t is not None:
                r[x] = t.group('value')
        return r

    def on_stopped(self, match):
        """
        *stopped,reason="reason",thread-id="id",stopped-threads="stopped",core="core"

            The target has stopped. The reason field can have one of the following values:

            breakpoint-hit
                A breakpoint was reached.
            watchpoint-trigger
                A watchpoint was triggered.
            read-watchpoint-trigger
                A read watchpoint was triggered.
            access-watchpoint-trigger
                An access watchpoint was triggered.
            function-finished
                An -exec-finish or similar CLI command was accomplished.
            location-reached
                An -exec-until or similar CLI command was accomplished.
            watchpoint-scope
                A watchpoint has gone out of scope.
            end-stepping-range
                An -exec-next, -exec-next-instruction, -exec-step, -exec-step-instruction or similar
                CLI command was accomplished.
            exited-signalled
                The inferior exited because of a signal.
            exited
                The inferior exited.
            exited-normally
                The inferior exited normally.
            signal-received
                A signal was received by the inferior.
            solib-event
                The inferior has stopped due to a library being loaded or unloaded.
                This can happen when stop-on-solib-events (see Files) is set or when a catch load
                or catch unload catchpoint is in use (see Set Catchpoints).
            fork
                The inferior has forked. This is reported when catch fork (see Set Catchpoints)
                has been used.
            vfork
                The inferior has vforked. This is reported in when catch vfork (see Set Catchpoints)
                has been used.
            syscall-entry
                The inferior entered a system call. This is reported when catch syscall
                (see Set Catchpoints) has been used.
            syscall-entry
                The inferior returned from a system call. This is reported when catch syscall
                (see Set Catchpoints) has been used.
            exec
                The inferior called exec. This is reported when catch exec
                (see Set Catchpoints) has been used.

        The id field identifies the thread that directly caused the stop – for example by hitting
        a breakpoint. Depending on whether all-stop mode is in effect (see All-Stop Mode),
        gdb may either stop all threads, or only the thread that directly triggered the stop.
        If all threads are stopped, the stopped field will have the value of "all".
        Otherwise, the value of the stopped field will be a list of thread identifiers.
        Presently, this list will always include a single thread, but frontend should be prepared
        to see several threads in the list. The core field reports the processor core on which
        the stop event has happened. This field may be absent if such information is not available.

        note:
        The stopped message includes more information as what we found in online docs.
        One of the missing info is about file and line. We use _stopped_info method for
        extracting this info as dict.
        """
        # stopped = r'stopped,(?P<info>.*)",
        # stopped-threads="(?P<stopped>[^"]+)",core="(?P<core>[^"]+)".*'

        info = self._stopped_info(match.group('info'))

        # stopped is mainly a routine dispatch of breakpoint, watch, catch or step range
        # but first we treat end-debugging conditions
        #    exited-signalled
        #        The inferior exited because of a signal.
        #    exited
        #        The inferior exited.
        #    exited-normally
        reason = info.get('reason', None)
        if reason is not None:
            # Ok, do filtering
            if re.match(r'exited(-.*)+', reason, re.DOTALL):
                # this is a normal or exit situation after signal. We're done
                self._attach = False
                self.done = True
                self.do_quit()
                return

        file_ = None
        line_ = None
        if 'line' in info:
            line_ = info['line']
        if 'fullname' in info:
            file_ = info['fullname']
        elif 'file' in info:
            file_ = info['file']
        if file_ is None or line_ is None:
            # fix at 16-01-2020 : if there's no file/line info, we continue stepping
            # we must change this to set as an user option later
            self.do_step()
            return
            # old code before fix: don't delete it.
            # if 'breakpoint_number' in info:
            #     breakpoint_id = info['breakpoint_number']
            #     if breakpoint_id not in self._breakpoints:
            #         self.post_debugger_info('Error: stopped at unknown breakpoint {}.'.format(breakpoint_id))
            #         return
            #     breakpoint_ = self._breakpoints[breakpoint_id]
            #     if file_ is None:
            #         file_ = breakpoint_.file
            #         if line_ is None:
            #             line_ = breakpoint_.line
        else:
            file_ = self.translate_remote_to_local_file(file_)
            # we always post absolute path
            # fix at 16-01-2020 : if file don't exists locally, send warn and step
            if not os.path.exists(file_):
                # this info may be useful for other purposes ... displaying it is annoying
                # self.post_debugger_info(
                #    'Error: local file {} not found. stepping...".'.format(file_))
                self.do_step()
                return
            else:
                self.post_file_line(file_, line_)

        if reason is None:
            # unknown reason, but stepping
            self.post_debugger_info('Error: stopped by unknown reason.')
        if 'stopped-threads' in info:
            thread_identifiers = info['stopped-threads']
            if thread_identifiers == 'all':
                for id_ in self._threads:
                    self._threads[id_].running = False
                self._stepping = True
            else:
                for thread_identifier in thread_identifiers.split(','):
                    if len(thread_identifiers) == 0:
                        continue
                    if thread_identifier not in self._threads:
                        self.post_debugger_info('Error: unknown thread {} stopped.'.format(thread_identifier))
                    else:
                        self._threads[thread_identifier].running = False
        elif 'thread-id' in info:
            thread_identifier = info['thread-id']
            if thread_identifier not in self._threads:
                self.post_debugger_info('Error: unknown thread {} stopped.'.format(thread_identifier))
            else:
                self._threads[thread_identifier].running = False
        if self._selected_thread is not None:
            if self._selected_thread.running:
                self._stepping = False
                self.post_update_threads()
                return
        if reason is not None:
            self.post_debugger_info('Stopped by {}.'.format(reason))
        # Attempt to send file/line
        self.post_update_threads()
        self.do_query_stack()
        self.do_query_locals()
        self.do_query_args()
        self.do_query_watches()
        self._stepping = True

    def translate_remote_to_local_file(self, file_):
        """Converts a remote path into local path"""
        if os.path.isabs(file_):
            return os.path.join(self._local_source_dir, file_[len(self._source_dir):])
        else:
            return os.path.abspath(os.path.join(self._local_source_dir, file_))

    def _translate_run_status_changes(self, text):
        """
        Async records are used to notify the gdb/mi client of additional changes that have occurred.
        Those changes can either be a consequence of gdb/mi commands (e.g., a breakpoint modified)
        or a result of target activity (e.g., target stopped).

        The following is the list of possible async records related to module status:

        =thread-group-added,id="id"
        =thread-group-removed,id="id"
            A thread group was either added or removed. The id field contains the gdb identifier
            of the thread group. When a thread group is added, it generally might not be associated
            with a running process. When a thread group is removed, its id becomes invalid and
            cannot be used in any way.

        =thread-group-started,id="id",pid="pid"
            A thread group became associated with a running program, either because the program was
            just started or the thread group was attached to a program. The id field contains the
            gdb identifier of the thread group. The pid field contains process identifier, specific
            to the operating system.

        =thread-group-exited,id="id"[,exit-code="code"]
            A thread group is no longer associated with a running program, either because the program
            has exited, or because it was detached from. The id field contains the gdb identifier
            of the thread group. code is the exit code of the inferior; it exists only when the
            inferior exited with some code.

        =thread-created,id="id",group-id="gid"
        =thread-exited,id="id",group-id="gid"
            A thread either was created, or has exited. The id field contains the gdb identifier
            of the thread. The gid field identifies the thread group this thread belongs to.

        =thread-selected,id="id"
            Informs that the selected thread was changed as result of the last command.
            This notification is not emitted as result of -thread-select command but is emitted
            whenever an MI command that is not documented to change the selected thread actually
            changes it. In particular, invoking, directly or indirectly (via user-defined command),
            the CLI thread command, will generate this notification.

            We suggest that in response to this notification, front ends highlight the selected thread
            and cause subsequent commands to apply to that thread.

        =library-loaded,...
            Reports that a new library file was loaded by the program. This notification has 4
            fields—id, target-name, host-name, and symbols-loaded. The id field is an opaque identifier
            of the library. For remote debugging case, target-name and host-name fields give the name
            of the library file on the target, and on the host respectively. For native debugging,
            both those fields have the same value. The symbols-loaded field is emitted only for backward
            compatibility and should not be relied on to convey any useful information. The thread-group
            field, if present, specifies the id of the thread group in whose context the library was
            loaded. If the field is absent, it means the library was loaded in the context of all
            present thread groups.

        =library-unloaded,...
            Reports that a library was unloaded by the program. This notification has 3 fields—id,
            target-name and host-name with the same meaning as for the =library-loaded notification.
            The thread-group field, if present, specifies the id of the thread group in whose context
            the library was unloaded. If the field is absent, it means the library was unloaded in
            the context of all present thread groups.

        =breakpoint-created,bkpt={...}
        =breakpoint-modified,bkpt={...}
        =breakpoint-deleted,bkpt={...}
            Reports that a breakpoint was created, modified, or deleted, respectively. Only user-visible
            breakpoints are reported to the MI user.

            The bkpt argument is of the same form as returned by the various breakpoint commands;
            See GDB/MI Breakpoint Commands.
        ----
        text : received message without first marker (=).
        """
        if len(text) > 0:
            for reg_expr in self._on_run_status_changes_map:
                match = reg_expr.match(text)
                if match is not None:
                    self._on_run_status_changes_map[reg_expr](match)
                    return True
            self.post_unknown_debug_info(
                'Error: cannot find syntax matching run status change "{text}".'.format(text=text))
        return True

    def on_thread_group_add(self, match):
        id_ = match.group('id')
        if len(id_) == 0:
            self.post_debugger_info('Error: missing identifier while adding a new thread group.')
            return
        if id_ in self._thread_groups:
            self.post_debugger_info('Error: duplicated identifier while adding a new thread group.')
            return
        self._thread_groups[id_] = DebugThreadGroup(id_)
        self.post_debugger_info('New thread group "{identifier}".'.format(identifier=id_))

    def on_thread_group_removed(self, match):
        id_ = match.group('id')
        if len(id_) == 0:
            self.post_debugger_info('Error: missing identifier while removing a thread group.')
            return
        if id_ not in self._thread_groups:
            self.post_debugger_info(
                'Error: removing unknown thread group "{identifier}".'.format(identifier=id_))
            return
        group = self._thread_groups[id_]
        if type(self._selected_thread) is DebugThreadInfo and self._selected_thread.group == group:
            self.selected_thread = None
        self._thread_groups[id_].delete()
        del self._thread_groups[id_]
        self.post_debugger_info('Deleted thread group "{identifier}".'.format(identifier=id_))
        self.post_update_threads()

    def on_thread_group_started(self, match):
        """
        start a thread group.

        notes
        It looks like we can received this event without previous thread group add notification,
        so we must handle this situation silently and add a missing thread group.
        Also, it seems that old gdb named this event as 'thread-group-created' and no pid were present.
        """
        id_ = match.group('id')
        if len(id_) == 0:
            self.post_debugger_info('Error: missing identifier while starting a thread group.')
            return
        if id_ not in self._thread_groups:
            self._thread_groups[id_] = DebugThreadGroup(id_)
        process_identifier = match.group('pid')
        if len(process_identifier) == 0:
            self.post_debugger_info('Error: missing process identifier while starting a thread group.')
            return
        thread_group = self._thread_groups[id_]
        thread_group.pid = process_identifier
        thread_group.started = True
        self.post_debugger_info(
            'Started thread group "{identifier}" (process "{process_identifier})".'.format(
                identifier=id_,
                process_identifier=process_identifier))

    def on_thread_group_exited(self, match):
        """
        exit a thread group.

        notes
        From the meaning of thread_group_started, it seems that this notification may
        simply followed by thread group deletion. We must test if we receive thread notifications
        about that.
        """
        id_ = match.group('id')
        if len(id_) == 0:
            self.post_debugger_info('Error: missing identifier while exiting a thread group.')
            return
        if id_ not in self._thread_groups:
            self.post_debugger_info(
                'Error: exiting from unknown thread group "{identifier}".'.format(
                    identifier=id_))
            return
        exit_code = match.group('code')
        thread_group = self._thread_groups[id_]
        thread_group.started = False
        if exit_code is not None:
            thread_group.exit_code = exit_code
            self.post_debugger_info(
                'Exited thread group "{identifier}" with status "{exit_code}".'.format(
                    identifier=id_, exit_code=exit_code))
        else:
            self.post_debugger_info(
                'Exited thread group "{identifier}".'.format(identifier=id_))

    def on_thread_created(self, match):
        group_identifier = match.group('gid')
        if len(group_identifier) == 0:
            self.post_debugger_info(
                'Error: missing thread group identifier while adding a thread.')
            return
        if group_identifier not in self._thread_groups:
            self.post_debugger_info(
                'Error: unknown thread group "{group_identifier}" while adding a thread.'.format(
                    group_identifier=group_identifier))
            return
        thread_identifier = match.group('id')
        if len(thread_identifier) == 0:
            self.post_debugger_info(
                'Error: missing identifier while adding new thread to thread group "{id}".'.format(
                    id=group_identifier))
            return
        thread_group = self._thread_groups[group_identifier]
        if thread_identifier in thread_group.threads:
            self.post_debugger_info(
                'Error: duplicated identifier while adding new thread to thread group "{id}".'.format(
                    id=group_identifier))
            return
        thread = DebugThreadInfo(thread_group, thread_identifier)
        self._threads[thread_identifier] = thread
        if self._selected_thread is None:
            self.selected_thread = thread
        self.post_debugger_info(
            'Add new thread "{thread_id}" to thread group "{group_id}"'.format(
                thread_id=thread_identifier,
                group_id=group_identifier))
        self.post_update_threads()

    def on_thread_exited(self, match):
        group_identifier = match.group('gid')
        if len(group_identifier) == 0:
            self.post_debugger_info('Error: missing thread group identifier while exiting a thread.')
            return
        if group_identifier not in self._thread_groups:
            self.post_debugger_info(
                'Error: unknown thread group "{group_identifier}" while exiting a thread.'.format(
                    group_identifier=group_identifier))
            return
        thread_identifier = match.group('id')
        if len(thread_identifier) == 0:
            self.post_debugger_info(
                'Error: missing identifier while thread exit from thread group "{id}".'.format(
                    id=group_identifier))
            return
        thread_group = self._thread_groups[group_identifier]
        if thread_identifier not in thread_group.threads:
            self.post_debugger_info(
                'Error: unknown thread "{thread_id}" while exiting from  thread group "{group_id}".'.format(
                    thread_id=thread_identifier,
                    group_id=group_identifier))
            return
        thread = thread_group.threads[thread_identifier]
        if self._selected_thread == thread:
            self.selected_thread = None
        thread.delete()
        del self._threads[thread_identifier]
        self.post_debugger_info('Exited thread "{thread_id}" from thread group "{group_id}"'.format(
            thread_id=thread_identifier,
            group_id=group_identifier))
        self.post_update_threads()

    def on_thread_selected(self, match):
        thread_identifier = match.group('id')
        if len(thread_identifier) == 0:
            self.post_debugger_info(
                'Error: missing thread identifier while selecting a thread.')
            return
        for group_identifier in self._thread_groups:
            thread_group = self._thread_groups[group_identifier]
            if thread_identifier in thread_group.threads:
                self.selected_thread = thread_group.threads[thread_identifier]
                self.post_debugger_info(
                    'Selected thread "{thread_id}" belonging to thread group "{group_id}".'.format(
                        thread_id=thread_identifier,
                        group_id=group_identifier))
                self.post_update_threads()
                return
        if self._attach:
            if 'unknown' not in self._thread_groups:
                self._thread_groups['unknown'] = DebugThreadGroup('unknown')
                thread_group = self._thread_groups['unknown']
                thread_group.pid = 'unknown'
                thread_group.started = True
                self.post_debugger_info('Started unknown thread group')
            thread_group = self._thread_groups['unknown']
            if thread_identifier not in thread_group.threads:
                thread = DebugThreadInfo(thread_group, thread_identifier)
                self._threads[thread_identifier] = thread
                self.post_debugger_info(
                    'Add new thread "{thread_id}" to thread group "unknown"'.format(
                        thread_id=thread_identifier))
            self.selected_thread = thread_group.threads['unknown']
            self.post_debugger_info(
                'Selected thread "{thread_id}" from thread group "unknown".'.format(
                    thread_id=thread_identifier))
            self.post_update_threads()
        else:
            self.post_debugger_info(
                'Error: can\'t select unknown thread "{thread_id}".'.format(
                    thread_id=thread_identifier))

    def on_library_loaded(self, match):
        library_identifier = match.group('id')
        if len(library_identifier) == 0:
            self.post_debugger_info(
                'Error: loading new library with missing identifier.')
            return
        if library_identifier in self._libraries:
            self.post_debugger_info(
                'Error: duplicated library identifier "{library_identifier}" while loading new library.'.format(
                    library_identifier=library_identifier))
            return

        target_name = match.group('target_name')
        if len(target_name) == 0:
            self.post_debugger_info(
                'Error: loading new library with missing target name.')
            return

        host_name = match.group('host_name')
        if len(host_name) == 0:
            self.post_debugger_info('Error: loading new library with missing host name.')
            return

        library = DebugLibraryInfo(library_identifier, target_name, host_name)
        self._libraries[library_identifier] = library

        # auxiliary info
        group_identifier = match.group('thread_group_id')
        from_address = match.group('from_address')
        to_address = match.group('to_address')

        if group_identifier is not None and len(group_identifier) > 0:
            if group_identifier not in self._thread_groups:
                self.post_debugger_info(
                    'Warning : loading library "{host_name}" with unknown thread group "{group_identifier}".'.format(
                        host_name=host_name,
                        group_identifier=group_identifier))
            else:
                library.thread_group = group_identifier
        if from_address is not None and len(from_address) > 0:
            library.from_address = from_address
        if to_address is not None and len(to_address) > 0:
            library.to_address = to_address
        self.post_debugger_info(
            'Loaded library {}.'.format(library))

    def on_library_unloaded(self, match):
        library_identifier = match.group('id')
        if len(library_identifier) == 0:
            self.post_debugger_info(
                'Error: unloading library with missing identifier.')
            return
        if library_identifier not in self._libraries:
            self.post_debugger_info(
                'Error: unloading unknown library "{}".'.format(library_identifier))
            return

        target_name = match.group('target_name')
        if len(target_name) == 0:
            self.post_debugger_info(
                'Error: unloading library with missing target name.')
            return

        host_name = match.group('host_name')
        if len(host_name) == 0:
            self.post_debugger_info('Error: unloading new library with missing host name.')
            return

        library = self._libraries[library_identifier]
        if library.target_name != target_name:
            self.post_debugger_info(
                'Error: unloading library with mismatched target name.')
            return
        if library.host_name != host_name:
            self.post_debugger_info(
                'Error: unloading library with mismatched host name.')
            return
        self.post_debugger_info(
            'Unloading library {}.'.format(library))
        del self._libraries[library_identifier]

    def _breakpoint_info(self, info):
        """
        the field breakpoint is somewhat variable.
        The captured fields are returned as dict.

        example syntax:

        (id|number)="number",type="type",disp="del"|"keep",
         enabled="y"|"n",addr="hex",func="funcname",file="filename",
         fullname="full_filename",line="lineno",[thread="threadno,]
         times="times


         we use the array of extracting regex, _break_point_fields.

        """
        r = {}
        for x in self._break_point_fields:
            t = self._break_point_fields[x].search(info)
            if t is not None:
                r[x] = t.group('value')
        return r

    def on_breakpoint_created(self, match):
        """Some comment here:

        Even when the gdb documentation says that we must receive this
        notification, the log of gdb output don't show this message.
        Instead, we received a ^done,bkpt=...
        So, we encode this message only for some version of gcc that
        really sends that.

        In other hand, the attributes of breakpoints are somewhat variable
        and parsing with rigid structure is a bad approach. We must do
        a change here and attempt to capture present attributes with a
        couple of small regex here. As this procedure would be
        necessary in other commands, the better way is to create
        a method returning a dict of found attributes.
        """
        # breakpoint_created = r'breakpoint-created,bkpt=\{(?P<breakpoint_info>[^\}]+)\}.*'
        attributes = self._breakpoint_info(match.group('breakpoint_info'))

        number = attributes.get('number', None)
        if number is None:
            self.post_debugger_info('Error: missing number in breakpoint attributes.')
            return

        if number in self._breakpoints:
            self.post_debugger_info('Error: breakpoint {} already exists.'.format(number))
            return

        breakpoint_ = DebugBreakPointInfo(**attributes)
        self._breakpoints[number] = breakpoint_
        self.post_debugger_info('Added {}'.format(breakpoint_))
        self.post_update_breakpoints()

    def on_breakpoint_modified(self, match):
        # breakpoint_modified = r'breakpoint-modified,bkpt=\{(?P<breakpoint_info>[^\}]+)\}.*'
        attributes = self._breakpoint_info(match.group('breakpoint_info'))

        number = attributes.get('number', None)
        if number is None:
            self.post_debugger_info('Error: modifying breakpoint with missing number.')
            return

        if number not in self._breakpoints:
            self.post_debugger_info('Error: modifying unknown breakpoint {}.'.format(number))
            return

        breakpoint_ = self._breakpoints[number]
        breakpoint_.attributes = attributes
        self.post_debugger_info('Modified {}'.format(breakpoint_))
        self.post_update_breakpoints()

    def on_breakpoint_deleted(self, match):
        # breakpoint_deleted = r'breakpoint-deleted,bkpt=\{(?P<breakpoint_info>[^\}]+)\}.*'
        attributes = self._breakpoint_info(match.group('breakpoint_info'))

        number = attributes.get('number', None)
        if number is None:
            self.post_debugger_info('Error: deleting breakpoint with missing number.')
            return

        if number not in self._breakpoints:
            self.post_debugger_info('Error: deleting unknown breakpoint {}.'.format(number))
            return

        breakpoint_ = self._breakpoints[number]
        self.post_debugger_info('Deleted {}'.format(breakpoint_))
        del self._breakpoints[number]
        self.post_update_breakpoints()

    def translate(self, line):
        if self._filter_gdb_output.match(line):
            return True
        # The gdb responses start with a character that identifies
        # the target stream:
        # "^" -> gdb result record
        # "~" -> Text to be displayed in CLI console: responses to CLI commands.
        # "@" -> Text output from running target
        # "&" -> Debugging messages produced by gdb internals
        # "*" -> exec async records are used to inform the gdb/mi client of changes in debugger state.
        # "=" -> notify async records are used to inform the gdb/mi client of changes in application state.
        # notes:
        # While we are not running in a remote target, the '@' marker would be missing. This case,
        # the real target output may be preceding any command.

        for i in range(0, len(line)):
            if line[i] in self._translate_map:
                if i > 0:
                    self._translate_output(line[:i])
                if self._translate_map[line[i]](line[i + 1:]):
                    return True
        return False

    @staticmethod
    def decode_msg(msg):
        """return a list of clean and translated string"""
        if msg is None:
            return '(null)'
        if len(msg) > 0:
            if msg[-1] == '"':
                if msg[0] == '"':
                    if len(msg) > 2:
                        msg = msg[1:-1]
                    else:
                        msg = ''
                elif msg[1] == '"':
                    if len(msg) > 3:
                        msg = msg[:1] + msg[2:-1]
        status = 0
        text = ''
        for i in range(len(msg)):
            ch = msg[i]
            if status == 0:
                if ch == '\\':
                    status = 1
                    continue
                text += ch
            elif status == 1:
                if ch == '\\':
                    status = 2
                    continue
                if ch == 'n':
                    text += '\n'
                    status = 0
                elif ch == 'r':
                    status = 0
                elif ch == 't':
                    text += '\t'
                    status = 0
                else:
                    text += '\\' + ch
                    status = 0
            elif status == 2:
                if ch == '\\':
                    text += '\\'
                    status = 1
                    continue
                if ch == 'n':
                    text += '\n'
                    status = 0
                elif ch == 'r':
                    status = 0
                elif ch == 't':
                    text += '\t'
                    status = 0
                else:
                    text += '\\' + ch
                    status = 0
        return text

    def pause_gdb(self):
        if self._process and not self.is_paused:
            os.killpg(os.getpgid(self._process.pid), signal.SIGSTOP)
            self._paused = True

    def resume_gdb(self):
        if self._process and self.is_paused:
            os.killpg(os.getpgid(self._process.pid), signal.SIGCONT)
            self._paused = False

    def receiver_process(self):
        """ method that do thread process"""
        from pexpect.exceptions import TIMEOUT
        self.post_output('\nwatching process ...\n')
        self.done = False

        from queue import Queue, Empty
        line_queue = Queue(1000)

        def enqueue_output():
            try:
                for line_ in iter(self._process.readline, ""):
                    if self.done:
                        break
                    if line_queue.qsize() > 60:
                        self.pause_gdb()
                    if type(line_) is bytes:
                        line_ = line_.decode('utf8', 'ignore')
                    print(line_)
                    line_queue.put(line_)
            except TIMEOUT:
                print("Timeout waiting for remote debugger.")

        threading.Thread(target=enqueue_output).start()

        while (not self.done and line_queue.qsize() > 0) or self._process.isalive():
            try:
                line = line_queue.get_nowait()  # or q.get(timeout=.1)
                line_queue.task_done()
                if line_queue.qsize() < 40:
                    self.resume_gdb()
                lines = line.split('\n')
                for this_line in lines:
                    if len(this_line) != 0:
                        if not self.translate(this_line):
                            self.post_output(this_line)
                        continue
                if not self._process.isalive():
                    break
            except Empty:
                pass
            wx.MilliSleep(20)
        self.post_output('\nclosing ...')
        # don't close own pipe!!!
        self._process.stdout.flush()
        if self._process.isalive():
            self._process.wait()
        self.done = True
        self._semaphore.release()
        self._process = None
        self.post_output('execution ended!\n')
        self.post_debug_end()

    def command_process(self):
        """send commands to the debugger"""
        self.done = False
        debug_log_file = os.path.expanduser('~/.debug_log.log')
        if os.path.exists(debug_log_file):
            os.remove(debug_log_file)
        with open(debug_log_file, 'w') as dlog:
            dlog.write('starting session\n')
            while not self.done:
                self._semaphore.acquire()
                if not len(self._message):
                    continue
                last_command = self._message[0]
                del self._message[0]
                print('sent command > {}'.format(last_command))
                try:
                    self._process.send(last_command)
                except IOError:
                    print('forced gdb stop')
                    self.force_stop()

    def force_stop(self):
        """Brute stop"""
        self.done = True
        self._semaphore.release()
        if self._process is not None:
            self._process.terminate(True)

    def send_command(self, msg):
        """Post a command for the debugger in the queue"""
        self._message.append(msg + '\n')
        self._semaphore.release()

    def send_user_command(self, msg):
        self.send_command(msg)

    def do_quit(self):
        if self._attach:
            self.do_detach()
            self._attach = False
        self.send_command('-gdb-exit')

    def do_restart(self):
        self.send_command('-exec-run')

    def do_continue(self):
        self.send_command('-exec-continue')

    def do_query_args(self):
        self.clean_args()
        self.send_command('-stack-list-arguments 1 0 0')

    def do_query_locals(self):
        self.clean_locals()
        self.send_command('-stack-list-locals 1')

    def do_query_stack(self):
        self.clean_stack()
        self.send_command(' -stack-list-frames 0 10')

    def do_query_watch(self, w):
        self.send_command('-var-evaluate-expression {}'.format(w.name))

    def do_query_watches(self):
        for w in self._watches:
            self.do_query_watch(w)

    def do_add_watch(self, w):
        self.send_command('-var-create {w.name} * {w.expression}'.format(w=w))

    def do_add_breakpoint(self, file_, line_):
        if os.path.isabs(file_):
            file_ = os.path.relpath(file_, self._local_source_dir)
        self.send_command('-break-insert {file_}:{line_}'.format(file_=file_, line_=line_))

    def do_disable_breakpoint(self, number):
        """disable-breakpoint must be implemented by the debugger-specific interface"""
        self._breakpoints[number].attributes['enabled'] = 'n'
        breakpoint_ = self._breakpoints[number]
        self.send_command('-break-disable {}'.format(breakpoint_.number))
        self.post_debugger_info('Disabled {}'.format(breakpoint_))
        self.post_update_breakpoints()

    def do_enable_breakpoint(self, number):
        """enable-breakpoint must be implemented by the debugger-specific interface"""
        self._breakpoints[number].attributes['enabled'] = 'y'
        breakpoint_ = self._breakpoints[number]
        self.send_command('-break-enable {}'.format(breakpoint_.number))
        self.post_debugger_info('Enabled {}'.format(breakpoint_))
        self.post_update_breakpoints()

    def do_delete_breakpoint(self, number):
        """delete-breakpoint must be implemented by the debugger-specific interface"""
        breakpoint_ = self._breakpoints[number]
        self.send_command('-break-delete {}'.format(breakpoint_.number))
        self.post_debugger_info('Deleted {}'.format(breakpoint_))
        del self._breakpoints[number]
        self.post_update_breakpoints()

    def disable_breakpoint(self, number, file_, line_):
        """Disable a breakpoint"""
        breakpoint_ = None
        if number in self._breakpoints:
            breakpoint_ = self._breakpoints[number]
        else:
            line_ = str(line_)
            for candidate in self._breakpoints:
                breakpoint_data = self._breakpoints[candidate]
                if breakpoint_data.line != line_:
                    continue
                if breakpoint_data.file != file_:
                    continue
                breakpoint_ = breakpoint_data
                number = breakpoint_.number
                break
            if breakpoint_ is None:
                return False
            self.do_disable_breakpoint(number)
            return True

    def delete_breakpoint(self, number, file_, line_):
        """delete a breakpoint by id or file,line"""
        # First attempt to do the best using id
        breakpoint_ = None
        if number in self._breakpoints:
            breakpoint_ = self._breakpoints[number]
        else:
            line_ = str(line_)
            for candidate in self._breakpoints:
                breakpoint_data = self._breakpoints[candidate]
                if breakpoint_data.line != line_:
                    continue
                if breakpoint_data.file != file_:
                    continue
                breakpoint_ = breakpoint_data
                number = breakpoint_.number
                break
        if breakpoint_ is None:
            return False
        self.do_delete_breakpoint(number)
        return True

    def do_delete_all_breakpoints(self):
        self.send_command('-break-delete')

    def do_next(self):
        """steps one line, not entering in calls"""
        self.send_command('-exec-next')

    def do_step(self):
        """steps one line, entering in calls"""
        self.send_command('-exec-step')

    def do_step_out(self):
        self.send_command('-exec-finish')

    def do_load_file(self, file_):
        self.send_command('file {file_}'.format(file_=file_))

    def do_set_program_arguments(self, arguments):
        self.send_command('-exec-arguments {}'.format(arguments))

    def do_attach(self, pid):
        self.send_command(' -target-attach {pid}'.format(pid=pid))

    def do_detach(self):
        self.send_command(' -target-detach')

    def debug_remote(self, executable):
        """Start a debug operation"""
        # Take a look to :
        # https://github.com/cs01/pygdbmi/tree/master/pygdbmi
        if self._process is not None:
            return False  # is already debugging
        from pexpect import pxssh
        try:
            self._process = pxssh.pxssh(use_poll=True, timeout=None)
            self._process.login(self._host, port=self._port, username=self._user, password=self._password)
            self._process.sendline('LC_ALL=en_US DISPLAY=:0 gdb --nx --quiet --interpreter=mi2  && exit')
        except pxssh.ExceptionPxssh:
            self._process = None
            self.post_output('execution failed.\n')
            return False
        # enqueue first commands
        self.send_command('-gdb-set mi-async off')
        # self.send_command('set host-charset US-ASCII')
        if self._source_dir:
            self.send_command('dir {}'.format(self._source_dir))
        self.do_load_file(executable)
        if self._program_arguments:
            self.do_set_program_arguments(self._program_arguments)

        self.activate_ui()
        self._paused = False
        threading.Thread(target=self.receiver_process, ).start()
        threading.Thread(target=self.command_process, ).start()
        return True

    def debug_attach_remote(self, executable):
        """Start a debug operation"""
        # The process is very similar but we use pidof to get the pid's of the program
        from pexpect import pxssh
        try:
            self._process = pxssh.pxssh(use_poll=True, timeout=None)
            self._process.login(self._host, port=self._port, username=self._user, password=self._password)
            self._process.sendline('pidof {}'.format(executable))
            self._process.prompt()
            items = self._process.before.decode('utf8', 'ignore').split('\r\n')
            pid = items[-2]
            print(' Debugging binary with pid {}'.format(pid))
            # TO DO : add environment
            # self._process.sendline('{} gdb --nx --quiet --interpreter=mi2 && exit'.format(self.environment))
            self._process.sendline('gdb --nx --quiet --interpreter=mi2 && exit')
        except pxssh.ExceptionPxssh:
            self._process = None
            self.post_output('execution failed.\n')
            return False
        self.send_command('-gdb-set target-async 1')
        self.send_command('-gdb-set pagination off')
        self.send_command('-gdb-set non-stop on')
        # self.send_command('-gdb-set mi-async off')
        # self.send_command('set host-charset US-ASCII')
        if self._source_dir:
            self.send_command('dir {}'.format(self._source_dir))
        self.do_attach(pid)
        self.activate_ui()
        self._paused = False
        threading.Thread(target=self.receiver_process, ).start()
        threading.Thread(target=self.command_process, ).start()
        return True
