

class DebugThreadInfo(object):
    """small class for thread information"""

    def __init__(self, **kwargs):
        """init"""
        self._is_current = kwargs.get('current', False)
        self._class = kwargs.get('cls', 'unknown')
        self._name = kwargs.get('name', '<unnamed>')
        self._status = kwargs.get('status', 'unknown')
        self._id = kwargs['id']
        self._stack = []
        super(DebugThreadInfo, self).__init__()

    def __str__(self):
        return '{self._name}:{self._class}={self._id} {self._status}'.format(self=self)

    @property
    def stack(self):
        """return thread stack"""
        return self._stack

    def append_stack(self, call, source_file, line):
        """Append a call to the stack"""
        self._stack.append((call, source_file, line))

    @property
    def is_current(self):
        """return info about if the thread is the current thread"""
        return self._is_current

    @property
    def class_name(self):
        """return the class name"""
        return self._class

    @property
    def name(self):
        """return the name"""
        return self._name

    @property
    def status(self):
        """return the thread status"""
        return self._status

    @property
    def id(self):
        """return the thread identifier"""
        return self._id

