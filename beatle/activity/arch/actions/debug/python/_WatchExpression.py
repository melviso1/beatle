

class WatchExpression(object):
    """Represents a watch expression"""

    def __init__(self, name, expression, value=''):
        super(WatchExpression, self).__init__()
        self._name = name
        self._expression = expression
        self._value = value

    @property
    def name(self):
        return self._name

    @property
    def expression(self):
        return self._expression

    @property
    def value(self):
        return self._value

