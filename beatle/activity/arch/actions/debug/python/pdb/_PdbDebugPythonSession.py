from .._DebugPythonSession import DebugPythonSession

class PdbDebugPythonSession(DebugPythonSession):
    """Handles interaction with (trepan3k) debugger and debug session handling"""

    def __init__(self, handler, script, source_dir=None):
        """Init"""
        super(PdbDebugPythonSession, self).__init__(handler, script, source_dir)
        self.debug_script(script)

    def capture_locals_info(self, text):
        """capture lines of text"""

    def process_locals_info(self):
        pass

    def capture_line_info(self, text):
        pass

    def capture_break_info(self, text):
        """store break info"""
        pass

    def process_break_info(self):
        """process break info"""
        pass

    def capture_thread_info(self, text):
        """store thread info"""
        pass

    def process_line_info(self):
        """Process line info"""
        pass

    def process_thread_info(self):
        """Process the full thread info and dispatch"""
        pass

    def receiver_process(self):
        """funcion que realiza el trabajo en el thread"""
        pass

    def command_process(self):
        """reads commands from the debugger"""
        pass

    def force_stop(self):
        """Brute stop"""
        pass

    def send_command(self, msg):
        """Post a command for the debugger in the queue"""
        pass

    def send_user_command(self, msg):
        pass

    def do_quit(self):
        pass

    def do_restart(self):
        """Does nothing really here, only ask for status"""
        pass

    def do_continue(self):
        pass

    def do_query_locals(self):
        pass

    def do_add_breakpoint(self, file_, line_):
        pass

    def do_delete_all_breakpoints(self):
        pass

    def do_disable_breakpoint(self, number):
        """disable-breakpoint must be implemented by the debugger-specific interface"""
        pass

    def do_enable_breakpoint(self, number):
        """enable-breakpoint must be implemented by the debugger-specific interface"""
        pass

    def do_delete_breakpoint(self, number):
        """delete-breakpoint must be implemented by the debugger-specific interface"""
        pass

    def do_next(self):
        """steps one line, not entering in calls"""
        pass

    def do_step(self):
        pass

    def do_step_out(self):
        pass

    def debug_script(self, script):
        """Start a debug operation"""
        pass

