

class DebugLocalInfo(object):
    """small class for local information"""

    def __init__(self, **kwargs):
        super(DebugLocalInfo, self).__init__()
        self._name = kwargs['name']
        if 'value' in kwargs:
            self._has_value = True
            self._value = kwargs['value']
        else:
            self._has_value = False
            self._value = None

    @property
    def name(self):
        return self._name

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        # TO-DO : dispatch command
        self._value = value

