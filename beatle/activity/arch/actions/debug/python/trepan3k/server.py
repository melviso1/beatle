#!/usr/bin/python3

# -*- coding: utf-8 -*-
import re
import sys

from trepan import options as Moptions
from trepan.__main__ import main
import tempfile

from trepan.inout import tcpfns


#monkey-patching Moptions.process_options -> because trepan3k fails to start a tcp server
#monkey-patching tmpfile.NamedTemporaryFile -> because trepan3k expect to be opened in text mode
#monkey-patching tcpfns.TCP_MAX_PACKET -> avoid pending trepan3k buffer bug
 
original_process_options = Moptions.process_options
original_NamedTemporaryFile = tempfile.NamedTemporaryFile


def tcp_process_options(debugger_name, pkg_version, sys_argv, option_list=None):
    opts, dbg_opts, sys_argv = original_process_options(debugger_name, pkg_version, sys_argv, option_list)
    opts.server = 'tcp'
    return opts, dbg_opts, sys_argv 


def text_NamedTemporaryFile(mode='w+t', buffering=-1, encoding=None,
                       newline=None, suffix=None, prefix=None,
                       dir=None, delete=True):
    return original_NamedTemporaryFile(mode=mode,buffering=buffering,
                        encoding=encoding, suffix=suffix, prefix=prefix,
                        dir=dir, delete=delete)

# mokey patching
Moptions.process_options = tcp_process_options
tempfile.NamedTemporaryFile = text_NamedTemporaryFile
tcpfns.TCP_MAX_PACKET = 65535


sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
sys.exit(main())
