"""
StateMachine is a single machine state process for
parsing the output of pp locals() command.

It's also a nice experiment for showing how easy is to build
a small state machine in Python.
"""


class StateMachine(object):
    """
    Simple state machine for analysing "pp locals()" output.
    """
    def __init__(self, text):
        self._blanks = [' ','\n','\t','\r']
        self._state = 0
        self._deep = 0
        self._name = ''
        self._tt = [
            self._state0, self._state1, self._state2,
            self._state3, self._state4, self._state5,
            self._state6, self._state7, self._state8,
            self._state9]
        self._run(text)

    def error(self):
        raise SyntaxError()

    def _run(self, text):
        self._locals = {}
        self._state = 0
        self._stack = {0:{'vars':{'all':{}}}}
        self._vars = {}
        self._deep = 0
        self._mark = None
        self._name = 'all'
        self._text = text
        for i in range(0,len(text)):
            self._index = i
            self._tt[self._state](text[i])

    @property
    def result(self):
        return self._stack[0]['vars']['all']

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        # print ('from state {} to {}'.format(self._state, value))
        self._state = value

    def push(self):
        self._stack[self._deep]['name'] = self._name
        self._deep = self._deep + 1
        self._stack[self._deep] = {'vars':{}}
        self.state = 1

    def pop(self):
        if self._deep == 0:
            self.error()
        _dict = self.vars
        self._stack[self._deep]['vars'] = None
        del self._stack[self._deep]
        self._deep = self._deep - 1
        self._name = self._stack[self._deep]['name']
        self.var = _dict

    @property
    def vars(self):
        return self._stack[self._deep]['vars']

    @property
    def var(self):
        self.vars[self._name]

    @var.setter
    def var(self, value):
        self.vars[self._name] = value

    def _state0(self, ch):
        """finding dict start"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch in self._blanks:
            return
        if ch != '{':
            self.error()
        self.push()

    def _state1(self, ch):
        """finding field name start"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch not in self._blanks:
            if ch == "'":
                self.state = 2
                self._mark = self._index + 1
            elif ch != ',':  # empty field is legal
                self.error()

    def _state2(self, ch):
        """finding end field name"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch == "'":
            self._name = self._text[self._mark:self._index]
            self.var = ''
            self.state = 3

    def _state3(self, ch):
        """finding ':' separator"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch not in self._blanks:
            if ch != ':':
                self.error()
            self._mark = self._index+1
            self.state = 4

    def _state4(self, ch):
        """unknown content type"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch not in self._blanks:
            if ch == '{':
                self.push()
                self.state = 1
            elif ch == '"':
                self.state = 5
            elif ch =="'":
                self.state = 6
            elif ch == ',':
                self.state = 1
            elif ch == '}':
                self.pop()
                self.state = 7
            else:
                self.state = 8

    def _state5(self, ch):
        """inside double quoted"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch == '"':
            self.state = 8

    def _state6(self, ch):
        """inside single quoted"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch == "'":
            self.state = 8

    def _state7(self, ch):
        """waiting end level or separator"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch not in self._blanks:
            if ch == ',':
                self.state = 1
            elif ch == '}':
                self.pop()
            else:
                self.error()

    def _state8(self, ch):
        """free content type (unexpected)"""
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch not in self._blanks:
            if ch == ',':
                self.var = self._text[self._mark: self._index]
                self.state = 9
            if ch == '}':
                self.var = self._text[self._mark: self._index]
                self.pop()
                self._state = 7
            elif ch == '"':
                self.state = 5
            elif ch == "'":
                self.state = 6

    def _state9(self, ch):
        # print('[{ch}] in state {self.state}'.format(self=self, ch=ch))
        if ch not in self._blanks:
            if ch == "'":
                self.state = 2
                self._mark = self._index + 1
            elif ch == '}':
                self.var = self._text[self._mark: self._index]
                self.pop()
                self._state = 7
            elif ch != ',':
                self.state = 8

