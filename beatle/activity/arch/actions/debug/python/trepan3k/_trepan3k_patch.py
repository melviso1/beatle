import io
from trepan import misc
from trepan.inout import tcpfns
from trepan.inout.input import DebuggerUserInput, readline_importable
from trepan.interfaces import client

tcpfns.TCP_MAX_PACKET = 65535  # monkey patching to avoid pending trepan3k buffer bug


def patch_open(self, inp, opts=None):
    """Use this to set where to read from.

    Set opts['try_lineedit'] if you want this input to interact
    with GNU-like readline library. By default, we will assume to
    try importing and using readline. If readline is not
    importable, line editing is not available whether or not
    opts['try_readline'] is set.

    Set opts['use_raw'] if input should use Python's use_raw(). If
    however 'inp' is a string and opts['use_raw'] is not set, we
    will assume no raw output. Note that an individual readline
    may override the setting.
    """
    if opts is None:
        opts = {}

    def get_option(key):
        return misc.option_set(opts, key, self.DEFAULT_OPEN_READ_OPTS)

    if isinstance(inp, (io.TextIOWrapper, io.StringIO)) or getattr(inp, 'isatty', lambda: False)():
        self.use_raw = get_option('use_raw')
    elif isinstance(inp, 'string'.__class__):  # FIXME
        if opts is None:
            self.use_raw = False
        else:
            self.use_raw = True
            pass
        inp = open(inp, 'r')
    else:
        self.use_raw = True
    self.input = inp
    self.line_edit = get_option('try_readline') and readline_importable()
    self.closed = False
    return


def patch_read_remote(self):
    """
    Send a message back to the server (in contrast to
    the local user output channel).
    """
    coded_line = self.inout.read_msg()
    if isinstance(coded_line, bytes):
        coded_line = coded_line.decode("utf-8")
    control = coded_line[0]
    remote_line = coded_line[1:]
    return control, remote_line


client.ClientInterface.read_remote=patch_read_remote
DebuggerUserInput.open = patch_open
