import atexit
import os
import re
import signal
import socket
import threading

import wx

from ._StateMachine import StateMachine
from .._DebugLocalInfo import DebugLocalInfo
from .._DebugPythonSession import DebugPythonSession
from .._DebugThreadInfo import DebugThreadInfo
from .._DebugFrameInfo import DebugFrameInfo
from .._DebugBreakPointInfo import DebugBreakPointInfo

# from ._trepan3k_patch import *


class Trepan3kDebugPythonSession(DebugPythonSession):
    """Handles interaction with (trepan3k) debugger and debug session handling"""

    def __init__(self, handler, script, source_dir=None):
        """Init"""
        self.intf = None
        self.ansi_escape = re.compile(r'\x1b[^m]*m')
        self.bin_obj = re.compile(r'<(?P<decl>.*?)>(?!\')')
        self.find_unquoted = [
            re.compile(r"'copyright': .*?\.,", re.DOTALL),
            re.compile(r"'credits': .*?\.,", re.DOTALL),
            re.compile(r"'license': .*?,", re.DOTALL),
            re.compile(r"'help': .*?\.,", re.DOTALL),
            re.compile(r"'exit': .*?,", re.DOTALL),
            re.compile(r"'quit': .*?,", re.DOTALL)
            ]        # hooks
        # new breakpoint format includes file offset information
        # 1   breakpoint    keep y    76 at /home/mel/workspace/primitiva/main.py:32
        self.find_break = re.compile(
            r'(?P<bpnum>[0-9]+)\s+' +
            r'breakpoint\s+' +
            r'(?P<disp>.+)\s+' +
            r'(?P<enabled>[yYnN]+)\s+' +
            r'(?P<offset>.*)\s*' +
            r'at\s+(?P<file>.+):(?P<line>[0-9]+)')

        self.find_frame = re.compile(
            r'(?P<indicator>(->|##))\s*' +  # -> if current frame ## otherwise
            r'(?P<level>[0-9]+)\s' +  # frame number
            r'(?P<func>.+)\s' +
            r'called\sfrom\sfile\s\'(?P<file>.+)\'\sat\sline\s(?P<line>[0-9]+)')

        # retained info
        self.info = {}
        self._process = None
        self._receiver = None
        self._message = []
        self._buffer = ''   # stack buffer
        self._bufferL = ''  # line buffer
        self._bufferT = ''  # threads/locals buffer info
        self._bufferB = ''  # breakpoints info buffer
        self._bufferS = ''  # backtrace info buffer
        self._semaphore = threading.Semaphore(0)
        super(Trepan3kDebugPythonSession, self).__init__(handler, script, source_dir)

    def capture_locals_info(self, text):
        """capture lines of text"""
        self._buffer += str(text)

    def process_locals_info(self):
        """Process the full thread info and dispatch
        format:
            <dict>

        where <dict> is

            {'<name>': <mixin_dict_value>

        where <mixin_dict_value> is one of

            <class '<name>'>(,'name': <mixin_dict_value>|})
            <unquoted text>(,'name': <mixin_dict_value>|})
            <quoted text>+(,'name': <mixin_dict_value>|})
            <dict>

        The better way is to process with an automaton
        """
        try:
            vars_ = StateMachine(self._buffer).result
            self._locals = dict(
                [(name, DebugLocalInfo(name=name, value=vars_[name])) for name in vars_]
            )
        except SyntaxError as e:
            # print(' failed to parse locals: {}'.format(str(e)))
            pass
        self._buffer = ''

    def capture_line_info(self, text):
        """store buffer lines"""
        self._bufferL += text

    def capture_break_info(self, text):
        """store break info"""
        self._bufferB += text

    def capture_backtrace(self, text):
        """store backtrace"""
        self._bufferS += text

    def process_break_info(self):
        """process break info"""
        text = self._bufferB
        if not text:
            return
        self._bufferB = ''
        lines = text.splitlines()
        self._breakpoints = {}
        for line in lines:
            z = self.find_break.match(line)
            if z:
                num = z.group('bpnum')
                self._breakpoints[num] = DebugBreakPointInfo(
                    num, file=z.group('file'), line=z.group('line'), enabled=z.group('enabled'))

    def process_backtrace(self):
        """process backtrace info"""
        text = self._bufferS
        if not text:
            return
        self._bufferS = ''
        lines = text.splitlines()
        self._stack_frames = []
        for line in lines:
            z = self.find_frame.match(line)
            if z:
                self._stack_frames.append(
                    DebugFrameInfo(
                        level = z.group('level'),
                        func=z.group('func'),
                        file=z.group('file'),
                        line=z.group('line')))

    def capture_thread_info(self, text):
        """store thread info"""
        self._bufferT += str(text)

    def process_line_info(self):
        """Process line info"""
        text = self._bufferL
        if not text:
            return
        self._bufferL = ''
        text = ''.join(text.splitlines())
        # handler = self._frame.GetEventHandler()
        # print 'processing line text {0}'.format(text)
        z = re.search(r'Line (?P<lineno>[0-9]*) of \"(?P<file>.*)\"', text)
        if z:
            self._line = line = int(z.group('lineno'))
            self._file = file = z.group('file')
            # post a message
            self.post_file_line(file, line)

    def process_thread_info(self):
        """Process the full thread info and dispatch"""
        text = self._bufferT
        if not text:
            return
        self._bufferT = ''
        lines = text.splitlines()[1:]  # discard separator
        thread_info_line = lines.pop(0)
        thread_info = re.search(
            r'(?P<ttype>..) <(?P<tclass>.+)\((?P<tname>\w+), (?P<tstatus>\w+) .*\)>: (?P<tid>[0-9]+)',
            thread_info_line)
        thread_current = False
        thread_class = '???'
        thread_name = '???'
        thread_status = '???'
        if thread_info:
            thread_current = (thread_info.group('ttype') == '=>')
            thread_class = thread_info.group('tclass')
            thread_name = thread_info.group('tname')
            thread_status = thread_info.group('tstatus')
        else:
            thread_info = re.search(r'\s*thread id: (?P<tid>[0-9]+)', thread_info_line)
            if not thread_info:
                # post a info message
                msg = 'Thread info:{info}'.format(info=thread_info_line)
                self.post_unknown_debug_info(msg)
                return
        # The thread info is append to the internal list
        thread_id = thread_info.group('tid')
        thread = DebugThreadInfo(
            current=thread_current,
            cls=thread_class,
            name=thread_name,
            status=thread_status,
            id=thread_id)
        self._threads[thread_id] = thread
        # Now parse the stack frame
        header = True
        while len(lines):
            try:
                item = lines.pop(0).strip()
                if item[:8] == '<module>':
                    thread.append_stack(item, None, None)
                    continue
                _line = lines.pop(0).strip()
                # skip separator
                # dissmiss debugger lines
                if header:
                    if 'trepan3k.processor.command.info' in item:
                        continue
                    if 'trepan3k.processor.cmdproc' in item:
                        continue
                    if 'trepan3k.lib.core.DebuggerCore' in item:
                        continue
                    if item[:13] == '_tracer_func(':
                        continue
                header = False
                z = re.search(r"called from file '(?P<source>.+)' at line (?P<line>[0-9]+)", _line)
                if z:
                    s = '{f}:{l} in {c}'.format(
                        f=z.group('source'),
                        l=z.group('line'),
                        c=item)
                    thread.append_stack(s, z.group('source'), z.group('line'))
                else:
                    thread.append_stack(_line, None, None)
            except:
                break
        #print thread.stack

    def receiver_process(self):
        """funcion que realiza el trabajo en el thread"""
        self.post_output('watching process ...\n')
        line = ''
        ended = False
        # import fcntl
        # fd = self._process.stdout.fileno()
        # fl = fcntl.fcntl(fd, fcntl.F_GETFL)
        # fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
        # while self._process.poll() is None:
        #     try:
        #         line = self._process.stdout.readline()
        #     except :
        #         continue
        try:
            for line in iter(self._process.stdout.readline, ""):
                if type(line) is bytes:
                    line = line.decode('ascii', 'ignone')
                if len(line) > 0:
                    # print(' posting {}'.format(str(line)))
                    if line.strip() != "Can't print_simple_array":  # temporally workaround trepan3k issue
                        self.post_output(line)
                    self._process.stdout.flush()
                if self._process.poll() is not None:  # that means the process ended
                    break
        except Exception as e:
            self.post_output("Debugger excepction received : {}".format(str(e)))
            self._process.stdout.flush()
        finally:
            ended = True
            self.post_output('\nclosing ...')
            # we couldn't close the stdout pipe (because is the beatle stdout)!!
            if not ended:
                self._process.wait()
            self.done = True
            self._process = None
            self.post_output('execution ended!\n')
            self.post_debug_end()

    def command_process(self):
        """reads commands from the debugger"""

        from trepan.interfaces import client, comcodes

        connection_opts = {'open': True,
                           'IO': 'TCP', 'HOST': '127.0.0.1', 'PORT': 13001}
        # remove problematic timeout:
        # import socket
        # default_timeout = socket.getdefaulttimeout()
        # socket.setdefaulttimeout(None)

        try:
            wx.Sleep(1)  # wait a bit for server startup
            self.intf = client.ClientInterface(connection_opts=connection_opts)
            # the client.ClientInterface installs an undesired atexit that both
            # prevents for deleting the debugger instance, and closes the
            # app standard streams!!!
            atexit.unregister(self.intf.finalize)
        except Exception as e:
            tries = 10
            while(tries > 0):
                try:
                    wx.Sleep(1)
                    self.intf = client.ClientInterface(connection_opts=connection_opts)
                    atexit.unregister(self.intf.finalize)
                    break
                except Exception as e:
                    tries = tries - 1
            if tries == 0:
                self.post_output('\nError: debugger connection failed.\n')
                if self._process is not None:
                    os.killpg(os.getpgid(self._process.pid), signal.SIGTERM)
                # socket.setdefaulttimeout(default_timeout)
                return
        # socket.setdefaulttimeout(default_timeout)

        self.post_output('Debugger connection open.\n')
        self.done = False
        last_command = None
        # Fix 30 dic 2020 : debug log is expensive and really only useful for
        # beatle development
        # # remove old log file
        # debug_log_file = os.path.expanduser('~/.debug_log.log')
        # if os.path.exists(debug_log_file):
        #     os.remove(debug_log_file)
        # with open(debug_log_file, 'w') as dlog:
        #     dlog.write('starting session\n')
        while not self.done:
            try:
                control, remote_msg = self.intf.read_remote()
            except socket.timeout:
                # The process may be running
                continue
            except Exception as e:
                # an exception has happen in the debugger
                self.post_output('Error: exception while read remote : {t} {string}.\n'.format(
                    t=type(e), string=str(e)))
                self.done = True
                continue
            remote_msg = self.ansi_escape.sub('', remote_msg)  # remove ansi console color escape codes
            try:
                if control == comcodes.COMMAND:
                    cmd = 'COMMAND'
                elif control == comcodes.CONFIRM_FALSE:
                    cmd = 'CONFIRM_FALSE'
                elif control == comcodes.CONFIRM_REPLY:
                    cmd = 'CONFIRM_REPLY'
                elif control == comcodes.CONFIRM_TRUE:
                    cmd = 'CONFIRM_TRUE'
                elif control == comcodes.PRINT:
                    cmd = 'PRINT'
                elif control == comcodes.PROMPT:
                    cmd = 'PROMPT'
                elif control == comcodes.QUIT:
                    cmd = 'QUIT'
                elif control == comcodes.RESTART:
                    cmd = 'RESTART'
                elif control == comcodes.SYNC:
                    cmd = 'SYNC'
                else:
                    cmd = 'UNKNOWN'
                # dlog.write('received remote msg: {d}[{m}] previous command: [{c}]\n'.format(
                #     d=cmd, m=remote_msg, c=last_command or 'None'))
            except:
                # dlog.write('failed to write remote\n')
                pass
            # print 'c, r', control, remote_msg
            if control == comcodes.PRINT:
                if last_command:
                    if last_command[0] == '#':
                        self.post_user_command_response(remote_msg)
                    elif last_command == 'info line':
                        # dlog.write('capturing line\n')
                        self.capture_line_info(remote_msg)
                    elif last_command == 'info threads verbose':
                        self.capture_thread_info(remote_msg)
                    elif last_command == 'pp locals()':
                        self.capture_locals_info(remote_msg)
                    elif last_command == 'info break':
                        self.capture_break_info(remote_msg)
                    elif last_command == 'backtrace':
                        self.capture_backtrace(remote_msg)
                    elif re.search(r'The program finished', remote_msg):
                        self.intf.write_remote(comcodes.CONFIRM_REPLY, 'quit')
                continue
            if control in [comcodes.CONFIRM_TRUE, comcodes.CONFIRM_FALSE]:
                self.intf.write_remote(comcodes.CONFIRM_REPLY, 'Y')
                continue
            if control == comcodes.PROMPT:
                # here, we read the message
                try:
                    if last_command == 'info line':
                        self.process_line_info()
                    elif last_command == 'info threads verbose':
                        self.process_thread_info()
                        self.post_update_threads()
                    elif last_command == 'pp locals()':
                        self.process_locals_info()
                        self.post_update_locals()
                    elif last_command == 'info break':
                        self.process_break_info()
                        self.post_update_breakpoints()
                    elif last_command == 'backtrace':
                        self.process_backtrace()
                        self.post_update_stack_frame()
                except:
                    pass
                self._stepping = True
                self._semaphore.acquire()
                if not len(self._message):
                    continue
                last_command = self._message[0]
                del self._message[0]
                self._stepping = False
                # debug commands
                #if last_command != 'info line':
                #    handler.AddPendingEvent(OutputEvent(last_command))
                if last_command[0] == '#':
                    #this is an user command, using debug command pane
                    self.intf.write_remote(comcodes.CONFIRM_REPLY, last_command[1:])
                else:
                    self.intf.write_remote(comcodes.CONFIRM_REPLY, last_command)
                continue
            if control == comcodes.QUIT:
                self.done = True
                continue
            if control == comcodes.RESTART:
                self.intf.inout.close()
                wx.Sleep(1)
                self.intf.inout.open()
            else:
                print("!! Weird status code received '%s'" % control)
                print(remote_msg,)
                pass
        self.intf.inout.close()
        del self.intf
        self.post_output('\nDebugger connection closed\n')
        self._semaphore.release()

    def force_stop(self):
        """Brute stop"""
        self.done = True
        self._semaphore.release()
        if self._process is not None:
            os.killpg(os.getpgid(self._process.pid), signal.SIGTERM)

    def send_command(self, msg):
        """Post a command for the debugger in the queue"""
        self._message.append(msg)
        self._semaphore.release()

    def send_user_command(self, msg):
        self.send_command('#'+msg)

    def do_quit(self):
        self.send_command('quit')

    def do_restart(self):
        """Does nothing really here, only ask for status"""
        self.do_query_line()
        self.do_query_threads()
        self.do_query_locals()
        self.do_query_breakpoints()
        self.do_query_stack()

    def do_continue(self):
        self.send_command('continue')
        # post standard commands for finish or bp
        self.do_query_line()
        self.do_query_threads()
        self.do_query_locals()
        self.do_query_stack()

    def do_query_line(self):
        self._bufferL = ''
        self.send_command('info line')

    def do_query_threads(self):
        self._bufferT = ''
        self.send_command('info threads verbose')

    def do_query_locals(self):
        self._buffer = ''
        self.send_command('pp locals()')

    def do_query_breakpoints(self):
        self._bufferB = ''
        self.send_command('info break')

    def do_query_stack(self):
        self._bufferS = ''
        self.clean_stack()
        self.send_command('backtrace')

    def do_add_breakpoint(self, file_, line_):
        self.send_command('#break {file_}:{line_}'.format(file_=file_, line_=line_))
        self.do_query_breakpoints()

    def do_delete_all_breakpoints(self):
        self.send_command('delete')
        self.do_query_breakpoints()

    def do_disable_breakpoint(self, number):
        """disable-breakpoint must be implemented by the debugger-specific interface"""
        self.send_command("#disable {}".format(number))
        self.do_query_breakpoints()

    def do_enable_breakpoint(self, number):
        """enable-breakpoint must be implemented by the debugger-specific interface"""
        self.send_command("#enable {}".format(number))
        self.do_query_breakpoints()

    def do_delete_breakpoint(self, number):
        """delete-breakpoint must be implemented by the debugger-specific interface"""
        self.send_command("#delete {}".format(number))
        self.do_query_breakpoints()

    def do_next(self):
        """steps one line, not entering in calls"""
        self.send_command('next')
        self.do_query_line()
        self.do_query_threads()
        self.do_query_locals()
        self.do_query_stack()

    def do_step(self):
        self.send_command('#step')
        self.do_query_line()
        self.do_query_threads()
        self.do_query_locals()
        self.do_query_stack()

    def do_step_out(self):
        self.send_command('#frame 0')
        self.send_command('finish')
        self.do_query_line()
        self.do_query_threads()
        self.do_query_locals()
        self.do_query_stack()

    def debug_script(self, script):
        """Start a debug operation"""
        if self._process is not None:
            self.post_output('Error: Debugger already running.\n')
            return False  # is already debugging
        # server_flags = [
        #     '--private', '--post-mortem', '--nx',
        #     '--port=13001', '--server']
        server_flags = [
            '--private', '--nx',
            '--port=13001', '--server']
        if self._source_dir:
            server_flags.append('--cd="{}"'.format(self._source_dir))
        server_command = 'python3 -m beatle.activity.arch.actions.debug.python.trepan3k.server {flags} {source_file}'.format(
            flags=' '.join(server_flags),
            source_file=script)
        import subprocess
        self.activate_ui()
        try:
            self._process = subprocess.Popen(server_command,
                bufsize=1,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT, shell=True,
                universal_newlines=True,
                start_new_session=True)
            if self._process is not None:
                threading.Thread(target=self.receiver_process,).start()
                threading.Thread(target=self.command_process,).start()
                return True
        except Exception as e:
            pass
        self.deactivate_ui()
        self.post_output('Error: Debugger session failed to start.\n')
        return False

