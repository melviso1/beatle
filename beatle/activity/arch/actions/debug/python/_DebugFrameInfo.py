

class DebugFrameInfo(object):
    """Represents a debug frame"""

    def __init__(self, **kwargs):
        self._level = kwargs['level']
        self._func = kwargs['func']
        self._file = kwargs['file']
        self._line = kwargs['line']

    @property
    def better_file(self):
        """for the moment, we don't have such a difference for python"""
        return self._file

    @property
    def file(self):
        return self._file

    @property
    def line(self):
        return self._line

    def __str__(self):
        return '{self._level}: {self._func} at {self._file}:{self._line}'.format(self=self)

