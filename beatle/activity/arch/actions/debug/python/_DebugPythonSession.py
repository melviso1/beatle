import signal
import re
import threading
import atexit
import os.path

import wx
from wx import aui

from beatle.lib import wxx
from beatle.lib.api import context
from beatle.app.ui.tools import clone_mnu
from beatle.activity.arch.ui import pane
from beatle.lib.handlers import identifier

from . import DebugPaneInfo
from ._WatchExpression import WatchExpression



class DebugPythonSession(object):
    """Handles interaction with debugger and debug session handling"""

    _show_threads = identifier('ID_SHOW_DEBUG_THREADS')
    _show_locals = identifier('ID_SHOW_DEBUG_LOCALS')
    _show_expressions = identifier('ID_SHOW_DEBUG_EXPRESSIONS')
    _show_breakpoints = identifier('ID_SHOW_DEBUG_BREAKPOINTS')

    def __init__(self, handler, script, source_dir=None):
        """Init"""
        self._handler = handler  # The view
        self._frame = context.get_frame()
        self._menu = None
        self._old_perspective = None
        self._perspective = None
        self.menu_pos = wx.NOT_FOUND
        self.m_stack = None
        self.m_command = None
        self.m_debug_info = None
        self._line = None
        self._file = None
        self.done = True

        self._debug_panes = [
            DebugPaneInfo(pane.Locals, self._show_locals, u"Locals", u"show frame locals."),
            DebugPaneInfo(pane.Expressions, self._show_expressions, u"Expressions", u"show watch expressions."),
            DebugPaneInfo(pane.Breakpoints, self._show_breakpoints, u"Breakpoints", u"show breakpoints."),
            DebugPaneInfo(pane.Threads, self._show_threads, u"Threads", u"show threads of current target."),
        ]

        self._source_dir = source_dir
        # retained info
        self.info = {}
        self._receiver = None
        self._message = []
        self._stepping = False
        self._threads = {}
        self._locals = {}
        self._watches = []
        self._watch_count = 0
        self._breakpoints = {}
        self._stack_frames = []

        super(DebugPythonSession, self).__init__()
        self.debug_script(script)

    @property
    def frame(self):
        """Access to common frame"""
        return self._frame

    @property
    def is_stepping(self):
        return self._stepping

    @property
    def threads(self):
        """return the thread dictionary"""
        return self._threads

    @property
    def locals(self):
        """return the locals dictionary"""
        return self._locals.values()

    @property
    def args(self):
        return []

    @property
    def watches(self):
        """return a list of watch expressions"""
        return self._watches

    @property
    def breakpoints(self):
        """return the dict of breakpoints"""
        return self._breakpoints

    def clean_stack(self):
        """remove stack frames"""
        while len(self._stack_frames) > 0:
            del self._stack_frames[0]
        self._current_frame = 0

    @property
    def stack(self):
        """return stack frame"""
        return self._stack_frames

    def add_watch_expression(self, expression):
        self._watch_count += 1
        name = 'var_{}'.format(self._watch_count)
        self._watches.append(WatchExpression(name, expression))

    def post_debugger_info(self, msg):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.DEBUGGER_INFO, message=msg + "\n"))

    def post_unknown_debug_info(self, msg):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UNKNOWN_DEBUG_INFO, msg))

    def post_debug_end(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.DEBUG_ENDED))

    def post_update_breakpoints(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_BREAKPOINTS_INFO))

    def post_update_stack_frame(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_STACK_FRAME))

    def post_update_threads(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_THREADS_INFO))

    def post_update_locals(self):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.UPDATE_LOCALS_INFO))

    def post_user_command_response(self, msg):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.USER_COMMAND_RESPONSE, msg))

    def post_file_line(self, file_, line_):
        self._handler.AddPendingEvent(
            wxx.DebuggerEvent(wxx.FILE_LINE_INFO, (file_, int(line_))))

    def post_output(self, message):
        handler = self._frame.GetEventHandler()
        handler.AddPendingEvent(wxx.OutputEvent(message))

    def activate_ui(self):
        """Activate the panes"""
        config = context.get_config()
        self._old_perspective = self._frame.m_mgr.SavePerspective()
        self._perspective = config.Read('/debuggers/python/perspectives/watch_book', '')
        status = {}
        for x in self._debug_panes:
            status[x.name] = config.ReadBool('/debuggers/python/panes/{}/visible'.format(x.name), True)
            x.index = config.ReadInt('/debuggers/python/panes/{}/index'.format(x.name), wx.NOT_FOUND)
        selected_pane = config.ReadInt('/debuggers/python/panes/selected', wx.NOT_FOUND)
        self._frame.clean_output_pane()
        self.add_debug_menu()
        self.add_stack_pane()
        self.add_debug_console_pane()
        self.activate_output_pane()
        self.activate_debug_watch_book(status)
        self.bind_events()
        if len(self._perspective):
            this_pane = self._frame.m_mgr.GetPane('watch_book')
            self._frame.m_mgr.LoadPaneInfo(self._perspective, this_pane)
        if selected_pane != wx.NOT_FOUND:
            self.m_debug_info.ChangeSelection(selected_pane)
        self._frame.m_mgr.Update()

    def bind_events(self):
        for this_pane in self._debug_panes:
            this_pane.bind_events()
        self._frame.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.on_debug_pane_closed, self.m_debug_info)
        self._frame.m_mgr.Bind(aui.EVT_AUI_PANE_CLOSE, handler=self.on_debug_watch_book_closed)

    def on_debug_pane_closed(self, event):
        event.Skip()
        index = self.m_debug_info.GetSelection()
        if index != wx.NOT_FOUND:
            try:
                instance = self.m_debug_info.GetPage(index)
                page = next(x for x in self._debug_panes if x.instance == instance)
                page.close()
                event.Veto()
            except StopIteration:
                pass

    def on_debug_watch_book_closed(self, event):
        """Process a close book event"""
        event.Skip()
        event.Skip()
        if event.GetPane().name == 'watch_book':
            for x in self._debug_panes:
                if x.instance is not None:
                    x.close()

    def unbind_events(self):
        self._frame.m_mgr.Unbind(aui.EVT_AUI_PANE_CLOSE, handler=self.on_debug_watch_book_closed)
        self._frame.Unbind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.m_debug_info)
        for this_pane in self._debug_panes:
            this_pane.unbind_events()

    def deactivate_ui(self):
        """Deactivate the ui"""
        this_pane = self._frame.m_mgr.GetPane('watch_book')
        self._perspective = self._frame.m_mgr.SavePaneInfo(this_pane)
        config = context.get_config()
        config.Write('/debuggers/python/perspectives/watch_book', self._perspective)
        for x in self._debug_panes:
            if x.instance is not None:
                x.index = self.m_debug_info.GetPageIndex(x.instance)
            config.WriteBool('/debuggers/python/panes/{}/visible'.format(x.name), x.shown)
            config.WriteInt('/debuggers/python/panes/{}/index'.format(x.name), x.index)
        if self.m_debug_info.GetPageCount() > 0:
            selection = self.m_debug_info.GetSelection()
        else:
            selection = wx.NOT_FOUND
        config.WriteInt('/debuggers/python/panes/selected', selection)
        self.unbind_events()
        self.deactivate_debug_watch_book()
        self.remove_debug_stack_pane()
        self.remove_debug_console_pane()
        self.delete_debug_menu()
        self._frame.m_mgr.LoadPerspective(self._old_perspective)
        self._frame.m_mgr.Update()

    def activate_output_pane(self):
        """Activate the output pane"""
        if self._frame.m_aux_panes == self._frame.m_aux_output_pane:
            return True
        candidate = [wx.NOT_FOUND]
        candidate.extend(i for i in range(0, self._frame.m_aux_panes.GetPageCount())
                         if self._frame.m_aux_panes.GetPage(i) == self._frame.m_aux_output_pane)
        i = max(candidate)
        if i == wx.NOT_FOUND:
            return False
        self._frame.m_aux_panes.SetSelection(i)
        return True

    def remove_debug_stack_pane(self):
        """Remove the stack pane"""
        aux_book = self._frame.m_aux_panes
        for i in range(0, aux_book.GetPageCount()):
            if aux_book.GetPage(i) == self.m_stack:
                aux_book.DeletePage(i)
                return

    def remove_debug_console_pane(self):
        """Remove the debug command pane"""
        aux_book = self._frame.m_aux_panes
        for i in range(0, aux_book.GetPageCount()):
            if aux_book.GetPage(i) == self.m_command:
                aux_book.DeletePage(i)
                return

    def add_debug_menu(self):
        """Add normal debug menu to main window"""
        bar = self._frame.m_menubar1
        help_ = self._frame.menuHelp
        self.menu_pos = bar.FindMenu(help_.menu_bar_title)
        if self.menu_pos == wx.NOT_FOUND:
            # couldn't add menu!!
            self._menu = None
            return
        self._menu = wxx.Menu()
        submenu_windows = wxx.Menu()
        for this_pane in self._debug_panes:
            this_pane.add_menu_item(submenu_windows)
        self._menu.AppendSubMenu(submenu_windows, u"Debug info panels")
        self._menu.AppendSeparator()
        # Populate debug menu
        clone_mnu(self._handler.debug_menu, self._menu)
        self._handler.register_menu('Debug', self._menu)
        self._frame.add_main_menu("Debug", self._menu, pos=self.menu_pos)

    def delete_debug_menu(self):
        """Remove normal debug menu from main window"""
        self._handler.unregister_menu('Debug')
        if self._menu is not None:
            self._frame.remove_main_menu("Debug", self._menu)
            self._menu.Destroy()
            self._menu = None

    def add_stack_pane(self):
        """Add stack frame pane"""
        self.m_stack = pane.StackFrame(self._frame.m_aux_panes, self)
        self._frame.m_aux_panes.AddPage(self.m_stack, u"stack frame", False)

    def add_debug_console_pane(self):
        """Add debug command pane"""
        self.m_command = pane.DebugConsolePane(self._frame.m_aux_panes, self)
        self._frame.m_aux_panes.AddPage(self.m_command, u"debug console", False)

    def activate_debug_watch_book(self, status):
        """Activate the watch panel"""
        self.m_debug_info = wxx.AuiNotebook(self._frame,
                                            wx.ID_ANY, wx.DefaultPosition,
                                            wx.Size(200, -1), aui.AUI_NB_DEFAULT_STYLE)
        self._frame.m_mgr.AddPane(
            self.m_debug_info,
            aui.AuiPaneInfo().
                Name('watch_book').
                Right().
                PinButton(True).
                Dock().
                Resizable().
                FloatingSize(wx.Size(120, -1)).
                DockFixed(False).
                Row(1).
                Layer(4))
        sorted_panes = sorted(self._debug_panes, key=lambda x: x.index)
        for this_pane in sorted_panes:
            this_pane.init_pane(self, status[this_pane.name])

    def deactivate_debug_watch_book(self):
        self._frame.m_mgr.DetachPane(self.m_debug_info)
        for this_pane in self._debug_panes:
            this_pane.uninit_pane()
        self.m_debug_info.Destroy()

    def update_debug_locals_ui(self):
        """Update the visual info"""
        self._debug_panes[0].update_data()

    def update_debug_breakpoints_ui(self):
        """Update the visual info"""
        self._debug_panes[2].update_data()

    def update_debug_stack_ui(self):
        """Update the visual info"""
        self.m_stack.update_data()

    def update_debug_console_pane_ui(self, response):
        """Update debug command pane"""
        self.m_command.update_data(response)

    def update_debug_threads_ui(self):
        """Update the visual info"""
        self._debug_panes[3].update_data()

    def force_stop(self):
        """Brute stop"""
        raise NotImplementedError

    def send_command(self, msg):
        """Post a command for the debugger in the queue"""
        raise NotImplementedError

    def send_user_command(self, msg):
        raise NotImplementedError

    def do_quit(self):
        raise NotImplementedError

    def do_restart(self):
        """Does nothing really here, only ask for status"""
        raise NotImplementedError

    def do_continue(self):
        raise NotImplementedError

    def disable_breakpoint(self, number, file_, line_):
        """Disable a breakpoint"""
        if number not in self._breakpoints:
            # search it
            try:
                target_line = str(line_)
                target_file = file_
                if self._source_dir:
                    target_file = os.path.join(self._source_dir, file_)
                number = next(bp for bp in self._breakpoints if
                              self._breakpoints[bp].line == target_line and
                              self._breakpoints[bp].file == target_file)
            except StopIteration as s:
                return False
        if self._breakpoints[number].attributes['enabled'] == 'y':
            self.do_disable_breakpoint(number)
        return True

    def enable_breakpoint(self, number, file_, line_):
        """Enable a breakpoint"""
        if number not in self._breakpoints:
            # search it
            try:
                target_line = str(line_)
                target_file = file_
                if self._source_dir:
                    target_file = os.path.join(self._source_dir, file_)
                number = next(bp for bp in self._breakpoints if
                              self._breakpoints[bp].line == target_line and
                              self._breakpoints[bp].file == target_file)
            except StopIteration as s:
                return False
        if self._breakpoints[number].attributes['enabled'] == 'n':
            self.do_enable_breakpoint(number)
        return True

    def delete_breakpoint(self, number, file_, line_):
        """delete a breakpoint by id or file,line"""
        # First attempt to do the best using id
        if number not in self._breakpoints:
            # search it
            try:
                target_line = str(line_)
                target_file = file_
                if self._source_dir:
                    target_file = os.path.join(self._source_dir, file_)
                number = next(bp for bp in list(self._breakpoints.values()) if
                          bp.line == target_line and bp.file == target_file).number
            except StopIteration as s:
                return False
        self.do_delete_breakpoint(number)
        return True

    def do_query_locals(self):
        """add-breakpoint must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_add_breakpoint(self, file_, line_):
        """add-breakpoint must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_delete_all_breakpoints(self):
        """delete-all-breakpoints must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_disable_breakpoint(self, number):
        """disable-breakpoint must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_enable_breakpoint(self, number):
        """enable-breakpoint must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_delete_breakpoint(self, number):
        """delete-breakpoint must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_next(self):
        """next must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_step(self):
        """step must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def do_step_out(self):
        """step-out must be implemented by the debugger-specific interface"""
        raise NotImplementedError

    def debug_script(self, script):
        """Start a debug operation. This method must be really implemented by the debugger-specific interface"""
        raise NotImplementedError

