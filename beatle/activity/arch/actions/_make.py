# -*- coding: utf-8 -*-
import wx
from beatle.lib.ostools import run


class Make(run):
    """handler of Make commands"""

    def __init__(self, project):
        super(Make, self).__init__()
        self._project = project

    def all(self):
        if "__WXMSW__" in wx.PlatformInfo:
            command = 'make all'
        else:
            command = 'make -j`nproc` all'
        kwargs = {
            'prolog': 'start build of {}:'.format(self._project.name),
            'cwd': self._project.dir,
            'command': command,
            'epilog': 'end build of {}.'.format(self._project.name)
            }
        self.__run__(**kwargs)

    def clean(self):
        kwargs = {
            'prolog': 'clean project {}:'.format(self._project.name),
            'cwd': self._project.dir,
            'command': 'make clean',
            'epilog': 'project {} cleaned.'.format(self._project.name)
            }
        self.__run__(**kwargs)
