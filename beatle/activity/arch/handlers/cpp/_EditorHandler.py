# -*- coding: utf-8 -*-
import wx
from wx import stc

from beatle.lib.handlers import EditorHandlerBase

MARGIN_LINE_NUMBERS = 0
MARK_MARGIN = 1
MARGIN_FOLD = 2

STC_MASK_MARKERS = ~stc.STC_MASK_FOLDERS

MARK_BOOKMARK = 0
MARK_BREAKPOINT_ENABLED = 1
MARK_BREAKPOINT_DISABLED = 2
MARK_BREAKPOINT_DELETED = 3
DEBUG_CURRENT = 32

# Colour type margin
SC_MARGIN_NUMBER = 1
SC_MARGIN_BACK = 2
SC_MARGIN_FORE = 3

# missing indicators
STC_INDICATOR_FULL_BOX = 16

# undefined wrappers
SCI_ANNOTATION_SET_TEXT = 2540
SCI_ANNOTATION_GET_TEXT = 2541
SCI_ANNOTATION_SET_STYLE = 2542
SCI_ANNOTATION_GET_STYLE = 2543
SCI_ANNOTATION_SET_STYLES = 2544
SCI_ANNOTATION_GET_STYLES = 2545
SCI_ANNOTATION_GET_LINES = 2546
SCI_ANNOTATION_CLEAR_ALL = 2547
ANNOTATION_HIDDEN = 0
ANNOTATION_STANDARD = 1
ANNOTATION_BOXED = 2
SCI_ANNOTATION_SET_VISIBLE = 2548
SCI_ANNOTATION_GET_VISIBLE = 2549
SCI_ANNOTATION_SET_STYLE_OFFSET = 2550
SCI_ANNOTATION_GET_STYLE_OFFSET = 2551

SCI_STYLE_SET_CHANGEABLE = 2099
STC_FIND_MATCH_CASE = 4
STC_FIND_POSIX = 4194304
STC_FIND_REGEXP = 2097152
STC_FIND_WHOLE_WORD = 2
STC_FIND_WORD_START = 1048576


BOOKMARK_TOGGLE = 30000
BOOKMARK_UP = 30001
BOOKMARK_DOWN = 30002
BREAKPOINT_TOGGLE = 30003
FIND = 30004
FIND_NEXT = 30005
FIND_PREV = 30006

STC_DEBUG_LINE = 31


class EditorHandler(EditorHandlerBase):
    """Declares c++ editor handler"""

    keyword_list = [
        "alignas",
        "alignof",
        "and",
        "and_eq",
        "asm",
        "auto",
        "bitand",
        "bitor",
        "break",
        "case",
        "catch",
        "class",
        "compl",
        "const",
        "constexpr",
        "const_cast",
        "continue",
        "decltype",
        "default",
        "delete",
        "do",
        "dynamic_cast",
        "else",
        "enum",
        "explicit",
        "export",
        "extern",
        "false",
        "for",
        "final",
        "friend",
        "goto",
        "if",
        "inline",
        "mutable",
        "namespace",
        "new",
        "noexcept",
        "not",
        "not_eq",
        "nullptr",
        "operator",
        "or",
        "or_eq",
        "private",
        "protected",
        "public",
        "register",
        "reinterpret_cast",
        "return",
        "signed",
        "sizeof",
        "static",
        "static_assert",
        "static_cast",
        "struct",
        "switch",
        "template",
        "this",
        "thread_local",
        "throw",
        "true",
        "try",
        "typedef",
        "typeid",
        "typename",
        "union",
        "unsigned",
        "using",
        "virtual",
        "void",
        "volatile",
        "wchar_t",
        "while",
        "xor xor_eq"
    ]

    style_items = [
        (stc.STC_STYLE_DEFAULT, "default"),
        (stc.STC_C_DEFAULT, "c default"),
        (stc.STC_C_DEFAULT + 64, "c default (disabled)"),
        (stc.STC_C_STRING, "string"),
        (stc.STC_C_STRINGEOL, "string eol"),
        (stc.STC_C_STRINGEOL + 64, "string eol (disabled)"),
        (stc.STC_C_STRING + 64, "string (disabled)"),
        (stc.STC_C_PREPROCESSOR, "preprocessor"),
        (stc.STC_C_PREPROCESSOR + 64, "preprocessor (disabled)"),
        (stc.STC_C_IDENTIFIER, "identifier"),
        (stc.STC_C_IDENTIFIER + 64, "identifier (disabled)"),
        (stc.STC_C_NUMBER, "number"),
        (stc.STC_C_NUMBER + 64, "number (disabled)"),
        (stc.STC_C_CHARACTER, "character"),
        (stc.STC_C_CHARACTER + 64, "character (disabled)"),
        (stc.STC_C_WORD, "keyword"),
        (stc.STC_C_WORD + 64, "keyword (disabled)"),
        (stc.STC_C_WORD2, "keyword2"),
        (stc.STC_C_WORD2 + 64, "keyword2 (disabled)"),
        (stc.STC_C_COMMENT, "comment"),
        (stc.STC_C_COMMENT + 64, "comment (disabled)"),
        (stc.STC_C_COMMENTLINE, "line comment"),
        (stc.STC_C_COMMENTLINE + 64, "line comment (disabled)"),
        (stc.STC_C_COMMENTLINEDOC, "line comment doc"),
        (stc.STC_C_COMMENTLINEDOC + 64, "line comment doc (disabled)"),
        (stc.STC_C_COMMENTDOC, "comment doc"),
        (stc.STC_C_COMMENTDOC + 64, "comment doc (disabled)"),
        (stc.STC_C_COMMENTDOCKEYWORD, "comment doc keyword"),
        (stc.STC_C_COMMENTDOCKEYWORD + 64, "comment doc keyword (disabled)"),
        (stc.STC_C_COMMENTDOCKEYWORDERROR, "comment doc keyword error"),
        (stc.STC_C_COMMENTDOCKEYWORDERROR + 64, "comment doc keyword error (disabled)"),
        (stc.STC_C_GLOBALCLASS, "global class"),
        (stc.STC_C_GLOBALCLASS + 64, "global class (disabled)"),
        # pike? (STC_C_HASH_QUOTED_STRING, "hash-quoted string"),
        # pike? (STC_C_HASH_QUOTED_STRING + 64, "hash-quoted string (disabled)"),
        (stc.STC_C_OPERATOR, "operator"),
        (stc.STC_C_OPERATOR + 64, "operator (disabled)"),
        (stc.STC_C_PREPROCESSORCOMMENT, "preprocessor comment"),
        (stc.STC_C_PREPROCESSORCOMMENT + 64, "preprocessor comment (disabled)"),
        (stc.STC_C_REGEX, "regex"),
        (stc.STC_C_REGEX + 64, "regex (disabled)"),
        (stc.STC_C_STRINGEOL, "string end-of-line"),
        (stc.STC_C_STRINGEOL + 64, "string end-of-line (disabled)"),
        (stc.STC_C_STRINGRAW, "raw string"),
        (stc.STC_C_STRINGRAW + 64, "raw string (disabled)"),
        (stc.STC_C_VERBATIM, "verbatim string"),
        (stc.STC_C_VERBATIM + 64, "verbatim string (disabled)"),
        (stc.STC_C_UUID, "uuid"),
        (stc.STC_C_UUID + 64, "uuid (disabled)"),
        (stc.STC_STYLE_BRACELIGHT, "brace light"),
        (stc.STC_STYLE_BRACELIGHT + 64, "brace light (inactive)"),
        (stc.STC_STYLE_BRACEBAD, "brace bad"),
        (stc.STC_STYLE_BRACEBAD + 64, "brace bad (inactive)"),
        (SCI_STYLE_SET_CHANGEABLE, "changeable"),
        (stc.STC_STYLE_LINENUMBER, "line numbers"),
        (stc.STC_STYLE_INDENTGUIDE, "indent guide")
        ]

    style = {
            'default': "fore:{fore},back:{back}",
            'c default': "fore:{fore},back:{back}",
            "c default (disabled)": "fore:#646464,back:{back}",
            'string': "fore:#69FFFF,back:{back}",
            'string (disabled)': "fore:#646464,back:{back}",
            'string eol': "fore:#69FFFF,back:{back}",
            'string eol (disabled)': "fore:#646464,back:{back}",
            'preprocessor': "fore:#a56900,back:{back}",
            'preprocessor (disabled)': "fore:#646464,back:{back}",
            'identifier': "fore:#DDDDDD,back:{back}",
            'identifier (disabled)': "fore:#9B9B9B,back:{back}",
            'number': "fore:#FF69FF,back:{back}",
            'number (disabled)': "fore:#FF69FF,back:{back}",
            'character': "fore:#69FFFF,back:{back}",
            'character (disabled)': "fore:#9B9B9B,back:{back}",
            'keyword': "fore:#2F96DD,back:{back}",
            'keyword (disabled)': "fore:#9B9B9B,back:{back}",
            'keyword2': "fore:#2F96DD,back:{back}",
            'keyword2 (disabled)': "fore:#9B9B9B,back:{back}",
            'comment': "fore:#696969,back:{back}",
            'comment (disabled)': "fore:#6B6B6B,back:{back}",
            'line comment': "fore:#696969,,back:{back}",
            'line comment (disabled)': "fore:#6B6B6B,,back:{back}",
            'line comment doc': "fore:#B69696,back:{back}",
            'line comment doc (disabled)': "fore:#6B6B6B,back:{back}",
            'comment doc': "fore:#B69696,back:{back}",
            'comment doc (disabled)': "fore:#BB6B6B,back:{back}",
            'comment doc keyword': "fore:#FFFF37,bold,back:{back}",
            'comment doc keyword (disabled)': "fore:#6B6B6B,bold,back:{back}",
            'comment doc keyword error': "fore:#37FFFF,bold,back:{back}",
            'comment doc keyword error (disabled)': "fore:#6B6B6B,bold,back:{back}",
            'operator': "fore:#7FFF7F,back:{back}",
            "operator (disabled)": "fore:#6B6B6B,back:{back}",
            "preprocessor comment": "fore:#969696,back:{back}",
            "preprocessor comment (disabled)": "fore:#6B6B6B,back:{back}",
            'regex': "fore:#FF6969,back:{back}",
            "brace light": "fore:#FFFF7F,back:{back},bold",
            "brace light (inactive)": "fore:#7FFF7F,back:{back},bold",
            "brace bad": "fore:#FF7F7F,bold,back:{back}",
            "brace bad (inactive)": "fore:#FF7F7F,back:{back},bold",
            "line numbers": "fore:#4B4B4B,back:{back}",
            "indent guide": "fore:#968888,back:{back}",
            # "changeable": "fore:#5F5F5F,back:#000000,bold",
            # "line numbers": "fore:#4B4B4B, back:#DCDCDC",
        }

    @classmethod
    def apply_colors(cls):
        dbc = wx.TheColourDatabase
        cls.fore = dbc.Find('LIGHT GREY').GetAsString(wx.C2S_HTML_SYNTAX)
        cls.back = dbc.Find('BLACK').GetAsString(wx.C2S_HTML_SYNTAX)

        for k in cls.style.keys():
            cls.style[k] = cls.style[k].format(fore=cls.fore, back=cls.back)

    def __init__(self, **kwargs):
        """Init"""
        # TO DO : Move to style handlers
        EditorHandler.apply_colors()
        self._fileObj = kwargs['obj']
        kwargs['lexer'] = stc.STC_LEX_CPP
        kwargs['text'] = self._fileObj.GetText()
        kwargs['read_only'] = self._fileObj.read_only
        super(EditorHandler, self).__init__(**kwargs)

    def init_fonts(self, editor):
        """default font initialization"""
        super(EditorHandler, self).init_fonts(editor)
        font = self.font
        for i in range(0, 39):
            editor.StyleSetFont(i + 64, font)    # inactive styles

    def setup_margins(self, editor):
        """Setup margins"""
        # Enable code folding
        global STC_MASK_MARKERS
        editor.SetMarginType(MARGIN_LINE_NUMBERS, stc.STC_MARGIN_NUMBER)
        editor.SetMarginType(MARK_MARGIN, stc.STC_MARGIN_SYMBOL)
        editor.SetMarginType(MARGIN_FOLD, stc.STC_MARGIN_SYMBOL)
        editor.SetMarginWidth(MARGIN_LINE_NUMBERS, 50)
        editor.SetMarginWidth(MARK_MARGIN, 11)
        editor.SetMarginWidth(MARGIN_FOLD, 15)
        editor.SetMarginMask(MARK_MARGIN, STC_MASK_MARKERS)
        editor.SetMarginMask(MARGIN_FOLD, stc.STC_MASK_FOLDERS)
        editor.SetMarginSensitive(MARK_MARGIN, True)
        editor.SetMarginSensitive(MARGIN_FOLD, True)
        editor.SetMarginSensitive(MARGIN_LINE_NUMBERS, True)

    def setup_markers(self, editor):
        """Setup markers"""
        import beatle.app.resources as rc
        editor.MarkerDefineBitmap(MARK_BOOKMARK, wx.Bitmap(rc.bookmark))
        editor.MarkerDefine(MARK_BREAKPOINT_ENABLED, stc.STC_MARK_CIRCLE, "blue", "red")
        editor.MarkerDefine(MARK_BREAKPOINT_DISABLED, stc.STC_MARK_CIRCLE, "blue", "black")
        editor.MarkerDefine(MARK_BREAKPOINT_DELETED, stc.STC_MARK_CIRCLE, "black", "black")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDEREND, stc.STC_MARK_BOXPLUSCONNECTED, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDEROPENMID, stc.STC_MARK_BOXMINUSCONNECTED, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDERMIDTAIL, stc.STC_MARK_TCORNER, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDERTAIL, stc.STC_MARK_LCORNER, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDERSUB, stc.STC_MARK_VLINE, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDER, stc.STC_MARK_BOXPLUS, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDEROPEN, stc.STC_MARK_BOXMINUS, "black", "gray")
        editor.MarkerDefine(STC_DEBUG_LINE, stc.STC_MARK_BACKGROUND,  "#442200", "#442200")

    def set_properties(self, editor):
        """Set other properties"""
        # Properties found from http://www.scintilla.org/SciTEDoc.html
        editor.SetProperty("fold", "1")
        editor.SetProperty("fold.comment", "1")
        editor.SetProperty("fold.compact", "0")
        editor.SetProperty("fold.margin.width", "5")
        editor.SetProperty("indent.automatic", "1")
        editor.SetProperty("indent.opening", "1")
        editor.SetProperty("indent.closing", "1")
        editor.SetProperty("indent.size", "4")
        editor.SetProperty("tabsize", "4")
        editor.SetProperty("indent.size", "4")
        editor.SetProperty("use.tabs", "1")
        editor.SetProperty("tab.indents", "1")
        editor.SetIndent(4)
        editor.SetUseTabs(False)
        editor.SetTabIndents(True)
        editor.SetTabWidth(4)
        editor.IndicatorSetStyle(DEBUG_CURRENT, STC_INDICATOR_FULL_BOX)
        editor.SetWrapMode(False)

    def setup_events(self, editor):
        """Setup events"""
        editor.CmdKeyAssign(ord('+'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMIN)
        editor.CmdKeyAssign(ord('-'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMOUT)
        editor.Bind(wx.EVT_MENU, editor.toggle_bookmark, id=BOOKMARK_TOGGLE)
        editor.Bind(wx.EVT_MENU, editor.PrevBookmark, id=BOOKMARK_UP)
        editor.Bind(wx.EVT_MENU, editor.NextBookmark, id=BOOKMARK_DOWN)
        editor.Bind(wx.EVT_MENU, editor.Find, id=FIND)
        editor.Bind(wx.EVT_MENU, editor.find_next, id=FIND_NEXT)
        editor.Bind(wx.EVT_MENU, editor.find_previous, id=FIND_PREV)
        editor.Bind(stc.EVT_STC_MARGINCLICK, editor.on_margin_click)
        editor.Bind(wx.EVT_KEY_UP, editor.on_key)
        editor.Bind(stc.EVT_STC_UPDATEUI, editor.on_update_edit_ui)
        editor.SetAcceleratorTable(wx.AcceleratorTable([
            # wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F9, BREAKPOINT_TOGGLE), <- mus be handled by the view
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F2, BOOKMARK_TOGGLE),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, wx.WXK_UP, BOOKMARK_UP),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, wx.WXK_DOWN, BOOKMARK_DOWN),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, ord('F'), FIND),
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F3, FIND_NEXT),
            wx.AcceleratorEntry(wx.ACCEL_SHIFT, wx.WXK_F3, FIND_PREV)
        ]))

    def initialize(self, editor):
        """Base initialization"""
        editor.StyleClearAll()
        editor.SetLexer(self.lexer)
        editor.SetCodePage(stc.STC_CP_UTF8)
        editor.SetStyleBits(editor.GetStyleBitsNeeded())
        self.init_fonts(editor)
        editor.HandleBreakpoints(self._fileObj.breakpoints)
        editor.HandleBookmarks(self._fileObj.bookmarks)
        self.set_properties(editor)
        # self.setup_events(editor)
        super(EditorHandler, self).initialize(editor)
        editor.EnableBreakpoints(True)
        editor.EnableBookmarks(True)
        editor.SetCaretForeground("{fore}".format(fore=self.fore))
        editor.SetFoldMarginHiColour(True, "{back}".format(back=self.back))
        editor.SetFoldMarginColour(True, "{back}".format(back=self.back))
        editor.SetMarginType(MARGIN_FOLD, SC_MARGIN_FORE)
        editor.SetSelBackground(1, "#333333")
