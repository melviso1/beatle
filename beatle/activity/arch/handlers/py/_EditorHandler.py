# -*- coding: utf-8 -*-

import wx
import keyword
from wx import stc
from beatle.lib.handlers import EditorHandlerBase
from beatle.app import resources as rc

MARGIN_LINE_NUMBERS = 0
MARK_MARGIN = 1
MARGIN_FOLD = 2

STC_MASK_MARKERS = ~stc.STC_MASK_FOLDERS

MARK_BOOKMARK = 0
MARK_BREAKPOINT_ENABLED = 1
MARK_BREAKPOINT_DISABLED = 2
MARK_BREAKPOINT_DELETED = 3
DEBUG_CURRENT = 32

# Colour type margin
SC_MARGIN_NUMBER = 1
SC_MARGIN_BACK = 2
SC_MARGIN_FORE = 3

# missing indicators
STC_INDICATOR_FULL_BOX = 16

# undefined wrappers
SCI_ANNOTATION_SET_TEXT = 2540
SCI_ANNOTATION_GET_TEXT = 2541
SCI_ANNOTATION_SET_STYLE = 2542
SCI_ANNOTATION_GET_STYLE = 2543
SCI_ANNOTATION_SET_STYLES = 2544
SCI_ANNOTATION_GET_STYLES = 2545
SCI_ANNOTATION_GET_LINES = 2546
SCI_ANNOTATION_CLEAR_ALL = 2547
ANNOTATION_HIDDEN = 0
ANNOTATION_STANDARD = 1
ANNOTATION_BOXED = 2
SCI_ANNOTATION_SET_VISIBLE = 2548
SCI_ANNOTATION_GET_VISIBLE = 2549
SCI_ANNOTATION_SET_STYLE_OFFSET = 2550
SCI_ANNOTATION_GET_STYLE_OFFSET = 2551

SCI_STYLE_SET_CHANGEABLE = 2099
STC_FIND_MATCH_CASE = 4
STC_FIND_POSIX = 4194304
STC_FIND_REGEXP = 2097152
STC_FIND_WHOLE_WORD = 2
STC_FIND_WORD_START = 1048576


BOOKMARK_TOGGLE = 30000
BOOKMARK_UP = 30001
BOOKMARK_DOWN = 30002
BREAKPOINT_TOGGLE = 30003
FIND = 30004
FIND_NEXT = 30005
FIND_PREV = 30006

STC_DEBUG_LINE = 31


class EditorHandler(EditorHandlerBase):
    """Declares python editor handler"""

    keyword_list = keyword.kwlist
    keyword_list2 = ['self', 'None', 'True', 'False', 'arg', 'kwarg', 'super']

    style_items = [
        (stc.STC_STYLE_DEFAULT, "default"),
        (stc.STC_STYLE_BRACELIGHT, "brace light"),
        (stc.STC_STYLE_BRACELIGHT + 64, "brace light (inactive)"),
        (stc.STC_STYLE_BRACEBAD, "brace bad"),
        (stc.STC_STYLE_BRACEBAD + 64, "brace bad (inactive)"),
        (SCI_STYLE_SET_CHANGEABLE, "changeable"),
        (stc.STC_STYLE_LINENUMBER, "line numbers"),
        (MARGIN_FOLD, "fold margin"),
        (stc.STC_STYLE_INDENTGUIDE, "indent guide"),
        (stc.STC_P_DEFAULT, 'python default'),
        (stc.STC_P_CHARACTER, 'char'),
        (stc.STC_P_CLASSNAME, 'class name'),
        (stc.STC_P_COMMENTBLOCK, 'comment block'),
        (stc.STC_P_COMMENTLINE, 'line comment'),
        (stc.STC_P_COMMENTLINE + 64, 'line comment (disabled)'),
        (stc.STC_P_DECORATOR, 'decorator'),
        (stc.STC_P_DEFNAME, 'function name'),
        (stc.STC_P_DEFNAME + 64, 'function name (disabled)'),
        (stc.STC_P_NUMBER, "number"),
        (stc.STC_P_IDENTIFIER, 'identifier'),
        (stc.STC_P_IDENTIFIER + 64, 'identifier (disabled)'),
        (stc.STC_P_OPERATOR, "operator"),
        (stc.STC_P_OPERATOR + 64, "operator (disabled)"),
        (stc.STC_P_STRING, "string"),
        (stc.STC_P_STRINGEOL, "string eol"),
        (stc.STC_P_TRIPLE, "triple quotes"),
        (stc.STC_P_TRIPLEDOUBLE, "triple double quotes"),
        (stc.STC_P_WORD, "keyword"),
        (stc.STC_P_WORD + 64, "keyword (disabled)"),
        (stc.STC_P_WORD2, "keyword2"),
        (stc.STC_P_WORD2 + 64, "keyword2 (disabled)"),
    ]

    style = {
        'default': "fore:#FFFFFF,back:#000000",
        'python default': "fore:#FFFFFF,back:#000000",
        'char': "fore:#69FFFF,back:#000000",
        'class name': "fore:#FFFF69,bold,back:#000000",
        'line comment': "fore:#696969,,back:#000000",
        'line comment (disabled)': "fore:#6B6B6B,,back:#000000",
        'decorator': "fore:#9BFF9B,back:#000000",
        'function name': "fore:#E7DDC3,back:#000000",
        'function name (disabled)': "fore:#73EEE1,back:#000000",
        'identifier': "fore:#DDDDDD,back:#000000",
        'identifier (disabled)': "fore:#9B9B9B,back:#000000",
        "number": "fore:#80A0A0,back:#000000",
        'operator': "fore:#7FFF7F,back:#000000",
        "operator (disabled)": "fore:#6B6B6B,back:#000000",
        'string': "fore:#69FFFF,back:#000000",
        'string (disabled)': "fore:#646464,back:#000000",
        'string eol': "fore:#69FFFF,back:#000000",
        'string eol (disabled)': "fore:#646464,back:#000000",
        "triple quotes": "fore:#69FFFF,back:#000000",
        "triple double quotes": "fore:#69FFFF,back:#000000",
        'keyword': "fore:#967FFF,bold,back:#000000",
        'keyword (disabled)': "fore:#9B9B9B,bold,back:#000000",
        'keyword2': "fore:#FF2F96,back:#000000",
        'keyword2 (disabled)': "fore:#9B9B9B,back:#000000",
        "brace light": "fore:#FFFF7F,back:#000000,bold",
        "brace light (inactive)": "fore:#7FFF7F,back:#000000,bold",
        "brace bad": "fore:#FF7F7F,bold,back:#000000",
        "changeable": "fore:#A0A0A0,back:#FFFFFF,bold",
        "line numbers": "fore:#4B4B4B,back:#000000",
        "indent guide": "fore:#968888,back:#000000",
        "fold margin": "back:#343434",
        }

    def __init__(self, **kwargs):
        """Init"""
        self._fileObj = kwargs['obj']
        kwargs['lexer'] = stc.STC_LEX_PYTHON
        kwargs['text'] = self._fileObj.GetText()
        kwargs['read_only'] = self._fileObj.read_only
        super(EditorHandler, self).__init__(**kwargs)

    def init_fonts(self, editor):
        """default font initialization"""
        super(EditorHandler, self).init_fonts(editor)
        font = self.font
        for i in range(0, 39):
            editor.StyleSetFont(i + 64, font)    # inactive styles

    def setup_margins(self, editor):
        """Setup margins"""
        # Enable code folding
        global STC_MASK_MARKERS
        editor.SetMarginType(MARGIN_LINE_NUMBERS, stc.STC_MARGIN_NUMBER)
        editor.SetMarginType(MARK_MARGIN, stc.STC_MARGIN_SYMBOL)
        editor.SetMarginType(MARGIN_FOLD, stc.STC_MARGIN_SYMBOL)
        editor.SetMarginWidth(MARGIN_LINE_NUMBERS, 50)
        editor.SetMarginWidth(MARK_MARGIN, 11)
        editor.SetMarginWidth(MARGIN_FOLD, 15)
        editor.SetMarginMask(MARK_MARGIN, STC_MASK_MARKERS)
        editor.SetMarginMask(MARGIN_FOLD, stc.STC_MASK_FOLDERS)
        editor.SetMarginSensitive(MARK_MARGIN, True)
        editor.SetMarginSensitive(MARGIN_FOLD, True)
        editor.SetMarginSensitive(MARGIN_LINE_NUMBERS, True)

    def setup_markers(self, editor):
        """Setup markers"""
        editor.MarkerDefineBitmap(MARK_BOOKMARK, wx.Bitmap(rc.bookmark))
        editor.MarkerDefine(MARK_BREAKPOINT_ENABLED, stc.STC_MARK_CIRCLE, "blue", "red")
        editor.MarkerDefine(MARK_BREAKPOINT_DISABLED, stc.STC_MARK_CIRCLE, "blue", "black")
        editor.MarkerDefine(MARK_BREAKPOINT_DELETED, stc.STC_MARK_CIRCLE, "blue", "black")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDEREND, stc.STC_MARK_BOXPLUSCONNECTED, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDEROPENMID, stc.STC_MARK_BOXMINUSCONNECTED, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDERMIDTAIL, stc.STC_MARK_TCORNER, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDERTAIL, stc.STC_MARK_LCORNER, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDERSUB, stc.STC_MARK_VLINE, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDER, stc.STC_MARK_BOXPLUS, "black", "gray")
        editor.MarkerDefine(stc.STC_MARKNUM_FOLDEROPEN, stc.STC_MARK_BOXMINUS, "black", "gray")
        editor.MarkerDefine(STC_DEBUG_LINE, stc.STC_MARK_BACKGROUND, "#442200", "#442200")

    def set_properties(self, editor):
        """Set other properties"""
        # Properties found from http://www.scintilla.org/SciTEDoc.html
        editor.SetProperty("fold", "1")
        editor.SetProperty("fold.comment", "1")
        editor.SetProperty("fold.compact", "0")
        editor.SetProperty("fold.margin.width", "5")
        editor.SetProperty("indent.automatic", "1")
        editor.SetProperty("indent.opening", "1")
        editor.SetProperty("indent.closing", "1")
        editor.SetProperty("indent.size", "4")
        editor.SetProperty("tabsize", "4")
        editor.SetProperty("indent.size", "4")
        editor.SetProperty("use.tabs", "1")
        editor.SetProperty("tab.indents", "1")
        editor.SetProperty("tab.timmy.whinge.level", "1")  # python indent
        editor.SetIndent(4)
        editor.SetUseTabs(False)
        editor.SetTabIndents(True)
        editor.SetTabWidth(4)
        editor.IndicatorSetStyle(DEBUG_CURRENT, STC_INDICATOR_FULL_BOX)
        editor.SetWrapMode(False)

    def setup_events(self, editor):
        """Set event handling"""
        editor.CmdKeyAssign(ord('+'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMIN)
        editor.CmdKeyAssign(ord('-'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMOUT)
        editor.Bind(wx.EVT_MENU, editor.toggle_bookmark, id=BOOKMARK_TOGGLE)
        editor.Bind(wx.EVT_MENU, editor.PrevBookmark, id=BOOKMARK_UP)
        editor.Bind(wx.EVT_MENU, editor.NextBookmark, id=BOOKMARK_DOWN)
        editor.Bind(wx.EVT_MENU, editor.Find, id=FIND)
        editor.Bind(wx.EVT_MENU, editor.find_next, id=FIND_NEXT)
        editor.Bind(wx.EVT_MENU, editor.find_previous, id=FIND_PREV)
        editor.Bind(stc.EVT_STC_MARGINCLICK, editor.on_margin_click)
        editor.Bind(wx.EVT_KEY_UP, editor.on_key)
        editor.Bind(stc.EVT_STC_UPDATEUI, editor.on_update_edit_ui)

        editor.SetAcceleratorTable( wx.AcceleratorTable([
            # wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F9, BREAKPOINT_TOGGLE), <- mus be handled by the view
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F2, BOOKMARK_TOGGLE),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, wx.WXK_UP, BOOKMARK_UP),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, wx.WXK_DOWN, BOOKMARK_DOWN),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, ord('F'), FIND),
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F3, FIND_NEXT),
            wx.AcceleratorEntry(wx.ACCEL_SHIFT, wx.WXK_F3, FIND_PREV),
        ]))

    def initialize(self, editor):
        """Base initialization"""
        editor.StyleClearAll()
        editor.SetLexer(self.lexer)
        editor.SetCodePage(stc.STC_CP_UTF8)
        editor.SetStyleBits(editor.GetStyleBitsNeeded())
        self.init_fonts(editor)
        editor.HandleBreakpoints(self._fileObj.breakpoints)
        editor.HandleBookmarks(self._fileObj.bookmarks)

        # this feature is really ugly! (80 columns gray marker)
        #
        self.set_properties(editor)
        self.setup_events(editor)

        super(EditorHandler, self).initialize(editor)
        editor.SetCaretForeground("#FFFFFF")
        editor.SetFoldMarginHiColour(True, "#000000")
        editor.SetFoldMarginColour(True, "#000000")
        editor.SetMarginType(MARGIN_FOLD, SC_MARGIN_FORE)
        editor.SetSelBackground(1, "#333333")
        editor.EnableBreakpoints(True)
        editor.EnableBookmarks(True)
