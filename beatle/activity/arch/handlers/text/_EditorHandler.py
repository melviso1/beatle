# -*- coding: utf-8 -*-


import wx
from wx.stc import STC_MASK_FOLDERS, STC_P_DEFAULT, STC_STYLE_DEFAULT, STC_LEX_NULL,\
    STC_MARGIN_SYMBOL, STC_MARGIN_NUMBER, STC_SCMOD_CTRL, STC_STYLE_LINENUMBER,\
    STC_CMD_ZOOMIN, STC_CMD_ZOOMOUT, EVT_STC_MARGINCLICK, \
    EVT_STC_UPDATEUI, STC_CP_UTF8

from beatle.lib.handlers import EditorHandlerBase
from beatle.app import resources as rc

MARGIN_LINE_NUMBERS = 0
MARK_MARGIN = 1
MARGIN_FOLD = 2

STC_MASK_MARKERS = ~STC_MASK_FOLDERS

MARK_BOOKMARK = 0
MARK_BREAKPOINT_ENABLED = 1
MARK_BREAKPOINT_DISABLED = 2
MARK_BREAKPOINT_DELETED = 3
DEBUG_CURRENT = 32

# missing indicators
STC_INDICATOR_FULL_BOX = 16

# undefined wrappers
SCI_ANNOTATIONSETTEXT = 2540
SCI_ANNOTATIONGETTEXT = 2541
SCI_ANNOTATION_SET_STYLE = 2542
SCI_ANNOTATION_GET_STYLE = 2543
SCI_ANNOTATION_SET_STYLES = 2544
SCI_ANNOTATION_GET_STYLES = 2545
SCI_ANNOTATION_GET_LINES = 2546
SCI_ANNOTATION_CLEAR_ALL = 2547
ANNOTATION_HIDDEN = 0
ANNOTATION_STANDARD = 1
ANNOTATION_BOXED = 2
SCI_ANNOTATION_SET_VISIBLE = 2548
SCI_ANNOTATION_GET_VISIBLE = 2549
SCI_ANNOTATION_SET_STYLE_OFFSET = 2550
SCI_ANNOTATION_GET_STYLE_OFFSET = 2551

SCI_STYLE_SET_CHANGEABLE = 2099
STC_FIND_MATCH_CASE = 4
STC_FIND_POSIX = 4194304
STC_FIND_REGEXP = 2097152
STC_FIND_WHOLE_WORD = 2
STC_FIND_WORD_START = 1048576


BOOKMARK_TOGGLE  = 30000
BOOKMARK_UP      = 30001
BOOKMARK_DOWN    = 30002
BREAKPOINT_TOGGLE= 30003
FIND = 30004
FIND_NEXT = 30005
FIND_PREV = 30006

import keyword


class EditorHandler(EditorHandlerBase):
    """Declares SQL editor handler"""

    keyword_list = keyword.kwlist
    keyword_list2 = []

    style_items = [
        (STC_STYLE_DEFAULT, 'default'),
        (STC_P_DEFAULT, 'paragraph default'),
        (STC_STYLE_LINENUMBER, "line numbers"),
        (MARGIN_FOLD, "fold margin"),
    ]

    style = {
        "fold margin": "back:#CBCBCB",
        'default': "fore:#FFFFFF,back:#000000",
        'paragraph default': "fore:#FFFFFF,back:#000000",
        "line numbers": "fore:#4B4B4B,back:#000000",
        #"indent guide": "fore:#968888,back:#000000",
    }

    def __init__(self, **kwargs):
        """Init"""
        self._fileObj = kwargs['obj']
        kwargs['lexer'] = STC_LEX_NULL
        kwargs['text'] = self._fileObj.GetText()
        kwargs['read_only'] = self._fileObj._read_only
        super(EditorHandler, self).__init__(**kwargs)

    def setup_margins(self, editor):
        """Setup margins"""
        # Enable code folding
        editor.SetMarginType(MARK_MARGIN, STC_MARGIN_SYMBOL)
        editor.SetMarginType(MARGIN_LINE_NUMBERS, STC_MARGIN_NUMBER)
        editor.SetMarginWidth(MARK_MARGIN, 11)
        editor.SetMarginMask(MARK_MARGIN, STC_MASK_MARKERS)
        editor.SetMarginSensitive(MARK_MARGIN, True)
        editor.SetMarginWidth(MARGIN_LINE_NUMBERS, 50)
        editor.SetMarginSensitive(MARGIN_LINE_NUMBERS, True)

    def setup_markers(self, editor):
        """Setup markers"""
        editor.MarkerDefineBitmap(MARK_BOOKMARK, wx.Bitmap(rc.bookmark))

    def set_properties(self, editor):
        """Set other properties"""
        # Properties found from http://www.scintilla.org/SciTEDoc.html
        editor.SetProperty("tabsize", "4")
        editor.SetProperty("use.tabs", "1")
        editor.SetUseTabs(True)
        editor.SetTabWidth(4)
        editor.SetWrapMode(True)

    def setup_events(self, editor):
        """Set event handling"""
        editor.CmdKeyAssign(ord('+'), STC_SCMOD_CTRL, STC_CMD_ZOOMIN)
        editor.CmdKeyAssign(ord('-'), STC_SCMOD_CTRL, STC_CMD_ZOOMOUT)
        editor.Bind(wx.EVT_MENU, editor.toggle_bookmark, id=BOOKMARK_TOGGLE)
        editor.Bind(wx.EVT_MENU, editor.PrevBookmark, id=BOOKMARK_UP)
        editor.Bind(wx.EVT_MENU, editor.NextBookmark, id=BOOKMARK_DOWN)
        editor.Bind(wx.EVT_MENU, editor.Find, id=FIND)
        editor.Bind(wx.EVT_MENU, editor.find_next, id=FIND_NEXT)
        editor.Bind(wx.EVT_MENU, editor.find_previous, id=FIND_PREV)
        editor.Bind(EVT_STC_MARGINCLICK, editor.on_margin_click)
        editor.Bind(wx.EVT_KEY_UP, editor.on_key)
        editor.Bind(EVT_STC_UPDATEUI, editor.on_update_edit_ui)

        aTable = wx.AcceleratorTable([
            # wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F9, BREAKPOINT_TOGGLE), <- mus be handled by the view
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F2, BOOKMARK_TOGGLE),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, wx.WXK_UP, BOOKMARK_UP),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, wx.WXK_DOWN, BOOKMARK_DOWN),
            wx.AcceleratorEntry(wx.ACCEL_CTRL, ord('F'), FIND),
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_F3, FIND_NEXT),
            wx.AcceleratorEntry(wx.ACCEL_SHIFT, wx.WXK_F3, FIND_PREV),
        ])
        editor.SetAcceleratorTable(aTable)

    def initialize(self, editor):
        """Base initialization"""
        editor.StyleClearAll()
        editor.SetLexer(self.lexer)
        editor.SetCodePage(STC_CP_UTF8)
        editor.SetStyleBits(editor.GetStyleBitsNeeded())
        self.init_fonts(editor)
        editor.HandleBookmarks(self._fileObj.bookmarks)
        editor.EnableBreakpoints(False)
        editor.EnableBookmarks(True)
        self.set_properties(editor)
        self.setup_events(editor)
        super(EditorHandler, self).initialize(editor)
        editor.SetCaretForeground("#FFFFFF")
