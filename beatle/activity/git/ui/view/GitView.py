import ast
import wx


from beatle import model, local_path
from beatle.model import git
from beatle.lib import wxx
from beatle.lib.decorators import class_property
from beatle.lib.handlers import Identifiers
from beatle.lib.tran import TransactionalMethod, TransactionStack

from beatle.app import resources as rc
from beatle.app.ui.tools import clone_mnu
from beatle.app.ui.wnd import WorkingWindow
from beatle.app.ui.view import BaseView
from beatle.lib.api import context
from beatle.activity.git.ui import ui
from beatle.activity.git.ui import dlg as ui_dlg


# Implementing GitView
class GitView(BaseView, ui.GitView):
    """View pane for git repository"""
    perspective = ''
    clipboard_handler = False

    #command ids
    _editDelete = Identifiers.register("ID_DELETE")
    _openRepo = Identifiers.register("ID_OPEN_REPO")
    _newRepo = Identifiers.register("ID_NEW_REPO")
    _addItem = Identifiers.register("ID_ADD_ITEM")
    _delItem = Identifiers.register("ID_DEL_ITEM")
    _stageItem = Identifiers.register("ID_STAGE_ITEM")
    _commitItem = Identifiers.register("ID_COMMIT_ITEM")
    _refreshRepo = Identifiers.register("ID_REFRES_REPO")
    _addRemote = Identifiers.register("ID_ADD_REMOTE")
    _pushRemote = Identifiers.register("ID_PUSH_REMOTE")


    def __init__(self, parent, root=None):
        """Initialize pane"""
        super(GitView, self).__init__(parent)
        self.frame = parent
        self.selected = None
        self.image_list = rc.get_bitmap_image_list()
        self.m_tree.SetImageList(self.image_list)
        self.m_tree.AddRoot('Repositories')
        self._create_menus()
        self._bind_events()
        self._set_accelerators()
        # when we create a models view, we need to update elements
        if root is None:
            # reload full stack
            app = context.get_app()
            for wrk in app.workspaces:
                self.insert(wrk)
        else:
            self.insert(root)

    def insert(self, element):
        """Nested insert elements in tree"""
        if self.do_render_add_element(element):
            for cls in element._child:
                for k in element[cls]:
                    self.insert(k)

    def get_view_status(self):
        """Get working info about the view"""
        return str(self.m_tree.GetExpansionState())

    def set_view_status(self, status):
        """Set working info about the view"""
        try:
            self.m_tree.SetExpansionState(ast.literal_eval(status))
            return True
        except:
            return False


    @class_property
    def name(cls):
        """returns the name of this view"""
        return 'Git'

    @classmethod
    def bitmap_index(cls):
        """return the bitmap index"""
        return rc.get_bitmap_index('git')

    @classmethod
    def bitmap(cls):
        """return the bitmap"""
        return rc.get_bitmap('git')

    def _create_menus(self):
        """Create a custom menu entry in main menu"""
        self._menu = wxx.Menu(
            [self._newRepo, u"New repository", u"create new git repository", wx.ITEM_NORMAL, wx.ArtProvider.GetBitmap(wx.ART_NEW, wx.ART_MENU)],
            [self._openRepo, u"Open repository", u"open existing repository", wx.ITEM_NORMAL, wx.ArtProvider.GetBitmap(wx.ART_FILE_OPEN, wx.ART_MENU)],
            [],
            [self._addRemote, u"Add remote", u"add remote repository", wx.ITEM_NORMAL, wx.Bitmap(local_path("app/res/git_remote.xpm"), wx.BITMAP_TYPE_ANY)],
            [self._pushRemote, u"Push remote", u"push remote repository", wx.ITEM_NORMAL, wx.Bitmap(local_path("app/res/git_push.xpm"), wx.BITMAP_TYPE_ANY)],
            [],
            [self._addItem, u"Add to repository", u"add item to repository control", wx.ITEM_NORMAL],
            [self._delItem, u"Remove from repository", u"remove item from repository control", wx.ITEM_NORMAL],
            [self._stageItem, u"Stage item", u"stage item to repository control", wx.ITEM_NORMAL],
            [self._commitItem, u"Commit repository", u"commit changes to repository", wx.ITEM_NORMAL],
            [self._refreshRepo, u"Refresh", u"update repository status", wx.ITEM_NORMAL, rc.get_bitmap('reload')],
            )

        self.register_menu('Git', self._menu)

    def _bind_events(self):
        """Binds events"""
        # Tree events
        self.m_tree.Bind(wx.EVT_TREE_SEL_CHANGED, self.on_tree_selection_changed)
        self.m_tree.Bind(wx.EVT_RIGHT_DOWN, self.on_tree_menu)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_delete, id=self._editDelete)
        self.Bind(wx.EVT_MENU, self.on_delete, id=self._editDelete)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_new_repository, id=self._newRepo)
        self.bind_special(wx.EVT_MENU, self.on_new_repository, id=self._newRepo)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_open_existing_repository, id=self._openRepo)
        self.bind_special(wx.EVT_MENU, self.on_open_existing_repository, id=self._openRepo)

        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateAddRemote, id=self._addRemote)
        self.bind_special(wx.EVT_MENU, self.OnAddRemote, id=self._addRemote)

        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdatePushRemote, id=self._pushRemote)
        self.bind_special(wx.EVT_MENU, self.OnPushRemote, id=self._pushRemote)

        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateAddItemToRepository, id=self._addItem)
        self.bind_special(wx.EVT_MENU, self.OnAddItemToRepository, id=self._addItem)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_delete_item_from_repository, id=self._delItem)
        self.Bind(wx.EVT_MENU, self.on_delete_item_from_repository, id=self._delItem)

        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateStageItem, id=self._stageItem)
        self.bind_special(wx.EVT_MENU, self.OnStageItem, id=self._stageItem)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_commit_repository, id=self._commitItem)
        self.bind_special(wx.EVT_MENU, self.on_commit_repository, id=self._commitItem)

        self.bind_special(wx.EVT_UPDATE_UI, self.OnUpdateRefreshStatus, id=self._refreshRepo)
        self.bind_special(wx.EVT_MENU, self.OnRefreshStatus, id=self._refreshRepo)

        super(GitView, self)._bind_events()

    def _set_accelerators(self):
        """Set the accelerator table"""
        # ctrl_alt = wx.ACCEL_CTRL + wx.ACCEL_ALT
        # ctrl_shf = wx.ACCEL_CTRL + wx.ACCEL_SHIFT
        aTable = wx.AcceleratorTable([
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_LEFT, BaseView._leftKeyId)
        ])
        self.SetAcceleratorTable(aTable)

    def do_render_add_element(self, obj):
        """Inserts element in tree"""
        # model tree
        tree_order = [model.Workspace, git.GitRepo, git.GitRemotes,
            git.GitRemote, git.GitDir, git.GitFile]
        if type(obj) not in tree_order:
            return False
        ti = tree_order.index(type(obj))
        #check parent
        if self.m_tree.HoldsObject(obj.parent):
            p = obj.parent
        else:
            p = self.m_tree.GetRootItem()
        if obj.parent is not None:
            # find some major friend item with the same class
            tribal = obj.parent[type(obj)]
            index = tribal.index(obj)
            pre = None
            while index > 0 and pre is None:
                index = index - 1
                candidate = tribal[index]
                if not self.m_tree.HoldsObject(candidate):
                    continue
                pre = candidate
            if pre is not None:
                self.m_tree.InsertItem(p, pre, obj.label,
                    obj.bitmap_index, obj.bitmap_index, obj)
                if type(obj) is model.cc.Constructor:
                    self.m_tree.SetItemBold(obj, bold=obj.is_preferred())
                elif type(obj) in [model.py.Module, model.py.Package]:
                    if obj._entry:
                        self.m_tree.SetItemTextColour(obj, wx.RED)
                    else:
                        self.m_tree.SetItemTextColour(obj, wx.LIGHT_GREY)
                    self.m_tree.SetItemBold(obj, bold=obj._entry)
                return True
        itemCount = 0
        citem, cookie = self.m_tree.GetFirstChild(p)
        if type(citem) is wxx.agw.GenericTreeItem:
            citem = self.m_tree.__fer__(citem)
        if type(citem) in tree_order:
            if ti <= tree_order.index(type(citem)):
                self.m_tree.PrependItem(p, obj.label,
                     obj.bitmap_index, obj.bitmap_index, obj)
                return True
        while type(citem) is not wxx.agw.GenericTreeItem or citem.IsOk():
            itemCount = itemCount + 1
            citem, cookie = self.m_tree.GetNextChild(p, cookie)
            if type(citem) not in tree_order:
                continue
            if ti <= tree_order.index(type(citem)):
                self.m_tree.InsertItemBefore(p, itemCount,
                     obj.label,
                     obj.bitmap_index, obj.bitmap_index, data=obj)
                return True
        #Ok, do apppend
        self.m_tree.AppendItem(p, obj.label,
            obj.bitmap_index, obj.bitmap_index, obj)
        return True

    def do_render_remove_element(self, obj):
        """Do remove element in tree"""
        if self.m_tree.HoldsObject(obj):
            self.m_tree.Delete(obj)

    def update_element(self, obj):
        """Update the tree label for a object"""
        if not self.m_tree.HoldsObject(obj):
            return
        self.m_tree.SetItemText(obj, obj.label)
        self.m_tree.SetItemImage(obj,
            obj.bitmap_index, wx.TreeItemIcon_Normal)
        if hasattr(obj, 'bitmap_open_index'):
            self.m_tree.SetItemImage(obj,
                obj.bitmap_open_index, wx.TreeItemIcon_Expanded)
            self.m_tree.SetItemImage(obj,
                obj.bitmap_open_index, wx.TreeItemIcon_SelectedExpanded)
        self.m_tree.SetItemImage(obj,
            obj.bitmap_index, wx.TreeItemIcon_Selected)

    def on_tree_selection_changed(self, event):
        """Handle select changed"""
        self.selected = self.m_tree.GetSelection()

    def on_update_new_repository(self, event):
        """Update new repository command"""
        event.Enable(bool(self.selected and type(self.selected) is model.Workspace))

    @TransactionalMethod('new git repo {0}')
    @wxx.CreationDialog(ui_dlg.GitRepoDialog, git.GitRepo)
    def on_new_repository(self, event):
        """New repository command"""
        return (context.get_frame(), self.selected.inner_repository_container)

    def on_update_open_existing_repository(self, event):
        """Update open existing repository"""
        event.Enable(bool(self.selected and type(self.selected) is model.Workspace))

    @TransactionalMethod('add git repo {0}')
    @wxx.CreationDialog(ui_dlg.OpenGitRepoDialog, git.GitRepo)
    def on_open_existing_repository(self, event):
        """Handle add folder command"""
        return (context.get_frame(), self.selected.inner_repository_container)

    def OnUpdateAddRemote(self, event):
        """Update add remote repo"""
        event.Enable(bool(self.selected and type(self.selected) is not model.Workspace
            and self.selected.repo))

    @TransactionalMethod('add remote repo {0}')
    @wxx.CreationDialog(ui_dlg.GitRemoteDialog, git.GitRemote)
    def OnAddRemote(self, event):
        """Handle add remote repo"""
        rl = self.selected.repo(git.GitRemotes)
        return  context.get_frame(), rl[0]

    def OnUpdatePushRemote(self, event):
        """Update add remote repo"""
        event.Enable(bool(self.selected and type(self.selected) is git.GitRemote))

    def OnPushRemote(self, event):
        """Handle add remote repo"""
        _repo = self.selected.repo._repo
        remote = _repo.remote(self.selected.name)
        dialog = ui_dlg.ProgressDialog(self.frame)
        dialog.Show()
        # wx.YieldIfNeeded()
        try:
            remote.push(progress=dialog.link())
            dialog.update('--', 0, message='DONE')
        except:
            dialog.update('--', 100, message='FAILED')
            pass
        dialog.Finished()
        return
        #         import git
        #         g = git.cmd.Git(self.selected.repo._uri)
        #         g.push('--set-upstream', self.selected.name, 'master')
        #         return
        #         _repo = self.selected.repo._repo
        #         ref = _repo.remote(self.selected.name)
        #         try:
        #             dialog = ui_dlg.ProgressDialog(self, self.frame)
        #             dialog.Show()
        #             if self.selected.password:
        #                 ref.push(refspec='refs/heads/master:refs/heads/master', password=self.selected.password,
        #                     progress=dialog)
        #             else:
        #                 ref.push(refspec='refs/heads/master:refs/heads/master', progress=dialog)
        #         except Exception, e:
        #             wx.MessageBox("Push to repository {name} failed: {error}.".format(
        #                 name=self.selected.name, error=e), "Error",
        #                 wx.OK | wx.CENTER | wx.ICON_ERROR, self)
        #         dialog.Finished()

    def on_tree_left_key(self, event):
        """If the selected node is expanded, simply collapse it.
        If not, navigate through parent"""
        if not self.selected:
            return
        if self.m_tree.IsExpanded(self.selected):
            self.m_tree.Collapse(self.selected)
        else:
            parent = self.selected.parent
            if self.m_tree.HoldsObject(parent):
                self.m_tree.SelectItem(parent)

    def on_tree_menu(self, event):
        """Handles context tree popup menu"""
        item = self.m_tree.HitTest(event.GetPosition())[0]
        if item is None:
            return
        self.m_tree.SelectItem(item)
        if self.selected is None:
            return
        menu = clone_mnu(self._menu, enabled=True, notitle=True)
        if menu is None:
            return
        menu.Popup(event.GetPosition(), owner=self)

    def on_delete(self, event):
        """delete element"""
        focus = self.FindFocus()
        if focus is not self.m_tree:
            event.Skip()
            return False
        obj = self.selected
        alias_map = {
            git.GitRepo: "git repository {0}",
            git.GitFile: "git file {0}",
            git.GitDir: "git dir {0}",
            git.GitRemote: "git remote {0}"}
        TransactionStack.do_begin_transaction("delete " +
             alias_map[type(obj)].format(obj._name))
        obj.delete()
        TransactionStack.do_commit()

    def on_update_delete(self, event):
        """Update delete element"""
        obj = self.selected
        event.Enable((not obj is None) and type(obj) is not model.Workspace
            and obj.can_delete)

    def on_update_commit_repository(self, event):
        """Update commit to repo"""
        obj = self.selected
        event.Enable(bool(obj and type(obj) is git.GitRepo
        and obj.stage_size > 0))

    def on_commit_repository(self, event):
        """Process a commit"""
        dialog = ui_dlg.CommitGitDialog(context.get_frame())
        if dialog.ShowModal() == wx.ID_OK:
            self.selected.commit(dialog._message)

    def OnUpdateRefreshStatus(self, event):
        """Update refresh status"""
        obj = self.selected
        event.Enable((not obj is None) and type(obj) is not model.Workspace)

    @TransactionalMethod('refresh git status')
    def OnRefreshStatus(self, event):
        """Refresh repository status"""
        working = WorkingWindow(context.get_frame())
        working.Show(True)
        # wx.YieldIfNeeded()
        self.selected.update_status()
        working.Close()
        return True

    def OnUpdateAddItemToRepository(self, event):
        """Update the operation of adding elements to repo"""
        obj = self.selected
        if not obj:
            event.Enable(False)
            return
        if type(obj) is git.GitFile:
            if obj._status == 'file':
                event.Enable(True)
            else:
                event.Enable(False)
            return
        #ok, buscamos un hijo
        fn = lambda x: (x._status == 'file')
        if obj(git.GitFile, filter=fn):
            event.Enable(True)
        else:
            event.Enable(False)

    def on_update_delete_item_from_repository(self, event):
        """Update the operation of removing element from git control"""
        obj = self.selected
        if not obj:
            event.Enable(False)
            return
        if type(obj) is git.GitFile:
            if obj._status not in ['git_file_deleted', 'file']:
                event.Enable(True)
            else:
                event.Enable(False)
            return
        #ok, buscamos un hijo
        fn = lambda x: (x._status not in ['git_file_deleted', 'file'])
        if obj(git.GitFile, filter=fn):
            event.Enable(True)
        else:
            event.Enable(False)

    def OnAddItemToRepository(self, event):
        """Add element to track"""
        obj = self.selected
        repo = obj.repo
        if type(obj) is git.GitFile:
            repo.add(obj)
            return
        fn = lambda x: (x._status == 'file')
        for item in obj(git.GitFile, filter=fn):
            repo.add(item)

    def on_delete_item_from_repository(self, event):
        """Remove element from tracking"""
        obj = self.selected
        repo = obj.repo
        if type(obj) is git.GitFile:
            repo.supr(obj)
            return
        fn = lambda x: (x._status not in ['git_file_deleted', 'file'])
        for item in obj(git.GitFile, filter=fn):
            repo.supr(item)

    def OnUpdateStageItem(self, event):
        """Update the operation of adding elements to repo"""
        obj = self.selected
        if not obj:
            event.Enable(False)
            return
        # para hacer stage es suficiente que el elemento
        # seleccionado o un hijo sea un GitFile con status modified
        if type(obj) is git.GitFile:
            if obj._status == 'git_file_modified':
                event.Enable(True)
            else:
                event.Enable(False)
            return
        # ok, buscamos un hijo
        fn = lambda x: (x._status == 'git_file_modified')
        if obj(git.GitFile, filter=fn):
            event.Enable(True)
        else:
            event.Enable(False)

    def OnStageItem(self, event):
        """Add element to track"""
        obj = self.selected
        repo = obj.repo
        if type(obj) is git.GitFile:
            repo.add(obj)
            return
        fn = lambda x: (x._status == 'git_file_modified')
        for item in obj(git.GitFile, filter=fn):
            repo.add(item)

    def on_update_edit_properties(self, event):
        """"""
        event.Enable(False)

    def on_edit_properties(self, event):
        """"""
        pass
