# -*- coding: utf-8 -*-

import os
import sys

__dir__ = os.path.realpath(os.path.abspath(os.path.dirname(__file__)))
__base__ = __dir__
if __base__[-1] in ["/", "\\"]:
    __base__ = __dir__[:-1]
__base__ = os.path.dirname(__base__)
if __base__ not in sys.path:
    sys.path.insert(0, __base__)


def local_path(relative_path):
    return os.path.join(__dir__, relative_path)


def do_imports():
    from . import lib
    from . import analytic
    from . import ctx
    from . import model
    from . import app
    from . import activity
    from . import plugin


do_imports()
