import sys
try:
    if '3.7' in sys.version:
        from .python37.bpy_wx_aui import StdToolBarArt
    else:
        from .bpy_wx_aui import StdToolBarArt
except ImportError:
    StdToolBarArt = False
