# -*- coding: utf-8 -*-

import mimetypes
import wx

from beatle.lib.decorators import class_property


def content_type_get_icon(mime_str):
    icon = None
    no_log = wx.LogNull()  # disable windows registry log errors
    file_type = wx.TheMimeTypesManager.GetFileTypeFromMimeType(mime_str)
    if file_type is not None:
        icon = file_type.GetIcon()
        if icon is None or not icon.IsOk():
            icon = None
    del file_type
    if icon is None:
        from beatle.app import resources as rc
        mime_to_resource_name= {
            'text/x-c++src': 'cppfile',
            'text/x-chdr': 'hfile',
            'text/plain': 'mfile',
            'text/x-python': 'pyfile',
        }
        if mime_str in mime_to_resource_name:
            icon = wx.Icon()
            icon.CopyFromBitmap(rc.get_bitmap(mime_to_resource_name[mime_str]))
    return icon


def file_type_get_mime(path):
    mime = None
    import os.path
    extension = os.path.splitext(path)[1]
    try:
        file_type = wx.TheMimeTypesManager.GetFileTypeFromExtension(extension)
    except AssertionError as e:
        file_type = None
    if file_type is not None:
        mime = file_type.GetMimeType()
        if not mime:
            if extension in ['.sh', '.bash', '.csh']:
                mime = 'application/x-shellscript'
        del file_type
    if mime is None:
        mime = mimetypes.guess_type(path)[0]
        if mime is None:
            if os.path.split(path)[1].lower() in ['makefile', 'readme', 'license']:
                mime = u'text/plain'
            elif extension in ['.sh', '.md', '.in']:
                mime = u'text/plain'
    # Fix : some mime types are specified as text/plain instead if text/<variant>
    if mime == 'text/plain':
        import os
        translate = {
            '.c': u'text/x-csrc',
            '.h':  u'text/x-chdr',
            '.cc': u'text/x-c++src',
            '.cpp': u'text/x-c++src',
            '.cxx': u'text/x-c++src',
            '.c++': u'text/x-c++src',
            '.hh':  u'text/x-c++hdr',
            '.hpp':  u'text/x-c++hdr',
            '.hxx':  u'text/x-c++hdr',
            '.h++':  u'text/x-c++hdr'
        }
        ext = os.path.splitext(path)[1].lower()
        mime = translate.get(ext, mime)
    if mime == 'application/x-extension-html':
        mime = 'text/html'
    elif mime == 'application/x-shellscript':
        mime = 'text/plain'
    return mime


def get_mime_types():
    """Query registered mime types.
    This method also enforces some required mime types"""
    return list(
       set(wx.TheMimeTypesManager.EnumAllFileTypes()).union(
           (
               u'text/x-csrc', u'text/x-chdr', u'text/x-c++src',
               u'text/x-c++hdr', u'application/x-executable',
               u'text/x-python'
           )))


class MimeHandler(object):
    """Class for handling mime types"""
    instance = None

    def __init__(self):
        """init"""
        self._extra_bitmap = {}
        self._mime = get_mime_types()
        self._mime_themed_icon = {}
        for x in self._mime:
            icon = content_type_get_icon(x)
            if icon is not None and icon.IsOk():
                self._mime_themed_icon[x] = icon
        self._themed_icon = []
        for themed_icon in self._mime_themed_icon.values():
            if themed_icon not in self._themed_icon:
                self._themed_icon.append(themed_icon)
        self._size = 24
        self._icons = None
        self._themed_icon_index = None
        self._image_list = None
        self.register_default_extra_bitmaps()
        MimeHandler.instance = self
        super(MimeHandler, self).__init__()

    @class_property
    def image_list(self):
        """returns the image list"""
        if not MimeHandler.instance:
            MimeHandler.instance = MimeHandler()
        if not MimeHandler.instance._image_list:
            MimeHandler.instance.realize()
        return MimeHandler.instance._image_list

    def realize(self):
        """ends the construction of the mime handler"""
        self._icons = self._load_icons()
        self._themed_icon_index = self._map_icon_index()
        self._image_list = self._create_image_list()

    def register_default_extra_bitmaps(self):
        """Extra bitmaps are those not corresponding to mime types, but
        that must be introduced in the image list"""
        from beatle.app import resources as rc
        xpm_list = list(rc._xpm.values())
        self._extra_bitmap.update(dict([(i, wx.Bitmap(xpm_list[i])) for i in range(0, len(xpm_list))]))

    def register_extra_bitmap(self, position, bitmap):
        """Some extra bitmaps may be added to the image list
        at specific positions"""
        self._extra_bitmap[position] = bitmap

    def _load_icons(self):
        """Reload icons for some size and returns a dict"""
        return dict([(x,x) for x in self._themed_icon])

    def _map_icon_index(self):
        """Create a map for the themed icon and image index"""
        index = 0
        themed_icon_index = {}
        for themed_icon in self._themed_icon:
            if self._icons[themed_icon]:
                # skip registered bitmaps that have fixed positions
                while index in self._extra_bitmap:
                    index = index + 1
                themed_icon_index[themed_icon] = index
                index = index + 1
            else:
                themed_icon_index[themed_icon] = wx.NOT_FOUND
        return themed_icon_index

    def _create_image_list(self):
        """Create an image list based on current icons"""
        index = 0
        icons = [x for x in self._icons.values() if x]
        iml = wx.ImageList(self._size, self._size, True, len(icons))
        for themed_icon in self._themed_icon:
            ico = self._icons[themed_icon]
            if ico is None:
                continue
            while index in self._extra_bitmap:
                iml.Add(self._extra_bitmap[index])
                index = index + 1
            if ico.GetWidth() != self._size or ico.GetHeight() != self._size:
                bmp = wx.Bitmap()
                bmp.CopyFromIcon(ico)
                img = bmp.ConvertToImage()
                sz = wx.Size(self._size, self._size)
                if ico.GetWidth() >= ico.GetHeight():
                    fact = float(self._size) / float(ico.GetWidth())
                    h = int(float(ico.GetHeight()) * fact)
                    img = img.Rescale(self._size, h, wx.IMAGE_QUALITY_HIGH)
                    pos = wx.Point(0, int((self._size - h) / 2))
                    img = img.Resize(sz, pos)
                else:
                    fact = float(self._size) / float(ico.GetHeight())
                    w = int(float(ico.GetWidth()) * fact)
                    img = img.Rescale(w, self._size, wx.IMAGE_QUALITY_HIGH)
                    pos = wx.Point(int((self._size - w) / 2), 0)
                    img = img.Resize(sz, pos)
                bmp = img.ConvertToBitmap()
                iml.Add(bmp)
            else:
                iml.Add(ico)
            index = index + 1
        return iml

    @classmethod
    def file_image_index(cls, path):
        """Returns the associated image index or wx.NOT_FOUND, based
        on mime type"""
        if not cls.instance:
            cls.instance = MimeHandler()
        self = cls.instance
        mime = file_type_get_mime(path)
        if mime not in self._mime:
            return wx.NOT_FOUND
        themed = self._mime_themed_icon[mime]
        return self._themed_icon_index[themed]
