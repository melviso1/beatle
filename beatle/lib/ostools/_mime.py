# -*- coding: utf-8 -*-
import wx

if "__WXGTK__" in wx.PlatformInfo:
    from .linux.mime import *
elif "__WXMSW__" in wx.PlatformInfo:
    from .win.mime import *

