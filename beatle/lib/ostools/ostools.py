# -*- coding: utf-8 -*-
import wx
import subprocess
import ast


def python_path():
    if "__WXMSW__" in wx.PlatformInfo:
        return "python3"
    else:
        return "/usr/bin/python"


def output_stack():
    import traceback
    import sys
    traceback.print_exc(file=sys.stdout)


def shell(command):
    """Execute a shell command and returns the value"""
    p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    if err:
        raise OSError('shell returns {0}'.format(err))
    return output.decode('ascii')


def import_dir(module, module_dir=None, project_dir=None):
    """get module info without loading"""
    if module_dir:
        try:
            s = shell('python3 -c "import os;os.chdir(\'{path}\');import {module} as key;print(dir(key))"'.format(
                path=module_dir, module=module))
            if s:
                return ast.literal_eval(s)
        except:
            pass
    if project_dir:
        try:
            s = shell('python3 -c "import os;os.chdir(\'{path}\');import {module} as key;print(dir(key))"'.format(
                path=project_dir, module=module))
            if s:
                return ast.literal_eval(s)
        except:
            pass
    try:
        s = shell('python3 -c "import {module} as key;print(dir(key))"'.format(module=module))
        return ast.literal_eval(s)
    except:
        pass
    return None
