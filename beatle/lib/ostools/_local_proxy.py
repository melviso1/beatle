"""local proxy starts a background proxy for handle requests done by beatle.
This utility was needed after detected some firewall issue that prevents beatle
effectively surfing the web. This implementation is not intended to be used
for other purposes."""

import wx
import subprocess
import os
from os.path import expanduser
import signal


class LocalProxy(object):

    def __init__(self, port=8901):
        self._command = '''        
from http.server import test,SimpleHTTPRequestHandler
test(SimpleHTTPRequestHandler,port={port},bind="127.0.0.1")'''.format(port=port)
        self._process = None
        if "__WXMSW__" in wx.PlatformInfo:
            self.runtime = "pythonw.exe"
            self.creation_flags = subprocess.CREATE_NO_WINDOW
        else:
            self.runtime = "python3"
            self.creation_flags = 0

    def start(self):
        if self._process is not None:
            raise RuntimeError("LocalProxy already running.")
        arg_list = [self.runtime, '-c', self._command]
        if "__WXMSW__" in wx.PlatformInfo:
            self._process = subprocess.Popen(
                arg_list,
                shell=False, cwd=expanduser('~'), creationflags=self.creation_flags)
        else:
            self._process = subprocess.Popen(
                arg_list,
                preexec_fn=os.setpgrp,
                shell=False, cwd=expanduser('~'), creationflags=self.creation_flags)

    def stop(self):
        if self._process is None:
            raise RuntimeError("LocalProxy not running.")
        if "__WXMSW__" in wx.PlatformInfo:
            subprocess.call(['taskkill', '/F', '/T', '/PID', str(self._process.pid)])
            self._process = None
        else:
            os.killpg(os.getpgid(self._process.pid), signal.SIGTERM)
        self._process = None
