'''
Created on 10 nov. 2018
The pourpose of this module is to provide a global file watcher for changes,
allowing to reload modified files automatically.

@author: mel
'''
import os, copy, traceback

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler, FileModifiedEvent


class fswatch(object):
    """Defines a global system watch"""

    instance = None
    schedules = {}  # dict dir_path -> (watch , count )
    handlers = {} # dict file_path -> { index -> (modified_callback, deleted_callback) }
    running = False

    def __init__(self):
        super(fswatch, self).__init__()
        if fswatch.instance is None:
            fswatch.instance = Observer()

    @classmethod
    def stop(cls):
        if fswatch.instance:
            fswatch.instance.unschedule_all()
            fswatch.instance.stop()
            fswatch.schedules = {}
            fswatch.handlers = {}
            fswatch.instance = None

    @classmethod
    def add_watch_for_file(cls, path, modified_callback=None, deleted_callback=None):
        """Adds a watch for a file. One of modified_callback or deleted_callback must
        be non None, or the call simply does nothing. Return watch index or 0 if not
        accepted"""
        if fswatch.instance is None:
            fswatch.instance = Observer()
        if modified_callback is None and deleted_callback is None:
            return 0
        dir_path = os.path.dirname(path)
        if dir_path not in cls.schedules:
            watch = cls.instance.schedule(FSWatchHnadler(), dir_path)
            count = 1
        else:
            watch, count = cls.schedules[dir_path]
            count = count + 1
        cls.schedules[dir_path] = (watch, count)
        if path not in cls.handlers:
            cls.handlers[path] = {1: (modified_callback, deleted_callback)}
        else:
            cls.handlers[path][count] = (modified_callback, deleted_callback)
        if not cls.running:
            cls.running = True
            cls.instance.start()
        return count

    @classmethod
    def remove_watch_for_file(cls, path, index):
        # add_handler_for_watch(self, event_handler, watch)
        if path not in cls.handlers:
            return False
        if index not in cls.handlers[path]:
            return False
        dir_path = os.path.dirname(path)
        if dir_path not in cls.schedules:
            return False
        watch, count = cls.schedules[dir_path]
        del cls.handlers[path][index]
        if len(cls.handlers[path]) == 0:
            del cls.handlers[path]
            if cls.instance is not None:
                cls.instance.unschedule(watch)
            del cls.schedules[dir_path]
        else:
            if count == index:
                cls.schedules[dir_path] = (watch, count-1)
        return True

    @classmethod
    def dispatch_modified(cls, path):
        if path not in cls.handlers:
            return
        this_handlers = copy.copy(cls.handlers[path])
        for index in this_handlers:
            modify_callback = this_handlers[index][0]
            if modify_callback is not None:
                try:
                    modify_callback()
                except:
                    # some issue with notify handler!
                    traceback.print_exc()
                    cls.remove_watch_for_file(path, index)

    @classmethod
    def dispatch_deleted(cls, path):
        if path not in cls.handlers:
            return
        this_handlers = copy.copy(cls.handlers[path])
        for index in this_handlers:
            delete_callback = this_handlers[index][1]
            if delete_callback is not None:
                try:
                    delete_callback()
                except:
                    # some issue with notify handler!
                    traceback.print_exc()
                    cls.remove_watch_for_file(path, index)


class FSWatchHnadler(FileSystemEventHandler):
    """Custom event handler for fswatch"""

    def __init__(self):
        """Initialize custom watcher"""
        super(FSWatchHnadler, self).__init__()

    def on_modified(self, event):
        if type(event) is FileModifiedEvent:
            fswatch.dispatch_modified(event.src_path)

    def on_deleted(self, event):
        if type(event) is FileModifiedEvent:
            fswatch.dispatch_delete(event.src_path)



