import os
import signal
import subprocess
import threading
import platform

import wx

from beatle.lib.api import context
from ..wxx import OutputEvent

if platform.system() == 'Windows':
    import psutil


class run(object):
    """Shell runner with log"""

    def __init__(self):
        """Init"""
        super(run, self).__init__()
        self._process = None
        self._paused = False
        self.done = False

    @property
    def is_running(self):
        return self._process is not None

    @property
    def is_paused(self):
        return self._paused

    @staticmethod
    def activate_ui():
        """Activate the panes"""
        context.send_clean_output_pane()
        context.send_activate_output_pane()

    @staticmethod
    def activate_terminal_ui():
        context.send_activate_terminal_pane()

    @staticmethod
    def deactivate_ui():
        """Deactivate the ui"""
        frame = context.get_frame()
        frame.m_mgr.Update()

    def exec_proc(self, **kwargs):
        """thread worker method"""
        frame = context.get_frame()
        if 'terminal' in kwargs and not "__WXMSW__" in wx.PlatformInfo:
            run.activate_terminal_ui()
            frame.post_terminal_command(kwargs['command'])
            return
        try:
            command = kwargs['command']
            proc_args = {
                'bufsize': 1,  # TO DO why buffering is not working on w64?
                'stdout': subprocess.PIPE,
                'stderr': subprocess.STDOUT,
                'shell': True,
                'start_new_session': True,
                'universal_newlines': False
            }
            cwd = kwargs.get('cwd', None).strip()
            if cwd:
                proc_args['cwd'] = cwd
            self._process = subprocess.Popen(command, **proc_args)
            run.activate_ui()

        except Exception as e:
            handler = frame.GetEventHandler()
            handler.AddPendingEvent(OutputEvent('execution failed: {}.\n'.format(str(e))))
            self.done = True
            return

        handler = frame.GetEventHandler()

        context.add_end_application_notify(self.on_run_exit_app)
        prolog = kwargs.get('prolog', None)

        if prolog is not None:
            handler.AddPendingEvent(OutputEvent('\n'+prolog+'\n'))

        from queue import Queue, Empty
        line_queue = Queue(10000)

        def enqueue_output():
            """Read process output and dispatch.
            We found some issues here, related to pipe's lock when
            attempting to stop execution. Following indications at
            https://stackoverflow.com/questions/10756383/timeout-on-subprocess-readline-in-python
            we use a poll"""
            read_loop = True
            while read_loop:
                read_loop = (self._process.poll() is None) and not self.done
                out_line = self._process.stdout.readline()
                while len(out_line) > 0:
                    if line_queue.qsize() > 600:
                        self.pause()
                    line_queue.put(out_line.decode('utf8', 'ignore'))
                    out_line = self._process.stdout.readline()

        read_output = threading.Thread(target=enqueue_output)
        read_output.start()
        dispatch_loop = True
        while dispatch_loop:
            more_data = True
            while more_data:
                try:
                    line = line_queue.get(timeout=.1)  # or q.get_nowait()
                    line_queue.task_done()
                    if line_queue.qsize() < 400:
                        self.resume()
                    if len(line):
                        handler.AddPendingEvent(OutputEvent(line))
                except Empty:
                    dispatch_loop = read_output.is_alive()
                    more_data = False
                    wx.MilliSleep(20)

        self.done = True
        # self._process.stdout.close()
        epilog = kwargs.get('epilog', None)
        return_code = self._process.wait()
        handler.AddPendingEvent(OutputEvent('\nreturn status:{}\n'.format(return_code)))
        if epilog is not None:
            handler.AddPendingEvent(OutputEvent('\n'+epilog+'\n'))
        context.cancel_end_application_notify(self.on_run_exit_app)
        self._process = None

    def pause(self):
        if self._process and not self.is_paused:
            if platform.system() == 'Windows':
                psutil.Process(self._process.pid).suspend()
            else:
                self._process.send_signal(signal.SIGSTOP)
            self._paused = True

    def resume(self):
        if self._process and self.is_paused:
            if platform.system() == 'Windows':
                psutil.Process(self._process.pid).resume()
            else:
                self._process.send_signal(signal.SIGCONT)
            self._paused = False

    def stop(self):
        process = self._process
        if getattr(process, '_maybe_zombi', False):
            # The user has unsuccessful attempted that before, so
            # we must relay on hard-close
            return False
        if process:
            try:
                process.send_signal(signal.SIGTERM)
            except:
                pass
            wx.MilliSleep(10)
            process = self._process
            if process is None:
                return True
            try:
                if process.poll() is None:
                    return False
                setattr(self._process, '_maybe_zombi', True)
            except:
                pass
        self.done = True
        return True

    def force_stop(self):
        """Brute stop"""
        process = self._process
        if process:
            try:
                # process.kill()
                os.killpg(os.getpgid(self._process.pid), signal.SIGKILL)
            except Exception as e:
                frame = context.get_frame()
                handler = frame.GetEventHandler()
                handler.AddPendingEvent(OutputEvent('kill process failed: {}.\n'.format(str(e))))
                pass
            wx.MilliSleep(10)
            process = self._process
            if process is None:
                return True
            try:
                if process.poll() is None:
                    return False
            except:
                pass
        self.done = True
        return True

    def __run__(self, **kwargs):
        """Start a clean operation"""
        if self._process:
            return  # is already running
        self._paused = False
        self.done = False
        threading.Thread(target=self.exec_proc, kwargs=kwargs).start()

    def on_run_exit_app(self):
        frame = context.get_frame()
        res = wx.MessageBox(
            message='Exit Beatle will stop running background processes.\nIts Ok?',
            caption='Question',
            style=wx.YES_NO | wx.ICON_INFORMATION,
            parent=frame)
        if res == wx.YES:
            if not self.stop():
                self.force_stop()
            return True
        else:
            return False
