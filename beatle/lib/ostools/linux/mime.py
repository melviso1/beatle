# -*- coding: utf-8 -*-

import mimetypes
import wx

from beatle.lib.decorators import class_property
import gi

if 'gtk3' in wx.PlatformInfo:
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk
else:
    gi.require_version("Gtk", "2.0")
    try:
        from gi.repository import Gtk
    except:
        pass
from gi.repository import Gio


def content_type_get_icon(mime_str):
    icon = None
    try:
        icon = Gio.content_type_get_icon(mime_str)
    except:
        pass
    if icon is None:
        fileType = wx.TheMimeTypesManager.GetFileTypeFromMimeType(mime_str)
        if fileType is not None:
            icon = fileType.GetIcon()
        del fileType
    return icon


def file_type_get_mime(path):
    mime = None
    f = Gio.File.new_for_path(path)
    info = f.query_info('standard::content-type', 0, None)
    mime = info.get_content_type()
    if mime is None:
        import os.path
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(os.path.splitext(path)[1])
        if fileType is not None:
            mime = fileType.GetMimeType()
            del fileType
    if mime is None:
        mime = mimetypes.guess_type(path)[0]
    return mime


class MimeHandler(object):
    """Class for handling mime types"""
    instance = None

    def __init__(self):
        """init"""
        self._extra_bitmap = {}
        self._handler = wx.TheMimeTypesManager
        self._mime = self._handler.EnumAllFileTypes()
        if 'application/x-executable' not in self._mime:
            self._mime.append('application/x-executable')
        self._mime_themed_icon = {}
        for x in self._mime:
            icon = content_type_get_icon(x)
            if icon is not None:
                self._mime_themed_icon[x] = icon
        self._themed_icon = []
        for themed_icon in self._mime_themed_icon.values():
            if themed_icon not in self._themed_icon:
                self._themed_icon.append(themed_icon)
        self._theme = Gtk.IconTheme.get_default()
        self._size = 24
        self._icons = None
        self._themed_icon_index = None
        self._image_list = None
        self.register_default_extra_bitmaps()
        MimeHandler.instance = self
        super(MimeHandler, self).__init__()

    @class_property
    def image_list(self):
        """returns the image list"""
        if not MimeHandler.instance:
            MimeHandler.instance = MimeHandler()
        if not MimeHandler.instance._image_list:
            MimeHandler.instance.realize()
        return MimeHandler.instance._image_list

    def realize(self):
        """ends the construction of the mime handler"""
        self._icons = self._load_icons()
        self._themed_icon_index = self._map_icon_index()
        self._image_list = self._create_image_list()

    def register_default_extra_bitmaps(self):
        """Adds some extra bitmaps that will be included
        in the image list"""
        from beatle.app import resources as rc
        xpm_list = list(rc._xpm.values())
        self._extra_bitmap.update(dict([(i, wx.Bitmap(xpm_list[i])) for i in range(0, len(xpm_list))]))

    def register_extra_bitmap(self, position, bitmap):
        """Some extra bitmaps may be added to the image list
        at specific positions"""
        self._extra_bitmap[position] = bitmap

    def _load_icons(self):
        """Reload icons for some size and returns a dict"""
        icons = {}
        size = self._size
        for themed_icon in self._themed_icon:
            info = self._theme.choose_icon(themed_icon.get_names(), size, 0)
            if info:
                icon = wx.Icon(info.get_filename(), desiredWidth=size)
                if icon.IsOk():  # Theme can specify invalid icons!!
                    icons[themed_icon] = wx.Icon(info.get_filename(), desiredWidth=size)
                    continue
            icons[themed_icon] = None
        return icons

    def _map_icon_index(self):
        """Create a map for the themed icon and image index"""
        index = 0
        themed_icon_index = {}
        for themed_icon in self._themed_icon:
            if self._icons[themed_icon]:
                # skip registered bitmaps that have fixed positions
                while index in self._extra_bitmap:
                    index = index + 1
                themed_icon_index[themed_icon] = index
                index = index + 1
            else:
                themed_icon_index[themed_icon] = wx.NOT_FOUND
        return themed_icon_index

    def _create_image_list(self):
        """Create an image list based on current icons"""
        index = 0
        icons = [x for x in self._icons.values() if x]
        iml = wx.ImageList(self._size, self._size, True, len(icons))
        for themed_icon in self._themed_icon:
            ico = self._icons[themed_icon]
            if ico is None:
                continue
            while index in self._extra_bitmap:
                iml.Add(self._extra_bitmap[index])
                index = index + 1
            if ico.GetWidth() != self._size or ico.GetHeight() != self._size:
                bmp = wx.Bitmap()
                bmp.CopyFromIcon(ico)
                img = bmp.ConvertToImage()
                sz = wx.Size(self._size, self._size)
                if ico.GetWidth() >= ico.GetHeight():
                    fact = float(self._size) / float(ico.GetWidth())
                    h = int(float(ico.GetHeight()) * fact)
                    img = img.Rescale(self._size, h, wx.IMAGE_QUALITY_HIGH)
                    pos = wx.Point(0, int((self._size - h) / 2))
                    img = img.Resize(sz, pos)
                else:
                    fact = float(self._size) / float(ico.GetHeight())
                    w = int(float(ico.GetWidth()) * fact)
                    img = img.Rescale(w, self._size, wx.IMAGE_QUALITY_HIGH)
                    pos = wx.Point(int((self._size - w) / 2), 0)
                    img = img.Resize(sz, pos)
                bmp = img.ConvertToBitmap()
                iml.Add(bmp)
            else:
                iml.Add(ico)
            index = index + 1
        return iml

    @classmethod
    def file_image_index(cls, path):
        """Returns the associated image index or wx.NOT_FOUND, based
        on mime type"""
        if not cls.instance:
            cls.instance = MimeHandler()
        self = cls.instance
        mime = file_type_get_mime(path)
        # ok, we can go from mime to themed icon and from that to icon index
        # Fix : some zero-lenght files fails in mime check
        if mime == 'text/plain':
            import os
            ext = os.path.splitext(path)[1]
            if ext in ['.c', '.C']:
                mime = u'text/x-csrc'
            elif ext in ['.h', '.H']:
                mime = u'text/x-chdr'
            elif ext in ['.cc', '.cpp', '.cxx', '.c++', '.CC', '.CPP', '.CXX', '.C++']:
                mime = u'text/x-c++src'
            elif ext in ['.hh', '.hpp', '.hxx', '.h++', '.HH', '.HPP', '.HXX', '.H++']:
                mime = u'text/x-c++hdr'
        if mime == 'application/x-extension-html':
            mime = 'text/html'
        if mime not in self._mime:
            return wx.NOT_FOUND
        themed = self._mime_themed_icon[mime]
        return self._themed_icon_index[themed]
