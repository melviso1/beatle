# -*- coding: utf-8 -*-


from ._context import LocalContext, THE_CONTEXT


def get_info(topic):
    return THE_CONTEXT.get_info(topic)


def add_application_focus_notify(callback):
    THE_CONTEXT.add_application_focus_notify(callback)


def app_get_focus():
    THE_CONTEXT.app_get_focus()


def app_lose_focus():
    THE_CONTEXT.app_lose_focus()


def set_info(topic, value):
    THE_CONTEXT.set_info(topic, value)


def add_end_application_notify(notify_callback):
    THE_CONTEXT.add_end_application_notify(notify_callback)


def cancel_end_application_notify(notify_callback):
    THE_CONTEXT.cancel_end_application_notify(notify_callback)


def can_close_app():
    return THE_CONTEXT.can_close_app


def get_app():
    """return the app"""
    return THE_CONTEXT.app


def set_app(app):
    """set the app"""
    THE_CONTEXT.app = app


def load():
    THE_CONTEXT.load()


def save():
    """do app status save"""
    THE_CONTEXT.save()


def get_config():
    return THE_CONTEXT.config


def set_config(cfg):
    """Set config"""
    THE_CONTEXT.config = cfg


def get_mru():
    """Get the mru"""
    return THE_CONTEXT.mru


def get_frame():
    """accessor for frame"""
    return THE_CONTEXT.frame


def destroy():
    """Destroy context"""
    THE_CONTEXT.destroy()


def send_activate_output_pane():
    THE_CONTEXT.send_activate_output_pane()


def send_activate_terminal_pane():
    THE_CONTEXT.send_activate_terminal_pane()


def send_clean_output_pane():
    THE_CONTEXT.send_clean_output_pane()


def load():
    """Starts the config (may be started after set app)"""
    THE_CONTEXT.load()


def save():
    """Save context"""
    THE_CONTEXT.save()


def set_frame(frame):
    THE_CONTEXT.set_frame(frame)


def get_current_view():
    return THE_CONTEXT.current_view


def save_project(project):
    """Save all changes"""
    THE_CONTEXT.save_project(project)


def render_undo_redo_add(obj):
    THE_CONTEXT.render_undo_redo_add(obj)


def render_undo_redo_loaded(obj):
    THE_CONTEXT.render_undo_redo_loaded(obj)


def render_undo_redo_removing(obj):
    """render undo/redo"""
    THE_CONTEXT.render_undo_redo_removing(obj)


def render_undo_redo_changed(obj):
    """render undo/redo"""
    THE_CONTEXT.render_undo_redo_changed(obj)


def render_undo_redo_unloaded(obj):
    """render unloaded removing"""
    THE_CONTEXT.render_undo_redo_unloaded(obj)


def get_context():
    return THE_CONTEXT
