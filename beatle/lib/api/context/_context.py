# -*- coding: utf-8 -*-
import wx

from beatle.lib import wxx
from beatle.lib.decorators import class_property
from beatle.lib.handlers import identifier


class Context(object):
    """Context is a required class for dealing between datamodel
    and rendering"""
    _app = None
    _config = None
    _dict = {}  # dictionary for Context info
    _end_notify_callbacks = []
    _app_focus_callbacks = []

    def __init__(self):
        """init"""
        pass

    @classmethod
    def get_info(cls, topic):
        return cls._dict.get(topic, False)

    @classmethod
    def set_info(cls, topic, value):
        cls._dict[topic] = value

    @classmethod
    def add_end_application_notify(cls, notify_callback):
        if notify_callback not in cls._end_notify_callbacks:
            cls._end_notify_callbacks.append(notify_callback)

    @classmethod
    def cancel_end_application_notify(cls, notify_callback):
        while notify_callback in cls._end_notify_callbacks:
            cls._end_notify_callbacks.remove(notify_callback)

    @classmethod
    def add_application_focus_notify(cls, notify_callback):
        if notify_callback not in cls._app_focus_callbacks:
            cls._app_focus_callbacks.append(notify_callback)

    @classmethod
    def cancel_application_focus_notify(cls, notify_callback):
        while notify_callback in cls._app_focus_callbacks:
            cls._app_focus_callbacks.remove(notify_callback)

    @class_property
    def can_close_app(cls):
        if not cls._app.exit_even_modified():
            return False
        for notify_callback in cls._end_notify_callbacks:
            if notify_callback() is False:
                return False
        _end_notify_callbacks = []
        return True

    @classmethod
    def notify_close_app(cls):
        for notify_callback in cls._end_notify_callbacks:
            notify_callback()

    @classmethod
    def app_get_focus(cls):
        for notify_callback in cls._app_focus_callbacks:
            notify_callback(True)

    @classmethod
    def app_lose_focus(cls):
        for notify_callback in cls._app_focus_callbacks:
            notify_callback(False)

    @class_property
    def app(cls):
        """return the app"""
        return cls._app

    @app.setter
    def app(cls, app):
        """set the app"""
        cls._app = app
        cls.load()

    @classmethod
    def load(cls):
        """do first app initialization"""
        pass

    @classmethod
    def save(cls):
        """do app status save"""
        pass

    @class_property
    def config(cls):
        """Get config"""
        return cls._config

    @config.setter
    def config(cls, config):
        """Set config"""
        cls._config = config

    @class_property
    def mru(cls):
        """Get the mru"""
        return cls._mru

    @classmethod
    def render_undo_redo_add(cls, obj):
        """render undo/redo"""
        raise RuntimeError("not implemented")

    @classmethod
    def render_undo_redo_loaded(cls, obj):
        """render loaded add"""
        raise RuntimeError("not implemented")

    @classmethod
    def render_undo_redo_unloaded(cls, obj):
        """render loaded add"""
        raise RuntimeError("not implemented")

    @classmethod
    def render_undo_redo_removing(cls, obj):
        """render undo/redo"""
        raise RuntimeError("not implemented")

    @classmethod
    def render_undo_redo_changed(cls, obj):
        """render undo/redo"""
        raise RuntimeError("not implemented")

    @classmethod
    def destroy(cls):
        """Destroy context"""
        cls.save()
        cls.notify_close_app()
        cls.frame.Destroy()


class LocalContext(Context):
    """Context for doing local works"""
    _frame = None

    _activate_output_pane = identifier('id_activate_output_pane')
    _activate_terminal_pane = identifier('id_activate_terminal_pane')
    _clean_output_pane = identifier('id_clean_output_pane')

    @class_property
    def frame(self):
        """accessor for frame"""
        return self._frame

    def __init__(self):
        """init"""
        super(LocalContext, self).__init__()

    @classmethod
    def send_activate_output_pane(_):  #wx.PyCommandEvent(wx.EVT_BUTTON.typeId, self.GetId())
        wx.PostEvent(_._frame, wx.CommandEvent(wx.EVT_MENU.typeId, _._activate_output_pane))

    @classmethod
    def send_activate_terminal_pane(_):
        wx.PostEvent(_._frame, wx.CommandEvent(wx.EVT_MENU.typeId, _._activate_terminal_pane))

    @classmethod
    def send_clean_output_pane(_):
        wx.PostEvent(_._frame, wx.CommandEvent(wx.EVT_MENU.typeId, _._clean_output_pane))

    @classmethod
    def load(cls):
        """Starts the config (may be started after set app)"""
        config = wx.Config('beatle3.local', vendorName='melvm',
                           style=wx.CONFIG_USE_LOCAL_FILE)
        config.SetPath('~/.config/')
        cls.config = config
        #
        cls._mru = wxx.FileHistory()
        cls._mru.Load(cls.config)
        cls.frame.load_views()
        # holding shift will prevent reload workspaces
        if not wx.GetKeyState(wx.WXK_SHIFT):
            cls.app.load_session()
        cls.frame.load_mru()
        cls.frame.build_help_menu()

    @classmethod
    def save(cls):
        """Save context"""
        mru = LocalContext.mru
        mru.Save(cls.config)
        cls.frame.save_views()
        cls.app.save_session()
        cls.config.Flush()

    @classmethod
    def set_frame(cls, frame):
        """Set frame window"""
        cls._frame = frame

    @class_property
    def current_view(cls):
        index = LocalContext.frame.viewBook.GetSelection()
        return (index is not None and LocalContext.frame.viewBook.GetPage(index)) or None

    @classmethod
    def save_project(cls, proj):
        """Save all changes"""
        book = LocalContext.frame.docBook
        for i in range(0, book.GetPageCount()):
            pane = book.GetPage(i)
            if hasattr(pane, '_object'):
                if pane._object.project == proj:
                    pane.commit()
        cls.app.save_project(proj)

    @classmethod
    def render_undo_redo_add(cls, obj):
        """render undo/redo"""
        # update old models
        if not hasattr(obj, '_visibleInTree'):
            obj.visible_in_tree = True
        # end old models
        if obj.visible_in_tree:
            cls.frame.do_render_add_element(obj)

    @classmethod
    def render_undo_redo_loaded(cls, obj):
        """render loaded add"""
        # update old models
        if not hasattr(obj, '_visibleInTree'):
            obj.visible_in_tree = True
        # end old models
        if obj.visible_in_tree:
            cls.frame.do_render_add_element(obj)

    @classmethod
    def render_undo_redo_removing(cls, obj):
        """render undo/redo"""
        if obj.visible_in_tree:
            cls.frame.do_render_remove_element(obj)

    @classmethod
    def render_undo_redo_changed(cls, obj):
        """render undo/redo"""
        cls.frame.update_element(obj)

    @classmethod
    def render_undo_redo_unloaded(cls, obj):
        """render unloaded removing"""
        if obj.visible_in_tree:
            cls.frame.do_render_remove_element(obj)


if 'THE_CONTEXT' not in globals():
    THE_CONTEXT = LocalContext()


