import wx
from beatle.lib.api import context
from beatle.lib import wxx


def tools_menu_item(identifier, text, helptext, kind=wx.ITEM_NORMAL):
    frame = context.get_frame()
    return wxx.MenuItem(frame.menuTools, identifier, text, helptext, kind)


def add_tools_menu(label, item):
    """Add a menu to main frame"""
    frame = context.get_frame()
    frame.add_tools_menu(label, item)


def current_view():
    """Get the current view"""
    frame = context.get_frame()
    return frame.get_current_view()
