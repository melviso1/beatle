# -*- coding: utf-8 -*-


class volatile(object):
    """Stores volatile info used while running"""
    _store = {}
    
    def __init__(self):
        super(volatile, self).__init__()
        
    @classmethod
    def set(cls, key, value):
        cls._store[key] = value
        
    @classmethod
    def get(cls, key, default=None):
        if key not in cls._store:
            return default
        return cls._store[key]

    
