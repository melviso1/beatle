#!/usr/bin/python3

import sys

with open(sys.argv[2],'rt') as file:
	s = file.readlines()
	# read first label
	for l in s:
		if l[:2] == '/*':
			continue
		if l[-1] == '\n':
			l=l[:-1]
		if l[-1] == '\r':
			l=l[:-1]
		if len(l) == 0:
			continue
		if l[:6] == 'static':
			print('{}=['.format(sys.argv[1]))
		else:
			if l[-1] == ';':
				l= l[:-1]
			if l[-1] == '}':
				l = l[:-1]
			print('b{}'.format(l))
	print(']')

		
	
