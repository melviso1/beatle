# -*- coding: utf-8 -*-

import wx
import wx.stc as stc


"""Defines basic interface for app/editor"""


class EditorHandlerBase(object):
    """Interface for editor handlers"""

    cookie_lexers = {
        'text': stc.STC_LEX_NULL,
        'c++': stc.STC_LEX_CPP,
        'python': stc.STC_LEX_PYTHON,
        'sql': stc.STC_LEX_SQL,
        'bash': stc.STC_LEX_BASH,
        'css': stc.STC_LEX_CSS,
        'html': stc.STC_LEX_HTML,
        'mathlab': stc.STC_LEX_MATLAB,
        'octave': stc.STC_LEX_OCTAVE,
        'xml': stc.STC_LEX_XML,
        'apache': stc.STC_LEX_CONF,
        'make': stc.STC_LEX_MAKEFILE,
        'nasm': stc.STC_LEX_ASM,
        'vhdl': stc.STC_LEX_VHDL
    }

    keyword_list = []
    keyword_list2 = []
    style_items = []
    style = {}

    def __init__(self, **kwargs):
        """Initialize handler"""
        self.lexer = kwargs.get('lexer', 'text')
        self.font = kwargs.get('font', 'config/defaultFont')
        self.text = kwargs.get('text', '')
        if 'keywords' in kwargs:
            self.keyword_list = kwargs['keywords']
        if 'keywords2' in kwargs:
            self.keyword_list2 = kwargs['keywords2']
        self.read_only = kwargs.get('read_only', False)
        super(EditorHandlerBase, self).__init__()

    @property
    def lexer(self):
        """get property"""
        return self._lexer

    @lexer.setter
    def lexer(self, value):
        """set property"""
        if value in self.cookie_lexers:
            self._lexer = self.cookie_lexers[value]
        else:
            self._lexer = value

    @property
    def font(self):
        """return the font"""
        return self._font

    @font.setter
    def font(self, font_section):
        """set the font by config section. fallback to default"""
        from beatle.lib.api import context
        config = context.get_config()
        self._font = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)
        default = self._font.GetNativeFontInfo().ToString()
        font_info = config.Read(font_section, default)
        self._font.SetNativeFontInfo(font_info)

    def update_font(self, font_info, editor):
        self._font.SetNativeFontInfo(font_info)
        self.init_fonts(editor)

    @property
    def keywords(self):
        """return keywords"""
        return ' '.join(self.keyword_list)

    @property
    def keywords2(self):
        """return keywords"""
        return ' '.join(self.keyword_list2)

    def setup_margins(self, editor):
        """Setup margins"""
        pass

    def setup_markers(self, editor):
        """Setup markers"""
        pass

    def set_properties(self, editor):
        """Set other properties"""
        pass

    def setup_events(self, editor):
        """Set event handling"""
        pass

    def init_fonts(self, editor):
        """default font initialization"""
        font = self.font
        for i in range(0, 39):
            editor.StyleSetFont(i, font)
        # increase space between lines using STC_STYLE_INDENTGUIDE
        size = int(font.GetPointSize()*1.05)
        editor.StyleSetFont(stc.STC_STYLE_INDENTGUIDE, wx.Font(wx.FontInfo(size)))

    def init_styles(self, editor):
        """Initialize the styles"""
        for item in self.style_items:
            (identifier, name) = item
            if name in self.style:
                editor.StyleSetSpec(identifier, self.style[name])
        editor.SetFoldMarginColour(True, "#FFFFFF")
        editor.SetFoldMarginHiColour(True, "#FFFFFF")

    def init_keywords(self, editor):
        """Initialize keywords"""
        editor.SetKeyWords(0, self.keywords)
        editor.SetKeyWords(1, self.keywords2)

    def setup_autocomplete(self, editor):
        """Setup autosuggested"""
        s = list(set(self.keyword_list + self.keyword_list2))  # prevent duplicates
        s.sort()
        editor._auto = ' '.join(s)

    def initialize(self, editor):
        """Initialize editor"""
        # if self.keywords:
        #    editor.SetKeyWords(0, self.keywords)
        self.setup_margins(editor)
        self.setup_markers(editor)
        self.set_properties(editor)
        self.init_styles(editor)
        self.init_keywords(editor)
        self.setup_autocomplete(editor)
        editor.ChangeValue(self.text)
        self.setup_events(editor)
        editor.EmptyUndoBuffer()
        editor.SetSavePoint()
        editor.SetUndoCollection(True)
        editor.SetReadOnly(self.read_only)

