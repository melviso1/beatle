'''
Created on 8 sept. 2018

AcceleratorHandler provides a way to handle shared keyboard shotcuts whose
meaning depends on context.

An AcceleratorHandler holds a Cooperation object identified by the tupla (flags, key_code).

@author: mel
'''

import wx
from ._CooperativeHandler import  CooperativeHandler, CooperativeResource
from ._IdentifiersHandler import Identifiers


class SharedAcceleratorHandler(object):

    _handler_counter = 0

    def __init__(self, handler, name):
        """Inicializacion."""
        self._handler = handler
        self._name = "shared_accelerator_handler_{name}".format(name=name)
        if CooperativeHandler.exists(self._name):
            raise RuntimeError('Found duplicated accelerator handler {self._name}'.format(self=self))
        self._cooperation = CooperativeHandler.register(self._name)
        self._cooperation._manager = self
        self._dispatcher={}

    @classmethod
    def add_accelerator(cls, handler, flags=0, key_code=0, cmdID=0):
        name = "shared_accelerator_handler_{name}".format(name=handler)
        if not CooperativeHandler.exists(name):
            raise RuntimeError('Accelerator handler {name} not found'.format(name=name))
        self = CooperativeHandler.register(name)._manager
        self.__add_accelerator__(flags, key_code, cmdID)

    def __add_accelerator__(self, flags, key_code, cmdID):
        kwargs = {'flags': flags,'key_code': key_code,'cmdID': cmdID}
        shortcut = '{flags} {key_code}'.format(**kwargs)
        if not self._cooperation.exists(shortcut):
            event_name = '{handler} {shortcut}'.format(handler=self._cooperation.name, shortcut=shortcut)
            event_id = Identifiers.register(event_name)
            self._dispatcher[(flags, key_code, event_id)] = AcceleratorDispatcher(self, shortcut)
        self._cooperation.add(CooperativeResource(shortcut, **kwargs))

    def get_accelerator_entries(self):
        """Return the list of accelerator entries suitable for add to event handler"""
        return [wx.AcceleratorEntry(*x) for x in self._dispatcher.keys()]

    def bind_events(self):
        for x in self._dispatcher.keys():
            self._handler.Bind(wx.EVT_MENU,
                self._dispatcher[x].on_accelerator_dispatcher, id=x[2])

    def dispatch(self, name, event):
        for resource in self._cooperation.resources(name):
            s_event = event.Clone()
            s_event.SetId(resource.data['cmdID'])
            wx.PostEvent(self._handler, s_event)



class AcceleratorDispatcher(object):

    """Handles a shared keyboard accelerator"""
    def __init__(self,handler,name):
        self._handler = handler
        self._name = name
        super(AcceleratorDispatcher, self).__init__()

    def on_accelerator_dispatcher(self, event):
        self._handler.dispatch(self._name, event)




