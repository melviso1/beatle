# -*- coding: utf-8 -*-
"""Defines handler for identifiers"""

import wx



class Identifiers(object):
    """Application identifiers handler"""
    _identifiers = {}
    _values = {}

    def __init__(self):
        """Ensures this is a singleton"""
        raise RuntimeError('IdentifiersHandler is a singleton')
        super(Identifiers, self).__init__()
        
    @classmethod
    def release_all(cls):
        del cls._identifiers
        del cls._values

    @classmethod
    def new(cls, name=None, value=None):
        """Create a new identifier
        notes
        -----
        An id created without name is a disposable identifier. 
        Identifiers created with wx.Window.NewId are created with 
        internal ref count = 1, but, after first use with the 
        wx window system, these ids will be destroyed. 
        If we attempt to use them again, wx asserts, because 
        he tracks 'dead' identifiers.
        
        Even the problem, that's good, because using "uncontrolled id's"
        is the direct way to hang.
        
        wx.NewIdRef, in other hand, keep his own ref counter, 
        allowing us to effectively reserve and use the id's.
        
        """
        if name:
            if name in cls._identifiers:
                raise RuntimeError('Identifier {name} already exists'.format(name=name))
            if value is None:                 
                ref = wx.NewIdRef()
                value = ref.GetValue()
                while value in cls._identifiers.values():
                    ref = wx.NewIdRef()
                    value = ref.GetValue()
                cls._identifiers[name]=ref
            else:
                print("warning: registering {} with fixed value may downgrade identifiers system".format(name))
                assert value not in cls._values.values()
            cls._values[name] = value
            return value
        else:
            return wx.Window.NewControlId() #discardable

    @classmethod
    def register(cls, name, value=None):
        """Register a identifier"""
        if name in cls._identifiers:
            local_value = cls._identifiers[name].GetId()
        else:
            local_value = cls._values.get(name, None)
        if local_value is None:
            local_value = cls.new(name, value)
        elif value is not None:
            raise ValueError('Attempting to register identifier {name} already registered with different value'.format(
                name=name))
        return local_value


def identifier(name):
    return Identifiers.register(name)

def new_identifier():
    return Identifiers.new()
        
