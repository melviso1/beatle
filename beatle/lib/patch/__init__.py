# monkey-patch for python3 < 3.7
import uuid
import wx

if not hasattr(uuid, 'SafeUUID'):
    from enum import Enum


    class SafeUUID(Enum):
        safe = 0
        unsafe = 1
        unknown = None


    uuid.SafeUUID = SafeUUID

wx.inmutableImageList = wx.ImageList
if "__WXMSW__" in wx.PlatformInfo:
    from beatle.lib.wxx._ImageList import ImageList
    wx.ImageList = ImageList

    import os
    os.killpg = os.kill  # windows has no process group
    os.getpgid = os.getpid

    import ctypes
    # Code taken from:
    # https://stackoverflow.com/questions/44398075/can-dpi-scaling-be-enabled-disabled-programmatically-on-a-per-session-basis
    # Query DPI Awareness (Windows 10 and 8)
    awareness = ctypes.c_int()
    errorCode = ctypes.windll.shcore.GetProcessDpiAwareness(0, ctypes.byref(awareness))
    # Set DPI Awareness  (Windows 10 and 8)
    errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)
    # the argument is the awareness level, which can be 0, 1 or 2:
    # for 1-to-1 pixel control I seem to need it to be non-zero (I'm using level 2)

    # Set DPI Awareness  (Windows 7 and Vista)
    success = ctypes.windll.user32.SetProcessDPIAware()

