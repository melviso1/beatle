
"""
    experimental : still unused

    decorator for delegated classes
    ===============================
    
    Beatle uses classes as keys for many things. Specifically when using models,
    some key classes, like Project or Folder must be reused without collisions
    or duplicated functionality.
    
    Creating FolderPython, FolderCpp, etc ... messes up the code and requires
    not reusing common behaviours.
    
    delegated decorator provides a fancy way to do that.
    
    @delegated 
    class Interface:
        ....
        
    clas Worker:
        ...
        
    my_hidden_worker = Interface(delegate=Worker())
    
    You can also embedd as many delegators as you want into any delegated class.
    
    my_hidden_collaborative_workers = Interface(delegates=[Worker(),Beeper()])
    
    The resolution order corresponds to declarative order list.
    
    The delegated decorator doesn't obligate to specify the delegate class(es),
    the decorated class is still usable alone.
    
    alone = Interface()
    
    
"""

def delegated(base):
    __init__ = base.__init__

    def get_attr_method(self, name):
        for obj in self.__dict__['__delegates__']:
            if hasattr(obj, name):
                return getattr(obj, name)
        raise AttributeError('{self.__class__.__name__}.{name} is invalid.'.format(self=self, name=name))

    def set_attr_method(self, name, value):
        for obj in self.__delegates__:
            if hasattr(obj, name):
                setattr(obj, name, value)
                return
        self.__dict__[name] = value

    def __wrapped_init__(self, *args, **kwargs):
        if 'delegate' in kwargs:
            __delegates__ = [kwargs['delegate'],]
            del kwargs['delegate']
        elif 'delegates' in kwargs:
            __delegates__ = [kwargs['delegates'],]
            del kwargs['delegates']
        else:
            __delegates__ = []

        # do real init
        __init__(self, *args, **kwargs)
        self.__delegates__ = __delegates__
        if len(__delegates__) > 0:
            for obj in __delegates__:
                obj.__interface__ = self
                
    base.__delegates__=[]
    base.__init__ = __wrapped_init__
    base.__setattr__ = set_attr_method
    base.__getattr__ = get_attr_method

    return base

