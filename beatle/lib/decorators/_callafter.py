"""
callafter decorator

This decorator extends the lifecycle of wxWidgets CallAfter mechanism.
Due to complex UI interactions CallAfter could be not enough to provide correct UI interface update mechanism.
One example is reordering items on Models tree. The transactional control could trigger extra events that could
yield on losing the tree focus (that could be further analyzed).
The quick solution is to do a controlled nested CallAfter by using this decorator.

Example:
    @callafter(2)
    def update_view():
        # do something

    update_view now equals to wx.CallAfter(wx.CallAfter(update_view))

so the argument is the nested call iteration.
"""

import wx


class callafter(object):
    """Class decorator for transactional methods"""
    def __init__(self, count=1):
        """Function decorator for event-loop delayed calls."""
        self._count = count

    def __call__(self, method):
        """"""
        def wrapped_call(*args, **kwargs):
            """Code for call method"""
            count = getattr(method, 'count', 0)
            setattr(method, 'count', (self._count+count-1) % self._count)
            if count == 0:
                method(*args, **kwargs)
            else:
                wx.CallAfter(wrapped_call, *args, **kwargs)

        setattr(method, 'count', self._count)
        return wrapped_call
