"""

    decorator for __setstate__ version manteinance
    ==============================================

    When loading old Beatle projects, some classes had changes that requires
    the presence of new attributes (or rename of others). In order to
    quick handle these changes, the upgrade_version decorator specify that
    such a __setstate__ method decorated must return an dictionary with
    the following dictionnary:

    {
        'rename':{'old_name_1':'new_name_1', 'old_name_2':'new_name_2',...},
        'delete':['name_1','name_2',...]
        'add':{'name_1':'value_1','name_2':'value_2',...}
    }

    The entries in the 'rename' value subdictionnary makes that if an 'old_name_x' is
    present in the __setstate__ parameter dictionnary, that entry being renamed
    to 'new_name_x'.
    The entries in the 'delete' value list makes that if an entry match some key
    of the __setstate__ paramater dictionnary, such a key gets removed.
    The entries in the 'add' value dictionnary makes that any non-existent key
    'name_x' in the __setstate__ parameter dictionnary gets added and initializated
    to 'value_x'
    All the 'rename', 'delete' or  'add' entries are optional.

    sample usage:

    @upgrade_version
    def __setstate__(self, data_dict):
        return {
            'rename':{'badPep8Name','good_pep8_name'},
            'delete':['temp_index_fix'],
            'add':{'hit_counter':0, 'licensed':False}
            }

    Notes on version 1.0:

    This is a nice decorator that would be improved with global version of load, in
    order to select what's really neccesary to apply in such a version. The use of
    such kind of decorator could even help to globally report cahnges for a single
    version. An idea is to globally record the decorator appliances so we could get
    all the info whitout class instances.


"""


def upgrade_version(function):
    """method decorator that provides self name access"""
    def wrap(self, state):
        """wrapped calls"""
        version_changes = function(self, state)
        if 'add' in version_changes:
            add = version_changes['add']
            for key in add:
                if key not in state:
                    state[key] = add[key]
        if 'rename' in version_changes:
            changes = version_changes['rename']
            for key in changes:
                if key in state:
                    state[changes[key]] = state[key]
                    del state[key]
        if 'delete' in version_changes:
            delete = version_changes['delete']
            for key in delete:
                if key in state:
                    del state[key]
        self.__dict__ = state
    return wrap

