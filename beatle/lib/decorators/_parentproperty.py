'''
Created on 26 ago. 2018

@author: mel
'''

class parentproperty(object):
    """This kind of property redirect calls on parent objects, if it exist.
    The purpouse of these kind properties is to  provide default implementation
    of master properties, handled by parent atoms, if they exist."""

    def __init__(self, fget=None, fset=None):
        self.fget = fget
        self.fset = fset

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        if self.fget is None:
            raise AttributeError("unreadable attribute")
        parent = getattr(obj,'_parent', None)
        if parent is not None:
            try :
                return getattr(parent, self.fget.__name__)
            except AttributeError :
                pass
        return self.fget(obj)

    def __set__(self, obj, value):
        if self.fset is None:
            raise AttributeError("can't set attribute")
        parent = getattr(obj,'_parent', None)
        if parent is not None and self.fset.__name__ in dir(parent):
            try :
                setattr(parent, self.fset.__name__, value)
                return
            except AttributeError :
                pass
        self.fset(obj, value)

    def getter(self, fget):
        return type(self)(fget, self.fset)

    def setter(self, fset):
        self.fset = fset
        return type(self)(self.fget, fset)

    
# Testing
#     
#     class Base1(object):
#         
#         def __init__(self):
#             super(Base1, self).__init__()
#             
#         @property
#         def one(self):
#             return "called Base1.one"
# 
#         @one.setter
#         def one(self, value):
#             print "setting Base1.one"
#             self._value = value
#             
#     class Base2(object):
#         
#         def __init__(self):
#             super(Base2, self).__init__()
# 
# 
#             
#     class Child(object):
#         def __init__(self, parent):
#             self._parent = parent
#             super(Child, self).__init__()
#             
#         @parentproperty
#         def one(self):
#             return "called Child.one"
#         
#         @one.setter
#         def one(self, value):
#             print "setting Child.one"
#             self._value = value
#         
#     b1 = Base1()
#     b2 = Base2()
#     c1 = Child(b1)
#     c2 = Child(b2)
#     print c1.one
#     print c2.one
#     c1.one = 12
#     c2.one = 23
#     
