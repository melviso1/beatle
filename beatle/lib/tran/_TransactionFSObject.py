# -*- coding: utf-8 -*-
"""Extensions for supporting local filesystem transactional objects.
Objects may be created/modified/deleted."""

#class recycle_bin
import os
import platform
import shutil

from ._TransactionObject import TransactionObject


class TransactionFSObject(TransactionObject):
    """This class constitutes the base for filesystem transaction objects.
    The main problem with this element is that we **must** avoid the use of
    relative paths. For this purpose, we need a base path provider"""
    def __init__(self, **kwargs):
        """init"""
        abspath = kwargs['file']
        # assert os.access(abspath, os.W_OK)  # test for write access
        # assert os.access(abspath, os.R_OK)  # test for read access
        self._mode_dir = os.path.isdir(abspath)
        self._uri = kwargs['uri']
        self._file = os.path.relpath(abspath, self._uri.dir)
        self._version = 0
        super(TransactionFSObject, self).__init__(**kwargs)

    @property
    def abs_file(self):
        """return the absolute file name"""
        try:
            return os.path.join(self._uri.dir, self._file)
        except:
            return self._file

    @property
    def file(self):
        return self._file

    @property
    def version_file(self):
        """return the current version file"""
        path, name = os.path.split(self.abs_file)
        return os.path.join(path, '.{name}.{version}'.format(name=name, version=self._version))

    @property
    def tmp_file(self):
        """return temporally fname"""
        path, name = os.path.split(self.abs_file)
        return os.path.join(path, '.{name}.tmp'.format(name=name))

    def swap(self, f1, f2):
        """swap files"""
        print("swap {f1}<->{f2}\n".format(f1=f1, f2=f2))
        shutil.copyfile(f1, self.tmp_file)
        shutil.copyfile(f2, f1)
        shutil.copyfile(self.tmp_file, f2)
        os.remove(self.tmp_file)

    def save_state(self):
        """Save state"""
        # The version of the file store changes and open a new version
        #print "saving state of version : {self._version}\n".format(self=self)
        try:
            if self._mode_dir:
                super(TransactionFSObject, self).save_state()
                return
            shutil.copyfile(self.abs_file, self.version_file)
        except:
            pass
        super(TransactionFSObject, self).save_state()

    def on_undo_redo_changed(self):
        """Do a transactional action"""
        if not self.in_undo_redo():
            self._version = self._version + 1
            return
        if self._mode_dir:
            return
        try:
            if self.in_undo():
                # This is a hack ...
                self._version = self._version + 1
                shutil.copyfile(self.abs_file, self.version_file)
                self._version = self._version - 1
            shutil.copyfile(self.version_file, self.abs_file)
        except:
            pass

    def on_undo_redo_loaded(self):
        """Do initial load"""
        self._version = 0

    def on_undo_redo_unloaded(self):
        """Do initial load"""
        pass

    def on_undo_redo_removing(self):
        """Do a transactional action"""
        # this operation is tentative:
        try:
            if self._mode_dir:
                os.rename(self.abs_file, self.tmp_file)
            else:
                shutil.copyfile(self.abs_file, self.version_file)
                os.remove(self.abs_file)
        except Exception as e:
            pass

    def on_undo_redo_add(self):
        """Do a transactional action"""
        if not self.in_undo_redo():
            return
        try:
            if self._mode_dir:
                os.rename(self.tmp_file, self.abs_file)
            else:
                shutil.copyfile(self.version_file, self.abs_file)
        except:
            pass

    def dispose(self):
        """When an object is finally removed from the stack,
        the associated external resources may be also released."""
        try:
            if self._mode_dir:
                f = self.tmp_file
                if os.access(f, os.W_OK):
                    shutil.rmtree(f)
            else:
                if self._version:
                    os.remove(self.version_file)
        except:
            pass

    def __setstate__(self, d):
        """Load pickle context"""
        file = d['_file']
        if platform.system()== 'Windows':
            file = file.replace('/','\\')
        else:
            file = file.replace('\\','/')
        d['_file'] = file

