# -*- coding: utf-8 -*-
"""This class defines a class decorator useful for creating methods that handles transactionality
in automatical manner"""

from ._TransactionStack import TransactionStack


class TransactionalMethod(object):
    """Class decorator for transactional methods"""
    def __init__(self, transaction_name=''):
        """Function decorator for transactional methods.
        The transaction_name is the name the transaction receives"""
        self._transaction_name = transaction_name
        self._base_method = None

    def __call__(self, method):
        """"""
        def wrapped_call(*args, **kwargs):
            """Code for transactional method"""
            # If we are inside an open transaction, we dont open
            # another again. Instead, do the associated operations
            # and return his 'something changes?' return value
            rollback_extra = getattr(method, 'on_rollback', None)
            if TransactionStack.InTransaction():
                return method(*args, **kwargs)
            # Ok, we on top of new transaction. We do our work,
            # commit changes or rollback when no changes or exception happens
            import traceback
            import sys
            try:
                TransactionStack.do_begin_transaction(self._transaction_name)
                value = method(*args, **kwargs)
                if value:
                    TransactionStack.do_commit()
                else:
                    TransactionStack.instance._backtrace = "operation was cancelled."
                    TransactionStack.do_rollback()
                    if rollback_extra is not None:
                        rollback_extra(*args, **kwargs)
                return value
            except Exception as inst:
                TransactionStack.instance._backtrace = traceback.format_exc()
                traceback.print_exc(file=sys.stdout)
                print(type(inst))     # the exception instance
                print(inst.args)      # arguments stored in .args
                print(inst)
                TransactionStack.do_rollback()
                if rollback_extra is not None:
                    rollback_extra(*args, **kwargs)

        setattr(wrapped_call, 'on_rollback', self.on_rollback)
        self._base_method = method
        self._instance = self
        return wrapped_call

    def on_rollback(self, method):
        base_method = self._base_method
        setattr(base_method, 'on_rollback', method)
        return method
