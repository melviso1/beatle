"""Transaction stack definition"""
import copy


def save_dict(obj):
    """Copy with temps"""
    s = copy.copy(obj.__dict__)
    if '_handler' in s:
        s['_handler'] = copy.copy(getattr(obj, '_handler').__dict__)
    if '_pane' in s:
        del s['_pane']
    if '_volatile_attributes' in s:
        for k in s['_volatile_attributes']:
            del s[k]
        del s['_volatile_attributes']
    return s


def restore_dict(s, obj):
    """Restore object dict"""
    if '_handler' in s:
        obj._handler.__dict__ = copy.copy(s['_handler'])
    normal_dict = [x for x in s if x != '_handler']
    for k in normal_dict:
        setattr(obj, k, s[k])


class Transaction(object):
    """Implements a single transaction.
    A Transaction instance represents a transaction unit, composed
     by several operations, supporting commit and rollback operations
     and when committed, undo and redo operations.

     properties:

        name : The transaction name.
        key : Special value that can be used to prevent nested propagation
              of updates.
     """
    def __init__(self, name):
        """Initialice transaction"""
        self._key = None
        self._name = name
        self._operations = []

    def dispose(self):
        """Notify all transaction components that it will be disposed"""
        for op in self._operations:
            op.dispose()

    @property
    def key(self):
        """return the key"""
        return self._key

    @key.setter
    def key(self, value):
        """sets the key"""
        self._key = value

    @property
    def name(self):
        return self._name

    @property
    def operations(self):
        return self._operations

    def insert_operation(self, pos, operation):
        self._operations.insert(pos, operation)

    def rollback(self):
        """Rollback transaction"""
        while len(self._operations) > 0:
            self._operations.pop().rollback()

    def undo(self):
        """Undoes transaction"""
        redo_operations = []
        while len(self._operations) > 0:
            redo_operations.append(self._operations.pop().undo())
        self._operations = redo_operations
        return self

    def Redo(self):
        """Redoes transaction"""
        undo_operations = []
        while len(self._operations) > 0:
            undo_operations.append(self._operations.pop().Redo())
        self._operations = undo_operations
        return self

    def refresh_changes(self):
        """Ensures visual update of saved states"""
        for operation in self._operations:
            if type(operation) is UndoChange:
                operation._obj.on_undo_redo_changed()


class UndoLoad(object):
    """Implements a undo load operation"""
    def __init__(self, obj):
        """Init"""
        obj.on_undo_redo_loaded()
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        pass

    def rollback(self):
        """Implements rollback"""
        self._obj.on_undo_redo_unloaded()
        self._obj.remove_relations()

    def undo(self):
        """Implements undo"""
        return RedoLoad(self._obj)


class UndoUnload(object):
    """Implements a undo unload operation"""
    def __init__(self, obj):
        """Init"""
        obj.on_undo_redo_unloaded()
        obj.remove_relations()
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        pass

    def rollback(self):
        """Implements rollback"""
        self._obj.restore_relations()
        self._obj.on_undo_redo_loaded()

    def undo(self):
        """Implements undo"""
        return RedoUnload(self._obj)


class RedoLoad(object):
    """Implements a redo for load operation"""
    def __init__(self, obj):
        """Initialize redo"""
        obj.on_undo_redo_unloaded()
        obj.remove_relations()
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        del self._obj  # unloaded objects dont interact

    def Redo(self):
        """Implements redo"""
        self._obj.restore_relations()
        return UndoLoad(self._obj)


class RedoUnload(object):
    """Implements a redo unload operation"""
    def __init__(self, obj):
        """Init"""
        obj.restore_relations()
        obj.on_undo_redo_loaded()
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        del self._obj  # unloaded objects dont interact
        pass

    def undo(self):
        """Implements undo"""
        return UndoUnload(self._obj)


class UndoNew(object):
    """Implements a undo new operation"""
    def __init__(self, obj, operation_queue=None):
        """Initialize the undo new.
        As this operation can involve user decided operations and some can raise an exception,
        we need to preserve the UndoNew object creation in order to enable a possible rollback."""
        exception_present = None
        try:
            obj.on_undo_redo_add()
        except Exception as e:
            exception_present = e
        finally:
            self._obj = obj
            if operation_queue is not None:
                operation_queue.append(self)
        if exception_present is not None:
            raise exception_present

    def dispose(self):
        """Remove associated data"""
        pass

    def rollback(self):
        """Implements rollback"""
        self._obj.on_undo_redo_removing()
        self._obj.remove_relations()

    def undo(self):
        """Implements undo"""
        return RedoNew(self._obj)


class RedoNew(object):
    """Implements a redo new operation"""
    def __init__(self, obj):
        """Initialize redo"""
        obj.on_undo_redo_removing()
        obj.remove_relations()
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        self._obj.dispose()

    def Redo(self):
        """Implements redo"""
        self._obj.restore_relations()
        return UndoNew(self._obj)


class UndoDelete(object):
    """Implements a undo delete operation"""
    def __init__(self, obj):
        """Initialize undo"""
        obj.on_undo_redo_removing()
        obj.remove_relations()
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        self._obj.dispose()

    def rollback(self):
        """Implements rollback"""
        self._obj.restore_relations()
        self._obj.on_undo_redo_add()

    def undo(self):
        """Implements undo"""
        return RedoDelete(self._obj)


class RedoDelete(object):
    """Implements a redo delete operation"""
    def __init__(self, obj):
        """Initialize redo"""
        obj.restore_relations()
        obj.on_undo_redo_add()
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        pass

    def Redo(self):
        """Implements redo"""
        return UndoDelete(self._obj)


class UndoChange(object):
    """Implements a undo change operation"""
    def __init__(self, obj):
        """Initialize undo change"""
        self._old_dict = save_dict(obj)
        self._obj = obj

    def dispose(self):
        """Remove associated data"""
        self._old_dict = None

    def rollback(self):
        """Implements rollback"""
        self._obj.remove_relations()
        restore_dict(self._old_dict, self._obj)
        self._obj.restore_relations()
        self._obj.on_undo_redo_changed()

    def undo(self):
        """Implements undo"""
        return RedoChange(self._old_dict, self._obj)


class RedoChange(object):
    """Implements a redo change operation"""
    def __init__(self, old_dict, obj):
        """Initialize redo change"""
        self._new_dict = save_dict(obj)
        self._obj = obj
        self._obj.remove_relations()
        restore_dict(old_dict, self._obj)
        # self._obj .__dict__ = oldObj.__dict__.copy()
        # respect_visuals_from(self._newObj, self._obj)
        self._obj.restore_relations()
        self._obj.on_undo_redo_changed()

    def dispose(self):
        """Remove associated data"""
        self._new_dict = None

    def Redo(self):
        """Implements redo"""
        r = UndoChange(self._obj)
        self._obj.remove_relations()
        restore_dict(self._new_dict, self._obj)
        self._obj.restore_relations()
        self._obj.on_undo_redo_changed()
        return r


class TransactionStack(object):
    """Implements a queue of stacked operations"""
    instance = None
    delayedCalls = {}
    delayedCallsFiltered = []

    @staticmethod
    def do_begin_transaction(name=None):
        """Starts a new transaction"""
        TransactionStack.instance.begin_transaction(name)

    @staticmethod
    def do_commit():
        """Commit transaction"""
        TransactionStack.instance.commit()

    @staticmethod
    def do_rollback():
        """Rollback transaction"""
        TransactionStack.instance.rollback()

    @staticmethod
    def do_undo():
        """Rollback transaction"""
        TransactionStack.instance.undo()

    @staticmethod
    def do_redo():
        """Rollback transaction"""
        TransactionStack.instance.Redo()

    @staticmethod
    def do_create(obj):
        """Create transaction"""
        TransactionStack.instance.Create(obj)

    @staticmethod
    def do_load(obj):
        """Load transaction"""
        TransactionStack.instance.Load(obj)

    @staticmethod
    def do_unload(obj):
        """Unload transaction"""
        TransactionStack.instance.Unload(obj)

    @staticmethod
    def do_delete(obj):
        """Create transaction"""
        TransactionStack.instance.delete(obj)

    @staticmethod
    def do_save_state(obj):
        """Save object state"""
        return TransactionStack.instance.save_state(obj)

    @staticmethod
    def is_saved_state(obj):
        """Check if the object state is saved in the current transaction"""
        TransactionStack.instance.is_saved_state(obj)

    @staticmethod
    def is_undo_load():
        return type(TransactionStack.instance) is UndoLoad

    @staticmethod
    def InTransaction():
        """Returns info about undo/redo"""
        return TransactionStack.instance._tran is not None

    @staticmethod
    def in_undo_redo():
        """Returns info about undo/redo"""
        return TransactionStack.instance._in_undo_redo

    @staticmethod
    def in_undo():
        """Returns info about undo/redo"""
        return TransactionStack.instance._in_undo

    @staticmethod
    def in_redo():
        """Returns info about undo/redo"""
        return TransactionStack.instance._in_redo

    @staticmethod
    def set_key(value):
        """Set the key for the current, uncommitted transaction.
        This key is used for preventing the source of an event
        gets backward updated. That is, the common use of this
        key is to encode the origin of the transaction."""
        TransactionStack.instance.key = value

    @staticmethod
    def get_key():
        """Get the key for the current, uncommitted transaction.
        This key could be used for example for preventing an editor
        update of code receiving transactional update from itself."""
        return TransactionStack.instance.key

    def __init__(self, deep):
        """Initialize the transaction stack"""
        self._deep = deep
        self._undo = []
        self._redo = []
        self._tran = None
        # self._inTran = False
        self._in_undo_redo = False
        self._in_redo = False
        self._in_undo = False
        self._logger = None
        TransactionStack.instance = self

    def transaction_status(self, hashcode):
        """This method is a facility for loggers to see the
        current status of some log entry. The return code
        will be:
            1 ... transaction exists in the redo stack
            0 ... transaction exists in the undo stack
           -1 ... transaction don't exist anymore (may be deleted from some stack)'"""
        if hashcode in (hash(tran) for tran in self._redo):
            return 1
        if hashcode in (hash(tran) for tran in self._undo):
            return 0
        return -1

    def SetLogger(self, logger):
        """Set an method logger that will be called for:

            a) Each new transaction commit
            b) Each new transaction undo
            c) Each new transaction redo

            The logger will receive the transaction
            and a text mode 'commit', 'undo' or 'redo'.
            Indeed, the logger will receive 'start'
            and 'end' messages (without transaction)
            for the SetLogger and for SetLogger(None).
            logger = fn(command, transaction=None)

            The obvious mission : create logs for projects
            and/or workspace
        """
        if self._logger == logger:
            # stupid call will be dissmissed
            return
        # finalize previous loggers
        if self._logger:
            self._logger('end')

        self._logger = logger
        # start new transaction logger
        if self._logger:
            self._logger('start')

    def begin_transaction(self, name=''):
        """Starts a new transaction"""
        if self._tran is None:
            self._tran = Transaction(name)
            # self._inTran = True
        return self._tran

    def commit(self):
        """Commit an open transaction"""
        if self._tran is None:
            return

        self._in_undo_redo = False
        self._in_redo = False
        self._in_undo = False
        self._tran.refresh_changes()

        # Hard protected against recursive calls - :)
        processed = {}
        while processed != TransactionStack.delayedCalls:
            methods = list(TransactionStack.delayedCalls.keys())
            for method in methods:
                if method in processed:
                    entry_list = [x for x in TransactionStack.delayedCalls[method] if x not in processed[method]]
                else:
                    processed[method] = []
                    entry_list = TransactionStack.delayedCalls[method]
                for entry in entry_list:
                    method(*entry[0], **entry[1])
                    processed[method].append(entry)

        self._tran.key = None  # The origin of the transaction is meaningless in stack
        self._undo.append(self._tran)
        if self._logger:
            self._logger('commit', transaction=self._tran)
        self._tran = None
        self.clean_redo()
        #         for method in TransactionStack.delayedCalls:
        #             if method in TransactionStack.delayedCallsFiltered:
        #                 continue
        #             for l in TransactionStack.delayedCalls[method]:
        #                 method(*l[0], **l[1])
        TransactionStack.delayedCalls = {}
        TransactionStack.delayedCallsFiltered = []
        while len(self._undo) > self._deep:
            self._undo[0].dispose()
            del self._undo[0]

    def rollback(self):
        """Rollback an open transaction"""
        if self._tran is None:
            return
        self._in_undo_redo = True
        if self._logger:
            self._logger('rollback', transaction=self)
        self._tran.rollback()
        TransactionStack.delayedCalls = {}
        TransactionStack.delayedCallsFiltered = []
        self._tran = None
        self._in_undo_redo = False

    def undo(self):
        """Undoes the last operation"""
        self._in_undo_redo = True
        self._in_undo = True
        if not self._tran is None:
            self.rollback()
        elif len(self._undo) > 0:
            if self._logger:
                self._logger('undo')
            self._redo.append(self._undo.pop().undo())
        self._in_undo_redo = False
        self._in_undo = False
        for method in TransactionStack.delayedCalls:
            if method in TransactionStack.delayedCallsFiltered:
                continue
            for l in TransactionStack.delayedCalls[method]:
                method(*l[0], **l[1])
        TransactionStack.delayedCalls = {}
        TransactionStack.delayedCallsFiltered = []

    def Redo(self):
        """Redoes the last operation"""
        self._in_undo_redo = True
        self._in_redo = True
        if not self._tran is None:
            self.rollback()
        if len(self._redo) > 0:
            if self._logger:
                self._logger('redo')
            self._undo.append(self._redo.pop().Redo())
        self._in_undo_redo = False
        self._in_redo = False
        for method in TransactionStack.delayedCalls:
            if method in TransactionStack.delayedCallsFiltered:
                continue
            for l in TransactionStack.delayedCalls[method]:
                method(*l[0], **l[1])
        TransactionStack.delayedCalls = {}
        TransactionStack.delayedCallsFiltered = []

    def CanRedo(self):
        """Check about redo availability"""
        return len(self._redo) > 0

    def CanUndo(self):
        """Check about undo availability"""
        return len(self._undo) > 0

    def clean_redo(self):
        """Clean redo stack"""
        for k in self._redo:
            k.dispose()
        self._redo = []

    def clean_undo(self):
        """Clean undo stack"""
        for k in self._undo:
            k.dispose()
        self._undo = []

    def undo_name(self):
        """Get undo name"""
        if len(self._undo) > 0:
            return self._undo[-1]._name
        return None

    @property
    def key(self):
        if self._tran:
            return self._tran.key
        return None

    @key.setter
    def key(self, value):
        """Sets the key for the current, uncommited transaction"""
        if self._tran:
            self._tran.key = value

    def GetName(self):
        """Get the current, uncommited transaction name"""
        if self._tran:
            return self._tran._name
        return None

    def SetName(self, name):
        """Set the current, uncommited transaction name"""
        if self._tran:
            self._tran._name = name

    def RedoName(self):
        """Get redo name"""
        if len(self._redo) > 0:
            return self._redo[-1]._name

    def save_state(self, obj):
        """Save object state."""
        # TODO : reaching here without transaction is a bug -> review
        local_transaction = self._tran is None
        if local_transaction:
            self.begin_transaction('saved state')
        if self.is_saved_state(obj):
            return False
        # some operations can trigger others so we must ensure order
        pos = len(self._tran.operations)
        self._tran.insert_operation(pos, UndoChange(obj))
        if local_transaction:
            self.commit()
        return True

    def is_saved_state(self, obj):
        """Checks about saved state"""
        if self._tran is None:
            return False
        for operation in self._tran.operations:
            if type(operation) is UndoChange:
                if operation._obj == obj:
                    return True
        return False

    def delete(self, obj):
        """Delete object"""
        if self._tran is None:
            self.begin_transaction()
        # some operations can trigger others so we must ensure order
        pos = len(self._tran.operations)
        self._tran.insert_operation(pos, UndoDelete(obj))

    def Create(self, obj):
        """Create object. This method must is not used to be called
        inside a undo/redo op with some exceptions (for example when
        called from parallel transactional environments, like ui design)"""
        # assert self._in_undo_redo is False
        if self._tran is None:
            self.begin_transaction()
            try:
                UndoNew(obj, self._tran.operations)
                self.commit()
            except:
                self.rollback()
        else:
            # some operations can trigger others so we must ensure order
            UndoNew(obj, self._tran.operations)

    def Load(self, obj):
        """Load object"""
        if self._tran is None:
            self.begin_transaction()
        # some operations can trigger others so we must ensure order
        pos = len(self._tran.operations)
        self._tran.insert_operation(pos, UndoLoad(obj))

    def Unload(self, obj):
        """Unload object"""
        if self._tran is None:
            self.begin_transaction()
        # some operations can trigger others so we must ensure order
        pos = len(self._tran.operations)
        self._tran.insert_operation(pos, UndoUnload(obj))


def get_current_transaction_name():
    return TransactionStack.instance.GetName()


def set_current_transaction_name(name):
    TransactionStack.instance.SetName(name)


def format_current_transaction_name(*args, **kwargs):
    """Format the transaction label, if any with missing information"""
    transaction_name = TransactionStack.instance.GetName()
    if transaction_name:
        TransactionStack.instance.SetName(transaction_name.format(*args, **kwargs))

