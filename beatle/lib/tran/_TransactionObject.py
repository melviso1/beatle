from ._TransactionStack import TransactionStack
from ._TransactionalMethod import TransactionalMethod 


class TransactionObject(object):
    """This class constitutes the base for transaction objects, i.e.
    objects that can be created and deleted with stack recording"""
    
    def __init__(self, *args, **kwargs):
        """Create and register a new transactional object instance.
        Because some circumstances (like file refresh) some objects could be
        inplace handlers and couldn't delete external resources while gets
        uncreated (that's different of being explicity deleted by the user)."""
        if kwargs.get('transactional', True):
            TransactionStack.do_create(self)
        else:            
            self.on_undo_redo_add()  # even if the object couldn't be transactionally destroyed, we execute his add

    def remove_relations(self):
        pass

    def restore_relations(self):
        pass

    def on_undo_redo_removing(self):
        pass

    def on_undo_redo_add(self):
        pass

    def on_undo_redo_changed(self):
        pass

    def in_undo_redo(self):
        return TransactionStack.in_undo_redo()

    def in_undo(self):
        return TransactionStack.in_undo()

    def in_redo(self):
        return TransactionStack.in_redo()

    def in_transaction(self):
        return TransactionStack.InTransaction()

    def save_state(self):
        """save the current object status.
        Return False iif the object state is already saved for the current transaction."""
        return TransactionStack.do_save_state(self)

    def is_saved_state(self):
        TransactionStack.is_saved_state(self)

    @TransactionalMethod("delete object")
    def delete(self):
        """Do a transactional delete for any object"""
        TransactionStack.do_delete(self)

    def dispose(self):
        """When an object is finally removed from the stack,
        the associated external resources may be also released."""
        pass
