# -*- coding: utf-8 -*-

from ._TransactionObject import TransactionObject
from ._TransactionStack import TransactionStack, format_current_transaction_name


class TransactionalMoveObject(TransactionObject):
    """Implements a relationship move operation"""
    def __init__(self, **kwargs):
        """Initialice move"""
        self._object = kwargs['object']
        self._origin = kwargs['origin']
        self._target = kwargs['target']
        self._target_index = kwargs.get('index', -1)  # -1 -> by default drop as last children
        self._origin_index = 0
        self._name = TransactionStack.instance.GetName()
        super(TransactionalMoveObject, self).__init__()
        # if the object has name, give the opportunity to add this name
        # to the current transaction name, so it will be visible in menu
        # take in account that not format will remains after that, so
        # nested transactions will no see his object names included, if any
        TransactionStack.instance.SetName(self._name)
        if hasattr(self._object, '_name'):
            format_current_transaction_name(self._object.name)

    def on_undo_redo_add(self):
        """Process on undo redo add"""
        self._object.on_undo_redo_removing()  # this is a hack specific for move object
        self._object.remove_relations()
        self._object._parent = self._target
        if hasattr(self._object, '_parentIndex'):
            self._origin_index = self._object._parentIndex
        self._object._parentIndex = self._target_index
        self._object.restore_relations()
        self._object.on_undo_redo_add()
        super(TransactionalMoveObject, self).on_undo_redo_add()

    def on_undo_redo_removing(self):
        """Process on undo redo removed"""
        self._object.on_undo_redo_removing()
        self._object.remove_relations()
        self._object._parent = self._origin
        if hasattr(self, '_origin_index'):
            self._object._parentIndex = self._origin_index
        elif hasattr(self._object, '_parentIndex'):
            del self._object._parentIndex
        self._object.restore_relations()
        self._object.on_undo_redo_add()
        super(TransactionalMoveObject, self).on_undo_redo_add()


