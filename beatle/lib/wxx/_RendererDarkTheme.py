# -*- coding: utf-8 -*-
""" Implements a dark theme renderer """
import wx
from beatle.lib.wxx.agw import _FlatMenu as FM
from beatle.lib.wxx.agw.artmanager import ArtManager

_white_check_xpm =[ b"16 16 9 1",
b" 	c None",
b".	c #84878A",
b"+	c #6F7173",
b"@	c #B1B2B3",
b"#	c #545759",
b"$	c #AEAFB0",
b"%	c #8C8E8F",
b"&	c #A2A3A4",
b"*	c #ACADAE",
b"                ",
b"                ",
b"                ",
b"            .   ",
b"           +@   ",
b"          #$%   ",
b"          &&    ",
b"   .     %$#    ",
b"   @+   +@+     ",
b"   %$# #$%      ",
b"    && &&       ",
b"    #$*$#       ",
b"     +@+        ",
b"                ",
b"                ",
b"                "]


class RendererDarkTheme(FM.FMRenderer):

    def __init__(self):
        super(RendererDarkTheme, self).__init__()
        #change default colors

        #self.itemTextColourDisabled = ArtManager.Get().LightColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_GRAYTEXT), 30)

        # Background Colours
        dbc = wx.TheColourDatabase
        self.itemTextColourDisabled = dbc.Find('DIM GRAY')
        self.menuFaceColour     = dbc.Find('DARK GREY')
        self.menuTextColour     = dbc.Find('LIGHT GRAY')
        self.menuBarFaceColour  = dbc.Find('DARK GREY')

        self.SetMenuBarHighlightColour(dbc.Find('DARK GREY'))
        # self.menuBarFocusFaceColour     = dbc.Find('DARK GREY')
        # self.menuBarFocusBorderColour   = dbc.Find('DARK GREY')
        # self.menuBarPressedFaceColour   = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
        # self.menuBarPressedBorderColour = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)

        #self.SetMenuHighlightColour(dbc.Find('NAVY'))
        self.menuFocusFaceColour     = dbc.Find('NAVY') # wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
        self.menuFocusBorderColour   = dbc.Find('NAVY') # wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
        self.menuPressedFaceColour   = wx.BLUE
        self.menuPressedBorderColour = wx.BLUE
        self.menuFocusTextColour     = dbc.Find('LIGHT STEEL BLUE')

        # self.buttonFaceColour          = dbc.Find('LIGHT GREY')
        # self.buttonBorderColour        = dbc.Find('DARK GREY')
        # self.buttonFocusFaceColour     = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
        # self.buttonFocusBorderColour   = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
        # self.buttonPressedFaceColour   = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
        # self.buttonPressedBorderColour = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)

        self.menuItemCheckMark = wx.Bitmap(_white_check_xpm)

    def DrawMenuBar(self, menubar, dc):
        super(RendererDarkTheme, self).DrawMenuBar(menubar, dc)

    def DrawMenu(self, flatmenu, dc):
        """
        Draws the menu.

        :param flatmenu: the :class:`FlatMenu` instance we need to paint;
        :param dc: an instance of :class:`wx.DC`.
        """

        menuRect = flatmenu.GetClientRect()
        menuBmp = wx.Bitmap(menuRect.width, menuRect.height)

        mem_dc = wx.MemoryDC()
        mem_dc.SelectObject(menuBmp)

        # colour the menu face with background colour
        backColour = self.menuFaceColour
        penColour  = self.menuTextColour
        # mel
        penColour = wx.Colour(penColour.red,penColour.green,penColour.blue,127)
        backBrush = wx.Brush(backColour)
        pen = wx.Pen(penColour)

        mem_dc.SetPen(pen)
        mem_dc.SetBrush(backBrush)
        mem_dc.DrawRectangle(menuRect)

        backgroundImage = flatmenu._backgroundImage

        if backgroundImage:
            mem_dc.DrawBitmap(backgroundImage, flatmenu._leftMarginWidth, 0, True)

        # draw items
        posy = 3
        nItems = len(flatmenu._itemsArr)

        # make all items as non-visible first
        for item in flatmenu._itemsArr:
            item.Show(False)

        visibleItems = 0
        screenHeight = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)

        numCols = flatmenu.GetNumberColumns()
        switch, posx, index = 1e6, 0, 0
        if numCols > 1:
            switch = int(math.ceil((nItems - flatmenu._first)/float(numCols)))

        # If we have to scroll and are not using the scroll bar buttons we need to draw
        # the scroll up menu item at the top.
        if not self.scrollBarButtons and flatmenu._showScrollButtons:
            posy += flatmenu.GetItemHeight()

        for nCount in range(flatmenu._first, nItems):

            visibleItems += 1
            item = flatmenu._itemsArr[nCount]
            self.DrawMenuItem(item, mem_dc, posx, posy,
                              flatmenu._imgMarginX, flatmenu._markerMarginX,
                              flatmenu._textX, flatmenu._rightMarginPosX,
                              nCount == flatmenu._selectedItem,
                              backgroundImage=backgroundImage)
            posy += item.GetHeight()
            item.Show()

            if visibleItems >= switch:
                posy = 2
                index += 1
                posx = flatmenu._menuWidth*index
                visibleItems = 0

            # make sure we draw only visible items
            pp = flatmenu.ClientToScreen(wx.Point(0, posy))

            menuBottom = (self.scrollBarButtons and [pp.y] or [pp.y + flatmenu.GetItemHeight()*2])[0]

            if menuBottom > screenHeight:
                break

        if flatmenu._showScrollButtons:
            if flatmenu._upButton:
                flatmenu._upButton.Draw(mem_dc)
            if flatmenu._downButton:
                flatmenu._downButton.Draw(mem_dc)

        dc.Blit(0, 0, menuBmp.GetWidth(), menuBmp.GetHeight(), mem_dc, 0, 0)
