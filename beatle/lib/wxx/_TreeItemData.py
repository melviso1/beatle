
# Little class for wrapping from classic
class TreeItemData(object):
	
	def __init__(self, obj):
		self._obj = obj

	def get_data(self):
		return self._obj

	def set_data(self, obj):
		self._obj = obj

