# -*- coding: utf-8 -*-
"""This module defines a custom event for dispatching messages to logger window"""

import wx


class OutputEvent(wx.PyCommandEvent):
    """Custom event"""
    _type = wx.NewEventType()

    def __init__(self, message, _id=wx.ID_ANY):
        """Initializer"""
        super(OutputEvent, self).__init__(OutputEvent._type, _id)
        self._message = message

    @property
    def level(self):
        """Returns the message level"""
        return self._level

    @property
    def message(self):
        """Returns the message"""
        return self._message

EVT_OUTPUT = wx.PyEventBinder(OutputEvent._type, 1)
