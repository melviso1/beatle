# -*- coding: utf-8 -*-
# cython: language_level=3

import wx
from wx import aui as base


class AuiNotebook(base.AuiNotebook):
    """Aui notebook override - windows version
    As long as windows couldn't handle different sizes in image lists,
    we use a custom wxx.ImageList that monkey patchs the wx.ImageList.
    The first implication is that we couldn't call native SetImageList.
    Fortunately, it looks that we could assign the bitmap we need
    to each page, so we could emulate the behaviour handling our custom
    imageList"""

    def __init__(self, *args, **kwargs):
        """Initialize notebook"""
        super(AuiNotebook, self).__init__(*args, **kwargs)

        dbc = wx.TheColourDatabase
        self._image_list = None
        tab_art = wx.aui.AuiSimpleTabArt().Clone()
        tab_art.SetActiveColour(dbc.Find('LIGHT BLUE'))
        self._text_colour = dbc.Find('WHITE')
        tab_art.SetColour(dbc.Find('LIGHT GREY'))
        self.SetArtProvider(tab_art)

    def AddPage(self, *args, **kwargs):
        """Interface AddPage"""
        bitmap = None
        if 'imageId' in kwargs:
            if self._image_list:
                bitmap = self._image_list.GetBitmap(kwargs['imageId'])
            del kwargs['imageId']
        elif 'bitmap' in kwargs:
            if self._image_list:
                bitmap = kwargs['bitmap']
            del kwargs['bitmap']
        elif len(args) == 4:
            if self._image_list and args[3] != wx.NOT_FOUND:
                bitmap = self._image_list.GetBitmap(args[3])
            args = tuple(args[:3])
        if super(AuiNotebook, self).AddPage(*args, **kwargs):
            if bitmap is not None:
                self.SetPageBitmap(self.PageCount - 1, bitmap)
            return True
        return False

    def InsertPage(self, *args, **kwargs):
        """Interface InsertPage"""
        bitmap = None
        if 'imageId' in kwargs:
            if self._image_list:
                bitmap = self._image_list.GetBitmap(kwargs['imageId'])
            del kwargs['imageId']
        elif len(args) == 5:
            if self._image_list:
                bitmap = self._image_list.GetBitmap(args[4])
            args = tuple(args[:4])
        if super(AuiNotebook, self).InsertPage(*args, **kwargs):
            if bitmap is not None:
                self.SetPageBitmap(args[0], bitmap)
            return True
        return False

    def SetPageImage(self, n, imageId):
        if self._image_list:
            self.SetPageBitmap(n, self._image_list.GetBitmap(imageId))

    def GetImageList(self):
        """
        GetImageList() -> ImageList

        Returns the associated image list, may be NULL.
        """
        return self._image_list

    def SetImageList(self, imageList):
        """
        SetImageList(imageList)

        Sets the image list to use.
        """
        self._image_list = imageList

    @property
    def ImageList(self):
        return self._image_list

    @ImageList.setter
    def ImageList(self, imageList):
        self._image_list = imageList
