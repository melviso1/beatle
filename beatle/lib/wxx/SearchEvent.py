# -*- coding: utf-8 -*-
"""This module defines a custom event for dispatching messages to logger window"""

import wx


class SearchEvent(wx.PyCommandEvent):
    """Custom event"""
    _type = wx.NewEventType()

    def __init__(self, fname, matches, _id=wx.ID_ANY):
        """Initializer"""
        super(SearchEvent, self).__init__(SearchEvent._type, _id)
        self._fname = fname
        self._matches = matches

    @property
    def matches(self):
        return self._matches

    @property
    def fname(self):
        """Returns the file"""
        return self._fname

EVT_SEARCH = wx.PyEventBinder(SearchEvent._type, 1)
