    # -*- coding: utf-8 -*-
""" Fix of wx.lib.agw.flatmenu.FileHistory """

from beatle.lib.wxx.agw._FlatMenu import GetMRUEntryLabel, FileHistory as FileHistoryBuggy


class FileHistory(FileHistoryBuggy):

    def __init__(self, *args,**kwargs):
        super(FileHistory, self).__init__(*args,**kwargs)

    def AddFilesToMenu2(self, menu):
        """
        Appends the files in the history list, to the given menu only.

        :param `menu`: an instance of :class:`FlatMenu`.
        """

        if not self._fileHistory:
            return

        if menu.GetMenuItemCount():
            menu.AppendSeparator()

        for index in range(len(self._fileHistory)):
            menu.Append(self._idBase + index, GetMRUEntryLabel(index, self._fileHistory[index]))

    def Save(self, config):
        """
        Saves the file history to the given `config` object.

        :param config: an instance of :class:`Config <ConfigBase>`.

        :note: This function should be called explicitly by the application.

        :see: :meth:~FileHistory.Load.
        """

        buffer = "file%d"

        for index in range(self._fileMaxFiles):

            if index < len(self._fileHistory):
                config.Write(buffer%(index+1), self._fileHistory[index])
            else:
                config.Write(buffer%(index+1), "")

    def RemoveFileFromHistory(self, index):
        """
        Removes the specified file from the history.

        :param integer `index`: the zero-based index indicating the file name position in
         the file list.
        """

        numFiles = len(self._fileHistory)
        if index >= numFiles:
            raise Exception("Invalid index in RemoveFileFromHistory: %d (only %d files)"%(index, numFiles))

        # delete the element from the array
        self._fileHistory.pop(index)
        numFiles -= 1

        for menu in self._fileMenus:
            # shift file names up
            for j in range(numFiles):
                menu.SetLabel(self._idBase + j, GetMRUEntryLabel(j, self._fileHistory[j]))

            # delete the last menu item which is unused now
            last_item_id = self._idBase + numFiles
            if menu.FindItem(last_item_id):
                menu.Delete(last_item_id)

            if not self._fileHistory:
                menu_items = menu.GetMenuItems()
                if len(menu_items) > 0:
                    last_menu_item = menu_items[-1]
                    if last_menu_item.IsSeparator():
                        menu.Delete(last_menu_item)
