# -*- coding: utf-8 -*-
# cython: language_level=3

import wx

if "__WXGTK__" in wx.PlatformInfo:
    if wx.__version__ < '4.1.1':
        from .linux._AuiNotebook import *
    else:
        from .win._AuiNotebook import *
elif "__WXMSW__" in wx.PlatformInfo:
    from .win._AuiNotebook import *