from beatle.lib.wxx.agw import FlatMenuItem


class MenuItem(FlatMenuItem):
    """Override FlatMenuItem to mimic wx.MenuItem"""
    def __init__(self,parent, *args, **kwargs):
        """Create a menu item.
            kwargs (=default):
                id = wx.ID_SEPARATOR
                label = ''
                helpString = ''
                kind = wx.ITEM_NORMAL
                subMenu = None
                normalBmp = wx.NullBitmap
                disabledBmp = wx.NullBitmap
                hotBmp = wx.NullBitmap
        """
        super(MenuItem, self).__init__(parent, *args, **kwargs)

    def SetBitmap(self, bmp):
        self.SetNormalBitmap(bmp)

    def SetKind(self, kind):
        self._kind = kind