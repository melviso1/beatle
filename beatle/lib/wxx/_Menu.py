"""
    wxx.Menu
    This class improves the creation of menus.
"""
import wx
from  beatle.lib.wxx.agw import _FlatMenu as FM
from ._MenuItem import MenuItem
from beatle.lib.wxx.agw.fmresources import MENU_HT_SCROLL_DOWN, MENU_HT_SCROLL_UP, MENU_HT_ITEM
# import sys, traceback


class MenuKbdRedirector(wx.EvtHandler):
    """ A keyboard event handler. """

    def __init__(self, menu, oldHandler):
        """
        Default class constructor.

        :param `menu`: an instance of :class:`FlatMenu` for which we want to redirect
         keyboard inputs;
        :param `oldHandler`: a previous (if any) :class:`EvtHandler` associated with
         the menu.
        """

        self._oldHandler = oldHandler
        self.SetMenu(menu)
        wx.EvtHandler.__init__(self)


    def SetMenu(self, menu):
        """
        Sets the listener menu.

        :param `menu`: an instance of :class:`FlatMenu`.
        """

        self._menu = menu


    def ProcessEvent(self, event):
        """
        Processes the inout event.

        :param `event`: any kind of keyboard-generated events.
        """

        if event.GetEventType() in [wx.wxEVT_KEY_DOWN, wx.wxEVT_CHAR, wx.wxEVT_CHAR_HOOK]:
            return self._menu.OnChar(event.GetKeyCode())
        else:
            return self._oldHandler.ProcessEvent(event)


class Menu(FM.FlatMenu):
    """Quick menu"""
    def __init__(self, *args,**kwargs):
        """Create a new menu.
         args: submenu_descr: list of arrays. Each entry must have the form:
             [identifier, itemtext, helpstring, kind, bitmap]
            for insert a menu item
        or
            []
            for insert a separator
        The identifier is the message identifier triggered by the menu.
        The bitmap may be not specified.
        """
        self._oldCur = None
        super(Menu, self).__init__(**kwargs)
        self._borderYWidth = 1
        self._itemHeight = 10
        self._is_popup = False
        self._keyboard_handler = None
        self._event_handler = None
        self.SetSize(wx.Size(self._menuWidth, self._itemHeight + 5))
        for element in args:
            assert type(element) is list
            if len(element) == 0:
                self.AppendSeparator()
                continue
            assert len(element) in [4,5]
            menu_item = MenuItem(self, *tuple(element[:4]))
            if len(element) == 5:
                menu_item.SetBitmap(element[4])
            self.AppendItem(menu_item)
        self.Bind(wx.EVT_SET_CURSOR, self.OnSetCursor)
        self._title = kwargs.get('title', '')

    @property
    def menu_bar_title(self):
        return getattr(self, '_menuBarFullTitle', None)

    def GetTitle(self):
        return self._title

    def SetTitle(self, title):
        self._title = title

    def OnSetCursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_ARROW))

    def OnMouseEnterWindow(self, event):
        """
        Handles the ``wx.EVT_ENTER_WINDOW`` event for :class:`FlatMenu`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """
        self._oldCur = self.GetCursor()
        self.SetCursor(wx.Cursor(wx.CURSOR_ARROW))
        event.Skip()

    def OnMouseLeaveWindow(self, event):
        """
        Handles the ``wx.EVT_ENTER_WINDOW`` event for :class:`FlatMenu`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """
        if self._oldCur is not None:
            self.SetCursor(self._oldCur)
        event.Skip()


    @property
    def items_count(self):
        """return the number of entries"""
        return len(self._itemsArr)

    def Append(self, *args, **kwargs):
        if isinstance(args[0], MenuItem):
            super(Menu, self).AppendItem(*args, **kwargs)
        else:
            super(Menu, self).Append(*args, **kwargs)

    # events
    def PostCmdEvent(self, itemIdx):
        """
        Actually sends menu command events.

        :param integer `itemIdx`: the menu item index for which we want to send a command event.
        """

        if itemIdx < 0 or itemIdx >= len(self._itemsArr):
            raise Exception("Invalid menu item")
            return

        item = self._itemsArr[itemIdx]

        # Create the event
        event = wx.CommandEvent(FM.wxEVT_FLAT_MENU_SELECTED, item.GetId())

        # For checkable item, set the IsChecked() value
        if item.IsCheckable():
            event.SetInt((item.IsChecked() and [1] or [0])[0])

        event.SetEventObject(self)

        if self._owner:
            wx.PostEvent(self._owner, event)
        else:
            wx.PostEvent(self, event)

    def TryOpenSubMenu(self, itemIdx, selectFirst=False):
        """
        If `itemIdx` is an item with submenu, open it.

        :param integer `itemIdx`: the index of the item for which we want to open the submenu;
        :param bool `selectFirst`: if ``True``, the first item of the submenu will be shown
         as selected.
        """

        if itemIdx < 0 or itemIdx >= len(self._itemsArr):
            return False

        item = self._itemsArr[itemIdx]
        if item.IsSubMenu() and not item.GetSubMenu().IsShown():

            if item.GetSubMenu().items_count == 0:
                return False

            pos = wx.Point()

            # Popup child menu
            pos.x = item.GetRect().GetWidth()+ item.GetRect().GetX()-5
            pos.y = item.GetRect().GetY()
            self._clearCurrentSelection = False
            self._openedSubMenu = item.GetSubMenu()
            item.GetSubMenu().Popup(pos, self._owner, self)

            # Select the first child
            if selectFirst:

                dc = wx.ClientDC(item.GetSubMenu())
                item.GetSubMenu()._selectedItem = 0
                item.GetSubMenu().DrawSelection(dc)

            return True
        return False

    def UpdateUI(self, recursive=True):
        """
        :param `recursive` : Do nested update
        """
        numEvents = len(self._itemsArr)
        cc = 0
        self._shiftePos = 0
        if recursive:
            for cc in range(numEvents):
                self.SendUIEvent(cc)
        else:
            for item in self._itemsArr:
                self.SendUIEvent(cc)
                cc += 1
                if item.IsSubMenu():
                    item.GetSubMenu().UpdateUI()


    def Popup(self, pt, owner=None, parent=None):
        """
        Pops up the menu.

        :param `pt`: the point at which the menu should be popped up (an instance
         of :class:`wx.Point`);
        :param `owner`: the owner of the menu. The owner does not necessarly mean the
         menu parent, it can also be the window that popped up it;
        :param `parent`: the menu parent window.
        """
        self._is_popup = True
        if "__WXMSW__" in wx.Platform:
            self._mousePtAtStartup = wx.GetMousePosition()

        # each time we popup, need to reset the starting index
        self._first = 0

        # Set the owner of the menu. All events will be directed to it.
        # If owner is None, the Default GetParent() is used as the owner
        self._owner = owner
        self.UpdateUI(False)

        # The FlatMenuBase is buggy because it expects that parent become
        # a Menu container and this generates Dismiss errors.
        # The quick way to solve that is to translate point before call
        if parent is None and isinstance(owner, wx.Window):
            pt = owner.ClientToScreen(pt)
        # Adjust menu position and show it
        FM.FlatMenuBase.Popup(self, pt, parent)
        self.SetCursor(wx.Cursor(wx.CURSOR_ARROW))

        artMgr = FM.ArtManager.Get()
        artMgr.MakeWindowTransparent(self, artMgr.GetTransparency())

        # Replace the event handler of the active window to direct
        # all keyboard events to us and the focused window to direct char events to us
        self._activeWin = wx.GetActiveWindow()
        if self._activeWin:
            oldHandler = self._activeWin.GetEventHandler()
            self._keyboard_handler = MenuKbdRedirector(self, oldHandler)
            self._activeWin.PushEventHandler(self._keyboard_handler)
            # print('pushed keyboard event handler {}'.format(self._keyboard_handler))
            # traceback.print_stack()
            # sys.stdout.flush()

        if "__WXMSW__" in wx.Platform:
            self._focusWin = wx.Window.FindFocus()
        elif "__WXGTK__" in wx.Platform:
            self._focusWin = self
        else:
            self._focusWin = None

        if self._focusWin:
            self._event_handler =  FM.FocusHandler(self)
            self._focusWin.PushEventHandler(self._event_handler)
            # print('pushed focus event handler {}'.format(self._event_handler))
            # traceback.print_stack()
            # sys.stdout.flush()

    def DoAction(self, itemIdx):
        """
        Performs an action based on user selection.

        :param integer `itemIdx`: the index of the item for which we want to perform the action.
        """

        if itemIdx < 0 or itemIdx >= len(self._itemsArr):
            raise Exception("Invalid menu item")
            return

        item = self._itemsArr[itemIdx]

        if not item.IsEnabled() or item.IsSeparator():
            return

        # Close sub-menu if needed
        self.CloseSubMenu(itemIdx)

        if item.IsSubMenu() and not item.GetSubMenu().IsShown():

            # Popup child menu
            self.TryOpenSubMenu(itemIdx)
            return

        if item.IsRadioItem():
            # if the radio item is already checked,
            # just send command event. Else, check it, uncheck the current
            # checked item in the radio item group, and send command event
            if not item.IsChecked():
                item._groupPtr.SetSelection(item)

        elif item.IsCheckable():

            item.Check(not item.IsChecked())
            dc = wx.ClientDC(self)
            self.DrawSelection(dc)

        if not item.IsSubMenu():
            self.PostCmdEvent(itemIdx)
            if isinstance(self._parentMenu, FM.FlatMenuBase):
                self.Dismiss(True, False)
            else:
                self.Dismiss(False, False)


    # event overrides
    def OnKeyDown(self, event):
        """
        Handles the ``wx.EVT_KEY_DOWN`` event for :class:`FlatMenu`.

        :param `event`: a :class:`KeyEvent` event to be processed.
        """
        if self._is_popup:
            if event.GetKeyCode() == wx.WXK_ESCAPE:
                self.Dismiss(True,True)
        self.OnChar(event.GetKeyCode())

    def OnChar(self, key):
        """
        Handles key events for :class:`FlatMenu`.

        :param `key`: the keyboard key integer code.
        """

        processed = True

        if key == wx.WXK_ESCAPE:

            if isinstance(self._parentMenu, FM.FlatMenu):
                self._parentMenu.CloseSubMenu(-1)
            else:
                self.Dismiss(True, True)

        elif key == wx.WXK_LEFT:

            if isinstance(self._parentMenu, FM.FlatMenu):
                # We are a submenu, dismiss us.
                self._parentMenu.CloseSubMenu(-1)
            else:
                # try to find our root menu, if we are attached to menubar,
                # let it try and open the previous menu
                root = self.GetRootMenu()
                if root:
                    if root._mb:
                        root._mb.ActivatePreviousMenu()
        else:
            processed = super(Menu, self).OnChar(key)
        return processed

    def OnMouseLeftUp(self, event):
        """
        Handles the ``wx.EVT_LEFT_UP`` event for :class:`FlatMenu`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """

        if self.TryScrollButtons(event):
            return

        pos = event.GetPosition()
        rect = self.GetClientRect()

        if not rect.Contains(pos):

            # The event is not in our coords,
            # so we try our parent
            if isinstance(self._parentMenu, FM.FlatMenu):
                win = self._parentMenu

                while win:

                    # we need to translate our client coords to the client coords of the
                    # window we forward this event to
                    ptScreen = self.ClientToScreen(pos)
                    p = win.ScreenToClient(ptScreen)

                    if win.GetClientRect().Contains(p):

                        event.SetX(p.x)
                        event.SetY(p.y)
                        win.OnMouseLeftUp(event)
                        return

                    else:
                        # try the grandparent
                        win = win._parentMenu
        else:
            self.ProcessMouseLClickEnd(pos)

        if self._showScrollButtons:

            if self._upButton:
                self._upButton.ProcessLeftUp(pos)
            if self._downButton:
                self._downButton.ProcessLeftUp(pos)


    def ProcessMouseRClick(self, pos):
        """
        Processes mouse right clicks.

        :param `pos`: the position at which the mouse right button was pressed,
         an instance of :class:`wx.Point`.
        """

        rect = self.GetClientRect()

        if not rect.Contains(pos):

            # The event is not in our coords,
            # so we try our parent
            if isinstance(self._parentMenu, FM.FlatMenu):

                win = self._parentMenu
                while win:

                    # we need to translate our client coords to the client coords of the
                    # window we forward self event to
                    ptScreen = self.ClientToScreen(pos)
                    p = win.ScreenToClient(ptScreen)

                    if win.GetClientRect().Contains(p):
                        win.ProcessMouseRClick(p)
                        return

                    else:
                        # try the grandparent
                        win = win._parentMenu

            # At this point we can assume that the event was not
            # processed, so we dismiss the menu and its children
            self.Dismiss(True, True)
            return

        # test if we are on a menu item
        res, itemIdx = self.HitTest(pos)
        if res == MENU_HT_ITEM:
            self.OpenItemContextMenu(itemIdx)


    def ProcessMouseLClick(self, pos):
        """
        Processes mouse left clicks.

        :param `pos`: the position at which the mouse left button was pressed,
         an instance of :class:`wx.Point`.
        """

        rect = self.GetClientRect()
        if not rect.Contains(pos):

            # The event is not in our coords,
            # so we try our parent
            if isinstance(self._parentMenu, FM.FlatMenu):
                win = self._parentMenu
                while win:

                    # we need to translate our client coords to the client coords of the
                    # window we forward self event to
                    ptScreen = self.ClientToScreen(pos)
                    p = win.ScreenToClient(ptScreen)

                    if win.GetClientRect().Contains(p):
                        win.ProcessMouseLClick(p)
                        return

                    else:
                        # try the grandparent
                        win = win._parentMenu

            # At this point we can assume that the event was not
            # processed, so we dismiss the menu and its children
            self.Dismiss(True, True)

    def ProcessMouseMove(self, pos):
        """
        Processes mouse movements.

        :param `pos`: the position at which the mouse was moved, an instance of :class:`wx.Point`.
        """

        rect = self.GetClientRect()

        if not rect.Contains(pos):

            # The event is not in our coords,
            # so we try our parent
            if isinstance(self._parentMenu, FM.FlatMenu):
                win = self._parentMenu
                while win:

                    # we need to translate our client coords to the client coords of the
                    # window we forward self event to
                    ptScreen = self.ClientToScreen(pos)
                    p = win.ScreenToClient(ptScreen)

                    if win.GetClientRect().Contains(p):
                        win.ProcessMouseMove(p)
                        return

                    else:
                        # try the grandparent
                        win = win._parentMenu

            # If we are attached to a menu bar,
            # let him process the event as well
            if self._mb:

                ptScreen = self.ClientToScreen(pos)
                p = self._mb.ScreenToClient(ptScreen)

                if self._mb.GetClientRect().Contains(p):

                    # let the menu bar process it
                    self._mb.ProcessMouseMoveFromMenu(p)
                    return

            if self._mb_submenu:
                ptScreen = self.ClientToScreen(pos)
                p = self._mb_submenu.ScreenToClient(ptScreen)
                if self._mb_submenu.GetClientRect().Contains(p):
                    # let the menu bar process it
                    self._mb_submenu.ProcessMouseMoveFromMenu(p)
                    return

            return
        # test if we are on a menu item
        res, itemIdx = self.HitTest(pos)

        if res == MENU_HT_SCROLL_DOWN:

            if self._downButton:
                self._downButton.ProcessMouseMove(pos)

        elif res == MENU_HT_SCROLL_UP:

            if self._upButton:
                self._upButton.ProcessMouseMove(pos)

        elif res == MENU_HT_ITEM:

            if self._downButton:
                self._downButton.ProcessMouseMove(pos)

            if self._upButton:
                self._upButton.ProcessMouseMove(pos)

            if self._selectedItem == itemIdx:
                return

            # Message to send when out of last selected item
            if self._selectedItem != -1:
                self.SendOverItem(self._selectedItem, False)
            self.SendOverItem(itemIdx, True)   # Message to send when over an item

            oldSelection = self._selectedItem
            self._selectedItem = itemIdx
            self.CloseSubMenu(self._selectedItem)

            dc = wx.ClientDC(self)
            self.DrawSelection(dc, oldSelection)

            self.TryOpenSubMenu(self._selectedItem)

            if self._mb:
                self._mb.RemoveHelp()
                if itemIdx >= 0:
                    self._mb.DoGiveHelp(self._itemsArr[itemIdx])

        else:

            # Message to send when out of last selected item
            if self._selectedItem != -1:
                item = self._itemsArr[self._selectedItem]
                if item.IsSubMenu() and item.GetSubMenu().IsShown():
                    return

                # Message to send when out of last selected item
                if self._selectedItem != -1:
                    self.SendOverItem(self._selectedItem, False)

            oldSelection = self._selectedItem
            self._selectedItem = -1
            dc = wx.ClientDC(self)
            self.DrawSelection(dc, oldSelection)

    # def Dismiss(self, dismissParent, resetOwner):
    #     """
    #     Dismisses the popup window.
    #
    #     :param bool `dismissParent`: whether to dismiss the parent menu or not;
    #     :param bool `resetOwner`: ``True`` to delete the link between this menu and the
    #      owner menu, ``False`` otherwise.
    #     """
    #     self._selectedItem = -1
    #
    #     if self._mb:
    #         self._mb.RemoveHelp()
    #     if self._focusWin:
    #         assert self._event_handler == self._focusWin.GetEventHandler()
    #         self._focusWin.PopEventHandler(True)
    #         self._focusWin = None
    #     if self._activeWin:
    #         assert self._keyboard_handler == self._activeWin.GetEventHandler()
    #         self._event_handler = None
    #         self._activeWin.PopEventHandler(True)
    #         self._activeWin = None
    #     FM.FlatMenu.Dismiss(self, dismissParent, resetOwner)

    def OnKillFocus(self, event):
        """
        Handles the ``wx.EVT_KILL_FOCUS`` event for :class:`FlatMenu`.

        :param `event`: a :class:`FocusEvent` event to be processed.
        """

        self.Dismiss(True, True)

    def AppendMenu(self, id, item, subMenu, helpString=""):
        """
        Adds a pull-right submenu to the end of the menu.

        :param integer `id`: the menu item identifier;
        :param string `item`: the string to appear on the menu item;
        :param `subMenu`: an instance of :class:`FlatMenu`, the submenu to append;
        :param string `helpString`: an optional help string associated with the item. By default,
         the handler for the ``EVT_FLAT_MENU_ITEM_MOUSE_OVER`` event displays this string
         in the status line.
        """

        newItem = MenuItem(self, id, item, helpString, wx.ITEM_NORMAL, subMenu)
        return self.AppendItem(newItem)

    def PrependMenu(self, id, item, subMenu, helpString=""):
        """
        Adds a pull-right submenu to the end of the menu.

        :param integer `id`: the menu item identifier;
        :param string `item`: the string to appear on the menu item;
        :param `subMenu`: an instance of :class:`FlatMenu`, the submenu to append;
        :param string `helpString`: an optional help string associated with the item. By default,
         the handler for the ``EVT_FLAT_MENU_ITEM_MOUSE_OVER`` event displays this string
         in the status line.
        """

        newItem = MenuItem(self, id, item, helpString, wx.ITEM_NORMAL, subMenu)
        return self.PrependItem(newItem)


