"""
Once upon a time it was taken the ugly decision to make that c++ side of
dialogs take a reference to his python side. Commodity for the framework,
disaster for developers, that see his dialogs doing strange things
before caught in account of the "forever living" feature.
This class is a tiny wrapper around  wx.Dialog just destroying the c++
side at end-modal. This class is only for modal dialogs.
"""
from wx import Dialog as SelfReferencedDialog


class Dialog(SelfReferencedDialog):

    def __init__(self, *args, **kwargs):
        super(Dialog, self).__init__(*args, **kwargs)

    def EndModal(self, retCode):
        super(Dialog, self).EndModal(retCode)
        self.Destroy()