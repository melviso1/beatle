# --------------------------------------------------------------------------- #
# SIMPLEBOOK Widget wxPython IMPLEMENTATION
#
# Based on Original C++ Code From Eran. You Can Find It At:
#
# http://wxforum.shadonet.com/viewtopic.php?t=5761&start=0
#
# License: wxWidgets license
#
#
# Base on Python Code By:
#
# Andrea Gavana, @ 02 Oct 2006
# Latest Revision: 15 Feb 2014, 23.00 GMT
#
#
#
# End Of Comments
# --------------------------------------------------------------------------- #

"""
:class:`Simplebook` is a generic implementation of :class:`Simplebook`.


Description
===========

:class:`Simplebook` is a implementation of the :class:`Simplebook` missing in wxPython Classic 


Usage
=====

Usage example::

    import wx
    from beatle.wxx import Simplebook

    class MyFrame(wx.Frame):

        def __init__(self, parent):

            wx.Frame.__init(self, parent, -1, "Simplebook Demo")        

            panel = wx.Panel(self)
            
            notebook = Simplebook(panel, -1)
            
            for i in xrange(3):
                notebook.AddPage(wx.Panel(notebook))
                
            sizer = wx.BoxSizer(wx.VERTICAL)
            sizer.Add(notebook, 1, wx.ALL|wx.EXPAND, 5)
            panel.SetSizer(sizer)


        
    # our normal wxApp-derived class, as usual

    app = wx.App(0)

    frame = MyFrame(None)
    app.SetTopWindow(frame)
    frame.Show()

    app.MainLoop()



Events Processing
=================

This class processes the following events:

========================================= ==================================================
Event Name                                Description
========================================= ==================================================
``EVT_SIMPLEBOOK_PAGE_CHANGED``         Notify client objects when the active page in :class:`Simplebook` has changed.
``EVT_SIMPLEBOOK_PAGE_CHANGING``        Notify client objects when the active page in :class:`Simplebook` is about to change.
``EVT_SIMPLEBOOK_PAGE_CLOSED``          Notify client objects when a page in :class:`Simplebook` has been closed.
``EVT_SIMPLEBOOK_PAGE_CLOSING``         Notify client objects when a page in :class:`Simplebook` is closing.
========================================= ==================================================


License And Version
===================

:class:`Simplebook` is distributed under the wxPython license.

Latest Revision: Mel Viso on 29-08-2018

Version 1.0
"""

__docformat__ = "epytext"


#----------------------------------------------------------------------
# Beginning Of SIMPLEBOOK wxPython Code
#----------------------------------------------------------------------

import wx


# Simplebook Events:
# wxEVT_SIMPLEBOOK_PAGE_CHANGED: Event Fired When You Switch Page;
# wxEVT_SIMPLEBOOK_PAGE_CHANGING: Event Fired When You Are About To Switch
# Pages, But You Can Still "Veto" The Page Changing By Avoiding To Call
# event.Skip() In Your Event Handler;
# wxEVT_SIMPLEBOOK_PAGE_CLOSING: Event Fired When A Page Is Closing, But
# You Can Still "Veto" The Page Changing By Avoiding To Call event.Skip()
# In Your Event Handler;
# wxEVT_SIMPLEBOOK_PAGE_CLOSED: Event Fired When A Page Is Closed.

wxEVT_SIMPLEBOOK_PAGE_CHANGED = wx.wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED
wxEVT_SIMPLEBOOK_PAGE_CHANGING = wx.wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING
wxEVT_SIMPLEBOOK_PAGE_CLOSING =   wx.NewEventType()
wxEVT_SIMPLEBOOK_PAGE_CLOSED = wx.NewEventType()

#-----------------------------------#
#        SimplebookEvent
#-----------------------------------#

EVT_SIMPLEBOOK_PAGE_CHANGED = wx.EVT_NOTEBOOK_PAGE_CHANGED
""" Notify client objects when the active page in :class:`Simplebook` has changed."""
EVT_SIMPLEBOOK_PAGE_CHANGING = wx.EVT_NOTEBOOK_PAGE_CHANGING
""" Notify client objects when the active page in :class:`Simplebook` is about to change."""
EVT_SIMPLEBOOK_PAGE_CLOSING = wx.PyEventBinder(wxEVT_SIMPLEBOOK_PAGE_CLOSING, 1)
""" Notify client objects when a page in :class:`Simplebook` is closing."""
EVT_SIMPLEBOOK_PAGE_CLOSED = wx.PyEventBinder(wxEVT_SIMPLEBOOK_PAGE_CLOSED, 1)
""" Notify client objects when a page in :class:`Simplebook` has been closed."""

# ---------------------------------------------------------------------------- #
# Class SimplebookEvent
# ---------------------------------------------------------------------------- #

class SimplebookEvent(wx.PyCommandEvent):
    """
    This events will be sent when a ``EVT_SIMPLEBOOK_PAGE_CHANGED``,
    ``EVT_SIMPLEBOOK_PAGE_CHANGING``, ``EVT_SIMPLEBOOK_PAGE_CLOSING``,
    ``EVT_SIMPLEBOOK_PAGE_CLOSED`` is mapped in the parent.
    """

    def __init__(self, eventType, eventId=1, nSel=-1, nOldSel=-1):
        """
        Default class constructor.

        :param `eventType`: the event type;
        :param `eventId`: the event identifier;
        :param `nSel`: the current selection;
        :param `nOldSel`: the old selection.
        """

        wx.PyCommandEvent.__init__(self, eventType, eventId)
        self._eventType = eventType
        self.notify = wx.NotifyEvent(eventType, eventId)

    def GetNotifyEvent(self):
        """ Returns the actual :class:`NotifyEvent`. """
        return self.notify


    def IsAllowed(self):
        """
        Returns ``True`` if the change is allowed (:meth:`~SimplebookEvent.Veto` hasn't been called) or
        ``False`` otherwise (if it was).
        """
        return self.notify.IsAllowed()


    def Veto(self):
        """
        Prevents the change announced by this event from happening.

        :note: It is in general a good idea to notify the user about the reasons
         for vetoing the change because otherwise the applications behaviour (which
         just refuses to do what the user wants) might be quite surprising.
        """
        self.notify.Veto()


    def Allow(self):
        """
        This is the opposite of :meth:`~SimplebookEvent.Veto`: it explicitly allows the event to be processed.
        For most events it is not necessary to call this method as the events are
        allowed anyhow but some are forbidden by default (this will be mentioned
        in the corresponding event description).
        """
        self.notify.Allow()


    def SetSelection(self, nSel):
        """
        Sets the event selection.

        :param nSel: an integer specifying the new selection.
        """
        self._selection = nSel


    def SetOldSelection(self, nOldSel):
        """
        Sets the id of the page selected before the change.

        :param nOldSel: an integer specifying the old selection.
        """
        self._oldselection = nOldSel


    def GetSelection(self):
        """ Returns the currently selected page, or -1 if none was selected. """

        return self._selection


    def GetOldSelection(self):
        """ Returns the page that was selected before the change, -1 if none was selected. """
        return self._oldselection




# ---------------------------------------------------------------------------- #
# Class Simplebook
# ---------------------------------------------------------------------------- #

class Simplebook(wx.Panel):
    """
    The :class:`Simplebook` is a full implementation of the :class:`Notebook`, and designed to be
    a drop-in replacement for :class:`Notebook`. The API functions are similar so one can
    expect the function to behave in the same way.
    """

    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=0, name="Simplebook"):
        """
        Default class constructor.

        :param `parent`: the :class:`Simplebook` parent;
        :param `id`: an identifier for the control: a value of -1 is taken to mean a default;
        :param `pos`: the control position. A value of (-1, -1) indicates a default position,
         chosen by either the windowing system or wxPython, depending on platform;
        :param `size`: the control size. A value of (-1, -1) indicates a default size,
         chosen by either the windowing system or wxPython, depending on platform;
        :param `style`: the underlying :class:`PyPanel` window style;
        :param `name`: the window name.
        """
        super(Simplebook, self).__init__(parent, id, pos, size, style)
        self._bForceSelection = False
        self._nPadding = 6
        self._nFrom = 0
        self._windows = []
        self._customPanel = None
        self._iPreviousActivePage = wx.NOT_FOUND
        self._iActivePage = wx.NOT_FOUND

        attr = self.GetDefaultAttributes()
        self.SetOwnForegroundColour(attr.colFg)
        self.SetOwnBackgroundColour(attr.colBg)
        
        #self._pages = PageContainer(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, style)
        self.Init()


    def Init(self):
        """ Initializes all the class attributes. """

        self._mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self._mainSizer)
        self._mainSizer.Layout()

        # The child panels will inherit this bg colour, so leave it at the default value
        #self.SetBackgroundColour(wx.SystemSettings_GetColour(wx.SYS_COLOUR_APPWORKSPACE))


    def DoGetBestSize(self):
        """
        Gets the size which best suits the window: for a control, it would be the
        minimal size which doesn't truncate the control, for a panel - the same
        size as it would have after a call to `Fit()`.

        :note: Overridden from :class:`PyPanel`.
        """

        if not self._windows:
            # Something is better than nothing... no pages!
            return wx.Size(20, 20)

        maxWidth = maxHeight = 0

        for win in self._windows:
            # Loop over all the windows to get their best size
            width, height = win.GetBestSize()
            maxWidth, maxHeight = max(maxWidth, width), max(maxHeight, height)

        return wx.Size(maxWidth, maxHeight)

    def SetCustomPage(self, panel):
        """
        Sets a custom panel to show when there are no pages left in :class:`Simplebook`.

        :param panel: any subclass of :class:`Window` will do, as long as it is suitable
         to be used as a notebook page. Examples include :class:`Panel`, :class:`ScrolledWindow`,
         and so on.
        """
        if panel is self._customPanel:
            return
        
        self.Freeze()
        if self._customPanel is not None:
            self._mainSizer.Detach(self._customPanel)
            self._customPanel.Show(False)
            self._customPanel.Destroy()
        self._customPanel = panel
        self.ShowCustomPage(True)
        self._mainSizer.Layout()
        self.Thaw()


    def GetCustomPage(self):
        """ Returns a custom panel to show when there are no pages left in :class:`Simplebook`. """
        return self._customPanel


    def ShowCustomPage(self, show=True):
        """ Hides the custom panel which is shown when there are no pages left in :class:`Simplebook`. """

        if self._customPanel is None:
            return
        self.Freeze()
        if show:
            if self.GetPageCount() == 0:
                self._mainSizer.Add(self._customPanel, 1, wx.EXPAND | wx.ALL, 2)
                self._customPanel.Show(True)
        else:
            self._customPanel.Show(False)
            if self._customPanel.GetContainingSizer() is not None:
                self._mainSizer.Detach(self._customPanel)
        self._mainSizer.Layout()
        self.Thaw()

    def GetPreviousSelection(self):
        """ Returns the previous selection. """
        return self._iPreviousActivePage


    def AddPage(self, page, text, select=False):
        """
        Adds a page to the :class:`Simplebook`.

        :param page: specifies the new page;
        :param text: specifies the text for the new page;
        :param select: specifies whether the page should be selected;

        :return: True if successful, False otherwise.
        """

        # sanity check
        if not page:
            return False

        min_size = page.GetMinSize()
        if not min_size.IsFullySpecified():
            page.SetMinSize((20,20))

        self.ShowCustomPage(False)

        # reparent the window to us
        page.Reparent(self)

        selected = select or len(self._windows) == 0
        selection = self._iActivePage
        if selected:
            # Check for selection and send events
            selected = False
            index = len(self._windows)
            event = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CHANGING, self.GetId())
            event.SetSelection(index)
            event.SetOldSelection(selection)
            event.SetEventObject(self)
            if not self.GetEventHandler().ProcessEvent(event) or event.IsAllowed() or len(self._windows) == 0:
                selected = True
        self._windows.append(page)
        self.Freeze()
        # Check if a new selection was made
        if selected:
            if selection >= 0:
                # Remove the window from the main sizer
                self._mainSizer.Detach(self._windows[selection])
                self._windows[selection].Hide()
            self._mainSizer.Add(page, 1, wx.EXPAND)
            self._iPreviousActivePage = selection
            self._iActivePage = index
            # Fire a wxEVT_SIMPLEBOOK_PAGE_CHANGED event
            event.SetEventType(wxEVT_SIMPLEBOOK_PAGE_CHANGED)
            self.GetEventHandler().ProcessEvent(event)
            page.Show()
        else:
            # Hide the page
            page.Hide()
        self.Thaw()
        self._mainSizer.Layout()
        self.Refresh()
        return True

    def InsertPage(self, index, page, text, select=True):
        """
        Inserts a new page at the specified position.

        :param index: specifies the position of the new page;
        :param page: specifies the new page;
        :param text: specifies the text for the new page;
        :param select: specifies whether the page should be selected;

        :return: True if successful, False otherwise.
        """

        # sanity check
        if not page:
            return False

        self.ShowCustomPage(False)

        # reparent the window to us
        page.Reparent(self)

        if not self._windows:

            self.AddPage(page, text, select)
            return True

        # Insert tab
        bSelected = select or not self._windows
        curSel = self._iActivePage

        index = max(0, min(index, len(self._windows)))

        if index <= len(self._windows):

            self._windows.insert(index, page)

        else:

            self._windows.append(page)

        if bSelected:

            bSelected = False

            # Check for selection and send events

            event = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CHANGING, self.GetId())
            event.SetSelection(index)
            event.SetOldSelection(curSel)
            event.SetEventObject(self)

            if not self.GetEventHandler().ProcessEvent(event) or event.IsAllowed() or len(self._windows) == 0:
                bSelected = True

        if index <= curSel:
            curSel = curSel + 1

        self.Freeze()

        # Check if a new selection was made
        if bSelected:

            if curSel >= 0:

                # Remove the window from the main sizer
                self._mainSizer.Detach(self._windows[curSel])
                self._windows[curSel].Hide()

            self._iPreviousActivePage = curSel
            self._iActivePage = index

            # Fire a wxEVT_SIMPLEBOOK_PAGE_CHANGED event
            event.SetEventType(wxEVT_SIMPLEBOOK_PAGE_CHANGED)
            event.SetOldSelection(curSel)  # take account of possible updated index!
            self.GetEventHandler().ProcessEvent(event)

        else:

            # Hide the page
            page.Hide()

        self.Thaw()
        self._mainSizer.Layout()
        self.Refresh()

        return True

    def GetSelection(self):
        return self._iActivePage
    
    def SetSelection(self, page):
        """
        Sets the selection for the given page.

        :param page: an integer specifying the new selected page.

        :note: The call to this function **generate** the page changing events.
        :note: This call can be vetoed
        
        """
        if page < 0 or page >= len(self._windows):
            return

        curSel = self._iActivePage
        if page == curSel:
            return
        
        event = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CHANGING, self.GetId())
        event.SetSelection(page)
        event.SetOldSelection(curSel)
        event.SetEventObject(self)

        if self.GetEventHandler().ProcessEvent(event) and not event.IsAllowed():
            return
        

        # program allows the page change
        #self.Freeze()  # we have some issue here, due to some freeze
        if curSel != wx.NOT_FOUND:

            # Remove the window from the main sizer
            self._mainSizer.Detach(self._windows[curSel])
            self._windows[curSel].Hide()

        # We leave a space of 0 pixel around the window
        self._mainSizer.Add(self._windows[page], 1, wx.EXPAND)

        self._windows[page].Show()
        #self.Thaw()

        self._mainSizer.Layout()
        # there is a real page changing
        self._iPreviousActivePage = curSel
        self._iActivePage = page
        
        event.SetEventType(wxEVT_SIMPLEBOOK_PAGE_CHANGED)
        self.GetEventHandler().ProcessEvent(event)

                
    def ChangeSelection(self, page):
        """
        Sets the selection for the given page.

        :param page: an integer specifying the new selected page.

        :note: The call to this function **does not** generate the page changing events.
        """

        if page < 0 or page >= len(self._windows):
            return

        curSel = self._iActivePage
        if page == curSel:
            return

        # program allows the page change
        self.Freeze()
        if curSel != wx.NOT_FOUND:

            # Remove the window from the main sizer
            self._mainSizer.Detach(self._windows[curSel])
            self._windows[curSel].Hide()

        # We leave a space of 0 pixel around the window
        self._mainSizer.Add(self._windows[page], 1, wx.EXPAND)

        self._windows[page].Show()
        self.Thaw()

        self._mainSizer.Layout()
        # there is a real page changing
        self._iPreviousActivePage = self._iActivePage
        self._iActivePage = page


    def DeletePage(self, index):
        """
        Deletes the specified page, and the associated window.

        :param index: an integer specifying the new selected page.

        :note: The call to this function generates the page changing events.
        """
        if index >= len(self._windows) or index < 0:
            return

        # Fire a closing event
        event = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CLOSING, self.GetId())
        event.SetSelection(index)
        event.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(event)

        # The event handler allows it?
        if not event.IsAllowed():
            return

        self.Freeze()

        # Delete the requested page
        pageRemoved = self._windows[index]

        # If the page is the current window, remove it from the sizer
        # as well
        if index == self._iActivePage:
            self._mainSizer.Detach(pageRemoved)

        # Remove it from the array as well
        self._windows.pop(index)

        # Now we can destroy it in wxWidgets use Destroy instead of delete
        pageRemoved.Destroy()

        self.Thaw()
        #--
        if self._iActivePage == index:
            if index == 0 and len(self._windows) > 0:
                self._iActivePage = 0
            else:
                self._iActivePage = index - 1
            self._iPreviousActivePage = -1
            changePage = True
        else:
            changePage = False
            if self._iActivePage > index:
                self._iActivePage = self._iActivePage - 1
                
        if self._iPreviousActivePage == index:
            self._iPreviousActivePage = -1
        elif self._iPreviousActivePage > index:
            self._iPreviousActivePage = self._iPreviousActivePage -1

        # Notify only if generates page change
        if changePage and self._iActivePage >= 0:

            # Check for selection and send event
            event = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CHANGING, self.GetId())
            event.SetSelection(self._iActivePage)
            event.SetOldSelection(self._iPreviousActivePage)
            event.SetEventObject(self)
            self.GetEventHandler().ProcessEvent(event)

            # Fire a wxEVT_FLATNOTEBOOK_PAGE_CHANGED event
            event.SetEventType(wxEVT_SIMPLEBOOK_PAGE_CHANGED)
            event.SetOldSelection(self._iPreviousActivePage)
            self.GetEventHandler().ProcessEvent(event)
        
        self.ShowCustomPage(True)
        self.Refresh()
        self.Update()

        # Fire a closed event
        closedEvent = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CLOSED, self.GetId())
        closedEvent.SetSelection(index)
        closedEvent.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(closedEvent)


    def DeleteAllPages(self):
        """ Deletes all the pages in the :class:`Simplebook`. """

        if not self._windows:
            return False

        self.Freeze()
        
        sel = self._iActivePage
        if sel != wx.NOT_FOUND and sel < len(self._windows):
            active_page = self._windows[sel]
            self._mainSizer.Detach(active_page)

        for page in self._windows:
            page.Destroy()

        self._windows = []
        self.Thaw()

        self._iActivePage = wx.NOT_FOUND
        self._iPreviousActivePage  = wx.NOT_FOUND
        self.ShowCustomPage(True)
        return True


    def GetCurrentPage(self):
        """ Returns the currently selected notebook page or ``None`` if none is selected. """
        sel = self._iActivePage
        if sel < 0 or sel >= len(self._windows):
            return None
        return self._windows[sel]


    def GetPage(self, index):
        """ Returns the window at the given page position, or ``None``. """

        if index<=0 or index >= len(self._windows):
            return None
        return self._windows[index]


    def GetPageIndex(self, win):
        """
        Returns the index at which the window is found.

        :param win: an instance of :class:`Window`.
        """

        try:
            return self._windows.index(win)
        except:
            return wx.NOT_FOUND


    def AdvanceSelection(self, forward=True):
        """
        Cycles through the tabs.

        :param forward: if ``True``, the selection is advanced in ascending order
         (to the right), otherwise the selection is advanced in descending order.

        :note: The call to this function generates the page changing events.
        """
        if forward:
            self.SetSelection(self._iActivePage + 1)
        else:
            self.SetSelection(self._iActivePage - 1)


    def GetPageCount(self):
        """ Returns the number of pages in the :class:`Simplebook` control. """
        return len(self._windows)


    def GetPageBestSize(self):
        """ Return the page best size. """
        return self.GetClientSize()


    def RemovePage(self, index):
        """
        Deletes the specified page, without deleting the associated window.

        :param index: an integer specifying the page index.
        """

        if index >= len(self._windows) or index < 0:
            return None

        # Fire a closing event
        event = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CLOSING, self.GetId())
        event.SetSelection(index)
        event.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(event)

        # The event handler allows it?
        if not event.IsAllowed():
            return None

        self.Freeze()

        pageRemoved = self._windows[index]

        # If the page is the current window, remove it from the sizer
        # as well
        if index == self._iActivePage:
            self._mainSizer.Detach(pageRemoved)

        # Remove it from the array as well
        self._windows.pop(index)

        self.Thaw()
        #--
        if self._iActivePage == index:
            if index == 0 and len(self._windows) > 0:
                self._iActivePage = 0
            else:
                self._iActivePage = index - 1
            self._iPreviousActivePage = -1
            changePage = True
        else:
            changePage = False
            if self._iActivePage > index:
                self._iActivePage = self._iActivePage - 1
                
        if self._iPreviousActivePage == index:
            self._iPreviousActivePage = -1
        elif self._iPreviousActivePage > index:
            self._iPreviousActivePage = self._iPreviousActivePage -1

        # Notify only if generates page change
        if changePage and self._iActivePage >= 0:

            # Check for selection and send event
            event = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CHANGING, self.GetId())
            event.SetSelection(self._iActivePage)
            event.SetOldSelection(self._iPreviousActivePage)
            event.SetEventObject(self)
            self.GetEventHandler().ProcessEvent(event)

            # Fire a wxEVT_FLATNOTEBOOK_PAGE_CHANGED event
            event.SetEventType(wxEVT_SIMPLEBOOK_PAGE_CHANGED)
            event.SetOldSelection(self._iPreviousActivePage)
            self.GetEventHandler().ProcessEvent(event)
        
        self.ShowCustomPage(True)
        self.Refresh()
        self.Update()

        # Fire a closed event
        closedEvent = SimplebookEvent(wxEVT_SIMPLEBOOK_PAGE_CLOSED, self.GetId())
        closedEvent.SetSelection(index)
        closedEvent.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(closedEvent)
        return pageRemoved



