# -*- coding: utf-8 -*-

from beatle.lib.api import volatile


def restore_position(function):
    """This decorator attempts to reposition window from volatile context"""
    def wrap(self, *args, **kwargs):
        """wrapped calls"""
        value = function(self, *args, **kwargs)  # must be called before reposition for window __init__
        info = volatile.get(type(self))
        if info is not None:
            self.SetRect(info)
        return value
    return wrap


def save_position(function):
    """This decorator stores window position in volatile context"""
    def wrap(self, *args, **kwargs):
        """wrapped calls"""
        volatile.set(type(self), self.GetRect())
        return function(self, *args, **kwargs)
    return wrap
