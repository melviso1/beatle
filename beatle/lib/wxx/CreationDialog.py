# -*- coding: utf-8 -*-

import wx
from beatle.lib.tran import TransactionStack, format_current_transaction_name


class CreationDialog(object):
    """Class decorator for creation dialog. With this decorator
    the decorated method only needs to send the dialog arguments"""
    def __init__(self, dialogType, objectType):
        """"""
        self._dialogType = dialogType
        self._objectType = objectType

    def __call__(self, method):
        """"""

        def wrapped_call(*args, **kwargs):
            """Code dialog creation"""
            args = method(*args, **kwargs)
            if args is False:
                return False
            dialog = self._dialogType(*args)
            rtv = dialog.ShowModal()
            if rtv != wx.ID_OK:
                return False
            kw_or_kwlargs = dialog.get_kwargs()
            # si kw_or_kwlargs es None, no se ha de hacer nada
            products = None
            obj = None
            if kw_or_kwlargs:
                # si kw_or_kwlargs es una lista, se interpreta que se han de crear
                # multiples objetos y se trata de una lista de kwargs
                if type(kw_or_kwlargs) is list:
                    products = [self._objectType(**kwargs) for kwargs in kw_or_kwlargs]
                    projects = list(set(x.project for x in products if hasattr(x,'project')))
                    if  TransactionStack.InTransaction():
                        for project in projects:
                            project.save_state()
                            project.modified = True
                    else:
                        for project in projects:
                            project.modified = True
                else:
                    obj = self._objectType(**kw_or_kwlargs)
                    if obj:
                        # give the possibility of insert object name in transaction label
                        if TransactionStack.InTransaction() and hasattr(obj, '_name'):
                            format_current_transaction_name(obj.name)
                        if getattr(obj, 'project', False):
                            if TransactionStack.InTransaction():
                                obj.project.save_state()
                            obj.project.modified = True
            # post initialization for project templates
            if getattr(dialog, 'post_process', False):
                if type(kw_or_kwlargs) is list:
                    wx.CallAfter(dialog.post_process, products)
                else:
                    wx.CallAfter(dialog.post_process, obj)
            return True
        return wrapped_call


class CreationDialogEx(object):
    """Class decorator for creation dialog with multi language support.
    The dialog and the object type for, is returned by the method"""
    def __init__(self):
        """"""
        super(CreationDialogEx, self).__init__()

    def __call__(self, method):
        """"""

        def wrapped_call(*args, **kwargs):
            """Code dialog creation"""
            args = method(*args, **kwargs)
            self._dialogType = args[0]
            self._objectType = args[1]
            args = args[2:]
            dialog = self._dialogType(*args)
            if dialog.ShowModal() != wx.ID_OK:
                return False
            kw_or_kwlargs = dialog.get_kwargs()
            #si kw_or_kwlargs es None, no se ha de hacer nada
            if kw_or_kwlargs:
                # si kw_or_kwlargs es una lista, se interpreta que se han de crear
                # multiples objetos y se trata de una lista de kwargs
                if type(kw_or_kwlargs) is list:
                    products = [self._objectType(**kwargs) for kwargs in kw_or_kwlargs]
                    projects = list(set(x.project for x in products if hasattr(x,'project')))
                    if  TransactionStack.InTransaction():
                        for project in projects:
                            project.save_state()
                            project.modified = True
                    else:
                        for project in projects:
                            project.modified = True
                else:
                    obj = self._objectType(**kw_or_kwlargs)
                    # give the possibility of insert object name in transaction label
                    if obj and TransactionStack.InTransaction() and hasattr(obj, '_name'):
                        format_current_transaction_name(obj.name)
                    if getattr(obj, 'project', False):
                        if TransactionStack.InTransaction():
                            obj.project.save_state()
                        obj.project.modified = True
            return True
        return wrapped_call
