# -*- coding: utf-8 -*-
"""
Under Windows, variable size image list is not supported.
This little class handles arbitrary size image list
"""
import wx
import six


class ImageList(object):
    """Variable size image list.
    This implementation is mainly due to windows fixed-size image lists.
    In order to simplify and respect implementation, this class is
    mainly a size-mapped collection of imageLists"""

    base_image_list = wx.ImageList

    def __init__(self, width=0, height=0, mask=True, initialCount=1):
        """Create an image list."""
        self._width = width
        self._height = height
        self._mask = mask
        self._initial_count = initialCount  # does nothing
        self._image_list = []  # array of image lists
        self._image_size_map = {}  # map size -> image_list index
        self._image_index_map = []  # map index -> ( image_list index, image index )
        self._count = 0

    def register_image_list(self, size):
        """Get image list by size and create it if doesn't exist. Returns index."""
        hashable = tuple(size)
        index = self._image_size_map.get(hashable)
        if index is None:
            image_list = self.base_image_list(size.GetWidth(), size.GetHeight(), self._mask)
            index = len(self._image_list)
            self._image_list.append(image_list)
            self._image_size_map[hashable] = index
        return index

    def check_index(self, index):
        if type(index) not in six.integer_types:
            raise ValueError('ImageList index must be integer.')
        if self._count <= index or index < 0:
            raise ValueError('ImageList index out of range.')

    def Add(self, *args, **kwargs):
        """Adds a new image or images"""
        element = None
        if len(args) + len(kwargs) > 2:
            raise ValueError('Invalid ImageList::Add number of arguments.')
        if len(args) > 0:
            element = args[0]
            index = self.register_image_list(wx.Size(element.Width, element.Height))
        else:
            if len(kwargs) == 0:
                raise ValueError('Invalid ImageList::Add arguments.')
            else:
                if 'bitmap' in kwargs:
                    element = kwargs['bitmap']
                elif 'icon' in kwargs:
                    element = kwargs['icon']
                else:
                    raise ValueError('Invalid ImageList::Add unknown keyword argument {}'.format(kwargs.keys()[0]))
                index = self.register_image_list(wx.Size(element.Width, element.Height))
        image_list = self._image_list[index]
        self._image_index_map.append((index, image_list.ImageCount))
        image_list.Add(*args, **kwargs)
        self._count += 1
        return self._count - 1

    def Create(self, width=0, height=0, mask=True, initialCount=1):
        """Initializes the list"""
        self._width = width
        self._height = height
        self._mask = mask
        self._initial_count = initialCount  # does nothing
        self._image_list = []  # array of image lists
        self._image_size_map = {}  # map size -> image_list index
        self._image_index_map = []  # map index -> ( image_list index, image index )
        self._count = 0
        return True

    def Draw(self, index, dc, x, y, flags=wx.IMAGE_LIST_NORMAL, solidBackground=False):
        """This is a very primitive implementation"""
        self.check_index(index)
        image_list_index, image_index = self._image_index_map[index]
        self._image_list[image_list_index].Draw(image_index, dc, x, y, flags, solidBackground)
        return True

    def GetBitmap(self, index):
        self.check_index(index)
        image_list_index, image_index = self._image_index_map[index]
        return self._image_list[image_list_index].GetBitmap(image_index)

    def GetIcon(self, index):
        self.check_index(index)
        image_list_index, image_index = self._image_index_map[index]
        return self._image_list[image_list_index].GetIcon(image_index)

    def GetImageCount(self):
        return self._count

    @property
    def ImageCount(self):
        return self.GetImageCount()

    def GetSize(self, *args, **kwargs):
        if len(args) == 1:
            index = args[0]
        elif 'index' in kwargs:
            index = kwargs['index']
        else:
            return wx.Size(self._width, self._height)
        self.check_index(index)
        image_list_index, image_index = self._image_index_map[index]
        if len(args) == 1:
            args = (image_index, *args[1:])
        elif 'index' in kwargs:
           kwargs['index'] =  image_index
        return self._image_list[image_list_index].GetSize(*args, **kwargs)

    @property
    def Size(self):
        return self.GetSize()

    def Remove(self, index):
        """Removes the image at the given position"""
        self.check_index(index)
        image_list_index, image_index = self._image_index_map[index]
        del self._image_index_map[index]
        self._image_list[image_list_index].Remove(image_index)
        for item in range(index, len(self._image_index_map)):
            if self._image_index_map[item][0] == image_list_index:
                self._image_index_map[item] = (image_list_index, self._image_index_map[item][1] - 1)
        self._count -= 1
        return True

    def RemoveAll(self):
        """Removes all the images in the list."""
        return self.Create(self._width, self._height, self._mask, self._initial_count)

    def Replace(self, *args, **kwargs):
        """Replaces the existing image with the new image."""
        # TODO : In Windows, imageLists are fixed size, so the current
        # TODO - replacement strategy don't works if new image has different size.
        if len(args) > 0:
            index = args[0]
        elif 'index' in kwargs:
            index = kwargs['index']
        else:
            raise ValueError('Missing ImageList::Replace bitmap index.')
        self.check_index(index)
        image_list_index, image_index = self._image_index_map[index]
        if len(args) > 0:
            args = (image_index, *args[1:])
        else:
            kwargs['index'] = image_index
        return self._image_list[image_list_index].Replace(*args, **kwargs)
