# -*- coding: utf-8 -*-

import wx
from  beatle.lib.tran import TransactionStack, format_current_transaction_name


class EditionDialog(object):
    """Class decorator for edition dialog. With this decorator
    the decorated method only needs to send the dialog arguments"""
    def __init__(self, dialogType):
        """"""
        self._dialogType = dialogType

    def __call__(self, method):
        """"""
        def wrapped_call(*args, **kwargs):
            """Code dialog edition"""
            wnd, obj = method(*args, **kwargs)
            dialog = self._dialogType(wnd, obj.parent)
            dialog.set_attributes(obj)
            if dialog.ShowModal() != wx.ID_OK:
                return False
            obj.save_state()
            dialog.copy_attributes(obj)
            if obj.project:
                obj.project.modified = True
            if obj and TransactionStack.InTransaction() and hasattr(obj, '_name'):
                format_current_transaction_name(obj.name)
            return True
        return wrapped_call