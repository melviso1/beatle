# -*- coding: utf-8 -*-
# cython: language_level=3

import wx
from wx import aui as base
from beatle.lib.bpy.wx.aui import StdToolBarArt


class AuiNotebook(base.AuiNotebook):
    """AuiNotebook override"""

    def __init__(self, *args, **kwargs):
        """Initialize notebook"""
        super(AuiNotebook, self).__init__(*args, **kwargs)
        dbc = wx.TheColourDatabase
        if StdToolBarArt is False:
            tab_art = wx.aui.AuiSimpleTabArt().Clone()
            tab_art.SetActiveColour(dbc.Find('LIGHT BLUE'))
            self._text_colour =  dbc.Find('WHITE')
            tab_art.SetColour(dbc.Find('LIGHT GREY'))
            self.SetArtProvider(tab_art)
        else:
            tab_art = StdToolBarArt().Clone()  # this is not reference counted!
            white = dbc.Find('WHITE')
            gray = dbc.Find('LIGHT GREY')
            indian_red = dbc.Find('INDIAN RED')
            tab_art.set_active_tab_colour(indian_red)
            tab_art.set_active_tab_text_colour(gray)
            tab_art.set_inactive_tab_text_colour(gray)
            tab_art.AssignTo(self)

    def AddPage(self, *args, **kwargs):
        """Interface AddPage"""
        # args[0].SetForegroundColour(self._text_colour)
        return super(AuiNotebook, self).AddPage(*args, **kwargs)


