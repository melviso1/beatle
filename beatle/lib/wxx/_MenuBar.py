"""
    wxx.Menu
    This class improves the creation of menu bar.
"""
import six
from ._Menu import Menu
from beatle.lib import wxx
from ._RendererDarkTheme import RendererDarkTheme
from ._MenuItem import MenuItem
from beatle.lib.wxx.agw.fmresources import *
from beatle.lib.wxx.agw._FlatMenu import FMRendererMgr, GetAccelIndex, MenuEntryInfo, \
    StatusBarTimer, _DELAY, FMCustomizeDlg

_ = wx.GetTranslation

wxEVT_FLAT_MENU_DISMISSED = wx.NewEventType()
wxEVT_FLAT_MENU_SELECTED = wx.wxEVT_COMMAND_MENU_SELECTED
wxEVT_FLAT_MENU_ITEM_MOUSE_OVER = wx.NewEventType()
wxEVT_FLAT_MENU_ITEM_MOUSE_OUT = wx.NewEventType()

EVT_FLAT_MENU_DISMISSED = wx.PyEventBinder(wxEVT_FLAT_MENU_DISMISSED, 1)
""" Used internally. """
EVT_FLAT_MENU_SELECTED = wx.PyEventBinder(wxEVT_FLAT_MENU_SELECTED, 2)
""" Fires the wx.EVT_MENU event for :class:`FlatMenu`. """
EVT_FLAT_MENU_RANGE = wx.PyEventBinder(wxEVT_FLAT_MENU_SELECTED, 2)
""" Fires the wx.EVT_MENU event for a series of :class:`FlatMenu`. """
EVT_FLAT_MENU_ITEM_MOUSE_OUT = wx.PyEventBinder(wxEVT_FLAT_MENU_ITEM_MOUSE_OUT, 1)
""" Fires an event when the mouse leaves a :class:`FlatMenuItem`. """
EVT_FLAT_MENU_ITEM_MOUSE_OVER = wx.PyEventBinder(wxEVT_FLAT_MENU_ITEM_MOUSE_OVER, 1)


class MenuBar(wx.Panel):
    """
    Implements the generic owner-drawn menu bar for :class:`MenuBar`.
    This is from the nice Andrea Gavana work with minor fixes.
    """

    def __init__(self, parent, id=wx.ID_ANY, iconSize=SmallIcons,
                 spacer=8, options=0):
        """
        Default class constructor.

        :param `parent`: the menu bar parent, must not be ``None``;
        :param integer `id`: the window identifier. If ``wx.ID_ANY``, will automatically create an identifier;
        :param integer `iconSize`: size of the icons in the toolbar. This can be one of the
         following values (in pixels):

         ==================== ======= =============================
         `iconSize` Bit        Value  Description
         ==================== ======= =============================
         ``LargeIcons``            32 Use large 32x32 icons
         ``SmallIcons``            16 Use standard 16x16 icons
         ==================== ======= =============================

        :param integer `spacer`: the space between the menu bar text and the menu bar border;
        :param integer `options`: a combination of the following bits:

         ========================= ========= =============================
         `options` Bit             Hex Value  Description
         ========================= ========= =============================
         ``FM_OPT_IS_LCD``               0x1 Use this style if your computer uses a LCD screen
         ``FM_OPT_MINIBAR``              0x2 Use this if you plan to use toolbar only
         ``FM_OPT_SHOW_CUSTOMIZE``       0x4 Show "customize link" in more menus, you will need to write your own handler. See demo.
         ``FM_OPT_SHOW_TOOLBAR``         0x8 Set this option is you are planing to use the toolbar
         ========================= ========= =============================

        """

        self._rendererMgr = FMRendererMgr()
        self._parent = parent
        self._curretHiliteItem = -1

        self._items = []
        self._dropDownButtonArea = wx.Rect()
        self._tbIconSize = iconSize
        self._tbButtons = []
        self._interval = 20      # 20 milliseconds
        self._showTooltip = -1

        self._haveTip = False
        self._statusTimer = None
        self._spacer = SPACER
        self._margin = spacer
        self._toolbarSpacer = TOOLBAR_SPACER
        self._toolbarMargin = TOOLBAR_MARGIN

        self._showToolbar = options & FM_OPT_SHOW_TOOLBAR
        self._showCustomize = options & FM_OPT_SHOW_CUSTOMIZE
        self._isLCD = options & FM_OPT_IS_LCD
        self._isMinibar = options & FM_OPT_MINIBAR
        self._options = options

        self._dropDownButtonState = ControlNormal
        self._moreMenu = None
        self._dlg = None
        self._tbMenu = None
        self._moreMenuBgBmp = None
        self._lastRadioGroup = 0
        self._mgr = None

        self._barHeight = 0
        self._menuBarHeight = 0
        self.SetBarHeight()
        self._oldCur = None
        super(MenuBar, self).__init__(parent, id, size=(-1, self._barHeight), style=wx.WANTS_CHARS )

        self.Bind(wx.EVT_ERASE_BACKGROUND, self.OnEraseBackground)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.Bind(EVT_FLAT_MENU_DISMISSED, self.OnMenuDismissed)
        self.Bind(wx.EVT_LEAVE_WINDOW, self.OnLeaveMenuBar)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnLeftDown)
        self.Bind(wx.EVT_LEFT_DCLICK, self.OnLeftDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnLeftUp)
        self.Bind(wx.EVT_IDLE, self.OnIdle)

        if "__WXGTK__" in wx.Platform:
            self.Bind(wx.EVT_LEAVE_WINDOW, self.OnLeaveWindow)

        self.SetFocus()

        # start the stop watch
        self._watch = wx.StopWatch()
        self._watch.Start()
        # Temporally set dark theme
        render = self.GetRendererManager()
        # render.SetTheme(render.AddRenderer(RendererDarkTheme()))
        self.Bind(wx.EVT_SET_CURSOR, self.OnSetCursor)

    def OnSetCursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_ARROW))

    def OnMouseEnterWindow(self, event):
        """
        Handles the ``wx.EVT_ENTER_WINDOW`` event for :class:`FlatMenu`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """
        self._oldCur = self.GetCursor()
        self.SetCursor(wx.Cursor(wx.CURSOR_ARROW))
        event.Skip()

    def OnMouseLeaveWindow(self, event):
        """
        Handles the ``wx.EVT_ENTER_WINDOW`` event for :class:`FlatMenu`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """
        if self._oldCur is not None:
            self.SetCursor(self._oldCur)
        event.Skip()

    def SetLabel(self, windowid, title):
        """This method mimics the SetLabel from standard menubar.
        mv: There is an annoying recovery with standard menu bar,
        since this class don't use menu identifiers at all"""
        pos = next((n for n in  range(len(self._items)) if self._items[n].GetMenu().GetId() == windowid), None)
        if pos is not None:
            menu = self._items[pos].GetMenu()
            menu._menuBarFullTitle = title
            position, label = GetAccelIndex(title)
            menu._menuBarLabelOnly = label

            self._items[pos] = MenuEntryInfo(title, menu)
            self.UpdateAcceleratorTable()
            self.ClearBitmaps(pos)
            self.Refresh()

    def Append(self, menu, title):
        """
        Adds the item to the end of the menu bar.

        :param `menu`: the menu to which we are appending a new item, an instance of :class:`FlatMenu`;
        :param string `title`: the menu item label, must not be empty.

        :see: :meth:`~FlatMenuBar.Insert`.
        """
        menu._menuBarFullTitle = title
        position, label = GetAccelIndex(title)
        menu._menuBarLabelOnly = label
        return self.Insert(len(self._items), menu, title)

    def OnIdle(self, event):
        """
        Handles the ``wx.EVT_IDLE`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`IdleEvent` event to be processed.
        """
        refresh = False
        if self._watch.Time() > self._interval:
            # it is time to process UpdateUIEvents
            for but in self._tbButtons:
                event = wx.UpdateUIEvent(but._tbItem.GetId())
                event.Enable(but._tbItem.IsEnabled())
                event.SetText(but._tbItem.GetLabel())
                event.SetEventObject(self)
                self.GetEventHandler().ProcessEvent(event)
                if but._tbItem.GetLabel() != event.GetText() or but._tbItem.IsEnabled() != event.GetEnabled():
                    refresh = True
                but._tbItem.SetLabel(event.GetText())
                but._tbItem.Enable(event.GetEnabled())

            self._watch.Start() # Reset the timer

        # we need to update the menu bar
        if refresh:
            self.Refresh()

    def SetBarHeight(self):
        """ Recalculates the :class:`FlatMenuBar` height when its settings change. """
        mem_dc = wx.MemoryDC()
        mem_dc.SelectObject(wx.Bitmap(1, 1))
        dummy, self._barHeight = mem_dc.GetTextExtent("Tp")
        mem_dc.SelectObject(wx.NullBitmap)

        if not self._isMinibar:
            self._barHeight += 2*self._margin # The menu bar margin
        else:
            self._barHeight  = 0

        self._menuBarHeight = self._barHeight

        if self._showToolbar :
            # add the toolbar height to the menubar height
            self._barHeight += self._tbIconSize + 2*self._toolbarMargin

        if self._mgr is None:
            return
        pn = self._mgr.GetPane("flat_menu_bar")
        pn.MinSize(wx.Size(-1, self._barHeight))
        self._mgr.Update()
        self.Refresh()

    def SetOptions(self, options):
        """
        Sets the :class:`FlatMenuBar` options, whether to show a toolbar, to use LCD screen settings etc...

        :param integer `options`: a combination of the following bits:

         ========================= ========= =============================
         `options` Bit             Hex Value  Description
         ========================= ========= =============================
         ``FM_OPT_IS_LCD``               0x1 Use this style if your computer uses a LCD screen
         ``FM_OPT_MINIBAR``              0x2 Use this if you plan to use toolbar only
         ``FM_OPT_SHOW_CUSTOMIZE``       0x4 Show "customize link" in more menus, you will need to write your own handler. See demo.
         ``FM_OPT_SHOW_TOOLBAR``         0x8 Set this option is you are planing to use the toolbar
         ========================= ========= =============================

        """

        self._options = options
        self._showToolbar = options & FM_OPT_SHOW_TOOLBAR
        self._showCustomize = options & FM_OPT_SHOW_CUSTOMIZE
        self._isLCD = options & FM_OPT_IS_LCD
        self._isMinibar = options & FM_OPT_MINIBAR
        self.SetBarHeight()
        self.Refresh()
        self.Update()

    def GetOptions(self):
        """
        Returns the :class:`FlatMenuBar` options, whether to show a toolbar, to use LCD screen settings etc...

        :see: :meth:`~FlatMenuBar.SetOptions` for a list of valid options.
        """
        return self._options


    def GetRendererManager(self):
        """
        Returns the :class:`FlatMenuBar` renderer manager.
        """
        return self._rendererMgr


    def GetRenderer(self):
        """
        Returns the renderer associated with this instance.
        """
        return self._rendererMgr.GetRenderer()


    def UpdateItem(self, item):
        """
        An item was modified. This function is called by :class:`FlatMenu` in case
        an item was modified directly and not via a :class:`UpdateUIEvent` event.

        :param `item`: an instance of :class:`FlatMenu`.
        """
        #if not self._showToolbar:
        #    return

        # search for a tool bar with id
        refresh = False
        for but in self._tbButtons:
            if but._tbItem.GetId() == item.GetId():
                if but._tbItem.IsEnabled() != item.IsEnabled():
                    refresh = True

                but._tbItem.Enable(item.IsEnabled())
                break
        if refresh:
            self.Refresh()


    def OnPaint(self, event):
        """
        Handles the ``wx.EVT_PAINT`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`PaintEvent` event to be processed.
        """
        # on GTK, dont use the bitmap for drawing,
        # draw directly on the DC

        if "__WXGTK__" in wx.Platform and not self._isLCD:
            self.ClearBitmaps(0)
        dc = wx.BufferedPaintDC(self)
        self.GetRenderer().DrawMenuBar(self, dc)


    def DrawToolbar(self, dc, rect):
        """
        Draws the toolbar (if present).

        :param `dc`: an instance of :class:`wx.DC`;
        :param `rect`: the toolbar client rectangle, an instance of :class:`wx.Rect`.
        """
        highlight_width = self._tbIconSize + self._toolbarSpacer
        highlight_height = self._tbIconSize + self._toolbarMargin

        xx = rect.x + self._toolbarMargin
        #yy = rect.y #+ self._toolbarMargin #+ (rect.height - height)/2
        # by default set all toolbar items as invisible
        for but in self._tbButtons:
            but._visible = False

        counter = 0
        # Get all the toolbar items
        for i in range(len(self._tbButtons)):

            xx += self._toolbarSpacer

            tbItem = self._tbButtons[i]._tbItem
            # the button width depends on its type
            if tbItem.IsSeparator():
                hightlight_width = SEPARATOR_WIDTH
            elif tbItem.IsCustomControl():
                control = tbItem.GetCustomControl()
                hightlight_width = control.GetSize().x + self._toolbarSpacer
            else:
                hightlight_width = self._tbIconSize + self._toolbarSpacer   # normal bitmap's width

            # can we keep drawing?
            if xx + highlight_width >= rect.width:
                break

            counter += 1

            # mark this item as visible
            self._tbButtons[i]._visible = True

            bmp = wx.NullBitmap
            #------------------------------------------
            # special handling for separator
            #------------------------------------------
            if tbItem.IsSeparator():

                # draw the separator
                buttonRect = wx.Rect(xx, rect.y+1, SEPARATOR_WIDTH, rect.height-2)
                self.GetRenderer().DrawToolbarSeparator(dc, buttonRect)
                xx += buttonRect.width
                self._tbButtons[i]._rect = buttonRect
                continue

            elif tbItem.IsCustomControl():
                control = tbItem.GetCustomControl()
                ctrlSize = control.GetSize()
                ctrlPos = wx.Point(xx, rect.y + (rect.height - ctrlSize.y)/2)
                if control.GetPosition() != ctrlPos:
                    control.SetPosition(ctrlPos)
                if not control.IsShown():
                    control.Show()
                buttonRect = wx.Rect(ctrlPos, ctrlSize)
                xx += buttonRect.width
                self._tbButtons[i]._rect = buttonRect
                continue
            else:
                if tbItem.IsEnabled():
                    bmp = tbItem.GetBitmap()
                else:
                    bmp = tbItem.GetDisabledBitmap()

            # Draw the toolbar image
            if bmp.IsOk():
                x = xx - self._toolbarSpacer/2
                #y = rect.y + (rect.height - bmp.GetHeight())/2 - 1
                y = rect.y + self._toolbarMargin/2
                buttonRect = wx.Rect(x, y, highlight_width, highlight_height)
                if i < len(self._tbButtons) and i >= 0:
                    if self._tbButtons[i]._tbItem.IsSelected():
                        tmpState = ControlPressed
                    else:
                        tmpState = ControlFocus
                    if self._tbButtons[i]._state == ControlFocus or self._tbButtons[i]._tbItem.IsSelected():
                        self.GetRenderer().DrawMenuBarButton(dc, buttonRect, tmpState) # TODO DrawToolbarButton? With separate toolbar colors
                    else:
                        self._tbButtons[i]._state = ControlNormal

                imgx = buttonRect.x + (buttonRect.width - bmp.GetWidth())/2
                imgy = buttonRect.y + (buttonRect.height - bmp.GetHeight())/2

                if self._tbButtons[i]._state == ControlFocus and not self._tbButtons[i]._tbItem.IsSelected():

                    # in case we the button is in focus, place it
                    # once pixle up and left
                    # place a dark image under the original image to provide it
                    # with some shadow
                    # shadow = ConvertToMonochrome(bmp)
                    # dc.DrawBitmap(shadow, imgx, imgy, True)

                    imgx -= 1
                    imgy -= 1

                dc.DrawBitmap(bmp, imgx, imgy, True)
                xx += buttonRect.width

                self._tbButtons[i]._rect = buttonRect
                #Edited by P.Kort

                if self._showTooltip == -1:
                    self.RemoveHelp()
                else:
                    try:
                        self.DoGiveHelp(self._tbButtons[self._showTooltip]._tbItem)
                    except:
                        if _debug:
                            print("FlatMenu.py; fn : DrawToolbar; Can't create Tooltip ")
                        pass

        for j in range(counter, len(self._tbButtons)):
            if self._tbButtons[j]._tbItem.IsCustomControl():
                control = self._tbButtons[j]._tbItem.GetCustomControl()
                control.Hide()

    def GetMoreMenuButtonRect(self):
        """ Returns a rectangle region, as an instance of :class:`wx.Rect`, surrounding the menu button. """

        clientRect = self.GetClientRect()
        rect = wx.Rect(*clientRect)
        rect.SetWidth(DROP_DOWN_ARROW_WIDTH)
        if clientRect.GetWidth() + rect.GetX() >= DROP_DOWN_ARROW_WIDTH + 3:
            rect.SetX(clientRect.GetWidth() + rect.GetX() - DROP_DOWN_ARROW_WIDTH - 3)
        else:
            rect.SetX(0)
        rect.SetY(2)
        if rect.GetHeight() > self._spacer:
            rect.SetHeight(rect.GetHeight() - self._spacer)

        return rect

    def DrawMoreButton(self, dc, state):
        """
        Draws 'more' button to the right side of the menu bar.

        :param `dc`: an instance of :class:`wx.DC`;
        :param integer `state`: the 'more' button state.

        :see: :meth:`wx.MenuEntryInfo.SetState() <MenuEntryInfo.SetState>` for a list of valid menu states.
        """

        if (not self._showCustomize) and self.GetInvisibleMenuItemCount() < 1 and  self.GetInvisibleToolbarItemCount() < 1:
            return

        # Draw a drop down menu at the right position of the menu bar
        # we use xpm file with 16x16 size, another 4 pixels we take as spacer
        # from the right side of the frame, this will create a DROP_DOWN_ARROW_WIDTH  pixels width
        # of unwanted zone on the right side

        rect = self.GetMoreMenuButtonRect()

        # Draw the bitmap
        if state != ControlNormal:
            # Draw background according to state
            self.GetRenderer().DrawButton(dc, rect, state)
        else:
            # Delete current image
            if self._moreMenuBgBmp.IsOk():
                dc.DrawBitmap(self._moreMenuBgBmp, rect.x, rect.y, True)

        dropArrowBmp = self.GetRenderer()._bitmaps["arrow_down"]

        # Calc the image coordinates
        xx = rect.x + (DROP_DOWN_ARROW_WIDTH - dropArrowBmp.GetWidth())//2
        yy = rect.y + (rect.height - dropArrowBmp.GetHeight())//2

        dc.DrawBitmap(dropArrowBmp, xx, yy + self._spacer, True)
        self._dropDownButtonState = state

    def HitTest(self, pt):
        """
        HitTest method for :class:`FlatMenuBar`.

        :param `pt`: an instance of :class:`wx.Point`, specifying the hit test position.

        :return: A tuple representing one of the following combinations:

         ========================= ==================================================
         Return Tuple              Description
         ========================= ==================================================
         (-1, 0)                   The :meth:`~FlatMenuBar.HitTest` method didn't find any item with the specified input point `pt` (``NOWHERE`` = 0)
         (`integer`, 1)            A menu item has been hit, its position specified by the tuple item `integer` (``MenuItem`` = 1)
         (`integer`, 2)            A toolbar item has ben hit, its position specified by the tuple item `integer` (``ToolbarItem`` = 2)
         (-1, 3)                   The drop-down area button has been hit (``DROPDOWNARROWBUTTON`` = 3)
         ========================= ==================================================

        """

        if self._dropDownButtonArea.Contains(pt):
            return -1, DROPDOWNARROWBUTTON

        for ii, item in enumerate(self._items):
            if item.GetRect().Contains(pt):
                return ii, MENUITEM

        # check for tool bar items
        if self._showToolbar:
            for ii, but in enumerate(self._tbButtons):
                if but._rect.Contains(pt):
                    # locate the corresponded menu item
                    enabled  = but._tbItem.IsEnabled()
                    separator = but._tbItem.IsSeparator()
                    visible  = but._visible
                    if enabled and not separator and visible:
                        self._showTooltip = ii
                        return ii, TOOLBARITEM

        self._showTooltip = -1
        return -1, NOWHERE

    def FindMenuItem(self, id):
        """
        Finds the menu item object associated with the given menu item identifier.

        :param integer `id`: the identifier for the sought :class:`FlatMenuItem`.

        :return: The found menu item object, or ``None`` if one was not found.
        """
        for item in self._items:
            mi = item.GetMenu().FindItem(id)
            if mi:
                return mi
        return None

    def OnSize(self, event):
        """
        Handles the ``wx.EVT_SIZE`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`wx.SizeEvent` event to be processed.
        """

        self.ClearBitmaps(0)
        self.Refresh()

    def OnEraseBackground(self, event):
        """
        Handles the ``wx.EVT_ERASE_BACKGROUND`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`EraseEvent` event to be processed.

        :note: This method is intentionally empty to reduce flicker.
        """
        pass

    def ShowCustomize(self, show=True):
        """
        Shows/hides the drop-down arrow which allows customization of :class:`FlatMenu`.

        :param bool `show`: ``True`` to show the customize menu, ``False`` to hide it.
        """
        if self._showCustomize == show:
            return
        self._showCustomize = show
        self.Refresh()

    def SetMargin(self, margin):
        """
        Sets the margin above and below the menu bar text.

        :param integer `margin`: height in pixels of the margin.
        """

        self._margin = margin

    def SetSpacing(self, spacer):
        """
        Sets the spacing between the menubar items.

        :param integer `spacer`: number of pixels between each menu item.
        """

        self._spacer = spacer

    def SetToolbarMargin(self, margin):
        """
        Sets the margin around the toolbar.

        :param integer `margin`: width in pixels of the margin around the tools in the toolbar.
        """

        self._toolbarMargin = margin

    def SetToolbarSpacing(self, spacer):
        """
        Sets the spacing between the toolbar tools.

        :param integer `spacer`: number of pixels between each tool in the toolbar.
        """

        self._toolbarSpacer = spacer

    def SetLCDMonitor(self, lcd=True):
        """
        Sets whether the PC monitor is an LCD or not.

        :param bool `lcd`: ``True`` to use the settings appropriate for a LCD monitor,
         ``False`` otherwise.
        """

        if self._isLCD == lcd:
            return

        self._isLCD = lcd
        self.Refresh()

    def ProcessMouseMoveFromMenu(self, pt):
        """
        This function is called from child menus, this allow a child menu to
        pass the mouse movement event to the menu bar.

        :param `pt`: an instance of :class:`wx.Point`.
        """

        idx, where = self.HitTest(pt)
        if where == MENUITEM:
            self.ActivateMenu(self._items[idx])

    def DoMouseMove(self, pt, leftIsDown):
        """
        Handles mouse move event.

        :param `pt`: an instance of :class:`wx.Point`;
        :param bool `leftIsDown`: ``True`` is the left mouse button is down, ``False`` otherwise.
        """

        # Reset items state
        for item in self._items:
            item.SetState(ControlNormal)

        idx, where = self.HitTest(pt)

        if where == DROPDOWNARROWBUTTON:
            self.RemoveHelp()
            if self._dropDownButtonState != ControlFocus and not leftIsDown:
                dc = wx.ClientDC(self)
                self.DrawMoreButton(dc, ControlFocus)

        elif where == MENUITEM:
            self._dropDownButtonState = ControlNormal
            # On Item
            self._items[idx].SetState(ControlFocus)

            # If this item is already selected, dont draw it again
            if self._curretHiliteItem == idx:
                return

            self._curretHiliteItem = idx
            if self._showToolbar:

                # mark all toolbar items as non-hilited
                for but in self._tbButtons:
                    but._state = ControlNormal

            self.Refresh()

        elif where == TOOLBARITEM:

            if self._showToolbar:
                if idx < len(self._tbButtons) and idx >= 0:
                    if self._tbButtons[idx]._state == ControlFocus:
                        return

                    # we need to refresh the toolbar
                    active = self.GetActiveToolbarItem()
                    if active != wx.NOT_FOUND:
                        self._tbButtons[active]._state = ControlNormal

                    for but in self._tbButtons:
                        but._state = ControlNormal

                    self._tbButtons[idx]._state = ControlFocus
                    self.DoGiveHelp(self._tbButtons[idx]._tbItem)
                    self.Refresh()

        elif where == NOWHERE:

            refresh = False
            self.RemoveHelp()

            if self._dropDownButtonState != ControlNormal:
                refresh = True
                self._dropDownButtonState = ControlNormal

            if self._showToolbar:
                tbActiveItem = self.GetActiveToolbarItem()
                if tbActiveItem != wx.NOT_FOUND:
                    self._tbButtons[tbActiveItem]._state = ControlNormal
                    refresh = True

            if self._curretHiliteItem != -1:

                self._items[self._curretHiliteItem].SetState(ControlNormal)
                self._curretHiliteItem = -1
                self.Refresh()

            if refresh:
                self.Refresh()

    def OnMouseMove(self, event):
        """
        Handles the ``wx.EVT_MOTION`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """
        pt = event.GetPosition()
        self.DoMouseMove(pt, event.LeftIsDown())

    def OnLeaveMenuBar(self, event):
        """
        Handles the ``wx.EVT_LEAVE_WINDOW`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`MouseEvent` event to be processed.

        :note: This method is for MSW only.
        """
        pt = event.GetPosition()
        self.DoMouseMove(pt, event.LeftIsDown())

    def ResetToolbarItems(self):
        """ Used internally. """

        for but in self._tbButtons:
            but._state = ControlNormal

    def GetActiveToolbarItem(self):
        """ Returns the active toolbar item. """

        for but in self._tbButtons:

            if but._state == ControlFocus or but._state == ControlPressed:
                return self._tbButtons.index(but)

        return wx.NOT_FOUND

    def GetBackgroundColour(self):
        """ Returns the menu bar background colour. """

        return self.GetRenderer().menuBarFaceColour

    def SetBackgroundColour(self, colour):
        """
        Sets the menu bar background colour.

        :param `colour`: a valid :class:`wx.Colour`.
        """

        self.GetRenderer().menuBarFaceColour = colour

    def SetForegroundColour(self, colour):
        setattr(self.GetRenderer(), 'menuTextColour', colour)

    def OnLeaveWindow(self, event):
        """
        Handles the ``wx.EVT_LEAVE_WINDOW`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`MouseEvent` event to be processed.

        :note: This method is for GTK only.
        """

        self._curretHiliteItem = -1
        self._dropDownButtonState = ControlNormal

        # Reset items state
        for item in self._items:
            item.SetState(ControlNormal)

        for but in self._tbButtons:
            but._state = ControlNormal

        self.Refresh()

    def OnMenuDismissed(self, event):
        """
        Handles the ``EVT_FLAT_MENU_DISMISSED`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`FlatMenuEvent` event to be processed.
        """

        pt = wx.GetMousePosition()
        pt = self.ScreenToClient(pt)

        idx, where = self.HitTest(pt)
        self.RemoveHelp()

        if where not in [MENUITEM, DROPDOWNARROWBUTTON]:
            self._dropDownButtonState = ControlNormal
            self._curretHiliteItem = -1
            for item in self._items:
                item.SetState(ControlNormal)

            self.Refresh()

    def OnLeftDown(self, event):
        """
        Handles the ``wx.EVT_LEFT_DOWN`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """
        from beatle.lib.api import context
        from beatle.lib.api import volatile

        volatile.set('focus', context.get_frame().FindFocus())

        pt = event.GetPosition()
        idx, where = self.HitTest(pt)

        if where == DROPDOWNARROWBUTTON:
            dc = wx.ClientDC(self)
            self.DrawMoreButton(dc, ControlPressed)
            self.PopupMoreMenu()

        elif where == MENUITEM:
            # Position the menu, the GetPosition() return the coords
            # of the button relative to its parent, we need to translate
            # them into the screen coords
            self.ActivateMenu(self._items[idx])

        elif where == TOOLBARITEM:
            redrawAll = False
            item = self._tbButtons[idx]._tbItem
            # try to toggle if its a check item:
            item.Toggle()
            # switch is if its a unselected radio item
            if not item.IsSelected() and item.IsRadioItem():
                group = item.GetGroup()
                for i in range(len(self._tbButtons)):
                    if self._tbButtons[i]._tbItem.GetGroup() == group and \
                      i != idx and self._tbButtons[i]._tbItem.IsSelected():
                        self._tbButtons[i]._state = ControlNormal
                        self._tbButtons[i]._tbItem.Select(False)
                        redrawAll = True
                item.Select(True)
            # Over a toolbar item
            if redrawAll:
                self.Refresh()
                if "__WXMSW__" in wx.Platform:
                    dc = wx.BufferedDC(wx.ClientDC(self))
                else:
                    dc = wx.ClientDC(self)
            else:
                dc = wx.ClientDC(self)
                self.DrawToolbarItem(dc, idx, ControlPressed)

            # TODO:: Do the action specified in this button
            self.DoToolbarAction(idx)

    def OnLeftUp(self, event):
        """
        Handles the ``wx.EVT_LEFT_UP`` event for :class:`FlatMenuBar`.

        :param `event`: a :class:`MouseEvent` event to be processed.
        """

        pt = event.GetPosition()
        idx, where = self.HitTest(pt)

        if where == TOOLBARITEM:
            # Over a toolbar item
            dc = wx.ClientDC(self)
            self.DrawToolbarItem(dc, idx, ControlFocus)

    def DrawToolbarItem(self, dc, idx, state):
        """
        Draws a toolbar item button.

        :param `dc`: an instance of :class:`wx.DC`;
        :param integer `idx`: the tool index in the toolbar;
        :param integer `state`: the button state.

        :see: :meth:`wx.MenuEntryInfo.SetState() <MenuEntryInfo.SetState>` for a list of valid menu states.
        """

        if idx >= len(self._tbButtons) or idx < 0:
            return

        if self._tbButtons[idx]._tbItem.IsSelected():
            state = ControlPressed
        rect = self._tbButtons[idx]._rect
        self.GetRenderer().DrawButton(dc, rect, state)

        # draw the bitmap over the highlight
        buttonRect = wx.Rect(*rect)
        x = rect.x + (buttonRect.width - self._tbButtons[idx]._tbItem.GetBitmap().GetWidth())/2
        y = rect.y + (buttonRect.height - self._tbButtons[idx]._tbItem.GetBitmap().GetHeight())/2

        if state == ControlFocus:

            # place a dark image under the original image to provide it
            # with some shadow
            # shadow = ConvertToMonochrome(self._tbButtons[idx]._tbItem.GetBitmap())
            # dc.DrawBitmap(shadow, x, y, True)

            # in case we the button is in focus, place it
            # once pixle up and left
            x -= 1
            y -= 1
        dc.DrawBitmap(self._tbButtons[idx]._tbItem.GetBitmap(), x, y, True)

    def ActivateMenu(self, menuInfo):
        """
        Activates a menu.

        :param `menuInfo`: an instance of :class:`wx.MenuEntryInfo`.
        """

        # first make sure all other menus are not popedup
        if menuInfo.GetMenu().IsShown():
            return

        idx = wx.NOT_FOUND

        for item in self._items:
            item.GetMenu().Dismiss(False, True)
            if item.GetMenu() == menuInfo.GetMenu():
                idx = self._items.index(item)

        # Remove the popup menu as well
        if self._moreMenu and self._moreMenu.IsShown():
            self._moreMenu.Dismiss(False, True)

        # make sure that the menu item button is highlited
        if idx != wx.NOT_FOUND:
            self._dropDownButtonState = ControlNormal
            self._curretHiliteItem = idx
            for item in self._items:
                item.SetState(ControlNormal)

            self._items[idx].SetState(ControlPressed)
            self.Refresh()
        rect = menuInfo.GetRect()
        menuPt = wx.Point(rect.x, rect.y)
        menuInfo.GetMenu().SetOwnerHeight(rect.height)
        menuInfo.GetMenu().Popup(menuPt, owner=self)

    def DoToolbarAction(self, idx):
        """
        Performs a toolbar button pressed action.

        :param integer `idx`: the tool index in the toolbar.
        """

        # we handle only button clicks
        tbItem = self._tbButtons[idx]._tbItem
        if tbItem.IsRegularItem() or tbItem.IsCheckItem() or tbItem.IsRadioItem():
            # Create the event
            event = wx.CommandEvent(wxEVT_FLAT_MENU_SELECTED, tbItem.GetId())
            event.SetEventObject(self)

            # all events are handled by this control and its parents
            # wx.PostEvent(self, event)  # async is better
            self.GetEventHandler().ProcessEvent(event)

    def FindMenu(self, title):
        """
        Returns the index of the menu with the given title or ``wx.NOT_FOUND`` if
        no such menu exists in this menubar.

        :param string `title`: may specify either the menu title (with accelerator characters,
         i.e. "&File") or just the menu label ("File") indifferently.
        """

        for ii, item in enumerate(self._items):
            accelIdx, labelOnly = GetAccelIndex(item.GetTitle())

            if labelOnly == title or item.GetTitle() == title:
                return ii

        return wx.NOT_FOUND

    def GetMenu(self, menuIdx):
        """
        Returns the menu at the specified index `menuIdx` (zero-based).

        :param integer `menuIdx`: the index of the sought menu.

        :return: The found menu item object, or ``None`` if one was not found.
        """

        if menuIdx >= len(self._items) or menuIdx < 0:
            return None

        return self._items[menuIdx].GetMenu()

    def GetMenuCount(self):
        """ Returns the number of menus in the menubar. """

        return len(self._items)

    def Insert(self, pos, menu, title):
        """
        Inserts the menu at the given position into the menu bar.

        :param integer `pos`: the position of the new menu in the menu bar;
        :param `menu`: the menu to add, an instance of :class:`FlatMenu`. :class:`FlatMenuBar` owns the menu and will free it;
        :param string `title`: the title of the menu.

        :note: Inserting menu at position 0 will insert it in the very beginning of it,
         inserting at position :meth:`~FlatMenuBar.GetMenuCount` is the same as calling :meth:`~FlatMenuBar.Append`.
        """

        menu.SetMenuBar(self)
        self._items.insert(pos, MenuEntryInfo(title, menu))
        self.UpdateAcceleratorTable()

        self.ClearBitmaps(pos)
        self.Refresh()
        return True

    def RemoveMenu(self, menu):
        """Removes the menu object from the menu bar."""
        index = next((pos for pos in range(len(self._items)) if self._items[pos].GetMenu() == menu), None)
        if index is None:
            return None
        return self.Remove(index)

    def Remove(self, pos):
        """
        Removes the menu from the menu bar and returns the menu object - the
        caller is responsible for deleting it.

        :param integer `pos`: the position of the menu in the menu bar.

        :note: This function may be used together with :meth:`~FlatMenuBar.Insert` to change the menubar
         dynamically.
        """

        if pos >= len(self._items):
            return None

        menu = self._items[pos].GetMenu()
        self._items.pop(pos)
        self.UpdateAcceleratorTable()

        # Since we use bitmaps to optimize our drawings, we need
        # to reset all bitmaps from pos and until end of vector
        # to force size/position changes to the menu bar
        self.ClearBitmaps(pos)
        self.Refresh()

        # remove the connection to this menubar
        menu.SetMenuBar(None)
        return menu

    def UpdateAcceleratorTable(self):
        """ Updates the parent accelerator table. """

        # first get the number of items we have
        updatedTable = []
        parent = self.GetParent()

        for item in self._items:

            updatedTable = item.GetMenu().GetAccelArray() + updatedTable

            # create accelerator for every menu (if it exist)
            title = item.GetTitle()
            mnemonic, labelOnly = GetAccelIndex(title)

            if mnemonic != wx.NOT_FOUND:

                # Get the accelrator character
                accelChar = labelOnly[mnemonic]
                accelString = "\tAlt+" + accelChar
                title += accelString

                accel = wx.AcceleratorEntry()
                accel.FromString(title)
                itemId = item.GetCmdId()

                if accel:

                    # connect an event to this cmd
                    parent.Connect(itemId, -1, wxEVT_FLAT_MENU_SELECTED, self.OnAccelCmd)
                    accel.Set(accel.GetFlags(), accel.GetKeyCode(), itemId)
                    updatedTable.append(accel)

        entries = [wx.AcceleratorEntry() for ii in range(len(updatedTable))]

        # Add the new menu items
        for i in range(len(updatedTable)):
            entries[i] = updatedTable[i]

        table = wx.AcceleratorTable(entries)
        del entries

        parent.SetAcceleratorTable(table)

    def ClearBitmaps(self, start=0):
        """
        Restores a :class:`NullBitmap` for all the items in the menu.

        :param integer `start`: the index at which to start resetting the bitmaps.
        """

        if self._isLCD:
            return

        for item in self._items[start:]:
            item.SetTextBitmap(wx.NullBitmap)
            item.SetSelectedTextBitmap(wx.NullBitmap)

    def OnAccelCmd(self, event):
        """
        Single function to handle any accelerator key used inside the menubar.

        :param `event`: a :class:`FlatMenuEvent` event to be processed.
        """

        for item in self._items:
            if item.GetCmdId() == event.GetId():
                self.ActivateMenu(item)

    def ActivateNextMenu(self):
        """ Activates next menu and make sure all others are non-active. """

        last_item = self.GetLastVisibleMenu()
        # find the current active menu
        for i in range(last_item+1):
            if self._items[i].GetMenu().IsShown():
                nextMenu = i + 1
                if nextMenu >= last_item:
                    nextMenu = 0
                self.ActivateMenu(self._items[nextMenu])
                return

    def GetLastVisibleMenu(self):
        """ Returns the index of the last visible menu on the menu bar. """

        last_item = 0

        # find the last visible item
        rect = wx.Rect()

        for item in self._items:

            if item.GetRect() == rect:
                break

            last_item += 1

        return last_item

    def ActivatePreviousMenu(self):
        """ Activates previous menu and make sure all others are non-active. """

        # find the current active menu
        last_item = self.GetLastVisibleMenu()

        for i in range(last_item):
            if self._items[i].GetMenu().IsShown():
                prevMenu = i - 1
                if prevMenu < 0:
                    prevMenu = last_item - 1

                if prevMenu < 0:
                    return

                self.ActivateMenu(self._items[prevMenu])
                return

    def CreateMoreMenu(self):
        """ Creates the drop down menu and populate it. """

        if not self._moreMenu:
            # first time
            self._moreMenu = Menu(parent=self)
            self._popupDlgCmdId = wx.NewIdRef()

            # Connect an event handler for this event
            self.Connect(self._popupDlgCmdId, -1, wxEVT_FLAT_MENU_SELECTED, self.OnCustomizeDlg)

        # Remove all items from the popup menu
        self._moreMenu.Clear()

        invM = self.GetInvisibleMenuItemCount()

        for i in range(len(self._items) - invM, len(self._items)):
            item = MenuItem(self._moreMenu, wx.ID_ANY, self._items[i].GetTitle(),
                                "", wx.ITEM_NORMAL, self._items[i].GetMenu())
            self._moreMenu.AppendItem(item)

        # Add invisible toolbar items
        invT = self.GetInvisibleToolbarItemCount()

        if self._showToolbar and invT > 0:
            if self.GetInvisibleMenuItemCount() > 0:
                self._moreMenu.AppendSeparator()

            for i in range(len(self._tbButtons) - invT, len(self._tbButtons)):
                if self._tbButtons[i]._tbItem.IsSeparator():
                    self._moreMenu.AppendSeparator()
                elif not self._tbButtons[i]._tbItem.IsCustomControl():
                    tbitem = self._tbButtons[i]._tbItem
                    item = MenuItem(self._tbMenu, tbitem.GetId(), tbitem.GetLabel(), "", wx.ITEM_NORMAL, None, tbitem.GetBitmap(), tbitem.GetDisabledBitmap())
                    item.Enable(tbitem.IsEnabled())
                    self._moreMenu.AppendItem(item)


        if self._showCustomize:
            if invT + invM > 0:
                self._moreMenu.AppendSeparator()
            item = MenuItem(self._moreMenu, self._popupDlgCmdId, _(six.u("Customize...")))
            self._moreMenu.AppendItem(item)

    def GetInvisibleMenuItemCount(self):
        """
        Returns the number of invisible menu items.

        :note: Valid only after the :class:`PaintEvent` has been processed after a resize.
        """

        return len(self._items) - self.GetLastVisibleMenu()

    def GetInvisibleToolbarItemCount(self):
        """
        Returns the number of invisible toolbar items.

        :note: Valid only after the :class:`PaintEvent` has been processed after a resize.
        """

        count = 0
        for i in range(len(self._tbButtons)):
            if self._tbButtons[i]._visible == False:
                break
            count = i

        return len(self._tbButtons) - count - 1

    def PopupMoreMenu(self):
        """ Pops up the 'more' menu. """

        if (not self._showCustomize) and self.GetInvisibleMenuItemCount() + self.GetInvisibleToolbarItemCount() < 1:
            return

        self.CreateMoreMenu()

        pt = self._dropDownButtonArea.GetTopLeft()
        pt = self.ClientToScreen(pt)
        pt.y += self._dropDownButtonArea.GetHeight()
        pt = self.ScreenToClient(pt)
        self._moreMenu.Popup(pt, owner=self)

    def OnCustomizeDlg(self, event):
        """
        Handles the customize dialog here.

        :param `event`: a :class:`FlatMenuEvent` event to be processed.
        """

        if not self._dlg:
            self._dlg = FMCustomizeDlg(self)
        else:
            # intialize the dialog
            self._dlg.Initialise()

        if self._dlg.ShowModal() == wx.ID_OK:
            # Handle customize requests here
            pass

        if "__WXGTK__" in wx.Platform:
            # Reset the more button
            dc = wx.ClientDC(self)
            self.DrawMoreButton(dc, ControlNormal)

    def AppendToolbarItem(self, item):
        """
        Appends a tool to the :class:`FlatMenuBar`.

        .. deprecated:: 0.9.5
           This method is now deprecated.

        :see: :meth:`~FlatMenuBar.AddTool`
        """

        newItem = ToolBarItem(item, wx.Rect(), ControlNormal)
        self._tbButtons.append(newItem)

    def AddTool(self, toolId, label="", bitmap1=wx.NullBitmap, bitmap2=wx.NullBitmap,
                kind=wx.ITEM_NORMAL, shortHelp="", longHelp=""):
        """
        Adds a tool to the toolbar.

        :param integer `toolId`: an integer by which the tool may be identified in subsequent
         operations;
        :param string `label`: the tool label string;
        :param integer `kind`: may be ``wx.ITEM_NORMAL`` for a normal button (default),
         ``wx.ITEM_CHECK`` for a checkable tool (such tool stays pressed after it had been
         toggled) or ``wx.ITEM_RADIO`` for a checkable tool which makes part of a radio
         group of tools each of which is automatically unchecked whenever another button
         in the group is checked;
        :param `bitmap1`: the primary tool bitmap, an instance of :class:`wx.Bitmap`;
        :param `bitmap2`: the bitmap used when the tool is disabled. If it is equal to
         :class:`NullBitmap`, the disabled bitmap is automatically generated by greing out
         the normal one;
        :param string `shortHelp`: a string used for the tools tooltip;
        :param string `longHelp`: this string is shown in the :class:`StatusBar` (if any) of the
         parent frame when the mouse pointer is inside the tool.
        """

        self._tbButtons.append(ToolBarItem(FlatToolbarItem(bitmap1, toolId, label, bitmap2, kind, shortHelp, longHelp), wx.Rect(), ControlNormal))


    def AddSeparator(self):
        """ Adds a separator for spacing groups of tools in toolbar. """

        if len(self._tbButtons) > 0 and not self._tbButtons[len(self._tbButtons)-1]._tbItem.IsSeparator():
            self._tbButtons.append(ToolBarItem(FlatToolbarItem(), wx.Rect(), ControlNormal))


    def AddControl(self, control):
        """
        Adds any control to the toolbar, typically e.g. a combobox.

        :param `control`: the control to be added, a subclass of :class:`wx.Window` (but no :class:`TopLevelWindow`).
        """

        self._tbButtons.append(ToolBarItem(FlatToolbarItem(control), wx.Rect(), ControlNormal))


    def AddCheckTool(self, toolId, label="", bitmap1=wx.NullBitmap, bitmap2=wx.NullBitmap, shortHelp="", longHelp=""):
        """
        Adds a new check (or toggle) tool to the toolbar.

        :see: :meth:`~FlatMenuBar.AddTool` for parameter descriptions.
        """

        self.AddTool(toolId, label, bitmap1, bitmap2, kind=wx.ITEM_CHECK, shortHelp=shortHelp, longHelp=longHelp)


    def AddRadioTool(self, toolId, label= "", bitmap1=wx.NullBitmap, bitmap2=wx.NullBitmap, shortHelp="", longHelp=""):
        """
        Adds a new radio tool to the toolbar.

        Consecutive radio tools form a radio group such that exactly one button in the
        group is pressed at any moment, in other words whenever a button in the group is
        pressed the previously pressed button is automatically released.

        You should avoid having the radio groups of only one element as it would be
        impossible for the user to use such button.

        By default, the first button in the radio group is initially pressed, the others are not.

        :see: :meth:`~FlatMenuBar.AddTool` for parameter descriptions.
        """

        self.AddTool(toolId, label, bitmap1, bitmap2, kind=wx.ITEM_RADIO, shortHelp=shortHelp, longHelp=longHelp)

        if len(self._tbButtons)<1 or not self._tbButtons[len(self._tbButtons)-2]._tbItem.IsRadioItem():
            self._tbButtons[len(self._tbButtons)-1]._tbItem.Select(True)
            self._lastRadioGroup += 1

        self._tbButtons[len(self._tbButtons)-1]._tbItem.SetGroup(self._lastRadioGroup)


    def SetUpdateInterval(self, interval):
        """
        Sets the UpdateUI interval for toolbar items. All UpdateUI events are
        sent from within :meth:`~FlatMenuBar.OnIdle` handler, the default is 20 milliseconds.

        :param integer `interval`: the updateUI interval in milliseconds.
        """

        self._interval = interval


    def PositionAUI(self, mgr, fixToolbar=True):
        """
        Positions the control inside a wxAUI / PyAUI frame manager.

        :param `mgr`: an instance of :class:`~wx.lib.agw.aui.framemanager.AuiManager` or :class:`framemanager`;
        :param bool `fixToolbar`: ``True`` if :class:`FlatMenuBar` can not be floated.
        """

        if CPP_AUI and isinstance(mgr, aui.AuiManager):
            pn = AuiPaneInfo()
        else:
            pn = PyAuiPaneInfo()

        xx = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)

        # We add our menu bar as a toolbar, with the following settings

        pn.Name("flat_menu_bar")
        pn.Caption("Menu Bar")
        pn.Top()
        pn.MinSize(wx.Size(xx/2, self._barHeight))
        pn.LeftDockable(False)
        pn.RightDockable(False)
        pn.ToolbarPane()

        if not fixToolbar:
            # We add our menu bar as a toolbar, with the following settings
            pn.BestSize(wx.Size(xx, self._barHeight))
            pn.FloatingSize(wx.Size(300, self._barHeight))
            pn.Floatable(True)
            pn.MaxSize(wx.Size(xx, self._barHeight))
            pn.Gripper(True)

        else:
            pn.BestSize(wx.Size(xx, self._barHeight))
            pn.Gripper(False)

        pn.Resizable(False)
        pn.PaneBorder(False)
        mgr.AddPane(self, pn)

        self._mgr = mgr


    def DoGiveHelp(self, hit):
        """
        Gives tooltips and help in :class:`StatusBar`.

        :param `hit`: the toolbar tool currently hovered by the mouse.
        """

        shortHelp = hit.GetShortHelp()
        if shortHelp:
            self.SetToolTip(shortHelp)
            self._haveTip = True

        longHelp = hit.GetLongHelp()
        if not longHelp:
            return

        topLevel = wx.GetTopLevelParent(self)

        if isinstance(topLevel, wx.Frame) and topLevel.GetStatusBar():
            statusBar = topLevel.GetStatusBar()

            if self._statusTimer and self._statusTimer.IsRunning():
                self._statusTimer.Stop()
                statusBar.PopStatusText(0)

            statusBar.PushStatusText(longHelp, 0)
            self._statusTimer = StatusBarTimer(self)
            self._statusTimer.Start(_DELAY, wx.TIMER_ONE_SHOT)


    def RemoveHelp(self):
        """ Removes the tooltips and statusbar help (if any) for a button. """

        if self._haveTip:
            self.SetToolTip("")
            self._haveTip = False

        if self._statusTimer and self._statusTimer.IsRunning():
            topLevel = wx.GetTopLevelParent(self)
            statusBar = topLevel.GetStatusBar()
            self._statusTimer.Stop()
            statusBar.PopStatusText(0)
            self._statusTimer = None


    def OnStatusBarTimer(self):
        """ Handles the timer expiring to delete the `longHelp` string in the :class:`StatusBar`. """

        topLevel = wx.GetTopLevelParent(self)
        statusBar = topLevel.GetStatusBar()
        statusBar.PopStatusText(0)
