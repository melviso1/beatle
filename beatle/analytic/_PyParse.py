# -*- coding: utf-8 -*-
"""The mission of this module is analyzing python code and
recreate hist structure"""

import ast
from . import astd


class PyParse(object):
    """Class for parsing a python text"""

    project = None

    @classmethod
    def set_project(this, project):
        """Sets the project"""
        PyParse.project = project

    def __init__(self, container, text):
        """Initializes the parser and do the work"""
        super(PyParse, self).__init__()
        self._outlines = []  # the set of lines to remove
        self._container = container
        self._text = text
        self._set_handlers()
        self._status = self.analyze()

    def register_imports_folder(self, parent):
        """Access or create to import folder"""
        from beatle import model
        s = parent[model.py.ImportsFolder]
        if len(s):
            return s[0]
        kwargs = {'parent': parent}
        return model.py.ImportsFolder(**kwargs)

    def inline(self):
        """This method returns the text without what was processed"""
        l = ['0'] + self._text.splitlines()
        u = [l[i] for i in range(1, len(l)) if i not in self._outlines]
        return '\n'.join(u)

    def _set_handlers(self):
        """The ast tree is composed by several types of nodes
        each one representing a kind of python expression.
        This method setup the handlers for each one type of
        nodes"""
        self._root_handler = {
            ast.Import: self.process_import,
            ast.ImportFrom: self.process_import_from,
            ast.ClassDef: self.process_class_def,
            ast.FunctionDef: self.process_function_def,
            ast.Assign: self.process_var,
            }

        self._handler = {
            ast.Expr: self.process_expr,  # Expressions can't be parsed at main level
            ast.Import: self.process_import,
            ast.ImportFrom: self.process_import_from,
            ast.ClassDef: self.process_class_def,
            ast.FunctionDef: self.process_function_def,
            ast.Assign: self.process_var,
            }

        self._text_handler = {
            ast.Expr: self._text_expr,
            ast.Attribute: self._text_attribute,
            ast.Name: self._text_name,
            ast.Str: self._text_str,
            ast.Num: self._num_str,
            ast.BinOp: self._binop_str,
            ast.Add: self._add_str,
            ast.Sub: self._sub_str,
            ast.Mult: self._mult_str,
            ast.Div: self._div_str,
            ast.Compare: self._compare_str,
            ast.FloorDiv: self._floordiv_str,
            ast.Mod: self._mod_str,
            ast.Pow: self._pow_str,
            ast.LShift: self._lshift_str,
            ast.RShift: self._rshift_str,
            ast.BitOr: self._bitor_str,
            ast.BitXor: self._bitxor_str,
            ast.BitAnd: self._and_str,
            ast.keyword: self._keyword_str,
            ast.Dict: self._dict_str,
            ast.Tuple: self._tuple_str,
            ast.List: self._list_str,
            ast.ListComp: self._listcomp_str,
            ast.comprehension: self._comprehension_str,
            ast.Subscript: self._subscript_str,
            ast.Index: self._index_str,
            ast.Call: self._call_str,
            ast.UnaryOp: self._unary_str,
            ast.NameConstant: self._constant_str,
            ast.Constant: self._constant_str,
            ast.Lambda: self._lambda_str,
            ast.arg: self._arg_str,
            ast.DictComp: self._dictcomp_str,
            }

    def call_root_handler(self, parent, node):
        """Do the call"""
        return self._root_handler.get(type(node), self.process_none)(parent, node)

    def call_handler(self, parent, node):
        """Do the call"""
        return self._handler.get(type(node), self.process_none)(parent, node)

    def process_node(self, parent, node):
        """This is the root of process analysing"""
        try:
            for child_node in ast.iter_child_nodes(node):
                self.call_root_handler(parent, child_node)
        except AttributeError as e:
            import traceback
            import sys
            traceback.print_exc(file=sys.stdout)
            print(type(e))  # the exception instance
            print(e.args)  # arguments stored in .args
            print(e)
            return False
        return True

    def text_node(self, node):
        """This represents the text converter"""
        self.outline(node)
        return self._text_handler.get(type(node), self.text_none)(node)

    def text_none(self, node):
        """This represents the missing handler case"""
        print('\nmissing text handler for {t}'.format(t=type(node)))
        return ''

    def process_none(self, parent, node):
        """Do an empty process for unknown nodes.
        The content of inline code (not class, variable or function definition) is
        not parsed and will be pushed as inline content in the module."""
        # print('\nmissing process handler for {t}'.format(t=type(node)))
        return True

    def process_expr(self, parent, node):
        """Add comment to parent? Otherwise, discarded as raw expression"""
        self.outline(node)
        parent.note = self.text_node(node.value).strip()[1:-1]
        return True

    def process_import(self, parent, node):
        """Process a import node"""
        from beatle import model
        if len(node.names):
            kwargs = {'parent': self.register_imports_folder(parent)}
            child_index = parent.last_child_index+1
            for n in node.names:
                kwargs['name'] = n.name
                kwargs['as'] = n.asname
                kwargs['child_index'] = child_index
                model.py.Import(**kwargs)
                child_index += 1
        self.outline(node)
        return True

    def process_import_from(self, parent, node):
        """Process a import from node"""
        from beatle import model
        child_index = parent.last_child_index + 1
        prefix = '.'*node.level
        if len(node.names):
            kwargs = {
                'parent': self.register_imports_folder(parent),
                'name': prefix + (node.module or '')
            }
            for n in node.names:
                kwargs['from'] = n.name
                kwargs['as'] = n.asname
                kwargs['child_index'] = child_index
                model.py.Import(**kwargs)
                child_index = child_index+1
        self.outline(node)
        return True

    def process_class_def(self, parent, node):
        """Process a class definition"""
        from beatle import model
        kwargs = {
            'parent': parent,
            'name': node.name,
            'raw': True,
            'note': ast.get_docstring(node, False) or '',
            'child_index': parent.last_child_index + 1
            }
        if not kwargs['note']:
            i = 0
        else:
            i = 1
        # the base classes must be resolved later
        cls = model.py.Class(**kwargs)
        # annotate bases for later resolution
        if node.bases:
            if PyParse.project:
                kwargs['parent'] = cls
                kwargs['raw'] = True
                for base in node.bases:
                    kwargs['ancestor'] = None
                    kwargs['name'] = self.text_node(base)
                    model.py.Inheritance(**kwargs)
            else:
                cls._bases = node.bases
        for j in range(i, len(node.body)):
            self.call_handler(cls, node.body[j])
        self.outline(node)
        return True

    def process_var(self, parent, node):
        """Process a single global assign.
        Some caveats here: if the parent is not a class, it means that
        the variable is global. If targets is a list with more than one
        element or with one target that is not ast.Name, this code must be inserted
        raw in the module. Expressions like a[0]= 1 are not declarations.
        Even more, if the variable name still exists, the code is also
        treat as raw.
        This applies also to classes, but this could result in unexpected
        behaviour. No one expects raw code inside class declaration, right?
        """
        from beatle import model

        if len(node.targets) > 1:
            return True  # not a declaration
        target = node.targets[0]
        if type(target) is not ast.Name:
            return True  # not a declaration
        name = target.id
        if type(parent) is model.py.Class:
            model_class = model.py.MemberData
        else:
            model_class = model.py.Data
        if len(parent(model_class, filter=lambda x: x.name == name)) > 0:
            return True  # already declared
        model_class(parent=parent, name=name,
                    value=self.text_node(node.value),
                    child_index=parent.last_child_index+1)
        return True

    def process_decorator(self, parent, node):
        """Process a decorator node"""
        from beatle import model
        child_index = parent.last_child_index + 1
        kwargs = {'parent': parent}
        if type(node) is ast.Name:
            kwargs['name'] = '{node.id}'.format(node=node)
        if type(node) is ast.Call:
            kwargs['call'] = True
            kwargs['name'] = self.text_node(node.func)
        else:
            kwargs['name'] = self.text_node(node)
        kwargs['child_index'] = child_index
        deco = model.py.Decorator(**kwargs)
        if type(node) is ast.Call:
            for arg in node.args:
                self.process_arg_def(deco, arg, context='value')
        return True

    def _text_expr(self, node):
        """process a ast.Expr"""
        return self.text_node(node.value)

    def _text_attribute(self, node):
        """process a ast.Attribute"""
        attr = node.attr
        if node.value:
            value = self.text_node(node.value)
            return '{value}.{attr}'.format(value=value, attr=attr)
        return attr

    def _text_name(self, node):
        """process ast.Name"""
        return node.id

    def _text_str(self, node):
        """process a ast.Str"""
        if '"' in node.s:
            return "'{node.s}'".format(node=node)
        return '"{node.s}"'.format(node=node)

    def _num_str(self, node):
        """process a ast.Num"""
        return "{node.n}".format(node=node)

    def _binop_str(self, node):
        """process ast.BinOp"""
        left = self.text_node(node.left)
        right = self.text_node(node.right)
        operand = self.text_node(node.op)
        return '{left} {operand} {right}'.format(
            left=left, right=right, operand=operand)

    def _add_str(self, node):
        """process ast.Add"""
        return '+'

    def _sub_str(self, node):
        """process ast.Sub"""
        return '-'

    def _mult_str(self, node):
        """process ast.Mult"""
        return '*'

    def _div_str(self, node):
        """process ast.Div"""
        return '/'

    def _compare_str(self, node):
        """process ast.Comparison"""
        right_terms = len(node.ops)
        operators_dict = {
            ast.Eq : "==",
            ast.NotEq: "!=",
            ast.Lt: "<",
            ast.LtE: "<=",
            ast.Gt: ">",
            ast.GtE: ">=",
            ast.Is: "is",
            ast.IsNot: "is not",
            ast.In: "in",
            ast.NotIn: "not in"
        }
        right = ' '.join(['{op} {comp}'.format(
            op=operators_dict[type(node.ops[i])],
            comp=self.text_node(node.comparators[i])) for i in range(0,right_terms)])
        return '{left} {right}'.format(left=self.text_node(node.left), right=right)

    def _floordiv_str(self, node):
        """process ast.FloorDiv"""
        return '//'

    def _mod_str(self, node):
        """process ast.Mod"""
        return '%'

    def _pow_str(self, node):
        """process ast.Pow"""
        return '**'

    def _lshift_str(self, node):
        """process ast.LShift"""
        return '<<'

    def _rshift_str(self, node):
        """process ast.RShift"""
        return '>>'

    def _bitor_str(self, node):
        """process ast.BitOr"""
        return '|'

    def _bitxor_str(self, node):
        """process ast.BitXor"""
        return '^'

    def _and_str(self, node):
        """process ast.BitAnd"""
        return '&'

    def _keyword_str(self, node):
        """process ast.keyword"""
        value = self.text_node(node.value)
        arg = node.arg
        return '{arg}={value}'.format(arg=arg, value=value)

    def _dict_str(self, node):
        """process ast.Dict"""
        assert(len(node.keys) == len(node.values))
        return '{{{cnt}}}'.format(cnt=', '.join(['{k}: {v}'.format(
            k=self.text_node(node.keys[i]),
            v=self.text_node(node.values[i])) for i in range(0, len(node.keys))]))

    def _tuple_str(self, node):
        """process ast.Tuple"""
        return '({cnt})'.format(cnt=', '.join([self.text_node(k) for k in node.elts]))

    def _list_str(self, node):
        """ast.List -> string"""
        return '[{cnt}]'.format(cnt=', '.join([self.text_node(k) for k in node.elts]))

    def _listcomp_str(self, node):
        """ast.ListComp -> string"""
        return '[{target} {generators}]'.format(
            target=self.text_node(node.elt),
            generators=' '.join([self.text_node(k) for k in node.generators])
        )

    def _comprehension_str(self, node):
        """ast.comprehension -> string"""
        ifs = ' if '.join([self.text_node(k) for k in node.ifs])
        return 'for {target} in {iter}{ifs}'.format(
            target=self.text_node(node.target),
            iter=self.text_node(node.iter),
            ifs=ifs)

    def _dictcomp_str(self, node):
        """ast.DictComp -> string"""
        return '{{ {key}:{value} {generator} }}'.format(
            key=self.text_node(node.key),
            value=self.text_node(node.value),
            generator=' '.join(self.text_node(x) for x in node.generators)
        )

    def _subscript_str(self, node):
        """process ast.Subscript"""
        name = self.text_node(node.value)
        _slice = self.text_node(node.slice)
        return '{name}[{slice}]'.format(name=name, slice=_slice)

    def _index_str(self, node):
        """process ast.Index"""
        return self.text_node(node.value)

    def _call_str(self, node):
        """process ast.Call"""
        args = [self.text_node(x) for x in node.args or []]
        args.extend([self.text_node(x) for x in node.keywords])
        starargs = getattr(node, 'starargs', None)
        if starargs:
            args.append('*{0}'.format(self.text_node(starargs)))
        kwargs = getattr(node,'kwargs', None)
        if kwargs:
            args.append('**{0}'.format(self.text_node(kwargs)))
        func = self.text_node(node.func)
        args = ', '.join(args)
        return '{func}({args})'.format(func=func, args=args)

    def _unary_str(self, node):
        """process ast.UnaryOp"""
        if type(node.op) == ast.Invert:
            return '~{0}'.format(self.text_node(node.operand))
        elif type(node.op) == ast.Not:
            return 'not {0}'.format(self.text_node(node.operand))
        elif type(node.op) == ast.UAdd:
            return '+{0}'.format(self.text_node(node.operand))
        elif type(node.op) == ast.USub:
            return '-{0}'.format(self.text_node(node.operand))
        else:
            return self.text_none(node)

    def _constant_str(self, node):
        if type(node.value) is str:
            return '"{node.value}"'.format(node=node)
        else:
            return '{node.value}'.format(node=node)

    def _lambda_str(self, node):
        """ast.Lambda -> string"""
        return 'lambda {args}: {expr}'.format(
            args=', '.join(self.text_node(k) for k in node.args.args),
            expr=self.text_node(node.body)
        )

    def _arg_str(self, node):
        """ast.arg -> string"""
        return node.arg

    def process_arg_def(self, parent, node, **kwargs):
        """Process an argument node.
        Note: This method is intended for used with function argument definitions, but
        it's also used with decorator invocation (not nice blur: must be fixed)"""
        from beatle import model
        child_index = parent.last_child_index+1

        if type(parent) is model.py.Decorator:
            name = self.text_node(node)
        else:
            self.outline(node)
            name = node.arg
        kwargs = {
            'parent': parent,
            'name': name,
            'default': kwargs.get('default', ''),
            'context': kwargs.get('context', 'declare'),
            'child_index': child_index
            }
        model.py.Argument(**kwargs)
        return True

    def process_function_def(self, parent, node):
        """Process a function definition"""
        from beatle.model.py import Class, InitMethod, MemberMethod, Function, ArgsArgument, KwArgsArgument
        child_index = parent.last_child_index+1
        kwargs = {
            'parent': parent,
            'name': node.name,
            'note': '',
            'raw': True,
            'child_index': child_index,
            }
        for deco in node.decorator_list:
            self.outline(deco)
        if node.lineno not in self._outlines:
            self._outlines.append(node.lineno)
        # get the comment if any
        line = None
        if len(node.body) > 0:
            line = node.body[0].lineno
            col = node.body[0].col_offset
            if type(node.body[0]) is ast.Expr:
                if type(node.body[0].value) in [ast.Str, ast.Constant]:
                    self.outline(node.body[0])
                    line = node.body[0].to_lineno + 1
                    kwargs['note'] = node.body[0].value.s
        # line = max(self._outlines) + 1
        if type(parent) is Class:
            """Se trata de una funcion miembro"""
            if node.name == '__init__':
                function = InitMethod(**kwargs)
            else:
                function = MemberMethod(**kwargs)
        else:
            function = Function(**kwargs)
        # Ok, add decorators
        for deco in node.decorator_list:
            self.process_decorator(function, deco)
        # Ok, add arguments
        kwargs['parent'] = function
        kwargs['note'] = ''
        v = node.args.defaults
        i = len(v)
        k = len(node.args.args)
        for arg in node.args.args:
            # this is valid for python3. For the moment, we
            # work in python 2
            # self.call_handler(function, arg)
            if k > i:
                self.process_arg_def(function, arg)
            else:
                self.process_arg_def(function, arg, default=self.text_node(v[-i]))
                i = i - 1
            k = k - 1
        if node.args.vararg is not None:
            ArgsArgument(**kwargs)
        if node.args.kwarg is not None:
            KwArgsArgument(**kwargs)
        # push all lines
        self.outline(node)
        #now, extract content from lines
        if line:
            code = [0] + self._text.splitlines()
            code = [code[i] for i in range(line, node.to_lineno + 1)]
            lcode = len(code)
            if lcode:
                try:
                    i = next(n for n in range(0, lcode) if len(code[n].strip()) > 0)
                    test = code[i]
                    extra = len(test) - len(test.lstrip())
                    fcode = []
                    if extra:
                        # filter comments that will be addeded and cutted wrong
                        for i in range(0, lcode):
                            s = code[i]
                            if len(s.strip()) and len(s[:extra].strip()):
                                continue
                            else:
                                fcode.append(s)
                        lcode = len(fcode)
                        code = fcode
                        code = [code[i][extra:] for i in range(0, lcode)]
                        col -= extra
                        if col < 0:
                            col = 0
                except StopIteration:
                    pass
            #en la primera linea del codigo hemos de tratar de todas maneras la columna
            #por si se trata de una expresion inline
            if code:
                try:
                    code[0] = code[0][col:]
                    function._content = '\n'.join(code)
                except:
                    print("col bug")
                #for some reason, some nodes are not simply traveled as childs
        return True

    def outline(self, node):
        """This method adds the lines represented by the node
        and his childs to the _outlines"""
        if hasattr(node, 'lineno'):
            s = node.lineno
            if hasattr(node, 'to_lineno'):
                t = node.to_lineno
                if s and t:
                    for i in range(s, t + 1):
                        if i not in self._outlines:
                            self._outlines.append(i)
                    return
            if s is not None and s not in self._outlines:
                self._outlines.append(s)
        try:
            for child_node in ast.iter_child_nodes(node):
                self.outline(child_node)
        except Exception as e:
            print('Error :{error}'.format(error=str(e)))

    def analyze(self):
        """This method analyzes the code"""
        import traceback
        import sys
        try:
            tree = astd.parse(self._text)
            return self.process_node(self._container, tree)
        except Exception as inst:
            traceback.print_exc(file=sys.stdout)
            print(type(inst))     # the exception instance
            print(inst.args)      # arguments stored in .args
            print(inst)
            return False



