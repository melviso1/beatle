# -*- coding: utf-8 -*-

_step_over = [
b"24 24 2 1",
b" 	c None",
b".	c #1323BA",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"         ....           ",
b"      ..........        ",
b"     ..... ......       ",
b"    ...       ....      ",
b"   ...          ...     ",
b"  ...            ...    ",
b"  ..              ..    ",
b"  ..              ...   ",
b" ..                .. ..",
b" ..             .. .....",
b" ..             ........",
b" ..              ...... ",
b"                  ..... ",
b"                  ....  ",
b"                   ..   ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        "]