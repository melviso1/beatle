# -*- coding: utf-8 -*-

_git = [
b"24 24 120 2",
b"  	c None",
b". 	c #27B639",
b"+ 	c #2AB937",
b"@ 	c #33BA45",
b"# 	c #36BB44",
b"$ 	c #3CBD49",
b"% 	c #39BE47",
b"& 	c #59C868",
b"* 	c #3CBE47",
b"= 	c #40BF4C",
b"- 	c #40BE4C",
b"; 	c #3FBF4C",
b"> 	c #42C04D",
b", 	c #40BF4D",
b"' 	c #60C86B",
b") 	c #40BD4A",
b"! 	c #44C04F",
b"~ 	c #42C052",
b"{ 	c #41BF50",
b"] 	c #41C04E",
b"^ 	c #43C052",
b"/ 	c #44C050",
b"( 	c #41BF49",
b"_ 	c #F47A63",
b": 	c #35BA42",
b"< 	c #37BD44",
b"[ 	c #F04D2F",
b"} 	c #F04F32",
b"| 	c #2CB539",
b"1 	c #2FB839",
b"2 	c #F26A51",
b"3 	c #F05134",
b"4 	c #F05236",
b"5 	c #5DD274",
b"6 	c #5BD472",
b"7 	c #F1593E",
b"8 	c #F15336",
b"9 	c #F15A3E",
b"0 	c #F05033",
b"a 	c #F2573B",
b"b 	c #E1655D",
b"c 	c #D8362A",
b"d 	c #D93B33",
b"e 	c #DA3B34",
b"f 	c #DB3B35",
b"g 	c #DA3B35",
b"h 	c #D93B34",
b"i 	c #D93C33",
b"j 	c #D7352C",
b"k 	c #D8584E",
b"l 	c #F15B3E",
b"m 	c #F15538",
b"n 	c #DE6765",
b"o 	c #DA372E",
b"p 	c #DB3D33",
b"q 	c #D93F34",
b"r 	c #D83F35",
b"s 	c #D84035",
b"t 	c #D94136",
b"u 	c #DA4136",
b"v 	c #DA4135",
b"w 	c #DB4136",
b"x 	c #DD3C2F",
b"y 	c #DC5E54",
b"z 	c #F4806B",
b"A 	c #EF4728",
b"B 	c #F15539",
b"C 	c #F58571",
b"D 	c #F79784",
b"E 	c #686767",
b"F 	c #525151",
b"G 	c #605F5F",
b"H 	c #636262",
b"I 	c #595858",
b"J 	c #F79E8D",
b"K 	c #F05235",
b"L 	c #F05336",
b"M 	c #656464",
b"N 	c #666666",
b"O 	c #6A6A6A",
b"P 	c #5A5A5A",
b"Q 	c #6B6B6B",
b"R 	c #656565",
b"S 	c #646464",
b"T 	c #727272",
b"U 	c #F1583C",
b"V 	c #909B9D",
b"W 	c #626161",
b"X 	c #6F6F6F",
b"Y 	c #585858",
b"Z 	c #5C5C5C",
b"` 	c #F45538",
b" .	c #808B8D",
b"..	c #5C5B5B",
b"+.	c #595959",
b"@.	c #F58671",
b"#.	c #F45234",
b"$.	c #7D898C",
b"%.	c #5A5959",
b"&.	c #5E5E5E",
b"*.	c #F3735C",
b"=.	c #828485",
b"-.	c #606060",
b";.	c #5B5B5B",
b">.	c #F04E30",
b",.	c #949291",
b"'.	c #626262",
b").	c #686868",
b"!.	c #F04C2F",
b"~.	c #6C6C6C",
b"{.	c #5D5D5D",
b"].	c #F15B3F",
b"^.	c #EF4A2C",
b"/.	c #F05034",
b"(.	c #707070",
b"_.	c #636363",
b":.	c #696969",
b"<.	c #676767",
b"[.	c #F47761",
b"}.	c #F05234",
b"                                                ",
b"                          . +                   ",
b"                          @ #                   ",
b"                          $ %                   ",
b"                & * = - ; = > > ; , % '         ",
b"                ' ) ! ~ { = ] ^ / / ( '         ",
b"            _             : <                   ",
b"          [ }             | 1                   ",
b"        2 3 4             5 6                   ",
b"        7 3 8                                   ",
b"        9 0 a   b c d d e f g h d i j k         ",
b"        l 3 m   n o p q r s t u v w x y         ",
b"        z A B                                   ",
b"          C [                                   ",
b"          D [       E E     F G H G I           ",
b"        J K L     M N O     P Q R S T           ",
b"        U 3 8   V W X       Y       Z           ",
b"        m 0 `    ...        +.      +.          ",
b"        @.0 #.  $.%.                &.          ",
b"        *.0     =.-.                ;.          ",
b"        >.3     ,.'.).            O S           ",
b"        !.0       ~.R R         '.).{.          ",
b"        ].^./.      (._.:.<.N :.R S             ",
b"          [.}.          X N ).<.                "]