# -*- coding: utf-8 -*-

_files =[
b"24 24 84 1",
b" 	c None",
b".	c #BBBABB",
b"+	c #AFAFAF",
b"@	c #DCDBDC",
b"#	c #DADADA",
b"$	c #DEDEDE",
b"%	c #DADBDA",
b"&	c #D7D7D7",
b"*	c #C8C8C8",
b"=	c #B7B7B7",
b"-	c #C3C3C3",
b";	c #E0E0E0",
b">	c #D5D5D5",
b",	c #CFCFCF",
b"'	c #C4C4C4",
b")	c #DFDFDF",
b"!	c #DCDCDC",
b"~	c #ACACAC",
b"{	c #B6B6B6",
b"]	c #BBBBBB",
b"^	c #E3E3E3",
b"/	c #D9D9D9",
b"(	c #E6E6E6",
b"_	c #E5E4E4",
b":	c #D8D8D8",
b"<	c #DDDDDD",
b"[	c #DBDBDB",
b"}	c #D1D1D1",
b"|	c #BABABA",
b"1	c #B2B2B2",
b"2	c #A8A8A8",
b"3	c #E1E0E1",
b"4	c #C9C9C9",
b"5	c #B9B9B9",
b"6	c #B5B5B5",
b"7	c #E2E1E2",
b"8	c #D4D4D4",
b"9	c #DBDCDC",
b"0	c #E4E4E4",
b"a	c #E1E1E1",
b"b	c #A2A2A2",
b"c	c #D4D5D5",
b"d	c #CED1D0",
b"e	c #D0D3D2",
b"f	c #D6D6D6",
b"g	c #E2E2E2",
b"h	c #E4E5E6",
b"i	c #D8D9D9",
b"j	c #DBD7D8",
b"k	c #DCDADB",
b"l	c #DAD9D9",
b"m	c #E7E7E7",
b"n	c #B1B1B1",
b"o	c #D8D6D7",
b"p	c #E1D5D8",
b"q	c #E6D4D9",
b"r	c #E3D7DA",
b"s	c #E2E4E4",
b"t	c #E8E9E9",
b"u	c #DFE2E1",
b"v	c #B4B4B4",
b"w	c #BFBFBF",
b"x	c #DBD9DA",
b"y	c #DBDADA",
b"z	c #DEDCDE",
b"A	c #B6B3B4",
b"B	c #E5E5E5",
b"C	c #E2DFE1",
b"D	c #B8B3B4",
b"E	c #DCDBDB",
b"F	c #DDDCDC",
b"G	c #E5E8E8",
b"H	c #B9B9BB",
b"I	c #D3D5D5",
b"J	c #D4D6D6",
b"K	c #EBEBEB",
b"L	c #EDEDED",
b"M	c #D6DADA",
b"N	c #D9DEDF",
b"O	c #DBDFE0",
b"P	c #EAEAEA",
b"Q	c #E8E8E8",
b"R	c #DCDCDB",
b"S	c #BDBDBD",
b"                        ",
b"   ........+            ",
b"   .@#$$%&*=-           ",
b"   .;###&>,'>*          ",
b"   .)!........~         ",
b"   .)#.@#$$%&*{]        ",
b"   .^/.;###&>,'>*       ",
b"   .($.)!.........      ",
b"   .(!.)#.@#$$%&*{]     ",
b"   ._:.^/.;###&>,'>*    ",
b"   .<#.($.)![[[/}||12   ",
b"   .3<.(!.)#[##!&4.56   ",
b"   .7#._:.^/8>90^a<!,b  ",
b"   .)8.<#.($cdef#$)g:2  ",
b"   .hi.3<.(!//jkl:)m;n  ",
b"   .g[.7#._:&opqrsmtuv  ",
b"   w5=.)8.<###x[y[[gzA  ",
b"      .hi.3<<<<<<<<BCD  ",
b"      .g[.7#####EF)(GH  ",
b"      w5=.)8IJJf&FKL(5  ",
b"         .hiMNO00gPKQ5  ",
b"         .g[/R<^^gB0)S  ",
b"         w5=6=55=5]5w   ",
b"                        "]