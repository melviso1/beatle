# -*- coding: utf-8 -*-

import wx
from beatle.lib import wxx

from ._workspace import _workspace
from ._folder import _folder
from ._folder_open import _folder_open
from ._project import _project
from ._class import _class
from ._py_class import _py_class
from ._friend import _friend
from ._method import _method
from ._py_method import _py_method
from ._classdiagram import _classdiagram
from ._member import _member
from ._py_member import _py_member
from ._constructor import _constructor
from ._py_init import _py_init
from ._context import _context
from ._destructor import _destructor
from ._inheritance import _inheritance
from ._py_inheritance import _py_inheritance
from ._relation import _relation
from ._folderT import _folderT
from ._folderP import _folderP
from ._folderI import _folderI
from ._type import _type
from ._parent import _parent
from ._library import _library
from ._child import _child
from ._cppfile import _cppfile
from ._pyfile import _pyfile
from ._hfile import _hfile
from ._mfile import _mfile
from ._python import _python
from ._protected import _protected
from ._private import _private
from ._target import _target
from ._cppproject import _cppproject
from ._namespace import _namespace
from ._module import _module
from ._py_module import _py_module
from ._function import _function
from ._py_function import _py_function
from ._data import _data
from ._py_variable import _py_variable
from ._enum import _enum
from ._tres import _tres
from ._py_args import _py_args
from ._py_kwargs import _py_kwargs
from ._py_import import _py_import
from ._models import _models
from ._files import _files
from ._tasks import _tasks
from ._git import _git
from ._py_argument import _py_argument
from ._py_package import _py_package
from ._start import _start
from ._info import _info
from ._mini_logo import _mini_logo
from ._git_repo import _git_repo
from ._git_file import _git_file
from ._git_file_modified import _git_file_modified
from ._git_file_staged import _git_file_staged
from ._git_file_deleted import _git_file_deleted
from ._file import _file
from ._reload import _reload
from ._decorator import _decorator
from ._bookmark import bookmark
from ._git_remote import _git_remote
from ._glass_clock import _glass_clock
from ._task_high import _task_high
from ._task_critical import _task_critical
from ._folder_pendings import _folder_pendings
from ._folder_pendings_open import _folder_pendings_open
from ._folder_current import _folder_current
from ._folder_current_open import _folder_current_open
from ._refresh import _refresh
from ._run import _run
from ._run_file import _run_file
from ._stop import _stop
from ._debug import _debug
from ._debug_file import _debug_file
from ._step_into import _step_into
from ._step_out import _step_out
from ._step_over import _step_over
from ._continue import _continue
from ._pyboost import _pyboost
from ._py_boost_class import _py_boost_class
from ._py_boost_method import _py_boost_method
from ._py_boost_member import _py_boost_member
from ._py_boost_constructor import _py_boost_constructor
from ._py_boost_from_python_converter import _py_boost_from_python_converter
from ._databases import _databases
from ._database_schema import _database_schema
from ._database_table import _database_table
from ._database_field import _database_field
from ._checked import _checked
from ._checked_disabled import _checked_disabled
from ._unchecked import _unchecked
from ._unchecked_disabled import _unchecked_disabled
from ._cpprpc import _cpprpc
from ._mini_logo import _mini_logo

# from _class_small import _class_small


_xpm = {
    'class':_class, 'constructor':_constructor, 'member':_member, 'method':_method, 'destructor':_destructor,
    'inheritance':_inheritance, 'type':_type, 'parent':_parent, 'child':_child, 'enum':_enum, 'py_argument':_py_argument,
    'py_args':_py_args, 'py_kwargs':_py_kwargs, 'py_init':_py_init, 'py_member':_py_member, 'py_method':_py_method,
    'py_inheritance':_py_inheritance, 'classdiagram':_classdiagram, 'relation':_relation, 'folderT':_folderT,
    'cppfile':_cppfile, 'hfile':_hfile, 'mfile':_mfile, 'target':_target, 'cppproject':_cppproject, 'namespace':_namespace,
    'folder':_folder, 'folder_open': _folder_open, 'project':_project, 'module':_module, 'function':_function,
    'py_function':_py_function, 'data':_data, 'py_variable':_py_variable, 'tres':_tres, 'friend':_friend, 'folderP':_folderP,
    'library':_library, 'python':_python, 'workspace':_workspace, 'context':_context, 'py_module':_py_module,
    'py_package':_py_package, 'py_class': _py_class, 'models': _models, 'files':_files, 'tasks':_tasks, 'py_import':_py_import,
    'pyfile':_pyfile, 'start':_start, 'info':_info, 'mini_loop':_mini_logo, 'git':_git, 'git_repo':_git_repo,
    'git_file':_git_file, 'git_file_modified':_git_file_modified, 'git_file_staged':_git_file_staged,
    'git_file_deleted':_git_file_deleted, 'file':_file, 'reload':_reload, 'decorator':_decorator, 'folderI':_folderI,
    'git_remote': _git_remote, 'glass_clock': _glass_clock, 'task_high': _task_high, 'task_critical': _task_critical,
    'folder_pendings': _folder_pendings, 'folder_pendings_open': _folder_pendings_open,
    'folder_current': _folder_current, 'folder_current_open': _folder_current_open, 'refresh': _refresh, 'run':_run,
    'run_file': _run_file, 'stop': _stop, 'step_into': _step_into, 'step_out': _step_out, 'step_over': _step_over,
    'continue': _continue, 'debug': _debug, 'debug_file': _debug_file, 'pyboost': _pyboost, 'py_boost_class': _py_boost_class,
    'py_boost_member': _py_boost_member, 'py_boost_method':_py_boost_method,'py_boost_constructor':_py_boost_constructor,
    'py_boost_from_python_converter':_py_boost_from_python_converter, 'databases':_databases,
    'database_schema':_database_schema, 'database_table':_database_table, 'database_field':_database_field,
    'cpprpc':_cpprpc, 'mini_logo':_mini_logo }


_accessed = ['class', 'constructor', 'member', 'method', 'destructor', 'inheritance',
             'type', 'parent', 'child', 'enum']
_access = {'protected': _protected, 'private': _private}

_bitmap_list = []
_bitmap_access = {}


def update_xpm_dict(resource_dict):
    _xpm.update(resource_dict)


def create_bitmap_list():
    """Create a bitmap list"""
    global _bitmap_list
    global _xpm
    global _bitmap_access
    global _accessed
    from beatle import app
    bitmaps_with_access = []
    for key in _xpm:
        bmp = wx.Bitmap(_xpm[key])
        _bitmap_list.append(bmp)
        if key in _accessed:
            bitmaps_with_access.append(bmp)
    _bitmap_access['public'] = {}
    for bmp in bitmaps_with_access:
        _bitmap_access['public'][_bitmap_list.index(bmp)] = _bitmap_list.index(bmp)
    for access in _access:
        prefix_bitmap_object = wx.Bitmap(_access[access])
        _bitmap_access[access] = app.suffix_bitmap_list(prefix_bitmap_object, bitmaps_with_access, _bitmap_list)


def get_bitmap_index(name, access=None):
    """Return the bitmap index"""
    try:
        base_index = list(_xpm.keys()).index(name)
    except Exception as e:
        print('invalid bitmap index in get_bitmap_index.')
        return wx.NOT_FOUND
    global _bitmap_list
    if len(_bitmap_list) == 0:
        create_bitmap_list()
    if access is None:
        return base_index
    return _bitmap_access[access][base_index]


def get_bitmap(name, access=None):
    """Get the specific bitmap"""
    global _bitmap_list
    index = get_bitmap_index(name, access)
    if index == wx.NOT_FOUND:
        raise KeyError('bitmap {0} not found'.format(name))
    return _bitmap_list[index]


def get_bitmap_image_list():
    """Returns the bitmap image list"""
    from beatle import app
    global _bitmap_list
    if len(_bitmap_list) == 0:
        create_bitmap_list()
    (w, h) = app.get_bitmap_max_size(_bitmap_list)
    # we use variable-sized bitmaps here. That don't
    # works in windows. We must fix that some day
    if wx.__version__ >= '3.0.0.0'  and wx.__version__ < '4.1.1':
        w = 0
        image_list = wx.ImageList(w, h, mask=True, initialCount=len(_bitmap_list))
    else:
        image_list = wxx.ImageList(w, h, mask=True, initialCount=len(_bitmap_list))
    for bmp in _bitmap_list:
        image_list.Add(bmp)
    return image_list
