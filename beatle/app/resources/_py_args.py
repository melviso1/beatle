# -*- coding: utf-8 -*-

_py_args = [
b"24 24 10 1",
b" 	c None",
b".	c #000000",
b"+	c #F2F1F1",
b"@	c #FFFFFF",
b"#	c #787878",
b"$	c #F9F9F9",
b"%	c #F7F7F7",
b"&	c #F6F6F6",
b"*	c #EBEBEB",
b"=	c #EDEDED",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"  ....................  ",
b"  .+@@@.@@@@@@@@@@@@@.  ",
b"  .@.@@.@@@@@@@@@@@@@.  ",
b"  .@.@@.@@#########@@.  ",
b"  .@.@@.@@#########@@.  ",
b"  .@.@@.@@@@@@@@@@@@@.  ",
b"  .$%@@.$$%&%%%%%%%%%.  ",
b"  ....................  ",
b"  .@@@@.@@@@*@@=@@@@@.  ",
b"  .@..@.@@@@@@@@@@@@@.  ",
b"  .@@.@.@@#########@@.  ",
b"  .@.@@.@@#########@@.  ",
b"  .@..@.@@@@@@@@@@@@@.  ",
b"  .@@@@.@@@@@@@@@@@@@.  ",
b"  ....................  ",
b"                        ",
b"                        ",
b"                        ",
b"                        "]