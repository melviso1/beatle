# -*- coding: utf-8 -*-

_task_critical = [
b"24 24 267 2",
b"      c None",
b".     c #D7A697",
b"+     c #D2938E",
b"@     c #CE9590",
b"#     c #CD928F",
b"$     c #CB8B8C",
b"%     c #CD7F8A",
b"&     c #D95B8A",
b"*     c #CFC7C5",
b"=     c #9D2515",
b"-     c #AC2914",
b";     c #972415",
b">     c #912115",
b",     c #902216",
b"'     c #9A2516",
b")     c #8D2017",
b"!     c #581D1A",
b"~     c #AA181C",
b"{     c #DA78AD",
b"]     c #E3E0DD",
b"^     c #ECE2D6",
b"/     c #931F17",
b"(     c #CF272F",
b"_     c #B11F22",
b":     c #A41E2B",
b"<     c #BB2727",
b"[     c #DC3857",
b"}     c #942018",
b"|     c #7F2321",
b"1     c #5C1D1A",
b"2     c #631F1B",
b"3     c #8D221F",
b"4     c #D93F5C",
b"5     c #D1B3BB",
b"6     c #A02716",
b"7     c #B62D1E",
b"8     c #DD436B",
b"9     c #CDA1A8",
b"0     c #BB2331",
b"a     c #DD3353",
b"b     c #76241E",
b"c     c #C0302C",
b"d     c #A21E2B",
b"e     c #4A2626",
b"f     c #482222",
b"g     c #B9232A",
b"h     c #DE64A0",
b"i     c #A62716",
b"j     c #BB3123",
b"k     c #D8CCBA",
b"l     c #C93131",
b"m     c #BD232E",
b"n     c #D52D35",
b"o     c #D83040",
b"p     c #BD2E2B",
b"q     c #7C2428",
b"r     c #3F2A29",
b"s     c #D5A1C0",
b"t     c #E05892",
b"u     c #B8291A",
b"v     c #D26883",
b"w     c #ECCA9E",
b"x     c #BE2C2C",
b"y     c #93233A",
b"z     c #BB2E1F",
b"A     c #902025",
b"B     c #D42E32",
b"C     c #DD3756",
b"D     c #911F21",
b"E     c #82242D",
b"F     c #D47899",
b"G     c #BC291E",
b"H     c #DAAD88",
b"I     c #FDF1A1",
b"J     c #FEF0A2",
b"K     c #E1B999",
b"L     c #B72D1F",
b"M     c #D72730",
b"N     c #E0897A",
b"O     c #D57386",
b"P     c #85221D",
b"Q     c #CC3640",
b"R     c #E4427B",
b"S     c #B12B15",
b"T     c #EDD1A5",
b"U     c #FFE699",
b"V     c #FEF4A4",
b"W     c #E8DAA2",
b"X     c #A6241A",
b"Y     c #D67C70",
b"Z     c #FCC57E",
b"`     c #D68378",
b" .    c #7F2019",
b"..    c #E7559D",
b"+.    c #E33562",
b"@.    c #FFF5E3",
b"#.    c #FBD99D",
b"$.    c #E0D196",
b"%.    c #A01D16",
b"&.    c #DE8F76",
b"*.    c #EAAB7C",
b"=.    c #D5577E",
b"-.    c #791E17",
b";.    c #D4A6C2",
b">.    c #E12F55",
b",.    c #B92C1D",
b"'.    c #F9E9C3",
b").    c #FDF5AB",
b"!.    c #E2CF96",
b"~.    c #991C19",
b"{.    c #D27B74",
b"].    c #CC767A",
b"^.    c #D2ADAA",
b"/.    c #6C221C",
b"(.    c #DDDDDD",
b"_.    c #F1ECE3",
b":.    c #DB2738",
b"<.    c #D32F2F",
b"[.    c #F3EEE5",
b"}.    c #B72B1D",
b"|.    c #D0827E",
b"1.    c #E2D5C0",
b"2.    c #F2EBE8",
b"3.    c #57241F",
b"4.    c #EEE9E2",
b"5.    c #D0262E",
b"6.    c #E64E8E",
b"7.    c #D2A0B6",
b"8.    c #D95F90",
b"9.    c #A32619",
b"0.    c #E8DBCE",
b"a.    c #E3D9D1",
b"b.    c #5B2723",
b"c.    c #E9E0D7",
b"d.    c #C22824",
b"e.    c #D59BB9",
b"f.    c #E75399",
b"g.    c #232321",
b"h.    c #3F2923",
b"i.    c #592019",
b"j.    c #79252B",
b"k.    c #D2BBB8",
b"l.    c #E4D9CE",
b"m.    c #AA2A14",
b"n.    c #DEDBD8",
b"o.    c #E0DEDE",
b"p.    c #A91920",
b"q.    c #7F221B",
b"r.    c #63221B",
b"s.    c #5F292D",
b"t.    c #D589AB",
b"u.    c #BA202B",
b"v.    c #E0D3C4",
b"w.    c #912213",
b"x.    c #DD69A2",
b"y.    c #CE5A67",
b"z.    c #84211C",
b"A.    c #A21E18",
b"B.    c #D1CEC5",
b"C.    c #E64887",
b"D.    c #D53944",
b"E.    c #D9C5B5",
b"F.    c #A82918",
b"G.    c #E5E3DF",
b"H.    c #C72925",
b"I.    c #DF2947",
b"J.    c #CB5359",
b"K.    c #7D221B",
b"L.    c #C12521",
b"M.    c #E2284B",
b"N.    c #E65499",
b"O.    c #D53948",
b"P.    c #E06CAB",
b"Q.    c #CFA3A1",
b"R.    c #AD291B",
b"S.    c #E11F36",
b"T.    c #D78261",
b"U.    c #E09364",
b"V.    c #DC926A",
b"W.    c #941F1A",
b"X.    c #D8324A",
b"Y.    c #E99A65",
b"Z.    c #D1252D",
b"`.    c #5F221B",
b" +    c #CFBBC7",
b".+    c #D27C96",
b"++    c #9B2215",
b"@+    c #E7CF95",
b"#+    c #F8E1AA",
b"$+    c #FDDD9F",
b"%+    c #FDEDCB",
b"&+    c #B8281A",
b"*+    c #DC3A5C",
b"=+    c #E5A26B",
b"-+    c #D74161",
b";+    c #551E17",
b">+    c #E03A65",
b",+    c #AF2819",
b"'+    c #F7EDDD",
b")+    c #FEF8EC",
b"!+    c #FEFBF0",
b"~+    c #FCF8E2",
b"{+    c #A02316",
b"]+    c #C52825",
b"^+    c #D44866",
b"/+    c #C12A1F",
b"(+    c #70201A",
b"_+    c #E8E8E8",
b":+    c #EBE1D9",
b"<+    c #C02922",
b"[+    c #DF2138",
b"}+    c #CB232B",
b"|+    c #D2222E",
b"1+    c #C72327",
b"2+    c #CD232B",
b"3+    c #A72817",
b"4+    c #79221B",
b"5+    c #B32A1B",
b"6+    c #832121",
b"7+    c #DEDEDE",
b"8+    c #D292AA",
b"9+    c #882214",
b"0+    c #A42416",
b"a+    c #AD2715",
b"b+    c #B32917",
b"c+    c #B9271B",
b"d+    c #BD261F",
b"e+    c #A12619",
b"f+    c #3E2422",
b"g+    c #382624",
b"h+    c #49221F",
b"i+    c #7F2325",
b"j+    c #D2AABE",
b"k+    c #261818",
b"l+    c #282021",
b"m+    c #251F20",
b"n+    c #282120",
b"o+    c #2E2624",
b"p+    c #322928",
b"q+    c #362B28",
b"r+    c #362928",
b"s+    c #332528",
b"t+    c #322425",
b"u+    c #D981B1",
b"v+    c #E3DAD9",
b"w+    c #7C221E",
b"x+    c #582422",
b"y+    c #432825",
b"z+    c #3F2725",
b"A+    c #3A2A26",
b"B+    c #342828",
b"C+    c #302726",
b"D+    c #352A27",
b"E+    c #482320",
b"F+    c #4F211D",
b"G+    c #48211E",
b"H+    c #792321",
b"I+    c #ECE7E4",
b"J+    c #D0AABA",
b"K+    c #DB4367",
b"L+    c #65241F",
b"M+    c #352725",
b"N+    c #382724",
b"O+    c #3A2522",
b"P+    c #3F2421",
b"Q+    c #47211F",
b"R+    c #DA88B8",
b"                  . + @ # $ % & *               ",
b"                  = - ; > , ' ) ! ~ { ]         ",
b"                ^ / ( _ : < [ } | 1 2 3 4       ",
b"                5 6 7 8 9 0 a b c d e f g       ",
b"                h i j k l m n | o p q r s       ",
b"                t u v w x y z A B C D E         ",
b"                F G H I J K L M N O P Q         ",
b"                R S T U V W X Y Z `  ...        ",
b"                +.6 @.#.I $.%.&.*.=.-.;.        ",
b"                >.,.  '.).!.~.{.].^./.(.        ",
b"              _.:.<.      [.}.|.1.2.3.          ",
b"              4.5.6.    7.8.9.0.  a.b.          ",
b"              c.d.e.  f.g.h.i.j.  k.E           ",
b"              l.m.n.  o.p.q.r.s.  t.u.          ",
b"              v.w.    x.y.z.A.B.  C.D.          ",
b"              E.F.G.H.I.J.K.L.M.N.O.P.          ",
b"              Q.R.S.T.U.V.W.X.Y.Z.`. +          ",
b"              .+++@+#+$+%+&+*+=+-+;+o.          ",
b"              >+,+'+)+!+~+{+]+^+/+(+_+          ",
b"            :+<+[+}+|+1+2+3+4+5+/+6+7+          ",
b"            8+9+0+a+b+c+d+e+f+g+h+i+            ",
b"            j+k+l+m+n+o+o+p+q+r+s+t+u+          ",
b"            v+w+x+y+z+A+B+C+D+E+F+G+H+          ",
b"                I+J+K+g L+M+N+O+P+Q+R+          "]