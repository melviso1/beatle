# -*- coding: utf-8 -*-

_database_field = [
b"24 24 41 1",
b" 	c None",
b".	c #686565",
b"+	c #929090",
b"@	c #B8B8B8",
b"#	c #B7B7B7",
b"$	c #ABABAB",
b"%	c #848484",
b"&	c #909090",
b"*	c #939393",
b"=	c #A8A8A8",
b"-	c #AAAAAA",
b";	c #BFBFBF",
b">	c #C2C2C2",
b",	c #D6D6D6",
b"'	c #5D5A5A",
b")	c #8D8B8B",
b"!	c #AFAFAF",
b"~	c #AEAEAE",
b"{	c #ACACAC",
b"]	c #A0A0A0",
b"^	c #101010",
b"/	c #1F1F1F",
b"(	c #252525",
b"_	c #2D2D2D",
b":	c #2E2E2E",
b"<	c #B9B9B9",
b"[	c #CECECE",
b"}	c #5C5959",
b"|	c #8C8A8A",
b"1	c #B3B3B3",
b"2	c #9B9B9B",
b"3	c #9D9D9D",
b"4	c #9F9F9F",
b"5	c #B2B2B2",
b"6	c #1F1C1C",
b"7	c #2C2727",
b"8	c #2A2626",
b"9	c #292525",
b"0	c #2A2525",
b"a	c #2B2727",
b"b	c #302B2B",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"      .+@@#$%&*=-;;>,   ",
b"      ')!~{]^/(_:##<[   ",
b"      }|##1222345##<[   ",
b"      6788899990888ab   ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        "];
