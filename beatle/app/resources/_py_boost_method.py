'''
Created on 1 oct. 2018

@author: mel
'''

_py_boost_method = [
b"24 24 285 2",
b"      c None",
b".     c #C6C5F8",
b"+     c #E6C7ED",
b"@     c #895FC1",
b"#     c #BFC3F9",
b"$     c #B274D1",
b"%     c #B099F1",
b"&     c #D5C3EB",
b"*     c #B2A7E6",
b"=     c #B5ACF1",
b"-     c #AC95ED",
b";     c #ACA2F3",
b">     c #AC96EE",
b",     c #AF92F0",
b"'     c #B486EA",
b")     c #B483E6",
b"!     c #C6A2E8",
b"~     c #935FC4",
b"{     c #AF85E0",
b"]     c #BAB4F4",
b"^     c #ADA4F0",
b"/     c #AC96EB",
b"(     c #B189E5",
b"_     c #B28EEC",
b":     c #B484E5",
b"<     c #D5C7F2",
b"[     c #D8CDEE",
b"}     c #9B64C3",
b"|     c #C3B2EC",
b"1     c #BAB2F0",
b"2     c #BDB0F3",
b"3     c #AC8BE6",
b"4     c #904FB4",
b"5     c #9B61C4",
b"6     c #AB74D4",
b"7     c #B489E9",
b"8     c #B282E0",
b"9     c #AB72CC",
b"0     c #D58BBF",
b"a     c #E4ABD5",
b"b     c #C179AA",
b"c     c #B24763",
b"d     c #B33347",
b"e     c #A15CC2",
b"f     c #A950B4",
b"g     c #C8B0EF",
b"h     c #C7B9F5",
b"i     c #AF83DB",
b"j     c #A466CA",
b"k     c #9770CF",
b"l     c #A079E0",
b"m     c #B38AED",
b"n     c #B487E9",
b"o     c #DFD1F2",
b"p     c #BA75A3",
b"q     c #B43E57",
b"r     c #B3354B",
b"s     c #B43F58",
b"t     c #B23347",
b"u     c #8B4DB1",
b"v     c #AE3A6F",
b"w     c #8B58B4",
b"x     c #B09BE7",
b"y     c #CDBFF7",
b"z     c #C5BDF4",
b"A     c #AD9EE4",
b"B     c #9B77E4",
b"C     c #807ADF",
b"D     c #B38DF0",
b"E     c #B488E9",
b"F     c #AD9CE9",
b"G     c #D891C5",
b"H     c #B23448",
b"I     c #B34560",
b"J     c #B3364C",
b"K     c #B3344A",
b"L     c #B23348",
b"M     c #B13145",
b"N     c #B12D3E",
b"O     c #AF2A3C",
b"P     c #C4ACE7",
b"Q     c #C4B2EC",
b"R     c #D5CFFD",
b"S     c #C2BDF8",
b"T     c #BEB7F3",
b"U     c #B487E8",
b"V     c #B18FEE",
b"W     c #B095F1",
b"X     c #B48BEA",
b"Y     c #9B409D",
b"Z     c #BC77A6",
b"`     c #AC658A",
b" .    c #B4374E",
b"..    c #B4374D",
b"+.    c #B13044",
b"@.    c #AF2F42",
b"#.    c #AF2D40",
b"$.    c #AF2E42",
b"%.    c #AF2C43",
b"&.    c #A83864",
b"*.    c #9F263A",
b"=.    c #836E8A",
b"-.    c #CDC3FC",
b";.    c #B9B4F5",
b">.    c #A88299",
b",.    c #AB9EF1",
b"'.    c #A1708D",
b").    c #B28DEE",
b"!.    c #ACA1F2",
b"~.    c #BE78A8",
b"{.    c #B43A53",
b"].    c #B43A51",
b"^.    c #B43A52",
b"/.    c #AB628A",
b"(.    c #B2709B",
b"_.    c #AD6A92",
b":.    c #B02F42",
b"<.    c #AC3746",
b"[.    c #BABEC1",
b"}.    c #7591A2",
b"|.    c #E8E6E6",
b"1.    c #A37FC6",
b"2.    c #AC71BB",
b"3.    c #B00202",
b"4.    c #A03737",
b"5.    c #A12424",
b"6.    c #8D7C9E",
b"7.    c #B476D0",
b"8.    c #AC668E",
b"9.    c #B4729E",
b"0.    c #B43F59",
b"a.    c #B43D56",
b"b.    c #AD567A",
b"c.    c #AD6B96",
b"d.    c #B874A3",
b"e.    c #BF79AA",
b"f.    c #CD82B7",
b"g.    c #887172",
b"h.    c #9BA691",
b"i.    c #325339",
b"j.    c #8E9582",
b"k.    c #914F9D",
b"l.    c #AE68C8",
b"m.    c #B33545",
b"n.    c #AF0000",
b"o.    c #927575",
b"p.    c #B08AD5",
b"q.    c #CA7FB2",
b"r.    c #B16F99",
b"s.    c #AD5678",
b"t.    c #B43C54",
b"u.    c #B4384F",
b"v.    c #E1A4D2",
b"w.    c #E4AED7",
b"x.    c #BD77A8",
b"y.    c #C87FB3",
b"z.    c #B680A3",
b"A.    c #5D941D",
b"B.    c #609F16",
b"C.    c #6DAD20",
b"D.    c #65A01F",
b"E.    c #8776B6",
b"F.    c #994BA3",
b"G.    c #AC1717",
b"H.    c #954157",
b"I.    c #A42223",
b"J.    c #889C9C",
b"K.    c #BB76A6",
b"L.    c #AE5373",
b"M.    c #AE5475",
b"N.    c #B43950",
b"O.    c #AE5371",
b"P.    c #EFC7E7",
b"Q.    c #D289BF",
b"R.    c #C199AB",
b"S.    c #6CA825",
b"T.    c #477017",
b"U.    c #588A1D",
b"V.    c #6DAF2A",
b"W.    c #A74A48",
b"X.    c #BD95A1",
b"Y.    c #AA6FB1",
b"Z.    c #CB80B3",
b"`.    c #AE5476",
b" +    c #B975A4",
b".+    c #AC678E",
b"++    c #B43D55",
b"@+    c #AF516F",
b"#+    c #D890C5",
b"$+    c #AC587C",
b"%+    c #6C782D",
b"&+    c #6CA924",
b"*+    c #69A622",
b"=+    c #657023",
b"-+    c #AA0002",
b";+    c #8A6464",
b">+    c #E9B7DC",
b",+    c #ECBFE1",
b"'+    c #AF506F",
b")+    c #B02F43",
b"!+    c #A53F4D",
b"~+    c #963F4A",
b"{+    c #8D4931",
b"]+    c #578F1D",
b"^+    c #721112",
b"/+    c #A91112",
b"(+    c #7E8D8D",
b"_+    c #E4ACD6",
b":+    c #C079AA",
b"<+    c #AD6B93",
b"[+    c #B16F9A",
b"}+    c #B3435E",
b"|+    c #B23146",
b"1+    c #A92A34",
b"2+    c #A0323C",
b"3+    c #9D5345",
b"4+    c #1D5D1C",
b"5+    c #733834",
b"6+    c #AE1011",
b"7+    c #975050",
b"8+    c #AC668D",
b"9+    c #AE6B94",
b"0+    c #AC5D81",
b"a+    c #B43B53",
b"b+    c #B10202",
b"c+    c #9B3737",
b"d+    c #9A6F75",
b"e+    c #75853E",
b"f+    c #4C7D1B",
b"g+    c #B06E98",
b"h+    c #AC5A7D",
b"i+    c #B3445F",
b"j+    c #B23B52",
b"k+    c #934C4C",
b"l+    c #AB5F83",
b"m+    c #6E9420",
b"n+    c #12562A",
b"o+    c #757575",
b"p+    c #C97FB4",
b"q+    c #D891C6",
b"r+    c #AC5B7E",
b"s+    c #BE6188",
b"t+    c #905959",
b"u+    c #6DA823",
b"v+    c #578B21",
b"w+    c #B0A2B3",
b"x+    c #3B5447",
b"y+    c #165C20",
b"z+    c #1D5621",
b"A+    c #42711F",
b"B+    c #353C2D",
b"C+    c #D992C5",
b"D+    c #D389BD",
b"E+    c #C06289",
b"F+    c #A81010",
b"G+    c #8B8282",
b"H+    c #7AAB26",
b"I+    c #2E6519",
b"J+    c #24542E",
b"K+    c #2A6B1E",
b"L+    c #52861F",
b"M+    c #7EAA2C",
b"N+    c #5E9622",
b"O+    c #2C592B",
b"P+    c #4F6E2C",
b"Q+    c #454843",
b"R+    c #B00B0B",
b"S+    c #A80E0E",
b"T+    c #AF2D30",
b"U+    c #73420D",
b"V+    c #6FAE24",
b"W+    c #62A423",
b"X+    c #6BA71E",
b"Y+    c #6C732E",
b"Z+    c #71A824",
b"`+    c #356B1E",
b" @    c #4F612F",
b".@    c #B31515",
b"+@    c #B00303",
b"@@    c #9A3333",
b"#@    c #4F5322",
b"$@    c #56681E",
b"%@    c #726E4D",
b"&@    c #454E26",
b"*@    c #425C1A",
b"                                                ",
b"                                                ",
b"                                    .           ",
b"                            + @ # $ %   &       ",
b"                          * = - ; > , ' )       ",
b"                        ! ~ { ] ^ / ( _ : < [   ",
b"                        } | 1 2 3 4 5 6 7 8 9   ",
b"          0 a b c   d   e f g h i j k l m n o   ",
b"          p q q r s t u v w x y z A B C D E ' F ",
b"      G H I r J K L M N O P Q R S T U V W X Y   ",
b"      Z `  ...r +.@.#.$.%.&.*.=.-.;.>.,.'.).!.  ",
b"    ~.{.].....^./.(._.:.<.[.}.|.1.2.3.4.5.6.7.  ",
b"    8.9.0.a.^.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.    ",
b"  q.r.s.t.u.J v.w.x.y.z.A.B.C.D.E.F.G.H.I.J.    ",
b"    K.L.M.N.^.O.  P.Q.R.S.T.U.V.W.X.Y.          ",
b"  Z.`. +.+u.++..@+  #+$+%+&+*+=+-+;+            ",
b"  >+,+d.].'+].r K u.)+!+~+{+]+^+/+(+            ",
b"    _+:+<+[+}+J K |+N 1+2+3+4+5+6+7+            ",
b"    Z   8+9+@.0+a+N b+c+d+e+f+                  ",
b"        g+h+[+[+i+j+k+M l+m+n+                o+",
b"        p+  q+g+r+s+t+L.  u+v+w+x+y+z+A+      B+",
b"            C+D+  E+F+G+  H+I+J+K+L+M+N+O+  P+Q+",
b"                    R+S+T+U+V+W+X+Y+    Z+`+ @  ",
b"                      .@+@@@#@$@%@      &@*@    "]
