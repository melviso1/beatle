# -*- coding: utf-8 -*-

_py_init = [
b"24 24 293 2",
b"  	c None",
b". 	c #F2DAAE",
b"+ 	c #F1CF97",
b"@ 	c #EDC584",
b"# 	c #CD9D50",
b"$ 	c #A06C25",
b"% 	c #AD834E",
b"& 	c #F4E5C7",
b"* 	c #F7D2A1",
b"= 	c #F7D199",
b"- 	c #F7CB8A",
b"; 	c #E9B86E",
b"> 	c #BD8631",
b", 	c #A1650D",
b"' 	c #A0620A",
b") 	c #A16B1D",
b"! 	c #D9C098",
b"~ 	c #F8D29B",
b"{ 	c #FACE90",
b"] 	c #FACC90",
b"^ 	c #F7C885",
b"/ 	c #E7B364",
b"( 	c #BE842A",
b"_ 	c #B17317",
b": 	c #BB7E22",
b"< 	c #C1872D",
b"[ 	c #B37F27",
b"} 	c #E9D8B3",
b"| 	c #F5D1A1",
b"1 	c #F8CC8C",
b"2 	c #FBCA89",
b"3 	c #FAC683",
b"4 	c #F8C179",
b"5 	c #E4AA56",
b"6 	c #C0842D",
b"7 	c #BF812A",
b"8 	c #D69B47",
b"9 	c #E9B767",
b"0 	c #DBA754",
b"a 	c #CA9B51",
b"b 	c #E9C995",
b"c 	c #F8C882",
b"d 	c #FBC785",
b"e 	c #FCC782",
b"f 	c #FAC47B",
b"g 	c #F5BA6B",
b"h 	c #DB9E44",
b"i 	c #BC7C22",
b"j 	c #C6862D",
b"k 	c #E5B566",
b"l 	c #F3D397",
b"m 	c #F0CE8E",
b"n 	c #D1A74C",
b"o 	c #D9BF88",
b"p 	c #DBA250",
b"q 	c #F9C47D",
b"r 	c #FAC77C",
b"s 	c #FAC276",
b"t 	c #F9BC6E",
b"u 	c #F0B25C",
b"v 	c #D8973B",
b"w 	c #BA791D",
b"x 	c #CB8B30",
b"y 	c #EEC27B",
b"z 	c #F8E8BC",
b"A 	c #FAEAC6",
b"B 	c #E9C681",
b"C 	c #D1A655",
b"D 	c #B28755",
b"E 	c #E2A350",
b"F 	c #F9C37C",
b"G 	c #F9C57A",
b"H 	c #F9BF74",
b"I 	c #F4B769",
b"J 	c #ECAE59",
b"K 	c #D39236",
b"L 	c #BE7D21",
b"M 	c #D09236",
b"N 	c #F0C883",
b"O 	c #FCF0CF",
b"P 	c #FDF7E3",
b"Q 	c #F7E4B7",
b"R 	c #DEB25F",
b"S 	c #E0C795",
b"T 	c #92532E",
b"U 	c #E2A04C",
b"V 	c #F9BD73",
b"W 	c #FAC179",
b"X 	c #F5BD6F",
b"Y 	c #F0B464",
b"Z 	c #E9AA55",
b"` 	c #CE8D31",
b" .	c #D19139",
b"..	c #F2CA8B",
b"+.	c #FCF0D4",
b"@.	c #FEF8ED",
b"#.	c #FDF1DA",
b"$.	c #EDC98B",
b"%.	c #D3AC62",
b"&.	c #7C3716",
b"*.	c #DC9D44",
b"=.	c #F6BB6B",
b"-.	c #F8BF75",
b";.	c #F6BC6F",
b">.	c #E6A753",
b",.	c #C7862A",
b"'.	c #B17014",
b").	c #C8882C",
b"!.	c #EBC27B",
b"~.	c #FAECC8",
b"{.	c #FFFAEC",
b"].	c #FFF6E6",
b"^.	c #F2D69F",
b"/.	c #D1A14C",
b"(.	c #752D0A",
b"_.	c #D99B3F",
b":.	c #F5BA6A",
b"<.	c #F9C076",
b"[.	c #F7BA6E",
b"}.	c #F2B462",
b"|.	c #E5A750",
b"1.	c #C28124",
b"2.	c #A3620B",
b"3.	c #BC7D21",
b"4.	c #E4B262",
b"5.	c #F6E2B6",
b"6.	c #FEF7E7",
b"7.	c #FEF6E8",
b"8.	c #F4DBA7",
b"9.	c #D19F45",
b"0.	c #DDC692",
b"a.	c #702701",
b"b.	c #D39235",
b"c.	c #F3B461",
b"d.	c #F5BA6D",
b"e.	c #F2B362",
b"f.	c #E2A44C",
b"g.	c #BF7D22",
b"h.	c #9A5808",
b"i.	c #AA6911",
b"j.	c #D5A14B",
b"k.	c #F0D69D",
b"l.	c #FCF2D7",
b"m.	c #FEF2D8",
b"n.	c #F2D194",
b"o.	c #CC9238",
b"p.	c #D0B177",
b"q.	c #6E2000",
b"r.	c #CA8627",
b"s.	c #F0B05D",
b"t.	c #F7BC70",
b"u.	c #F3B867",
b"v.	c #EFB360",
b"w.	c #E4A54E",
b"x.	c #BC7B1D",
b"y.	c #995606",
b"z.	c #9D5B09",
b"A.	c #C48B33",
b"B.	c #EBC27A",
b"C.	c #F7E5B9",
b"D.	c #F6E2B7",
b"E.	c #E9C077",
b"F.	c #C28428",
b"G.	c #C8A96B",
b"H.	c #6D2000",
b"I.	c #C27B1E",
b"J.	c #E9AA53",
b"K.	c #F6BD6E",
b"L.	c #F4B968",
b"M.	c #C07F22",
b"N.	c #985408",
b"O.	c #945304",
b"P.	c #AE7416",
b"Q.	c #D8A147",
b"R.	c #F2C980",
b"S.	c #F4CA83",
b"T.	c #DEA550",
b"U.	c #B4781D",
b"V.	c #C4A471",
b"W.	c #6F2502",
b"X.	c #B76E16",
b"Y.	c #E4A346",
b"Z.	c #F5B969",
b"`.	c #F4B969",
b" +	c #EEB360",
b".+	c #E3A54D",
b"++	c #C07F24",
b"@+	c #975505",
b"#+	c #904D05",
b"$+	c #A6650F",
b"%+	c #C48729",
b"&+	c #D7A043",
b"*+	c #D99F44",
b"=+	c #C78729",
b"-+	c #A25F0B",
b";+	c #C4A679",
b">+	c #85461A",
b",+	c #AA5F0D",
b"'+	c #DB9A3C",
b")+	c #F2B666",
b"!+	c #F0B462",
b"~+	c #E6A84F",
b"{+	c #C48429",
b"]+	c #9F5F08",
b"^+	c #9E5F07",
b"/+	c #B07115",
b"(+	c #C18326",
b"_+	c #C08326",
b":+	c #BB7E20",
b"<+	c #AC6B17",
b"[+	c #90500A",
b"}+	c #AD7E51",
b"|+	c #9F5908",
b"1+	c #CE8E31",
b"2+	c #F0B364",
b"3+	c #F1B665",
b"4+	c #E9AB57",
b"5+	c #D19136",
b"6+	c #B67518",
b"7+	c #BD7C1E",
b"8+	c #D29231",
b"9+	c #D69635",
b"0+	c #BE7F23",
b"a+	c #AA690F",
b"b+	c #904E09",
b"c+	c #9F6F33",
b"d+	c #9B5C0D",
b"e+	c #BA7A1E",
b"f+	c #E5A953",
b"g+	c #F2B767",
b"h+	c #F3B96E",
b"i+	c #EFB563",
b"j+	c #DFA349",
b"k+	c #D6983E",
b"l+	c #DC9F44",
b"m+	c #E7AA50",
b"n+	c #E3A54B",
b"o+	c #C28226",
b"p+	c #94500C",
b"q+	c #713201",
b"r+	c #CAB081",
b"s+	c #B99061",
b"t+	c #A86811",
b"u+	c #D59842",
b"v+	c #F1B96D",
b"w+	c #F4BC72",
b"x+	c #F0B767",
b"y+	c #EBB35E",
b"z+	c #E8AF59",
b"A+	c #EBB25D",
b"B+	c #EFB45F",
b"C+	c #E9AC54",
b"D+	c #BA7922",
b"E+	c #712F03",
b"F+	c #996731",
b"G+	c #A87C3A",
b"H+	c #BC812C",
b"I+	c #E9B267",
b"J+	c #F0BA71",
b"K+	c #F1BA6E",
b"L+	c #EBB564",
b"M+	c #EBB360",
b"N+	c #EBB35F",
b"O+	c #E9B058",
b"P+	c #D4983D",
b"Q+	c #904E0B",
b"R+	c #804816",
b"S+	c #DBC49C",
b"T+	c #BB9050",
b"U+	c #DEA45C",
b"V+	c #EBBB74",
b"W+	c #EDBA70",
b"X+	c #E4AE5E",
b"Y+	c #DDA64F",
b"Z+	c #D29A42",
b"`+	c #BC832D",
b" @	c #9A5E10",
b".@	c #9C6319",
b"+@	c #DBC293",
b"@@	c #DFC794",
b"#@	c #E8BE82",
b"$@	c #E6B66F",
b"%@	c #D8A04F",
b"&@	c #B67B26",
b"*@	c #9F6412",
b"=@	c #9F6A16",
b"-@	c #B78E43",
b";@	c #DEC79F",
b">@	c #EBD5AA",
b",@	c #DFBE85",
b"'@	c #C39E5A",
b")@	c #C9A467",
b"!@	c #D9BD8D",
b"                                                ",
b"                  . + @ # $ %                   ",
b"              & * = - ; > , ' ) !               ",
b"              ~ { ] ^ / ( _ : < [ }             ",
b"            | 1 2 3 4 5 6 7 8 9 0 a             ",
b"          b c d e f g h i j k l m n o           ",
b"          p q r s t u v w x y z A B C           ",
b"        D E F G H I J K L M N O P Q R S         ",
b"        T U V W X Y Z ` w  ...+.@.#.$.%.        ",
b"        &.*.=.-.;.Y >.,.'.).!.~.{.].^./.        ",
b"        (._.:.<.[.}.|.1.2.3.4.5.6.7.8.9.0.      ",
b"        a.b.c.V d.e.f.g.h.i.j.k.l.m.n.o.p.      ",
b"        q.r.s.t.u.v.w.x.y.z.A.B.C.D.E.F.G.      ",
b"        H.I.J.K.L.v.w.M.N.O.P.Q.R.S.T.U.V.      ",
b"        W.X.Y.Z.`. +.+++@+#+$+%+&+*+=+-+;+      ",
b"        >+,+'+)+`.!+~+{+]+^+/+(+_+:+<+[+        ",
b"        }+|+1+2+:.3+4+5+6+7+8+9+0+a+b+c+        ",
b"          d+e+f+g+h+i+j+k+l+m+n+o+p+q+r+        ",
b"          s+t+u+v+w+x+y+z+A+B+C+D+E+F+          ",
b"            G+H+I+J+K+L+M+N+O+P+Q+R+S+          ",
b"              T+U+V+W+X+Y+Z+`+ @.@+@            ",
b"                @@#@$@%@&@*@=@-@;@              ",
b"                    >@,@'@)@!@                  ",
b"                                                "]
