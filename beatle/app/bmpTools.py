import wx


def join_bitmap(left_bitmap, right_bitmap):
    """Create a new bitmap by joining"""
    width = left_bitmap.GetWidth() + right_bitmap.GetWidth()
    height = max(left_bitmap.GetHeight(), right_bitmap.GetHeight())
    left_offset = (height - left_bitmap.GetHeight()) // 2
    right_offset = (height - right_bitmap.GetHeight()) // 2
    image = wx.Bitmap(width, height)
    dc = wx.MemoryDC(image)
    MASK = (0, 0, 1)
    dc.SetBackground(wx.Brush(MASK))
    dc.Clear()
    dc.DrawBitmap(left_bitmap, 0, left_offset, True)
    dc.DrawBitmap(right_bitmap, left_bitmap.GetWidth(), right_offset, True)
    del dc
    image.SetMaskColour(MASK)
    return image


def prefix_bitmap_list(prefix_bitmap, bitmap_list, bitmap_result):
    """
    Prefixes a list of bitmaps with another bitmap
    and increases the original list.
    A map original_index:new_index is created and returned
    where original_index is the index in bitmap_result
    bitmap_list must be a subset of bitmap_result
    """
    new_map = {}
    i = 0
    j = len(bitmap_result)
    for bitmap in bitmap_list:
        prefixed_bitmap = join_bitmap(prefix_bitmap, bitmap)
        bitmap_result.append(prefixed_bitmap)
        new_map[i] = j
        i = i + 1
        j = j + 1
    return new_map


def suffix_bitmap_list(suffix_bitmap, bitmap_list, bitmap_result):
    """
    Suffixes a list of bitmaps with another bitmap
    and increases the original list.
    A map original_index:new_index is created and returned
    where original_index is the index in bitmap_result
    bitmap_list must be a subset of bitmap_result
    """
    new_map = {}
    i = 0
    j = len(bitmap_result)
    for bitmap in bitmap_list:
        suffixed_bitmap = join_bitmap(bitmap, suffix_bitmap)
        bitmap_result.append(suffixed_bitmap)
        new_map[i] = j
        i = i + 1
        j = j + 1
    return new_map
    for bitmap in first_map:
        new_map[bitmap_list.index(bitmap)] = bitmap_result.index(
            first_map[bitmap])
    return new_map


def get_bitmap_max_size(bitmap_list):
    """
    Gets the maximum width and height for a bitmap list
    """
    width = 0
    height = 0
    for bmp in bitmap_list:
        width = max(width, bmp.GetWidth())
        height = max(height, bmp.GetHeight())
    return width, height
