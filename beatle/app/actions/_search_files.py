# coding=utf-8

""" 
    Search in files
    ===============
    
    The class search_files implements a search thread that post results to the main
    Window.
    
    Arguments
    =========
    
    iterable        : iterable returning full file paths to search
    expression      : literal or regexpr to search
    case_sensitive  : True (default) if the search is case case sensitive of False if not.
    regexpr         : False (default) if the search is literal or True if it is a regular expression.
    
    
    Results
    =======
    
    The search results are posted to the application event manager as SearchEvents
    
    
"""

import os, re, threading

from beatle.lib import wxx
from beatle.lib.api import context


class search_files(object):
    """Do the search"""
        
    def __init__(self, iterable, expression, case_sensitive=True, regexpr=False ):
        super(search_files, self).__init__()
        self._iterable = iterable
        self._expression = expression
        self._case_sensitive = case_sensitive
        self._regexpr = regexpr
        self._cancel_thread = False
        context.add_end_application_notify(self.cancel_search)
        self.___run___()
            
    def cancel_search(self):
        if self._thread.is_alive:
            self._cancel_thread = True
            self._thread.join()
            
    def ___run___(self):
        """start the search thread"""
        self._thread = threading.Thread(target=self.___search___).start()

    def ___search___(self,**kwargs):
        """Search loop"""        
        handler = context.get_frame().GetEventHandler()
        if self._regexpr:
            if not self._case_sensitive:
                expression = re.compile(self._expression,re.IGNORECASE)
            else:
                expression = re.compile(self._expression)
        else:
            if self._case_sensitive:
                expression = self._expression
            else:
                expression = self._expression.upper()
        while not self._cancel_thread:
            results = False
            try:
                target = next(self._iterable)
            except StopIteration:  # end of loop
                self._cancel_thread = True
                continue
            if not os.access(target, os.R_OK):
                # print('skip file with no access: {}'.format(f))
                continue
            try:
                matches = []
                with open(target, "r") as f:
                    line_counter = 0
                    for line in f:
                        line_counter = line_counter + 1
                        if self._regexpr:
                            if expression.search(line) is None:
                                continue
                            matches.append((line_counter, line.strip()))
                        else:
                            if self._case_sensitive:
                                if expression not in line:
                                    continue
                                matches.append((line_counter,line.strip()))
                            else:
                                if expression not in line.upper():
                                    continue
                                matches.append((line_counter,line.strip()))
                    if len(matches) > 0:
                        handler.AddPendingEvent(wxx.SearchEvent(target, matches))
            except Exception as e:
                pass
        context.cancel_end_application_notify(self.cancel_search)
      
            
        
