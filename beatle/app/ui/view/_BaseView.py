# -*- coding: utf-8 -*-

"""
This class allows to set some common handlers and methods for
easy view declaration"""

import wx
from wx import aui
import beatle
from beatle import model, local_path
from beatle.lib import wxx
from beatle.lib.decorators import class_property
from beatle.lib.tran import TransactionalMethod, TransactionStack
from beatle.lib.api import context
from beatle.app.ui import dlg
from beatle.activity.models.ui import dlg as mdlg
from beatle.lib.handlers import identifier
from beatle.app.ui.tools import edit


class BaseView(object):
    """Declares a base view class"""

    _saveWorkspace = identifier("ID_SAVE_WORKSPACE")
    _closeWorkspace = identifier("ID_CLOSE_WORKSPACE")
    _closeProject = identifier("ID_CLOSE_PROJECT")
    _save_project = identifier("ID_SAVE_PROJECT")
    _editProperties = identifier("ID_EDIT_PROPERTIES")
    _editDelete = identifier("ID_DELETE")
    _newProject = identifier("ID_NEW_PROJECT")
    _newProjectAui = identifier("ID_NEW_PROJECT_AUI")  # prevent AUI toolbar interference
    _openProject = identifier("ID_OPEN_PROJECT")
    _openProjectAui = identifier("ID_OPEN_PROJECT_AUI")  # prevent AUI toolbar interference
    _importProject = identifier("ID_IMPORT_PROJECT")
    _editCopy = identifier("ID_COPY")
    _editCut = identifier("ID_CUT")
    _editPaste = identifier("ID_PASTE")

    # Resources
    _enabled_breakpoint = None
    _disabled_breakpoint = None

    # class variables defines installed handlers
    workspace_handler = True
    project_handler = True
    clipboard_handler = True
    build_handler = False
    run_handler = False
    debug_handler = False

    # command ids: tree
    _leftKeyId = wx.Window.NewControlId()

    # command ids: build
    _buildProjectId = identifier("ID_BUILD_PROJECT")
    _cleanProjectId = identifier("ID_CLEAN_PROJECT")

    # command ids: run
    _runProjectId = identifier("ID_RUN_PROJECT")
    _runFileId = identifier("ID_RUN_FILE")
    _stopRunId = identifier("ID_STOP_RUN")
    _addPathToConsoleId = identifier("ID_ADD_PATH_TO_CONSOLE")

    # command ids: debug
    _debug_attach_id = identifier("ID_DEBUG_ATTACH")
    _toggleBreakpointId = identifier("ID_TOGGLE_BREAKPOINT")
    _enableBreakpointId = identifier("ID_ENABLE_BREAKPOINT")
    _disableBreakpointId = identifier("ID_DISABLE_BREAKPOINT")
    _deleteBreakpointId = identifier("ID_DELETE_BREAKPOINT")

    _debugProjectId = identifier("ID_DEBUG_PROJECT")
    _debugFileId = identifier("ID_DEBUG_FILE")
    _debugContinueId = identifier('ID_DEBUG_CONTINUE')
    _debugStepOverId = identifier('ID_DEBUG_STEP_OVER')
    _debugStepIntoId = identifier('ID_DEBUG_STEP_INTO')
    _debugStepOutId = identifier('ID_DEBUG_STEP_OUT')
    _debugStopId = identifier('ID_DEBUG_STOP')

    # update_context : these properties will be used for update menus
    @property
    def can_edit_properties(self):
        """Are the properties of the selected element editable?"""
        if self.selected and not self.selected.read_only:
            return True
        return False

    @property
    def aggregated_preferences(self):
        from beatle.app.ui.tools import BasePreferences
        return BasePreferences(self.selected)

    @class_property
    def enabled_breakpoint(cls):
        if cls._enabled_breakpoint is None:
            cls._enabled_breakpoint = wx.Bitmap(local_path("app/res/breakpoint.xpm"), wx.BITMAP_TYPE_ANY)
        return cls._enabled_breakpoint

    @class_property
    def disabled_breakpoint(cls):
        if cls._disabled_breakpoint is None:
            cls._disabled_breakpoint = wx.Bitmap(local_path("app/res/breakpoint_disabled.xpm"), wx.BITMAP_TYPE_ANY)
        return cls._disabled_breakpoint

    def __init__(self, *args, **kwargs):
        """Initialize base view"""
        super(BaseView, self).__init__(*args, **kwargs)
        self._evtHandler = wx.EvtHandler()
        self._handler_installed = False
        self._registered_menus = {}
        self._registered_toolbars = []
        self._perspective_base = None
        self.selected = None
        # conditionals:
        self._submenuDebug = None

    def _create_menus(self):
        """Create the base view menus"""
        if self.project_handler:
            self._menuProject = wxx.Menu()
            previous = False
            if self.build_handler:
                self._buildProject = wxx.MenuItem(
                    self._menuProject, self._buildProjectId, u"Build\tCtrl+B",
                    u"build current project.", wx.ITEM_NORMAL)
                self._buildProject.SetBitmap(wx.Bitmap(local_path("app/res/build.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._buildProject)
                self._cleanProject = wxx.MenuItem(
                    self._menuProject, self._cleanProjectId, u"Clean\tCtrl+Shift+B",
                    u"clean current project.", wx.ITEM_NORMAL)
                self._cleanProject.SetBitmap(wx.Bitmap(local_path("app/res/clean.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._cleanProject)
                previous = True
            if self.run_handler:
                if previous:
                    self._menuProject.AppendSeparator()
                previous = True
                # run project
                self._runProject = wxx.MenuItem(
                    self._menuProject, self._runProjectId, u"Run project",
                    u"run current project.", wx.ITEM_NORMAL)
                self._runProject.SetBitmap(wx.Bitmap(local_path("app/res/run.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._runProject)
                # run file
                self._runFile = wxx.MenuItem(
                    self._menuProject, self._runFileId, u"Run file",
                    u"run current file", wx.ITEM_NORMAL)
                self._runFile.SetBitmap(wx.Bitmap(local_path("app/res/run_file.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._runFile)
                # stop run
                self._stopRun = wxx.MenuItem(
                    self._menuProject, self._stopRunId, u"Stop execution",
                    u"stop running project.", wx.ITEM_NORMAL)
                self._stopRun.SetBitmap(wx.Bitmap(local_path("app/res/stop.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._stopRun)

            if self.debug_handler:
                if previous:
                    self._menuProject.AppendSeparator()
                previous = True
                self._debug_attach = wxx.MenuItem(
                    self._menuProject, self._debug_attach_id, u"Debug process",
                    u"attach debugger to running process", wx.ITEM_NORMAL)
                self._menuProject.AppendItem(self._debug_attach)
                # debug project
                self._debugProject = wxx.MenuItem(
                    self._menuProject, self._debugProjectId, u"Debug project",
                    u"debug current project.", wx.ITEM_NORMAL)
                self._debugProject.SetBitmap(wx.Bitmap(local_path("app/res/debug.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._debugProject)
                # debug file
                self._debugFile = wxx.MenuItem(
                    self._menuProject, self._debugFileId, u"Debug file",
                    u"debug current file.", wx.ITEM_NORMAL)
                self._debugFile.SetBitmap(wx.Bitmap(local_path("app/res/debug_file.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._debugFile)

                self._menuProject.AppendSeparator()
                self._toggleBreakpoint = wxx.MenuItem(
                    self._menuProject, self._toggleBreakpointId, u"Toggle breakpoint\tF9",
                    u"toggle breakpoint.", wx.ITEM_CHECK)
                self._toggleBreakpoint.SetBitmap(self.enabled_breakpoint)
                self._menuProject.AppendItem(self._toggleBreakpoint)
                # debug continue
                self._debugContinue = wxx.MenuItem(
                    self._menuProject, self._debugContinueId,
                    u"Debug continue\tF5", u"continue execution.", wx.ITEM_NORMAL)
                self._debugContinue.SetBitmap(wx.Bitmap(local_path("app/res/continue.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._debugContinue)
                # debug step over
                self._debugStepOver = wxx.MenuItem(
                    self._menuProject, self._debugStepOverId,
                    u"Debug step over\tF6", u"execute step over.", wx.ITEM_NORMAL)
                self._debugStepOver.SetBitmap(wx.Bitmap(local_path("app/res/step_over.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._debugStepOver)
                # debug step into
                self._debugStepInto = wxx.MenuItem(
                    self._menuProject, self._debugStepIntoId,
                    u"Debug step into\tF7", u"execute step into.", wx.ITEM_NORMAL)
                self._debugStepInto.SetBitmap(wx.Bitmap(local_path("app/res/step_into.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._debugStepInto)
                # debug step out
                self._debugStepOut = wxx.MenuItem(
                    self._menuProject, self._debugStepOutId,
                    u"Debug step out\tF8", u"execute step out.", wx.ITEM_NORMAL)
                self._debugStepOut.SetBitmap(wx.Bitmap(local_path("app/res/step_out.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._debugStepOut)
                # debug stop
                self._debugStop = wxx.MenuItem(
                    self._menuProject, self._debugStopId, u"Debug stop",
                    u"close debugger session.", wx.ITEM_NORMAL)
                self._debugStop.SetBitmap(wx.Bitmap(local_path("app/res/stop.xpm"), wx.BITMAP_TYPE_ANY))
                self._menuProject.AppendItem(self._debugStop)

            # TODO : this looks ugly here... were to put this?
            self._addPathToConsole = wxx.MenuItem(
                 self._menuProject, self._addPathToConsoleId, u"Add path to console",
                 u"add the project path to console", wx.ITEM_NORMAL)
            self._menuProject.AppendItem(self._addPathToConsole)

            self.append_project_menus(self._menuProject)  # give the opportunity to views for adding here

            self.register_menu('Project', self._menuProject)

    def append_project_menus(self, menu):
        """This is a place holder for views that would add custom
        items to project menu."""
        pass

    def register_menu(self, name, menu):
        """Appends a dynamic menu to the view, This menu will be
        shown only when the view becomes active"""
        self._registered_menus[name] = menu

    def unregister_menu(self, name):
        if name in self._registered_menus:
            del self._registered_menus[name]

    def register_toolbar(self, toolbar):
        """Appends a dynamic toolbar to the view, This toolbar will be
        shown only when the view becomes active"""
        self._registered_toolbars.append(toolbar)

    def bind_special(self, evtID, handler, source=None, id=-1, id2=-1):
        """Do a special binding for pluggable event handler and also internal event handler"""
        self._evtHandler.Bind(evtID, handler, source=source, id=id, id2=id2)
        self.Bind(evtID, handler, source=source, id=id, id2=id2)

    def _delete_toolbars(self):
        """Destroy associated toolbard"""
        while len(self._registered_toolbars):
            self._registered_toolbars.pop().Destroy()

    def _create_toolbars(self):
        """Create the associated toolbars"""
        # common elements
        if self.build_handler:
            self._barBuild = aui.AuiToolBar(self.frame, wx.ID_ANY, wx.DefaultPosition,
                wx.Size(16,16), style=aui.AUI_TB_GRIPPER | aui.AUI_TB_HORZ_LAYOUT|aui.AUI_TB_PLAIN_BACKGROUND)
            els = [self._buildProject, self._cleanProject]
            bar = self._barBuild
            bar.SetMinSize(wx.Size(16, 16))
            for el in els:
                bar.AddTool(el.GetId(), el.GetLabel(), el.GetBitmap(), short_help_string=el.GetHelp())
            bar.Realize()
            name = "build_toolbar_{0}".format(self.__class__)
            self.frame.m_mgr.AddPane(bar, aui.AuiPaneInfo().Name(name).Top().Hide().
                Caption(u"build").PinButton(True).Gripper().Dock().Resizable().
                DockFixed(False).Row(0).Position(3).Layer(10).ToolbarPane())
            self.register_toolbar(bar)
        if self.run_handler:
            self._barRun = aui.AuiToolBar(self.frame, wx.ID_ANY, wx.DefaultPosition,
                wx.Size(16,16), style=aui.AUI_TB_GRIPPER | aui.AUI_TB_HORZ_LAYOUT|aui.AUI_TB_PLAIN_BACKGROUND)
            els = [self._runFile, self._runProject, self._stopRun]
            bar = self._barRun
            bar.SetMinSize(wx.Size(16, 16))
            for el in els:
                bar.AddTool(el.GetId(), el.GetLabel(), el.GetBitmap(), short_help_string=el.GetHelp())
            bar.Realize()
            name = "run_toolbar_{0}".format(self.__class__)
            self.frame.m_mgr.AddPane(bar, aui.AuiPaneInfo().Name(name).Top().Hide().
                Caption(u"run").PinButton(True).Gripper().Dock().Resizable().
                DockFixed(False).Row(0).Position(4).Layer(10).ToolbarPane())
            self.register_toolbar(bar)

        if self.debug_handler:
            self._barDebug = aui.AuiToolBar(self.frame, wx.ID_ANY, wx.DefaultPosition,
                wx.Size(16,16), style=aui.AUI_TB_GRIPPER | aui.AUI_TB_HORZ_LAYOUT|aui.AUI_TB_PLAIN_BACKGROUND)

            els = [self._debugFile, self._debugProject, self._debugContinue,
                self._debugStepInto, self._debugStepOver, self._debugStepOut,
                self._debugStop]
            bar = self._barDebug
            bar.SetMinSize(wx.Size(16, 16))
            for el in els:
                bar.AddTool(el.GetId(), el.GetLabel(), el.GetBitmap(), short_help_string=el.GetHelp())
            bar.AddSeparator()
            bar.AddTool(
                self._toggleBreakpointId, "breakpoint",
                self.enabled_breakpoint,
                kind=wx.ITEM_CHECK,short_help_string="toggle breakpoint")
            bar.Realize()
            name = "debug_toolbar_{0}".format(self.__class__)
            self.frame.m_mgr.AddPane(bar, aui.AuiPaneInfo().Name(name).Top().Hide().
                Caption(u"debug").PinButton(True).Gripper().Dock().Resizable().
                DockFixed(False).Row(0).Position(5).Layer(10).ToolbarPane())
            self.register_toolbar(bar)

    def _bind_events(self):
        """Connect common handlers"""
        frame = context.get_frame()
        if self.workspace_handler:
            self.bind_special(wx.EVT_MENU, self.on_save_workspace, id=self._saveWorkspace)
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_save_workspace, id=self._saveWorkspace)
            self.bind_special(wx.EVT_MENU, self.on_close_workspace, id=self._closeWorkspace)
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_close_workspace, id=self._closeWorkspace)
            self.bind_special(wx.EVT_MENU, self.on_close_project, id=self._closeProject)
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_close_project, id=self._closeProject)

        if self.build_handler:
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_build_project, id=self._buildProjectId)
            self.bind_special(wx.EVT_MENU, self.on_build_project, id=self._buildProjectId)
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_build_project, id=self._cleanProjectId)
            self.bind_special(wx.EVT_MENU, self.on_clean_project, id=self._cleanProjectId)

        if self.project_handler:
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_save_project, id=self._save_project)
            self.bind_special(wx.EVT_MENU, self.on_save_project, id=self._save_project)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_add_path_to_console, id=self._addPathToConsoleId)
            self.bind_special(wx.EVT_MENU, self.on_add_path_to_console, id=self._addPathToConsoleId)

        if self.run_handler:
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_run_project, id=self._runProjectId)
            self.bind_special(wx.EVT_MENU, self.on_run_project, id=self._runProjectId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_run_file, id=self._runFileId)
            self.bind_special(wx.EVT_MENU, self.on_run_file, id=self._runFileId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_stop_run, id=self._stopRunId)
            self.bind_special(wx.EVT_MENU, self.on_stop_run, id=self._stopRunId)

        if self.debug_handler:
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_attach, id=self._debug_attach_id)
            self.bind_special(wx.EVT_MENU, self.on_debug_attach, id=self._debug_attach_id)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_toggle_breakpoint, id=self._toggleBreakpointId)
            self.bind_special(wx.EVT_MENU, self.on_toggle_breakpoint, id=self._toggleBreakpointId)

            # hidden commands handled in debu session
            self.bind_special(wx.EVT_MENU, self.on_enable_breakpoint, id=self._enableBreakpointId)
            self.bind_special(wx.EVT_MENU, self.on_disable_breakpoint, id=self._disableBreakpointId)
            self.bind_special(wx.EVT_MENU, self.on_delete_breakpoint, id=self._deleteBreakpointId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_project, id=self._debugProjectId)
            self.bind_special(wx.EVT_MENU, self.on_debug_project, id=self._debugProjectId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_file, id=self._debugFileId)
            self.bind_special(wx.EVT_MENU, self.on_debug_file, id=self._debugFileId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_continue, id=self._debugContinueId)
            self.bind_special(wx.EVT_MENU, self.on_debug_continue, id=self._debugContinueId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_step_over, id=self._debugStepOverId)
            self.bind_special(wx.EVT_MENU, self.on_debug_step_over, id=self._debugStepOverId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_step_into, id=self._debugStepIntoId)
            self.bind_special(wx.EVT_MENU, self.on_debug_step_into, id=self._debugStepIntoId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_step_out, id=self._debugStepOutId)
            self.bind_special(wx.EVT_MENU, self.on_debug_step_out, id=self._debugStepOutId)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_debug_stop, id=self._debugStopId)
            self.bind_special(wx.EVT_MENU, self.on_debug_stop, id=self._debugStopId)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_edit_properties, id=self._editProperties)
        self.bind_special(wx.EVT_MENU, self.on_edit_properties, id=self._editProperties)

        self.Bind(wx.EVT_UPDATE_UI, self.on_update_delete, id=self._editDelete)
        self.Bind(wx.EVT_MENU, self.on_delete, id=self._editDelete)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_new_project, id=self._newProject)
        self.bind_special(wx.EVT_MENU, self.on_new_project, id=self._newProject)
        self.bind_special(wx.EVT_MENU, self.on_new_project_aui, id=self._newProjectAui)

        self.bind_special(wx.EVT_MENU, self.on_open_project, id=self._openProject)
        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_open_project, id=self._openProject)
        self.bind_special(wx.EVT_MENU, self.on_open_project_aui, id=self._openProjectAui)

        self.bind_special(wx.EVT_UPDATE_UI, self.on_update_import_project, id=self._importProject)
        self.bind_special(wx.EVT_MENU, self.on_import_project, id=self._importProject)

        self.bind_special(wx.EVT_MENU, self.on_show_view_toolbars, id=frame.m_view_showToolbars.GetId())

        # if the view has transactional behavior, activate edit handlers
        if self.clipboard_handler:
            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_edit_copy, id=self._editCopy)
            self.bind_special(wx.EVT_MENU, self.on_edit_copy, id=self._editCopy)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_edit_cut, id=self._editCut)
            self.bind_special(wx.EVT_MENU, self.on_edit_cut, id=self._editCut)

            self.bind_special(wx.EVT_UPDATE_UI, self.on_update_edit_paste, id=self._editPaste)
            self.bind_special(wx.EVT_MENU, self.on_edit_paste, id=self._editPaste)

        self.bind_special(wx.EVT_MENU, self.frame.on_open_mru_file, None, wx.ID_FILE1, wx.ID_FILE9)

        # bind special navigation facility
        self.Bind(wx.EVT_MENU, self.on_tree_left_key, id=BaseView._leftKeyId)

    # properties
    def debug_menu(self):
        return self._submenuDebug

    # default handlers
    def on_update_run_project(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_run_file(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_stop_run(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_attach(self, event):
        event.Enable(False)

    def on_debug_attach(self, event):
        pass

    def on_update_toggle_breakpoint(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_project(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_file(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_continue(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_step_over(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_step_into(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_step_out(self, event):
        """default event handler"""
        event.Enable(False)

    def on_update_debug_stop(self, event):
        """default event handler"""
        event.Enable(False)

    def on_run_project(self, event):
        """default event handler"""
        pass

    def on_run_file(self, event):
        """default event handler"""
        pass

    def on_stop_run(self, event):
        """default event handler"""
        pass

    def on_debug_project(self, event):
        """default event handler"""
        pass

    def on_toggle_breakpoint(self, event):
        """default event handler"""
        pass

    def on_enable_breakpoint(self, event):
        """default event handler"""
        pass

    def on_disable_breakpoint(self, event):
        """default event handler"""
        pass

    def on_delete_breakpoint(self, event):
        """default event handler"""
        pass

    def on_debug_file(self, event):
        """default event handler"""
        pass

    def on_debug_continue(self, event):
        """default event handler"""
        pass

    def on_debug_step_over(self, event):
        """default event handler"""
        pass

    def on_debug_step_into(self, event):
        """default event handler"""
        pass

    def on_debug_step_out(self, event):
        """default event handler"""
        pass

    def on_debug_stop(self, event):
        """default event handler"""
        pass

    def on_update_edit_properties(self, event):
        """Handle update edit properties"""
        event.Enable(self.can_edit_properties)

    def on_edit_properties(self, event):
        """Handle edit properties method"""
        if self.can_edit_properties:
            edit(context.get_frame(), self.selected)

    def on_update_save_workspace(self, event):
        """Handle update save project"""
        event.Enable(bool(self.selected and self.selected.workspace))

    def on_save_workspace(self, event):
        """Handle save project"""
        this_workspace = self.selected and self.selected.workspace
        if this_workspace is not None:
            app = context.get_app()
            app.save_workspace(this_workspace)

    def on_update_close_workspace(self, event):
        """Handle update close workspace"""
        event.Enable(bool(self.selected and self.selected.workspace))

    @TransactionalMethod('close workspace')
    def on_close_workspace(self, event):
        """Handle save project"""
        wrk = self.selected and self.selected.workspace
        if wrk is None:
            return
        TransactionStack.do_unload(wrk)
        return True

    def on_update_close_project(self, event):
        """Handle update close workspace"""
        event.Enable(bool(self.selected and self.selected.project))

    @TransactionalMethod('close project')
    def on_close_project(self, event):
        """Handle save project"""
        prj = self.selected and self.selected.project
        if prj is None:
            return
        TransactionStack.do_unload(prj)
        return True

    def on_update_build_project(self, event):
        """Update build project"""
        s = self.selected
        s = s and s.project
        build = (s and s.buildable) or False
        event.Enable(build)

    def on_build_project(self, event):
        pass

    def on_clean_project(self, event):
        pass

    def on_update_save_project(self, event):
        """Update save project method"""
        selected = self.selected
        if selected is None:
            event.Enable(False)
        else:
            project = selected.project
            event.Enable(project and project.modified or False)

    def on_save_project(self, event):
        """Handle save project"""
        this_project = self.selected and self.selected.project
        if this_project is not None:
            app = context.get_app()
            app.save_project(this_project)

    def on_update_add_path_to_console(self, event):
        """Update save project method"""
        event.Enable(False)

    def on_add_path_to_console(self, event):
        """Update save project method"""
        pass

    def on_update_new_project(self, event):
        """Update add project"""
        obj = self.selected
        if obj is None or obj.inner_project_container is None:
            event.Enable(False)
        else:
            event.Enable(True)

    @TransactionalMethod('new project {0}')
    @wxx.CreationDialog(mdlg.ProjectDialog, model.Project)
    def on_new_project(self, event):
        """Handle new project command"""
        obj = self.selected
        if obj is None or obj.inner_project_container is None:
            return False
        return context.get_frame(), self.selected and self.selected.inner_project_container

    def on_new_project_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._newProject)
        wx.PostEvent(self.GetEventHandler(), event_new)

    def on_update_open_project(self, event):
        """Handle update edit properties"""
        event.Enable(bool(self.selected and self.selected.inner_project_container))

    def on_open_project_aui(self, event):
        event_new = wx.PyCommandEvent(wx.EVT_MENU.typeId, self._openProject)
        wx.PostEvent(self.GetEventHandler(), event_new)

    @TransactionalMethod('open project')
    def on_open_project(self, event):
        """Handle open project method"""
        path = wx.EmptyString
        v = self.selected
        if v:
            workspace = v.workspace
            if workspace:
                path = workspace.dir
        else:
            workspace = None
        dialog = wx.FileDialog(self, "Select project file", path,
            wx.EmptyString, "Project files (*.pcc)|*.pcc", wx.FD_OPEN)
        if dialog.ShowModal() != wx.ID_OK:
            return None
        fname = dialog.GetPath()
        app = context.get_app()
        project = app.load_project(fname, workspace=workspace)
        if project:
            TransactionStack.do_load(project)
            if not workspace:
                mru = context.get_mru()
                mru.AddFileToHistory(fname)
            project.modified = False
        return project

    def on_update_import_project(self, event):
        """Update import procjec"""
        obj = self.selected
        event.Enable(bool(obj and obj.workspace))

    @TransactionalMethod('import project')
    def on_import_project(self, event):
        """Handle import project event"""
        frame = context.get_frame()
        dialog = dlg.ImportProjectDialog(frame, self.selected.workspace)
        if dialog.ShowModal() == wx.ID_OK:
            # create modeless dialog
            progress = None
            try:
                progress = dlg.WaitDialog(frame)
                progress.InitDialog()
                progress.Show(True)
                progress.Centre()
                progress.Refresh()
                progress.Update()
                v = dialog.DoImport(progress)
                progress.Close()
                return v
            except:
                progress.Close()
        return False

    def install_handler(self):
        """Installs the event handler"""
        frame = context.get_frame()
        if self._handler_installed:
            return False  # already installed
        frame.PushEventHandler(self._evtHandler)
        self._handler_installed = True
        return True  # ok, installed

    def remove_handler(self):
        """Remove the event handler"""
        frame = context.get_frame()
        if not self._handler_installed:
            return False  # already removed
        # pop event handlers until found this
        other_event_handlers = []
        event_handler = frame.PopEventHandler()
        try:
            while event_handler != self._evtHandler:
                other_event_handlers.append(event_handler)
                event_handler = frame.PopEventHandler()
            if event_handler == self._evtHandler:
                self._handler_installed = False
            result = True
        except Exception as e:
            print("Error : {}".format(e))
            self._handler_installed = False
            result = False
        # reinstall the event handlers chain
        while len(other_event_handlers):
            frame.PushEventHandler(other_event_handlers.pop())
        return result

    def notify_show(self):
        """Called where the view is show"""
        frame = context.get_frame()
        if not self.install_handler():
            return  # avoid extra calls
        for menu in self._registered_menus:
            self.frame.add_main_menu(menu, self._registered_menus[menu])
        if self._perspective_base is None:
            # if there is not a base perspective, we create one
            for toolbar in self._registered_toolbars:
                frame.m_mgr.GetPane(toolbar).Show()
                toolbar.Realize()
            self._perspective_base = frame.m_mgr.SavePerspective()
        cls = type(self)
        if cls.perspective:
            frame.m_mgr.LoadPerspective(cls.perspective)
            # frame.m_mgr.Update()
        frame.load_views_perspective()
        frame.m_mgr.Update()
        # wx.YieldIfNeeded()
        self.SetFocus()

    def notify_hide(self):
        """Called where the view is hidden"""
        if not self.remove_handler():
            return  # avoid extra calls
        # store current perspective for later use
        cls = type(self)
        frame = context.get_frame()
        cls.perspective = frame.m_mgr.SavePerspective()
        frame.save_views_perspective()
        for menu in self._registered_menus:
            self.frame.remove_main_menu(menu, self._registered_menus[menu])
        changes = False
        for toolbar in self._registered_toolbars:
            frame.m_mgr.GetPane(toolbar).Hide()
            toolbar.Realize()
            changes = True
        if changes:
            frame.m_mgr.Update()
            # wx.YieldIfNeeded()

    def on_show_view_toolbars(self, event):
        """Show all the view toolbars"""
        frame = context.get_frame()
        frame.m_mgr.LoadPerspective(self._perspective_base)


