'''
Created on 25 ago. 2018

@author: mel
'''


class BasePreferences(object):
    '''
    Aggregated preferences base template.
    This class does nothing but acts as a template for defining preferences.
    It's not necessary to use it as base, but follow the notes and implement
    required methods.
    An instance of similar class must be returned from an aggregated_preferences
    property belonging to a active view that would include his own preferences.
    '''

    def __init__(self, common):
        """Constructor"""
        self._object = common
        super(BasePreferences, self).__init__()
        
    def add_pages(self, book):
        """Required method. The book argument is a wx.Treebook where
        add the reuired preferences"""
        pass
    
    def save(self):
        """Process changes"""
        return True
    
