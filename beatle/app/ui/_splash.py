# -*- coding: utf-8 -*-

import wx
import wx.adv


class AppSplashScreen(wx.adv.SplashScreen):
    """
    Create a splash screen widget.
    """
    
    def __init__(self, parent=None):
        """Initialize splash screen"""
        from beatle import local_path
        aBitmap = wx.Image(name=local_path("app/res/beatle.jpg")).ConvertToBitmap()
        splashStyle = wx.adv.SPLASH_CENTRE_ON_SCREEN #| wx.adv.SPLASH_TIMEOUT
        splashDuration = 0  # milliseconds
        # Call the constructor with the above arguments in exactly the
        # following order.
        win=wx.adv.SplashScreen.__init__(self, aBitmap, splashStyle,
                                 splashDuration, parent)
        self.Bind(wx.EVT_CLOSE, self.on_exit)
        wx.SafeYield(win)
        self.running = True

    def on_exit(self, evt):
        """Continue process"""
        self.Hide()
        # The program will freeze without this line.
        evt.Skip()  # Make sure the default handler runs too...
        self.running = False
