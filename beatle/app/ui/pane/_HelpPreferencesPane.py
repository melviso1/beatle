import wx

from .._base import HelpPreferencesPaneBase
from beatle.lib.api import context


class HelpPreferencesPane(HelpPreferencesPaneBase):
	def __init__(self, parent):
		super(HelpPreferencesPane, self).__init__(parent)
		# adjust controls
		sz = self.m_help_items.GetClientSize()
		self.m_help_items.InsertColumn(0, '', wx.LIST_FORMAT_LEFT, sz.x)
		self._changes = False
		self.update_data()
		
	def read_config(self):
		"""Read config"""
		self._entry = {}
		self._order = {}
		config = context.get_config()
		items = config.ReadInt("config/help/items", 3)
		if items >= 1:
			label = config.Read("config/help/item/1/label", "cpp reference ...")
			url = config.Read("config/help/item/1/url", "https://en.cppreference.com/w/")
			descr = config.Read("config/help/item/1/descr", "check online c++ reference")			
			self._entry[label] = (url, descr)
			self._order[1] = label
		if items >= 2:
			label = config.Read("config/help/item/2/label", "python reference ...")
			url = config.Read("config/help/item/2/url", "https://python.readthedocs.io/en/latest")
			descr = config.Read("config/help/item/2/descr", "check python online reference")			
			self._entry[label] = (url, descr)
			self._order[2] = label
		if items >= 3:
			label = config.Read("config/help/item/3/label", "python digger")
			url = config.Read("config/help/item/3/url", "http://pydigger.com")
			descr = config.Read("config/help/item/3/descr", "")			
			self._entry[label] = (url, descr)
			self._order[3] = label
		#if items >= 4:
		#	 label = context.config.Read("config/help/item/3/label", "online tutorials")
		#	 url = context.config.Read("config/help/item/3/url", "cpp reference ...")
		for index in range(4, items+1):
			label = config.Read("config/help/item/{0}/label".format(index), '')
			url = config.Read("config/help/item/{0}/url".format(index), '')
			descr = config.Read("config/help/item/{0}/descr".format(index), "")			
			if label and url:
				self._entry[label] = (url, descr)
				self._order[index] = label
				
	def SaveConfig(self):
		"""Save config"""
		items = len(self._entry)
		config = context.get_config()
		config.WriteInt("config/help/items",items)
		for index in range(1, items+1):
			key = self._order[index]
			value, descr = self._entry[key]
			config.Write("config/help/item/{0}/label".format(index), key)
			config.Write("config/help/item/{0}/url".format(index), value)
			config.Write("config/help/item/{0}/descr".format(index), descr)
				
	def update_data(self):
		"""Update help preferences data"""
		self.read_config()
		self.m_help_items.DeleteAllItems()
		items = len(self._entry)
		for index in range(1, items+1):
			key = self._order[index]
			#descr = self._entry[key][1]
			self.m_help_items.Append((key,))	
			
	def SaveData(self):		
		"""Save information"""
		if self._changes:
			self.SaveConfig()
			context.get_frame().build_help_menu()
		
	# Handlers for HelpPreferences events.
	def on_select_item(self, event ):
		self.m_edit_btn.Enable(True)
		self.m_del_btn.Enable(True)
		
	def on_deselect_item(self, event):
		self.m_edit_btn.Enable(False)
		self.m_del_btn.Enable(False)
	
	def on_new_item(self, event ):
		from ..dlg import HelpItemDialog
		_dlg = HelpItemDialog(self)
		index = len(self._entry)+1
		if _dlg.ShowModal() == wx.ID_OK:
			new_key = _dlg._label
			new_url = _dlg._url
			new_descr = _dlg._help
			if new_key in self._entry:
				#alert key already exists
				choice = wx.MessageBox("The item {0} already exists.\nOwerwrite?".format(new_key),
					"Warning", wx.YES_NO | wx.ICON_INFORMATION, self)
				if choice is not wx.YES:
					return
				index = dict([(y,x) for (x,y) in self._order])[new_key]				
			else:
				self.m_help_items.Append((new_key,))	
			self._changes = True
			self._entry[new_key] = (new_url,new_descr)
			self._order[index] = new_key 
			
	def on_enter_item(self, event):
		self.on_edit_item(event)
	
	def on_edit_item(self, event):
		from ..dlg import HelpItemDialog
		index = self.m_help_items.GetFirstSelected()
		if index is wx.NOT_FOUND:
			return
		key = self.m_help_items.GetItemText(index)
		if key not in self._entry:
			return
		url, descr = self._entry[key]
		_dlg = HelpItemDialog(self)
		_dlg.set_attributes(key, url, descr)
		if _dlg.ShowModal() == wx.ID_OK:
			new_key = _dlg._label
			new_url = _dlg._url
			new_descr = _dlg._help
			if new_key != key:
				self._changes = True
				del self._entry[key]
				self.m_help_items.SetItemText(index, new_key)
				self._entry[new_key] = (new_url, new_descr)
				self._order[index+1] = new_key
			elif new_url != url or new_descr != descr:
				self._changes = True
				self._entry[new_key] = (new_url, new_descr)
	
	def on_delete_item(self, event ):
		index = self.m_help_items.GetFirstSelected()
		if index != wx.NOT_FOUND:
			key = self.m_help_items.GetItemText(index)
			self.m_help_items.DeleteItem(index)
			if key in self._entry:
				index = index + 1
				del self._entry[key]
				del self._order[index]
				index = index + 1
				for k in range(index, len(self._entry)+2): 
					self._order[k] = k -1
				self._changes = True
		self.m_edit_btn.Enable(False)
		self.m_del_btn.Enable(False)
		
	
	
	
