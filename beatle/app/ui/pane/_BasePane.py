# -*- coding: utf-8 -*-

"""
This class allows to set some common handlers and methods for
easy view declaration"""

import wx

from beatle.lib.api import context
from beatle.lib.handlers import identifier


class BasePane(object):
    """Declares a base pane class"""
    _next_doc_tab = identifier('ID_NEXT_DOC')  # used for document panes
    _prev_doc_tab = identifier('ID_PREV_DOC')  # used for document panes

    def __init__(self, *args, **kwargs):
        """Initialize base view"""
        self._evtHandler = wx.EvtHandler()
        self._handler_installed = False
        self._focus = False
        self._show = False
        self._accel = [
            wx.AcceleratorEntry(wx.ACCEL_CTRL, wx.WXK_TAB, self._next_doc_tab),
            wx.AcceleratorEntry(wx.ACCEL_CTRL | wx.ACCEL_SHIFT, wx.WXK_TAB, self._prev_doc_tab),
            ]
        super(BasePane, self).__init__(*args, **kwargs)

    def append_accelerators(self, *args):
        self._accel.extend(list(args))

    def bind_events(self):
        frame = context.get_frame()
        self.Bind(wx.EVT_MENU, id=self._next_doc_tab, handler=lambda x: frame.next_doc_pane())
        self.Bind(wx.EVT_MENU, id=self._prev_doc_tab, handler=lambda x: frame.prev_doc_pane())

    def bind_accelerators(self):
        """setup accelerators"""
        self.SetAcceleratorTable(wx.AcceleratorTable(self._accel))

    def bind_special(self, evtID, handler, source=None, id=-1, id2=-1):
        """Do a special binding for pluggable event handler and also internal event handler"""
        self._evtHandler.Bind(evtID, handler, source=source, id=id, id2=id2)
        self.Bind(evtID, handler, source=source, id=id, id2=id2)

    def OnGetFocus(self, event):
        """Install event handler"""
        self.install_handler()

    def OnKillFocus(self, event):
        """Install event handler"""
        self.remove_handler()

    def install_handler(self):
        """Installs the event handler"""
        if self._handler_installed:
            return False  # already installed
        context.get_frame().PushEventHandler(self._evtHandler)
        self._handler_installed = True
        return True  # ok, installed

    def ensure_top_handler(self):
        if context.get_frame().GetEventHandler() != self._evtHandler:
            #  self._handler_installed = True
            self.remove_handler()
            self.install_handler()

    def remove_handler(self):
        """Remove the event handler"""
        if not self._handler_installed:
            return False  # already removed
        # pop event handlers until found this
        other_event_handlers = []
        event_handler = context.get_frame().PopEventHandler()
        try:
            while event_handler != self._evtHandler:
                other_event_handlers.append(event_handler)
                event_handler = context.get_frame().PopEventHandler()
            if event_handler == self._evtHandler:
                self._handler_installed = False
            result = True
        except:
            print("Fatal error : missing event handler")
            self._handler_installed = False
            result = False
        # reinstall the event handlers chain
        while len(other_event_handlers):
            context.get_frame().PushEventHandler(other_event_handlers.pop())
        return result

    @property
    def is_event_handler(self):
        """return info about if the handler is installed"""
        return self._handler_installed

    def pre_delete(self):
        """Remove the event handler"""
        self.remove_handler()

    def notify_show(self):
        """Called where the git view is show"""
        self._show = True
        self.install_handler()

    def notify_hide(self):
        """Called where the git view is hidden"""
        self._show = False
        self.remove_handler()


