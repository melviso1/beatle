import wx

from .._base import WebPreferencesPaneBase
from beatle.lib.api import context


class WebPreferencesPane(WebPreferencesPaneBase):
	def __init__(self, parent, **kwargs ):
		self._config = context.get_config()
		self._use_proxy = self._config.ReadBool("config/web/useProxy", False)
		self._http_proxy = self._config.Read("config/web/http_proxy",'')
		self._https_proxy = self._config.Read("config/web/https_proxy",'')
		self._ftp_proxy = self._config.Read("config/web/ftp_proxy",'')
		self._use_auto = kwargs.get('auto',True)
		# -- enable this when multiple proxy is done:
		# self._use_same = kwargs.get('same',False)
		# -- delete this when multiple proxy is done:
		self._use_same = self._config.ReadBool("config/web/same_proxy",True)
		super(WebPreferencesPane, self).__init__(parent)
		self.update_data()
	
	def update_data(self):
		"""Setup control values"""
		if self._use_proxy:
			self.m_radioBtn5.SetValue(True)
			self.st_http_proxy.Enable(True)
			self.m_http_proxy.Enable(True)
			# -- enable this when multiple proxy is done
			# self.m_checkBox6.Enable(True)
			# detailed = not self.m_checkBox6.GetValue()
			# self.st_https_proxy.Enable(detailed)
			# self.m_https_proxy.Enable(detailed)
			# self.st_ftp_proxy.Enable(detailed)
			# self.m_ftp_proxy.Enable(detailed)
		else:
			self.m_radioBtn4.SetValue(True)
			self.st_http_proxy.Enable(False)
			self.m_http_proxy.Enable(False)
			self.m_checkBox6.Enable(False)
			self.st_https_proxy.Enable(False)
			self.m_https_proxy.Enable(False)
			self.st_ftp_proxy.Enable(False)
			self.m_ftp_proxy.Enable(False)
		self.m_http_proxy.SetValue(self._http_proxy)
		self.m_https_proxy.SetValue(self._https_proxy)
		self.m_ftp_proxy.SetValue(self._ftp_proxy)
		self.m_checkBox6.SetValue(self._use_same)
		
	def get_data(self):
		"""Get data from controls"""
		self._use_proxy = self.m_radioBtn5.GetValue()
		self._http_proxy = self.m_http_proxy.GetValue()
		self._https_proxy = self.m_https_proxy.GetValue()
		self._ftp_proxy = self.m_ftp_proxy.GetValue()
		# -- enable this when multiple proxy is done
		# self._use_same = self.m_checkBox6.GetValue()		
		
	def SaveData(self):
		self.get_data()
		self._config.WriteBool("config/web/useProxy",self._use_proxy)
		self._config.WriteBool("config/web/same_proxy",self._use_same)
		self._config.Write("config/web/http_proxy",self._http_proxy.encode('UTF-8'))
		self._config.Write("config/web/https_proxy",self._https_proxy.encode('UTF-8'))
		self._config.Write("config/web/ftp_proxy",self._ftp_proxy.encode('UTF-8'))
		
		
	# Handlers for WebPreferencesPane events.
	def OnAutoNetwork(self, event ):
		# disable all controls
		self.st_http_proxy.Enable(False)
		self.m_http_proxy.Enable(False)
		# -- enable this when multiple proxy is done
		# self.m_checkBox6.Enable(False)
		# self.st_https_proxy.Enable(False)
		# self.m_https_proxy.Enable(False)
		# self.st_ftp_proxy.Enable(False)
		# self.m_ftp_proxy.Enable(False)
		self.m_button4.Enable(True)
	
	def OnManualProxy(self, event ):
		self.st_http_proxy.Enable(True)
		self.m_http_proxy.Enable(True)
		# -- enable this when multiple proxy is done
		# self.m_checkBox6.Enable(True)
		# detailed = not self.m_checkBox6.GetValue()
		# self.st_https_proxy.Enable(detailed)
		# self.m_https_proxy.Enable(detailed)
		# self.st_ftp_proxy.Enable(detailed)
		# self.m_ftp_proxy.Enable(detailed)
		self.m_button4.Enable(True)
	
	def OnChangeHttpProxy(self, event ):
		self.m_button4.Enable(True)
	
	def OnSameProxy(self, event ):
		# -- enable this when multiple proxy is done
		# detailed = not self.m_checkBox6.GetValue()
		# self.st_https_proxy.Enable(detailed)
		# self.m_https_proxy.Enable(detailed)
		# self.st_ftp_proxy.Enable(detailed)
		# self.m_ftp_proxy.Enable(detailed)
		# self.m_button4.Enable(True)
		pass
	
	def OnChangeHttpsProxy(self, event ):
		self.m_button4.Enable(True)
	
	def OnChangeFtpProxy(self, event ):
		self.m_button4.Enable(True)

	def OnApply(self, event ):
		import ctypes
		import platform
		self.get_data()
		s_plat = platform.platform() 
		if 'Linux' not in s_plat and 'linux' not in s_plat:
			wx.MessageBox('Proxy is only supported in linux','Error',
				wx.OK | wx.CENTER | wx.ICON_ERROR, self)
			return
		if self._use_proxy:
			try:	
				soup = ctypes.cdll.LoadLibrary('libsoup-2.4.so')
				webkit = ctypes.cdll.LoadLibrary('libwebkitgtk-1.0.so')
				gobject = ctypes.cdll.LoadLibrary('libgobject-2.0.so')
				if '64bit' in platform.architecture():
					webkit.webkit_get_default_session.restype = ctypes.c_uint64
					soup.soup_uri_new.restype = ctypes.c_uint64
					gobject.g_object_set.argtypes=(ctypes.c_uint64, ctypes.c_char_p, ctypes.c_uint64, ctypes.c_uint64)
					soup.soup_uri_free.argtypes=(ctypes.c_uint64,)
				else:
					#untested
					webkit.webkit_get_default_session.restype = ctypes.c_uint32
					soup.soup_uri_new.restype = ctypes.c_uint32
					gobject.g_object_set.argtypes=(ctypes.c_uint32, ctypes.c_char_p, ctypes.c_uint32, ctypes.c_uint32)
					soup.soup_uri_free.argtypes=(ctypes.c_uint32,)
				proxy_uri = soup.soup_uri_new('http://{proxy}'.format(proxy=self._http_proxy))
				session = webkit.webkit_get_default_session()
				gobject.g_object_set(session, "proxy-uri", proxy_uri, 0)
				soup.soup_uri_free(proxy_uri)
			except:
				wx.MessageBox('Failed','Error', wx.OK | wx.CENTER | wx.ICON_ERROR, self)
				return
		self.m_button4.Enable(False)

	
