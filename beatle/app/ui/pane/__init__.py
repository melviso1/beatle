# -*- coding: utf-8 -*-

from ._BasePane import BasePane
from ._NavigatorPane import NavigatorPane
from ._HelpPreferencesPane import HelpPreferencesPane
from ._WebPreferencesPane import WebPreferencesPane
