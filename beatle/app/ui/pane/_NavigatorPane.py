# -*- coding: utf-8 -*-

import wx
import wx.html2
from .._base import NavigatorPaneBase
from ._BasePane import BasePane
from beatle.lib.api import context


class NavigatorPane(BasePane, NavigatorPaneBase):
    """Implements navigator pane"""
    def __init__(self, parent, mainframe):
        """Intialization of method editor"""
        super(NavigatorPane, self).__init__(parent)
        self._mainframe = mainframe
        self._url = None
        self.Bind(wx.html2.EVT_WEBVIEW_LOADED, self.OnPageLoaded, self.m_page)
        self.Bind(wx.html2.EVT_WEBVIEW_NAVIGATING, self.OnPageLoading, self.m_page)

        config = context.get_config()
        self.use_proxy=config.ReadBool("config/web/useProxy", False)
        if self.use_proxy:
            import platform
            if platform.system() == 'Linux':
                import ctypes
                http_proxy=config.Read("config/web/http_proxy",'')
                same=config.ReadBool("config/web/same_proxy",False)
                if not same:
                    # This is not yet implemented. We need a GSimpleProxyResolver
                    # instead of setting a proxy-uri
                    https_proxy = config.Read("config/web/https_proxy",'')
                    ftp_proxy = config.Read("config/web/ftp_proxy",'')
                try:
                    self.soup = ctypes.cdll.LoadLibrary('libsoup-2.4.so')
                    #self.webkit = ctypes.cdll.LoadLibrary('libwebkitgtk-1.0.so')
                    self.webkit = ctypes.cdll.LoadLibrary('libwebkit2gtk-4.0.so')
                    self.gobject = ctypes.cdll.LoadLibrary('libgobject-2.0.so')
                    if '64bit' in platform.architecture():
                        self.webkit.webkit_get_default_session.restype = ctypes.c_uint64
                        self.soup.soup_uri_new.restype = ctypes.c_uint64
                        self.gobject.g_object_set.argtypes=(ctypes.c_uint64, ctypes.c_char_p, ctypes.c_uint64, ctypes.c_uint64)
                        self.soup.soup_uri_free.argtypes=(ctypes.c_uint64,)
                    else:
                        # untested
                        self.webkit.webkit_get_default_session.restype = ctypes.c_uint32
                        self.soup.soup_uri_new.restype = ctypes.c_uint32
                        self.gobject.g_object_set.argtypes=(ctypes.c_uint32, ctypes.c_char_p, ctypes.c_uint32, ctypes.c_uint32)
                        self.soup.soup_uri_free.argtypes=(ctypes.c_uint32,)
                    proxy_uri = self.soup.soup_uri_new('http://{proxy}'.format(proxy=http_proxy))
                    self.session = self.webkit.webkit_get_default_session()
                    self.gobject.g_object_set(self.session, "proxy-uri", proxy_uri, 0)
                    self.soup.soup_uri_free(proxy_uri)
                except Exception as e:
                    wx.MessageBox('Failed to setup a proxy', 'Error',
                        wx.OK | wx.CENTER | wx.ICON_ERROR, self)

    @property
    def url(self):
        return self._url

    def NavigateTo(self, str_url):
        """Do navigation"""
        self._url = str_url
        self.m_url.Clear()
        self.m_url.WriteText(str_url)
        self.m_page.LoadURL(str_url)

    def OnPageLoaded(self, event):
        """The page is fully loaded"""
        #self.m_page.RunScript('<..some javascript code>')

    def OnPageLoading(self, event):
        """The web resource is loading.
        This method can veto the event preventing the
        web page become loaded."""
        event.Skip()

    def pre_delete(self):
        """Ensure webview destruction"""
        super(NavigatorPane, self).pre_delete()
        self.reserve_webview()


