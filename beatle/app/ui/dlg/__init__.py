# -*- coding: utf-8 -*-

from ._FindTextDialog import FindTextDialog
from ._FolderDialog import FolderDialog
from ._FindInFilesDialog import FindInFilesDialog
from ._ImportProjectDialog import ImportProjectDialog
from ._WaitDialog import WaitDialog
from ._CodeNavigatorDialog import CodeNavigatorDialog
from ._WorkspaceDialog import WorkspaceDialog
from ._HelpItemDialog import HelpItemDialog
from ._PreferencesDialog import PreferencesDialog
from ._UnsavedQuitDialog import UnsavedQuitDialog
from ._FileAlreadyExistsDialog import FileAlreadyExistsDialog
from ._AboutDlg import AboutDlg

