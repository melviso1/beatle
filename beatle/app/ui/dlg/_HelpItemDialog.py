
import wx

from beatle.lib import wxx
from .._base import HelpItemDialogBase


class HelpItemDialog(HelpItemDialogBase):
	"""
	This dialog allows to setup a help item.
	Help items are displayed in help menu.
	"""
	@wxx.SetInfo(__doc__)
	def __init__(self, parent):
		super(HelpItemDialog, self).__init__(parent)

	def on_ok(self, event):
		self._label = self.m_textCtrl56.GetValue()
		self._url = self.m_textCtrl29.GetValue()
		self._help = self.m_textCtrl30.GetValue()
		self.EndModal(wx.ID_OK)

	def on_cancel(self, event):
		self.EndModal(wx.ID_CANCEL)

	def set_attributes(self, label, url, _help):
		"""Transfer attributes from existing constructor"""
		self._label = label
		self._url = url
		self._help = _help
		self.m_textCtrl56.SetValue(self._label)
		self.m_textCtrl29.SetValue(self._url)
		self.m_textCtrl30.SetValue(self._help)
		self.SetTitle("Edit help item")
