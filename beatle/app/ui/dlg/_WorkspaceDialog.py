
import os
import wx

from .._base import WorkspaceDialogBase
from beatle.lib import wxx


class WorkspaceDialog(WorkspaceDialogBase):
    """
    Workspace edition dialog.
    Used for creation of new workspace or
    for edition of existing one.
    """
    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, container=None):
        """initialization"""
        from beatle.app import resources as rc
        super(WorkspaceDialog, self).__init__(parent)
        # container es la clase destino
        self.container = container
        self._original = ""
        self._autodir = True
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap("workspace"))
        self.SetIcon(icon)

    # Handlers for NewWorkspace events.
    def on_change_worspace_name(self, event):
        """Handle workspace name change"""
        if self._autodir:
            name = self.m_textCtrl1.GetValue()
            std = wx.StandardPaths.Get().GetDocumentsDir()
            #create a custom subdirectory
            self.m_textCtrl9.SetValue(os.path.join(std, name))

    def on_choose_dir(self, event):
        """Launch subdialog for choosing dir"""
        dialog = wx.DirDialog(self, "Select directory",
            self.m_textCtrl9.GetValue(), wx.DD_DIR_MUST_EXIST)
        if dialog.ShowModal() == wx.ID_OK:
            self.m_textCtrl9.SetValue(dialog.GetPath())
            self._autodir = False

    def validate(self):
        """validates dialog when ok request"""
        self._name = self.m_textCtrl1.GetValue()
        if len(self._name) == 0:
            wx.MessageBox("Workspace name must be non empty", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self._base = self.m_textCtrl9.GetValue()
        if len(self._name) == 0:
            wx.MessageBox("Invalid workspace base directory", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self._note = self.m_richText3.GetValue()
        return True

    def get_kwargs(self):
        """Returns kwargs dictionary suitable for object creation"""
        kwargs = {}
        kwargs['name'] = self._name
        kwargs['dir'] = self._base
        kwargs['note'] = self._note
        return kwargs

    def copy_attributes(self, workspace):
        """Copy attributes to method"""
        workspace._name = self._name
        workspace._dir = self._base
        workspace.note = self._note

    def set_attributes(self, workspace):
        """Setup attributes for editing already method"""
        self.SetTitle("Edit workspace")
        self.m_textCtrl1.SetValue(workspace._name)
        self.m_textCtrl9.SetValue(workspace._dir)
        self.m_richText3.SetValue(workspace.note)

    @wxx.save_position
    def on_cancel(self, event):
        """cancel event handler"""
        self.EndModal(wx.ID_CANCEL)

    @wxx.save_position
    def on_ok(self, event):
        """Handle OnOk"""
        if self.validate():
            self.EndModal(wx.ID_OK)


