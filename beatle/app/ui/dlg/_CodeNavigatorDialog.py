

import wx

from .._base import CodeNavigatorDialogBase
from beatle.lib import wxx


class CodeNavigatorDialog(CodeNavigatorDialogBase):
    """Dialog for code insertion"""
    @wxx.restore_position
    def __init__(self, parent, method):
        """Initialice dialog"""
        self.method = method
        super(CodeNavigatorDialog, self).__init__(parent)

    def add_root(self, name, _type):
        """Add element to tree"""
        # we need to find all visible methods
        self.m_treeCtrl3.Append(self.root, name, data=wxx.TreeItemData(_type))

    def ShowModal(self):
        """Initialize dialog and go"""
        from beatle import model
        self.root = self.m_treeCtrl3.AddRoot(wx.EmptyString)
        # insert methods for variables
        t = model.cc.typeinst(type=self.method.inner_class, ptr=True, constptr=True)
        self.add_root('this', t)

        super(wx.Dialog, self).Show()  # skip dialog modeless handling but show window TODO: is this required?
        return super(CodeNavigatorDialog, self).ShowModal()

    def OnKeyDown(self, event):
        if event.GetKeyCode() == wx.WXK_ESCAPE:
            self.EndModal(wx.ID_CANCEL)

    # Handlers for codeNavigator events.
    def OnSelectedItem(self, event):
        """Handle select item"""
        # TODO: Implement OnSelectedItem
        pass

    def on_expand_item(self, event):
        # TODO: Implement on_expand_item
        """Handle select item"""
        pass


