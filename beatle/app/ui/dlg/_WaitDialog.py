
from .._base import WaitDialogBase

# Implementing Wait
class WaitDialog(WaitDialogBase):
    """This class shows a dialog"""
    def __init__(self, parent):
        """Initialize dialog"""
        super(WaitDialog, self).__init__(parent)
