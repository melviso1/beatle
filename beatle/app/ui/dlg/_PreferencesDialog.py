
import wx 

from beatle.lib import wxx
from .._base import PreferencesDialogBase, FontPreferencesPaneBase
from ..pane import HelpPreferencesPane
from beatle.lib.api import context


class PreferencesDialog(PreferencesDialogBase):
	"""This dialog allows to configure general settings
	of the environment. Please take in consideration that,
	althought the main operations can be undoed later,
	gui settings are not restored this way."""
	@wxx.SetInfo(__doc__)
	def __init__(self, parent):
		"""Initialization"""
		super(PreferencesDialog, self).__init__(parent)
		self._config = context.get_config()
		self._fontPane = FontPreferencesPaneBase(self.m_treeBook)
		self._helpPane = HelpPreferencesPane(self.m_treeBook)
		self.view_preferences = False
		import platform
		s_plat = platform.platform() 
		if 'Linux' in s_plat or 'linux' in s_plat:
			self._linux = True
		else:
			self._linux = False
		if self._linux:
			from ..pane import WebPreferencesPane
			self._webPane  = WebPreferencesPane(self.m_treeBook)
		if wx.__version__ >= '3.0.0.0':
			self.m_treeBook.AddPage(self._fontPane, u"Fonts", False, wx.NOT_FOUND)
			self.m_treeBook.AddPage(self._helpPane, u"Help", False, wx.NOT_FOUND)
			if self._linux:
				self.m_treeBook.AddPage(self._webPane, u"Web", False, wx.NOT_FOUND)
		else:
			self.m_treeBook.AddPage(self._fontPane, u"Fonts", False, wx.NullBitmap)
			self.m_treeBook.AddPage(self._helpPane, u"Help", False, wx.NullBitmap)
			if self._linux:
				self.m_treeBook.AddPage(self._webPane, u"Web", False, wx.NullBitmap)

		# ---- fonts ----
		default_font = wx.SystemSettings.GetFont(wx.SYS_ANSI_FIXED_FONT)
		default_font_string = self.GetFontString(default_font)
		# ---- base font (used for editors)----

		font_string = self._config.Read("config/defaultFont", default_font_string)
		self._font = self.GetStringFont(font_string)
		self._fontPane.m_fontPicker.SetSelectedFont(self._font)
		# ---- trees font ----
		font_string = self._config.Read("config/treesFont", default_font_string)
		self._treesFont = self.GetStringFont(font_string)
		self._fontPane.m_treesFontPicker.SetSelectedFont(self._treesFont)

		# ---- other preferences (by context)
		self.setup_aggregated_preferences()

		from beatle.lib.api import volatile

		self.preferences = volatile.get(type(self), {})
		if 'page' in self.preferences:
			self.m_treeBook.ChangeSelection(self.preferences['page'])

	def setup_aggregated_preferences(self):
		"""Initialize aggregated preferences"""
		current_view = context.get_current_view()
		self.view_preferences = getattr(current_view,'aggregated_preferences', False)
		if self.view_preferences:
			self.view_preferences.add_pages(self.m_treeBook)

	def OnInitDialog(self, event):
		"""Dialog initialization"""
		self._fontPane.m_fontPicker.SetSelectedFont(self._font)
		self._fontPane.m_treesFontPicker.SetSelectedFont(self._treesFont)

	
	def GetFontString(self, font):
		"""Get info about current font"""
		return font.GetNativeFontInfo().ToString()

	def GetStringFont(self, string):
		"""Get info current font"""
		f = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)
		f.SetNativeFontInfo(string)
		return f
	
	# Handlers for PreferencesDialog events.
	def on_ok(self, event):
		from beatle.lib.api import volatile
		self.preferences['page'] = self.m_treeBook.GetSelection()
		volatile.set(type(self), self.preferences)
		if self.view_preferences:
			if not self.view_preferences.save():
				return
		# ---- fonts ----
		self._config.Write(
			"config/defaultFont",
			self.GetFontString(self._fontPane.m_fontPicker.GetSelectedFont()))
		self._config.Write(
			"config/treesFont",
			self.GetFontString(self._fontPane.m_treesFontPicker.GetSelectedFont()))
		if self._linux:
			self._webPane.get_data()
			self._webPane.SaveData()
		self._helpPane.SaveData()
		self._config.Flush()
		self.EndModal(wx.ID_OK)

	def on_cancel(self, event):
		self.EndModal(wx.ID_CANCEL)


