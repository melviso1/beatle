
import wx

from .._base import FindTextDialogBase 
from beatle.lib import wxx


class FindTextDialog(FindTextDialogBase):
    """
    This dialog allows to search for text.
    Press F3 for search again for more occurrences.
    Press Shift + F3 for searching back.
    """
    @wxx.SetInfo(__doc__)
    def __init__(self, parent, text=wx.EmptyString):
        """Initialize dialog"""
        super(FindTextDialog, self).__init__(parent)
        self.m_search_string.SetValue(text)

    def validate(self):
        """Process OnOk event"""
        self.seach_text = self.m_search_string.GetValue()
        if not len(self.seach_text):
            wx.MessageBox("Search text must be non empty", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        return True

    # Handlers for FindText events.
    def on_cancel(self, event):
        """Cancel seach"""
        self.EndModal(wx.ID_CANCEL)

    def on_ok(self, event):
        """Begin search"""
        if self.validate():
            self.EndModal(wx.ID_OK)
