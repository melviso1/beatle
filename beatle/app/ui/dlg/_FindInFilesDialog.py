
import re, wx

from beatle.lib import wxx
from .._base import FindInFilesDialogBase


class FindInFilesDialog(FindInFilesDialogBase):
    """
    This dialog allows to find for a text
    inside files. The results will be
    show in the search pane.
    """
    @wxx.SetInfo(__doc__)
    @wxx.restore_position
    def __init__(self, parent, container=None, search_text=''):
        """Initialize dialog"""
        self.container = container
        super(FindInFilesDialog, self).__init__(parent)
        self.m_search_string.SetValue(search_text)

    def validate(self):
        """validate the dialog"""
        self.searchString = self.m_search_string.GetValue()
        if len(self.searchString) == 0:
            wx.MessageBox('Please specify a non empty search string', 'Error', wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self.regexpr = self.m_checkBox5.GetValue()
        self.case_sensitive = self.m_checkBox4.GetValue()
        if self.regexpr:
            # check if the expression is valid
            try:
                re.compile(self.searchString)
            except:
                wx.MessageBox('The regular expression is invalid', 'Error', wx.OK | wx.CENTER | wx.ICON_ERROR, self)
                return False
        if self.m_radioBtn1.GetValue():
            self.mode = 0
        elif self.m_radioBtn2.GetValue():
            self.mode = 1
        elif  self.m_radioBtn3.GetValue():
            self.mode = 2
        else:
            # paranoid check (this couldn't happen)
            wx.MessageBox('Please select scope', 'Error', wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        return True

    @wxx.save_position
    def on_ok(self, event):
        """Handler for Ok button press"""
        if self.validate():
            self.EndModal(wx.ID_OK)

    @wxx.save_position
    def on_cancel(self, event):
        """Handler for Cancel button press"""
        self.EndModal(wx.ID_CANCEL)

