# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Oct 27 2020)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

# special import for beatle development
from beatle.lib import wxx
from beatle.lib.handlers import Identifiers
###########################################################################
## Class AboutDlgBase
###########################################################################

class AboutDlgBase ( wxx.Dialog ):
	
	def __init__( self, parent ):
		wxx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"About dialog", pos = wx.DefaultPosition, size = wx.Size( 826,718 ), style = 0 )
		
		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 4, 41, 43 ) )
		
		fgSizer1 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer1.AddGrowableCol( 0 )
		fgSizer1.AddGrowableRow( 0 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_ALL )
		
		self.m_scrolledWindow1 = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.m_scrolledWindow1.SetScrollRate( 5, 5 )
		self.m_scrolledWindow1.SetMinSize( wx.Size( -1,300 ) )
		
		fgSizer2 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer2.AddGrowableCol( 0 )
		fgSizer2.AddGrowableRow( 0 )
		fgSizer2.SetFlexibleDirection( wx.BOTH )
		fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_ALL )
		
		self.m_staticText1 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"\n\t\t\t\t\t\tBeatle 3.0 v.021120\n\nThis program is free software, licensed under GNU GENERAL PUBLIC LICENCE v2 -1991.\nThis program includes third-party software with his own licences. \n\nPress \"Licence's info...\" button for browsing additional licence files.\n\nThe complete list is almost impossible to include for a single-developer project like \"Beatle\", \neven in an \"AppImage\" distribution, that includes many system files. If you detect some\nmissing licence here, please tell me about on the gitlab project site : \n\n    \t\t\t\thttps://gitlab.com/melviso1/beatle\n\nI want to say thanks here to all the free software developers that make me able to do this\nwork, and as is impossible to mention all of them here, I wish to express my gratitude \nto someone of them:\n\n            Rocky Bernstein and team for the trepan3k python debugger\n            Robin Dunn and team for wxPython\n            Tamás Szelei for the amazing rpclib\n            David Abrahams and Stefan Seefeld for boost.python\n            The wxWidgets team\n            Michael Trier, Sebastian Thiel and team for GitPython\n            Andrea Gavana for his aui wxWidgets developments\n            Dimitri van Heesch for doxygen\n            José Antonio Hurtado and others for wxFormBuilder\n\n\nmel viso, 2020.\n\n", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )
		self.m_staticText1.SetForegroundColour( wx.Colour( 224, 224, 224 ) )
		self.m_staticText1.SetBackgroundColour( wx.Colour( 4, 41, 43 ) )
		
		fgSizer2.Add( self.m_staticText1, 1, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		self.m_scrolledWindow1.SetSizer( fgSizer2 )
		self.m_scrolledWindow1.Layout()
		fgSizer2.Fit( self.m_scrolledWindow1 )
		fgSizer1.Add( self.m_scrolledWindow1, 1, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer4 = wx.FlexGridSizer( 0, 3, 0, 0 )
		fgSizer4.AddGrowableCol( 0 )
		fgSizer4.AddGrowableRow( 0 )
		fgSizer4.SetFlexibleDirection( wx.BOTH )
		fgSizer4.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer4.SetMinSize( wx.Size( -1,42 ) ) 
		
		fgSizer4.Add( ( 0, 22), 1, wx.EXPAND, 5 )
		
		self.m_button1 = wx.Button( self, wx.ID_ANY, u"Licence's info...", wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER )
		self.m_button1.SetForegroundColour( wx.Colour( 208, 208, 208 ) )
		self.m_button1.SetBackgroundColour( wx.Colour( 58, 58, 58 ) )
		
		fgSizer4.Add( self.m_button1, 0, wx.ALL, 5 )
		
		self.m_button2 = wx.Button( self, wx.ID_ANY, u"Ok", wx.DefaultPosition, wx.DefaultSize, wx.NO_BORDER )
		self.m_button2.SetDefault() 
		self.m_button2.SetForegroundColour( wx.Colour( 208, 208, 208 ) )
		self.m_button2.SetBackgroundColour( wx.Colour( 58, 58, 58 ) )
		
		fgSizer4.Add( self.m_button2, 0, wx.ALL, 5 )
		
		
		fgSizer1.Add( fgSizer4, 0, wx.EXPAND|wx.BOTTOM|wx.RIGHT, 5 )
		
		
		self.SetSizer( fgSizer1 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_button1.Bind( wx.EVT_BUTTON, self.OnLicensesInfo )
		self.m_button2.Bind( wx.EVT_BUTTON, self.OnOk )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OnLicensesInfo( self, event ):
		event.Skip()
	
	def OnOk( self, event ):
		event.Skip()
	

