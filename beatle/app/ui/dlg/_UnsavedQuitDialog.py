
from .._base import UnsavedQuitDialogBase
from beatle.lib.handlers import identifier
from beatle.lib import wxx
import wx

ID_DISCARD = identifier('ID_DISCARD')


class UnsavedQuitDialog(UnsavedQuitDialogBase ):
	"""
	This dialog is shown because you attempt to exit the application
	without saving changes. You can either cancel the exit, save and
	exit or quit discarding unsaved changes.
	"""
	@wxx.SetInfo(__doc__)
	@wxx.restore_position
	def __init__(self, parent, modified ):
		super(UnsavedQuitDialog, self).__init__(parent)
		self.m_unsaved_projects_list.AppendItems([x.name for x in modified])
		self.Centre()

	@wxx.save_position
	def OnDiscard(self, event ):
		self.EndModal(ID_DISCARD)

	@wxx.save_position
	def OnSaveChanges( self, event ):
		self.EndModal(wx.ID_OK)

	@wxx.save_position
	def OnCancel( self, event ):
		self.EndModal(wx.ID_CANCEL)
