
import os, wx

from beatle.lib import wxx
from .._base import ImportProjectDialogBase


# Implementing ImportProject
class ImportProjectDialog(ImportProjectDialogBase):
    """
    This dialog allows to import a project
    into the workspace.
    """
    @wxx.SetInfo(__doc__)
    def __init__(self, parent, workspace):
        """Initialize dialog"""
        super(ImportProjectDialog, self).__init__(parent)
        self.workspace = workspace

    def CppImport(self, dlg=None):
        """Do the work of importing C++ files"""
        from beatle.model import Project
        kwargs = {
            'parent': self.workspace, 'name': self.project_name, 'language': 'c++',
            'dir': self.dir,
            'note': 'project imported from {self.origin_dir}'.format(self=self)}
        project = Project(**kwargs)
        return project.update_from_sources(dir=self.origin_dir, progress=dlg)

    def PythonImport(self, dlg=None):
        """Do the work of importing python files"""
        #create the project
        from beatle.model import Project
        kwargs = {
            'parent': self.workspace, 'name': self.project_name, 'language': 'python',
            'dir': self.dir,
            'note': 'project imported from {self.origin_dir}'.format(self=self)}
        project = Project(**kwargs)
        return project.update_from_sources(dir=self.origin_dir, progress=dlg)

    def validate(self):
        """validate dialog"""
        # first of all, get the base dir
        self.origin_dir = self.m_dirPicker2.Path
        if not os.access(self.origin_dir, os.R_OK):
            wx.MessageBox("The path {self.origin_dir} is not readable".format(self=self), "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self.project_name = os.path.split(self.origin_dir)[1]
        if len(self.project_name) == 0:
            wx.MessageBox("The path {self.dir} is invalid".format(self=self), "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self.dir = os.path.join(self.workspace._dir, self.project_name)
        #Operate throgh the import target
        self.option = self.m_choicebook4.GetSelection()
        return True

    def DoImport(self, dlg=None):
        """This call is expected to come after EndDialog(wx.ID_OK)"""
        if self.option == 0:
            return self.PythonImport(dlg)
        else:
            return self.CppImport(dlg)

    def on_cancel(self, event):
        """cancel event handler"""
        self.EndModal(wx.ID_CANCEL)

    def on_ok(self, event):
        """Handle OnOk"""
        if self.validate():
            self.EndModal(wx.ID_OK)


