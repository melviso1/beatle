
import wx

from beatle.lib import wxx
from .._base import FolderDialogBase


# Implementing NewFolder
class FolderDialog(FolderDialogBase):
    """
    This dialog allows to setup the folder name.
    Folders are organizative items and allow to
    apply contexts to contained elements.
    """
    @wxx.SetInfo(__doc__)
    def __init__(self, parent, container):
        """Initialization"""
        from beatle.app import resources as rc
        super(FolderDialog, self).__init__(parent)
        self._container = container
        self.original = ""
        self.m_textCtrl2.SetFocus()
        self.m_sdbSizer2OK.SetDefault()
        icon = wx.Icon()
        icon.CopyFromBitmap(rc.get_bitmap("folder"))
        self.SetIcon(icon)

    def validate(self):
        """Process OnOk event"""
        self._name = self.m_textCtrl2.GetValue().strip()
        if len(self._name) == 0:
            wx.MessageBox("Folder name must be non empty", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        if self.original != self._name:
            master = self._container.outer_class or self._container.outer_namespace
            master_folders = [x.name for x in self._container.top_folders
                if (x.outer_class or x.outer_namespace) == master]
            if self._name in master_folders:
                wx.MessageBox("Folder already exist", "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, self)
                return False
        self._note = self.m_richText3.GetValue()
        return True

    def copy_attributes(self, folder):
        """Transfer attributes to folder"""
        folder._name = self._name
        folder.note = self._note

    def set_attributes(self, folder):
        """Transfer attributes from existing constructor"""
        self._container = folder.parent
        self._name = folder.name
        self.original = folder.name
        self._note = folder.note
        self.m_textCtrl2.SetValue(self._name)
        self.m_richText3.SetValue(self._note)
        self.SetTitle("Edit folder")

    def get_kwargs(self):
        """return arguments suitable for object instance"""
        return {'parent': self._container, 'name': self._name,
            'note': self._note}

    def on_ok(self, event):
        """On Ok"""
        if self.validate():
            self.EndModal(wx.ID_OK)

    def on_cancel(self, event):
        self.EndModal(wx.ID_CANCEL)



