
import wx

from beatle.lib import wxx
from .._base import FileAlreadyExistsDialogBase
from beatle.lib.handlers import Identifiers

ID_YESALL = Identifiers.register("ID_YESALL")


class FileAlreadyExistsDialog(FileAlreadyExistsDialogBase ):
	"""
	This dialog is shown because some previous file
	must be overwritted. You can either choose to
	cancel, to skip, to overwrite and to say ok
	to this and subsequent notifications about.
	"""

	@wxx.SetInfo(__doc__)
	@wxx.restore_position
	def __init__(self, parent, element ):
		super(FileAlreadyExistsDialog, self).__init__(parent)
		self.m_staticText.SetLabelText('The file {} already exists, Do you want to overwrite it?'.format(element))

	def OnYes(self, event ):
		self.EndModal(wx.YES)

	def OnYesAll(self, event ):
		self.EndModal(ID_YESALL)

	def on_cancel(self, event ):
		self.EndModal(wx.CANCEL)

	def OnNo(self, event ):
		self.EndModal(wx.NO)
