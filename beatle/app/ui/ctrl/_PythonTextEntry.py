# -*- coding: utf-8 -*-

import wx
from wx import stc
from beatle.lib.wxx.py.shell import Shell
from beatle.lib.wxx.py.editwindow import FACES
import pydoc
from beatle.lib.handlers import identifier

# monkey-patching pydoc avoid pager crash
pydoc.pager = pydoc.plainpager


class PythonTextEntry(Shell):

    _delete_id = identifier("ID_DELETE")

    @classmethod
    def apply_colors(cls):
        dbc = wx.TheColourDatabase
        cls.fore= dbc.Find('LIGHT GREY').GetAsString(wx.C2S_HTML_SYNTAX)
        cls.back = dbc.Find('DARK GREY').GetAsString(wx.C2S_HTML_SYNTAX)
        FACES['backcol']=cls.back
        FACES['forecol']=cls.fore

    """wxTextCtrl override for execute python code"""
    def __init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString,
                 pos=wx.DefaultPosition,
                 size=wx.DefaultSize,
                 style=wx.TE_MULTILINE | wx.TE_PROCESS_ENTER,
                 validator=wx.DefaultValidator, name=wx.TextCtrlNameStr):
        """Initialize the control"""
        PythonTextEntry.apply_colors()
        glb = globals()
        _l = {'__builtins__': glb['__builtins__'], '__name__': '__main__', '__doc__': None, '__package__': None}
        super(PythonTextEntry, self).__init__(parent, id, locals=_l)
        self.setLocalShell()
        self.SetForegroundColour(PythonTextEntry.fore)
        self.SetBackgroundColour(PythonTextEntry.back)
        self.Bind(wx.EVT_MENU, lambda evt: self.delete, id=self._delete_id)
        parent.SetAcceleratorTable(wx.AcceleratorTable([
            wx.AcceleratorEntry(wx.ACCEL_NORMAL, wx.WXK_DELETE, self._delete_id),
        ]))

    def delete(self):
        """Handle delete key"""
        endpos = self.GetTextLength()
        startpos = self.promptPosEnd
        if endpos > startpos:
            self.SetText(text = self.GetText()[:-1])

    def setStyles(self, faces):
        """Configure font size, typeface and color for lexer."""

        # Default style
        self.StyleSetSpec(stc.STC_STYLE_DEFAULT,
                          "face:{mono},size:{size},fore:{forecol},back:{backcol}".format(**faces))

        self.StyleClearAll()
        self.SetSelForeground(True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))
        self.SetSelBackground(True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT))

        # Built in styles
        self.StyleSetSpec(stc.STC_STYLE_LINENUMBER, "back:#3F3F3F,face:{mono},size:{lnsize}".format(**faces))
        self.StyleSetSpec(stc.STC_STYLE_CONTROLCHAR,"face:%(mono)s" % faces)
        self.StyleSetSpec(stc.STC_STYLE_BRACELIGHT, "fore:#FFFF00,back:#000077")
        self.StyleSetSpec(stc.STC_STYLE_BRACEBAD, "fore:#00FFFF,back:#000077")

        # Python styles
        self.StyleSetSpec(stc.STC_P_DEFAULT, "face:%(mono)s" % faces)
        self.StyleSetSpec(stc.STC_P_COMMENTLINE, "fore:#FF80FF,face:%(mono)s" % faces)
        self.StyleSetSpec(stc.STC_P_NUMBER,
                          "")
        self.StyleSetSpec(stc.STC_P_STRING, "fore:#80FF80,face:%(mono)s" % faces)
        self.StyleSetSpec(stc.STC_P_CHARACTER, "fore:#80FF80,face:%(mono)s" % faces)
        self.StyleSetSpec(stc.STC_P_WORD, "fore:#FFFF80,bold")
        self.StyleSetSpec(stc.STC_P_TRIPLE, "fore:#80FFFF")
        self.StyleSetSpec(stc.STC_P_TRIPLEDOUBLE, "fore:#FFFFCC,back:#000017")
        self.StyleSetSpec(stc.STC_P_CLASSNAME, "fore:#FFFF00,bold")
        self.StyleSetSpec(stc.STC_P_DEFNAME, "fore:#FF8080,bold")
        self.StyleSetSpec(stc.STC_P_OPERATOR, "")
        self.StyleSetSpec(stc.STC_P_IDENTIFIER, "")
        self.StyleSetSpec(stc.STC_P_COMMENTBLOCK, "fore:#808080")
        self.StyleSetSpec(stc.STC_P_STRINGEOL, "fore:{forecol},face:{mono},back:#1F3F1F,eolfilled".format(**faces))
        self.SetCaretForeground("{fore}".format(fore=self.fore))











