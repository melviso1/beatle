# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 27 2020)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.adv
import wx.xrc
import wx.html2
from beatle.lib import wxx
from beatle import local_path
import wx.richtext
from beatle.lib.wxx.agw import CustomTreeCtrl
from wx import aui

# special import for beatle development
from beatle.lib.handlers import identifier

ID_NEW_WORKSPACE = identifier('ID_NEW_WORKSPACE')
ID_NEW_WORKSPACE_AUI = identifier('ID_NEW_WORKSPACE_AUI')
ID_NEW_PROJECT = identifier('ID_NEW_PROJECT')
ID_NEW_PROJECT_AUI = identifier("ID_NEW_PROJECT_AUI")  # prevent AUI interference
ID_OPEN_WORKSPACE = identifier('ID_OPEN_WORKSPACE')
ID_OPEN_PROJECT = identifier('ID_OPEN_PROJECT')
ID_OPEN_PROJECT_AUI = identifier('ID_OPEN_PROJECT_AUI')
ID_CLOSE_WORKSPACE = identifier('ID_CLOSE_WORKSPACE')
ID_CLOSE_PROJECT = identifier('ID_CLOSE_PROJECT')
ID_IMPORT_PROJECT = identifier('ID_IMPORT_PROJECT')
ID_SAVE_WORKSPACE = identifier('ID_SAVE_WORKSPACE')
ID_SAVE_PROJECT = identifier('ID_SAVE_PROJECT')
ID_QUIT = identifier('ID_QUIT')
ID_UNDO = identifier('ID_UNDO')
ID_REDO = identifier('ID_REDO')
ID_COPY = identifier('ID_COPY')
ID_CUT = identifier('ID_CUT')
ID_PASTE = identifier('ID_PASTE')
ID_DELETE = identifier('ID_DELETE')
ID_EDIT_OPEN = identifier('ID_EDIT_OPEN')
ID_EDIT_CONTEXT = identifier('ID_EDIT_CONTEXT')
ID_EDIT_USER_SECTIONS = identifier('ID_EDIT_USER_SECTIONS')
ID_EDIT_PROPERTIES = identifier('ID_EDIT_PROPERTIES')
ID_PREFERENCES = identifier('ID_PREFERENCES')
ID_FIND_IN_FILES = identifier('ID_FIND_IN_FILES')
ID_DISCARD = identifier('ID_DISCARD')
ID_YESALL = identifier('ID_YESALL')


###########################################################################
## Class FontPreferencesPaneBase
###########################################################################

class FontPreferencesPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(500, 300),
                          style=wx.TAB_TRAVERSAL)

        fgSizer78 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer78.AddGrowableCol(1)
        fgSizer78.SetFlexibleDirection(wx.BOTH)
        fgSizer78.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText44 = wx.StaticText(self, wx.ID_ANY, u"Defaul text font:", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText44.Wrap(-1)
        fgSizer78.Add(self.m_staticText44, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_fontPicker = wx.FontPickerCtrl(self, wx.ID_ANY, wx.NullFont, wx.DefaultPosition, wx.DefaultSize,
                                              wx.FNTP_DEFAULT_STYLE)
        self.m_fontPicker.SetMaxPointSize(100)
        fgSizer78.Add(self.m_fontPicker, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText34 = wx.StaticText(self, wx.ID_ANY, u"Trees font:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText34.Wrap(-1)
        fgSizer78.Add(self.m_staticText34, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_treesFontPicker = wx.FontPickerCtrl(self, wx.ID_ANY, wx.NullFont, wx.DefaultPosition, wx.DefaultSize,
                                                   wx.FNTP_DEFAULT_STYLE)
        self.m_treesFontPicker.SetMaxPointSize(100)
        fgSizer78.Add(self.m_treesFontPicker, 0, wx.ALL | wx.EXPAND, 5)

        self.SetSizer(fgSizer78)
        self.Layout()

    def __del__(self):
        pass


###########################################################################
## Class NavigatorPaneBase
###########################################################################

# wx.html2 has some leak when we destroy all the instances.
# as workaround we dont delete any instance and simply reuse them
#
stock_html_items = []


class NavigatorPaneBase(wx.Panel):

    @classmethod
    def clean_browser_cache(cls):
        """callback for cleanup at end application"""
        global stock_html_items
        for html_client in stock_html_items:
            html_client.Destroy()
        stock_html_items = []

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(500, 300),
                          style=wx.TAB_TRAVERSAL | wx.WANTS_CHARS)
        self.SetExtraStyle(wx.WS_EX_BLOCK_EVENTS)
        fgSizer193 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer193.AddGrowableCol(0)
        fgSizer193.AddGrowableRow(1)
        fgSizer193.SetFlexibleDirection(wx.BOTH)
        fgSizer193.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self.m_url = wx.TextCtrl(self, wx.ID_ANY, u"aa", wx.DefaultPosition, wx.DefaultSize,
                                 wx.TE_PROCESS_ENTER | wx.SIMPLE_BORDER)
        self.m_url.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_SLANT, wx.FONTWEIGHT_NORMAL,
                    False, "Arial"))
        self.m_url.Hide()
        fgSizer193.Add(self.m_url, 0, wx.EXPAND, 5)
        if len(stock_html_items) == 0:
            self.m_page = wx.html2.WebView.New(self)
            from beatle.lib.api import context
            context.add_end_application_notify(self.clean_browser_cache)
        else:
            self.m_page = stock_html_items[0]
            del stock_html_items[0]
            self.m_page.Reparent(self)
            self.m_page.Show()
        fgSizer193.Add(self.m_page, 1, wx.EXPAND, 5)
        self.SetSizer(fgSizer193)
        self.Layout()

        # Connect Events
        self.m_url.Bind(wx.EVT_TEXT_ENTER, self.on_enter_url)

    def reserve_webview(self):
        """store webview element for later use"""
        self.m_page.Hide()
        self.m_page.Reparent(None)
        self.Update()
        stock_html_items.append(self.m_page)
        self.m_page = None

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_enter_url(self, event):
        event.Skip()


###########################################################################
## Class TasksPaneBase
###########################################################################

class TasksPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(731, 300),
                          style=wx.TAB_TRAVERSAL)
        fgSizer91 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer91.AddGrowableCol(1)
        fgSizer91.AddGrowableRow(0)
        fgSizer91.SetFlexibleDirection(wx.BOTH)
        fgSizer91.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self.m_listCtrl2 = wx.ListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT)
        fgSizer91.Add(self.m_listCtrl2, 1, wx.ALL | wx.EXPAND, 5)
        fgSizer92 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer92.AddGrowableCol(0)
        fgSizer92.AddGrowableRow(1)
        fgSizer92.SetFlexibleDirection(wx.BOTH)
        fgSizer92.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        fgSizer93 = wx.FlexGridSizer(2, 4, 0, 0)
        fgSizer93.AddGrowableCol(1)
        fgSizer93.AddGrowableCol(3)
        fgSizer93.SetFlexibleDirection(wx.BOTH)
        fgSizer93.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self.m_staticText50 = wx.StaticText(self, wx.ID_ANY, u"task", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText50.Wrap(-1)
        fgSizer93.Add(self.m_staticText50, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)
        self.m_textCtrl34 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer93.Add(self.m_textCtrl34, 0, wx.ALL | wx.EXPAND, 5)
        self.m_staticText51 = wx.StaticText(self, wx.ID_ANY, u"status", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText51.Wrap(-1)
        fgSizer93.Add(self.m_staticText51, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_choice20Choices = [u"pending", u"doing", u"done", wx.EmptyString, wx.EmptyString, wx.EmptyString]
        self.m_choice20 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice20Choices, 0)
        self.m_choice20.SetSelection(0)
        fgSizer93.Add(self.m_choice20, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText52 = wx.StaticText(self, wx.ID_ANY, u"priority", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText52.Wrap(-1)
        fgSizer93.Add(self.m_staticText52, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_choice21Choices = [u"High", u"Normal", u"Low"]
        self.m_choice21 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice21Choices, 0)
        self.m_choice21.SetSelection(1)
        fgSizer93.Add(self.m_choice21, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText53 = wx.StaticText(self, wx.ID_ANY, u"type", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText53.Wrap(-1)
        fgSizer93.Add(self.m_staticText53, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_choice22Choices = [u"BUG", u"CHANGE", u"IMPROVE"]
        self.m_choice22 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice22Choices, 0)
        self.m_choice22.SetSelection(1)
        fgSizer93.Add(self.m_choice22, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer92.Add(fgSizer93, 1, wx.EXPAND, 5)

        sbSizer22 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Comments"), wx.VERTICAL)

        self.m_richText16 = wx.richtext.RichTextCtrl(sbSizer22.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                     wx.DefaultPosition, wx.DefaultSize,
                                                     0 | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)
        sbSizer22.Add(self.m_richText16, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer92.Add(sbSizer22, 1, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        fgSizer91.Add(fgSizer92, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer91)
        self.Layout()

    def __del__(self):
        pass


###########################################################################
## Class WebPreferencesPaneBase
###########################################################################

class WebPreferencesPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(500, 300),
                          style=wx.TAB_TRAVERSAL)

        fgSizer61 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer61.AddGrowableCol(0)
        fgSizer61.AddGrowableRow(0)
        fgSizer61.AddGrowableRow(1)
        fgSizer61.SetFlexibleDirection(wx.BOTH)
        fgSizer61.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        connection = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"connection"), wx.VERTICAL)

        self.m_radioBtn4 = wx.RadioButton(connection.GetStaticBox(), wx.ID_ANY, u"Automatic network configuration",
                                          wx.DefaultPosition, wx.DefaultSize, 0)
        connection.Add(self.m_radioBtn4, 0, wx.ALL, 5)

        self.m_radioBtn5 = wx.RadioButton(connection.GetStaticBox(), wx.ID_ANY, u"Manual proxy settings",
                                          wx.DefaultPosition, wx.DefaultSize, 0)
        connection.Add(self.m_radioBtn5, 0, wx.ALL, 5)

        fgSizer62 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer62.AddGrowableCol(1)
        fgSizer62.AddGrowableRow(2)
        fgSizer62.SetFlexibleDirection(wx.BOTH)
        fgSizer62.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer62.Add((40, 0), 1, wx.EXPAND, 5)

        fgSizer63 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer63.AddGrowableCol(1)
        fgSizer63.SetFlexibleDirection(wx.BOTH)
        fgSizer63.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.st_http_proxy = wx.StaticText(connection.GetStaticBox(), wx.ID_ANY, u"http proxy:", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.st_http_proxy.Wrap(-1)
        self.st_http_proxy.Enable(False)

        fgSizer63.Add(self.st_http_proxy, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_http_proxy = wx.TextCtrl(connection.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                        wx.DefaultSize, 0 | wx.STATIC_BORDER)
        self.m_http_proxy.Enable(False)

        fgSizer63.Add(self.m_http_proxy, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer62.Add(fgSizer63, 1, wx.EXPAND, 5)

        fgSizer62.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox6 = wx.CheckBox(connection.GetStaticBox(), wx.ID_ANY, u"use same proxy for all protocols",
                                       wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox6.SetValue(True)
        self.m_checkBox6.Enable(False)

        fgSizer62.Add(self.m_checkBox6, 0, wx.ALL, 5)

        fgSizer62.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer631 = wx.FlexGridSizer(2, 2, 0, 0)
        fgSizer631.AddGrowableCol(1)
        fgSizer631.SetFlexibleDirection(wx.BOTH)
        fgSizer631.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.st_https_proxy = wx.StaticText(connection.GetStaticBox(), wx.ID_ANY, u"https proxy:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.st_https_proxy.Wrap(-1)
        self.st_https_proxy.Enable(False)

        fgSizer631.Add(self.st_https_proxy, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_https_proxy = wx.TextCtrl(connection.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, 0 | wx.STATIC_BORDER)
        self.m_https_proxy.Enable(False)

        fgSizer631.Add(self.m_https_proxy, 0, wx.ALL | wx.EXPAND, 5)

        self.st_ftp_proxy = wx.StaticText(connection.GetStaticBox(), wx.ID_ANY, u"ftp proxy:", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.st_ftp_proxy.Wrap(-1)
        self.st_ftp_proxy.Enable(False)

        fgSizer631.Add(self.st_ftp_proxy, 0, wx.ALL, 5)

        self.m_ftp_proxy = wx.TextCtrl(connection.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                       wx.DefaultSize, 0 | wx.STATIC_BORDER)
        self.m_ftp_proxy.Enable(False)

        fgSizer631.Add(self.m_ftp_proxy, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer62.Add(fgSizer631, 1, wx.EXPAND, 5)

        connection.Add(fgSizer62, 1, wx.EXPAND, 5)

        fgSizer61.Add(connection, 1, wx.EXPAND, 5)

        self.m_button4 = wx.Button(self, wx.ID_ANY, u"Apply", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button4.Enable(False)

        fgSizer61.Add(self.m_button4, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.SetSizer(fgSizer61)
        self.Layout()

        # Connect Events
        self.m_radioBtn4.Bind(wx.EVT_RADIOBUTTON, self.OnAutoNetwork)
        self.m_radioBtn5.Bind(wx.EVT_RADIOBUTTON, self.OnManualProxy)
        self.m_http_proxy.Bind(wx.EVT_TEXT, self.OnChangeHttpProxy)
        self.m_checkBox6.Bind(wx.EVT_CHECKBOX, self.OnSameProxy)
        self.m_https_proxy.Bind(wx.EVT_TEXT, self.OnChangeHttpsProxy)
        self.m_ftp_proxy.Bind(wx.EVT_TEXT, self.OnChangeFtpProxy)
        self.m_button4.Bind(wx.EVT_BUTTON, self.OnApply)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnAutoNetwork(self, event):
        event.Skip()

    def OnManualProxy(self, event):
        event.Skip()

    def OnChangeHttpProxy(self, event):
        event.Skip()

    def OnSameProxy(self, event):
        event.Skip()

    def OnChangeHttpsProxy(self, event):
        event.Skip()

    def OnChangeFtpProxy(self, event):
        event.Skip()

    def OnApply(self, event):
        event.Skip()


###########################################################################
## Class HelpPreferencesPaneBase
###########################################################################

class HelpPreferencesPaneBase(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(616, 334),
                          style=wx.TAB_TRAVERSAL)

        sbSizer14 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"help items"), wx.VERTICAL)

        fgSizer65 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer65.AddGrowableCol(0)
        fgSizer65.AddGrowableRow(0)
        fgSizer65.SetFlexibleDirection(wx.BOTH)
        fgSizer65.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_help_items = wx.ListCtrl(sbSizer14.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                        wx.LC_AUTOARRANGE | wx.LC_NO_HEADER | wx.LC_REPORT | wx.LC_SINGLE_SEL)
        fgSizer65.Add(self.m_help_items, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer66 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer66.AddGrowableCol(0)
        fgSizer66.AddGrowableRow(3)
        fgSizer66.SetFlexibleDirection(wx.BOTH)
        fgSizer66.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_new_btn = wx.Button(sbSizer14.GetStaticBox(), wx.ID_ANY, u"New", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer66.Add(self.m_new_btn, 0, wx.ALL | wx.EXPAND, 5)

        self.m_edit_btn = wx.Button(sbSizer14.GetStaticBox(), wx.ID_ANY, u"Edit", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_edit_btn.Enable(False)

        fgSizer66.Add(self.m_edit_btn, 0, wx.ALL | wx.EXPAND, 5)

        self.m_del_btn = wx.Button(sbSizer14.GetStaticBox(), wx.ID_ANY, u"Delete", wx.DefaultPosition, wx.DefaultSize,
                                   0)
        self.m_del_btn.Enable(False)

        fgSizer66.Add(self.m_del_btn, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer65.Add(fgSizer66, 1, wx.EXPAND, 5)

        sbSizer14.Add(fgSizer65, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(sbSizer14)
        self.Layout()

        # Connect Events
        self.m_help_items.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_enter_item)
        self.m_help_items.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.on_deselect_item)
        self.m_help_items.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_select_item)
        self.m_new_btn.Bind(wx.EVT_BUTTON, self.on_new_item)
        self.m_edit_btn.Bind(wx.EVT_BUTTON, self.on_edit_item)
        self.m_del_btn.Bind(wx.EVT_BUTTON, self.on_delete_item)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_enter_item(self, event):
        event.Skip()

    def on_deselect_item(self, event):
        event.Skip()

    def on_select_item(self, event):
        event.Skip()

    def on_new_item(self, event):
        event.Skip()

    def on_edit_item(self, event):
        event.Skip()

    def on_delete_item(self, event):
        event.Skip()


###########################################################################
## Class HelpItemDialogBase
###########################################################################

class HelpItemDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New help item", pos=wx.DefaultPosition,
                           size=wx.Size(568, 252), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer125 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer125.AddGrowableCol(0)
        fgSizer125.AddGrowableRow(0)
        fgSizer125.AddGrowableRow(2)
        fgSizer125.SetFlexibleDirection(wx.BOTH)
        fgSizer125.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer125.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer126 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer126.AddGrowableCol(1)
        fgSizer126.SetFlexibleDirection(wx.HORIZONTAL)
        fgSizer126.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_ALL)

        self.m_staticText85 = wx.StaticText(self, wx.ID_ANY, u"menu label:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText85.Wrap(-1)
        fgSizer126.Add(self.m_staticText85, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl56 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_textCtrl56.SetMinSize(wx.Size(150, -1))

        fgSizer126.Add(self.m_textCtrl56, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 5)

        self.m_staticText35 = wx.StaticText(self, wx.ID_ANY, u"url:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText35.Wrap(-1)
        fgSizer126.Add(self.m_staticText35, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl29 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer126.Add(self.m_textCtrl29, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText351 = wx.StaticText(self, wx.ID_ANY, u"help string:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText351.Wrap(-1)
        fgSizer126.Add(self.m_staticText351, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl30 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer126.Add(self.m_textCtrl30, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer125.Add(fgSizer126, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer125.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer158 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer158.AddGrowableCol(1)
        fgSizer158.SetFlexibleDirection(wx.BOTH)
        fgSizer158.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer158.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer10 = wx.StdDialogButtonSizer()
        self.m_sdbSizer10OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer10.AddButton(self.m_sdbSizer10OK)
        self.m_sdbSizer10Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer10.AddButton(self.m_sdbSizer10Cancel)
        m_sdbSizer10.Realize();

        fgSizer158.Add(m_sdbSizer10, 1, wx.EXPAND, 5)

        fgSizer125.Add(fgSizer158, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer125)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer10OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class FileDialogBase
###########################################################################

class FileDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"NewFile", pos=wx.DefaultPosition, size=wx.Size(260, 120),
                           style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer125 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer125.AddGrowableCol(0)
        fgSizer125.AddGrowableRow(0)
        fgSizer125.SetFlexibleDirection(wx.BOTH)
        fgSizer125.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer126 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer126.AddGrowableCol(1)
        fgSizer126.SetFlexibleDirection(wx.BOTH)
        fgSizer126.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_ALL)

        self.m_staticText85 = wx.StaticText(self, wx.ID_ANY, u"File name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText85.Wrap(-1)
        fgSizer126.Add(self.m_staticText85, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl56 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_textCtrl56.SetMinSize(wx.Size(150, -1))

        fgSizer126.Add(self.m_textCtrl56, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND,
                       5)

        fgSizer125.Add(fgSizer126, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer158 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer158.AddGrowableCol(1)
        fgSizer158.SetFlexibleDirection(wx.BOTH)
        fgSizer158.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer158.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer10 = wx.StdDialogButtonSizer()
        self.m_sdbSizer10OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer10.AddButton(self.m_sdbSizer10OK)
        self.m_sdbSizer10Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer10.AddButton(self.m_sdbSizer10Cancel)
        m_sdbSizer10.Realize();

        fgSizer158.Add(m_sdbSizer10, 1, wx.EXPAND, 5)

        fgSizer125.Add(fgSizer158, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer125)
        self.Layout()

        self.Centre(wx.BOTH)

    def __del__(self):
        pass


###########################################################################
## Class FolderDialogBase
###########################################################################

class FolderDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New folder", pos=wx.DefaultPosition,
                           size=wx.Size(423, 390), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer6 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer6.AddGrowableCol(0)
        fgSizer6.AddGrowableRow(1)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer7 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer7.AddGrowableCol(1)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer7.Add(self.m_staticText4, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer7.Add(self.m_textCtrl2, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)

        fgSizer6.Add(fgSizer7, 0, wx.EXPAND | wx.ALL, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    wx.SIMPLE_BORDER | wx.HSCROLL | wx.VSCROLL | wx.WANTS_CHARS)
        self.m_richText3.SetMargins(left=20, top=20)
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer6.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer162 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer162.AddGrowableCol(1)
        fgSizer162.SetFlexibleDirection(wx.BOTH)
        fgSizer162.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer162.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK)
        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel)
        m_sdbSizer2.Realize();

        fgSizer162.Add(m_sdbSizer2, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        fgSizer6.Add(fgSizer162, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer6)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer2OK.Bind(wx.EVT_BUTTON, self.on_ok)
        self.m_sdbSizer2Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

###########################################################################
## Class CodeNavigatorDialogBase
###########################################################################

class CodeNavigatorDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.Point(-1, -1),
                           size=wx.Size(284, 404), style=0 | wx.NO_BORDER | wx.WANTS_CHARS)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle(self.GetExtraStyle() | wx.WS_EX_BLOCK_EVENTS)

        fgSizer131 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer131.AddGrowableCol(0)
        fgSizer131.AddGrowableRow(0)
        fgSizer131.SetFlexibleDirection(wx.BOTH)
        fgSizer131.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_treeCtrl3 = wx.TreeCtrl(self, wx.ID_ANY, wx.Point(0, 0), wx.DefaultSize,
                                       wx.TR_HAS_BUTTONS | wx.TR_HIDE_ROOT | wx.TR_SINGLE)
        fgSizer131.Add(self.m_treeCtrl3, 0, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(fgSizer131)
        self.Layout()

        # Connect Events
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.m_treeCtrl3.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.m_treeCtrl3.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.OnSelectedItem)
        self.m_treeCtrl3.Bind(wx.EVT_TREE_ITEM_EXPANDED, self.on_expand_item)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnKeyDown(self, event):
        event.Skip()

    def OnSelectedItem(self, event):
        event.Skip()

    def on_expand_item(self, event):
        event.Skip()


###########################################################################
## Class ImportDialogBase
###########################################################################

class ImportDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                           size=wx.Size(387, 164), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        self.Centre(wx.BOTH)

    def __del__(self):
        pass


###########################################################################
## Class NoteDialogBase
###########################################################################

class NoteDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New note", pos=wx.DefaultPosition,
                           size=wx.Size(480, 299), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer103 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer103.AddGrowableCol(0)
        fgSizer103.AddGrowableRow(0)
        fgSizer103.SetFlexibleDirection(wx.BOTH)
        fgSizer103.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer30 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, wx.EmptyString), wx.VERTICAL)

        self.m_text = wx.richtext.RichTextCtrl(sbSizer30.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.DefaultSize,
                                               0 | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)
        sbSizer30.Add(self.m_text, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer103.Add(sbSizer30, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer156 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer156.AddGrowableCol(1)
        fgSizer156.SetFlexibleDirection(wx.BOTH)
        fgSizer156.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer156.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer7 = wx.StdDialogButtonSizer()
        self.m_sdbSizer7OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer7.AddButton(self.m_sdbSizer7OK)
        self.m_sdbSizer7Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer7.AddButton(self.m_sdbSizer7Cancel)
        m_sdbSizer7.Realize();

        fgSizer156.Add(m_sdbSizer7, 1, wx.EXPAND, 5)

        fgSizer103.Add(fgSizer156, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer103)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer7OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class ProjectDialogBase
###########################################################################

class ProjectDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New Project", pos=wx.DefaultPosition,
                           size=wx.Size(437, 561), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.fgSizer4 = wx.FlexGridSizer(2, 1, 0, 0)
        self.fgSizer4.AddGrowableCol(0)
        self.fgSizer4.AddGrowableRow(0)
        self.fgSizer4.SetFlexibleDirection(wx.BOTH)
        self.fgSizer4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_ALL)

        self.m_auinotebook2 = wxx.AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NB_BOTTOM)
        self.m_panel5 = wx.Panel(self.m_auinotebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.fgSizer65 = wx.FlexGridSizer(3, 1, 0, 0)
        self.fgSizer65.AddGrowableCol(0)
        self.fgSizer65.AddGrowableRow(1)
        self.fgSizer65.SetFlexibleDirection(wx.BOTH)
        self.fgSizer65.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer5 = wx.FlexGridSizer(5, 2, 0, 0)
        fgSizer5.AddGrowableCol(1)
        fgSizer5.SetFlexibleDirection(wx.BOTH)
        fgSizer5.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText2 = wx.StaticText(self.m_panel5, wx.ID_ANY, u"Name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        fgSizer5.Add(self.m_staticText2, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl1 = wx.TextCtrl(self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer5.Add(self.m_textCtrl1, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText3 = wx.StaticText(self.m_panel5, wx.ID_ANY, u"Base directory:",
                                           wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        fgSizer5.Add(self.m_staticText3, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer29 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer29.AddGrowableCol(0)
        fgSizer29.SetFlexibleDirection(wx.BOTH)
        fgSizer29.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_textCtrl9 = wx.TextCtrl(self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_READONLY)
        fgSizer29.Add(self.m_textCtrl9, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_bpButton3 = wx.BitmapButton(
            self.m_panel5, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_FOLDER_OPEN, wx.ART_BUTTON),
            wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer29.Add(self.m_bpButton3, 0, wx.ALL, 5)
        fgSizer5.Add(fgSizer29, 1, wx.EXPAND, 5)
        self.fgSizer65.Add(fgSizer5, 1, wx.EXPAND | wx.ALL, 5)

        self.m_choicebook1 = wx.Choicebook(self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT)
        self.m_panel18 = wx.Panel(self.m_choicebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer70 = wx.FlexGridSizer(5, 2, 0, 0)
        fgSizer70.AddGrowableCol(1)
        fgSizer70.SetFlexibleDirection(wx.BOTH)
        fgSizer70.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText30 = wx.StaticText(self.m_panel18, wx.ID_ANY, u"Headers subdir.:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText30.Wrap(-1)
        fgSizer70.Add(self.m_staticText30, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl23 = wx.TextCtrl(self.m_panel18, wx.ID_ANY, u"include", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer70.Add(self.m_textCtrl23, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText31 = wx.StaticText(self.m_panel18, wx.ID_ANY, u"Sources subdir.:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText31.Wrap(-1)
        fgSizer70.Add(self.m_staticText31, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl24 = wx.TextCtrl(self.m_panel18, wx.ID_ANY, u"src", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer70.Add(self.m_textCtrl24, 0, wx.ALL | wx.EXPAND, 5)

        self.m_checkBox52 = wx.CheckBox(self.m_panel18, wx.ID_ANY, u"master include", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox52.SetValue(True)
        fgSizer70.Add(self.m_checkBox52, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl25 = wx.TextCtrl(self.m_panel18, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        fgSizer70.Add(self.m_textCtrl25, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer70.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_checkBox7 = wx.CheckBox(self.m_panel18, wx.ID_ANY, u"serialization support", wx.DefaultPosition,
                                       wx.DefaultSize, 0)

        fgSizer70.Add(self.m_checkBox7, 0, wx.ALL, 5)

        fgSizer70.Add((0, 0),1, wx.EXPAND, 5)

        self.m_checkBox8 = wx.CheckBox(self.m_panel18, wx.ID_ANY, u"undo/redo support", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        self.m_checkBox8.Enable(False)

        fgSizer70.Add(self.m_checkBox8, 0, wx.ALL, 5)

        self.m_panel18.SetSizer(fgSizer70)
        self.m_panel18.Layout()
        fgSizer70.Fit(self.m_panel18)
        self.m_choicebook1.AddPage(self.m_panel18, u"c++ project", True)
        self.m_panel19 = wx.Panel(self.m_choicebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_choicebook1.AddPage(self.m_panel19, u"python project", False)
        self.m_panel13 = wx.Panel(self.m_choicebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer58 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer58.AddGrowableCol(0)
        fgSizer58.AddGrowableRow(0)
        fgSizer58.SetFlexibleDirection(wx.BOTH)
        fgSizer58.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_choicebook4 = wx.Choicebook(self.m_panel13, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                           wx.CHB_DEFAULT)
        self.m_panel14 = wx.Panel(self.m_choicebook4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer59 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer59.AddGrowableCol(0)
        fgSizer59.SetFlexibleDirection(wx.BOTH)
        fgSizer59.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer60 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer60.AddGrowableCol(1)
        fgSizer60.SetFlexibleDirection(wx.BOTH)
        fgSizer60.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText29 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"Host:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText29.Wrap(-1)
        fgSizer60.Add(self.m_staticText29, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl252 = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                         0)
        fgSizer60.Add(self.m_textCtrl252, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText27 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"User:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText27.Wrap(-1)
        fgSizer60.Add(self.m_staticText27, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl231 = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                         0)
        fgSizer60.Add(self.m_textCtrl231, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText28 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"Password:", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText28.Wrap(-1)
        fgSizer60.Add(self.m_staticText28, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_textCtrl241 = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                         wx.TE_PASSWORD)
        fgSizer60.Add(self.m_textCtrl241, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer59.Add(fgSizer60, 1, wx.EXPAND, 5)

        sbSizer8 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel14, wx.ID_ANY, u"Default schema"), wx.VERTICAL)

        m_comboBox1Choices = []
        self.m_comboBox1 = wx.ComboBox(sbSizer8.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                       wx.DefaultSize, m_comboBox1Choices, 0)
        sbSizer8.Add(self.m_comboBox1, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer59.Add(sbSizer8, 1, wx.EXPAND, 5)

        fgSizer61 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer61.AddGrowableCol(0)
        fgSizer61.SetFlexibleDirection(wx.BOTH)
        fgSizer61.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer61.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_button3 = wx.Button(self.m_panel14, wx.ID_ANY, u"Test connection", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button3.Enable(False)

        fgSizer61.Add(self.m_button3, 0, wx.ALL, 5)

        fgSizer59.Add(fgSizer61, 1, wx.EXPAND, 5)

        self.m_panel14.SetSizer(fgSizer59)
        self.m_panel14.Layout()
        fgSizer59.Fit(self.m_panel14)
        self.m_choicebook4.AddPage(self.m_panel14, u"mysql", False)
        fgSizer58.Add(self.m_choicebook4, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        self.m_panel13.SetSizer(fgSizer58)
        self.m_panel13.Layout()
        fgSizer58.Fit(self.m_panel13)
        self.m_choicebook1.AddPage(self.m_panel13, u"database project", False)
        self.fgSizer65.Add(self.m_choicebook1, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel5.SetSizer(self.fgSizer65)
        self.m_panel5.Layout()
        self.fgSizer65.Fit(self.m_panel5)
        self.m_auinotebook2.AddPage(self.m_panel5, u"General", True)
        self.m_panel6 = wx.Panel(self.m_auinotebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer69 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer69.AddGrowableCol(0)
        fgSizer69.AddGrowableRow(1)
        fgSizer69.SetFlexibleDirection(wx.BOTH)
        fgSizer69.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer68 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer68.AddGrowableCol(1)
        fgSizer68.SetFlexibleDirection(wx.BOTH)
        fgSizer68.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText33 = wx.StaticText(self.m_panel6, wx.ID_ANY, u"Author:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText33.Wrap(-1)
        fgSizer68.Add(self.m_staticText33, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl251 = wx.TextCtrl(self.m_panel6, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                         0)
        fgSizer68.Add(self.m_textCtrl251, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText35 = wx.StaticText(self.m_panel6, wx.ID_ANY, u"Date:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText35.Wrap(-1)
        fgSizer68.Add(self.m_staticText35, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_datePicker1 = wx.adv.DatePickerCtrl(self.m_panel6, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition,
                                                   wx.DefaultSize, wx.adv.DP_DEFAULT | wx.adv.DP_SHOWCENTURY)
        fgSizer68.Add(self.m_datePicker1, 0, wx.ALL | wx.EXPAND, 5)

        self.m_checkBox53 = wx.CheckBox(self.m_panel6, wx.ID_ANY, u"Add license:", wx.DefaultPosition, wx.DefaultSize,
                                        0)
        fgSizer68.Add(self.m_checkBox53, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        m_choice16Choices = [u"wxWidgets license", u"GNU GPL", u"GNU LGPL", u"FreeBSD", u"Creative Commons"]
        self.m_choice16 = wx.Choice(self.m_panel6, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice16Choices, 0)
        self.m_choice16.SetSelection(0)
        fgSizer68.Add(self.m_choice16, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer69.Add(fgSizer68, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        sbSizer20 = wx.StaticBoxSizer(wx.StaticBox(self.m_panel6, wx.ID_ANY, u"description"), wx.VERTICAL)

        self.m_richText13 = wx.richtext.RichTextCtrl(sbSizer20.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                     wx.DefaultPosition, wx.DefaultSize,
                                                     0 | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)
        sbSizer20.Add(self.m_richText13, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer69.Add(sbSizer20, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        self.m_panel6.SetSizer(fgSizer69)
        self.m_panel6.Layout()
        fgSizer69.Fit(self.m_panel6)
        self.m_auinotebook2.AddPage(self.m_panel6, u"Authoring", False)
        self.m_panel7 = wx.Panel(self.m_auinotebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer72 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer72.AddGrowableCol(0)
        fgSizer72.AddGrowableRow(0)
        fgSizer72.SetFlexibleDirection(wx.BOTH)
        fgSizer72.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer73 = wx.FlexGridSizer(3, 2, 0, 0)
        fgSizer73.AddGrowableCol(1)
        fgSizer73.SetFlexibleDirection(wx.BOTH)
        fgSizer73.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText3111 = wx.StaticText(self.m_panel7, wx.ID_ANY, u"Project type:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText3111.Wrap(-1)
        fgSizer73.Add(self.m_staticText3111, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        m_choice151Choices = [u"unspecified", u"static library", u"shared library", u"executable", u"only code"]
        self.m_choice151 = wx.Choice(self.m_panel7, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice151Choices,
                                     0)
        self.m_choice151.SetSelection(3)
        fgSizer73.Add(self.m_choice151, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText321 = wx.StaticText(self.m_panel7, wx.ID_ANY, u"Versión:", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText321.Wrap(-1)
        fgSizer73.Add(self.m_staticText321, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        gSizer1 = wx.GridSizer(1, 3, 0, 0)

        self.m_spinCtrl11 = wx.SpinCtrl(self.m_panel7, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size(40, 40),
                                        wx.SP_ARROW_KEYS, 0, 10, 0)
        gSizer1.Add(self.m_spinCtrl11, 1, wx.ALL | wx.EXPAND, 5)

        self.m_spinCtrl21 = wx.SpinCtrl(self.m_panel7, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size(40, 40),
                                        wx.SP_ARROW_KEYS, 0, 10, 0)
        gSizer1.Add(self.m_spinCtrl21, 1, wx.ALL | wx.EXPAND, 5)

        self.m_spinCtrl31 = wx.SpinCtrl(self.m_panel7, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size(40, 40),
                                        wx.SP_ARROW_KEYS, 0, 10, 0)
        gSizer1.Add(self.m_spinCtrl31, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer73.Add(gSizer1, 1, wx.EXPAND, 5)

        self.m_staticText43 = wx.StaticText(self.m_panel7, wx.ID_ANY, u"Build:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText43.Wrap(-1)
        fgSizer73.Add(self.m_staticText43, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_checkBox54 = wx.CheckBox(self.m_panel7, wx.ID_ANY, u"create makefile", wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        fgSizer73.Add(self.m_checkBox54, 0, wx.ALL, 5)

        fgSizer72.Add(fgSizer73, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        self.m_panel7.SetSizer(fgSizer72)
        self.m_panel7.Layout()
        fgSizer72.Fit(self.m_panel7)
        self.m_auinotebook2.AddPage(self.m_panel7, u"Generation", False)

        self.fgSizer4.Add(self.m_auinotebook2, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer161 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer161.AddGrowableCol(1)
        fgSizer161.SetFlexibleDirection(wx.BOTH)
        fgSizer161.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer161.SetMinSize(wx.Size(-1, 28))
        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.Size(-1, 28), wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer161.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer1 = wx.StdDialogButtonSizer()
        self.m_sdbSizer1OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer1.AddButton(self.m_sdbSizer1OK)
        self.m_sdbSizer1Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer1.AddButton(self.m_sdbSizer1Cancel)
        m_sdbSizer1.Realize();
        m_sdbSizer1.SetMinSize(wx.Size(-1, 32))

        fgSizer161.Add(m_sdbSizer1, 0, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.RIGHT, 5)

        self.fgSizer4.Add(fgSizer161, 0, wx.EXPAND, 5)

        self.SetSizer(self.fgSizer4)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_textCtrl1.Bind(wx.EVT_TEXT, self.on_change_project_name)
        self.m_bpButton3.Bind(wx.EVT_BUTTON, self.on_choose_dir)
        self.m_choicebook1.Bind(wx.EVT_CHOICEBOOK_PAGE_CHANGED, self.on_page_changed)
        self.m_choicebook1.Bind(wx.EVT_CHOICEBOOK_PAGE_CHANGING, self.on_page_changing)
        self.m_checkBox52.Bind(wx.EVT_CHECKBOX, self.on_toggle_master_include)
        self.m_checkBox7.Bind(wx.EVT_CHECKBOX, self.on_serialization_support)
        self.m_checkBox8.Bind(wx.EVT_CHECKBOX, self.on_undo_redo_support)
        self.m_textCtrl252.Bind(wx.EVT_TEXT, self.on_mysql_host_change)
        self.m_textCtrl231.Bind(wx.EVT_TEXT, self.on_mysql_user_change)
        self.m_textCtrl241.Bind(wx.EVT_TEXT, self.on_mysql_password_change)
        self.m_button3.Bind(wx.EVT_BUTTON, self.on_test_database_connection)
        self.m_sdbSizer1OK.Bind(wx.EVT_BUTTON, self.on_ok)
        self.m_sdbSizer1Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)

    def __del__(self):
        pass

    def on_change_project_name(self, event):
        event.Skip()

    def on_choose_dir(self, event):
        event.Skip()

    def on_page_changed(self, event):
        event.Skip()

    def on_page_changing(self, event):
        event.Skip()

    def on_toggle_master_include(self, event):
        event.Skip()

    def on_serialization_support(self, event):
        event.Skip()

    def on_undo_redo_support(self, event):
        event.Skip()

    def on_mysql_host_change(self, event):
        event.Skip()

    def on_mysql_user_change(self, event):
        event.Skip()

    def on_mysql_password_change(self, event):
        event.Skip()

    def on_test_database_connection(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()


###########################################################################
## Class PreferencesDialogBase
###########################################################################

class PreferencesDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                           size=wx.Size(913, 621), style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer77 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer77.AddGrowableCol(0)
        fgSizer77.AddGrowableRow(0)
        fgSizer77.SetFlexibleDirection(wx.BOTH)
        fgSizer77.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_treeBook = wx.Treebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                      wx.NB_LEFT | wx.BORDER_SUNKEN)

        fgSizer77.Add(self.m_treeBook, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer164 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer164.AddGrowableCol(1)
        fgSizer164.SetFlexibleDirection(wx.HORIZONTAL)
        fgSizer164.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_NONE)

        fgSizer164.SetMinSize(wx.Size(200, 32))
        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer164.Add(self.m_info, 0, wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        m_sdbSizer3 = wx.StdDialogButtonSizer()
        self.m_sdbSizer3OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer3.AddButton(self.m_sdbSizer3OK)
        self.m_sdbSizer3Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer3.AddButton(self.m_sdbSizer3Cancel)
        m_sdbSizer3.Realize();
        m_sdbSizer3.SetMinSize(wx.Size(100, 32))

        fgSizer164.Add(m_sdbSizer3, 0, wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer77.Add(fgSizer164, 0, wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.TOP | wx.BOTTOM, 5)

        self.SetSizer(fgSizer77)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer3OK.Bind(wx.EVT_BUTTON, self.on_ok)
        self.m_sdbSizer3Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()


###########################################################################
## Class FullScreenWindowBase
###########################################################################

class FullScreenWindowBase(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(500, 300),
                          style=wx.MAXIMIZE | wx.STAY_ON_TOP | wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.BORDER_NONE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle(wx.WS_EX_BLOCK_EVENTS | wx.WS_EX_TRANSIENT)

        self.Centre(wx.BOTH)

    def __del__(self):
        pass


###########################################################################
## Class MainWindowBase
###########################################################################
from beatle.lib.wxx import RendererDarkTheme


class MainWindowBase(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"beatle", pos=wx.DefaultPosition,
                          size=wx.Size(1124, 740), style=wx.DEFAULT_FRAME_STYLE | wx.SYSTEM_MENU)
        dbc = wx.TheColourDatabase
        fore = dbc.Find('LIGHT GREY')
        back = dbc.Find('DARK GREY')

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.Hide()
        self.m_mgr = wx.aui.AuiManager()
        self.m_mgr.SetManagedWindow(self)
        self.m_mgr.SetFlags(
            wx.aui.AUI_MGR_ALLOW_FLOATING | wx.aui.AUI_MGR_TRANSPARENT_DRAG | wx.aui.AUI_MGR_TRANSPARENT_HINT)
        self.m_mgr.GetArtProvider().SetMetric(wx.aui.AUI_DOCKART_PANE_BORDER_SIZE, 0)

        self.m_menubar1 = wxx.MenuBar(self, wx.MB_DOCKABLE)
        self.m_menubar1._isLCD = True
        self.menuFile = wxx.Menu()
        self.newWorkspace = wxx.MenuItem(self.menuFile, ID_NEW_WORKSPACE, u"New workspace", u"create a new workspace",
                                         wx.ITEM_NORMAL)
        self.newWorkspace.SetBitmap(wx.Bitmap(local_path('app/res/workspace.xpm'), wx.BITMAP_TYPE_ANY))
        self.menuFile.Append(self.newWorkspace)

        self.newProject = wxx.MenuItem(self.menuFile, ID_NEW_PROJECT, u"New project ..." + u"\t" + u"Ctrl+N",
                                       u"create a new project", wx.ITEM_NORMAL)
        self.newProject.SetBitmap(wx.Bitmap(local_path('app/res/new_project.xpm'), wx.BITMAP_TYPE_ANY))
        self.menuFile.Append(self.newProject)

        self.menuFile.AppendSeparator()

        self.openWorkspace = wxx.MenuItem(self.menuFile, ID_OPEN_WORKSPACE, u"Open workspace ...",
                                          u"open an existing workspace", wx.ITEM_NORMAL)
        self.menuFile.Append(self.openWorkspace)

        self.openProject = wxx.MenuItem(self.menuFile, ID_OPEN_PROJECT, u"Open project ..." + u"\t" + u"Ctrl+O",
                                        u"open an existing project", wx.ITEM_NORMAL)
        self.openProject.SetBitmap(wx.Bitmap(local_path('app/res/open_project.xpm'), wx.BITMAP_TYPE_ANY))
        self.menuFile.Append(self.openProject)

        self.menuMRU = wxx.Menu()
        self.menuFile.AppendSubMenu(self.menuMRU, u"Recent files")

        self.menuFile.AppendSeparator()

        self.m_menuItem26 = wxx.MenuItem(self.menuFile, ID_CLOSE_WORKSPACE, u"Close workspace", wx.EmptyString,
                                         wx.ITEM_NORMAL)
        self.menuFile.Append(self.m_menuItem26)

        self.m_menuItem27 = wxx.MenuItem(self.menuFile, ID_CLOSE_PROJECT, u"Close project", wx.EmptyString,
                                         wx.ITEM_NORMAL)
        self.menuFile.Append(self.m_menuItem27)

        self.menuFile.AppendSeparator()

        self.importProject = wxx.MenuItem(self.menuFile, ID_IMPORT_PROJECT, u"Import ...",
                                          u"import external project in current workspace", wx.ITEM_NORMAL)
        self.menuFile.Append(self.importProject)

        self.menuFile.AppendSeparator()

        self.saveWorkspace = wxx.MenuItem(self.menuFile, ID_SAVE_WORKSPACE, u"Save workspace",
                                          u"save current workspace", wx.ITEM_NORMAL)
        self.saveWorkspace.SetBitmap(wx.Bitmap(local_path('app/res/save_all.xpm'), wx.BITMAP_TYPE_ANY))
        self.menuFile.Append(self.saveWorkspace)

        self.save_project = wxx.MenuItem(self.menuFile, ID_SAVE_PROJECT, u"Save project" + u"\t" + u"Ctrl+S",
                                        u"save current project", wx.ITEM_NORMAL)
        self.save_project.SetBitmap(wx.Bitmap(local_path("app/res/save.xpm"), wx.BITMAP_TYPE_ANY))
        # self.save_project.SetBitmap( wx.ArtProvider.GetBitmap(u"gtk-save", wx.ART_MENU ) )
        self.menuFile.Append(self.save_project)

        self.menuFile.AppendSeparator()

        self.quit = wxx.MenuItem(self.menuFile, ID_QUIT, u"Quit" + u"\t" + u"Alt+F4", u"exit application",
                                 wx.ITEM_NORMAL)
        self.quit.SetBitmap(wx.ArtProvider.GetBitmap(u"gtk-quit", wx.ART_MENU))
        self.menuFile.Append(self.quit)

        self.m_menubar1.Append(self.menuFile, u"&Main")

        self.menuEdit = wxx.Menu()
        self.undo = wxx.MenuItem(self.menuEdit, ID_UNDO, u"Undo" + u"\t" + u"Ctrl+Z", u"undoes the last operation",
                                 wx.ITEM_NORMAL)
        self.undo.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_UNDO, wx.ART_TOOLBAR))
        self.menuEdit.Append(self.undo)

        self.redo = wxx.MenuItem(self.menuEdit, ID_REDO, u"Redo" + u"\t" + u"Ctrl+Y", u"redoes the last operation",
                                 wx.ITEM_NORMAL)
        self.redo.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_REDO, wx.ART_TOOLBAR))
        self.menuEdit.Append(self.redo)

        self.menuEdit.AppendSeparator()

        self.copy = wxx.MenuItem(self.menuEdit, ID_COPY, u"Copy" + u"\t" + u"Ctrl+C", u"copy the selected element",
                                 wx.ITEM_NORMAL)
        self.copy.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_COPY, wx.ART_MENU))
        self.menuEdit.Append(self.copy)

        self.cut = wxx.MenuItem(self.menuEdit, ID_CUT, u"Cut" + u"\t" + u"Ctrl+X", u"cut the selected element",
                                wx.ITEM_NORMAL)
        self.cut.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_CUT, wx.ART_MENU))
        self.menuEdit.Append(self.cut)

        self.paste = wxx.MenuItem(self.menuEdit, ID_PASTE, u"Paste" + u"\t" + u"Ctrl+V", u"paste from clipboard",
                                  wx.ITEM_NORMAL)
        self.paste.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_PASTE, wx.ART_MENU))
        self.menuEdit.Append(self.paste)

        self.menuEdit.AppendSeparator()

        self.delete = wxx.MenuItem(self.menuEdit, ID_DELETE, u"Delete" + u"\t" + u"Del", u"delete the selected element",
                                   wx.ITEM_NORMAL)
        self.delete.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_MENU))
        self.menuEdit.Append(self.delete)

        self.menuEdit.AppendSeparator()

        self.editOpen = wxx.MenuItem(self.menuEdit, ID_EDIT_OPEN, u"Open ...", u"open the selected element",
                                     wx.ITEM_NORMAL)
        self.editOpen.SetBitmap(wx.ArtProvider.GetBitmap(u"gtk-edit", wx.ART_MENU))
        self.menuEdit.Append(self.editOpen)

        self.editContext = wxx.MenuItem(self.menuEdit, ID_EDIT_CONTEXT,
                                        u"Select contexts ..." + u"\t" + u"Ctrl+Shift+C",
                                        u"edit the  context of selected element", wx.ITEM_NORMAL)
        self.menuEdit.Append(self.editContext)

        self.menu_edit_user_sections = wxx.MenuItem(self.menuEdit, ID_EDIT_USER_SECTIONS,
                                             u"Edit user sections ..." + u"\t" + u"Ctrl+Shift+U",
                                             u"edit the user sections", wx.ITEM_NORMAL)
        self.menuEdit.Append(self.menu_edit_user_sections)

        self.editProperties = wxx.MenuItem(self.menuEdit, ID_EDIT_PROPERTIES, u"Properties ..." + u"\t" + u"Ctrl+Enter",
                                           u"edit the properties of selected element", wx.ITEM_NORMAL)
        self.editProperties.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_INFORMATION, wx.ART_MENU))
        self.menuEdit.Append(self.editProperties)

        self.menuEdit.AppendSeparator()

        self.preferences = wxx.MenuItem(self.menuEdit, ID_PREFERENCES, u"Preferences",
                                        u"edit the application preferences", wx.ITEM_NORMAL)
        self.preferences.SetBitmap(wx.ArtProvider.GetBitmap(u"gtk-preferences", wx.ART_TOOLBAR))
        self.menuEdit.Append(self.preferences)

        self.m_menubar1.Append(self.menuEdit, u"&Edit")

        self.menuSearch = wxx.Menu()
        self.find = wxx.MenuItem(self.menuSearch, wx.ID_ANY, u"&Find ..." + u"\t" + u"Ctrl+F", u"find any occurrence ",
                                 wx.ITEM_NORMAL)
        self.menuSearch.Append(self.find)

        self.findInFiles = wxx.MenuItem(
            self.menuSearch, ID_FIND_IN_FILES, u"Find in files ...\tCtrl+Shift+F",
            u"find any occurrence in files", wx.ITEM_NORMAL)
        self.menuSearch.Append(self.findInFiles)

        self.next = wxx.MenuItem(self.menuSearch, wx.ID_ANY, u"Next" + u"\t" + u"F3", wx.EmptyString, wx.ITEM_NORMAL)
        self.menuSearch.Append(self.next)

        self.previous = wxx.MenuItem(self.menuSearch,
                                     wx.ID_ANY, u"previous\tShift+F3", wx.EmptyString, wx.ITEM_NORMAL)
        self.menuSearch.Append(self.previous)

        self.m_menubar1.Append(self.menuSearch, u"&Search")

        self.menuView = wxx.Menu()
        self.menuView.AppendSeparator()

        self.m_menuItem72 = wxx.MenuItem(self.menuView, wx.ID_ANY, u"Reset perspective", wx.EmptyString, wx.ITEM_NORMAL)
        self.menuView.Append(self.m_menuItem72)

        self.m_view_showToolbars = wxx.MenuItem(self.menuView, wx.ID_ANY, u"show toolbars",
                                                u"Show all the current view toolbars", wx.ITEM_NORMAL)
        self.menuView.Append(self.m_view_showToolbars)

        self.m_views = wxx.MenuItem(self.menuView, wx.ID_ANY, u"Views", wx.EmptyString, wx.ITEM_CHECK)
        self.menuView.Append(self.m_views)
        self.m_views.Check(True)

        self.m_auxiliaryPanes = wxx.MenuItem(self.menuView, wx.ID_ANY, u"Auxiliary panes", wx.EmptyString,
                                             wx.ITEM_CHECK)
        self.menuView.AppendItem(self.m_auxiliaryPanes)
        self.m_auxiliaryPanes.Check(True)

        self.m_menubar1.Append(self.menuView, u"&View")

        self.menuTools = wxx.Menu()
        self.m_menubar1.Append(self.menuTools, u"&Tools")

        self.menuHelp = wxx.Menu()
        self.m_menubar1.Append(self.menuHelp, u"&Help")

        # self.SetMenuBar(self.m_menubar1)
        self.m_mgr.AddPane(
            self.m_menubar1,
            aui.AuiPaneInfo().Name(u"flat_menu_bar").ToolbarPane().Top().DockFixed(True).Dockable(False).
                MinSize(400, -1).Movable(False).PaneBorder(True).Gripper(False).Row(0).Layer(50))

        self.m_auiToolBarFile = aui.AuiToolBar(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(16, 16),
                                               aui.AUI_TB_GRIPPER | aui.AUI_TB_HORZ_LAYOUT | aui.AUI_TB_PLAIN_BACKGROUND)
        self.m_new_workspace = self.m_auiToolBarFile.AddTool(ID_NEW_WORKSPACE_AUI, u"new workspace",
                                                             wx.Bitmap(local_path('app/res/workspace.xpm'),
                                                                       wx.BITMAP_TYPE_ANY), wx.NullBitmap,
                                                             wx.ITEM_NORMAL, u"new workspace", wx.EmptyString, None)

        self.m_new = self.m_auiToolBarFile.AddTool(ID_NEW_PROJECT_AUI, u"new project",
                                                   wx.Bitmap(local_path('app/res/new_project.xpm'), wx.BITMAP_TYPE_ANY), wx.NullBitmap,
                                                   wx.ITEM_NORMAL, u"new project", wx.EmptyString, None)

        self.m_open = self.m_auiToolBarFile.AddTool(ID_OPEN_PROJECT_AUI, u"open project",
                                                    wx.Bitmap(local_path('app/res/open_project.xpm'), wx.BITMAP_TYPE_ANY),
                                                    wx.NullBitmap, wx.ITEM_NORMAL, u"open project", wx.EmptyString,
                                                    None)

        save_bmp = wx.Bitmap(local_path("app/res/save.xpm"), wx.BITMAP_TYPE_ANY)
        save_disabled_bmp = wx.Bitmap(local_path("app/res/save_disabled.xpm"), wx.BITMAP_TYPE_ANY)
        self.m_save_project = self.m_auiToolBarFile.AddTool(ID_SAVE_PROJECT, u"save project", save_bmp,
                                                            save_disabled_bmp, wx.ITEM_NORMAL, u"save current",
                                                            wx.EmptyString, None)

        self.m_save_workspace = self.m_auiToolBarFile.AddTool(ID_SAVE_WORKSPACE, u"tool",
                                                              wx.Bitmap(local_path('app/res/save_all.xpm'),
                                                                        wx.BITMAP_TYPE_ANY), wx.NullBitmap,
                                                              wx.ITEM_NORMAL, u"save workspace", wx.EmptyString, None)

        self.m_auiToolBarFile.Realize()
        self.m_mgr.AddPane(self.m_auiToolBarFile, aui.AuiPaneInfo().Name(u"file_toolbar").Top().Caption(u"file").
                           PinButton(True).Gripper().Dock().Resizable().FloatingSize(wx.Size(-1, -1)).
                           Row(0).Position(0).Layer(10).PaneBorder(False).ToolbarPane())

        self.m_auiToolBarEdit = aui.AuiToolBar(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(16, 16),
                                               style=aui.AUI_TB_GRIPPER | aui.AUI_TB_HORZ_LAYOUT | aui.AUI_TB_PLAIN_BACKGROUND)
        self.m_undo = self.m_auiToolBarEdit.AddTool(ID_UNDO, u"tool",
                                                    wx.ArtProvider.GetBitmap(wx.ART_UNDO, wx.ART_TOOLBAR),
                                                    wx.NullBitmap, wx.ITEM_NORMAL, u"undo last operation",
                                                    wx.EmptyString, None)

        self.m_redo = self.m_auiToolBarEdit.AddTool(ID_REDO, u"tool",
                                                    wx.ArtProvider.GetBitmap(wx.ART_REDO, wx.ART_TOOLBAR),
                                                    wx.NullBitmap, wx.ITEM_NORMAL, u"redo last operation",
                                                    wx.EmptyString, None)

        self.m_auiToolBarEdit.AddSeparator()

        self.m_copy = self.m_auiToolBarEdit.AddTool(ID_COPY, u"copy",
                                                    wx.ArtProvider.GetBitmap(wx.ART_COPY, wx.ART_TOOLBAR),
                                                    wx.NullBitmap, wx.ITEM_NORMAL, u"copy selected", wx.EmptyString,
                                                    None)

        self.m_cut = self.m_auiToolBarEdit.AddTool(ID_CUT, u"cut", wx.ArtProvider.GetBitmap(wx.ART_CUT, wx.ART_TOOLBAR),
                                                   wx.NullBitmap, wx.ITEM_NORMAL, u"cut selected", wx.EmptyString, None)

        self.m_paste = self.m_auiToolBarEdit.AddTool(ID_PASTE, u"paste",
                                                     wx.ArtProvider.GetBitmap(wx.ART_PASTE, wx.ART_TOOLBAR),
                                                     wx.NullBitmap, wx.ITEM_NORMAL, u"paste from clipboard",
                                                     wx.EmptyString, None)

        self.m_auiToolBarEdit.AddSeparator()

        self.m_delete = self.m_auiToolBarEdit.AddTool(ID_DELETE, u"delete",
                                                      wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_TOOLBAR),
                                                      wx.NullBitmap, wx.ITEM_NORMAL, u"delete selected", wx.EmptyString,
                                                      None)

        self.m_auiToolBarEdit.Realize()
        self.m_mgr.AddPane(self.m_auiToolBarEdit, aui.AuiPaneInfo().Name(u"edit_toolbar").Top().Caption(u"edit").
                           PinButton(True).Gripper().Dock().Resizable().PaneBorder(False).FloatingSize(wx.Size(-1, -1)).
                           Row(0).Position(1).Layer(10).ToolbarPane())

        self.m_statusBar1 = self.CreateStatusBar(1, wx.STB_SIZEGRIP, wx.ID_ANY)
        self.viewBook = wxx.AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(300, -1),
                                        aui.AUI_NB_TOP | aui.AUI_NB_SCROLL_BUTTONS | aui.AUI_NB_TAB_MOVE | aui.AUI_NB_TOP | aui.AUI_NB_WINDOWLIST_BUTTON | wx.NO_BORDER)
        self.viewBook.SetMinSize(wx.Size(300, -1))

        self.m_mgr.AddPane(self.viewBook, aui.AuiPaneInfo().Name(u"views").Left().Caption(u"views").
                           CaptionVisible(False).MaximizeButton(True).MinimizeButton(True).PinButton(True).
                           PaneBorder(False).Dock().Resizable().FloatingSize(wx.Size(120, 300)).
                           Row(0).Position(0).Layer(4))

        self.docBook = wxx.AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1),
                                       aui.AUI_NB_CLOSE_ON_ACTIVE_TAB | aui.AUI_NB_SCROLL_BUTTONS | aui.AUI_NB_TAB_EXTERNAL_MOVE | aui.AUI_NB_TAB_MOVE | aui.AUI_NB_TAB_SPLIT | aui.AUI_NB_TOP | aui.AUI_NB_WINDOWLIST_BUTTON | wx.FULL_REPAINT_ON_RESIZE | wx.NO_BORDER)
        self.m_mgr.AddPane(self.docBook, aui.AuiPaneInfo().Name(u"editors").Center().Caption(u"editors").
                           CaptionVisible(False).CloseButton(False).MaximizeButton(True).PinButton(True).
                           PaneBorder(False).Dock().Resizable().FloatingSize(wx.Size(120, 300)).DockFixed(True).
                           Row(0).Layer(0))

        self.m_aux_panes = wx.Notebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NB_TOP | wx.NO_BORDER)
        self.m_mgr.AddPane(self.m_aux_panes, aui.AuiPaneInfo().Name(u"auxiliary").Bottom().Caption(u"auxiliary panes").
                           CaptionVisible(True).MaximizeButton(True).MinimizeButton(True).PinButton(True).
                           PaneBorder(False).Dock().Resizable().FloatingSize(wx.Size(200, 150)).
                           Row(1).MinSize(wx.Size(200, 150)).Layer(2))

        self.m_panel37 = wx.Panel(self.m_aux_panes, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer159 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer159.AddGrowableCol(0)
        fgSizer159.AddGrowableRow(0)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        from beatle.app.ui.ctrl import PythonTextEntry
        self.m_pythonEntry = PythonTextEntry(self.m_panel37)
        fgSizer159.Add(self.m_pythonEntry, 1, wx.ALL | wx.EXPAND, 5)

        self.m_panel37.SetSizer(fgSizer159)
        self.m_panel37.Layout()
        fgSizer159.Fit(self.m_panel37)
        self.m_aux_panes.AddPage(self.m_panel37, u"console", True)
        self.m_panel_log = wx.Panel(self.m_aux_panes, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer243 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer243.AddGrowableCol(0)
        fgSizer243.AddGrowableRow(0)
        fgSizer243.SetFlexibleDirection(wx.BOTH)
        fgSizer243.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_toolLog = wx.TextCtrl(self.m_panel_log, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                     wx.TE_MULTILINE | wx.TE_READONLY)
        fgSizer243.Add(self.m_toolLog, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel_log.SetSizer(fgSizer243)
        self.m_panel_log.Layout()
        fgSizer243.Fit(self.m_panel_log)
        self.m_aux_panes.AddPage(self.m_panel_log, u"log", False)
        self.m_aux_output_pane = wx.Panel(self.m_aux_panes, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                          wx.TAB_TRAVERSAL)
        fgSizer2431 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer2431.AddGrowableCol(0)
        fgSizer2431.AddGrowableRow(0)
        fgSizer2431.SetFlexibleDirection(wx.BOTH)
        fgSizer2431.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.auxOutput = wx.richtext.RichTextCtrl(self.m_aux_output_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                  wx.DefaultSize,
                                                  wx.TE_READONLY | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)
        self.auxOutput.SetForegroundColour(fore)
        self.auxOutput.SetBackgroundColour(back)
        style = wx.richtext.RichTextAttr()
        style.SetTextColour(fore)
        self.auxOutput.SetBasicStyle(style)
        self.auxOutput.SetMargins(left=10, top=10)
        fgSizer2431.Add(self.auxOutput, 1, wx.EXPAND | wx.ALL, 5)

        self.m_aux_output_pane.SetSizer(fgSizer2431)
        self.m_aux_output_pane.Layout()
        fgSizer2431.Fit(self.m_aux_output_pane)
        self.m_aux_panes.AddPage(self.m_aux_output_pane, u"output", False)
        self.m_searchPane = wx.Panel(self.m_aux_panes, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_searchPane.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))

        fgSizer53 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer53.AddGrowableCol(0)
        fgSizer53.AddGrowableRow(0)
        fgSizer53.SetFlexibleDirection(wx.BOTH)
        fgSizer53.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_search_results = CustomTreeCtrl(self.m_searchPane,
                                               style=wx.TR_DEFAULT_STYLE | wx.TR_HAS_VARIABLE_ROW_HEIGHT)
        il = wx.ImageList(16, 16)
        self.fldridx = il.Add(wx.ArtProvider.GetBitmap(wx.ART_FOLDER, wx.ART_OTHER, (16, 16)))
        self.fldropenidx = il.Add(wx.ArtProvider.GetBitmap(wx.ART_FILE_OPEN, wx.ART_OTHER, (16, 16)))
        self.fileidx = il.Add(wx.ArtProvider.GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, (16, 16)))

        self.m_search_results.SetImageList(il)

        fgSizer53.Add(self.m_search_results, 0, wx.ALL | wx.EXPAND, 5)

        self.m_searchPane.SetSizer(fgSizer53)
        self.m_searchPane.Layout()
        fgSizer53.Fit(self.m_searchPane)
        self.m_aux_panes.AddPage(self.m_searchPane, u"search", False)

        self.m_mgr.Update()
        self.Centre(wx.BOTH)

        # Connect Events
        self.Bind(wx.EVT_CLOSE, self.on_exit)
        self.Bind(wx.EVT_MENU, self.on_new_workspace, id=self.newWorkspace.GetId())
        self.Bind(wx.EVT_MENU, self.on_new_workspace_aui, id=ID_NEW_WORKSPACE_AUI)
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.newProject.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.newProject.GetId())
        self.Bind(wx.EVT_MENU, self.on_open_workspace, id=self.openWorkspace.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.openProject.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.openProject.GetId())
        self.Bind(wx.EVT_MENU, self.on_close_workspace, id=self.m_menuItem26.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_close_workspace, id=self.m_menuItem26.GetId())
        self.Bind(wx.EVT_MENU, self.on_close_project, id=self.m_menuItem27.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_close_project, id=self.m_menuItem27.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.importProject.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.importProject.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.saveWorkspace.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.saveWorkspace.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.save_project.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.save_project.GetId())
        self.Bind(wx.EVT_MENU, self.on_exit, id=self.quit.GetId())
        self.Bind(wx.EVT_MENU, self.on_edit_undo, id=self.undo.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_edit_undo, id=self.undo.GetId())
        self.Bind(wx.EVT_MENU, self.on_edit_redo, id=self.redo.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_edit_redo, id=self.redo.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.copy.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.copy.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.cut.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.cut.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.paste.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.paste.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.delete.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.delete.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.editOpen.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.editOpen.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.editContext.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.editContext.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.menu_edit_user_sections.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.menu_edit_user_sections.GetId())
        self.Bind(wx.EVT_MENU, self.translate_on_event, id=self.editProperties.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.translate_on_update_event, id=self.editProperties.GetId())
        self.Bind(wx.EVT_MENU, self.on_edit_preferences, id=self.preferences.GetId())
        self.Bind(wx.EVT_MENU, self.on_find_in_files, id=self.findInFiles.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_find_in_files, id=self.findInFiles.GetId())
        self.Bind(wx.EVT_MENU, self.on_reset_perspective, id=self.m_menuItem72.GetId())
        self.Bind(wx.EVT_MENU, self.on_toggle_views_panes, id=self.m_views.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_toggle_views_panes, id=self.m_views.GetId())
        self.Bind(wx.EVT_MENU, self.on_toggle_auxiliary_panes, id=self.m_auxiliaryPanes.GetId())
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_toggle_auxiliary_panes, id=self.m_auxiliaryPanes.GetId())
        self.viewBook.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.on_view_page_changed)
        self.viewBook.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGING, self.on_view_page_changing)
        self.viewBook.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.on_view_page_closed)
        self.docBook.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.on_page_changed)
        self.docBook.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGING, self.on_page_changing)
        self.docBook.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.on_close_pane)

    def __del__(self):
        self.m_mgr.UnInit()

    # Virtual event handlers, overide them in your derived class
    def on_exit(self, event):
        event.Skip()

    def on_new_workspace(self, event):
        event.Skip()

    def on_new_workspace_aui(self, event):
        event.Skip()

    def translate_on_event(self, event):
        event.Skip()

    def translate_on_update_event(self, event):
        event.Skip()

    def on_open_workspace(self, event):
        event.Skip()

    def on_close_workspace(self, event):
        event.Skip()

    def on_update_close_workspace(self, event):
        event.Skip()

    def on_close_project(self, event):
        event.Skip()

    def on_update_close_project(self, event):
        event.Skip()

    def on_edit_undo(self, event):
        event.Skip()

    def on_update_edit_undo(self, event):
        event.Skip()

    def on_edit_redo(self, event):
        event.Skip()

    def on_update_edit_redo(self, event):
        event.Skip()

    def on_edit_preferences(self, event):
        event.Skip()

    def on_find_in_files(self, event):
        event.Skip()

    def on_update_find_in_files(self, event):
        event.Skip()

    def on_reset_perspective(self, event):
        event.Skip()

    def on_toggle_views_panes(self, event):
        event.Skip()

    def on_update_toggle_views_panes(self, event):
        event.Skip()

    def on_toggle_auxiliary_panes(self, event):
        event.Skip()

    def on_update_toggle_auxiliary_panes(self, event):
        event.Skip()

    def on_view_page_changed(self, event):
        event.Skip()

    def on_view_page_changing(self, event):
        event.Skip()

    def on_view_page_closed(self, event):
        event.Skip()

    def on_page_changed(self, event):
        event.Skip()

    def on_page_changing(self, event):
        event.Skip()

    def on_close_pane(self, event):
        event.Skip()


###########################################################################
## Class FindTextDialogBase
###########################################################################

class FindTextDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Find ", pos=wx.DefaultPosition, size=wx.Size(331, 118),
                           style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer68 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer68.AddGrowableCol(0)
        fgSizer68.AddGrowableRow(0)
        fgSizer68.AddGrowableRow(2)
        fgSizer68.SetFlexibleDirection(wx.BOTH)
        fgSizer68.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer68.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer70 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer70.AddGrowableCol(1)
        fgSizer70.AddGrowableRow(0)
        fgSizer70.SetFlexibleDirection(wx.BOTH)
        fgSizer70.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText34 = wx.StaticText(self, wx.ID_ANY, u"search:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText34.Wrap(-1)
        fgSizer70.Add(self.m_staticText34, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_search_string = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TE_PROCESS_ENTER)
        fgSizer70.Add(self.m_search_string, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer68.Add(fgSizer70, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)

        fgSizer68.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)
        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer68.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer68)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_search_string.Bind(wx.EVT_TEXT_ENTER, self.on_ok)
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_ok(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()


###########################################################################
## Class FindInFilesDialogBase
###########################################################################

class FindInFilesDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY,
                           title=u"Find in files", pos=wx.DefaultPosition,
                           size=wx.Size(436, 358), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer68 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer68.AddGrowableCol(0)
        fgSizer68.AddGrowableRow(1)
        fgSizer68.SetFlexibleDirection(wx.BOTH)
        fgSizer68.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer70 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer70.AddGrowableCol(1)
        fgSizer70.AddGrowableRow(0)
        fgSizer70.SetFlexibleDirection(wx.BOTH)
        fgSizer70.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText34 = wx.StaticText(self, wx.ID_ANY, u"search:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText34.Wrap(-1)
        fgSizer70.Add(self.m_staticText34, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_search_string = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TE_PROCESS_ENTER)
        fgSizer70.Add(self.m_search_string, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer68.Add(fgSizer70, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 5)

        fgSizer61 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer61.AddGrowableCol(0)
        fgSizer61.AddGrowableCol(1)
        fgSizer61.AddGrowableRow(0)
        fgSizer61.SetFlexibleDirection(wx.BOTH)
        fgSizer61.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        sbSizer6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"options"), wx.VERTICAL)

        self.m_checkBox4 = wx.CheckBox(sbSizer6.GetStaticBox(), wx.ID_ANY, u"&match case", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        sbSizer6.Add(self.m_checkBox4, 0, wx.ALL, 5)

        self.m_checkBox5 = wx.CheckBox(sbSizer6.GetStaticBox(), wx.ID_ANY, u"regular expression", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        sbSizer6.Add(self.m_checkBox5, 0, wx.ALL, 5)

        fgSizer61.Add(sbSizer6, 1, wx.EXPAND, 5)

        sbSizer7 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"scope"), wx.VERTICAL)

        self.m_radioBtn1 = wx.RadioButton(sbSizer7.GetStaticBox(), wx.ID_ANY, u"open files", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        sbSizer7.Add(self.m_radioBtn1, 0, wx.ALL, 5)

        self.m_radioBtn2 = wx.RadioButton(sbSizer7.GetStaticBox(), wx.ID_ANY, u"project files", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        sbSizer7.Add(self.m_radioBtn2, 0, wx.ALL, 5)

        self.m_radioBtn3 = wx.RadioButton(sbSizer7.GetStaticBox(), wx.ID_ANY, u"workspace files", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        sbSizer7.Add(self.m_radioBtn3, 0, wx.ALL, 5)

        fgSizer61.Add(sbSizer7, 1, wx.EXPAND, 5)

        fgSizer68.Add(fgSizer61, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)
        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer68.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer68)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_search_string.Bind(wx.EVT_TEXT_ENTER, self.on_ok)
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, override them in your derived class
    def on_ok(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()


###########################################################################
## Class WorkspaceDialogBase
###########################################################################

class WorkspaceDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"New workspace", pos=wx.DefaultPosition,
                           size=wx.Size(429, 401), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        fgSizer172 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer172.AddGrowableCol(0)
        fgSizer172.AddGrowableRow(1)
        fgSizer172.SetFlexibleDirection(wx.BOTH)
        fgSizer172.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        fgSizer5 = wx.FlexGridSizer(5, 2, 0, 0)
        fgSizer5.AddGrowableCol(1)
        fgSizer5.SetFlexibleDirection(wx.BOTH)
        fgSizer5.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY, u"Name:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        fgSizer5.Add(self.m_staticText2, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_textCtrl1 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer5.Add(self.m_textCtrl1, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText3 = wx.StaticText(self, wx.ID_ANY, u"Base directory:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        fgSizer5.Add(self.m_staticText3, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer29 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer29.AddGrowableCol(0)
        fgSizer29.SetFlexibleDirection(wx.BOTH)
        fgSizer29.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_textCtrl9 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer29.Add(self.m_textCtrl9, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_bpButton3 = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_FOLDER_OPEN, wx.ART_BUTTON),
                                           wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        fgSizer29.Add(self.m_bpButton3, 0, wx.ALL, 5)

        fgSizer5.Add(fgSizer29, 1, wx.EXPAND, 5)

        fgSizer172.Add(fgSizer5, 1, wx.EXPAND, 5)

        sbSizer9 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Documentation"), wx.VERTICAL)

        self.m_richText3 = wx.richtext.RichTextCtrl(sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    0 | wx.HSCROLL | wx.SUNKEN_BORDER | wx.VSCROLL | wx.WANTS_CHARS)
        sbSizer9.Add(self.m_richText3, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer172.Add(sbSizer9, 1, wx.EXPAND, 5)

        fgSizer22 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer22.AddGrowableCol(0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer22.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button5 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer22.Add(self.m_button5, 0, wx.ALL, 5)

        self.m_button6 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button6.SetDefault()
        fgSizer22.Add(self.m_button6, 0, wx.ALL, 5)

        fgSizer172.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer172)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_textCtrl1.Bind(wx.EVT_TEXT, self.on_change_worspace_name)
        self.m_bpButton3.Bind(wx.EVT_BUTTON, self.on_choose_dir)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_ok)
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_cancel)


    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_change_worspace_name(self, event):
        event.Skip()

    def on_choose_dir(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()


###########################################################################
## Class WaitDialogBase
###########################################################################

class WaitDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Please wait while processing files",
                           pos=wx.DefaultPosition, size=wx.Size(553, 186),
                           style=wx.CAPTION | wx.RESIZE_BORDER | wx.STAY_ON_TOP)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle(self.GetExtraStyle() | wx.WS_EX_PROCESS_IDLE | wx.WS_EX_PROCESS_UI_UPDATES)

        fgSizer224 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer224.AddGrowableCol(0)
        fgSizer224.AddGrowableRow(0)
        fgSizer224.AddGrowableRow(2)
        fgSizer224.SetFlexibleDirection(wx.BOTH)
        fgSizer224.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer224.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer225 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer225.AddGrowableCol(1)
        fgSizer225.SetFlexibleDirection(wx.BOTH)
        fgSizer225.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer225.Add((10, 0), 1, wx.EXPAND, 5)

        self.m_text = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                  wx.TE_READONLY | wx.NO_BORDER)
        self.m_text.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME))

        fgSizer225.Add(self.m_text, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 5)

        fgSizer225.Add((10, 0), 1, wx.EXPAND, 5)

        fgSizer224.Add(fgSizer225, 1, wx.EXPAND, 5)

        fgSizer224.Add((0, 0), 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer224)
        self.Layout()

        self.Centre(wx.BOTH)

    def __del__(self):
        pass


###########################################################################
## Class WorkingWindowBase
###########################################################################

class WorkingWindowBase(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(236, 171), style=wx.FRAME_FLOAT_ON_PARENT | wx.FRAME_NO_TASKBAR)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer242 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer242.AddGrowableCol(0)
        fgSizer242.AddGrowableRow(0)
        fgSizer242.AddGrowableRow(2)
        fgSizer242.SetFlexibleDirection(wx.BOTH)
        fgSizer242.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_animCtrl = wx.adv.AnimationCtrl(self, wx.ID_ANY, wx.adv.NullAnimation, wx.DefaultPosition,
                                               wx.DefaultSize, wx.adv.AC_DEFAULT_STYLE | wx.NO_BORDER)

        self.m_animCtrl.SetInactiveBitmap(wx.NullBitmap)
        fgSizer242.Add(self.m_animCtrl, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_staticText112 = wx.StaticText(self, wx.ID_ANY, u"Please wait ...", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText112.Wrap(-1)
        fgSizer242.Add(self.m_staticText112, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer242.Add((0, 0), 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer242)
        self.Layout()

        self.Centre(wx.BOTH)

    def __del__(self):
        pass


###########################################################################
## Class ImportProjectDialogBase
###########################################################################

class ImportProjectDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Import project", pos=wx.DefaultPosition,
                           size=wx.Size(429, 339), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer178 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer178.AddGrowableCol(0)
        fgSizer178.AddGrowableRow(1)
        fgSizer178.SetFlexibleDirection(wx.BOTH)
        fgSizer178.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer233 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer233.AddGrowableCol(1)
        fgSizer233.SetFlexibleDirection(wx.BOTH)
        fgSizer233.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText110 = wx.StaticText(self, wx.ID_ANY, u"base directory", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText110.Wrap(-1)
        fgSizer233.Add(self.m_staticText110, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.m_dirPicker2 = wx.DirPickerCtrl(self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition,
                                             wx.DefaultSize,
                                             wx.DIRP_DEFAULT_STYLE | wx.DIRP_DIR_MUST_EXIST | wx.DIRP_USE_TEXTCTRL)
        fgSizer233.Add(self.m_dirPicker2, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer178.Add(fgSizer233, 1, wx.EXPAND, 5)

        self.m_choicebook4 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT)
        self.m_panel29 = wx.Panel(self.m_choicebook4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer183 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer183.AddGrowableCol(0)
        fgSizer183.AddGrowableRow(0)
        fgSizer183.SetFlexibleDirection(wx.BOTH)
        fgSizer183.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        m_checkList6Choices = []
        self.m_checkList6 = wx.CheckListBox(self.m_panel29, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            m_checkList6Choices, 0)
        fgSizer183.Add(self.m_checkList6, 0, wx.ALL | wx.EXPAND, 5)

        self.m_panel29.SetSizer(fgSizer183)
        self.m_panel29.Layout()
        fgSizer183.Fit(self.m_panel29)
        self.m_choicebook4.AddPage(self.m_panel29, u"python", False)
        self.m_panel28 = wx.Panel(self.m_choicebook4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer182 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer182.SetFlexibleDirection(wx.BOTH)
        fgSizer182.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_panel28.SetSizer(fgSizer182)
        self.m_panel28.Layout()
        fgSizer182.Fit(self.m_panel28)
        self.m_choicebook4.AddPage(self.m_panel28, u"c++", False)
        fgSizer178.Add(self.m_choicebook4, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(1)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        m_sdbSizer12 = wx.StdDialogButtonSizer()
        self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer12.AddButton(self.m_sdbSizer12OK)
        self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel)
        m_sdbSizer12.Realize();

        fgSizer159.Add(m_sdbSizer12, 1, wx.EXPAND, 5)

        fgSizer178.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer178)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_sdbSizer12Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer12OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class UnsavedQuitDialogBase
###########################################################################

class UnsavedQuitDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Unsaved changes", pos=wx.DefaultPosition,
                           size=wx.Size(600, 335), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer71 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer71.AddGrowableCol(0)
        fgSizer71.AddGrowableRow(1)
        fgSizer71.SetFlexibleDirection(wx.BOTH)
        fgSizer71.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText36 = wx.StaticText(self, wx.ID_ANY,
                                            u"Dou you want to discard unsaved changes? The following workspaces or projects have unsaved modifications:",
                                            wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText36.Wrap(500)
        fgSizer71.Add(self.m_staticText36, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        m_unsaved_projects_listChoices = []
        self.m_unsaved_projects_list = wx.ListBox(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                  m_unsaved_projects_listChoices, 0)
        fgSizer71.Add(self.m_unsaved_projects_list, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(0)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        fgSizer75 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer75.SetFlexibleDirection(wx.BOTH)
        fgSizer75.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_button9 = wx.Button(self, ID_DISCARD, u"Discard changes", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer75.Add(self.m_button9, 0, wx.ALL, 5)

        self.m_button10 = wx.Button(self, wx.ID_CANCEL, u"Cancel Quit", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer75.Add(self.m_button10, 0, wx.ALL, 5)

        self.m_button8 = wx.Button(self, wx.ID_OK, u"Save changes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button8.SetDefault()
        fgSizer75.Add(self.m_button8, 0, wx.ALL, 5)

        fgSizer159.Add(fgSizer75, 1, wx.EXPAND, 5)

        fgSizer71.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer71)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button9.Bind(wx.EVT_BUTTON, self.OnDiscard)
        self.m_button8.Bind(wx.EVT_BUTTON, self.OnSaveChanges)
        self.m_button10.Bind(wx.EVT_BUTTON, self.OnCancel)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnDiscard(self, event):
        event.Skip()

    def OnSaveChanges(self, event):
        event.Skip()

    def OnCancel(self, event):
        event.Skip()


###########################################################################
## Class FileAlreadyExistsDialogBase
###########################################################################

class FileAlreadyExistsDialogBase(wxx.Dialog):

    def __init__(self, parent):
        wxx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"File already exists", pos=wx.DefaultPosition,
                           size=wx.Size(469, 193), style=wx.CAPTION | wx.STAY_ON_TOP)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer74 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer74.AddGrowableCol(0)
        fgSizer74.AddGrowableRow(0)
        fgSizer74.SetFlexibleDirection(wx.BOTH)
        fgSizer74.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText = wx.StaticText(self, wx.ID_ANY, u"The file {} already exists, Do you want to overwrite it?",
                                          wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText.Wrap(280)
        fgSizer74.Add(self.m_staticText, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer159 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer159.AddGrowableCol(0)
        fgSizer159.SetFlexibleDirection(wx.BOTH)
        fgSizer159.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer159.Add(self.m_info, 0, wx.ALL, 5)

        fgSizer78 = wx.FlexGridSizer(1, 4, 0, 0)
        fgSizer78.SetFlexibleDirection(wx.BOTH)
        fgSizer78.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_button11 = wx.Button(self, wx.ID_YES, u"&Yes", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer78.Add(self.m_button11, 0, wx.ALL, 5)

        self.m_button12 = wx.Button(self, ID_YESALL, u"Yes to &all", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer78.Add(self.m_button12, 0, wx.ALL, 5)

        self.m_button14 = wx.Button(self, wx.ID_CANCEL, u"&Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer78.Add(self.m_button14, 0, wx.ALL, 5)

        self.m_button13 = wx.Button(self, wx.ID_NO, u"&No", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button13.SetDefault()
        fgSizer78.Add(self.m_button13, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        fgSizer159.Add(fgSizer78, 0, 0, 5)

        fgSizer74.Add(fgSizer159, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer74)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button11.Bind(wx.EVT_BUTTON, self.OnYes)
        self.m_button12.Bind(wx.EVT_BUTTON, self.OnYesAll)
        self.m_button14.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button13.Bind(wx.EVT_BUTTON, self.OnNo)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def OnYes(self, event):
        event.Skip()

    def OnYesAll(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def OnNo(self, event):
        event.Skip()
