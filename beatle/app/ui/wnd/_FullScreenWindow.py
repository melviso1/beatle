
import wx

from .._base import FullScreenWindowBase
from beatle.lib.api import context


class FullScreenWindow(FullScreenWindowBase):
    """Implements fullscreen window"""

    def __init__(self, pane):
        """Initialize pane"""
        super(FullScreenWindow, self).__init__(None)
        self._frame = context.get_frame()
        self._pane = pane
        self._index = pane._notebook.GetPageIndex(pane)
        pane._notebook.RemovePage(self._index)
        sizer = wx.FlexGridSizer(1, 1, 0, 0)
        sizer.AddGrowableCol(0)
        sizer.AddGrowableRow(0)
        sizer.SetFlexibleDirection(wx.BOTH)
        sizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        pane.Reparent(self)
        pane.Show()
        sizer.Add(pane, 0, wx.EXPAND | wx.ALL, 0)
        self.SetSizer(sizer)
        self.Layout()
        self._frame.Hide()
        self.Show()
        self.ShowFullScreen(True)
        wx.GetApp().SetTopWindow(self)

    # Handlers for FullScreen events.
    def leave(self):
        """Toggle fullscreen"""
        self._pane.Reparent(None)
        obj = self._pane._object
        text = obj.tab_label
        self._pane._notebook.InsertPage(self._index, self._pane, text, True, obj.bitmap_index)
        self._frame.Show()
        wx.GetApp().SetTopWindow(context.get_frame())
