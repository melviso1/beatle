
import sys, os


from .._base import WorkingWindowBase

class WorkingWindow(WorkingWindowBase):
    """Window for showing wait"""

    def __init__(self, parent):
        """Initialize window"""
        super(WorkingWindow, self).__init__(parent)
        path = os.path.split(sys.argv[0])[0]
        self.m_animCtrl.LoadFile(os.path.join(path, "app/res/wait.gif"))
        self.m_animCtrl.Play()

