# import gettext

import pickle
import os
import signal
import ast

import wx
import beatle

# from .splash import AppSplashScreen
# from beatle.lib.api import context ,logger

# _globals = sys.modules[__name__]

use_splash = True


class BeatleInstance(wx.App):
    """Clase aplicacion"""

    # theApp = None

    def __init__(self, *args, **kwargs):
        """Initialize the application object"""
        self.projects = []
        self.workspaces = []
        self.frame = None
        # self.web_server = None
        self.log = None
        self.splash = None
        self.proxy = beatle.lib.ostools.LocalProxy()
        super(BeatleInstance, self).__init__(*args, **kwargs)

    def OnInit(self):
        """UI initialization"""
        self.frame = beatle.app.ui.MainWindow(self)
        global use_splash
        if use_splash:
            self.splash = beatle.app.ui.AppSplashScreen()
        view = self.frame.get_current_view()
        if view:
            self.frame.m_mgr.LoadPerspective(view._perspective_base)
        wx.CallAfter(self.startup)
        return True

    def OnExit(self):
        try:
            self.proxy.stop()
        except RuntimeError as e:
            pass
        return super(BeatleInstance, self).OnExit()

    def startup(self):

        # self.presLan_en = gettext.translation("BeatleInstance", "./locale",
        #     languages=['en'])
        # self.locale = wx.Locale(wxLANGUAGE_ENGLISH)
        # locale.setlocale(locale.LC_ALL, 'EN')
        # self.presLan_en.install()
        wx.GetApp().SetTopWindow(self.frame)
        stack = beatle.lib.tran.TransactionStack(50)
        self.log = beatle.ctx.logger()
        self.log.SetFile('.beatle3.log')
        wx.Log.EnableLogging()
        stack.SetLogger(self.log)
        beatle.lib.api.context.set_app(self)
        self.SafeYield(self.frame, True)
        self.frame.Show()
        import subprocess
        from os.path import expanduser
        # avoid problems with module imports
        try:
            self.proxy.start()
        except RuntimeError as e:
            pass
        if self.splash:
            self.splash.Close()
        return True

    def exit_even_modified(self):
        """Checks about unsaved documents"""
        chain_workspaces = ""
        modified_workspaces = []
        for workspace in self.workspaces:
            if workspace.modified:
                chain_workspaces += "\n" + workspace._name
                modified_workspaces.append(workspace)

        chain_projects = ""
        modified_projects = []
        for project in self.projects:
            if project.modified:
                if project not in modified_workspaces:
                    chain_projects += "\n" + project._name
                    modified_projects.append(project)

        if len(modified_workspaces) > 0 or len(modified_projects) > 0:
            from beatle.app.ui.dlg import UnsavedQuitDialog
            dlg = UnsavedQuitDialog(self.frame, modified_workspaces + modified_projects)
            option = dlg.ShowModal()
            if option == wx.ID_CANCEL:
                return False
            if option == wx.ID_OK:
                for workspace in modified_workspaces:
                    self.save_workspace(workspace)
                for project in modified_projects:
                    self.save_project(project)
        self.log.Dump()
        try:
            # self.web_server.kill()
            # self.web_server.send_signal(signal.SIGINT)
            pass
        except Exception as e:
            import traceback
            import sys
            traceback.print_exc(file=sys.stdout)
            print(type(e))  # the exception instance
            print(e.args)  # arguments stored in .args
            print(e)

        return True

    def find_project(self, name):
        """search for project by name"""
        try:
            return next(x for x in self.projects if x.name == name)
        except StopIteration:
            return None

    def exist_project(self, name):
        """Check if the project is already loaded"""
        return self.find_project(name) is not None

    def add_project(self, project):
        """Create a new project and store it in the internal project's list."""
        if project not in self.projects:
            self.projects.append(project)
        return project

    def add_workspace(self, workspace):
        """Create a new workspace and store it in the internal project's list"""
        if workspace not in self.workspaces:
            # append a web server to the workspace
            self.workspaces.append(workspace)
        return workspace

    def remove_workspace(self, workspace):
        """Remove a workspace from the internal project list"""
        while workspace in self.workspaces:
            index = self.workspaces.index(workspace)
            del self.workspaces[index]

    def remove_project(self, project):
        """Remove a project from the internal project list"""
        while project in self.projects:
            index = self.projects.index(project)
            del self.projects[index]
        if project.workspace is not None:
            project.workspace.modified = True

    def save_project(self, project):
        """Save a project"""
        if not project in self.projects:
            wx.MessageBox("Project " + project._name + " not exists", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self.frame)
            return
        project_dir = project.dir.replace("//", "/")
        if not os.path.exists(project_dir):
            os.makedirs(project_dir)
            if not os.path.exists(project_dir):
                wx.MessageBox("Failed creating projec directory:\n" +
                              project_dir, "Error",
                              wx.OK | wx.CENTER | wx.ICON_ERROR, self.frame)
                return
        # serialize projects to temporal first in order to prevent
        # errors.
        file_name_tmp = (project_dir + "/" + project._name + ".tmp").replace("//", "/")
        with open(file_name_tmp, "wb") as f:
            pickle.dump(project, f, 2)
        file_name = (project_dir + "/" + project._name + ".pcc").replace("//", "/")
        if os.path.exists(file_name):
            os.remove(file_name)
        os.rename(file_name_tmp, file_name)
        project.modified = False
        if project.workspace is None:
            mru = beatle.lib.api.context.get_mru()
            mru.AddFileToHistory(file_name)

    def save_session(self):
        """Save the current session"""
        wl = self.get_session_workspaces()  # workspace's path
        if not wl:
            return False
        config = beatle.lib.api.context.get_config()
        config.Write('/last_session/workspaces', str(wl))
        tl = beatle.lib.api.context.get_frame().get_views_status()  # get the open views status
        config.Write('/last_session/views', str(tl))
        return True

    @beatle.lib.tran.TransactionalMethod('reload last session')
    def load_session(self):
        """Read last session"""
        config = beatle.lib.api.context.get_config()
        wl = config.Read('/last_session/workspaces', '')
        if not wl:
            return False
        wl = ast.literal_eval(wl)  # restore list
        if not self.set_session_workspaces(wl):
            return False
        tl = config.Read('/last_session/views', '')
        if not tl:
            return True
        tl = ast.literal_eval(tl)
        if tl:
            beatle.lib.api.context.get_frame().set_views_status(tl)
        return True

    def save_workspace(self, workspace_object):
        """Save a workspace (including contained projects)"""
        from beatle.model import Project
        workspace_dir = workspace_object._dir.replace("//", "/")
        if not os.path.exists(workspace_dir):
            try:
                os.makedirs(workspace_dir)
            except OSError:
                wx.MessageBox(
                    "Failed creating workspace directory:\n{dir}".format(dir=workspace_dir), "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, beatle.lib.api.context.get_frame())
                return
            if not os.path.exists(workspace_dir):
                wx.MessageBox(
                    "Failed creating workspace directory:\n{dir}".format(dir=workspace_dir), "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, beatle.lib.api.context.get_frame())
                return
        for project in workspace_object[Project]:
            self.save_project(project)
        file_name = (workspace_dir + "/" + workspace_object.name + ".wrk").replace("//", "/")
        with open(file_name, "wb") as f:
            pickle.dump(workspace_object, f, 2)
        workspace_object.modified = False
        mru = beatle.lib.api.context.get_mru()
        mru.AddFileToHistory(file_name)

    def get_session_workspaces(self):
        """Get the full list of opened workspaces. This is a tentative.
        If some workspace is unsaved, returns False. Otherwise,
        returns the workspaces list."""
        files = []
        for this_workspace in self.workspaces:
            if this_workspace._modified:
                return False
            _dir = this_workspace.dir.replace("//", "/")
            file_name = (_dir + "/" + this_workspace._name + ".wrk").replace("//", "/")
            files.append(file_name)
        return files

    def set_session_workspaces(self, files):
        """Attempts to load a list of workspaces"""
        status = True
        for file_name in files:
            workspace = self.load_workspace(file_name)
            if workspace:
                beatle.lib.tran.TransactionStack.do_load(workspace)
            else:
                status = False
        return status

    def load_project(self, file_name, workspace=None, skip_compare=True):
        """Load a project from file"""
        # reload workspace must avoid waste time checking duplicates
        if not skip_compare:
            for project in self.projects:
                already_file_name = (project.dir + "/" + project.name + ".pcc").replace("//", "/")
                print(("compare " + already_file_name + " with " + file_name + "\n"))
                if already_file_name == file_name:
                    return None
        import traceback
        import sys
        from beatle.model import Project
        try:
            with open(file_name, "rb") as f:
                tmp_dir = os.path.split(file_name)[0]
                project = pickle.load(f, encoding='utf-8')
            if not isinstance(project, Project):
                return None
            if getattr(project, 'dir', None):
                project.dir = tmp_dir
            if workspace:
                workspace.add_child(project)
                workspace.modified = True
                project._parent = workspace
            self.projects.append(project)
            project.modified = False
            return project
        except Exception as inst:
            traceback.print_exc(file=sys.stdout)
            print(type(inst))  # the exception instance
            print(inst.args)  # arguments stored in .args
            print(inst)
            return None

    def load_workspace(self, file_name):
        """Load a workspace"""
        import traceback
        import sys
        from beatle.model import Workspace
        # for this_workspace in self.workspaces:
        #     already_file_name = (this_workspace.dir + "/" + this_workspace.name + ".wrk").replace("//", "/")
        #     print(("compare " + already_file_name + " with " + file_name + "\n"))
        #     if already_file_name == file_name:
        #         return None
        try:
            with open(file_name, "rb") as f:
                Workspace._dir = os.path.split(file_name)[0]
                workspace = pickle.load(f, encoding='utf-8')
            if not isinstance(workspace, Workspace):
                return False
            self.workspaces.append(workspace)
            workspace.modified = False
            mru = beatle.lib.api.context.get_mru()
            mru.AddFileToHistory(file_name)
            return workspace
        except Exception as inst:
            traceback.print_exc(file=sys.stdout)
            print(type(inst))  # the exception instance
            print(inst.args)  # arguments stored in .args
            print(inst)
            return None
