# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import wx

from beatle.lib import wxx
from beatle.lib.api import context
from beatle.lib.handlers import identifier
from ._ToolLogExplorerPane import ToolLogExplorerPane
from beatle.app import resources as rc


class log_explorer(wx.EvtHandler):
    """Class for providing log explorer"""
    instance = None

    def __init__(self):
        """Initialize log explorer"""
        super(log_explorer, self).__init__()
        #create the menus
        self._log_explorer = identifier('ID_LOG_EXPLORER')
        self.append_tools_menu()
        #bind the menu handlers
        self.Bind(wx.EVT_UPDATE_UI, self.OnUpdateLogExplorer, id=self._log_explorer)
        self.Bind(wx.EVT_MENU, self.OnLogExplorer, id=self._log_explorer)
        
    def append_tools_menu(self):
        frame = context.get_frame()
        self._log_explorer_menu = wxx.MenuItem(frame.menuTools, self._log_explorer, u"Log explorer",
            u"show current transaction log", wx.ITEM_NORMAL)
        frame.add_tools_menu('Log explorer', self._log_explorer_menu)

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        pass

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        return log_explorer()

    def OnUpdateLogExplorer(self, event):
        """Handle the update"""
        event.Enable(True)

    def OnLogExplorer(self, event):
        """Handle the command"""
        # create a pane
        frame = context.get_frame()
        p = ToolLogExplorerPane(frame.docBook)
        frame.docBook.AddPage(p, "Log explorer", True, rc.get_bitmap_index("mini_logo"))

