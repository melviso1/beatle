# -*- coding: utf-8 -*-
import os
import wx

from beatle.lib import wxx
from beatle.lib.api import context
from beatle.lib.handlers import identifier


class CheckUpdates(wx.EvtHandler):
    """Class for providing CheckUpdates"""
    instance = None

    def __init__(self):
        """Initialize ast explorer"""
        self._check_update = identifier('CHECK_UPDATES_ID')
        self._check_updates_menu = None
        super(CheckUpdates, self).__init__()
        self.append_tools_menu()
        # bind the menu handlers
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_check_updates, id=self._check_update)
        self.Bind(wx.EVT_MENU, self.on_check_updates, id=self._check_update)
        
    def append_tools_menu(self):
        frame = context.get_frame()
        self._check_updates_menu = wxx.MenuItem(
            frame.menuTools, self._check_update, u"Check updates",
            u"check for existing updates", wx.ITEM_NORMAL)
        frame.add_tools_menu('Check updates', self._check_updates_menu)

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        pass

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        return CheckUpdates()

    def on_update_check_updates(self, event):
        """Handle the update"""
        event.Enable(True)

    def on_check_updates(self, event):
        """Handle the command"""
        self.do_update()

    @staticmethod
    def parse_package_info(raw_string):
        """Parses pkginfo and returns a dictionary"""
        lines = raw_string.splitlines(False)

        def stripper(x):
            return x[0].strip().upper(), x[1].strip()

        return dict([stripper(x.split(':', 1)) for x in lines if ':' in x])

    @staticmethod
    def package_info_version(pkg_dict):
        """Get comparable package info from dict"""
        return [int(ns) for ns in pkg_dict['VERSION'].split('.')]

    def do_update(self, automode=False):
        """Do the real update process"""
        # Check internet connection
        import socket
        try:
            socket.setdefaulttimeout(2)
            socket.gethostbyname("pypi.python.org")
        except Exception as e:
            if not automode:
                wx.MessageBox("Unable to access pypi repository.\nIs internect accesible?", "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
            return
        try:
            from urllib.request import urlopen
            url = urlopen('https://pypi.python.org/pypi?name=beatle&:action=display_pkginfo', timeout=3)  # nosec
            raw = url.read().decode('utf-8')
        except Exception as e:
            if not automode:
                wx.MessageBox("We apologyze, but we can't access to beatle repository.\nPlease check again later.", "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
            return
        online_info = self.parse_package_info(raw)
        if 'VERSION' not in online_info:
            if not automode:
                wx.MessageBox("Missing version in beatle package", "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
            return
        try:
            import subprocess as sp
            raw, err = sp.Popen(['pip', 'show', 'beatle'], stdout=sp.PIPE, stderr=sp.PIPE).communicate()
        except Exception as e:
            if not automode:
                wx.MessageBox("We apologyze, but we can't read beatle pip info.", "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
            return
        local_info = self.parse_package_info(raw)
        if 'VERSION' not in local_info:
            if not automode:
                wx.MessageBox("Missing version in beatle package", "Error",
                    wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
            return
        online_version = self.package_info_version(online_info)
        local_version = self.package_info_version(local_info)
        if local_version >= online_version:
            if not automode:
                wx.MessageBox("Beatle is up to date!", "Info",
                    wx.OK | wx.CENTER | wx.ICON_INFORMATION, context.get_frame())
            return
        else:
            vo = local_info['VERSION']
            vn = online_info['VERSION']
            if len(vo) == 3 and len(vn) == 3:
                if vo[1] != vn[1] or vo[0] != vn[0]:
                    extra_info = '\nnote : THE NEW VERSION IS NOT BACKWARD COMPATIBLE WITH YOUR CURRENT VERSION'
                else:
                    extra_info = ''
            else:
                extra_info = '\nnote : The versions must have three digits. Something wrong happens.'
            option = wx.MessageBox(
                "There are a new version of Beatle.\n"
                "your current version is {vo}\n"
                "the new version is {vn}\n"
                "Do you want to update?{extra_info}".format(vo=vo, vn=vn, extra_info=extra_info), "Info",
                    wx.YES_NO | wx.CENTER | wx.ICON_QUESTION, context.get_frame())
            if option != wx.YES:
                return
        # attempt to do without sudo
        success = True
        if os.system("pip install beatle --upgrade"):
            # Ok, now with sudo
            if os.system("gksudo 'pip install beatle --upgrade'"):
                success = False
        if success:
            wx.MessageBox(
                "Beatle is now up to date! Please restart the\n"
                "application for using the new version.", "Info",
                wx.OK | wx.CENTER | wx.ICON_INFORMATION, context.get_frame())
        else:
            wx.MessageBox("Beatle is not updated.", "Info",
                wx.OK | wx.CENTER | wx.ICON_INFORMATION, context.get_frame())

