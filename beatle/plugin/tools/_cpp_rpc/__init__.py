import wx

from beatle import local_path
from beatle.lib import wxx
from beatle.lib.api import context
from beatle.lib.handlers import identifier
from beatle.lib.tran import TransactionalMethod
from beatle.activity.models.ui.view import ModelsView
from beatle.model import Project
from beatle.model.cc import CCProject, TypesFolder

from .model import RPCClient, RPCServer, RPCClassServer, RPCFunctionServer
from .ui import RPCServerDialog, RPCClientDialog, RPCServerEditor


class RPCWizard(wx.EvtHandler):
    """Class for providing rpclib wizard"""
    instance = None

    def __init__(self):
        """Initialize ast explorer"""
        super(RPCWizard, self).__init__()

        # register custom module classes and classes overrides
        CCProject.module_classes.append(RPCClient)
        CCProject.module_classes.append(RPCServer)
        CCProject.class_overrides.append(RPCClassServer)
        CCProject.class_overrides.append(RPCFunctionServer)

        # register global identifiers
        self._editOpen = identifier("ID_EDIT_OPEN")
        self._rpc_server_new = identifier("ID_RPC_SERVER_NEW")
        self._rpc_client_new = identifier("ID_RPC_CLIENT_NEW")

        self._object = None
        self._rpc_wizard_menu_new_server = None
        self._rpc_wizard_menu_new_client = None
        self._project = None

        self.append_tools_menu()
        self.bind_events()

        ModelsView.extend_tree_order([RPCServer, RPCClassServer, RPCFunctionServer], TypesFolder)

        ModelsView.alias_map[RPCClient] = 'rpc client configuration'
        ModelsView.alias_map[RPCServer] = 'rpc server configuration'
        ModelsView.alias_map[RPCClassServer] = 'rpc server class interface'
        ModelsView.alias_map[RPCFunctionServer] = 'rpc server function interface'

        ModelsView.edit_map.update({
            self.open_rpc_server: [RPCServer],
            self.open_rpc_client: [RPCClient]
        })
        # ModelsView._popup_map.update({self.on_rpc_server_popup: [RPCClient]})
        ModelsView._handlers.append(self)

    @property
    def can_edit_open(self):
        frame = context.get_frame()
        view = frame.viewBook.GetCurrentPage()
        if type(view) is ModelsView:
            if type(view.selected) in [RPCServer, RPCClient]:
                return True
        return False

    def on_update_rpc_wizard(self, event):
        """Handle the update of menu enable/disable status.
        This method attempts to get volatile self._project initialized
        for the method to apply he wizard"""
        frame = context.get_frame()
        self._project = None
        book = frame.viewBook
        try:
            view = book.GetCurrentPage()
            if view is not None:
                # get the selected item
                selected = view.selected
                if selected is not None:
                    project = getattr(selected, 'project', None)
                    if project and project.language == 'c++':
                        self._project = project
        except Exception as e:
            print(" some exception happen in rpc wizard:{}".format(e))
        event.Enable(bool(self._project))

    def append_tools_menu(self):
        # import beatle.app.resources as rc
        frame = context.get_frame()
        bmpserver = wx.Bitmap(local_path("plugin/tools/_cpp_rpc/res/cpprpcserver.xpm"), wx.BITMAP_TYPE_ANY)
        bmpclient = wx.Bitmap(local_path("plugin/tools/_cpp_rpc/res/cpprpcclient.xpm"), wx.BITMAP_TYPE_ANY)

        frame.add_tools_separator()
        self._rpc_wizard_menu_new_server = wxx.MenuItem(
            frame.menuTools, self._rpc_server_new, u"c++ rpc server",
            u"add c++ rpc server.", wx.ITEM_NORMAL)
        self._rpc_wizard_menu_new_server.SetBitmap(bmpserver)
        frame.add_tools_menu('c++ rpc server', self._rpc_wizard_menu_new_server)

        self._rpc_wizard_menu_new_client = wxx.MenuItem(
            frame.menuTools, self._rpc_client_new, u"c++ rpc client",
            u"add c++ rpc client.", wx.ITEM_NORMAL)
        self._rpc_wizard_menu_new_client.SetBitmap(bmpclient)
        frame.add_tools_menu('c++ rpc client', self._rpc_wizard_menu_new_client)

    def bind_events(self):
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_rpc_wizard, id=self._rpc_server_new)
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_rpc_wizard, id=self._rpc_client_new)
        self.Bind(wx.EVT_MENU, self.on_rpc_server, id=self._rpc_server_new)
        self.Bind(wx.EVT_MENU, self.on_rpc_client, id=self._rpc_client_new)

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        pass

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        if cls.instance is None:
            cls.instance = RPCWizard()
        return cls.instance

    @TransactionalMethod('add c++ rpc server')
    @wxx.CreationDialog(RPCServerDialog, RPCServer)
    def on_rpc_server(self, event):
        # TODO : remove that lines when finished
        dlg = wx.MessageDialog(
            context.get_frame(), "Not yet implemented.",
            "Error", style=wx.OK | wx.ICON_ERROR)
        dlg.ShowModal()
        dlg.Destroy()
        return
        frame = context.get_frame()
        self._project = None
        try:
            selected = frame.get_current_view().selected
            if selected and selected.project.language == 'c++':
                return frame, selected.project
        except Exception as e:
            pass
        return False

    @TransactionalMethod('add c++ rpc client')
    @wxx.CreationDialog(RPCClientDialog, RPCClient)
    def on_rpc_client(self, event):
        # TODO : remove that lines when finished
        dlg = wx.MessageDialog(
            context.get_frame(), "Not yet implemented.",
            "Error", style=wx.OK | wx.ICON_ERROR)
        dlg.ShowModal()
        dlg.Destroy()
        return
        frame = context.get_frame()
        self._project = None
        try:
            selected = frame.get_current_view().selected
            if selected :
                project = selected.project
                if selected.project.language == 'c++':
                    if len(RPCClient.rpc_server_project(project)) == 0:
                        dlg = wx.MessageDialog(
                            context.get_frame(), "Can't find a suitable rpc server open project",
                            "Error", style=wx.OK | wx.ICON_ERROR)
                        dlg.ShowModal()
                        dlg.Destroy()
                    else:
                        self._project = project
                        return frame, selected.project
        except Exception as e:
            pass
        return False

    def open_rpc_server(self, module, view=None):
        """Handles the edition of the pyBoost wizard as a document"""
        if getattr(module, '_pane', None):
            raise RuntimeError('Already open pane for module')
        frame = context.get_frame()
        pane = RPCServerEditor(frame.docBook, module)
        module._pane = pane
        frame.docBook.AddPage(pane, module.tab_label, True, module.bitmap_index)

    def open_rpc_client(self, module, view=None):
        """Handles the edition of the pyBoost wizard as a document"""
        # TODO
        """
        if getattr(module, '_pane', None):
            raise RuntimeError('Already open pane for module')
        frame = context.get_frame()
        pane = RPCClientEditor(frame.docBook, frame, module)
        module._pane = pane
        frame.docBook.AddPage(pane, module.tab_label, True, module.bitmap_index)
        """
