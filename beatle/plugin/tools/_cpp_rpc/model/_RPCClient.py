from beatle.model import Project, cc
from beatle.lib.api import context
from ._RPCServer import RPCServer


class RPCClient(cc.Module):
    """This class stores config of rpc client.
    Even this class is module-based, it's a container for
    rpc server exports and behaves like special class container:
    No one can drop classes here."""

    def __init__(self, **kwargs):
        super(RPCClient, self).__init__(**kwargs)

    @classmethod
    def rpc_server_project(cls, excluded):
        cpp_projects = []
        # Search for the available servers in beatle
        for w in context.get_app().workspaces:
            cpp_projects.extend(
                w(Project, filter=lambda p: p != excluded and p.language == 'c++' and len(p(RPCServer)) > 0))
        return cpp_projects

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("cpprpc")
