"""
    RPCFunctionServer : interface class for exported model functions
    ================================================================

    Reference:

        self._exported  : The model class to be exported
        self._server    : The rpc server class parent of this one
        self._members   : List of members to export from the exported class
        self._members_accessors : Dictionary of the form member -> "_ref_instance->{member.name}".
                                  This dictionary is used for building MSG_PACK serialization.
        self._methods   : List of the methods to export from the exported class
        self._sanitized_method_names : Dictionary that transform an method name into a sanitized one.
        self._static_methods    : Dictionary of the form exported method -> static member method
                                  used for calling the instance method



"""
from beatle.model import cc
from beatle import lib
from beatle.lib.utils import cached_type


class RPCFunctionServer(cc.Class):
    """Implements an inteface class"""

    def __init__(self, **kwargs):
        """
        Initialize a class that acts as an interface to some internal class
        self._internal : Holds the reference to the internal class that will be exported through rpc.
        """
        self._exported = kwargs['internal']  # the function exported
        self._server = kwargs['server']      # the current RPCServer instance
        self._static_method = None
        self._sanitized_method_name = self.sanitize_method_name()
        kwargs['name'] = '{self._exported.name}_interface'.format(self=self)
        super(RPCFunctionServer, self).__init__(**kwargs)

    def sanitize_method_name(self):
        """This method converts the exported methods that holds uncommon names to standard function
        names. The current conversion done is:
        operator == -> operator_eq      operator ++ -> operator_si      operator %= -> operator_sm
        operator != -> operator_ne      operator -- -> operator_di      operator () -> operator_CD
        operator >  -> operator_gt      operator += -> operator_sa      operator *  -> operator_p
        operator >  -> operator_gt      operator -= -> operator_sd      operator &  -> operator_r
        operator >= -> operator_ge      operator *= -> operator_st
        operator <= -> operator_le      operator /= -> operator_sq
        """
        name = self._exported.name
        if name.startswith('operator'):
            rest = name[9:].strip()
            try:
                index = ['==', '!=', '>', '<', '>=', '<=', '++', '--', '+=', '-=', '*=', '/=', '%=',
                         '+', '-', '*', '/', '%', '^', '->'].index(rest)
                convert = ['eq', 'ne', 'gt', 'lt', 'ge', 'le', 'si', 'sd', 'sa', 'sd', 'st', 'sq', 'sm',
                           'ad', 'sb', 'tm', 'dv', 'md', 'pw', 'pt']
                name = 'operator_{code}'.format(code=convert[index])
            except ValueError:
                rest.replace('(', 'C')
                rest.replace(')', 'D')
                rest.replace('*', 'p')
                rest.replace('&', 'r')
                rest.replace(' ', '_')
                name = 'operator_{code}'.format(code=rest)
        return name

    def type(self, reference_type, **kwargs):
        """commodity for accessing registered types"""
        if type(reference_type) is str:
            _type = lib.utils.cached_type(self.project, reference_type)
        else:
            _type = reference_type
        return cc.typeinst(type=_type, **kwargs)

    def build(self, **kwargs):
        """Create the content of the class interface.
        This comprises several steps:
            1 - include required headers : <c++.rpc/server.h> and <map>
            2 - add friendship for marshall
            3 - build internal members
            5 - add friendship into the exported class in order to have full access
            6 - create a pointer _instance_index holding the unique identifier from the object
            7 - create a pointer _object_ptr that would point to the handled object
            8 - create boolean _ami_owner telling  if this interface is a capture over existing object or the owner one
            9 - create protected constructor
           10 - create a protected destructor that can also destroy the object if owned
            """
        self.build_user_code()
        self.build_internal_constructor()
        self.build_exported_method(**kwargs)
        self.build_binding();

    def build_user_code(self):
        # 1 - include required headers : <c++.rpc/server.h> and <map>
        self.user_code_h1 = """
#include <c++.rpc/server.h>"""

        # 2 - add friendship for marshall
        # 3 - create a static dictionary _class_map translating public identifiers to instances of this class (decl)
        # 4 - create a static dictionary _interface_map translating internal classes  to instances of this class (decl)
        self.user_code_h2 = """
    friend class {ns}::marshall;""".format(ns=self._server.namespace)

    def build_internal_constructor(self):
        """
            Create protected constructor
            ----------------------------
            This constructor deleted, because this class really acts as a namespace.
        """
        cc.Constructor(
            access='protected',
            autoargs=False,
            autoinit=False,
            parent=self,
            deleted=True)

    def build_exported_method(self, **kwargs):
        """This method is responsible for generating export method for the method"""

        content = 'TO DO'
        note = 'Not yet implemented.'
        x = self._exported
        t = self._server.managed_type_instance(x.type_instance)
        content = self.code_export_marshal_function(self._exported)
        note = 'interface for {name}::{call} and interface instance'.format(name=self._exported.scoped, call=x.call)
        self._static_method = cc.MemberMethod(
            access='public',
            type=t,
            name=self._sanitized_method_name,
            parent=self,
            static=True,
            read_only=False,
            content=content,
            note=note
        )
        classes = self._server.exported_class_list
        for arg in x[cc.Argument]:
            if arg.type_instance.type in classes:
                cc.Argument(
                    parent=self._static_method,
                    name='{name}_instance_index'.format(name=arg.name),
                    type=self.type('size_t'))
            else:
                cc.Argument(
                    parent=self._static_method,
                    name=arg.name,
                    type=arg.type_instance)

    def build_binding(self):
        """This method registers the calls in the rpc server"""
        # build the content
        content = 'server.bind("{key}",&{name});\n'.format(key=str(self._exported.uid), name=self._static_method.name)

        # add exit session binding\n\t_server.bind("close",[](){rpc::this_session().post_exit();});'
        binder = cc.MemberMethod(
            parent=self,
            static=True,
            name='bind',
            type=self.type('void'),
            content=content)
        cc.Argument(
            parent=binder,
            type=self.type('rpc::server', ref=True),
            name='server'
        )

    def code_export_marshal_function(self, method):

        type_instance = method.type_instance
        # caso especial: si el tipo retornado corresponde a una clase
        #                exportada, ha de realizarse un mapping.
        #                En este caso siempre se retornara el objeto de
        #                interfaz, aunque la intancia de tipo sea puntero
        # type_instance = self.managed_type_instance(method.type_instance)
        if type_instance.type in self._server.exported_class_list:
            response_type = self._server.interface_class(type_instance.type)
            if type_instance.is_ptr:
                return self.code_export_function_returning_managed_class_ptr.format(
                    reponse_cls=response_type.name, call=method.call)
            elif type_instance.is_ref:
                return self.code_export_function_returning_managed_class_reference.format(
                    reponse_cls=response_type.name, call=method.call)
            else:
                raise RuntimeError("functions returning by-value instances of managed classes is not allowed so far.")
        elif type_instance.is_ptr or type_instance.type_name != 'void':
            return """
return {call};
""".format(call=method.call)
        else:
            return """
{call};
""".format(call=method.call)

    @property
    def code_export_function_returning_managed_class_ptr(self):
        return """
auto response_object = {call};
if( response_object == nullptr )
{{
    rpc::this_handler().respond_error("nullptr returned.");
}}
{response_cls}* interface={response_cls}::register(response_object);
if( interface == nullptr )
{{
    rpc::this_handler().respond_error("failed to create interface for returned value.");
}}
return *interface;
"""

    @property
    def code_export_function_returning_managed_class_reference(self):
        return """
auto& response_object = {call};
{response_cls}* interface={response_cls}::register(&response_object);
if( interface == nullptr )
{{
    rpc::this_handler().respond_error("failed to create interface for returned value.");
}}
return *interface;
"""

    @property
    def code_export_function_returning_managed_class_value(self):
        return """
/** 
this is a risky bussiness : we return a temporary object!
**/
auto response_object = {call};
{response_cls}* interface={response_cls}::register(&response_object);
if( interface == nullptr )
{{
    rpc::this_handler().respond_error("failed to create interface for returned value.");
}}
return *interface;
"""
