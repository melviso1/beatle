import beatle
import copy
from beatle import lib
from beatle.model import cc
from beatle.lib.tran import TransactionStack, DelayedMethod
from ._RPCClassServer import RPCClassServer
from ._RPCFunctionServer import RPCFunctionServer


def static(kwargs):
    kwargs['static'] = True
    return kwargs


def public(kwargs):
    kwargs['access'] = 'public'
    return kwargs


def read_only(kwargs):
    kwargs['read_only'] = True
    return kwargs


def static_readonly_method(**kwargs):
    return cc.MemberMethod(**static(read_only(kwargs)))


def static_method(**kwargs):
    return cc.MemberMethod(**static(read_only(kwargs)))


class RPCServer(cc.Module):
    """This class stores config of rpc server.
    Even this class is module-based, it's a container for
    rpc server exports and behaves like special class container:
    No one can drop classes here."""

    skip_build = True  # don't include this module in the makefile

    def type(self, reference_type, **kwargs):
        if type(reference_type) is str:
            _type = lib.utils.cached_type(self.project, reference_type)
        else:
            _type = reference_type
        return cc.typeinst(type=_type, **kwargs)

    def __init__(self, **kwargs):
        self._exports = kwargs.get('exports', [])
        self._port = kwargs.get('port', 8080)
        self._namespace = kwargs.get('namespace', 'rpc')

        self._container = self
        self._marshal = None
        self._class_interface_dict = {}  # each class interface needs to known what classes are exported
        self._function_interface_dict = {}
        self._published_methods_map = {}
        self._exported_classes = []  # incremental list
        self._exported_functions = []
        self._marshal_exports_folder = None
        super(RPCServer, self).__init__(**kwargs)
        self.project_settings()
        self.build(**kwargs)

    @property
    def port(self):
        return self._port

    @property
    def exported_class_list(self):
        return self._exported_classes

    @property
    def exports(self):
        return self._exports

    @property
    def namespace(self):
        return self._namespace

    def interface_class(self, class_):
        """This method returns the class that behaves as interface for another class"""
        return self._class_interface_dict[class_]  # must fail if not exists!

    def managed_type_instance(self, type_instance):
        """This method checks a type instance that could be used
        by a method as return type, or by argument, to translate
        into managed interface"""
        kwargs = type_instance.get_kwargs()
        base_type = kwargs['type']
        if base_type in self._exported_classes:
            kwargs['type'] = self._class_interface_dict[base_type]
            return cc.typeinst(**kwargs)
        return type_instance

    def project_settings(self):
        """When you add a rpc server, we have the following requirements:
            - use position independent code (-fPIC).
            - pthread support.
            - link against rpc and pthread (if this is not a static library)."""
        self.project.save_state()
        self.project.current_build_config.cpp_compiler.position_independent_code = True
        self.project.current_build_config.cpp_compiler.pthread_support = True
        if self.project.current_build_config.target.type != 'static library':
            self.project.current_build_config.linker.pthreads = True
            if 'pthread' not in self.project.current_build_config.linker.libs:
                self.project.current_build_config.linker.libs.append('pthread')
            if 'rpc' not in self.project.current_build_config.linker.libs:
                self.project.current_build_config.linker.libs.append('rpc')

    def build(self, **kwargs):
        self.save_state()
        self.build_namespace(**kwargs)
        self.create_export_marshal(**kwargs)  # Create the export marshall structure
        self.export_classes(**kwargs)
        self.export_functions(**kwargs)
        self.create_binder_and_run()

    def build_namespace(self, **kwargs):
        """
        Create the container namespace
        ------------------------------
        By default, the rpc server is populated inside the namespace rpc, creating them
        if nonexistent. The user can specify a compound namespace, like "rpc::dynamics"
        and then, two namespaces will be created and the innermost will be used as container
        for the rpc implementation.
        """
        self._namespace = kwargs.get('namespace', '')
        if len(self._namespace) == 0:
            return
        chain = self._namespace.split('::')
        container = self
        for namespace_name in chain:
            container = cc.Namespace(parent=container, name=namespace_name)
        self._container = container

    def create_export_marshal(self, **kwargs):
        """Create the export marshall.
        The export is basically a class rpc::marshall with deleted constructor which contains
        a bind.
        """
        self._marshal = cc.Class(parent=self._container, name='marshall', is_struct=True, read_only=True)
        if len(self._namespace) == 0:
            ns = ''
        else:
            ns = self._namespace+'::'
        cc.Constructor(parent=self._marshal, deleted=True, read_only=True)
        cc.Destructor(parent=self._marshal, access='private', read_only=True)

        # include the required files from librpc
        self._marshal.user_code_h1 = '#include <map>\n'
        self._marshal.user_code_h1 += '#include "c++.rpc/server.h"\n'
        self._marshal.user_code_h1 += '#include "c++.rpc/this_handler.h"\n'
        self._marshal.user_code_h1 += '#include "c++.rpc/this_session.h"\n'

        # instances storage (hardcoded by now for fast)
        self._marshal.user_code_h2 = '    static std::map<size_t,void*> _real_object_map;\n'
        self._marshal.user_code_h2 += '    static rpc::server _server;\n'
        # self._marshal.user_code_s2 = 'std::map<size_t,void*> {ns}marshall::_real_object_map;\n'.format(ns=ns)
        self._marshal.user_code_s2 += 'rpc::server  {ns}marshall::_server({port});'.format(port=self._port, ns=ns)

    def export_classes(self, **kwargs):
        """
            Export selected classes
            -----------------------
            This must be done in three steps:

                - Create an interface class for each one of the exported classes.
                  These classes will have the name of the exported classes with the suffix _interface.
                  Also, a friendship allowing access to internals of the exported class will be added
                  to that ones. The translation will be stored inside self._class_interface_dict
                   internal class -> interface class)

                - Mimic the inheritance between the internal classes in the interface classes for
                  allowing access to the same methods.

                - Build the internals of the classes exports.

        """
        # create interface classes
        self._exported_classes = [x for x in self._exports if type(x) is cc.Class]
        for cls in self._exported_classes:
            interface_class = RPCClassServer(
                parent=self._container,
                server=self, internal=cls, read_only=True, meta_args=kwargs)
            # add friendship between interface class and server
            cc.Friendship(parent=self._marshal, target=interface_class)
            self._class_interface_dict[cls] = interface_class

        # replicate inheritance
        for cls in self._exported_classes:
            for base in [x for x in cls.direct_bases if x in self._exported_classes]:
                cc.Inheritance(
                    access='public',
                    parent=self._class_interface_dict[cls],
                    ancestor=self._class_interface_dict[base])

        # Now, we can build the classes exports
        for cls in self._exported_classes:
            self._class_interface_dict[cls].build(**kwargs)

    def export_functions(self, **kwargs):
        self._exported_functions = [x for x in self._exports if type(x) is cc.Function]
        for function in self._exported_functions:
            interface_function = RPCFunctionServer(
                parent=self._container,
                server=self, internal=function, read_only=True, meta_args=kwargs)
            self._function_interface_dict[function] = interface_function
            interface_function.build(**kwargs)

    def create_binder_and_run(self):
        """Create a binding method that prepares rpc_interface to work"""
        content = ''
        for cls in self._exported_classes:
            interface_class = self._class_interface_dict[cls]
            scoped = interface_class.scoped_str('', False)
            content +='\n    {scoped}::bind(_server);'.format(scoped=interface_class.scoped_str('', False))
        for function in self._exported_functions:
            interface_function = self._function_interface_dict[function]
            content += '\n    {scoped}::bind(_server);'.format(scoped=interface_function.scoped_str('', False))
        content += '\n    _server.bind("close",[](){rpc::this_session().post_exit();});'

        static_readonly_method(
            parent=self._marshal,
            name='bind', type=self.type('void'),
            content=content)

        static_readonly_method(
            parent=self._marshal,
            name='run', type=self.type('void'),
            content='\tmarshall::bind();\n\tmarshall::_server.run();\n')

    def draggable(self):
        """we don't allow this top be moved."""
        return False

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("cpprpc")

    @DelayedMethod()
    def export_code_files(self, force=True, logger=None):
        for element in self(RPCClassServer):
            element.export_code_files(force, logger)

    def update_headers(self, force=False):
        """Realiza la generacion de fuentes"""
        pass

    def update_sources(self, force=False):
        """Realiza la generacion de fuentes"""
        pass



