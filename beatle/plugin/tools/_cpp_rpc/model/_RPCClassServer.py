"""
    RPCClassServer : interface class for exported model class
    =========================================================

    Reference:

        self._exported  : The model class to be exported
        self._server    : The rpc server class parent of this one
        self._members   : List of members to export from the exported class
        self._members_accessors : Dictionary of the form member -> "_ref_instance->{member.name}".
                                  This dictionary is used for building MSG_PACK serialization.
        self._methods   : List of the methods to export from the exported class
        self._sanitized_method_names : Dictionary that transform an method name into a sanitized one.
        self._static_methods    : Dictionary of the form exported method -> static member method
                                  used for calling the instance method



"""
from beatle.model import cc
from beatle import lib
from beatle.lib.utils import cached_type


class RPCClassServer(cc.Class):
    """Implements an inteface class"""

    def __init__(self, **kwargs):
        """
        Initialize a class that acts as an interface to some internal class
        self._internal : Holds the reference to the internal class that will be exported through rpc.
        """
        self._exported = kwargs['internal']  # the class exported
        self._server = kwargs['server']      # the current RPCServer instance
        kwargs['name'] = '{self._exported.name}_interface'.format(self=self)

        super(RPCClassServer, self).__init__(**kwargs)

        # common initializations
        self._members = []
        self._methods = []
        self._members_accessors = {}
        self._sanitized_method_names = {}
        self._static_methods = {}

        self.capture_exports()

    def capture_exports(self):
        """Initialize"""
        found_names = {}

        def sanitize_method_name(method):
            """This method converts the exported methods that holds uncommon names to standard function
            names. The current conversion done is:
            operator == -> operator_eq      operator ++ -> operator_si      operator %= -> operator_sm
            operator != -> operator_ne      operator -- -> operator_di      operator () -> operator_CD
            operator >  -> operator_gt      operator += -> operator_sa      operator *  -> operator_p
            operator >  -> operator_gt      operator -= -> operator_sd      operator &  -> operator_r
            operator >= -> operator_ge      operator *= -> operator_st
            operator <= -> operator_le      operator /= -> operator_sq
            """
            # Note that there are a lot of work still pending here
            if type(method) is cc.Constructor:
                name = 'instance_constructor'
            elif type(method) is cc.Destructor:
                name = 'instance_destructor'
            else:
                name = method.name
                if name.startswith('operator'):
                    rest = name[9:].strip()
                    try:
                        index = ['==', '!=', '>', '<', '>=', '<=', '++', '--', '+=', '-=', '*=', '/=', '%=',
                                 '+', '-', '*', '/', '%', '^', '->'].index(rest)
                        convert = ['eq', 'ne', 'gt', 'lt', 'ge', 'le', 'si', 'sd', 'sa', 'sd', 'st', 'sq', 'sm',
                                   'ad', 'sb', 'tm', 'dv', 'md', 'pw', 'pt']
                        name = 'operator_{code}'.format(code=convert[index])
                    except ValueError:
                        rest.replace('(', 'C')
                        rest.replace(')', 'D')
                        rest.replace('*', 'p')
                        rest.replace('&', 'r')
                        rest.replace(' ', '_')
                        name = 'operator_{code}'.format(code=rest)
            while name in found_names:
                count = found_names[name] + 1
                name = '{0}_{1}'.format(name, count)
                found_names[name] = 1
            else:
                found_names[name] = 1
            return name

        from .._RPCWizard import EXPORTABLE_MEMBER_METHOD_CLASSES

        self._members = [x for x in self._server.exports if type(x)
                         is cc.MemberData and x.inner_class is self._exported]

        self._members_accessors = dict((x, '_ref_instance->'+x.prefixed_name) for x in self._members)
        self._methods = [x for x in self._server.exports if type(x) in EXPORTABLE_MEMBER_METHOD_CLASSES
                         and x.inner_class is self._exported]
        self._sanitized_method_names = dict((x, sanitize_method_name(x)) for x in self._methods)

    def type(self, reference_type, **kwargs):
        """commodity for accessing registered types"""
        if type(reference_type) is str:
            _type = lib.utils.cached_type(self.project, reference_type)
        else:
            _type = reference_type
        return cc.typeinst(type=_type, **kwargs)

    @property
    def exported_bases(self):
        return [x for x in self._exported.direct_bases if x in self._server.exported_class_list]

    def build(self, **kwargs):
        """Create the content of the class interface.
        This comprises several steps:
            1 - include required headers : <c++.rpc/server.h> and <map>
            2 - add friendship for marshall
            3 - build internal members
            5 - add friendship into the exported class in order to have full access
            6 - create a pointer _instance_index holding the unique identifier from the object
            7 - create a pointer _object_ptr that would point to the handled object
            8 - create boolean _ami_owner telling  if this interface is a capture over existing object or the owner one
            9 - create protected constructor
           10 - create a protected destructor that can also destroy the object if owned
            """
        self.build_user_code()
        self.build_relation()
        self.build_internal_members()
        self.build_internal_constructor()
        self.build_internal_register()
        self.build_internal_destructor()
        self.build_exported_methods(**kwargs)
        self.build_binding();
        self.build_msgpack();

    def build_user_code(self):
        # 1 - include required headers : <c++.rpc/server.h> and <map>
        self.user_code_h1 = """
#include <c++.rpc/server.h>
#include <map>"""

        # 2 - add friendship for marshall
        # 3 - create a static dictionary _class_map translating public identifiers to instances of this class (decl)
        # 4 - create a static dictionary _interface_map translating internal classes  to instances of this class (decl)
        self.user_code_h2 = """
    friend class {ns}::marshall;
    static std::map<size_t, {this_class}*> _interface_index_map;  // index -> interface
    static std::map<{internal_class}*,{this_class}*> _interface_instance_map;  // instance -> interface 
   """.format(ns=self._server.namespace, internal_class=self._exported.scoped, this_class=self.name)

        # 3 - create a static dictionary _class_map translating public identifiers to instances of this class (impl)
        # 4 - create a static dictionary _interface_map translating internal classes  to instances of this class (impl)
        self.user_code_s2 = """
std::map<size_t, {this_class}*> {ns}::{self.name}::_interface_index_map{{}};
std::map<{internal_class}*, {this_class}*> {ns}::{self.name}::_interface_instance_map{{}};
""".format(ns=self._server.namespace, internal_class=self._exported.scoped,
                       this_class=self.scoped, self=self)

    def build_relation(self):
        """Add a relation from the internal class instance to the interface,
        for lifetime control."""
        # TODO : why critical sections dont work?
        cc.Relation(
            access='public',
            from_class=self._exported,
            to_class=self,
            fromName='instance',
            toName='rpc_interface',
            single=True,
            aggregate=True,
            critical=False,
            static=False,
            filter=False,
            unique=False,
            from_access='private',
            to_access='private',
            implementation='standard')

    def build_internal_members(self):
        """Create the required members in the class interface
        The members are:

            size_t managed_instance_index : an integer representing the internal instance
            bool ami_owner : a boolean (defaulting to false) that indicate if the interface
                             class is the owner of the internal instance.
        """
        # 1 - create a pointer managed_instance_index holding the unique identifier from the object.
        #     This object would be included in the message_pack list for the class, but cant be considered
        #     as an exported member. This is used for serializing the interface class as one
        cc.MemberData(
                type=cc.typeinst(type=cached_type(self.project, 'size_t')),
                access='private',
                default='0',
                name='managed_instance_index',
                parent=self,
                visibleInTree=False)

        # 8 - create boolean _ami_owner telling  if this interface is a capture over existing object or the owner one
        cc.MemberData(
            type=cc.typeinst(type=cached_type(self.project, 'bool')),
            access='private',
            default='false',
            name='ami_owner',
            parent=self,
            visibleInTree=False
        )

    def build_internal_constructor(self):
        """
            Create protected constructor
            ----------------------------
            This constructor is used for inserting the _managed_instance_index
            in the _class_map and _managed_instance in the _interface_map dictionaries.
        """
        # Create protected constructor.
        # This constructor also insert the object in the static maps
        ctor = cc.Constructor(
            access='protected',
            autoargs=False,
            autoinit=False,
            parent=self,
            init=""": _ref_instance{nullptr}
, _managed_instance_index{reinterpret_cast<size_t>(this)}
, _ami_owner{is_owner}
""",
            content="""    __init__( instance );
    //add this object to the static maps
    _interface_index_map[_managed_instance_index] = this;
    _interface_instance_map[instance] = this;
""",
            note='Create a internal instance rpc interface.'
        )
        #   add required arguments
        cc.Argument(
            parent=ctor,
            type=cc.typeinst(type=self._exported, ptr=True),
            name="instance"
        )
        cc.Argument(
            parent=ctor,
            type=cc.typeinst(type=cached_type(self.project, 'bool')),
            name="is_owner",
            default='false'
        )

    def build_internal_register(self):
        """Create a static method interface* <interface>::register(instance)
        that search for a interface to the instance. If none is found, a new
        interface is created for the instance."""
        register_method = cc.MemberMethod(
            name='register_instance',
            access='protected',
            static=True,
            parent=self,
            type=cc.typeinst(type=self, ptr=True),
            content="""    auto iter = _interface_instance_map.find(instance);
    if ( iter != _interface_instance_map.end() )
    {{
        return iter->second;
    }}
    auto result = new {interface}(instance, is_owner);
    assert(_interface_instance_map.find(instance) != _interface_instance_map.end());
    return result;
    """.format(interface=self.scoped),
            note="""    This method search for a registered interface to the object instance
and return the existing one if success. Otherwise, this method
create and registers a new interface for the object and returns it.
""")
        cc.Argument(
            name='instance',
            type=self.type(self._exported, ptr=True, constptr=True),
            parent=register_method,
        )
        cc.Argument(
            name='is_owner',
            type=self.type('bool'),
            default='false',
            parent=register_method
        )

    def build_internal_destructor(self):
        """Create a delete method thar conditionally deletes the instance
        In the current implementation, interface classes are also multiowned aggregate
        passive instances of the managed class, so this method cant be a destructor
        """
        cc.MemberMethod(
            type=cc.typeinst(type=cached_type(self.project, 'void')),
            parent=self,
            access='protected',
            virtual=False,
            name="destroy",
            content="""    if ( _ami_owner )
    {{
        delete _ref_instance;
    }}
    else
    {{
        delete this;
    }}
    """.format(self=self),
            note="""
            Delete the interface, and also the contained object if this is owned by this interface."""
        )
        cc.Destructor(
            parent=self,
            virtual=True,
            access='protected',
            read_only=True,
            content="""    __exit__();
    _interface_instance_map.erase(_ref_instance);""",
            note="default destructor."
        )

    def build_exported_methods(self, **kwargs):
        """This method is responsible for generating export methods for the classes.
        This is executed after the interface classes are already created, because of
        some type translation could be required."""

        content = 'TO DO'
        note = 'Not yet implemented.'
        for x in self._methods:
            if type(x) is cc.Constructor:
                t = self.type(self, ref=True)
                content = self.code_export_marshal_constructor.format(cls=self._exported.scoped, call=x.call)
                note = 'create new class {name} and interface instance'.format(name=self._exported.scoped)
            elif type(x) is  cc.Destructor:
                content = self.code_export_marshal_destructor.format(cls=self._exported.scoped)
                note = 'delete the interface instance and, if owned, the {name} instance.'.format(name=self._exported.scoped)
                t = cc.typeinst(type=cached_type(self.project, 'void'))
            elif isinstance(x, cc.MemberMethod):
                t = self._server.managed_type_instance(x.type_instance)
                content = self.code_export_marshal_class_method(self._exported, x)
                note = 'interface for {name}::{call} and interface instance'.format(name=self._exported.scoped, call=x.call)
            else:
                t = x.type_instance
            interface_method = cc.MemberMethod(
                access='public',
                type=t,
                name=self._sanitized_method_names[x],
                parent=self,
                static=True,
                read_only=False,
                content=content,
                note=note
            )
            self._static_methods[x] = interface_method
            if type(x) is not cc.Constructor and not x.static:
                cc.Argument(
                    parent=interface_method,
                    name='instance_index',
                    type=self.type('size_t'))
            classes = self._server.exported_class_list
            for arg in x[cc.Argument]:
                if arg.type_instance.type in classes:
                    cc.Argument(
                        parent=interface_method,
                        name='{name}_instance_index'.format(name=arg.name),
                        type=self.type('size_t'))
                else:
                    cc.Argument(
                        parent=interface_method,
                        name=arg.name,
                        type=arg.type_instance)

    def build_binding(self):
        """This method registers the calls in the rpc server"""
        # build the content
        content = '   \n'.join('server.bind("{key}",&{name});'.format(
            key=str(x.uid), name=self._static_methods[x].name) for x in self._methods)

        # add exit session binding\n\t_server.bind("close",[](){rpc::this_session().post_exit();});'
        binder = cc.MemberMethod(
            parent=self,
            static=True,
            name='bind',
            type=self.type('void'),
            # type=cc.typeinst(type_alias='rpc::server'),
            content=content)
        # add argument
        cc.Argument(
            parent=binder,
            type=self.type('rpc::server', ref=True),
            name='server'
        )

    def build_msgpack(self):
        """This method builds the msgpack definition"""
        self.user_code_h3="""public:
    MSGPACK_DEFINE_ARRAY(_managed_instance_index,{})\n""".format(','.join(self._members_accessors.values()))

    @property
    def code_export_marshal_constructor(self):
        return """
    {cls}* object = new {call};
    if ( object == nullptr )
    {{
        rpc::this_handler().respond_error("allocation of new {cls} instance failed");
    }}
    {cls}_interface* interface = {cls}_interface::register_instance(object, true);
    if ( interface == nullptr )
    {{
        delete object;
        rpc::this_handler().respond_error("allocation new {cls} interface failed");
    }}
    if ( ! interface->_ami_owner )
    {{
        delete object;  // that will delete also the interface
        rpc::this_handler().respond_error("ownership adquisition of new {cls} object failed");
    }}
    return *interface;
"""

    @property
    def code_export_marshal_destructor(self):
        return """
auto it = {cls}_interface::_interface_index_map.find(instance_index);
if( it == {cls}_interface::_interface_index_map.end() )
{{
    rpc::this_handler().respond_error("invalid object index.");
}}
else
{{
    it->second->destroy();
}}
"""

    def code_export_marshal_class_method(self, cls, method):

        type_instance = method.type_instance
        # caso especial: si el tipo retornado corresponde a una clase
        #                exportada, ha de realizarse un mapping.
        #                En este caso siempre se retornara el objeto de
        #                interfaz, aunque la intancia de tipo sea puntero
        # type_instance = self.managed_type_instance(method.type_instance)
        if type_instance.type in self._server.exported_class_list:
            response_type = self._server.interface_class(type_instance.type)
            if type_instance.is_ptr:
                if method.static:
                    return self.code_export_marshal_managed_static_ptr_class_method.format(
                        cls=cls.name, reponse_cls=response_type.name, call=method.call)
                else:
                    return self.code_export_marshal_managed_ptr_class_method.format(
                        cls=cls.name, reponse_cls=response_type.name, call=method.call)
            elif type_instance.is_ref:
                if method.static:
                    return self.code_export_marshal_managed_static_reference_class_method.format(
                        cls=cls.name, reponse_cls=response_type.name, call=method.call)
                else:
                    return self.code_export_marshal_managed_reference_class_method.format(
                        cls=cls.name, reponse_cls=response_type.name, call=method.call)
            else:
                raise RuntimeError("methods returning by-value instances of managed classes is not allowed so far.")
        elif type_instance.is_ptr or type_instance.type_name != 'void':
            return """
auto it= {cls}_interface::_interface_index_map.find(instance_index);
if(it == {cls}_interface::_interface_index_map.end())
{{
     rpc::this_handler().respond_error("invalid object index.");
}}
return it->second->_ref_instance->{call};
""".format(cls=cls.name, call=method.call)
        else:
            return """
auto it= {cls}_interface::_interface_index_map.find(instance_index);
if(it == {cls}_interface::_interface_index_map.end())
{{
     rpc::this_handler().respond_error("invalid object index.");
}}
it->second->_ref_instance->{call};
""".format(cls=cls.name, call=method.call)

    @property
    def code_export_marshal_managed_static_ptr_class_method(self):
        return """
auto response_object = {cls}::{call};
if( response_object == nullptr )
{{
    rpc::this_handler().respond_error("nullptr returned.");
}}
{response_cls}* interface={response_cls}::register(response_object);
if( interface == nullptr )
{{
    rpc::this_handler().respond_error("failed to create interface for returned value.");
}}
return *interface;
"""

    @property
    def code_export_marshal_managed_ptr_class_method(self):
        return """
auto it = _interface_index_map.find(instance_index);
if( it == _interface_index_map.en() )
{{
    rpc::this_handler().respond_error("invalid interface reference.");
}}
auto result = it->second->_ref_instance->{call};
{response_cls}* interface ={response_cls}::register(result); 
if( interface == nullptr )
{{
    rpc::this_handler().respond_error("failed to bind interface results.");
}}
return *interface;
"""

    @property
    def code_export_marshal_managed_static_reference_class_method(self):
        return """
auto& response_object = {cls}::{call};
size_t uid_local = reinterpret_cast<size_t>(&response_object);
if( librpc_toolkit::_real_object_map.find(uid_local) == librpc_toolkit::_real_object_map.end() )
{{
    librpc_toolkit::_real_object_map[ uid_local ] = &response_object;
    response_object._rpc_object_index = uid_local;
}}
return response_object;
"""

    @property
    def code_export_marshal_managed_reference_class_method(self):
        return """
{cls}_interface interface = {cls}_interface::_interface_index_map(instance_index);
if( object == nullptr )
{{
    rpc::this_handler().respond_error("invalid object reference.");
}}
auto& ref = interface->_ref_instance->{call};
response_cls* interface = {response_cls}::register_instance(&ref);
if( interface == nullptr )
{{
    rpc::this_handler().respond_error("failed to bind interface for result.");
}}
return *interface;
"""

    @property
    def code_export_marshal_managed_static_temporary_class_method(self):
        return """
auto response_object = {cls}::{call};
return response_object;
"""

    @property
    def code_export_marshal_temporary_class_method(self):
        return """
auto object = librpc_toolkit::{cls}_accessor(instance_index);
if( object == nullptr )
{{
    rpc::this_handler().respond_error("invalid object reference.");
}}
auto response_object = object->{call};
return response_object;
"""
