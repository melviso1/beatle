# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-110-gef5c4c31)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
from beatle.lib import wxx
from beatle.lib.wxx.agw import _CustomTreeCtrl as CT

###########################################################################
## Class RPCClientDialogBase
###########################################################################

class RPCClientDialogBase ( wxx.Dialog ):

	def __init__( self, parent ):
		wxx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 894,616 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		fgSizer13 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer13.AddGrowableCol( 0 )
		fgSizer13.AddGrowableRow( 0 )
		fgSizer13.SetFlexibleDirection( wx.BOTH )
		fgSizer13.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		fgSizer9 = wx.FlexGridSizer( 1, 3, 0, 0 )
		fgSizer9.AddGrowableCol( 2 )
		fgSizer9.AddGrowableRow( 0 )
		fgSizer9.SetFlexibleDirection( wx.BOTH )
		fgSizer9.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fgSizer9.Add( ( 10, 0), 1, wx.EXPAND, 5 )

		self.m_staticText6 = wx.StaticText( self, wx.ID_ANY, u"Select the projects, classes, members and methods for what you wish to generate imports.\nPlease take in account that you can only create imports from projects with export definitions.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( 200 )

		fgSizer9.Add( self.m_staticText6, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 5 )

		sbSizer21 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"select projects" ), wx.VERTICAL )

		style = (CT.TR_MULTIPLE | CT.TR_AUTO_CHECK_CHILD | CT.TR_AUTO_CHECK_PARENT
		| CT.TR_AUTO_TOGGLE_CHILD | CT.TR_HIDE_ROOT)
		self.m_import_tree =CT.CustomTreeCtrl(sbSizer21.GetStaticBox(), style=style)

		sbSizer21.Add( self.m_import_tree, 1, wx.ALL|wx.EXPAND, 5 )


		fgSizer9.Add( sbSizer21, 1, wx.EXPAND|wx.ALL, 5 )


		fgSizer13.Add( fgSizer9, 1, wx.EXPAND, 5 )

		fgSizer43 = wx.FlexGridSizer( 1, 3, 0, 0 )
		fgSizer43.AddGrowableCol( 0 )
		fgSizer43.SetFlexibleDirection( wx.BOTH )
		fgSizer43.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_info = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0|wx.BORDER_NONE )

		self.m_info.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ) )
		fgSizer43.Add( self.m_info, 0, wx.ALL, 5 )

		self.m_button8 = wx.Button( self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer43.Add( self.m_button8, 0, wx.ALL, 5 )

		self.m_button7 = wx.Button( self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )

		self.m_button7.SetDefault()
		fgSizer43.Add( self.m_button7, 0, wx.ALL, 5 )


		fgSizer13.Add( fgSizer43, 1, wx.EXPAND, 5 )


		self.SetSizer( fgSizer13 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button8.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_button7.Bind( wx.EVT_BUTTON, self.on_ok )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_cancel( self, event ):
		event.Skip()

	def on_ok( self, event ):
		event.Skip()


