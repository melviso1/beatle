"""Subclass of RPCServerEditorBase, which is generated by wxFormBuilder."""
import re
import wx
import copy
from beatle import model
from beatle.lib import wxx, tran
from beatle.lib.api import context
from beatle.lib.wxx.agw import GenericTreeItem, EVT_TREE_ITEM_CHECKING, EVT_TREE_ITEM_CHECKED
from beatle.model import cc
from .ui_server import RPCServerEditorBase
from ..model import RPCClassServer, RPCServer, RPCClient


class RPCServerEditor(RPCServerEditorBase):
    """
     Using this pane you could select the data, methods or classes
     to be exported by your server.
     Please take in account that, when exporting elements from another
     projects, it  must be linked from the current one. You must also
     ensure the visibility of these exported elements from the current
     project.
     """

    @wxx.SetInfo(__doc__)
    def __init__(self, parent, module):
        from .._RPCWizard import EXPORTABLE_MEMBER_METHOD_CLASSES
        self.EXPORTABLE_MEMBER_METHOD_CLASSES = EXPORTABLE_MEMBER_METHOD_CLASSES
        self._project = module.project
        self._object = module
        self._show_other_projects = False
        self._name = module.name
        self._namespace = module.namespace
        self._port = module.port
        self._exports = copy.copy(self._object.exports)
        super(RPCServerEditor, self).__init__(parent)
        self.m_name.SetValue(self._name)
        self.m_port.SetValue(str(self._port))
        self.m_namespace.SetValue(self._namespace)
        self.__setup_tree_image_list__()
        self.__fill_tree__()
        self.__bind_events__()

    def __setup_tree_image_list__(self):
        import beatle.app.resources as rc
        from beatle.lib.wxx.agw import EVT_TREE_ITEM_CHECKED
        self.image_list = rc.get_bitmap_image_list()
        self.check_image_list = wx.ImageList(16, 16, 4)
        self.check_image_list.Add(wx.Bitmap(rc._checked))
        self.check_image_list.Add(wx.Bitmap(rc._unchecked))
        self.check_image_list.Add(wx.Bitmap(rc._checked_disabled))
        self.check_image_list.Add(wx.Bitmap(rc._unchecked_disabled))
        self.m_export_tree.SetImageListCheck(16, 16, self.check_image_list)
        self.m_export_tree.SetImageList(self.image_list)

    def __fill_tree__(self):
        from .._RPCWizard import EXPORTABLE_MEMBER_METHOD_CLASSES

        def class_is_exportable(x):
            try:
                next(method for method in x(*EXPORTABLE_MEMBER_METHOD_CLASSES) if method.access == 'public')
                return True
            except StopIteration:
                pass
            try:
                next(member for member in x(cc.MemberData) if member.access == 'public')
                return True
            except StopIteration:
                pass
            return False

        def project_is_exportable(x):
            try:
                next(clase for clase in x(cc.Class) if class_is_exportable(x))
                return True
            except StopIteration:
                pass
            try:
                next(method for method in x(cc.Function) if not method.static)
                return True
            except StopIteration:
                pass
            try:
                next(data for data in x(cc.Data) if not data.static)
                return True
            except StopIteration:
                pass
            return False

        root_item = self.m_export_tree.AddRoot('hidden')
        if self._show_other_projects:
            projects = [x for x in self._project.parent(model.Project) if
                        x._language == 'c++' and project_is_exportable(x)]
        else:
            projects = [self._project]
        for project in projects:
            project_item = self.m_export_tree.AppendItem(
                root_item,
                text=project.tree_label, ct_type=1, wnd=None,
                image=self._project.bitmap_index,
                selImage=project.bitmap_index, data=project)
            classes = [x for x in project.level_classes if x.inner([RPCServer, RPCClient]) is None and
                       class_is_exportable(x)]
            for cls in classes:
                self.__add_to_server_tree__(project_item, cls)
            functions = [x for x in project(cc.Function) if not x.static]
            for function in functions:
                self.__add_to_server_tree__(project_item, function)
            variables = [x for x in project(cc.Data) if not x.static]
            for variable in variables:
                self.__add_to_server_tree__(project_item, variable)

    def __add_to_server_tree__(self, treeitem, obj):
        # se inserta el elemento
        this_item = self.m_export_tree.AppendItem(
            treeitem,
            text=obj.tree_label, ct_type=1, wnd=None,
            image=obj.bitmap_index,
            selImage=obj.bitmap_index, data=obj)
        if obj in self._exports:
            self.m_export_tree.CheckItem(this_item, True)
        if not self.m_export_tree.IsExpanded(treeitem):
            self.m_export_tree.Expand(treeitem)

        def is_public_class_member(x):
            return x.inner_class is obj and x._access == 'public'

        def is_explicit_public_class_member(x):
            return x.parent.inner_class is obj and getattr(x, '_access', 'private') == 'public'

        if type(obj) is cc.Class:
            for member_method in obj(*self.EXPORTABLE_MEMBER_METHOD_CLASSES, filter=is_public_class_member):
                if (type(member_method.parent) is cc.RelationTo and
                        type(member_method.parent.key.to_class) is RPCClassServer):
                    continue
                self.__add_to_server_tree__(this_item, member_method)
            for member_data in obj(cc.MemberData, filter=is_public_class_member):
                self.__add_to_server_tree__(this_item, member_data)
            for member_class in obj(cc.Class, filter=is_explicit_public_class_member):
                self.__add_to_server_tree__(this_item, member_class)

    def __bind_events__(self):
        self.m_export_tree.Bind(EVT_TREE_ITEM_CHECKED, self.on_tree_check_item)

    def on_tree_check_item(self, event):
        """When some item is toggled, we must add or remove it from export table.
        It's possible that we need to propagate the action upwards (to the parent) and
        downwards (to the children)"""

        def has_checked_child(item_):
            """Comprueba si el item posee algun hjo checkeado"""
            child_item, cookie = self.m_export_tree.GetFirstChild(item_)
            while child_item.IsOk():
                if self.m_export_tree.IsItemChecked(child_item):
                    return True
                child_item, cookie = self.m_export_tree.GetNextChild(item_, cookie)
            return False

        def populate_check_to_child(item_, check_):
            """Populate selection from parent to child"""
            checked = self.m_export_tree.IsItemChecked(item_)
            obj_ = self.m_export_tree.GetPyData(item_)
            if check_:
                if obj_ not in self._exports:
                    self._exports.append(obj_)
                elif checked == check_:
                    return
            else:
                if obj_ in self._exports:
                    self._exports.remove(obj_)
                elif checked == check_:
                    return
            self.m_export_tree.CheckItem2(item_, check_, True)
            child_item, cookie = self.m_export_tree.GetFirstChild(item_)
            while child_item.IsOk():
                populate_check_to_child(child_item, check_)
                child_item, cookie = self.m_export_tree.GetNextChild(item_, cookie)

        def populate_check_to_parents(item_):
            obj_ = self.m_export_tree.GetPyData(item_)
            check_ = self.m_export_tree.IsItemChecked(item_)
            if obj_ is not None and check_ != has_checked_child(item_):
                if check_:
                    if obj_ in self._exports:
                        self._exports.remove(obj_)
                else:
                    if obj_ not in self._exports:
                        self._exports.append(obj_)
                self.m_export_tree.CheckItem2(item_, not check_, True)
                populate_check_to_parents(self.m_export_tree.GetItemParent(item_))

        item = event.GetItem()
        check = self.m_export_tree.IsItemChecked(item)
        populate_check_to_child(item, check)
        populate_check_to_parents(self.m_export_tree.GetItemParent(item))

    def on_show_other_projects(self, event):
        """Called when the user selects to show other projects"""
        self._show_other_projects = self.m_checkBox1.GetValue()
        self.m_export_tree.DeleteAllItems()
        self.__fill_tree__()

    def get_kwargs(self):
        """returns arguments for instance... pending"""
        kwargs = {
            'parent': self._project,
            'name': self._name,
            'namespace': self._namespace,
            'port': self._port,
            'exports': self._exports
        }
        return kwargs

    def validate(self):
        """Dialog validation"""
        self._name = self.m_name.GetValue().strip()
        if len(self._name) == 0:
            wx.MessageBox("Server name must not be empty", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        if re.match("^([A-Za-z_])[0-9A-Za-z_]*", self._name) is None:
            wx.MessageBox("Server name contains invalid characters", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self._namespace = self.m_namespace.GetValue().strip()
        if len(self._namespace) > 0:
            if re.match("^([A-Za-z_])[0-9A-Za-z_]*(::([A-Za-z_])[0-9A-Za-z_]*)*", self._namespace) is None:
                wx.MessageBox("Server namespace contains invalid characters", "Error",
                              wx.OK | wx.CENTER | wx.ICON_ERROR, self)

        self._port = self.m_port.GetValue().strip()
        if len(self._port) == 0:
            wx.MessageBox("Server port must not be empty", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False

        if not self._port.isdigit():
            wx.MessageBox("Server port must be numeric", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        self._port = int(self._port)
        if self._port < 0 or self._port > 65535:
            wx.MessageBox("Server port ranger is invalid (0-65535)", "Error",
                          wx.OK | wx.CENTER | wx.ICON_ERROR, self)
            return False
        if len(self._exports) == 0:
            if wx.MessageBox("No exports selected. You wish to delete the rpc server?", "Question",
                             wx.YES_NO | wx.CENTER | wx.ICON_QUESTION, self) == wx.YES:
                return True
            return False
        return True

    def on_port_char(self, event):
        pass

    def apply(self):
        module = self._object
        module.save_state()
        module.delete()
        if len(self._exports) > 0:
            RPCServer(**self.get_kwargs())

    def commit(self):
        pass

    def on_cancel(self, event):
        frame = context.get_frame()
        frame.do_close_current_doc_pane()
        return False

    @tran.TransactionalMethod('modify rpc server')
    def on_ok(self, event):
        if self.validate():
            self.apply()
            frame = context.get_frame()
            frame.do_close_current_doc_pane()
            return True
        return False
