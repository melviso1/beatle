# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jul 11 2021)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.adv
from wx.lib.agw.customtreectrl import CustomTreeCtrl
from beatle.lib import wxx
from beatle.lib.wxx.agw import _CustomTreeCtrl as CT

# special import for beatle development
from beatle.lib.handlers import identifier
###########################################################################
## Class RPCServerDialogBase
###########################################################################

class RPCServerDialogBase ( wxx.Dialog ):
    
    def __init__( self, parent ):
        wxx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"new c++ RPC server", pos = wx.DefaultPosition, size = wx.Size( 894,616 ), style = wx.DEFAULT_DIALOG_STYLE )
        
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        
        fgSizer8 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer8.AddGrowableCol( 0 )
        fgSizer8.AddGrowableRow( 0 )
        fgSizer8.SetFlexibleDirection( wx.BOTH )
        fgSizer8.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        fgSizer11 = wx.FlexGridSizer( 1, 3, 0, 0 )
        fgSizer11.AddGrowableCol( 2 )
        fgSizer11.AddGrowableRow( 0 )
        fgSizer11.SetFlexibleDirection( wx.BOTH )
        fgSizer11.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        fgSizer11.Add( ( 10, 0), 1, wx.EXPAND, 5 )
        
        self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"Select the classes, members and methods that will be exposed by rpc server. Please take in account that   methods return or members types must be basic types or exported types. Exporting pointers, although supported has not more protection than using it locally.", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( 200 )
        fgSizer11.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 5 )
        
        sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"select what to serve:" ), wx.VERTICAL )

        fgSizer2 = wx.FlexGridSizer( 3, 1, 0, 0 )
        fgSizer2.AddGrowableCol( 0 )
        fgSizer2.AddGrowableRow( 0 )
        fgSizer2.SetFlexibleDirection( wx.BOTH )
        fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        style = (CT.TR_MULTIPLE | CT.TR_AUTO_CHECK_CHILD | CT.TR_AUTO_CHECK_PARENT
        | CT.TR_AUTO_TOGGLE_CHILD | CT.TR_HIDE_ROOT)
        self.m_export_tree = CT.CustomTreeCtrl(sbSizer2.GetStaticBox(), style=style)

        fgSizer2.Add( self.m_export_tree, 1, wx.ALL|wx.EXPAND, 5 )
        
        fgSizer3 = wx.FlexGridSizer( 3, 3, 0, 0 )
        fgSizer3.AddGrowableCol( 1 )
        fgSizer3.SetFlexibleDirection( wx.BOTH )
        fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_staticText8 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"name:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText8.Wrap( -1 )
        fgSizer3.Add( self.m_staticText8, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_name = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, u"server", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer3.Add( self.m_name, 0, wx.ALL|wx.EXPAND, 5 )


        fgSizer3.Add( ( 20, 0), 1, wx.EXPAND, 5 )

        self.m_staticText3 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"server listener port:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        fgSizer3.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
        
        self.m_port = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, u"8080", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer3.Add( self.m_port, 0, wx.ALL, 5 )
        

        fgSizer3.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_staticText9 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"namespace:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        fgSizer3.Add( self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
        
        self.m_namespace = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, u"rpc", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer3.Add( self.m_namespace, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )
        
        
        fgSizer3.Add( ( 0, 0), 1, wx.EXPAND, 5 )


        fgSizer2.Add( fgSizer3, 1, wx.EXPAND, 5 )
        
        self.m_checkBox1 = wx.CheckBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"show other projects", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_checkBox1.Enable( False )
        self.m_checkBox1.Hide()

        fgSizer2.Add( self.m_checkBox1, 0, wx.ALL, 5 )


        sbSizer2.Add( fgSizer2, 1, wx.EXPAND|wx.TOP, 5 )
        
        
        fgSizer11.Add( sbSizer2, 1, wx.EXPAND|wx.ALL, 5 )
        
        
        fgSizer8.Add( fgSizer11, 1, wx.EXPAND|wx.TOP, 5 )

        fgSizer43 = wx.FlexGridSizer( 1, 3, 0, 0 )
        fgSizer43.AddGrowableCol( 0 )
        fgSizer43.SetFlexibleDirection( wx.BOTH )
        fgSizer43.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_info = wx.BitmapButton( self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
        fgSizer43.Add( self.m_info, 0, wx.ALL, 5 )

        self.m_button8 = wx.Button( self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer43.Add( self.m_button8, 0, wx.ALL, 5 )
        
        self.m_button7 = wx.Button( self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_button7.SetDefault()
        fgSizer43.Add( self.m_button7, 0, wx.ALL, 5 )
        

        fgSizer8.Add( fgSizer43, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( fgSizer8 )
        self.Layout()
        
        self.Centre( wx.BOTH )
        
        # Connect Events
        self.m_port.Bind( wx.EVT_CHAR, self.OnPortChar )
        self.m_checkBox1.Bind( wx.EVT_CHECKBOX, self.on_show_other_projects )
        self.m_button8.Bind( wx.EVT_BUTTON, self.on_cancel )
        self.m_button7.Bind( wx.EVT_BUTTON, self.on_ok )
    
    def __del__( self ):
        pass
    
    
    # Virtual event handlers, overide them in your derived class
    def OnPortChar( self, event ):
        event.Skip()

    def on_show_other_projects(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()


    def on_ok(self, event):
        event.Skip()


###########################################################################
## Class RPCServerEditorBase
###########################################################################

class RPCServerEditorBase ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 722,599 ), style = wx.TAB_TRAVERSAL )

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer8 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer8.AddGrowableCol(0)
        fgSizer8.AddGrowableRow(0)
        fgSizer8.SetFlexibleDirection(wx.BOTH)
        fgSizer8.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer11 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer11.AddGrowableCol(2)
        fgSizer11.AddGrowableRow(0)
        fgSizer11.SetFlexibleDirection(wx.BOTH)
        fgSizer11.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer11.Add((10, 0), 1, wx.EXPAND, 5)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY,
                                           u"Select the classes, members and methods that will be exposed by rpc server. Please take in account that   methods return or members types must be basic types or exported types. Exporting pointers, although supported has not more protection than using it locally.",
                                           wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(200)
        fgSizer11.Add(self.m_staticText2, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        sbSizer2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"select what to serve:"), wx.VERTICAL)

        fgSizer2 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer2.AddGrowableCol(0)
        fgSizer2.AddGrowableRow(0)
        fgSizer2.SetFlexibleDirection(wx.BOTH)
        fgSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        style = (CT.TR_MULTIPLE | CT.TR_AUTO_CHECK_CHILD | CT.TR_AUTO_CHECK_PARENT
                 | CT.TR_AUTO_TOGGLE_CHILD | CT.TR_HIDE_ROOT)
        self.m_export_tree = CT.CustomTreeCtrl(sbSizer2.GetStaticBox(), style=style)

        fgSizer2.Add(self.m_export_tree, 1, wx.ALL | wx.EXPAND, 5)

        fgSizer3 = wx.FlexGridSizer(3, 3, 0, 0)
        fgSizer3.AddGrowableCol(1)
        fgSizer3.SetFlexibleDirection(wx.BOTH)
        fgSizer3.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText8 = wx.StaticText(sbSizer2.GetStaticBox(), wx.ID_ANY, u"name:", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText8.Wrap(-1)
        fgSizer3.Add(self.m_staticText8, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_name = wx.TextCtrl(sbSizer2.GetStaticBox(), wx.ID_ANY, u"server", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer3.Add(self.m_name, 0, wx.ALL | wx.EXPAND, 5)

        fgSizer3.Add((20, 0), 1, wx.EXPAND, 5)

        self.m_staticText3 = wx.StaticText(sbSizer2.GetStaticBox(), wx.ID_ANY, u"server listener port:",
                                           wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        fgSizer3.Add(self.m_staticText3, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_port = wx.TextCtrl(sbSizer2.GetStaticBox(), wx.ID_ANY, u"8080", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer3.Add(self.m_port, 0, wx.ALL, 5)

        fgSizer3.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_staticText9 = wx.StaticText(sbSizer2.GetStaticBox(), wx.ID_ANY, u"namespace:", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer3.Add(self.m_staticText9, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_namespace = wx.TextCtrl(sbSizer2.GetStaticBox(), wx.ID_ANY, u"rpc", wx.DefaultPosition, wx.DefaultSize,
                                       0)
        fgSizer3.Add(self.m_namespace, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer3.Add((0, 0), 1, wx.EXPAND, 5)

        fgSizer2.Add(fgSizer3, 1, wx.EXPAND, 5)

        self.m_checkBox1 = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u"show other projects", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        self.m_checkBox1.Enable(False)
        self.m_checkBox1.Hide()

        fgSizer2.Add(self.m_checkBox1, 0, wx.ALL, 5)

        sbSizer2.Add(fgSizer2, 1, wx.EXPAND | wx.TOP, 5)

        fgSizer11.Add(sbSizer2, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer8.Add(fgSizer11, 1, wx.EXPAND | wx.TOP, 5)

        fgSizer43 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer43.AddGrowableCol(0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap(wx.ART_TIP, wx.ART_BUTTON),
                                      wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        fgSizer43.Add(self.m_info, 0, wx.ALL, 5)

        self.m_button8 = wx.Button(self, wx.ID_CANCEL, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer43.Add(self.m_button8, 0, wx.ALL, 5)

        self.m_button7 = wx.Button(self, wx.ID_OK, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_button7.SetDefault()
        fgSizer43.Add(self.m_button7, 0, wx.ALL, 5)

        fgSizer8.Add(fgSizer43, 1, wx.EXPAND, 5)

        self.SetSizer(fgSizer8)
        self.Layout()

        # Connect Events
        self.m_port.Bind(wx.EVT_CHAR, self.OnPortChar)
        self.m_checkBox1.Bind(wx.EVT_CHECKBOX, self.on_show_other_projects)
        self.m_button8.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__( self ):
        pass


    # Virtual event handlers, overide them in your derived class
    def OnPortChar( self, event ):
        event.Skip()

    def on_cancel( self, event ):
        event.Skip()

    def on_ok( self, event ):
        event.Skip()
    

