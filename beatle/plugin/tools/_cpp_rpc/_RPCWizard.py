"""
RPCWizard : contains the wizard for creating servers of clients through c++rpc
"""
import copy
import wx
from . import ui

from beatle import model, lib, local_path
from beatle.model import cc
from beatle.lib import wxx
from beatle.lib.api import context
from beatle.lib.handlers import identifier
from beatle.lib.tran import TransactionalMethod
from beatle.activity.models.ui.view import ModelsView
from beatle.model.cc import CCProject


from .model import RPCClient, RPCServer, RPCClassServer
EXPORTABLE_MEMBER_METHOD_CLASSES = (cc.MemberMethod, cc.Constructor, cc.Destructor, cc.IsClassMethod, cc.SetterMethod,
                                    cc.GetterMethod)


def static(kwargs):
    kwargs['static'] = True
    return kwargs


def public(kwargs):
    kwargs['access'] = 'public'
    return kwargs


def read_only(kwargs):
    kwargs['read_only'] = True
    return kwargs


def static_readonly_method(**kwargs):
    return cc.MemberMethod(**static(read_only(kwargs)))


def static_method(**kwargs):
    return cc.MemberMethod(**static(read_only(kwargs)))


# ID_RPC_SERVER_NEW = identifier("ID_RPC_SERVER_NEW")
# ID_RPC_CLIENT_NEW = identifier("ID_RPC_CLIENT_NEW")

#
# class RPCWizard(wx.EvtHandler):
#     """Class for providing rpclib wizard"""
#     instance = None
#
#     def __init__(self):
#         """Initialize ast explorer"""
#         super(RPCWizard, self).__init__()
#         self._cpp_rpc = identifier('ID_CPP_RPC_WIZARD')
#         self._rpc_wizard_menu = None
#         self._rpc_wizard_menu_new_server = None
#         self._rpc_wizard_menu_new_client = None
#         self._rpc_wizard_submenu = None
#
#         # register custom module classes
#         CCProject.module_classes.append(RPCClient)
#         CCProject.module_classes.append(RPCServer)
#         CCProject.class_overrides.append(RPCClassServer)
#         ModelsView.extend_tree_order([RPCServer, RPCClassServer], cc.TypesFolder)
#
#         ModelsView.alias_map[RPCClient] = 'rpc client configuration'
#         ModelsView.alias_map[RPCServer] = 'rpc server configuration'
#         ModelsView.alias_map[RPCClassServer] = 'rpc server class interface'
#         # to do
#         # ModelsView._edit_map.update({
#         #     self.open_rpc_server: [RPCServer]})
#         # ModelsView._popup_map.update({
#         #     self.on_rpc_server_popup: [RPCServer]
#         # })
#         # ModelsView._handlers.append(self)
#
#         # old
#         self._published_class_list = []
#         self.append_tools_menu()
#         self.bind_events()
#
#         self._imported_class_map = {}
#         self._project = None
#         self._config = None
#         self._cpp_rpc_menu = None
#         self._rpc_namespace = None
#         self._import_project_folder = None
#         self._marshal = None
#         self._marshal_exports_folder = None
#         self._published_methods_map = {}
#         self.rpc_object_index = None
#         self._marshal_class_folder = None
#         self._marshal_conversion_folder = None
#         self._import_module = None
#
#     def append_tools_menu(self):
#         # import beatle.app.resources as rc
#         frame = context.get_frame()
#         bmp = wx.Bitmap(local_path("plugin/tools/_cpp_rpc/res/cpprpc.xpm"), wx.BITMAP_TYPE_ANY)
#         bmpserver = wx.Bitmap(local_path("plugin/tools/_cpp_rpc/res/cpprpcserver.xpm"), wx.BITMAP_TYPE_ANY)
#         bmpclient = wx.Bitmap(local_path("plugin/tools/_cpp_rpc/res/cpprpcclient.xpm"), wx.BITMAP_TYPE_ANY)
#
#         frame.add_tools_separator()
#         self._rpc_wizard_menu_new_server = wxx.MenuItem(
#             frame.menuTools, ID_RPC_SERVER_NEW, u"c++ rpc server",
#             u"add c++ rpc server.", wx.ITEM_NORMAL)
#         self._rpc_wizard_menu_new_server.SetBitmap(bmpserver)
#         frame.add_tools_menu('c++ rpc server', self._rpc_wizard_menu_new_server)
#
#         self._rpc_wizard_menu_new_client = wxx.MenuItem(
#             frame.menuTools, ID_RPC_CLIENT_NEW, u"c++ rpc client",
#             u"add c++ rpc client.", wx.ITEM_NORMAL)
#         self._rpc_wizard_menu_new_client.SetBitmap(bmpclient)
#         frame.add_tools_menu('c++ rpc client', self._rpc_wizard_menu_new_client)
#
#     def bind_events(self):
#         self.Bind(wx.EVT_UPDATE_UI, self.on_update_rpc_wizard, id=self._cpp_rpc)
#         self.Bind(wx.EVT_UPDATE_UI, self.on_update_rpc_wizard, id=ID_RPC_SERVER_NEW)
#         self.Bind(wx.EVT_UPDATE_UI, self.on_update_rpc_wizard, id=ID_RPC_CLIENT_NEW)
#         self.Bind(wx.EVT_MENU, self.on_rpc_server, id=ID_RPC_SERVER_NEW)
#         self.Bind(wx.EVT_MENU, self.on_rpc_client, id=ID_RPC_CLIENT_NEW)
#
#     @TransactionalMethod('add c++ rpc server')
#     @wxx.CreationDialog(ui.RPCServerDialog, RPCServer)
#     def on_rpc_server(self, event):
#         frame = context.get_frame()
#         self._project = None
#         try:
#             selected = frame.get_current_view().selected
#             if selected and selected.project.language == 'c++':
#                 return frame, selected.project
#         except Exception as e:
#             pass
#         return False
#
#     @TransactionalMethod('add c++ rpc client')
#     @wxx.CreationDialog(ui.RPCClientDialog, RPCClient)
#     def on_rpc_client(self, event):
#         frame = context.get_frame()
#         self._project = None
#         try:
#             selected = frame.get_current_view().selected
#             if selected and selected.project.language == 'c++':
#                 return frame, selected.project
#         except Exception as e:
#             pass
#         return False
#
#     @classmethod
#     def add_resources(cls):
#         """The mission of this method is to add custom xpm resources to the tree"""
#         pass
#
#     @classmethod
#     def load(cls):
#         """Setup tool for the environment"""
#         if cls.instance is None:
#             cls.instance = RPCWizard()
#         return cls.instance
#
#     def on_update_rpc_wizard(self, event):
#         """Handle the update of menu enable/disable status"""
#         # get the view book
#         frame = context.get_frame()
#         self._project = None
#         book = frame.viewBook
#         try:
#             view = book.GetCurrentPage()
#             if view is not None:
#                 # get the selected item
#                 selected = view.selected
#                 if selected is not None:
#                     project = getattr(selected, 'project', None)
#                     if project and project.language == 'c++':
#                         self._project = project
#         except Exception as e:
#             print(" some exception happen in rpc wizard:{}".format(e))
#         event.Enable(bool(self._project))
#
#     # Este es el viejo metodo que funcionaba
#     # def on_rpc_wizard_disabled(self, event):
#     #     """Handle the command"""
#     #     frame = context.get_frame()
#     #     dlg = WizardDialog(frame, self._project)
#     #     dlg.SetPageSize(wx.Size(600, 400))
#     #     result = dlg.RunWizard(dlg.m_wizPage1)
#     #     if result:
#     #         self._config = dlg.config
#     #         if self._config.parent is None:
#     #             self._project.add_child(self._config)
#     #         if dlg.mode == 'server':
#     #             self.create_server_exports()
#     #         if dlg.mode == 'client':
#     #             self.create_client_imports()
#     #         if dlg.mode == 'clean':
#     #             self.clean_rpc_info()
#     #     dlg.Destroy()
#
#     @TransactionalMethod('c++ rpc server exports')
#     def create_server_exports(self):
#         """Apply export through rpclib. See http://rpclib.net/"""
#         self._project.save_state()
#         self.create_server_folder()
#         self.create_export_marshal()
#         self.export_classes()
#         self.export_methods()
#         self.create_export_bindings()
#         self.create_export_run()
#         return True
#
#     @TransactionalMethod('c++ rpc client imports')
#     def create_client_imports(self):
#         self._project.save_state()
#         self.create_client_folder()
#         for project in self.imported_projects:
#             if not self.create_project_imports(project):
#                 return False
#         return True
#
#     @TransactionalMethod('c++ rpc clean')
#     def clean_rpc_info(self):
#         self._project.save_state()
#         # ------
#         # server
#         # ------
#         if getattr(self, '_marshal_exports_folder', False):
#             self._marshal_exports_folder.delete()
#             del self._marshal_exports_folder
#
#         classes = self._project(cc.Class)
#         for cls in classes:
#             if 'cpp_rpc' in cls.plugin:
#                 cls.save_state()
#                 cls.plugin['cpp_rpc'] = {}
#         self._published_class_list = []
#         if getattr(self, "_marshal", False):
#             self._marshal.delete()
#             del self._marshal
#         self._config.remove_folder()
#         # ------
#         # client
#         # ------
#         self._config.remove_folder_client()
#
#     def create_server_folder(self):
#         self._config.remove_folder()
#         self._config.folder = cc.Folder(
#             parent=self._project,
#             read_only=True,
#             name='rpc server')
#
#     def create_client_folder(self):
#         self._config.remove_folder_client()
#         self._config.folder_client = cc.Folder(
#             parent=self._project,
#             read_only=True,
#             name='rpc client')
#
#     def create_project_imports(self, project):
#         """create the project import structure"""
#         self.create_import_project_folder(project)
#         self.create_import_marshal(project)
#         self.import_classes(project)
#         self.import_methods(project)
#         return True
#
#     def create_import_project_folder(self, project):
#         self._import_project_folder = cc.Folder(
#             parent=self._config.folder_client,
#             name='imports from {}'.format(project.name))
#
#     def create_export_marshal(self):
#         self._marshal = cc.Class(parent=self._config.folder,
#                                        name='librpc_toolkit', is_struct=True, read_only=True)
#         # singleton
#         cc.Constructor(parent=self._marshal ,access='private', read_only=True)
#         cc.Destructor(parent=self._marshal ,access='private', read_only=True)
#         # map include
#         self._marshal.user_code_h1 = '#include <map>\n'
#         self._marshal.user_code_h1 += '#include "c++.rpc/server.h"\n'
#         self._marshal.user_code_h1 += '#include "c++.rpc/this_handler.h"\n'
#         self._marshal.user_code_h1 += '#include "c++.rpc/this_session.h"\n'
#         # instances storage (hardcoded by now for fast)
#         self._marshal.user_code_h2 = '\tstatic std::map<size_t,void*> _real_object_map;\n'
#         self._marshal.user_code_h2 += '\tstatic rpc::server _server;\n'
#         self._marshal.user_code_s2 = 'std::map<size_t,void*> librpc_toolkit::_real_object_map;\n'
#         self._marshal.user_code_s2 += 'rpc::server  librpc_toolkit::_server({port});'.format(
#             port=self._config.port)
#         self._published_methods_map = {}  # uid -> method
#
#     def create_import_marshal(self, project):
#         self._marshal = cc.Class(
#             parent= self._import_project_folder,
#             name='remote_{}'.format(project.name))
#         # singleton
#         cc.Constructor(parent=self._marshal, access='private')
#         cc.Destructor(parent=self._marshal, access='private')
#         self._marshal.user_code_h1 = '#include "c++.rpc/client.h"'
#         # the class holds a static member rpc::client*
#         # that will be setup later
#         self._marshal.user_code_h2 = """\tstatic rpc::client* _connector;\n\tstatic bool _in_call;\n"""
#         self._marshal.user_code_s1 = '#include <iostream>'
#         self._marshal.user_code_s2 = """//initialization of connection
# rpc::client* remote_{project}::_connector = nullptr;
# bool remote_{project}::_in_call = false;\n""".format(project=project.name)
#
#         # bool connect(const char* address)
#         port = project[RPC][0].port  # puerto configurado en el servidor
#         connect_method = static_readonly_method(
#             parent = self._marshal,
#             type=self.type('bool'),
#             name='connect',
#             content= self.code_import_marshal_connect.format(project=project.name, port=port))
#         # el conector acepta un string como direccion del servidor
#         cc.Argument(parent=connect_method,
#                           type=self.type('char', ptr=True, const=True),
#                           name='address')
#
#         # bool disconnect()
#         static_readonly_method(
#             parent = self._marshal,
#             type=self.type('bool'),
#             name='disconnect',
#             content= self.code_import_marshal_disconnect.format(project=project.name))
#
#     def export_classes(self):
#         self._published_class_list = []
#         for cls in self.sorted_exported_classes:
#             self.annotate_exported_class(cls)
#             self.create_export_marshal_class_folders(cls)
#             self.create_export_marshal_class_accessor(cls)
#             self.export_class_constructors(cls)
#             self.export_class_methods(cls)
#             self.export_class_destructor(cls)
#
#     def import_classes(self, project):
#         self._imported_class_map = {}  # foreign class -> local class
#         # Observation : We need to split the import process in two
#         # loops, because we need to capture exported types first
#         for cls in self.sorted_imported_classes(project):
#             self.create_import_class(cls)
#         for cls in self.sorted_imported_classes(project):
#             self.create_import_members(cls)
#             self.create_import_constructors(cls)
#             self.create_import_class_methods(project ,cls)
#             self.create_import_destructor(cls)
#
#     def export_methods(self):
#         methods = self.exported_methods
#         if len(methods) == 0:
#             return
#         self.create_export_marshal_global_folder()
#         for method in self.exported_methods:
#             type_instance = method.type_instance
#             if type_instance.type in self.exported_classes:
#                 type_instance = self.type(type_instance.type)
#
#             # the export class must have one static member
#             interface_method = self.create_export_marshal_method(
#                 type=type_instance,
#                 name='{method}'.format(method=method.name),
#                 content=self.code_export_marshal_method(method)
#             )
#             for arg in method[cc.Argument]:
#                 cc.Argument(
#                     parent=interface_method,
#                     name=arg.name,
#                     type=arg.type_instance)
#             self._published_methods_map[str(method.uid) ] =interface_method
#
#     def import_methods(self, project):
#         for method in self.imported_methods(project):
#             imported_method = cc.Function(
#                 parent=self.register_imported_parent(method),
#                 name=method.name,
#                 read_only=False,
#                 type=self.local_type(method.type_instance ,ref=False ,const=False))
#             arguments = method[cc.Argument]
#             for arg in arguments:
#                 cc.Argument(parent=imported_method,
#                                   type=self.local_type(arg.type_instance),
#                                   name=arg.name)
#             arg_list = ['"{}"'.format(str(method.uid)) ] +[x.name for x in arguments]
#             imported_method._content = self.code_import_method(method).format(
#                 project= project.name,
#                 arg_list=','.join(arg_list),
#                 typename=str(imported_method.type_instance).format(''))
#
#     def create_export_bindings(self):
#         content = '     ' +'\n    '.join('_server.bind("{key}",&librpc_toolkit::{name});'.format(
#             key=x, name=self._published_methods_map[x].name) for x in self._published_methods_map)
#         content += '\n\n    //add exit session binding\n\t_server.bind("close",[](){rpc::this_session().post_exit();});'
#         static_readonly_method(
#             parent=self._marshal,
#             name='bind_all', type=self.type('void'),
#             content = content)
#
#     def create_export_run(self):
#         static_readonly_method(
#             parent=self._marshal,
#             name='run', type=self.type('void'),
#             content='\tlibrpc_toolkit::bind_all();\n\tlibrpc_toolkit::_server.run();\n')
#
#     def create_import_class(self, cls):
#         import_cls = cc.Class(
#             parent=self.register_imported_parent(cls),
#             prefix=cls._memberPrefix,
#             name=cls.name)
#         # hemos de crear las herencias necesarias
#         for base in cls.direct_bases:
#             if base in self._imported_class_map:
#                 cc.Inheritance(parent=import_cls, ancestor=self._imported_class_map[base])
#         # the object reference must be inserted now
#         self.rpc_object_index = cc.MemberData(
#             parent=import_cls, read_only=True,
#             type=self.type('size_t'),
#             name='rpc_object_index')
#
#         cc.Friendship(parent=self._marshal ,target=import_cls)
#         # build  msgpack packed structures
#         imported_members_names = [x.prefixed_name for x in self.imported_class_members(cls)]
#         imported_members_names.insert(0, self.rpc_object_index.prefixed_name)
#         # MSGPACK_DEFINE_ARRAY(<data1>,...<dataN>)
#         pack_list = imported_members_names +['MSGPACK_BASE_ARRAY({})'.format(x.scoped) for x in
#                                               import_cls.direct_bases]
#
#         import_cls.user_code_h1 = '#include "c++.rpc/client.h"'
#         import_cls.user_code_h2 = '\tfriend class {name};\n'.format(name=self._marshal.name)
#         import_cls.user_code_h3 = '\tpublic:\n\tMSGPACK_DEFINE_ARRAY({})'.format(','.join(pack_list))
#         self._imported_class_map[cls] = import_cls
#
#     def annotate_exported_class(self, cls):
#         # construimos el empaquetamiento de msgpack
#         exported_members_names = [x.prefixed_name for x in self.exported_class_members(cls)]
#         exported_members_names.insert(0, '_rpc_object_index')
#         published_bases = [x for x in cls.direct_bases if x in self._published_class_list]
#
#         pack_list = exported_members_names + ['MSGPACK_BASE_ARRAY({})'.format(x.scoped) for x in published_bases]
#         cls.plugin['cpp_rpc'] = {}
#         cls.plugin['cpp_rpc']['declare_before'] = '#include "c++.rpc/server.h"'
#         cls.plugin['cpp_rpc'][
#             'declare_inside_first'] = '\tfriend class ::librpc_toolkit;\n\tsize_t _rpc_object_index=0;\n'
#         cls.plugin['cpp_rpc']['declare_inside_last'] = '\tpublic:\n\tMSGPACK_DEFINE_ARRAY({})\n'.format(
#             ','.join(pack_list))
#         self._published_class_list.append(cls)
#
#     def create_export_marshal_class_folders(self, cls):
#         """Create the folders corresponding to the export of some class:
#             + <class> marshalling
#                 |
#                 + converters <-- hold converters to instances
#                 |
#                 + exports    <-- hold exported methods
#
#             """
#         self._marshal_class_folder = cc.Folder(
#             parent=self._marshal,
#             name='{cls} marshaling'.format(cls=cls.name))
#         self._marshal_conversion_folder = cc.Folder(
#             parent=self._marshal_class_folder,
#             name='converters')
#         self._marshal_exports_folder = cc.Folder(
#             parent=self._marshal_class_folder,
#             name='exports')
#
#     def create_export_marshal_class_accessor(self, cls):
#         """Create a function that converts object reference into real object"""
#         method = static_readonly_method(
#             parent=self._marshal_conversion_folder,
#             type=self.type(cls, ptr=True),
#             name='{cls}_accessor'.format(cls=cls.name),
#             content=self.code_export_marshal_class_accessor.format(cls=cls.scoped))
#         cc.Argument(parent=method, type=self.type('size_t'), name='reference')
#
#     def create_import_members(self, cls):
#         for member in self.imported_class_members(cls):
#             cc.MemberData(
#                 parent=self._imported_class_map[cls], read_only=True,
#                 access=member._access,
#                 type=self.local_type(member.type_instance),
#                 name=member.name)
#
#     def create_import_constructors(self, cls):
#         ctors = self.imported_class_constructors(cls)
#         # create default constructor if missing (msgpack)
#         if len([x for x in ctors if len(x[cc.Argument]) == 0]) == 0:
#             # constructor por defecto requerido por msgpack
#             default_ctor = cc.Constructor(
#                 parent=self._imported_class_map[cls],
#                 access='public', autoargs=False, read_only=True)
#             default_ctor._init = ''
#             default_ctor._content = ''
#
#         imported_members_names = [x.prefixed_name for x in self.imported_class_members(cls)]
#         imported_members_names.insert(0, self.rpc_object_index.prefixed_name)
#
#         for ctor in ctors:
#             imported_ctor = cc.Constructor(
#                 parent=self._imported_class_map[cls],
#                 access='public', autoargs=False, read_only=True)
#             # argumentos
#             arguments = ctor[cc.Argument]
#             for arg in arguments:
#                 cc.Argument(
#                     parent=imported_ctor,
#                     type=self.local_type(arg.type_instance),
#                     name=arg.name)
#             # codigo de constructor
#             arg_list = ','.join(['"{}"'.format(str(ctor.uid))] + [x.name for x in arguments])
#             imported_ctor._content = self.code_import_constructor.format(
#                 project=cls.project.name,
#                 cls=cls.name,
#                 arg_list=arg_list,
#                 name=cls.scoped,
#                 copy_values='\n    '.join('{name} = data.{name};'.format(name=x) for x in imported_members_names))
#
#     def export_class_constructors(self, cls):
#         for ctor in self.exported_class_constructors(cls):
#             # El constructor crea un nuevo objeto con los parametros recibidos
#             # y en caso de que todo vaya bien, se crea un objeto de interfaz, a retornar
#
#             ctor_marshal = self.create_export_marshal_method(
#                 type=self.type(cls, ref=True),
#                 name='new_{name}'.format(name=cls.name),
#                 content=self.code_export_marshal_constructor.format(cls=cls.scoped, call=ctor.call),
#                 note='create new class {name} and interface objects'.format(name=cls.scoped)
#             )
#             # The marshall arguments are the same as of the selected constructor
#             for arg in ctor[cc.Argument]:
#                 cc.Argument(
#                     parent=ctor_marshal,
#                     name=arg.name,
#                     type=arg.type_instance)
#             # ctor_marshal._content=self.code_export_marshal_constructor.format(cls=cls.name,call=ctor.call)
#             self._published_methods_map[str(ctor.uid)] = ctor_marshal
#
#     def create_import_class_methods(self, project, cls):
#         classes = self.imported_classes(project)
#         for method in self.imported_class_methods(cls):
#             imported_method = cc.MemberMethod(
#                 parent=self._imported_class_map[cls], read_only=True,
#                 static=method.static,
#                 name=method.name,
#                 type=self.local_type(method.type_instance, ref=False, const=False))
#             arguments = method[cc.Argument]
#
#             translated_arguments = []
#             for arg in arguments:
#                 cc.Argument(parent=imported_method,
#                                   type=self.local_type(arg.type_instance),
#                                   name=arg.name)
#                 # si el tipo del argumento esta en los exports, debe enviarse su indice remoto
#                 if arg.type_instance.type in classes:
#                     if arg.type_instance.is_ptr:
#                         translated_arguments.append('{name}->_rpc_object_index'.format(name=arg.name))
#                     else:
#                         translated_arguments.append('{name}._rpc_object_index'.format(name=arg.name))
#                 else:
#                     translated_arguments.append(arg.name)
#             if not method.static:
#                 arg_list = ['"{}"'.format(str(method.uid)), '_rpc_object_index'] + translated_arguments
#             else:
#                 arg_list = ['"{}"'.format(str(method.uid)), ] + translated_arguments
#             imported_method._content = self.code_import_member_method(method).format(
#                 project=cls.project.name,
#                 arg_list=','.join(arg_list),
#                 typename=str(imported_method.type_instance).format('')
#             )
#
#     def export_class_methods(self, cls):
#         methods = self.exported_class_methods(cls)
#         # We must check the methods for filter names
#         count_names = {}
#         method_name = {}
#         for method in methods:
#             name = method.name
#             if name in count_names:
#                 count = count_names[name] + 1
#                 method_name[method] = '{0}_{1}'.format(name, count)
#                 count_names[name] = count
#             else:
#                 method_name[method] = name
#                 count_names[name] = 1
#         for method in self.exported_class_methods(cls):
#             type_instance = method.type_instance
#             # special case : if the return type is an exported class
#             #                we need to do a mapping.
#             #                This case we must always return the local interface
#             #                even if it's a pointer.
#             if type_instance.type in self.exported_classes:
#                 type_instance = self.type(type_instance.type)
#
#             # do a static method in the export class
#             interface_method = self.create_export_marshal_method(
#                 type=type_instance,
#                 name='{cls}_{method}'.format(cls=cls.name, method=method_name[method]),
#                 content=self.code_export_marshal_class_method(cls, method)
#             )
#             if not method.static:
#                 cc.Argument(
#                     parent=interface_method,
#                     name='rpc_object_index',
#                     type=self.type('size_t'))
#             classes = self.exported_classes
#             for arg in method[cc.Argument]:
#                 if arg.type_instance.type in classes:
#                     cc.Argument(
#                         parent=interface_method,
#                         name='{name}_rpc_object_index'.format(name=arg.name),
#                         type=self.type('size_t'))
#                 else:
#                     cc.Argument(
#                         parent=interface_method,
#                         name=arg.name,
#                         type=arg.type_instance)
#             self._published_methods_map[str(method.uid)] = interface_method
#
#     def create_import_destructor(self, cls):
#         dtors = [x for x in self._config.imports
#                  if type(x) is cc.Destructor and x.inner_class is cls]
#         if len(dtors) == 1:
#             cc.Destructor(
#                 parent=self._imported_class_map[cls], read_only=True,
#                 content=self.code_import_destructor.format(project=cls.project.name, key=str(dtors[0].uid)))
#
#     def export_class_destructor(self, cls):
#         dtor_methods = [x for x in self._config._exports if type(x) is cc.Destructor and x.inner_class is cls]
#         if len(dtor_methods) == 1:
#             dtor_marshal = self.create_export_marshal_method(
#                 type=self.type('bool'),
#                 name='delete_{name}'.format(name=cls.name),
#                 content=self.code_export_destructor.format(scoped=cls.scoped, cls=cls.name))
#             cc.Argument(
#                 parent=dtor_marshal,
#                 name='rpc_object_index',
#                 type=self.type('size_t'))
#             self._published_methods_map[str(dtor_methods[0].uid)] = dtor_marshal
#
#     def create_export_marshal_global_folder(self):
#         """Create the folder for exported global methods"""
#         self._marshal_exports_folder = cc.Folder(
#             parent=self._marshal,
#             name='exported methods')
#
#     def register_imported_parent(self, element):
#         if element.parent.inner_class in self._imported_class_map:
#             return self._imported_class_map[element.parent.inner_class]
#         if type(element) is cc.Function:
#             if getattr(self, "_import_module", None) is None:
#                 self._import_module = cc.Module(
#                     parent=self._import_project_folder,
#                     name='rpc_imported_functions',
#                     header='rpc_imported_functions.h',
#                     source='rpc_imported_functions.cpp')
#             return self._import_module
#         ns = element.inner_namespace
#         if ns is not None:
#             namespaces = self._import_project_folder(cc.Namespace, filter=lambda x: x.name == ns.name)
#             if len(namespaces) > 0:
#                 return namespaces[0]
#             return cc.Namespace(parent=self._import_project_folder, name=ns.name)
#         return self._import_project_folder
#
#     def type(self, rtype, **kwargs):
#         if type(rtype) is str:
#             _type = lib.utils.cached_type(self._project, rtype)
#         else:
#             _type = rtype
#         return cc.typeinst(type=_type, **kwargs)
#
#     def local_type(self, type_instance, **overrides):
#         """Return a type instance belonging to the project that represents
#         the foreign type_instance provided as argument"""
#         assert (type(type_instance) is cc.typeinst)
#         kwargs = type_instance.get_kwargs()
#         typename = type_instance.type.scoped
#         del kwargs['type']
#         for key in overrides:
#             kwargs[key] = overrides[key]
#         return self.type(typename, **kwargs)
#
#     @property
#     def imported_projects(self):
#         """return the list of imported projects"""
#         return [x for x in self._config.imports if type(x) is model.Project]
#
#     @property
#     def exported_classes(self):
#         """return the list of exported classes"""
#         return [x for x in self._config._exports if type(x) is cc.Class]
#
#     def imported_classes(self, project):
#         """return the list of imported classes"""
#         return [x for x in self._config.imports if type(x) is cc.Class and x.project == project]
#
#     @property
#     def sorted_exported_classes(self):
#         """return the list of exported classes ordered by no dependence of next classes"""
#         result = []
#         pending_classes = copy.copy(self.exported_classes)
#         while len(pending_classes) > 0:
#             cls = None
#             for candidate in pending_classes:
#                 if len([x for x in candidate.direct_bases if x in pending_classes]) == 0:
#                     if candidate.parent.inner_class not in pending_classes:
#                         cls = candidate
#                         break
#             if cls is None:
#                 raise RuntimeError("couldn't export circular dependency!")
#             pending_classes.remove(cls)
#             result.append(cls)
#         return result
#
#     def sorted_imported_classes(self, project):
#         """return the list of imported classes ordered by no dependence of next classes"""
#         result = []
#         pending_classes = copy.copy(self.imported_classes(project))
#         while len(pending_classes) > 0:
#             cls = None
#             for candidate in pending_classes:
#                 if len([x for x in candidate.direct_bases if x in pending_classes]) == 0:
#                     if candidate.parent.inner_class not in pending_classes:
#                         cls = candidate
#                         break
#
#             if cls is None:
#                 raise RuntimeError("couldn't import circular dependency!")
#             pending_classes.remove(cls)  # mark as procesed for next loop
#             result.append(cls)
#         return result
#
#     @property
#     def exported_methods(self):
#         return [x for x in self._config._exports if type(x) is cc.Function]
#
#     def imported_methods(self, project):
#         return [x for x in self._config._imports if type(x) is cc.Function and x.project is project]
#
#     def imported_class_methods(self, cls):
#         return [x for x in self._config._imports if type(x) in [
#             cc.MemberMethod,
#             cc.GetterMethod,
#             cc.SetterMethod,
#             cc.IsClassMethod] and x.inner_class is cls]
#
#     def exported_class_members(self, cls):
#         return [x for x in self._config._exports
#                 if type(x) is cc.MemberData and x.inner_class is cls]
#
#     def imported_class_members(self, cls):
#         return [x for x in self._config._imports
#                 if type(x) is cc.MemberData and x.inner_class is cls]
#
#     def exported_class_constructors(self, cls):
#         return [x for x in self._config._exports if type(x) is
#                 cc.Constructor and x.inner_class is cls]
#
#     def imported_class_constructors(self, cls):
#         return [x for x in self._config._imports
#                 if type(x) is cc.Constructor and x.inner_class is cls]
#
#     def exported_class_methods(self, cls):
#         """return the list of exported methods, not includding ctors nor dtors"""
#         return [x for x in self._config._exports
#                 if type(x) in [cc.MemberMethod, cc.GetterMethod,
#                                cc.SetterMethod, cc.IsClassMethod]
#                 and x.inner_class is cls]
#
#     def create_export_marshal_method(self, **kwargs):
#         return static_readonly_method(
#             parent=self._marshal_exports_folder, access='protected', **kwargs)
#
#     def code_export_marshal_class_method(self, cls, method):
#         # TO DO
#         # we must translate all the arguments that belongs to exported
#         # types, in order to handle his translation accordingly
#         # ... insert a {translate_argument} part inside all code formats
#
#         type_instance = method.type_instance
#         # caso especial: si el tipo retornado corresponde a una clase
#         #                exportada, ha de realizarse un mapping.
#         #                En este caso siempre se retornara el objeto de
#         #                interfaz, aunque la intancia de tipo sea puntero
#         if type_instance._type in self.exported_classes:
#             reponse_type = type_instance._type
#             if type_instance.is_ptr:
#                 if method.static:
#                     return self.code_export_marshal_managed_static_ptr_class_method.format(
#                         cls=cls.name, reponse_cls=reponse_type.name, call=method.call)
#                 else:
#                     return self.code_export_marshal_managed_ptr_class_method.format(
#                         cls=cls.name, reponse_cls=reponse_type.name, call=method.call)
#             elif type_instance.is_ref:
#                 if method.static:
#                     return self.code_export_marshal_managed_static_reference_class_method.format(
#                         cls=cls.name, reponse_cls=reponse_type.name, call=method.call)
#                 else:
#                     return self.code_export_marshal_managed_reference_class_method.format(
#                         cls=cls.name, reponse_cls=reponse_type.name, call=method.call)
#             else:
#                 if method.static:
#                     return self.code_export_marshal_managed_static_temporary_class_method.format(
#                         cls=cls.name, reponse_cls=reponse_type.name, call=method.call)
#                 else:
#                     return self.code_export_marshal_temporary_class_method.format(
#                         cls=cls.name, reponse_cls=reponse_type.name, call=method.call)
#         elif type_instance.is_ptr or type_instance.type_name != 'void':
#             return 'return librpc_toolkit::{cls}_accessor(rpc_object_index)->{call};'.format(
#                 cls=cls.name, call=method.call)
#         else:
#             return 'librpc_toolkit::{cls}_accessor(rpc_object_index)->{call};'.format(
#                 cls=cls.name, call=method.call)
#
#     def code_export_marshal_method(self, method):
#         type_instance = method.type_instance
#         # caso especial: si el tipo retornado corresponde a una clase
#         #                exportada, ha de realizarse un mapping.
#         #                En este caso siempre se retornara el objeto de
#         #                interfaz, aunque la intancia de tipo sea puntero
#         if type_instance._type in self.exported_classes:
#             if type_instance.is_ptr:
#                 return self.code_export_marshal_managed_ptr_method.format(call=method.call)
#             elif type_instance.is_ref:
#                 return self.code_export_marshal_managed_reference_method.format(call=method.call)
#             else:
#                 return self.code_export_marshal_temporary_method.format(call=method.call)
#         elif type_instance.is_ptr or type_instance.type_name != 'void':
#             return 'return ::{call};'.format(call=method.call)
#         else:
#             return '{call};'.format(call=method.call)
#
#     @staticmethod
#     def code_import_member_method(method):
#         if method.type_instance.is_void:
#             return """
#     if ( remote_{project}::_connector == nullptr )
#     {{
#         throw "remote {project} is not connected";
#     }}
#     //prevent back synchronization
#     if ( remote_{project}::_in_call )
#     {{
#         return;
#     }}
#     remote_{project}::_in_call = true;
#     remote_{project}::_connector->call({arg_list});
#     remote_{project}::_in_call = false;"""
#         else:
#             return """
#     if ( remote_{project}::_connector == nullptr )
#     {{
#         throw "remote {project} is not connected";
#     }}
#     if ( remote_{project}::_in_call )
#     {{
#         throw "reentrant call detected!!!";
#     }}
#     remote_{project}::_in_call = true;
#     {typename} result = remote_{project}::_connector->call({arg_list}).as<{typename}>();
#     remote_{project}::_in_call = false;
#     return result;"""
#
#     @staticmethod
#     def code_import_method(method):
#         if method.type_instance.is_void:
#             return """
#     if ( remote_{project}::_connector == nullptr )
#     {{
#         throw "remote {project} is not connected";
#     }}
#     //prevent back synchronization
#     if ( remote_{project}::_in_call )
#     {{
#         return;
#     }}
#     remote_{project}::_in_call = true;
#     remote_{project}::_connector->call({arg_list});
#     remote_{project}::_in_call = false;"""
#         else:
#             return """
#     if ( remote_{project}::_connector == nullptr )
#     {{
#         throw "remote {project} is not connected";
#     }}
#     if ( remote_{project}::_in_call )
#     {{
#         throw "reentrant call detected!!!";
#     }}
#     remote_{project}::_in_call = true;
#     {typename} result = remote_{project}::_connector->call({arg_list}).as<{typename}>();
#     remote_{project}::_in_call = false;
#     return result;"""
#
#     @property
#     def code_export_marshal_class_accessor(self):
#         return """
#     auto accessor=librpc_toolkit::_real_object_map.find(reference);
#     if ( accessor == librpc_toolkit::_real_object_map.end() )
#     {{
#         return nullptr;
#     }}
#     return reinterpret_cast<{cls}*>(accessor->second);"""
#
#     @property
#     def code_export_marshal_constructor(self):
#         return """
# {cls}* object = new {call};
# if ( object == nullptr )
# {{
#     rpc::this_handler().respond_error("allocation failed");
# }}
# //ok, create unique identifier
# size_t uid_local = reinterpret_cast<size_t>(object);
# librpc_toolkit::_real_object_map[ uid_local ] = object;
#
# //setup reference and update interfaz
# object->_rpc_object_index = uid_local;
#
# return *object;
# """
#
#     @property
#     def code_export_marshal_managed_static_ptr_class_method(self):
#         return """
# auto response_object = {cls}::{call};
# if( response_object == nullptr )
# {{
#     rpc::this_handler().respond_error("nullptr returned.");
# }}
# size_t uid_local = reinterpret_cast<size_t>(response_object);
# if( librpc_toolkit::_real_object_map.find(uid_local) == librpc_toolkit::_real_object_map.end() )
# {{
#     librpc_toolkit::_real_object_map[ uid_local ] = response_object;
#     response_object->_rpc_object_index = uid_local;
# }}
# return *response_object;
# """
#
#     @property
#     def code_export_marshal_managed_ptr_method(self):
#         return """
# auto response_object = {call};
# if( response_object == nullptr )
# {{
#     rpc::this_handler().respond_error("nullptr returned.");
# }}
# size_t uid_local = reinterpret_cast<size_t>(response_object);
# if( librpc_toolkit::_real_object_map.find(uid_local) == librpc_toolkit::_real_object_map.end() )
# {{
#     librpc_toolkit::_real_object_map[ uid_local ] = response_object;
#     response_object->_rpc_object_index = uid_local;
# }}
# return *response_object;
# """
#
#     @property
#     def code_export_marshal_managed_ptr_class_method(self):
#         return """
# auto object = librpc_toolkit::{cls}_accessor(rpc_object_index);
# if( object == nullptr )
# {{
#     rpc::this_handler().respond_error("invalid object reference.");
# }}
# auto response_object = object->{call};
# if( response_object == nullptr )
# {{
#     rpc::this_handler().respond_error("nullptr returned.");
# }}
# size_t uid_local = reinterpret_cast<size_t>(response_object);
# if( librpc_toolkit::_real_object_map.find(uid_local) == librpc_toolkit::_real_object_map.end() )
# {{
#     librpc_toolkit::_real_object_map[ uid_local ] = response_object;
#     response_object->_rpc_object_index = uid_local;
# }}
# return *response_object;
# """
#
#     @property
#     def code_export_marshal_managed_static_reference_class_method(self):
#         return """
# auto& response_object = {cls}::{call};
# size_t uid_local = reinterpret_cast<size_t>(&response_object);
# if( librpc_toolkit::_real_object_map.find(uid_local) == librpc_toolkit::_real_object_map.end() )
# {{
#     librpc_toolkit::_real_object_map[ uid_local ] = &response_object;
#     response_object._rpc_object_index = uid_local;
# }}
# return response_object;
# """
#
#     @property
#     def code_export_marshal_managed_reference_method(self):
#         return """
# auto& response_object = {call};
# size_t uid_local = reinterpret_cast<size_t>(&response_object);
# if( librpc_toolkit::_real_object_map.find(uid_local) == librpc_toolkit::_real_object_map.end() )
# {{
#     librpc_toolkit::_real_object_map[ uid_local ] = &response_object;
#     response_object._rpc_object_index = uid_local;
# }}
# return response_object;
# """
#
#     @property
#     def code_export_marshal_managed_reference_class_method(self):
#         return """
# auto object = librpc_toolkit::{cls}_accessor(rpc_object_index);
# if( object == nullptr )
# {{
#     rpc::this_handler().respond_error("invalid object reference.");
# }}
# auto& response_object = object->{call};
# size_t uid_local = reinterpret_cast<size_t>(&response_object);
# if( librpc_toolkit::_real_object_map.find(uid_local) == librpc_toolkit::_real_object_map.end() )
# {{
#     librpc_toolkit::_real_object_map[ uid_local ] = &response_object;
#     response_object._rpc_object_index = uid_local;
# }}
# return response_object;
# """
#
#     @property
#     def code_export_marshal_managed_static_temporary_class_method(self):
#         return """
# auto response_object = {cls}::{call};
# return response_object;
# """
#
#     @property
#     def code_export_marshal_temporary_method(self):
#         return """
# auto response_object = {call};
# return response_object;
# """
#
#     @property
#     def code_export_marshal_temporary_class_method(self):
#         return """
# auto object = librpc_toolkit::{cls}_accessor(rpc_object_index);
# if( object == nullptr )
# {{
#     rpc::this_handler().respond_error("invalid object reference.");
# }}
# auto response_object = object->{call};
# return response_object;
# """
#
#     @property
#     def code_export_destructor(self):
#         return """
# {scoped}* ptr_object = librpc_toolkit::{cls}_accessor( rpc_object_index );
# if ( ptr_object == nullptr )
# {{
#     rpc::this_handler().respond_error("fatal: missing target object with reference.");
#     return false;
# }}
# delete ptr_object;
# librpc_toolkit::_real_object_map.erase( rpc_object_index );
# return true;
# """
#
#     @property
#     def code_import_marshal_connect(self):
#         return """
#     if(remote_{project}::_connector != nullptr )
#     {{
#         std::cerr << "Error: connection to {project} already open." << std::endl;
#         return false;
#     }}
#     remote_{project}::_connector = new rpc::client(address,{port});
#     // TO DO : Adjust desired timeout
#     remote_{project}::_connector->set_timeout(500);
#     return true;
#     """
#
#     @property
#     def code_import_marshal_disconnect(self):
#         return """
#     if(remote_{project}::_connector != nullptr )
#     {{
#         remote_{project}::_connector->call("close");
#         delete remote_{project}::_connector;
#         remote_{project}::_connector = nullptr;
#     }}
#     else
#     {{
#         std::cerr << "Warning: connection to {project} already closed." << std::endl;
#     }}
#     return true;
#     """
#
#     @property
#     def code_import_constructor(self):
#         return """
#     if ( remote_{project}::_connector == nullptr )
#     {{
#         throw "remote {project} is not connected";
#     }}
#     //prevent back synchronization
#     if ( remote_{project}::_in_call )
#     {{
#         return;
#     }}
#     remote_{project}::_in_call = true;
#     auto data = remote_{project}::_connector->call({arg_list}).as<{name}>();
#     {copy_values}
#     //prevent remote destruction
#     data._rpc_object_index=0;
#     remote_{project}::_in_call = false;
#     """
#
#     @property
#     def code_import_destructor(self):
#         return """
#     /** c++11 destructors defaults to noexcept, so blind eyes here!
#     if ( remote_{project}::_connector == nullptr )
#     {{
#         throw "remote {project} is not connected";
#     }}**/
#     if ( remote_{project}::_in_call )
#     {{
#         return;
#     }}
#     remote_{project}::_in_call = true;
#     if(_rpc_object_index!=0)
#     {{
#         remote_{project}::_connector->call("{key}",_rpc_object_index);
#     }}
#     remote_{project}::_in_call = false;
#     """
