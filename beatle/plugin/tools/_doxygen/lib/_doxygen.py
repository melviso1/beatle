# -*- coding: utf-8 -*-

import os, threading, signal, subprocess
import wx
from beatle.lib.wxx import OutputEvent
from beatle.lib.api import context



class doxygen(object):
    """doxygen builder with log"""

    def __init__(self, module):
        """Init"""
        super(doxygen, self).__init__()
        self._process = None
        from shutil import which
        path = which('doxygen')
        if "__WXMSW__" in wx.PlatformInfo:
            if path is None:
                # attempt standard path
                path = r"C:\Program Files\doxygen\bin\doxygen.exe"
                if not os.path.exists(path):
                    path = None
        if path is None:
            with wx.MessageDialog(None, "Can't find doxygen. Is it installed?", "Error") as dialog:
                dialog.ShowModal()
            return
        kwargs = {
            'prolog': 'building doxygen for {}:'.format(module.project.name),
            'cwd': module.project.dir,
            'command': [path, 'doxygen.cfg'],
            'epilog': 'end doxygen build for {}.'.format(module.project.name)
            }
        self.__run__(**kwargs)

    def activate_ui(self):
        """Activate the panes"""
        context.get_frame().clean_output_pane()
        self.activate_output_pane()

    def deactivate_ui(self):
        """Deactivate the ui"""
        frame = context.get_frame()
        frame.m_mgr.Update()

    def activate_output_pane(self):
        """Activate the output pane"""
        frame = context.get_frame()
        if frame.m_aux_panes == frame.m_aux_output_pane:
            return True
        candidate = [wx.NOT_FOUND]
        candidate.extend(i for i in range(0, frame.m_aux_panes.GetPageCount())
                         if frame.m_aux_panes.GetPage(i) == frame.m_aux_output_pane)
        i = max(candidate)
        if i == wx.NOT_FOUND:
            return False
        frame.m_aux_panes.SetSelection(i)
        return True

    def exec_proc(self,**kwargs):
        """funcion que realiza el trabajo en el thread"""
        ended = False
        try:
            command = kwargs['command']
            cwd = kwargs.get('cwd', None)
            if cwd:
                self._process = subprocess.Popen(command,
                    bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False,
                    cwd=cwd, start_new_session=True)
            else:
                self._process = subprocess.Popen(command,
                    bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False,
                    start_new_session=True)
            self.activate_ui()
        except Exception as e:
            handler = context.get_frame().GetEventHandler()
            handler.AddPendingEvent(OutputEvent('execution failed: {}.\n'.format(str(e))))
            return
        handler = context.get_frame().GetEventHandler()
        prolog = kwargs.get('prolog', None)
        if prolog:
            handler.AddPendingEvent(OutputEvent('\n'+prolog+'\n'))
        for line in iter(self._process.stdout.readline, ""):
            line = line.decode('ascii', 'ignore')
            line = line.strip()
            if len(line):
                handler.AddPendingEvent(OutputEvent(line+'\n'))
                self._process.stdout.flush()
                continue
            # wx.YieldIfNeeded()
            if self._process.poll() is not None:
                ended = True
                break
        epilog = kwargs.get('epilog', None)
        if epilog:
            handler.AddPendingEvent(OutputEvent('\n'+epilog+'\n'))
        self._process.stdout.flush()
        # wx.YieldIfNeeded()
        self._process.stdout.close()
        if not ended:
            self._process.wait()
        self._process = None

    def force_stop(self):
        """Brute stop"""
        self.done = True
        if self._process:
            os.killpg(os.getpgid(self._process.pid), signal.SIGTERM)

    def __run__(self, **kwargs):
        """Start a generatio operation"""
        if self._process:
            return  # is already running
        threading.Thread(target=self.exec_proc,kwargs=kwargs).start()

