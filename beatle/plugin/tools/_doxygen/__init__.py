
import os
import wx


from beatle.lib.api import context
from beatle import local_path
from beatle.lib import wxx
from beatle.lib.handlers import identifier
from beatle.lib.tran import TransactionalMethod
from beatle.activity.models.ui.view import ModelsView 
from beatle.model.cc import CCProject 

from .ui import DoxygenDialog, DoxygenEditor
from .model import DoxygenModule
from .res import _doxygen


class DoxygenWizard(wx.EvtHandler):
    """Class for providing doxygen wizard"""
    instance = None

    def __init__(self):
        """Initialize ast explorer"""
        super(DoxygenWizard, self).__init__()
        
        # register custom module class
        CCProject.module_classes.append(DoxygenModule)

        # register global identifiers
        self._editOpen = identifier("ID_EDIT_OPEN")
        self._doxygen_new = identifier("ID_DOXYGEN_NEW")
        self._doxygen_generate = identifier("ID_DOXYGEN_GENERATE")
        self._doxygen_browse = identifier("ID_DOXYGEN_BROWSE")

        self._object = None
        self._doxygen_menu = None
        self._doxygen_menu_new = None
        self._doxygen_menu_generate = None
        self._doxygen_menu_browse = None
        self.project = None
        
        self.append_tools_menu()
        self.bind_events()
        
        #register model
        ModelsView.tree_order.extend([DoxygenModule, ])
        ModelsView.alias_map[DoxygenModule] = 'c++ doxygen configuration'
        ModelsView._edit_map.update({
            self.open_doxygen_wizard : [DoxygenModule]})
        ModelsView._popup_map.update({
            self.on_doxygen_popup: [DoxygenModule]
            })
        
        ModelsView._handlers.append(self)
        
    @property
    def can_edit_open(self):
        frame = context.get_frame()
        view = frame.viewBook.GetCurrentPage()
        if type(view) is ModelsView:
            if type(view.selected) is DoxygenModule:
                return True
        return False
        
    def append_tools_menu(self):
        frame = context.get_frame()
        frame.add_tools_separator()
        bmp = wx.Bitmap(local_path("plugin/tools/_doxygen/res/doxygen.xpm"), wx.BITMAP_TYPE_ANY)
        self._doxygen_menu_new = wxx.MenuItem(frame.menuTools, self._doxygen_new, u"doxygen wizard",
                                              u"configure c++ doxygen documentation generator.", wx.ITEM_NORMAL)
        self._doxygen_menu_generate = wxx.MenuItem(frame.menuTools, self._doxygen_generate, u"doxygen build",
            u"run doxygen for build the c++ documentation.", wx.ITEM_NORMAL)
        self._doxygen_menu_browse = wxx.MenuItem(frame.menuTools, self._doxygen_browse, u"browse doxygen documentation",
            u"browse generated documentation.", wx.ITEM_NORMAL)

        self._doxygen_menu_new.SetBitmap(bmp)
        self._doxygen_menu_generate.SetBitmap(bmp)
        self._doxygen_menu_browse.SetBitmap(bmp)

        frame.add_tools_menu('doxygen wizard', self._doxygen_menu_new)
        frame.add_tools_menu('doxygen build', self._doxygen_menu_generate)
        frame.add_tools_menu('browse doxygen documentation', self._doxygen_menu_browse)

    def bind_events(self):
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_doxygen_wizard, id=self._doxygen_new)
        self.Bind(wx.EVT_MENU, self.on_doxygen_wizard, id=self._doxygen_new)
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_generate_documentation, id=self._doxygen_generate)
        self.Bind(wx.EVT_MENU, self.on_generate_documentation, id=self._doxygen_generate)
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_browse_documentation, id=self._doxygen_browse)
        self.Bind(wx.EVT_MENU, self.on_browse_documentation, id=self._doxygen_browse)

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        from beatle.app.resources import update_xpm_dict
        update_xpm_dict({'doxygen': _doxygen})

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        if cls.instance is None:
            cls.instance = DoxygenWizard()
        return cls.instance
    
    def on_update_doxygen_wizard(self, event):
        """Handle the update of menu enable/disable status"""
        # get the view book
        frame = context.get_frame()
        try:
            project = frame.get_current_view().selected.project
            event.Enable(project.language == 'c++' and len(project[DoxygenModule]) == 0)
        except:
            event.Enable(False)
        
    def on_update_generate_documentation(self, event):
        """Handle the update of menu enable/disable status"""
        frame = context.get_frame()
        try:
            project = frame.get_current_view().selected.project
            event.Enable(len(project[DoxygenModule]) > 0)
        except Exception as e:
            event.Enable(False)
        
    def on_update_browse_documentation(self, event):
        frame = context.get_frame()
        try:
            project = frame.get_current_view().selected.project
            module = project[DoxygenModule][0]
            event.Enable(module._GENERATE_HTML == 'YES')
        except:
            event.Enable(False)
        
    @TransactionalMethod('Add doxygen support')
    @wxx.CreationDialog(DoxygenDialog, DoxygenModule)
    def on_doxygen_wizard(self, event):
        """Do add boost.python interface"""
        frame = context.get_frame()
        self.project = None
        try:
            selected = frame.get_current_view().selected
            if selected and selected.project.language == 'c++':
                return frame, selected
        except Exception as e:
            pass
        return False
        
    def open_doxygen_wizard(self, module, view=None):
        """Handles the edition of the pyBoost wizard as a document"""
        if getattr(module, '_pane', None):
            raise RuntimeError('Already open pane for module')
        frame = context.get_frame()
        pane = DoxygenEditor(frame.docBook, frame, module)
        module._pane = pane
        frame.docBook.AddPage(pane, module.tab_label, True, module.bitmap_index)
        
    def on_generate_documentation(self, event):
        frame = context.get_frame()
        try:
            frame.get_current_view().selected.project[DoxygenModule][0].generate_documentation()
        except Exception as e:
            return 

    def on_doxygen_popup(self, event, obj):
        """Show popup for actions on doxygen module"""
        bmp = wx.Bitmap(local_path("plugin/tools/_doxygen/res/doxygen.xpm"), wx.BITMAP_TYPE_ANY)
        menu = wxx.Menu()
        mi = wxx.MenuItem(menu, self._doxygen_browse, u"Browse documentation.", u"Browse generated documentation.", wx.ITEM_NORMAL)
        mi.SetBitmap(bmp)
        menu.Append(mi)
        menu.AppendSeparator()
        mi = wxx.MenuItem(menu, self._editOpen, u"Open ...", u"Edit doxygen configuration.", wx.ITEM_NORMAL)
        mi.SetBitmap(bmp)
        menu.Append(mi)
        mi = wxx.MenuItem(menu, self._doxygen_generate, u"Generate documentation.", u"Generate doxygen documentation.", wx.ITEM_NORMAL)
        mi.SetBitmap(bmp)
        menu.Append(mi)
        view = context.get_frame().get_current_view()
        menu.Popup(event.GetPosition(), owner=view)

    def on_browse_documentation(self, event):
        frame = context.get_frame()
        project = frame.get_current_view().selected.project
        module = project[DoxygenModule][0]
        if not module._generated:
            if not module.generate_documentation():
                return
        if "__WXMSW__" in wx.PlatformInfo:
            separator = '\\'
        else:
            separator = '/'
        output_path = separator.join(x for x in [module._OUTPUT_DIRECTORY,module._HTML_OUTPUT] if len(x))
        filename = '{0}{1}index{2}'.format(output_path, separator, module._HTML_FILE_EXTENSION)
        if filename[0] != separator:
            filename = os.path.realpath(os.path.join(project.dir, filename))
        if "__WXMSW__" in wx.PlatformInfo:
            url = 'file:///{0}'.format(filename)
        else:
            common = os.path.commonprefix([filename,os.path.expanduser('~')])
            filename = os.path.relpath(filename, common)
            url = 'http://localhost:8901/{0}'.format(filename)
        frame.show_web_url(url, "project documentation")
