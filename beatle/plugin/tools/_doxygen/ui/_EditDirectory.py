"""Subclass of EditDirectoryBase, which is generated by wxFormBuilder."""

import wx
from beatle.lib import wxx
from ._ui import EditDirectoryBase


# Implementing EditDirectoryBase
class EditDirectory(EditDirectoryBase):
	"""
	Using this dialog you can select a directory
	containing sources to be scanned. 
	"""
	@wxx.SetInfo(__doc__)	
	def __init__(self, parent, path='' ):
		self._path = path
		super(EditDirectory, self).__init__(parent)
		self.m_std_buttonsOK.SetDefault()
		if len(self._path) > 0:
			self.SetTitle("Edit input directory")
			self.m_dirPicker3.SetPath(self._path)

	@property
	def path(self):
		return self._path
		
	def validate(self):
		self._path = self.m_dirPicker3.GetPath().strip()
		if len(self._path) > 0:
			return True
		wx.MessageBox("Directory path must be not empty.","Error", wx.ICON_HAND)
		return False
	
	def on_ok(self, event):
		if self.validate():
			self.EndModal(wx.ID_OK)

	def on_cancel(self, event ):
		self.EndModal(wx.ID_CANCEL)
