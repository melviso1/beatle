# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Aug 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.richtext

from beatle.lib import wxx

# special import for beatle development
from beatle.lib.handlers import Identifiers
###########################################################################
## Class DoxygenDialogBase
###########################################################################

class DoxygenDialogBase (wxx.Dialog ):
	
	def __init__(self, parent):
		wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"New doxygen setup", pos = wx.DefaultPosition, size = wx.Size(866,555 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )
		
		self.SetSizeHints(wx.DefaultSize, wx.DefaultSize )
		
		fgSizer53 = wx.FlexGridSizer(2, 1, 0, 0 )
		fgSizer53.AddGrowableCol(0 )
		fgSizer53.AddGrowableRow(0 )
		fgSizer53.SetFlexibleDirection( wx.BOTH )
		fgSizer53.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_choicebook2 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel11 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		fgSizer17 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer17.AddGrowableCol( 0 )
		fgSizer17.AddGrowableRow( 0 )
		fgSizer17.SetFlexibleDirection( wx.BOTH )
		fgSizer17.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer13 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel11, wx.ID_ANY, u"Project" ), wx.VERTICAL )
		
		fgSizer37 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer37.AddGrowableCol( 0 )
		fgSizer37.AddGrowableRow( 1 )
		fgSizer37.SetFlexibleDirection( wx.BOTH )
		fgSizer37.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer38 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer38.AddGrowableCol( 1 )
		fgSizer38.SetFlexibleDirection( wx.BOTH )
		fgSizer38.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText7 = wx.StaticText( sbSizer13.GetStaticBox(), wx.ID_ANY, u"name:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		fgSizer38.Add(self.m_staticText7, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_project_name = wx.TextCtrl( sbSizer13.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		fgSizer38.Add(self.m_project_name, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText8 = wx.StaticText( sbSizer13.GetStaticBox(), wx.ID_ANY, u"version:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText8.Wrap( -1 )
		fgSizer38.Add(self.m_staticText8, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_project_version = wx.TextCtrl( sbSizer13.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer38.Add(self.m_project_version, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer37.Add( fgSizer38, 1, wx.EXPAND, 5 )
		
		sbSizer14 = wx.StaticBoxSizer( wx.StaticBox( sbSizer13.GetStaticBox(), wx.ID_ANY, u"input directories" ), wx.VERTICAL )
		
		fgSizer39 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer39.AddGrowableCol( 0 )
		fgSizer39.AddGrowableRow( 0 )
		fgSizer39.SetFlexibleDirection( wx.BOTH )
		fgSizer39.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer40 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer40.AddGrowableCol( 0 )
		fgSizer40.AddGrowableRow( 0 )
		fgSizer40.SetFlexibleDirection( wx.BOTH )
		fgSizer40.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		m_input_dirsChoices = []
		self.m_input_dirs = wx.ListBox( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_input_dirsChoices, 0 )
		self.m_input_dirs.SetToolTip( u"Double-click for edit directory" )
		
		fgSizer40.Add(self.m_input_dirs, 1, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer41 = wx.FlexGridSizer( 5, 1, 0, 0 )
		fgSizer41.SetFlexibleDirection( wx.BOTH )
		fgSizer41.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_btn_add_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ADD_BOOKMARK, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		fgSizer41.Add(self.m_btn_add_input_dir, 0, wx.ALL, 5 )
		
		self.m_btn_remove_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_btn_remove_input_dir.Enable( False )
		
		fgSizer41.Add(self.m_btn_remove_input_dir, 0, wx.ALL, 5 )
		
		self.m_btn_move_up_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_UP, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_btn_move_up_input_dir.Enable( False )
		
		fgSizer41.Add(self.m_btn_move_up_input_dir, 0, wx.ALL, 5 )
		
		self.m_btn_move_down_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_DOWN, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_btn_move_down_input_dir.Enable( False )
		
		fgSizer41.Add(self.m_btn_move_down_input_dir, 0, wx.ALL, 5 )
		
		
		fgSizer41.Add( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		fgSizer40.Add( fgSizer41, 1, wx.EXPAND|wx.RIGHT, 5 )
		
		
		fgSizer39.Add( fgSizer40, 1, wx.EXPAND, 5 )
		
		
		sbSizer14.Add( fgSizer39, 1, wx.EXPAND, 5 )
		
		self.m_scan_recursive = wx.CheckBox( sbSizer14.GetStaticBox(), wx.ID_ANY, u"scan recursively", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer14.Add(self.m_scan_recursive, 0, wx.ALL, 5 )
		
		
		fgSizer37.Add( sbSizer14, 1, wx.EXPAND|wx.ALL, 5 )
		
		sbSizer19 = wx.StaticBoxSizer( wx.StaticBox( sbSizer13.GetStaticBox(), wx.ID_ANY, u"Output" ), wx.VERTICAL )
		
		fgSizer46 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer46.AddGrowableCol( 1 )
		fgSizer46.SetFlexibleDirection( wx.HORIZONTAL )
		fgSizer46.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText11 = wx.StaticText( sbSizer19.GetStaticBox(), wx.ID_ANY, u"directory:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )
		fgSizer46.Add(self.m_staticText11, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_output_dir = wx.DirPickerCtrl( sbSizer19.GetStaticBox(), wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE|wx.DIRP_USE_TEXTCTRL )
		fgSizer46.Add(self.m_output_dir, 1, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT, 5 )
		
		
		sbSizer19.Add( fgSizer46, 1, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.LEFT, 5 )
		
		
		fgSizer37.Add( sbSizer19, 1, wx.EXPAND|wx.BOTTOM|wx.RIGHT|wx.LEFT, 5 )
		
		
		sbSizer13.Add( fgSizer37, 1, wx.EXPAND|wx.BOTTOM, 5 )
		
		
		fgSizer17.Add( sbSizer13, 1, wx.EXPAND|wx.ALL, 5 )
		
		fgSizer42 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer42.AddGrowableCol( 0 )
		fgSizer42.AddGrowableRow( 0 )
		fgSizer42.AddGrowableRow( 1 )
		fgSizer42.SetFlexibleDirection( wx.BOTH )
		fgSizer42.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer15 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel11, wx.ID_ANY, u"Mode" ), wx.VERTICAL )
		
		fgSizer43 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer43.AddGrowableCol( 0 )
		fgSizer43.AddGrowableRow( 1 )
		fgSizer43.SetFlexibleDirection( wx.BOTH )
		fgSizer43.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_reference_source = wx.CheckBox( sbSizer15.GetStaticBox(), wx.ID_ANY, u"reference source", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_reference_source.SetValue(True) 
		fgSizer43.Add(self.m_reference_source, 0, wx.ALL, 5 )
		
		fgSizer44 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer44.AddGrowableCol( 1 )
		fgSizer44.SetFlexibleDirection( wx.BOTH )
		fgSizer44.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText9 = wx.StaticText( sbSizer15.GetStaticBox(), wx.ID_ANY, u"extraction mode:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )
		fgSizer44.Add(self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		m_extraction_modeChoices = [ u"all entities", u"only comented entities" ]
		self.m_extraction_mode = wx.Choice( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_extraction_modeChoices, 0 )
		self.m_extraction_mode.SetSelection( 0 )
		fgSizer44.Add(self.m_extraction_mode, 1, wx.ALL, 5 )
		
		self.m_staticText10 = wx.StaticText( sbSizer15.GetStaticBox(), wx.ID_ANY, u"optimize for:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText10.Wrap( -1 )
		fgSizer44.Add(self.m_staticText10, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		m_optmize_forChoices = [ u"c++", u"c", u"c#" ]
		self.m_optmize_for = wx.Choice( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_optmize_forChoices, 0 )
		self.m_optmize_for.SetSelection( 0 )
		fgSizer44.Add(self.m_optmize_for, 1, wx.ALL, 5 )
		
		
		fgSizer43.Add( fgSizer44, 1, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		sbSizer15.Add( fgSizer43, 1, wx.EXPAND, 5 )
		
		
		fgSizer42.Add( sbSizer15, 1, wx.EXPAND|wx.TOP|wx.RIGHT, 5 )
		
		sbSizer18 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel11, wx.ID_ANY, u"Diagrams" ), wx.VERTICAL )
		
		self.m_diagram_maker = wx.Choicebook( sbSizer18.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel22 = wx.Panel(self.m_diagram_maker, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_diagram_maker.AddPage(self.m_panel22, u"no diagrams", False )
		self.m_panel23 = wx.Panel(self.m_diagram_maker, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_diagram_maker.AddPage(self.m_panel23, u"builtin diagram generator", False )
		self.m_panel24 = wx.Panel(self.m_diagram_maker, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer45 = wx.FlexGridSizer( 3, 2, 0, 0 )
		fgSizer45.SetFlexibleDirection( wx.BOTH )
		fgSizer45.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_graphwiz_class = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"class", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_class.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_class, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_collaboration = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"collaboration", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_collaboration.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_collaboration, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_includes = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"includes", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_includes.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_includes, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_included_by = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"included by", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_included_by.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_included_by, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_hierarchy = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"hierarchy", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_hierarchy.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_hierarchy, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_call = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"call", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer45.Add(self.m_graphwiz_call, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		
		self.m_panel24.SetSizer( fgSizer45 )
		self.m_panel24.Layout()
		fgSizer45.Fit(self.m_panel24 )
		self.m_diagram_maker.AddPage(self.m_panel24, u"graphwiz dot package", True )
		sbSizer18.Add(self.m_diagram_maker, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer42.Add( sbSizer18, 1, wx.EXPAND|wx.RIGHT, 5 )
		
		
		fgSizer17.Add( fgSizer42, 1, wx.EXPAND, 5 )
		
		
		self.m_panel11.SetSizer( fgSizer17 )
		self.m_panel11.Layout()
		fgSizer17.Fit(self.m_panel11 )
		self.m_choicebook2.AddPage(self.m_panel11, u"basic configuration", True )
		self.m_panel12 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer28 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer28.AddGrowableCol( 0 )
		fgSizer28.AddGrowableRow( 0 )
		fgSizer28.SetFlexibleDirection( wx.BOTH )
		fgSizer28.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_splitter1 = wx.SplitterWindow(self.m_panel12, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
		self.m_splitter1.SetSashGravity( 1 )
		self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )
		
		self.m_panel121 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel13 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer30 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer30.AddGrowableCol( 0 )
		fgSizer30.AddGrowableRow( 0 )
		fgSizer30.SetFlexibleDirection( wx.BOTH )
		fgSizer30.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_textctrl_property_description = wx.richtext.RichTextCtrl(self.m_panel13, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY|wx.HSCROLL|wx.SIMPLE_BORDER|wx.VSCROLL|wx.WANTS_CHARS )
		fgSizer30.Add(self.m_textctrl_property_description, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		
		self.m_panel13.SetSizer( fgSizer30 )
		self.m_panel13.Layout()
		fgSizer30.Fit(self.m_panel13 )
		self.m_splitter1.SplitVertically(self.m_panel121, self.m_panel13, 500 )
		fgSizer28.Add(self.m_splitter1, 1, wx.EXPAND, 5 )
		
		
		self.m_panel12.SetSizer( fgSizer28 )
		self.m_panel12.Layout()
		fgSizer28.Fit(self.m_panel12 )
		self.m_choicebook2.AddPage(self.m_panel12, u"advanced configuration", False )
		fgSizer53.Add(self.m_choicebook2, 1, wx.EXPAND|wx.ALL, 5 )
		
		fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer159.AddGrowableCol( 1 )
		fgSizer159.SetFlexibleDirection( wx.BOTH )
		fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
		fgSizer159.Add(self.m_info, 0, wx.ALL, 5 )
		
		m_std_buttons = wx.StdDialogButtonSizer()
		self.m_std_buttonsOK = wx.Button(self, wx.ID_OK )
		m_std_buttons.AddButton(self.m_std_buttonsOK )
		self.m_std_buttonsCancel = wx.Button(self, wx.ID_CANCEL )
		m_std_buttons.AddButton(self.m_std_buttonsCancel )
		m_std_buttons.Realize();
		
		fgSizer159.Add( m_std_buttons, 1, wx.EXPAND, 5 )
		
		
		fgSizer53.Add( fgSizer159, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( fgSizer53 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_project_name.Bind( wx.EVT_TEXT, self.OnProjectNameChanged )
		self.m_project_version.Bind( wx.EVT_TEXT, self.OnProjectVersionChanged )
		self.m_input_dirs.Bind( wx.EVT_LISTBOX, self.OnSelectInputDirectory )
		self.m_input_dirs.Bind( wx.EVT_LISTBOX_DCLICK, self.OnEditInputDirectory )
		self.m_btn_add_input_dir.Bind( wx.EVT_BUTTON, self.OnAddInputDirectory )
		self.m_btn_remove_input_dir.Bind( wx.EVT_BUTTON, self.on_delete_input_directory)
		self.m_btn_move_up_input_dir.Bind( wx.EVT_BUTTON, self.OnMoveUpInputDirectory )
		self.m_btn_move_down_input_dir.Bind( wx.EVT_BUTTON, self.OnMoveDownInputDirectory )
		self.m_scan_recursive.Bind( wx.EVT_CHECKBOX, self.on_toggle_scan_recursive)
		self.m_output_dir.Bind( wx.EVT_DIRPICKER_CHANGED, self.OnChangedOutputDir )
		self.m_reference_source.Bind( wx.EVT_CHECKBOX, self.OnToggleReferenceSource )
		self.m_extraction_mode.Bind( wx.EVT_CHOICE, self.OnChangeExtractionMode )
		self.m_optmize_for.Bind( wx.EVT_CHOICE, self.OnChangeOptimizeFor )
		self.m_diagram_maker.Bind( wx.EVT_CHOICEBOOK_PAGE_CHANGED, self.OnChangedDiagramMaker )
		self.m_graphwiz_class.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizClass )
		self.m_graphwiz_collaboration.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizCollaboration )
		self.m_graphwiz_includes.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizIncludes )
		self.m_graphwiz_included_by.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizIncludedBy )
		self.m_graphwiz_hierarchy.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizHierarchy )
		self.m_graphwiz_call.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizCall )
		self.m_std_buttonsCancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_std_buttonsOK.Bind( wx.EVT_BUTTON, self.on_ok )
	
	def __del__(self):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OnProjectNameChanged(self, event):
		event.Skip()
	
	def OnProjectVersionChanged(self, event):
		event.Skip()
	
	def OnSelectInputDirectory(self, event):
		event.Skip()
	
	def OnEditInputDirectory(self, event):
		event.Skip()
	
	def OnAddInputDirectory(self, event):
		event.Skip()
	
	def on_delete_input_directory(self, event):
		event.Skip()
	
	def OnMoveUpInputDirectory(self, event):
		event.Skip()
	
	def OnMoveDownInputDirectory(self, event):
		event.Skip()
	
	def on_toggle_scan_recursive(self, event):
		event.Skip()
	
	def OnChangedOutputDir(self, event):
		event.Skip()
	
	def OnToggleReferenceSource(self, event):
		event.Skip()
	
	def OnChangeExtractionMode(self, event):
		event.Skip()
	
	def OnChangeOptimizeFor(self, event):
		event.Skip()
	
	def OnChangedDiagramMaker(self, event):
		event.Skip()
	
	def OnToggleGraphWizClass(self, event):
		event.Skip()
	
	def OnToggleGraphWizCollaboration(self, event):
		event.Skip()
	
	def OnToggleGraphWizIncludes(self, event):
		event.Skip()
	
	def OnToggleGraphWizIncludedBy(self, event):
		event.Skip()
	
	def OnToggleGraphWizHierarchy(self, event):
		event.Skip()
	
	def OnToggleGraphWizCall(self, event):
		event.Skip()
	
	def on_cancel(self, event):
		event.Skip()
	
	def on_ok(self, event):
		event.Skip()
	
	def m_splitter1OnIdle(self, event):
		self.m_splitter1.SetSashPosition( 500 )
		self.m_splitter1.Unbind( wx.EVT_IDLE )
	

###########################################################################
## Class DoxygenEditorBase
###########################################################################

class DoxygenEditorBase (wx.Panel):
	
	def __init__(self, parent):
		wx.Panel.__init__ (self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
		
		fgSizer53 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer53.AddGrowableCol( 0 )
		fgSizer53.AddGrowableRow( 0 )
		fgSizer53.SetFlexibleDirection( wx.BOTH )
		fgSizer53.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_choicebook2 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel11 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		fgSizer17 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer17.AddGrowableCol( 0 )
		fgSizer17.AddGrowableRow( 0 )
		fgSizer17.SetFlexibleDirection( wx.BOTH )
		fgSizer17.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer13 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel11, wx.ID_ANY, u"Project" ), wx.VERTICAL )
		
		fgSizer37 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer37.AddGrowableCol( 0 )
		fgSizer37.AddGrowableRow( 1 )
		fgSizer37.SetFlexibleDirection( wx.BOTH )
		fgSizer37.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer38 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer38.AddGrowableCol( 1 )
		fgSizer38.SetFlexibleDirection( wx.BOTH )
		fgSizer38.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText7 = wx.StaticText( sbSizer13.GetStaticBox(), wx.ID_ANY, u"name:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		fgSizer38.Add(self.m_staticText7, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_project_name = wx.TextCtrl( sbSizer13.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		fgSizer38.Add(self.m_project_name, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText8 = wx.StaticText( sbSizer13.GetStaticBox(), wx.ID_ANY, u"version:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText8.Wrap( -1 )
		fgSizer38.Add(self.m_staticText8, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_project_version = wx.TextCtrl( sbSizer13.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer38.Add(self.m_project_version, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer37.Add( fgSizer38, 1, wx.EXPAND, 5 )
		
		sbSizer14 = wx.StaticBoxSizer( wx.StaticBox( sbSizer13.GetStaticBox(), wx.ID_ANY, u"input directories" ), wx.VERTICAL )
		
		fgSizer39 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer39.AddGrowableCol( 0 )
		fgSizer39.AddGrowableRow( 0 )
		fgSizer39.SetFlexibleDirection( wx.BOTH )
		fgSizer39.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer40 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer40.AddGrowableCol( 0 )
		fgSizer40.AddGrowableRow( 0 )
		fgSizer40.SetFlexibleDirection( wx.BOTH )
		fgSizer40.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		m_input_dirsChoices = []
		self.m_input_dirs = wx.ListBox( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_input_dirsChoices, 0 )
		self.m_input_dirs.SetToolTip( u"Double-click for edit directory" )
		
		fgSizer40.Add(self.m_input_dirs, 1, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer41 = wx.FlexGridSizer( 5, 1, 0, 0 )
		fgSizer41.SetFlexibleDirection( wx.BOTH )
		fgSizer41.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_btn_add_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ADD_BOOKMARK, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		fgSizer41.Add(self.m_btn_add_input_dir, 0, wx.ALL, 5 )
		
		self.m_btn_remove_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_btn_remove_input_dir.Enable(False)
		
		fgSizer41.Add(self.m_btn_remove_input_dir, 0, wx.ALL, 5 )
		
		self.m_btn_move_up_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_UP, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_btn_move_up_input_dir.Enable(False)
		
		fgSizer41.Add(self.m_btn_move_up_input_dir, 0, wx.ALL, 5 )
		
		self.m_btn_move_down_input_dir = wx.BitmapButton( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_DOWN, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_btn_move_down_input_dir.Enable(False)
		
		fgSizer41.Add(self.m_btn_move_down_input_dir, 0, wx.ALL, 5 )
		
		
		fgSizer41.Add( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		fgSizer40.Add( fgSizer41, 1, wx.EXPAND|wx.RIGHT, 5 )
		
		
		fgSizer39.Add( fgSizer40, 1, wx.EXPAND, 5 )
		
		
		sbSizer14.Add( fgSizer39, 1, wx.EXPAND, 5 )
		
		self.m_scan_recursive = wx.CheckBox( sbSizer14.GetStaticBox(), wx.ID_ANY, u"scan recursively", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer14.Add(self.m_scan_recursive, 0, wx.ALL, 5 )
		
		
		fgSizer37.Add( sbSizer14, 1, wx.EXPAND|wx.ALL, 5 )
		
		sbSizer19 = wx.StaticBoxSizer( wx.StaticBox( sbSizer13.GetStaticBox(), wx.ID_ANY, u"Output" ), wx.VERTICAL )
		
		fgSizer46 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer46.AddGrowableCol( 1 )
		fgSizer46.SetFlexibleDirection( wx.HORIZONTAL )
		fgSizer46.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText11 = wx.StaticText( sbSizer19.GetStaticBox(), wx.ID_ANY, u"directory:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )
		fgSizer46.Add(self.m_staticText11, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_output_dir = wx.DirPickerCtrl( sbSizer19.GetStaticBox(), wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE|wx.DIRP_USE_TEXTCTRL )
		fgSizer46.Add(self.m_output_dir, 1, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.RIGHT, 5 )
		
		
		sbSizer19.Add( fgSizer46, 1, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.LEFT, 5 )
		
		
		fgSizer37.Add( sbSizer19, 1, wx.EXPAND|wx.BOTTOM|wx.RIGHT|wx.LEFT, 5 )
		
		
		sbSizer13.Add( fgSizer37, 1, wx.EXPAND|wx.BOTTOM, 5 )
		
		
		fgSizer17.Add( sbSizer13, 1, wx.EXPAND|wx.ALL, 5 )
		
		fgSizer42 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer42.AddGrowableCol( 0 )
		fgSizer42.AddGrowableRow( 0 )
		fgSizer42.AddGrowableRow( 1 )
		fgSizer42.SetFlexibleDirection( wx.BOTH )
		fgSizer42.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer15 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel11, wx.ID_ANY, u"Mode" ), wx.VERTICAL )
		
		fgSizer43 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer43.AddGrowableCol( 0 )
		fgSizer43.AddGrowableRow( 1 )
		fgSizer43.SetFlexibleDirection( wx.BOTH )
		fgSizer43.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_reference_source = wx.CheckBox( sbSizer15.GetStaticBox(), wx.ID_ANY, u"reference source", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_reference_source.SetValue(True) 
		fgSizer43.Add(self.m_reference_source, 0, wx.ALL, 5 )
		
		fgSizer44 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fgSizer44.AddGrowableCol( 1 )
		fgSizer44.SetFlexibleDirection( wx.BOTH )
		fgSizer44.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText9 = wx.StaticText( sbSizer15.GetStaticBox(), wx.ID_ANY, u"extraction mode:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )
		fgSizer44.Add(self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		m_extraction_modeChoices = [ u"all entities", u"only comented entities" ]
		self.m_extraction_mode = wx.Choice( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_extraction_modeChoices, 0 )
		self.m_extraction_mode.SetSelection( 0 )
		fgSizer44.Add(self.m_extraction_mode, 1, wx.ALL, 5 )
		
		self.m_staticText10 = wx.StaticText( sbSizer15.GetStaticBox(), wx.ID_ANY, u"optimize for:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText10.Wrap( -1 )
		fgSizer44.Add(self.m_staticText10, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		m_optmize_forChoices = [ u"c++", u"c", u"c#" ]
		self.m_optmize_for = wx.Choice( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_optmize_forChoices, 0 )
		self.m_optmize_for.SetSelection( 0 )
		fgSizer44.Add(self.m_optmize_for, 1, wx.ALL, 5 )
		
		
		fgSizer43.Add( fgSizer44, 1, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		sbSizer15.Add( fgSizer43, 1, wx.EXPAND, 5 )
		
		
		fgSizer42.Add( sbSizer15, 1, wx.EXPAND|wx.TOP|wx.RIGHT, 5 )
		
		sbSizer18 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel11, wx.ID_ANY, u"Diagrams" ), wx.VERTICAL )
		
		self.m_diagram_maker = wx.Choicebook( sbSizer18.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel22 = wx.Panel(self.m_diagram_maker, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_diagram_maker.AddPage(self.m_panel22, u"no diagrams", False )
		self.m_panel23 = wx.Panel(self.m_diagram_maker, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_diagram_maker.AddPage(self.m_panel23, u"builtin diagram generator", False )
		self.m_panel24 = wx.Panel(self.m_diagram_maker, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer45 = wx.FlexGridSizer( 3, 2, 0, 0 )
		fgSizer45.SetFlexibleDirection( wx.BOTH )
		fgSizer45.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_graphwiz_class = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"class", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_class.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_class, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_collaboration = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"collaboration", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_collaboration.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_collaboration, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_includes = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"includes", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_includes.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_includes, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_included_by = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"included by", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_included_by.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_included_by, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_hierarchy = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"hierarchy", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_graphwiz_hierarchy.SetValue(True) 
		fgSizer45.Add(self.m_graphwiz_hierarchy, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_graphwiz_call = wx.CheckBox(self.m_panel24, wx.ID_ANY, u"call", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer45.Add(self.m_graphwiz_call, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		
		self.m_panel24.SetSizer( fgSizer45 )
		self.m_panel24.Layout()
		fgSizer45.Fit(self.m_panel24 )
		self.m_diagram_maker.AddPage(self.m_panel24, u"graphwiz dot package", True )
		sbSizer18.Add(self.m_diagram_maker, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer42.Add( sbSizer18, 1, wx.EXPAND|wx.RIGHT, 5 )
		
		
		fgSizer17.Add( fgSizer42, 1, wx.EXPAND, 5 )
		
		
		self.m_panel11.SetSizer( fgSizer17 )
		self.m_panel11.Layout()
		fgSizer17.Fit(self.m_panel11 )
		self.m_choicebook2.AddPage(self.m_panel11, u"basic configuration", True )
		self.m_panel12 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer28 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer28.AddGrowableCol( 0 )
		fgSizer28.AddGrowableRow( 0 )
		fgSizer28.SetFlexibleDirection( wx.BOTH )
		fgSizer28.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_splitter1 = wx.SplitterWindow(self.m_panel12, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
		self.m_splitter1.SetSashGravity( 1 )
		self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )
		
		self.m_panel121 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel13 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer30 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer30.AddGrowableCol( 0 )
		fgSizer30.AddGrowableRow( 0 )
		fgSizer30.SetFlexibleDirection( wx.BOTH )
		fgSizer30.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_textctrl_property_description = wx.richtext.RichTextCtrl(self.m_panel13, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY|wx.HSCROLL|wx.SIMPLE_BORDER|wx.VSCROLL|wx.WANTS_CHARS )
		fgSizer30.Add(self.m_textctrl_property_description, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		
		self.m_panel13.SetSizer( fgSizer30 )
		self.m_panel13.Layout()
		fgSizer30.Fit(self.m_panel13 )
		self.m_splitter1.SplitVertically(self.m_panel121, self.m_panel13, 500 )
		fgSizer28.Add(self.m_splitter1, 1, wx.EXPAND, 5 )
		
		
		self.m_panel12.SetSizer( fgSizer28 )
		self.m_panel12.Layout()
		fgSizer28.Fit(self.m_panel12 )
		self.m_choicebook2.AddPage(self.m_panel12, u"advanced configuration", False )
		fgSizer53.Add(self.m_choicebook2, 1, wx.EXPAND|wx.ALL, 5 )
		
		fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer159.AddGrowableCol( 1 )
		fgSizer159.SetFlexibleDirection( wx.BOTH )
		fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
		fgSizer159.Add(self.m_info, 0, wx.ALL, 5 )
		
		m_std_buttons = wx.StdDialogButtonSizer()
		self.m_std_buttonsOK = wx.Button(self, wx.ID_OK )
		m_std_buttons.AddButton(self.m_std_buttonsOK )
		self.m_std_buttonsCancel = wx.Button(self, wx.ID_CANCEL )
		m_std_buttons.AddButton(self.m_std_buttonsCancel )
		m_std_buttons.Realize();
		
		fgSizer159.Add( m_std_buttons, 1, wx.EXPAND, 5 )
		
		
		fgSizer53.Add( fgSizer159, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( fgSizer53 )
		self.Layout()
		
		# Connect Events
		self.m_project_name.Bind( wx.EVT_TEXT, self.OnProjectNameChanged )
		self.m_project_version.Bind( wx.EVT_TEXT, self.OnProjectVersionChanged )
		self.m_input_dirs.Bind( wx.EVT_LISTBOX, self.OnSelectInputDirectory )
		self.m_input_dirs.Bind( wx.EVT_LISTBOX_DCLICK, self.OnEditInputDirectory )
		self.m_btn_add_input_dir.Bind( wx.EVT_BUTTON, self.OnAddInputDirectory )
		self.m_btn_remove_input_dir.Bind( wx.EVT_BUTTON, self.on_delete_input_directory)
		self.m_btn_move_up_input_dir.Bind( wx.EVT_BUTTON, self.OnMoveUpInputDirectory )
		self.m_btn_move_down_input_dir.Bind( wx.EVT_BUTTON, self.OnMoveDownInputDirectory )
		self.m_scan_recursive.Bind( wx.EVT_CHECKBOX, self.on_toggle_scan_recursive)
		self.m_output_dir.Bind( wx.EVT_DIRPICKER_CHANGED, self.OnChangedOutputDir )
		self.m_reference_source.Bind( wx.EVT_CHECKBOX, self.OnToggleReferenceSource )
		self.m_extraction_mode.Bind( wx.EVT_CHOICE, self.OnChangeExtractionMode )
		self.m_optmize_for.Bind( wx.EVT_CHOICE, self.OnChangeOptimizeFor )
		self.m_diagram_maker.Bind( wx.EVT_CHOICEBOOK_PAGE_CHANGED, self.OnChangedDiagramMaker )
		self.m_graphwiz_class.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizClass )
		self.m_graphwiz_collaboration.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizCollaboration )
		self.m_graphwiz_includes.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizIncludes )
		self.m_graphwiz_included_by.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizIncludedBy )
		self.m_graphwiz_hierarchy.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizHierarchy )
		self.m_graphwiz_call.Bind( wx.EVT_CHECKBOX, self.OnToggleGraphWizCall )
		self.m_std_buttonsCancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_std_buttonsOK.Bind( wx.EVT_BUTTON, self.on_ok )
	
	def __del__(self):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OnProjectNameChanged(self, event):
		event.Skip()
	
	def OnProjectVersionChanged(self, event):
		event.Skip()
	
	def OnSelectInputDirectory(self, event):
		event.Skip()
	
	def OnEditInputDirectory(self, event):
		event.Skip()
	
	def OnAddInputDirectory(self, event):
		event.Skip()
	
	def on_delete_input_directory(self, event):
		event.Skip()
	
	def OnMoveUpInputDirectory(self, event):
		event.Skip()
	
	def OnMoveDownInputDirectory(self, event):
		event.Skip()
	
	def on_toggle_scan_recursive(self, event):
		event.Skip()
	
	def OnChangedOutputDir(self, event):
		event.Skip()
	
	def OnToggleReferenceSource(self, event):
		event.Skip()
	
	def OnChangeExtractionMode(self, event):
		event.Skip()
	
	def OnChangeOptimizeFor(self, event):
		event.Skip()
	
	def OnChangedDiagramMaker(self, event):
		event.Skip()
	
	def OnToggleGraphWizClass(self, event):
		event.Skip()
	
	def OnToggleGraphWizCollaboration(self, event):
		event.Skip()
	
	def OnToggleGraphWizIncludes(self, event):
		event.Skip()
	
	def OnToggleGraphWizIncludedBy(self, event):
		event.Skip()
	
	def OnToggleGraphWizHierarchy(self, event):
		event.Skip()
	
	def OnToggleGraphWizCall(self, event):
		event.Skip()
	
	def on_cancel(self, event):
		event.Skip()
	
	def on_ok(self, event):
		event.Skip()
	
	def m_splitter1OnIdle(self, event):
		self.m_splitter1.SetSashPosition( 500 )
		self.m_splitter1.Unbind( wx.EVT_IDLE )
	

###########################################################################
## Class EditDirectoryBase
###########################################################################

class EditDirectoryBase ( wxx.Dialog ):
	
	def __init__(self, parent):
		wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"New input directory", pos = wx.DefaultPosition, size = wx.Size( 417,199 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		
		fgSizer29 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer29.AddGrowableCol( 0 )
		fgSizer29.AddGrowableRow( 0 )
		fgSizer29.SetFlexibleDirection( wx.BOTH )
		fgSizer29.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer30 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer30.AddGrowableCol( 0 )
		fgSizer30.AddGrowableRow( 0 )
		fgSizer30.AddGrowableRow( 2 )
		fgSizer30.SetFlexibleDirection( wx.BOTH )
		fgSizer30.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		
		fgSizer30.Add( ( 0, 0), 1, wx.EXPAND, 5 )
		
		fgSizer34 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer34.AddGrowableCol( 1 )
		fgSizer34.AddGrowableRow( 0 )
		fgSizer34.SetFlexibleDirection( wx.BOTH )
		fgSizer34.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText11 = wx.StaticText(self, wx.ID_ANY, u"directory:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )
		fgSizer34.Add(self.m_staticText11, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_dirPicker3 = wx.DirPickerCtrl(self, wx.ID_ANY, u"/home", u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_USE_TEXTCTRL )
		fgSizer34.Add(self.m_dirPicker3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		fgSizer30.Add( fgSizer34, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		
		fgSizer30.Add( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		fgSizer29.Add( fgSizer30, 1, wx.EXPAND, 5 )
		
		fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer159.AddGrowableCol( 1 )
		fgSizer159.SetFlexibleDirection( wx.BOTH )
		fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
		fgSizer159.Add(self.m_info, 0, wx.ALL, 5 )
		
		m_std_buttons = wx.StdDialogButtonSizer()
		self.m_std_buttonsOK = wx.Button(self, wx.ID_OK )
		m_std_buttons.AddButton(self.m_std_buttonsOK )
		self.m_std_buttonsCancel = wx.Button(self, wx.ID_CANCEL )
		m_std_buttons.AddButton(self.m_std_buttonsCancel )
		m_std_buttons.Realize();
		
		fgSizer159.Add( m_std_buttons, 1, wx.EXPAND, 5 )
		
		
		fgSizer29.Add( fgSizer159, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( fgSizer29 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_std_buttonsCancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_std_buttonsOK.Bind( wx.EVT_BUTTON, self.on_ok )
	
	def __del__(self):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_cancel(self, event):
		event.Skip()
	
	def on_ok(self, event):
		event.Skip()
	

