# -*- coding: utf-8 -*-

_global = [
b"16 16 43 1",
b" 	c None",
b".	c #E5BDE5",
b"+	c #7C7C7C",
b"@	c #E4BBE3",
b"#	c #EE7E96",
b"$	c #F8444D",
b"%	c #FC2729",
b"&	c #FD2121",
b"*	c #FB2F33",
b"=	c #F55461",
b"-	c #EA93B1",
b";	c #E4BAE1",
b">	c #F64E5A",
b",	c #F64F5B",
b"'	c #EA97B5",
b")	c #E5B5DB",
b"!	c #E7A9CD",
b"~	c #ED849D",
b"{	c #F7454F",
b"]	c #EF778E",
b"^	c #F5515D",
b"/	c #F93B43",
b"(	c #E99BBB",
b"_	c #FD2425",
b":	c #E4B7DE",
b"<	c #FD2224",
b"[	c #E5B7DD",
b"}	c #FF1F1F",
b"|	c #F93A41",
b"1	c #E99BBA",
b"2	c #F07388",
b"3	c #E4B9E0",
b"4	c #F64C57",
b"5	c #EA95B3",
b"6	c #E5B4DA",
b"7	c #E6AFD4",
b"8	c #EC89A4",
b"9	c #EF768D",
b"0	c #F93E46",
b"a	c #FD2526",
b"b	c #FB3035",
b"c	c #F45968",
b"d	c #E99DBD",
b"                ",
b"   ..........   ",
b"  +..........+  ",
b" ...@#$%&*=-... ",
b" ..;>,');!~{... ",
b" ..]^.......... ",
b" ../(.......... ",
b" .._:.......... ",
b" ..<[....}}}... ",
b" ..|1......}... ",
b" ..2,......}... ",
b" ..3{456;78}... ",
b" ...;90a&bcd... ",
b"  +..........+  ",
b"   ..........   ",
b"                "]