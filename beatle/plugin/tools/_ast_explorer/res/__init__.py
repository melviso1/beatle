# -*- coding: utf-8 -*-
import wx
import ast

from ._expr import _expr
from ._module import _module
from ._import import _import
from ._str import _str
from ._alias import _alias
from ._assign import _assign
from ._name import _name
from ._binop import _binop
from ._store import _store
from ._load import _load
from ._attribute import _attribute
from ._add import _add
from ._num import _num
from ._classdef import _classdef
from ._functiondef import _functiondef
from ._arguments import _arguments
from ._param import _param
from ._call import _call
from ._dict import _dict
from ._return import _return
from ._listcomp import _listcomp
from ._tuple import _tuple
from ._comprehension import _comprehension
from ._global import _global
from ._for import _for
from ._keyword import _keyword
from ._if import _if
from ._bool import _bool
from ._and import _and
from ._compare import _compare
from ._in import _in
from ._subscript import _subscript
from ._index import _index
from ._unary import _unary
from ._not import _not
from ._eq import _eq
from ._not_eq import _not_eq
from ._delete import _delete
from ._del import _del
from ._sub import _sub
from ._not_in import _not_in
from ._gte import _gte
from ._lte import _lte
from ._lt import _lt
from ._gt import _gt
from ._is import _is
from ._is_not import _is_not
from ._floor_div import _floor_div
from ._continue import _continue
from ._try import _try
from ._or import _or
from ._except import _except
from ._print import _print
from ._list import _list
from ._lambda import _lambda

_xpm = {
    'expr':_expr, 'module': _module, 'import': _import,
    'std': _str, 'alias': _alias, 'assign': _assign,
    'name':_name, 'binop': _binop, 'store': _store,
    'load': _load, 'attribute': _attribute, 'add': _add,
    'num': _num, 'classdef': _classdef, 'functiondef':_functiondef,
    'arguments':_arguments, 'param': _param, 'call':_call,
    'dict': _dict, 'return': _return, 'tuple': _tuple, 'listcom':_listcomp,
    'comprehension': _comprehension, 'global': _global, 'for': _for,
    'keyword': _keyword, 'if':_if, 'bool':_bool, 'and':_and, 'compare':_compare,
    'in':_in, 'subscript':_subscript, 'index':_index, 'unary':_unary, 'not':_not,
    'eq': _eq, 'not_eq':_not_eq, 'delete':_delete, 'del':_del, 'sub':_sub,
    'not_in': _not_in, 'gte':_gte, 'lte': _lte, 'lt':_lt, 'gt': _gt, 'is':_is,
    'is_not': _is_not, 'floor_div': _floor_div, 'continue': _continue,
    'try': _try, 'or': _or, 'except': _except, 'print': _print, 'list': _list,
    'lambda': _lambda
}

_bitmap_list = []


def create_bitmap_list():
    """Create a bitmap list"""
    global _bitmap_list
    global _xpm
    _bitmap_list = [wx.Bitmap(_xpm[index]) for index in _xpm.keys()]


def get_bitmap_index(name):
    """Return the bitmap index"""
    try:
        base_index = list(_xpm.keys()).index(name)
    except Exception as e:
        print('invalid bitmap index in get_bitmap_index.')
        return wx.NOT_FOUND
    return base_index


def GetBitmap(name):
    """Get the specific bitmap"""
    index = get_bitmap_index(name)
    if index == wx.NOT_FOUND:
        raise KeyError('bitmap {0} not found'.format(name))
        return None
    global _bitmap_list
    if not len(_bitmap_list):
        create_bitmap_list()
    return _bitmap_list[index]


def get_bitmap_image_list():
    """Returns the bitmap image list"""
    global _bitmap_list
    if len(_bitmap_list) == 0:
        create_bitmap_list()
    image_list = wx.ImageList(16, 16, True, len(_bitmap_list))
    for bmp in _bitmap_list:
        image_list.Add(bmp)
    return image_list
