# -*- coding: utf-8 -*-

_compare = [
b"16 16 21 1",
b" 	c None",
b".	c #68BD68",
b"+	c #7C7C7C",
b"@	c #548B54",
b"#	c #3A483A",
b"$	c #323532",
b"%	c #3B4B3B",
b"&	c #5B9D5B",
b"*	c #405940",
b"=	c #5DA25D",
b"-	c #66B866",
b";	c #538953",
b">	c #384238",
b",	c #5FA85F",
b"'	c #374037",
b")	c #60A960",
b"!	c #394639",
b"~	c #548A54",
b"{	c #3A4A3A",
b"]	c #558E55",
b"^	c #323232",
b"                ",
b"   ..........   ",
b"  +..........+  ",
b" .............. ",
b" ....@#$%&..... ",
b" ....*=-;>..... ",
b" .......,'..... ",
b" ......)!~..... ",
b" ......{]...... ",
b" ......^....... ",
b" .............. ",
b" ......^....... ",
b" ......^....... ",
b"  +..........+  ",
b"   ..........   ",
b"                "]