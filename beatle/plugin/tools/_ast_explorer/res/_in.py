# -*- coding: utf-8 -*-

_in = [
b"16 16 32 1",
b" 	c None",
b".	c #68BD68",
b"+	c #7C7C7C",
b"@	c #5EA35E",
b"#	c #435F43",
b"$	c #374037",
b"%	c #333533",
b"&	c #3C4D3C",
b"*	c #558E55",
b"=	c #5CA05C",
b"-	c #538853",
b";	c #63B163",
b">	c #64B464",
b",	c #415941",
b"'	c #548B54",
b")	c #323232",
b"!	c #3B4B3B",
b"~	c #3C4E3C",
b"{	c #5C9F5C",
b"]	c #4A724A",
b"^	c #4F7E4F",
b"/	c #62AE62",
b"(	c #384338",
b"_	c #579457",
b":	c #64B364",
b"<	c #65B565",
b"[	c #5A995A",
b"}	c #3F543F",
b"|	c #333833",
b"1	c #323332",
b"2	c #3D4E3D",
b"3	c #558F55",
b"                ",
b"   ..........   ",
b"  +..........+  ",
b" ....@#$%&*.... ",
b" ...=$-;>...... ",
b" ...,'......... ",
b" ...))......... ",
b" ...)))))!*.... ",
b" ...))......... ",
b" ...~{......... ",
b" ...]^......... ",
b" .../(_:<...... ",
b" ....[}|123.... ",
b"  +..........+  ",
b"   ..........   ",
b"                "]