# -*- coding: utf-8 -*-

_functiondef = [
b"16 16 4 1",
b" 	c None",
b".	c #219D95",
b"+	c #7C7C7C",
b"@	c #D8B0B0",
b"                ",
b"   ..........   ",
b"  +..........+  ",
b" ....@@@@@@.... ",
b" ....@......... ",
b" ....@......... ",
b" ....@......... ",
b" ....@@@@@@.... ",
b" ....@......... ",
b" ....@......... ",
b" ....@......... ",
b" ....@......... ",
b" ....@......... ",
b"  +..........+  ",
b"   ..........   ",
b"                "]