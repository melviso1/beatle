# -*- coding: utf-8 -*-
import wx

from beatle.lib import wxx
from beatle.lib.api import context
import beatle.analytic.astd as astd
from beatle.lib.handlers import identifier
from beatle.model import arch
from .ToolAstExplorerPane import ToolAstExplorerPane
from . import res


class AstExplorer(wx.EvtHandler):
    """Class for providing ast explorer"""
    instance = None

    def __init__(self):
        """Initialize ast explorer"""
        self._ast_explorer = identifier('ID_AST_EXPLORER')
        self._ast_explorer_menu = None
        super(AstExplorer, self).__init__()
        self.append_tools_menu()
        #bind the menu handlers
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_ast_explorer, id=self._ast_explorer)
        self.Bind(wx.EVT_MENU, self.on_ast_explorer, id=self._ast_explorer)

    def append_tools_menu(self):
        frame = context.get_frame()
        self._ast_explorer_menu = wxx.MenuItem(
            frame.menuTools, self._ast_explorer, u"Ast explorer",
            u"show ast structure", wx.ITEM_NORMAL)
        frame.add_tools_menu('Ast explorer', self._ast_explorer_menu)

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        pass

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        if cls.instance is None:
            cls.instance = AstExplorer()
        return cls.instance

    def on_update_ast_explorer(self, event):
        """Handle the update"""
        # get the view book
        frame = context.get_frame()
        self.selected = None
        book = frame.viewBook
        try:
            view = book.GetCurrentPage()
            if view is not None:
                # get the selected item
                selected = view.selected
                if type(selected) is arch.File:
                    if selected.project.language == 'python':
                        self.selected = selected
        except Exception as e:
            pass
        if self.selected is None:
            event.Enable(False)
        else:
            event.Enable(True)

    def on_ast_explorer(self, event):
        """Handle the command"""
        import beatle.app.resources as rc
        item = self.selected
        path = item.abs_file
        try:
            with open(path, "r") as python_file:
                content = python_file.read()
                data = astd.parse(content)
        except Exception as e:
            wx.MessageBox("The file contains errors", "Error",
                wx.OK | wx.CENTER | wx.ICON_ERROR, context.get_frame())
            return
        frame = context.get_frame()
        pane = ToolAstExplorerPane(frame.docBook, item, data)
        frame.docBook.Freeze()
        frame.docBook.AddPage(pane, path, True, rc.get_bitmap_index("mini_logo"))
        frame.docBook.Thaw()

