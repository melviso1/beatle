# -*- coding: utf-8 -*-
data_view_control = [
b"24 24 10 1",
b" 	c None",
b".	c #3E9ADE",
b"+	c #858585",
b"@	c #FFFFFF",
b"#	c #555753",
b"$	c #FF3300",
b"%	c #386093",
b"&	c #FFCC00",
b"*	c #1871CE",
b"=	c #5CAD25",
b"                        ",
b" ...................... ",
b" ...................... ",
b" ..++++++++++++++++++++ ",
b" ..+@@@@@@@@@@@@@@@@@@@ ",
b" ..+@@#@@@@@@@@@@@@@@@@ ",
b" ..+@#$@%%%%%%@@@@@@@@@ ",
b" ..+@@@@@@@@@@@@@@@@@@@ ",
b" ..+@@@@#@@@@@@@@@@@@@@ ",
b" ..+@@@#&@%%%%%%@@@@@@@ ",
b" ..+@@@@@@@@@@@@@@@@@@@ ",
b" ..+@@@@@@#@@@@@@@@@@@@ ",
b" ..+@@@@@#*@%%%%%%@@@@@ ",
b" ..+@@@@@@@@@@@@@@@@@@@ ",
b" ..+@@@@@@#@@@@@@@@@@@@ ",
b" ..+@@@@@#*@%%%%%%@@@@@ ",
b" ..+@@@@@@@@@@@@@@@@@@@ ",
b" ..+@@@@@@@@#@@@@@@@@@@ ",
b" ..+@@@@@@@#=@%%%%%%@@@ ",
b" ..+@@@@@@@@@@@@@@@@@@@ ",
b" ..+@@@@#@@@@@@@@@@@@@@ ",
b" ..+@@@#&@%%%%%%@@@@@@@ ",
b" ..+@@@@@@@@@@@@@@@@@@@ ",
b"                        "]
