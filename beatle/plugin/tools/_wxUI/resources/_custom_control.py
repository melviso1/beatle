# -*- coding: utf-8 -*-
custom_control = [
b"24 24 179 2",
b"  	c None",
b". 	c #626262",
b"+ 	c #656565",
b"@ 	c #666666",
b"# 	c #EDEDED",
b"$ 	c #ECECEC",
b"% 	c #EBEBEB",
b"& 	c #E9E9E9",
b"* 	c #E7E7E7",
b"= 	c #E5E5E5",
b"- 	c #E2E2E2",
b"; 	c #DFDFDF",
b"> 	c #DDDDDD",
b", 	c #DBDBDB",
b"' 	c #D9D9D9",
b") 	c #D7D7D7",
b"! 	c #D8D8D8",
b"~ 	c #D6D6D6",
b"{ 	c #D3D3D3",
b"] 	c #D1D1D1",
b"^ 	c #CDCDCD",
b"/ 	c #CBCBCB",
b"( 	c #C6C6C6",
b"_ 	c #C4C4C4",
b": 	c #BFBFBF",
b"< 	c #BBBBBB",
b"[ 	c #B7B7B7",
b"} 	c #B2B2B2",
b"| 	c #AFAFAF",
b"1 	c #ABABAB",
b"2 	c #D4D4D4",
b"3 	c #CFCFCF",
b"4 	c #CCCCCC",
b"5 	c #C9C9C9",
b"6 	c #C5C5C5",
b"7 	c #C1C1C1",
b"8 	c #BDBDBD",
b"9 	c #B9B9B9",
b"0 	c #B4B4B4",
b"a 	c #B1B1B1",
b"b 	c #AEAEAE",
b"c 	c #A7A7A7",
b"d 	c #CCCDCF",
b"e 	c #9EB1D9",
b"f 	c #7497DF",
b"g 	c #698FDE",
b"h 	c #6A8FDC",
b"i 	c #668DDC",
b"j 	c #6B8ED6",
b"k 	c #889DC6",
b"l 	c #AEAFB2",
b"m 	c #CDCED1",
b"n 	c #94ACDE",
b"o 	c #6E94E1",
b"p 	c #9FBAF1",
b"q 	c #C2D3F6",
b"r 	c #CFDCF7",
b"s 	c #D2DFF8",
b"t 	c #ABC1ED",
b"u 	c #7392D3",
b"v 	c #7793CA",
b"w 	c #AAACAF",
b"x 	c #9EB2DB",
b"y 	c #618DE6",
b"z 	c #AAC4FA",
b"A 	c #D1DEF9",
b"B 	c #EEF3FC",
b"C 	c #EEF3FE",
b"D 	c #F6F9FE",
b"E 	c #EDF2FD",
b"F 	c #BBCFF9",
b"G 	c #658AD6",
b"H 	c #7E94C0",
b"I 	c #A8A8A8",
b"J 	c #A4A4A4",
b"K 	c #E8E8E8",
b"L 	c #D0D0D0",
b"M 	c #6993E7",
b"N 	c #7CA2F0",
b"O 	c #A8C4FC",
b"P 	c #E2EAFA",
b"Q 	c #DAE6FE",
b"R 	c #A6C3FE",
b"S 	c #C1D3F9",
b"T 	c #FBFCFD",
b"U 	c #B1CAFD",
b"V 	c #749AE8",
b"W 	c #4A79DA",
b"X 	c #A5A5A5",
b"Y 	c #A2A2A2",
b"Z 	c #E6E6E6",
b"` 	c #CACACA",
b" .	c #447DF1",
b"..	c #75A0F9",
b"+.	c #81AAFD",
b"@.	c #A7C3FD",
b"#.	c #90B4FE",
b"$.	c #84A9F6",
b"%.	c #CBD9F5",
b"&.	c #E1EAFD",
b"*.	c #6F9CF7",
b"=.	c #4681FA",
b"-.	c #1C64F6",
b";.	c #A0A0A0",
b">.	c #3273F8",
b",.	c #5D91FB",
b"'.	c #5E93FE",
b").	c #6898FA",
b"!.	c #C4D4F6",
b"~.	c #E4ECFC",
b"{.	c #87ACF7",
b"].	c #4782FB",
b"^.	c #3F7EFF",
b"/.	c #105EFE",
b"(.	c #9E9E9E",
b"_.	c #3374FA",
b":.	c #4884FE",
b"<.	c #4E88FE",
b"[.	c #4D87FD",
b"}.	c #6F9BF3",
b"|.	c #EDF2FB",
b"1.	c #8FB3FB",
b"2.	c #4A85FC",
b"3.	c #4582FF",
b"4.	c #1861F8",
b"5.	c #5A8AEC",
b"6.	c #3F7DFD",
b"7.	c #4F88FD",
b"8.	c #6092F8",
b"9.	c #ADC5F7",
b"0.	c #6093FC",
b"a.	c #3577FD",
b"b.	c #3E73DE",
b"c.	c #7997D3",
b"d.	c #3E7AF2",
b"e.	c #5A8FFA",
b"f.	c #5A90FE",
b"g.	c #7EA5F4",
b"h.	c #DAE4F8",
b"i.	c #85ACFD",
b"j.	c #588FFE",
b"k.	c #5A8EF8",
b"l.	c #2B6CEE",
b"m.	c #798EB9",
b"n.	c #688ACF",
b"o.	c #477CE9",
b"p.	c #628FEA",
b"q.	c #7CA5F8",
b"r.	c #B1C9FB",
b"s.	c #7EA6F8",
b"t.	c #608DE8",
b"u.	c #3C73E5",
b"v.	c #7089BB",
b"w.	c #9FA1A4",
b"x.	c #718EC9",
b"y.	c #5582DD",
b"z.	c #3C74E6",
b"A.	c #3B73E5",
b"B.	c #366FE6",
b"C.	c #4C79D7",
b"D.	c #7D91BA",
b"E.	c #A4A5A8",
b"F.	c #A3A3A3",
b"G.	c #E3E3E3",
b"H.	c #BEBEBE",
b"I.	c #B8B8B8",
b"J.	c #ADADAD",
b"K.	c #AAAAAA",
b"L.	c #9C9C9C",
b"M.	c #E1E1E1",
b"N.	c #C3C3C3",
b"O.	c #C0C0C0",
b"P.	c #BABABA",
b"Q.	c #A1A1A1",
b"R.	c #9F9F9F",
b"S.	c #BCBCBC",
b"T.	c #B6B6B6",
b"U.	c #B3B3B3",
b"V.	c #9D9D9D",
b"                                                ",
b"                                                ",
b"        . + + @ @ + + + @ + + + @ @ @ .         ",
b"      . @ # # $ % & * = - - ; > , ' ) + .       ",
b"      + # # ! ~ { ] ^ / ( _ : < [ } | 1 +       ",
b"      + % ~ 2 ] 3 4 5 6 7 8 9 0 a b 1 c @       ",
b"      + % ~ 2 ] d e f g h i j k l b 1 c @       ",
b"      + % ~ 2 m n o p q r s t u v w 1 c @       ",
b"      + & { ] x y z A B C D E F G H I J @       ",
b"      + K L 3 M N O P Q R S T U V W X Y +       ",
b"      + Z ^ `  ...+.@.#.$.%.&.*.=.-.Y ;.@       ",
b"      @ = 5 ( >.,.'.'.).!.~.{.].^./.;.(.+       ",
b"      + & { ] _.:.<.[.}.|.1.2.3.^.4.I J @       ",
b"      + K L 3 5.6.<.7.8.9.0.7.7.a.b.X Y +       ",
b"      + Z ^ ` c.d.e.f.g.h.i.j.k.l.m.Y ;.@       ",
b"      @ = 5 ( _ n.o.p.q.r.s.t.u.v.w.;.(.+       ",
b"      @ = 5 ( 6 7 x.y.z.A.B.C.D.E.F.;.(.+       ",
b"      @ G.( _ 7 H.< I.0 a J.K.c J ;.(.L.+       ",
b"      @ M.N.O.8 P.[ 0 a b K.c J Q.R.L.L.@       ",
b"      . + : S.9 T.U.a J.K.c J Q.R.V.L.@ .       ",
b"        . + @ + @ + @ + + @ + @ + + @ .         ",
b"                                                ",
b"                                                ",
b"                                                "]
