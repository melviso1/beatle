# -*- coding: utf-8 -*-
status_bar = [
b"24 24 10 1",
b" 	c None",
b".	c #BBB9AD",
b"+	c #BCB9AD",
b"@	c #716F64",
b"#	c #CECBBD",
b"$	c #CECCBD",
b"%	c #DFDCCD",
b"&	c #DFDDCD",
b"*	c #ECE9D8",
b"=	c #ACA899",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b" ..+..+.+..+....+.+...@ ",
b" #$#$#$$$$$$##$#######@ ",
b" %&%%%%%%%%&&&%%&&&&&&@ ",
b" *********************@ ",
b" ******************=**@ ",
b" *********************@ ",
b" ****************=*=**@ ",
b" *********************@ ",
b" **************=*=*=**@ ",
b" *********************@ ",
b" @@@@@@@@@@@@@@@@@@@@@@ ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        ",
b"                        "]
