# -*- coding: utf-8 -*-
import wx

import beatle.lib.api as api
from beatle.lib.api import context
from beatle.lib import wxx
from beatle.lib.tran import TransactionalMethod
from beatle.lib.handlers import identifier
from beatle.app.ui.tools import clone_mnu, append_copy_of_menu_item
from beatle.activity.models.ui.view import ModelsView
from .resources import *
from .model import Design, ccEventHandler, pyEventHandler

from .ui import uiDialog, uiDesign
from beatle.model import cc, py


ENABLED_LANGUAGES = ['c++', 'python']


# noinspection PyPep8Naming
class wxUI(wx.EvtHandler):
    """Class for providing ast explorer"""
    instance = None
    image_map = {
            'scrolled_window': scrolled_window,
            'search_control': search_control,
            'separator': separator,
            'sizer': sizer,
            'slider': slider,
            'spin_button': spin_button,
            'spin_control': spin_control,
            'splitter_window': splitter_window,
            'static_bitmap': static_bitmap,
            'static_box_sizer': static_box_sizer,
            'static_line': static_line,
            'static_text': static_text,
            'status_bar': status_bar,
            'styled_text': styled_text,
            'submenu': submenu,
            'text_control': text_control,
            'timer': timer,
            'toggle_button': toggle_button,
            'toolbar': toolbar,
            'tool': tool,
            'tool_separator': tool_separator,
            'tree_control': tree_control,
            'tree_list_control_column': tree_list_control_column,
            'tree_list_control': tree_list_control,
            'wizard_page': wizard_page,
            'wizard': wizard,
            'wrap_sizer': wrap_sizer,
            'wx_design': wx_design,
            'animation_control': animation_control,
            'aui_notebook': aui_notebook,
            'aui_toolbar': aui_toolbar,
            'bitmap_button': bitmap_button,
            'bitmap_combo_box': bitmap_combo_box,
            'button': button,
            'calendar_control': calendar_control,
            'check_box': check_box,
            'check_listbox': check_listbox,
            'choice_book': choice_book,
            'choice_box': choice_box,
            'colour_picker': colour_picker,
            'collapsible_panel': collapsible_panel,
            'combo_box': combo_box,
            'custom_control': custom_control,
            'data_view_column': data_view_column,
            'data_view_control': data_view_control,
            'data_view_list_column': data_view_list_column,
            'data_view_list_control': data_view_list_control,
            'data_view_tree_control': data_view_tree_control,
            'date_picker': date_picker,
            'dialog': dialog,
            'dialog_buttons_sizer' : dialog_buttons_sizer,
            'dir_picker': dir_picker,
            'file_picker': file_picker,
            'flexible_sizer': flexible_sizer,
            'font_picker': font_picker,
            'frame': frame,
            'gauge': gauge,
            'generic_dir_control': generic_dir_control,
            'grid_bag_sizer': grid_bag_sizer,
            'grid_control': grid_control,
            'grid_sizer': grid_sizer,
            'html_window': html_window,
            'hyperlink_control': hyperlink_control,
            'info_bar': info_bar,
            'list_book': list_book,
            'list_box': list_box,
            'list_control': list_control,
            'menu_bar': menu_bar,
            'menu_item': menu_item,
            'menu': menu,
            'menu_separator': menu_separator,
            'notebook': notebook,
            'panel': panel,
            'property_grid_item': property_grid_item,
            'property_grid_manager': property_grid_manager,
            'property_grid_page': property_grid_page,
            'property_grid': property_grid,
            'radio_box': radio_box,
            'radio_button': radio_button,
            'ribbon_bar': ribbon_bar,
            'ribbon_button_bar': ribbon_button_bar,
            'ribbon_button': ribbon_button,
            'ribbon_dropdown_button': ribbon_dropdown_button,
            'ribbon_dropdown_tool': ribbon_dropdown_tool,
            'ribbon_gallery_item': ribbon_gallery_item,
            'ribbon_gallery': ribbon_gallery,
            'ribbon_hybrid_button': ribbon_hybrid_button,
            'ribbon_hybrid_tool': ribbon_hybrid_tool,
            'ribbon_page': ribbon_page,
            'ribbon_panel': ribbon_panel,
            'ribbon_toggle_button': ribbon_toggle_button,
            'ribbon_toggle_tool': ribbon_toggle_tool,
            'ribbon_toolbar': ribbon_toolbar,
            'ribbon_tool': ribbon_tool,
            'rich_text': rich_text,
            'scroll_bar': scroll_bar,
            'simple_book': simple_book,
        }

    def __init__(self):
        """Initialize wxGUI"""
        super(wxUI, self).__init__()
        self._id_wxui_new = identifier("ID_WXUI_NEW")
        self._wxui_menu = None
        self.append_tools_menu()
        self.project = None
        self.setup_model_handlers()
        self.bind_events()
        if ccEventHandler not in cc.CCProject.member_method_classes:
            cc.CCProject.member_method_classes.append(ccEventHandler)
        if pyEventHandler not in py.PyProject.member_method_classes:
            py.PyProject.member_method_classes.append(pyEventHandler)
        # register custom editors
        from .lib import ButtonAndTextCtrlEditor, FixTextCtrlEditor
        ButtonAndTextCtrlEditor()
        FixTextCtrlEditor()

    def setup_model_handlers(self):
        """Integrate the model elements inside the models view"""
        # update modelview
        ModelsView.extend_tree_order(
            [Design, ccEventHandler, pyEventHandler], cc.TypesFolder)
        ModelsView.alias_map[Design] = 'wxWidgets UI design "{0}"'
        ModelsView.alias_map[ccEventHandler] = 'wxWidgets event handler "{0}"'
        ModelsView.alias_map[pyEventHandler] = 'wxPython event handler "{0}"'
        ModelsView.edit_map.update({
            self.open_design: [Design],
            self.open_cc_event_handler: [ccEventHandler],
            self.open_py_event_handler: [pyEventHandler],
        })
        ModelsView.popup_map.update({
            self.on_design_popup: [Design],  # TODO : prevent default popup
            self.on_event_handler_popup: [ccEventHandler, pyEventHandler],
        })

        associations = {
            Design: (uiDialog, 'edit design {0}'),
        }
        from beatle.app.ui.tools import DIALOG_DICT
        DIALOG_DICT.update(associations)
        ModelsView.handlers.append(self)

    @property
    def can_edit_open(self):
        frame = context.get_frame()
        view = frame.viewBook.GetCurrentPage()
        if type(view) is ModelsView:
            if type(view.selected) is Design:
                return True
        return False

    def on_design_popup(self, event, obj):
        main_frame = api.context.get_frame()
        models_view = context.get_frame().get_view(ModelsView)
        popup_menu = wxx.Menu()
        popup_menu.Append(identifier("ID_EDIT_HIDE"), "Hide", "Hide this element and child from tree.")
        popup_menu.Append(identifier("ID_EDIT_SHOW"), "Show", "Show hidden child in tree.")
        popup_menu.AppendSeparator()
        popup_menu.Append(identifier("ID_EDIT_OPEN"), "Open ...", "Edit user code.")
        append_copy_of_menu_item(popup_menu, main_frame.editProperties)
        popup_menu.Popup(event.GetPosition(), owner=models_view)

    def on_event_handler_popup(self, event, obj):
        pass

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        from beatle.app.resources import update_xpm_dict
        update_xpm_dict(cls.image_map)

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        if cls.instance is None:
            cls.instance = wxUI()
        return cls.instance
    
    def append_tools_menu(self):
        # to do : remove usage of context in favor of api
        app_frame = context.get_frame()
        self._wxui_menu = api.ui.tools_menu_item(self._id_wxui_new, u"wxWidgets UI Designer",
                                                 u"create a new wxWidgets interface set.")
        self._wxui_menu.SetBitmap(wx.Bitmap(wx_design))
        app_frame.add_tools_separator()
        api.ui.add_tools_menu('wxUI', self._wxui_menu)
        
    def bind_events(self):
        self.Bind(wx.EVT_MENU, self.on_edit_open_selected, id=identifier("ID_EDIT_OPEN"))
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_add_wx_ui, id=self._id_wxui_new)
        self.Bind(wx.EVT_MENU, self.on_add_wx_ui, id=self._id_wxui_new)

    def on_edit_open_selected(self, event):
        """redirect the command to the model view.
        The view is not available while plugin initialization, so we need to use this redirector"""
        models_view = context.get_frame().get_view(ModelsView)
        if models_view is not None:
            models_view.on_edit_open_selected(event)

    def on_update_add_wx_ui(self, event):
        from beatle.activity.models.ui.view import ModelsView
        try:
            view = api.ui.current_view()
            if type(view) is not ModelsView:
                event.Enable(False)
            else:
                project = view.selected.project
                event.Enable(project.language in ENABLED_LANGUAGES)
        except:
            event.Enable(False)
            
    @TransactionalMethod('Add wxWidgets UI {0}')
    @wxx.CreationDialog(uiDialog, Design)
    def on_add_wx_ui(self, event):
        """Do add wxWidgets ui designer/generator"""
        main_frame = api.context.get_frame()
        self.project = None
        try:
            selected = main_frame.get_current_view().selected
            if selected and selected.project.language in ENABLED_LANGUAGES:
                return main_frame, selected
        except Exception as e:
            pass
        return False

    def open_design(self, module, view=None):
        """Handles the edition of the wxUI wizard as a document"""
        if getattr(module, '_pane', None):
            raise RuntimeError('Already open pane for module')
        main_frame = api.context.get_frame()
        pane = uiDesign(main_frame.docBook, main_frame, module)
        module._pane = pane
        main_frame.docBook.AddPage(pane, module.tab_label, True, module.bitmap_index)

    def open_cc_event_handler(self, handler, view=None):
        if getattr(handler, '_pane', None):
            raise RuntimeError('Already open pane for event handler')
        from beatle.activity.models.cc.ui.pane import MethodPane
        main_frame = api.context.get_frame()
        this_pane = MethodPane(main_frame.docBook, main_frame, handler)
        handler._pane = this_pane
        main_frame.docBook.AddPage(this_pane, handler.tab_label, True, bitmap=handler.get_tab_bitmap())

    def open_py_event_handler(self, handler, view=None):
        if getattr(handler, '_pane', None):
            raise RuntimeError('Already open pane for event handler')
        from beatle.activity.models.py.ui.pane import MethodPane
        main_frame = api.context.get_frame()
        this_pane = MethodPane(main_frame.docBook, main_frame, handler)
        handler._pane = this_pane
        main_frame.docBook.AddPage(this_pane, handler.tab_label, True, bitmap=handler.get_tab_bitmap())
