import types
import wx
import wx.propgrid as pg
from beatle.lib.handlers import identifier
from beatle.lib.api import volatile


def get_focus_event_handler(instance, event):
    volatile.set('pg.text_ctrl', instance)


def lose_focus_event_handler(instance, event):
    if volatile.get('pg.text_ctrl') == instance:
        volatile.set('pg.text_ctrl', None)


class FixTextCtrlEditor(pg.PGTextCtrlEditor):
    global_instance = None

    def __init__(self):
        super(FixTextCtrlEditor, self).__init__()
        FixTextCtrlEditor.global_instance = pg.PropertyGrid.DoRegisterEditorClass(self, self.Name)

    def CreateControls(self, propgrid, prop, pos, size):
        result = super(FixTextCtrlEditor, self).CreateControls(propgrid, prop, pos, size)
        if result is not None:
            # some race condition can lead to failed control creation
            control = result.Primary
            control.on_get_focus = types.MethodType(get_focus_event_handler, control)
            control.on_lose_focus = types.MethodType(lose_focus_event_handler, control)
            control.Bind(wx.EVT_SET_FOCUS, control.on_get_focus)
            control.Bind(wx.EVT_KILL_FOCUS, control.on_lose_focus)
        return result

    def GetName(self):
        return "FixTextCtrl"

    @property
    def Name(self):
        return self.GetName()


class FixStringProperty(pg.StringProperty):
    """Implements a long string property but with the edit button on the left side in order to avoid
    the annoying collisions with vertical scrollbar"""

    def __init__(self, label=pg.PG_LABEL, name=pg.PG_LABEL, value=wx.EmptyString):
        super(FixStringProperty, self).__init__(label=label, name=name, value=value)
        self.SetEditor("FixTextCtrl")

    def GetClassName(self):
        return "FixTextCtrl"

