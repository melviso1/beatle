import wx
import wx.propgrid as pg


class QuickColorProperty(pg.SystemColourProperty):
    """This class is a interface for handling font info as values."""

    def __init__(self, label, name, value):
        if type(value) is wx.SystemColour:
            converted_value = pg.ColourPropertyValue(value)
        else:
            converted_value = pg.ColourPropertyValue(wx.Colour(*value))
        super(QuickColorProperty, self).__init__(label, name, converted_value)

    def SetValue(self, value):
        if type(value) is wx.SystemColour:
            converted_value = pg.ColourPropertyValue(value)
        else:
            converted_value = pg.ColourPropertyValue(wx.Colour(*value))
        super(QuickColorProperty, self).SetValue(converted_value)

    def SetValueInEvent(self, value):
        if type(value) is wx.SystemColour:
            converted_value = pg.ColourPropertyValue(value)
        else:
            converted_value = pg.ColourPropertyValue(wx.Colour(*value))
        super(QuickColorProperty, self).SetValueInEvent(converted_value)

    def GetValue(self):
        value = super(QuickColorProperty, self).GetValue()
        if type(value) is pg.ColourPropertyValue:
            if value.m_type == 0xFFFFFF:  # This is a custom colour, user choosen
                return value.m_colour
            else:
                return wx.SystemColour(value.m_type)
        else:
            return value

