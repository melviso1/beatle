"""Some useful decorator for reducing code"""
import wx
from beatle.lib.tran import TransactionalMethod


def extend_events(name):
    """This class decorator gets a property by name that
    extends/override base events."""
    def internal(class_):
        @property
        def events(self):
            value = super(class_, self).events
            value.update(getattr(self, name))
            return value
        setattr(class_, 'events', events)
        return class_
    return internal


def with_style(class_):
    """Add the attributes required for handling styles"""
    @property
    def style_value(self):
        """Return the value required for dynamic tree creation"""
        return self.style_value_helper(class_.style_list, self._style) | super(class_, self).style_value

    @property
    def py_style_str(self):
        if self._style == 0:
            return super(class_, self).py_style_str
        sz = len(class_.style_list_py_text)
        control_style_value = '|'.join(
            class_.style_list_py_text[index] for index in range(sz)
            if self._style & (2 ** index)
        )
        base = super(class_, self).py_style_str
        if base == '0':
            return control_style_value
        return '|'.join((control_style_value, base))

    @property
    def cc_style_str(self):
        """Return a string suitable for the style declaration"""
        if self._style == 0:
            return super(class_, self).cc_style_str
        sz = len(class_.style_list_cc_text)
        control_style_value = '|'.join(
            class_.style_list_cc_text[index] for index in range(sz)
            if self._style & (2 ** index)
        )
        base = super(class_, self).cc_style_str
        if base == '0':
            return control_style_value
        return '|'.join((control_style_value, base))

    setattr(class_, 'style_value', style_value)
    setattr(class_, 'py_style_str', py_style_str)
    setattr(class_, 'cc_style_str', cc_style_str)
    return class_


def with_extra_style(class_):
    """Add the attribute py_extra_style_value_text to the class,
    allowing to recursively append base styles"""
    @property
    def py_extra_style_value_text(self):
        if self._extra_style == 0:
            return super(class_, self).py_extra_style_value_text
        sz = len(class_.extra_style_list_py_text)
        control_extra_style_value = '|'.join(
            class_.extra_style_list_py_text[index] for index in range(sz)
            if self._extra_style & (2 ** index)
        )
        base = super(class_, self).py_extra_style_value_text
        if base == '0':
            return control_extra_style_value
        return '|'.join((control_extra_style_value, base))

    setattr(class_, 'py_extra_style_value_text', py_extra_style_value_text)
    return class_


def add_control_method(class_, name):
    def wrap(method):
        @TransactionalMethod('add {name}'.format(name=name))
        def on_control(self, event):
            parent = self._selected_object is not None and \
                     self._selected_object.inner_true(lambda x: getattr(x, 'can_add_control', False))
            if parent is None:
                return False
            safe_name = name.strip().replace(' ', '_')
            self.tree_select(class_(
                parent=parent, name='_{name}_{n}'.format(name=safe_name, n=str(self.next_count(class_))),
                wxui_child_index=self.prepare_index(parent)))
            self._object.project.modified = True
            return True

        return on_control

    return wrap


def popup_errors_to_user(method):

    def wrap(self, *args, **kwargs):
        try:
            return method(self, *args, **kwargs)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index + 1:].strip()
            if len(message)>0:
                wx.MessageBox(message, 'Error', wx.OK | wx.ICON_ERROR)
            raise ValueError('')

    return wrap

