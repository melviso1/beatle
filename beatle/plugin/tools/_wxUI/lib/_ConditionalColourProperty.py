import wx
import wx.propgrid as pg
import re
from ._QuickColorProperty import QuickColorProperty


class Boolean(pg.BoolProperty):
    def __init__(self, parent, label=pg.PG_LABEL, name=pg.PG_LABEL, value=-1):
        self._parent = parent
        self._help = None
        if type(label) is tuple:
            self._label = label[0]
            self._help = label[1]
        else:
            self._label = label
        super(Boolean, self).__init__(label=self._label, name=name,value=value)
        self._parent.AppendChild(self)
        if self._help is not None:
            self.SetHelpString(self._help)

    def IntToValue(self, text, argFlags=0):
        ok, value = super(Boolean, self).IntToValue(text, argFlags)
        if ok:
            self._parent.update(self, value)
        return ok, value

    def SetValueInEvent(self, value):
        super(Boolean, self).SetValueInEvent(value)
        self._parent.update(self, value)


class _Colour(QuickColorProperty):
    def __init__(self, parent, label=pg.PG_LABEL, name=pg.PG_LABEL, value=-1):
        self._parent = parent
        self._help = None
        if type(label) is tuple:
            self._label = label[0]
            self._help = label[1]
        else:
            self._label = label
        super(_Colour, self).__init__(label=self._label, name=name,value=value)
        self._parent.AppendChild(self)
        if self._help is not None:
            self.SetHelpString(self._help)

    def SetValue(self, value):
        super(_Colour, self).SetValue(value)
        self._parent.update(self, value)

    def SetValueInEvent(self, value):
        super(_Colour, self).SetValueInEvent(value)
        self._parent.update(self, value)

    def StringToValue(self, text, argFlags=0):
        ok, value = super(_Colour, self).StringToValue(text, argFlags)
        if ok:
            self._parent.update(self, value)
        return ok, value

    # def IntToValue(self, text, argFlags=0):
    #     ok, value = super(_Colour, self).IntToValue(text, argFlags)
    #     if ok:
    #         if type(value) is pg.ColourPropertyValue:
    #             self._parent.update(self, value.m_colour)
    #     return ok, value


class ConditionalColourProperty(pg.PGProperty):
    """This class implements a property for position edition"""
    def __init__(self, label=pg.PG_LABEL, name=pg.PG_LABEL, labels=['change colour', 'colour'],
                 value=(False, wx.SYS_COLOUR_WINDOWTEXT)):
        assert len(labels) == len(value)
        self._value = value
        self._labels = labels
        super(ConditionalColourProperty, self).__init__(label=label, name=name)
        # Add child properties
        self._property_array = [
            Boolean(self, label=self._labels[0], value=self._value[0]),
            _Colour(self, label=self._labels[1], value=self._value[1])
        ]
        self._condition = self._property_array[0]

        self.SetValue(self._value)

    def __str__(self):
        return str(self._value)

    def update(self, ctrl, value):
        try:
            i = self._property_array.index(ctrl)
        except ValueError:
            return
        if self._value[i] == value:
            return
        aux = list(self._value)
        aux[i] = value
        self._value = tuple(aux)
        ctrl.SetValue(value)  # fix update
        self.SetValue(self._value)

    def ChildChanged(self,  old_value, index, child_value):
        if self._value[index] == child_value:
            return
        aux = list(self._value)
        aux[index] = child_value
        self._value = tuple(aux)
        return self._value

    def DoGetEditorClass(self):
        """
        Determines what editor should be used for this property type. This
        is one way to specify one of the stock editors.
        """
        return pg.PropertyGridInterface.GetEditorByName("TextCtrl")

    def ValueToString(self, value, flags):
        """
        Convert the given property value to a string.
        Some
        """
        if not value[0]:
            return str((False,)+tuple(value[1:]))
        return str((True,)+tuple(value[1:]))

    def StringToValue(self, st, flags):
        """
        Convert a string to the correct type for the propery.

        If failed, return False or (False, None). If success, return tuple
        (True, newValue).
        """
        try:
            match = re.match(r'\s*\(\s*(?P<content>[^\)]*)\s*\)\s*', st)
            if match is None:
                return False, self._value
            dim = len(self._labels)
            items = match.group('content').split(',')
            if len(items) != dim:
                return False, self._value
            try:
                self._value = tuple([eval(x) for x in items])
            except ValueError:
                return False, self._value
            for i in range(dim):
                self._property_array[i].SetValue(self._value[i])
        except (ValueError, TypeError):
            pass
        except:
            raise
        return True, self._value

    def GetValue(self):
        return tuple([self._property_array[i].GetValue() for i in range(2)])
