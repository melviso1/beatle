import wx.propgrid as pg
import re


class IntegerCoordinate(pg.IntProperty):
    def __init__(self, parent, label=pg.PG_LABEL, name=pg.PG_LABEL, value=-1):
        self._parent = parent
        self._help = None
        if type(label) is tuple:
            self._label = label[0]
            self._help = label[1]
        else:
            self._label = label
        super(IntegerCoordinate, self).__init__(label=self._label, name=name,value=value)
        self._parent.AppendChild(self)
        if self._help is not None:
            self.SetHelpString(self._help)

    def StringToValue(self, text, argFlags=0):
        ok, value = super(IntegerCoordinate, self).StringToValue(text, argFlags)
        if ok:
            self._parent.update(self, value)
        return ok, value


class CoordinateProperty(pg.PGProperty):
    """This class implements a property for position edition"""
    def __init__(self, label=pg.PG_LABEL, name=pg.PG_LABEL, labels=['x', 'y'], value=(-1,-1)):
        assert len(labels) == len(value)
        self._value = value
        self._labels = labels
        super( CoordinateProperty, self).__init__(label=label, name=name)
        # Add child properties
        self._property_array = [
            IntegerCoordinate(self, label=self._labels[i], value=self._value[i])
            for i in range(len(self._value))
        ]
        self.SetValue(self._value)

    def __str__(self):
        return str(self._value)

    def update(self, ctrl, value):
        try:
            i = self._property_array.index(ctrl)
        except ValueError:
            return
        if self._value[i] == value:
            return
        aux = list(self._value)
        aux[i] = value
        self._value = tuple(aux)
        ctrl.SetValue(value)  # fix update
        self.SetValue(self._value)

    def DoGetEditorClass(self):
        """
        Determines what editor should be used for this property type. This
        is one way to specify one of the stock editors.
        """
        return pg.PropertyGridInterface.GetEditorByName("TextCtrl")

    def ValueToString(self, value, flags):
        """
        Convert the given property value to a string.
        """
        return str(self)

    def StringToValue(self, st, flags):
        """
        Convert a string to the correct type for the propery.

        If failed, return False or (False, None). If success, return tuple
        (True, newValue).
        """
        try:
            match = re.match(r'\s*\(\s*(?P<content>[^\)]*)\s*\)\s*', st)
            if match is None:
                return False, self._value
            dim = len(self._labels)
            items = match.group('content').split(',')
            if len(items) != dim:
                return False, self._value
            try:
                self._value = tuple([int(x) for x in items])
            except ValueError:
                return False, self._value
            for i in range(dim):
                self._property_array[i].SetValue(self._value[i])
        except (ValueError, TypeError):
            pass
        except:
            raise
        return True, self._value

