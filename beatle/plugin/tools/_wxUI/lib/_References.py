""" wxWidgers dependency register
    -----------------------------

    We need some wxWidgets definitions like classses for constructing
    the ui models.
    Some classes can skipped using type instance aliases, but other
    usages, like for inheritance, aren't so simple.

    This file does two things: search for class definition inside the
    project, that match the required class name (user is responsible for
    bad matching) or create the dependency required.

    The dependency required classes will be created inside the structure

    + external dependency
        + wxWidgets
            ....

"""
import wx
from beatle.model import cc
from beatle.lib.utils import cached_type


def register_cpp_class(project, name):
    if project.language != 'c++':
        return None
    # ok, search inside project and don't create on-the-fly
    element = cached_type(project, name, False)
    if element is not None:
        if not type(element) is cc.Class:
            wx.LogError("Invalid type {}. Expected a class.".format(name))
            return None
        return element
    # ok, the type don't exist. We must create it
    dependencies_folder = next((x for x in project[cc.Folder] if x.name == "External dependencies"), None)
    if dependencies_folder is None:
        dependencies_folder = cc.Folder(
            parent = project,
            name = "External dependencies"
        )
    wx_widgets_folder = next((x for x in dependencies_folder[cc.Folder] if x.name == "wxWidgets library"), None)
    if wx_widgets_folder is None:
        wx_widgets_folder = cc.Folder(
            parent = dependencies_folder,
            name = "wxWidgets library"
        )
    element = cc.Class(
        parent = wx_widgets_folder,
        name = name,
        is_external=True
    )
    constructor = cc.Constructor(parent=element, access='public')
    cc.Argument(parent=constructor, name='parent', type=cc.typeinst(type_alias='wxWindow', ptr=True))
    cc.Argument(parent=constructor, name='id',
                type=cc.typeinst(type=cached_type(project,'wxWindowID'), const=True, ref=True),
                default='wxID_ANY',
                )
    if name in ['wxDialog', 'wxFrame', 'wxWizard']:
        cc.Argument(parent=constructor, name='title',
                    type=cc.typeinst(type=cached_type(project,'wxString'), const=True, ref=True),
                    default='""',
                    )
    if name in ['wxWizard']:
        cc.Argument(parent=constructor, name='bitmap',
                    type=cc.typeinst(type=cached_type(project, 'wxBitmap'), const=True, ref=True),
                    default='wxNullBitmap',
                    )

    cc.Argument(parent=constructor, name='pos',
                type=cc.typeinst(type=cached_type(project,'wxPoint'), const=True, ref=True),
                default='wxDefaultPosition',
                )
    if name not in ['wxWizard']:
        cc.Argument(parent=constructor, name='size',
                    type=cc.typeinst(type=cached_type(project,'wxSize'), const=True, ref=True),
                    default='wxDefaultSize',
                    )
    if name == 'wxListBox':
        cc.Argument(parent=constructor, name='n',
                    type=cc.typeinst(type=cached_type(project,'int'), const=False, ref=False),
                    default='0',
                    )
        cc.Argument(parent=constructor, name='choices',
                    type=cc.typeinst(type=cached_type(project,'wxString'),
                                     const=False, ref=False, array=True, arraySize=''),
                    default='nullptr',
                    )

    if name == 'wxFrame':
        style = 'wxDEFAULT_DIALOG_STYLE'
    elif name == 'wxDIALOG':
        style = 'wxDEFAULT_DIALOG_STYLE'
    else:
        style = 'wxTAB_TRAVERSAL'

    cc.Argument(parent=constructor, name='style',
                type=cc.typeinst(type=cached_type(project,'long')),
                default=style,
                )
    cc.Argument(parent=constructor, name='name',
                type=cc.typeinst(type=cached_type(project,'wxString'), const=True, ref=True),
                default=f'{name}NameStr',
                )
    return element


def register_cpp_dependencies(project):
    """For building a wxWidgets project, we need some
    dependencies to be satisfied"""
    # testing solution, needs rework
    cpp_flags = project.current_build_config._cpp_compiler._other_flags
    if "wx-config --cppflags" not in cpp_flags:
        project.current_build_config._cpp_compiler._other_flags += '`wx-config --cppflags`'
    link_flags = project.current_build_config._linker._flags
    if "wx-config --libs" not in link_flags:
        project.current_build_config._linker._flags += '`wx-config --libs`'



