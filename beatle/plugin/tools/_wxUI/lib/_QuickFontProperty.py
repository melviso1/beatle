import wx
import wx.propgrid as pg


class QuickFontProperty(pg.FontProperty):
    """This class is an interface for handling font info as values."""

    def __init__(self, label, name, value):
        converted_value = wx.Font()
        converted_value.SetNativeFontInfo(value)
        super(QuickFontProperty, self).__init__(label, name, converted_value)

    def SetValue(self, value):
        converted_value = wx.Font()
        converted_value.SetNativeFontInfo(value)
        super(QuickFontProperty, self).SetValue(converted_value)

    def SetValueInEvent(self, value):
        converted_value = wx.Font()
        converted_value.SetNativeFontInfo(value)
        super(QuickFontProperty, self).SetValueInEvent(converted_value)

    def GetValue(self):
        value = super(QuickFontProperty, self).GetValue()
        if type(value) is wx.Font:
            return value.GetNativeFontInfo().ToString()
        return value

