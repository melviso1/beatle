import wx.propgrid as pg
import re


class EditChoiceProperty(pg.EditEnumProperty):
    """This class is a specialization of EnumProperty, intended
    to use only string as options and result"""

    def __init__(self, label=pg.PG_LABEL, name=pg.PG_LABEL, choices=[], value=''):
        self._choices = choices
        self._value = value
        super(EditChoiceProperty, self).__init__(
            label=label, name=name, labels=choices, values=[x for x in range(len(choices))], value=value)

    def __str__(self):
        return str(self._value)

    def DoGetValue(self):
        return str(self)

    def StringToValue(self, text, flags):
        self._value = text
        return True, text

    def IntToValue(self, value, flags):
        """
        Convert the given property value to a string.
        """
        if value < 0 or value > len(self._choices):
            return False,  self._value
        self._value = self._choices[value]
        return True, self._value

    def ValueToString(self, value, flags):
        return str(self)