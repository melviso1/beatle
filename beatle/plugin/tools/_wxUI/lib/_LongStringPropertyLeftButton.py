import wx
import wx.propgrid as pg


class ButtonAndTextCtrlEditor(pg.PGTextCtrlAndButtonEditor):
    global_instance = None

    def __init__(self):
        super(ButtonAndTextCtrlEditor, self).__init__()
        ButtonAndTextCtrlEditor.global_instance = pg.PropertyGrid.DoRegisterEditorClass(self, self.Name)

    def CreateControls(self, propgrid, property, pos, size):
        result = super(ButtonAndTextCtrlEditor, self).CreateControls(propgrid, property, pos, size)
        primary = result.Primary
        secondary = result.Secondary
        pos1 = primary.GetPosition()
        pos2 = secondary.GetPosition()
        sz = pos2.x - pos1.x
        pos2.x = pos2.x + secondary.GetSize().x - sz
        primary.SetPosition(pos2)
        sz1 = primary.GetSize()
        sz2 = secondary.GetSize()
        sz2.x = sz2.x-2
        sz2.y = sz1.y-2
        secondary.SetPosition(pos1)
        secondary.SetSize(sz2)
        return result

    def GetName(self):
        return "ButtonAndTextCtrl"

    @property
    def Name(self):
        return self.GetName()


class LongStringPropertyLeftButton(pg.LongStringProperty):
    """Implements a long string property but with the edit button on the left side in order to avoid
    the annoying collisions with vertical scrollbar"""

    def __init__(self, label=pg.PG_LABEL, name=pg.PG_LABEL, value=wx.EmptyString):
        super(LongStringPropertyLeftButton, self).__init__(label=label, name=name, value=value)
        self.SetEditor("ButtonAndTextCtrl")

    def GetClassName(self):
        return "LongStringPropertyLeftButton"

