"""This is a little decorator used for showing to the user some kind of runtime errors"""
import wx


def popup_errors(function):
    """Execute a function and show a popup to the user, with information
    about the error. This is designed specifically for use with create_window
    methods and show why this fail. This makes easy to design some parts that
     required the user to know what he's doing. For example, trying to add a
     control to a grid sizer that has not enough rows*columns to hold another
     child item."""
    def decorated(*args, **kwargs):
        try:
            function(*args, **kwargs)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index + 1:].strip()
            wx.MessageBox(message, 'Error', wx.OK | wx.ICON_ERROR)
            raise exception
    return decorated
