import wx
import wx.propgrid as pg
import re


class BitmapProperty(pg.PGProperty):
    """The bitmap file property has blurry bug and also we need to offer here
    the different possibilities to the user."""

    options = ['Load from file',
               'Load from art provider',
               'Load from embedded file',
               'Load from bitmap resource',
               'Load from icon resource']
    artid_map = {
        'wxART_ADD_BOOKMARK': wx.ART_ADD_BOOKMARK,
        'wxART_CDROM': wx.ART_CDROM,
        'wxART_CLOSE': wx.ART_CLOSE,
        'wxART_COPY': wx.ART_COPY,
        'wxART_CROSS_MARK': wx.ART_CROSS_MARK,
        'wxART_CUT': wx.ART_CUT,
        'wxART_DEL_BOOKMARK': wx.ART_DEL_BOOKMARK,
        'wxART_DELETE': wx.ART_DELETE,
        'wxART_ERROR': wx.ART_ERROR,
        'wxART_EXECUTABLE_FILE': wx.ART_EXECUTABLE_FILE,
        'wxART_FILE_OPEN': wx.ART_FILE_OPEN,
        'wxART_FILE_SAVE': wx.ART_FILE_SAVE,
        'wxART_FILE_SAVE_AS': wx.ART_FILE_SAVE_AS,
        'wxART_FIND': wx.ART_FIND,
        'wxART_FIND_AND_REPLACE': wx.ART_FIND_AND_REPLACE,
        'wxART_FLOPPY': wx.ART_FLOPPY,
        'wxART_FOLDER': wx.ART_FOLDER,
        'wxART_FOLDER_OPEN': wx.ART_FOLDER_OPEN,
        'wxART_GO_BACK': wx.ART_GO_BACK,
        'wxART_GO_DIR_UP': wx.ART_GO_DIR_UP,
        'wxART_GO_DOWN': wx.ART_GO_DOWN,
        'wxART_GO_FORWARD': wx.ART_GO_FORWARD,
        'wxART_GO_HOME': wx.ART_GO_HOME,
        'wxART_GO_TO_PARENT': wx.ART_GO_TO_PARENT,
        'wxART_GO_UP': wx.ART_GO_UP,
        'wxART_GOTO_FIRST': wx.ART_GOTO_FIRST,
        'wxART_GOTO_LAST': wx.ART_GOTO_LAST,
        'wxART_HARDDISK': wx.ART_HARDDISK,
        'wxART_HELP': wx.ART_HELP,
        'wxART_HELP_BOOK': wx.ART_HELP_BOOK,
        'wxART_HELP_FOLDER': wx.ART_HELP_FOLDER,
        'wxART_HELP_PAGE': wx.ART_HELP_PAGE,
        'wxART_HELP_SETTINGS': wx.ART_HELP_SETTINGS,
        'wxART_HELP_SIDE_PANEL': wx.ART_HELP_SIDE_PANEL,
        'wxART_INFORMATION': wx.ART_INFORMATION,
        'wxART_LIST_VIEW': wx.ART_LIST_VIEW,
        'wxART_MINUS': wx.ART_MINUS,
        'wxART_MISSING_IMAGE': wx.ART_MISSING_IMAGE,
        'wxART_NEW': wx.ART_NEW,
        'wxART_NEW_DIR': wx.ART_NEW_DIR,
        'wxART_NORMAL_FILE': wx.ART_NORMAL_FILE,
        'wxART_PASTE': wx.ART_PASTE,
        'wx.ART_PLUS': wx.ART_PLUS,
        'wxART_PRINT': wx.ART_PRINT,
        'wxART_QUESTION': wx.ART_QUESTION,
        'wxART_QUIT': wx.ART_QUIT,
        'wxART_REDO': wx.ART_REDO,
        'wxART_REMOVABLE': wx.ART_REMOVABLE,
        'wxART_REPORT_VIEW': wx.ART_REPORT_VIEW,
        'wxART_TICK_MARK': wx.ART_TICK_MARK,
        'wxART_TIP': wx.ART_TIP,
        'wxART_UNDO': wx.ART_UNDO,
        'wxART_WARNING': wx.ART_WARNING,
    }
    client_id_map = {
        'wxART_TOOLBAR': wx.ART_TOOLBAR,
        'wxART_MENU': wx.ART_MENU,
        'wxART_FRAME_ICON': wx.ART_FRAME_ICON,
        'wxART_CMN_DIALOG': wx.ART_CMN_DIALOG,
        'wxART_HELP_BROWSER': wx.ART_HELP_BROWSER,
        'wxART_MESSAGE_BOX': wx.ART_MESSAGE_BOX,
        'wxART_BUTTON': wx.ART_BUTTON
    }

    @classmethod
    def artid_value(cls, index):
        return list(cls.artid_map.values())[index]

    @classmethod
    def artid_key(cls, index):
        return list(cls.artid_map.keys())[index]

    @classmethod
    def artclientid_value(cls, index):
        return list(cls.client_id_map.values())[index]

    @classmethod
    def artclientid_key(cls, index):
        return list(cls.client_id_map.keys())[index]

    def __init__(self, label=pg.PG_LABEL, name=pg.PG_LABEL, value=(), path=None):
        """Here, the value is a tupla. (indicator, ...)
        indicator specifies what kind of bitmap image we can select. Accordingly with
        wxformbuilder we must be able to select the following options:

            0 : Load from file
            1 : Load from embedded file (c++)
            2 : Load from bitmap resource (c++)
            3 : Load from icon resource (c++)
            4 : Load from art provider

        For the moment, we don't implement options 1-3 (exclusive to c++)
        and (2-4) rarely used and platform-dependent. Option 1 will be done later
        because requires more effort and will be usable for both python and c++
        (this option is used intesively in beatle by hand.

        0-Load from file
        ----------------
        This case, the property will show a bitmap file selection and the resulting tuple
        will be of the form (1, filename:string)

        4-Load from art provider
        This case the tuple must be of the form (1, art_id:integer, client_id:integer)

        """
        self._value = value
        self._method_property = None
        self._file_property = None
        self._art_id_property = None
        self._art_id_client = None

        # self._file_property = pg.ImageFileProperty(label='file', name='image_file', value='')
        self._file_property = pg.FileProperty(label='file', name='image_file', value='')
        self._file_property.SetAttribute(
            pg.PG_FILE_WILDCARD, "image files (*.bmp;*.gif;*.xpm;*.png; *.jpg)|*.bmp;*.gif;*.png;*.xpm;*.png;*.jpg")

        if path is not None:
            self._file_property.SetAttribute(
                pg.PG_FILE_INITIAL_PATH, path
            )
            self._file_property.SetAttribute(
                pg.PG_FILE_SHOW_RELATIVE_PATH, True
            )
            self._file_property.SetAttribute(
                pg.PG_FILE_SHOW_FULL_PATH, False
            )

        artid_labels = list(BitmapProperty.artid_map.keys())
        self._art_id_property = pg.EnumProperty(
            label='id', name='id', labels=artid_labels,
            values=[x for x in range(len(artid_labels))])
        clientid_labels = list(BitmapProperty.client_id_map.keys())
        self._art_id_client = pg.EnumProperty(
            label='client', name='client', labels=clientid_labels,
            values=[x for x in range(len(clientid_labels))])
        self._file_property.ChangeFlag(pg.PG_PROP_HIDDEN, True)
        self._art_id_property.ChangeFlag(pg.PG_PROP_HIDDEN, True)
        self._art_id_client.ChangeFlag(pg.PG_PROP_HIDDEN, True)
        pg.PGProperty.__init__(self, label, name)

    def Initialize(self):
        self.GetGrid().AppendIn(self, self._file_property)
        self.GetGrid().AppendIn(self, self._art_id_property)
        self.GetGrid().AppendIn(self, self._art_id_client)
        self.SetChoices(pg.PGChoices(['Load from file', 'Load from art provider'], [0, 1]))
        selection = self._value[0]
        if selection == 0:
            self._file_property.SetValue(self._value[1])
            self._value = (-1, self._value[1])
        else:
            self._art_id_property.SetChoiceSelection(self._value[1])
            self._art_id_client.SetChoiceSelection(self._value[2])
            self._value = (-1, self._value[1], self._value[2])
        self.SetChoiceSelection(selection)

    def OnEvent(self, propgrid, wnd_primary, event):
        # event.Skip()
        if type(event) in (
                wx.ChildFocusEvent, wx.FocusEvent, wx.SizeEvent, wx.EraseEvent, wx.PaintEvent, wx.UpdateUIEvent,
                wx.IdleEvent, wx.SetCursorEvent, wx.MouseEvent, wx.CommandEvent, wx.WindowCreateEvent):
            return super(BitmapProperty, self).OnEvent(propgrid, wnd_primary, event)
        if event.ClassName == 'wxNcPaintEvent':
            return super(BitmapProperty, self).OnEvent(propgrid, wnd_primary, event)
        return False

    def ChildChanged(self, thisValue, childIndex, childValue):
        # when child changes
        if childIndex == 0:
            self._file_property.SetValue(childValue)
        if childIndex == 1:
            self._art_id_property.SetValue(childValue)
        if childIndex == 2:
            self._art_id_client.SetValue(childValue)
        pass

    def DoGetValidator(self):
        return None

    def OnSetValue(self):
        new_value = self.GetValue()
        if self._value != new_value:
            if new_value[0] == 0:
                # This is a load from file bitmap
                self._art_id_property.ChangeFlag(pg.PG_PROP_HIDDEN, True)
                self._art_id_client.ChangeFlag(pg.PG_PROP_HIDDEN, True)
                self._file_property.ChangeFlag(pg.PG_PROP_HIDDEN, False)
            if new_value[0] == 1:
                self._file_property.ChangeFlag(pg.PG_PROP_HIDDEN, True)
                self._art_id_property.ChangeFlag(pg.PG_PROP_HIDDEN, False)
                self._art_id_client.ChangeFlag(pg.PG_PROP_HIDDEN, False)
            self._value = new_value

    def GetValue(self):
        if self.m_value == 0:
            return 0, self._file_property.m_value
        elif self.m_value == 1:
            return 1, self._art_id_property.m_value, self._art_id_client.m_value
        return -1,-1


    def DoGetEditorClass(self):
        self._method_property = pg.PropertyGridInterface.GetEditorByName("Choice")
        return self._method_property

    def ValueToString(self, value, flags):
        """
        Convert the given property value to a string.
        """
        return BitmapProperty.options[value]

    def StringToValue(self, st, flags):
        """
                Convert a string to the correct type for the property.

                If failed, return False or (False, None). If success, return tuple
                (True, newValue).
                """
        try:
            val = BitmapProperty.options.index(st)
            return (True, val)
        except (ValueError, TypeError):
            pass
        except:
            raise
        return (False, None)
