import os
import wx


def get_colour(name, kwargs):
    """Get the color value. Return False if color is not specified."""
    value = kwargs.get(name, (False, wx.SYS_COLOUR_WINDOW))
    if not value[0]:
        return False
    value = value[1]
    if type(value) is wx.SystemColour:
        return wx.SystemSettings.GetColour(value)
    return value


def sanitize_name(str):
    """Return a snaitized name used for class names"""
    return str.strip().replace(' ', '_').replace('\t', '_')


def get_bitmap_from_tuple(t, project=None):
    """return the corresponding bitmap to a tuple"""
    from ._BitmapProperty import BitmapProperty
    if t[0] == 0:
        path = t[1]
        if len(path) == 0:
            return None
        else:
            cwd = None
            if project:
                cwd = os.getcwd()
                # temporally change working dir in order to resolve resources
                os.chdir(project.dir)
            bitmap = wx.Bitmap(path, wx.BITMAP_TYPE_ANY)
            if project:
                os.chdir(cwd)
            if not bitmap.IsOk():
                return None
    elif t[0] == 1:
        bitmap = wx.ArtProvider.GetBitmap(
            BitmapProperty.artid_value(t[1]),
            BitmapProperty.artclientid_value(t[2]))
        if not bitmap.IsOk():
            return None
    else:
        return None
    return bitmap


def get_bitmap_py_text_from_tuple(t):
    """return the corresponding bitmap code from a tuple"""
    from ._BitmapProperty import BitmapProperty
    if t[0] == 0:
        path = t[1]
        if len(path) == 0:
            return ''
        else:
            return 'wx.Bitmap("{path}", wx.BITMAP_TYPE_ANY)'.format(path=path)
    elif t[0] == 1:
        art_id = BitmapProperty.artid_key(t[1])
        art_id = 'wx.'+art_id[2:]
        client_id = BitmapProperty.artclientid_key(t[2])
        client_id = 'wx.'+client_id[2:]
        return 'wx.ArtProvider.GetBitmap({art_id}, {client_id})'.format(
            art_id=art_id, client_id=client_id)
    return ''


def get_bitmap_cpp_text_from_tuple(t):
    """return the corresponding bitmap code from a tuple"""
    from ._BitmapProperty import BitmapProperty
    if t[0] == 0:
        path = t[1]
        if len(path) == 0:
            return ''
        else:
            return 'wxBitmap("{path}", wxBITMAP_TYPE_ANY)'.format(path=path)
    elif t[0] == 1:
        art_id = BitmapProperty.artid_key(t[1])
        client_id = BitmapProperty.artclientid_key(t[2])
        return 'wxArtProvider::GetBitmap({art_id}, {client_id})'.format(
            art_id=art_id, client_id=client_id)
    return ''


def get_colour_from_tuple(value):
    """Get a string suitable for python colour"""
    from ._Conversions import py_syscolour_name
    if not value[0]:
        return None
    value = value[1]
    if type(value) is wx.SystemColour:
        return wx.SystemSettings.GetColour(value)
    return value


def get_colour_py_text_from_tuple(t):
    """Get a string suitable for python colour"""
    from ._Conversions import py_syscolour_name
    if not t[0]:
        return ''
    if type(t[1]) is wx.SystemColour:
        name = py_syscolour_name(t[1])
        if name is None:
            return ''
        return 'wx.SystemSettings.GetColour({name})'.format(name=name)
    else:
        return 'wx.Colour{colour}'.format(colour=str(t[1]))


def get_colour_cc_text_from_tuple(t):
    """Get a string suitable for python colour"""
    from ._Conversions import cpp_syscolour_name
    if not t[0]:
        return ''
    if type(t[1]) is wx.SystemColour:
        name = cpp_syscolour_name(t[1])
        if name is None:
            return ''
        return 'wxSystemSettings::GetColour({name})'.format(name=name)
    else:
        return 'wxColour{colour}'.format(colour=str(t[1]))
