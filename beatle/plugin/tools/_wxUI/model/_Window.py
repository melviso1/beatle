import copy
import wx
from beatle.model import cc, py
from ._wxUICommon import wxUICommon
from ._ccEventHandler import ccEventHandler
from ._pyEventHandler import pyEventHandler
from .controls._tools import *
from beatle.lib.handlers import identifier
from ..lib import get_colour_py_text_from_tuple, get_colour_cc_text_from_tuple
from beatle.lib.decorators import upgrade_version


class Window(wxUICommon):
    """This class represents a base window"""
    window_style_list = (
        wx.ALWAYS_SHOW_SB, wx.CLIP_CHILDREN, wx.DOUBLE_BORDER, wx.FULL_REPAINT_ON_RESIZE, wx.HSCROLL,wx.NO_BORDER,
        wx.RAISED_BORDER, wx.SIMPLE_BORDER, wx.STATIC_BORDER, wx.SUNKEN_BORDER, wx.TAB_TRAVERSAL, wx.TRANSPARENT_WINDOW,
        wx.VSCROLL, wx.WANTS_CHARS)

    window_extra_style_list = (
        wx.WS_EX_BLOCK_EVENTS, wx.WS_EX_PROCESS_IDLE, wx.WS_EX_PROCESS_UI_UPDATES, wx.WS_EX_TRANSIENT,
        wx.WS_EX_VALIDATE_RECURSIVELY)

    py_window_style_list_text = (
        'wx.ALWAYS_SHOW_SB', 'wx.CLIP_CHILDREN', 'wx.DOUBLE_BORDER', 'wx.FULL_REPAINT_ON_RESIZE', 'wx.HSCROLL',
        'wx.NO_BORDER', 'wx.RAISED_BORDER', 'wx.SIMPLE_BORDER', 'wx.STATIC_BORDER', 'wx.SUNKEN_BORDER',
        'wx.TAB_TRAVERSAL', 'wx.TRANSPARENT_WINDOW', 'wx.VSCROLL', 'wx.WANTS_CHARS')

    py_window_extra_style_list_text = (
        'wx.WS_EX_BLOCK_EVENTS', 'wx.WS_EX_PROCESS_IDLE', 'wx.WS_EX_PROCESS_UI_UPDATES', 'wx.WS_EX_TRANSIENT',
        'wx.WS_EX_VALIDATE_RECURSIVELY')

    cc_window_style_list_text = (
        'wxALWAYS_SHOW_SB', 'wxCLIP_CHILDREN', 'wxDOUBLE_BORDER', 'wxFULL_REPAINT_ON_RESIZE', 'wxHSCROLL',
        'wxNO_BORDER', 'wxRAISED_BORDER', 'wxSIMPLE_BORDER', 'wxSTATIC_BORDER', 'wxSUNKEN_BORDER',
        'wxTAB_TRAVERSAL', 'wxTRANSPARENT_WINDOW', 'wxVSCROLL', 'wxWANTS_CHARS')

    cc_window_extra_style_list_text = (
        'wxWS_EX_BLOCK_EVENTS', 'wxWS_EX_PROCESS_IDLE', 'wxWS_EX_PROCESS_UI_UPDATES', 'wxWS_EX_TRANSIENT',
        'wxWS_EX_VALIDATE_RECURSIVELY')

    def __init__(self, **kwargs):
        from .controls.layout import GridBagSizerControl
        from .controls.containers import NotebookControl, AuiNotebookControl, ListbookControl, \
            ChoicebookControl, SimplebookControl

        # -- initialize data  with default values
        self._id = 'wxID_ANY'
        self._pos = (-1, -1)
        self._size = (-1, -1)
        self._minimum_size = (-1, -1)
        self._maximum_size = (-1, -1)
        self._font_info = (False, '-1; Default; ; Normal; Normal; Not Underlined')
        self._foreground_colour = (False, wx.SYS_COLOUR_WINDOWTEXT)
        self._background_colour = (False, wx.SYS_COLOUR_WINDOWFRAME)
        self._window_name = ''
        self._window_style = 0
        self._window_extra_style = 0
        self._tooltip = ''
        self._context_menu = True
        self._context_help = ''
        self._enabled = True
        self._hidden = False
        self._subclass = ''
        self._subitem_adaptor = None  # special handling when the window is inside a special container
        self._tab_container_adaptor = None
        # -- initialize event handlers
        #    - key events
        self._on_char = ''
        self._on_key_down = ''
        self._on_key_up = ''
        #    - mouse events
        self._on_enter_window = ''
        self._on_leave_window = ''
        self._on_left_button_double_click = ''
        self._on_left_button_down = ''
        self._on_left_button_up = ''
        self._on_middle_button_double_click = ''
        self._on_middle_button_down = ''
        self._on_middle_button_up = ''
        self._on_mouse_move = ''
        self._on_mouse_events = ''
        self._on_mouse_wheel = ''
        self._on_right_button_double_click = ''
        self._on_right_button_down = ''
        self._on_right_button_up = ''
        #    - windows events
        self._on_set_focus = ''
        self._on_kill_focus = ''
        self._on_paint = ''
        self._on_erase_background = ''
        self._on_size = ''
        self._on_update_ui = ''

        # -- working variables
        self._wxui_event_handlers = {}
        #  the  base class will call the undo create and this one will trigger
        #  the visual editor, so we must determine first if this is a sizer item child
        self._parent = kwargs.get('parent', None)
        # if self._parent is not None:
        #     if getattr(self._parent, 'is_sizer', False):
        #         if type(self._parent) is GridBagSizerControl:
        #             self._subitem_adaptor = BagSizerItemAdaptor(control=self)
        #         else:
        #             self._subitem_adaptor = SizerItemAdaptor(control=self)
        #     elif type(self._parent) is NotebookControl:
        #         self._subitem_adaptor = NotebookContainerAdaptor(control=self)
        #     elif type(self._parent) is ListbookControl:
        #         self._subitem_adaptor = ListbookContainerAdaptor(control=self)
        #     elif type(self._parent) is AuiNotebookControl:
        #         self._subitem_adaptor = AuiNotebookContainerAdaptor(control=self)
        #     elif type(self._parent) is ChoicebookControl:
        #         self._subitem_adaptor = ChoicebookContainerAdaptor(control=self)
        #     elif type(self._parent) is SimplebookControl:
        #         self._subitem_adaptor = SimplebookContainerAdaptor(control=self)
        self._implementation = None
        self._custom_identifier = None
        super(Window, self).__init__(**kwargs)
        self.set_kwargs(kwargs)
        # self.implement()

    def __getstate__(self):
        """Set picke context: prevent
        attempt to serialize edition windows"""
        state = dict(self.__dict__)
        if '_edition_window' in state:
            state['_edition_window'] = None
        return state

    @upgrade_version
    def __setstate__(self, data_dict):
        return {
            'add': {
                '_custom_identifier': None,
            },
        }

    @property
    def implementation(self):
        return self._implementation

    @property
    def subitem_adaptor(self):
        return self._subitem_adaptor

    @property
    def local_window_kwargs(self):
        return {
            'id': self._id,
            'pos': self._pos,
            'size': self._size,
            'minimum_size': self._minimum_size,
            'maximum_size': self._maximum_size,
            'font_info': self._font_info,
            'foreground_colour': self._foreground_colour,
            'background_colour': self._background_colour,
            'window_name': self._window_name,
            'window_style': self._window_style,
            'window_extra_style': self._window_extra_style,
            'tooltip': self._tooltip,
            'context_menu': self._context_menu,
            'context_help': self._context_help,
            'enabled': self._enabled,
            'hidden': self._hidden,
            'subclass': self._subclass,
            # events
            'on_char': self._on_char,
            'on_key_down': self._on_key_down,
            'on_key_up': self._on_key_up,
            'on_enter_window': self._on_enter_window,
            'on_leave_window': self._on_leave_window,
            'on_left_button_double_click': self._on_left_button_double_click,
            'on_left_button_down': self._on_left_button_down,
            'on_left_button_up': self._on_left_button_up,
            'on_middle_button_double_click': self._on_middle_button_double_click,
            'on_middle_button_down': self._on_middle_button_down,
            'on_middle_button_up': self._on_middle_button_up,
            'on_mouse_move': self._on_mouse_move,
            'on_mouse_events': self._on_mouse_events,
            'on_mouse_wheel': self._on_mouse_wheel,
            'on_right_button_double_click': self._on_right_button_double_click,
            'on_right_button_down': self._on_right_button_down,
            'on_right_button_up': self._on_right_button_up,
            'on_set_focus': self._on_set_focus,
            'on_kill_focus': self._on_kill_focus,
            'on_paint': self._on_paint,
            'on_erase_background': self._on_erase_background,
            'on_size': self._on_size,
            'on_update_ui': self._on_update_ui,
        }

    @local_window_kwargs.setter
    def local_window_kwargs(self, kwargs):
        """This method sets the local values."""
        self._id = kwargs.get('id', self._id)
        self._pos = tuple(kwargs.get('pos', self._pos))
        self._size = tuple(kwargs.get('size', self._size))
        self._minimum_size = tuple(kwargs.get('minimum_size', self._minimum_size))
        self._maximum_size = tuple(kwargs.get('maximum_size', self._maximum_size))
        self._font_info = tuple(kwargs.get('font_info', self._font_info))
        self._foreground_colour = tuple(kwargs.get('foreground_colour', self._foreground_colour))
        self._background_colour = kwargs.get('background_colour', self._background_colour)
        self._window_name = kwargs.get('window_name', self._window_name)
        self._window_style = kwargs.get('window_style', self._window_style)
        self._window_extra_style = kwargs.get('window_extra_style', self._window_extra_style)
        self._tooltip = kwargs.get('tooltip', self._tooltip)
        self._context_menu = kwargs.get('context_menu', self._context_menu)
        self._context_help = kwargs.get('context_help', self._context_help)
        self._enabled = kwargs.get('enabled', self._enabled)
        self._hidden = kwargs.get('hidden', self._hidden)
        self._subclass = kwargs.get('subclass', self._subclass)

        self._on_char = kwargs.get('on_char', self._on_char)
        self._on_key_down = kwargs.get('on_key_down', self._on_key_down)
        self._on_key_up = kwargs.get('on_key_up', self._on_key_up)
        self._on_enter_window = kwargs.get('on_enter_window', self._on_enter_window)
        self._on_leave_window = kwargs.get('on_leave_window', self._on_leave_window)
        self._on_left_button_double_click = kwargs.get(
            'on_left_button_double_click', self._on_left_button_double_click)
        self._on_left_button_down = kwargs.get('on_left_button_down', self._on_left_button_down)
        self._on_left_button_up = kwargs.get('on_left_button_up', self._on_left_button_up)
        self._on_middle_button_double_click = kwargs.get(
            'on_middle_button_double_click', self._on_middle_button_double_click)
        self._on_middle_button_down = kwargs.get('on_middle_button_down', self._on_middle_button_down)
        self._on_middle_button_up = kwargs.get('on_middle_button_up', self._on_middle_button_up)
        self._on_mouse_move = kwargs.get('on_mouse_move', self._on_mouse_move)
        self._on_mouse_events = kwargs.get('on_mouse_events', self._on_mouse_events)
        self._on_mouse_wheel = kwargs.get('on_mouse_wheel', self._on_mouse_wheel)
        self._on_right_button_double_click = kwargs.get(
            'on_right_button_double_click', self._on_right_button_double_click)
        self._on_right_button_down = kwargs.get('on_right_button_down', self._on_right_button_down)
        self._on_right_button_up = kwargs.get('on_right_button_up', self._on_right_button_up)
        self._on_set_focus = kwargs.get('on_set_focus', self._on_set_focus)
        self._on_kill_focus = kwargs.get('on_kill_focus', self._on_kill_focus)
        self._on_paint = kwargs.get('on_paint', self._on_paint)
        self._on_erase_background = kwargs.get('on_erase_background', self._on_erase_background)
        self._on_size = kwargs.get('on_size', self._on_size)
        self._on_update_ui = kwargs.get('on_update_ui', self._on_update_ui)


    @property
    def local_window_events(self):
        return {
            'on_char': (self._on_char, 'wx.EVT_CHAR', 'wxEVT_CHAR'),
            'on_key_down': (self._on_key_down, 'wx.EVT_KEY_DOWN', 'wxEVT_KEY_DOWN'),
            'on_key_up': (self._on_key_up, 'wx.EVT_KEY_UP', 'wxEVT_KEY_UP'),
            'on_enter_window': (self._on_enter_window, 'wx.EVT_ENTER_WINDOW', 'wxEVT_ENTER_WINDOW'),
            'on_leave_window': (self._on_leave_window, 'wx.EVT_LEAVE_WINDOW', 'wxEVT_LEAVE_WINDOW'),
            'on_left_button_double_click': (self._on_left_button_double_click,'wx.EVT_LEFT_DCLICK', 'wxEVT_LEFT_DCLICK'),
            'on_left_button_down': (self._on_left_button_down, 'wx.EVT_LEFT_DOWN', 'wxEVT_LEFT_DOWN'),
            'on_left_button_up': (self._on_left_button_up, 'wx.EVT_LEFT_UP', 'wxEVT_LEFT_UP'),
            'on_middle_button_double_click': (self._on_middle_button_double_click, 'wx.EVT_MIDDLE_DCLICK',
                                              'wxEVT_MIDDLE_DCLICK'),
            'on_middle_button_down': (self._on_middle_button_down,'wx.EVT_MIDDLE_DOWN', 'wxEVT_MIDDLE_DOWN'),
            'on_middle_button_up': (self._on_middle_button_up,'wx.EVT_MIDDLE_UP', 'wxEVT_MIDDLE_UP'),
            'on_mouse_move': (self._on_mouse_move,'wx.EVT_MOTION', 'wxEVT_MOTION'),
            'on_mouse_events': (self._on_mouse_events, 'wx.EVT_MOUSE', 'wxEVT_MOUSE'),
            'on_mouse_wheel': (self._on_mouse_wheel, 'wx.EVT_MOUSEWHEEL', 'wxEVT_MOUSEWHEEL'),
            'on_right_button_double_click': (self._on_right_button_double_click,'wx.EVT_RIGHT_DCLICK',
                                             'wxEVT_RIGHT_DCLICK'),
            'on_right_button_down': (self._on_right_button_down,'wx.EVT_RIGHT_DOWN', 'wxEVT_RIGHT_DOWN'),
            'on_right_button_up': (self._on_right_button_up,'wx.EVT_RIGHT_UP', 'wxEVT_RIGHT_UP'),
            'on_set_focus': (self._on_set_focus, 'wx.EVT_SET_FOCUS', 'wxEVT_SET_FOCUS'),
            'on_kill_focus': (self._on_kill_focus, 'wx.EVT_KILL_FOCUS', 'wxEVT_KILL_FOCUS'),
            'on_paint': (self._on_paint, 'wx.EVT_PAINT', 'wxEVT_PAINT'),
            'on_erase_background': (self._on_erase_background, 'wx.EVT_ERASE_BACKGROUND', 'wxEVT_ERASE_BACKGROUND'),
            'on_size': (self._on_size, 'wx.EVT_SIZE', 'wxEVT_SIZE'),
            'on_update_ui': (self._on_update_ui, 'wx.EVT_UPDATE_UI', 'wxEVT_UPDATE_UI'),
        }

    @property
    def events(self):
        return self.local_window_events

    @property
    def kwargs(self):
        """Return the full kwargs dictionary"""
        value = self.local_window_kwargs
        value.update(super(Window, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        """Set the hierarchy down kwargs.
        This is usually called under transaction"""
        prev_id = self._id
        self.local_window_kwargs = kwargs
        if prev_id != self._id:
            self.save_state()
            if self.project.language == 'python':
                if self._custom_identifier is not None:
                    self.top_window.unregister_python_identifier(prev_id)
                self._custom_identifier = self.top_window.register_python_identifier(self._id)
        super(Window, self).set_kwargs(kwargs)

    @property
    def is_sizer(self):
        return False

    @property
    def model_attributes(self):
        _property = [{
            'label': 'Window',
            'type': 'category',
            'help': 'Attributes from wxWindow',
            'child': [
                {
                    'label': 'id',
                    'type': 'string',
                    'value': self._id,
                    'name': 'id',
                    'help': 'window unique identifier'
                },
                {
                    'label': 'pos',
                    'type': 'int_vector',
                    'value': self._pos,
                    'name': 'pos',
                    'labels': [
                        ('x', 'Horizontal coordinate.'),
                        ('y', 'Vertical coordinate.')
                    ],
                    'help': 'window position'
                },
                {
                    'label': 'size',
                    'type': 'int_vector',
                    'value': self._size,
                    'name': 'size',
                    'labels': [
                        ('width', "screen width"),
                        ('height', "screen height")
                    ],
                    'help': 'window position. The default value is (-1, -1). '
                            ' This default is interpreted as that size must '
                            'be decided at runtime.'
                },
                {
                    'label': 'minimum size',
                    'type': 'int_vector',
                    'value': self._minimum_size,
                    'name': 'minimum_size',
                    'labels': [
                        ('width', "screen width"),
                        ('height', "screen height")
                    ],
                    'help': 'The minimum required size of the window, '
                            'that will be used by layout mechanism.'
                },
                {
                    'label': 'maximum size',
                    'type': 'int_vector',
                    'value': self._maximum_size,
                    'name': 'maximum_size',
                    'labels': [
                        ('width', "screen width"),
                        ('height', "screen height")
                    ],
                    'help': 'The maximum required size of the window, '
                            'that will be used by layout mechanism.'
                },
                {
                    'label': 'font',
                    'type': 'conditional font',
                    'value': self._font_info,
                    'name': 'font_info',
                    'help': 'The window font.'
                },
                {
                    'label': 'foreground colour',
                    'type': 'conditional colour',
                    'value': self._foreground_colour,
                    'name': 'foreground_colour',
                    'help': 'set foreground colour',
                },
                {
                    'label': 'background color',
                    'type': 'conditional colour',
                    'value' : self._background_colour,
                    'name': 'background_colour',
                    'help': 'The window background color.'
                },
                {
                    'label': 'window name',
                    'type': 'string',
                    'value' : self._window_name,
                    'name': 'window_name',
                    'help': 'The window class name.'
                },
                {
                    'label': 'window style',
                    'type': 'multi_choice',
                    'value' : self._window_style,
                    'name': 'window_style',
                    'help': 'The window style.',
                    'options': [
                        ('wxALWAYS_SHOW_SB',
                         'If a window has scrollbars, disable them instead of hiding '
                         'them when they are not needed (i.e. when the size of the '
                         'window is big enough to not require the scrollbars to navigate it). '
                         'This style is currently only implemented for wxMSW and wxUniversal '
                         'and does nothing on the other platforms'),
                        ('wxCLIP_CHILDREN',
                         'Use this style to eliminate flicker caused by the background being repainted,'
                         ' then children being painted over them. Windows only.'),
                        ('wxDOUBLE_BORDER',  'Displays a double border. Windows and Mac only.'),
                        ('wxFULL_REPAINT_ON_RESIZE',
                         'Use this style to force a complete redraw of the window whenever it is resized'
                         ' instead of redrawing just the part of the window affected by resizing. '
                         'Note that this was the behaviour by default before 2.5.1 release and that if '
                         'you experience redraw problems with code which previously used to work you may '
                         'want to try this. Currently this style applies on GTK+ 2 and Windows only, and '
                         'full repainting is always done on other platforms.'),
                        ('wxHSCROLL', 'Use this style to enable a horizontal scrollbar.'),
                        ('wxNO_BORDER', 'Displays no border, overriding the default border style for the window.'),
                        ('wxRAISED_BORDER', 'Displays a raised border.'),
                        ('wxSIMPLE_BORDER', 'Displays a thin border around the window. wxBORDER is the old name '
                                            'for this style.'),
                        ('wxSTATIC_BORDER', 'Displays a border suitable for a static control. Windows only.'),
                        ('wxSUNKEN_BORDER', 'Displays a sunken border.'),
                        ('wxTAB_TRAVERSAL', 'Use this to enable tab traversal for non-dialog windows.'),
                        ('wxTRANSPARENT_WINDOW', 'The window is transparent, that is, it will not receive paint '
                                                 'events. Windows only.'),
                        ('wxVSCROLL', 'Use this style to enable a vertical scrollbar.'),
                        ('wxWANTS_CHARS', 'Use this to indicate that the window wants to get all char/key events '
                                          'for all keys - even for keys like TAB or ENTER which are usually used for'
                                          ' dialog navigation and which wouldn\'t be generated without this style. '
                                          'If you need to use this style in order to get the arrows or etc., but '
                                          'would still like to have normal keyboard navigation take place, you should '
                                          'create and send a wxNavigationKeyEvent in response to the key events for '
                                          'Tab and Shift-Tab.'),
                    ]
                },
                {
                    'label': 'window extra style',
                    'type': 'multi_choice',
                    'value': self._window_extra_style,
                    'name': 'window_extra_style',
                    'help': 'The window extra style.',
                    'options': [
                        ('wxWS_EX_BLOCK_EVENTS', 'wxCommandEvents and the objects of the derived classes are forwarded '
                                                 'to the parent window and so on recursively by default. Using this '
                                                 'flag for the given window allows to block this propagation at this '
                                                 'window, i.e. prevent the events from being propagated further '
                                                 'upwards. Dialogs have this flag on by default.'),
                        ('wxWS_EX_PROCESS_IDLE', 'This window should always process idle events, even if the mode set '
                                                 'by wxIdleEvent::SetMode is wxIDLE_PROCESS_SPECIFIED.'),
                        ('wxWS_EX_PROCESS_UI_UPDATES', 'This window should always process UI update events, even if '
                                                       'the mode set by wxUpdateUIEvent::SetMode is '
                                                       'wxUPDATE_UI_PROCESS_SPECIFIED.'),
                        ('wxWS_EX_TRANSIENT', 'Don\'t use this window as an implicit parent for the other windows: '
                                              'this must be used with transient windows as otherwise there is the '
                                              'risk of creating a dialog/frame with this window as a parent which '
                                              'would lead to a crash if the parent is destroyed before the child.'),
                        ('wxWS_EX_VALIDATE_RECURSIVELY', 'By default, Validate/TransferDataTo/FromWindow() only work'
                                                         ' on direct children of the window (compatible behaviour). '
                                                         'Set this flag to make them recursively descend into all '
                                                         'subwindows.')
                    ]
                },
                {
                    'label': 'tooltip',
                    'type': 'string',
                    'value': self._tooltip,
                    'name': 'tooltip',
                    'help': 'Attach a tooltip to the window.',
                },
                {
                    'label': 'context menu',
                    'type': 'boolean',
                    'value': self._context_menu,
                    'name': 'context_menu',
                    'help': 'Generates a event handler for displaying a menu assigned to this window as context menu.',
                },
                {
                    'label': 'context help',
                    'type': 'string',
                    'value': self._context_help,
                    'name': 'context_help',
                    'help': 'Attach context-sensitive help to the window.'
                },
                {
                    'label': 'enabled',
                    'type': 'boolean',
                    'value': self._enabled,
                    'name': 'enabled',
                    'help': 'Enable or disable the window for user input. Note that when a parent window is disabled,'
                            ' all of its children are disabled as well and they are reenabled again when the parent is.'
                },
                {
                    'label': 'hidden',
                    'type': 'boolean',
                    'value': self._hidden,
                    'name': 'hidden',
                    'help': 'Shows or hides the window'
                },
                {
                    'label': 'subclass',
                    'type': 'string',
                    'value': self._subclass,
                    'name': 'subclass',
                    'help': 'You can use that to change the final class by another thar behaves in a similar way.'
                            'That helps development avoiding the necessity for integrate those similar classes in '
                            'the wxUI environment. You must required headers in the project configuration.'
                },

            ]
        }]
        _event = [{
            'label': 'wxWindow',
            'type': 'category',
            'help': 'Events from wxWindow',
            'child': [
                {
                    'label': 'keyboard events',
                    'type': 'category',
                    'help': 'Events related with keyboard',
                    'child':[
                        {
                            'label': 'on char',
                            'type': 'string',
                            'value': self._on_char,
                            'name': 'on_char',
                            'help': 'handles wxEVT_CHAR event.'
                        },
                        {
                            'label': 'on key_down',
                            'type': 'string',
                            'value': self._on_key_down,
                            'name': 'on_key_down',
                            'help': 'handles wxEVT_KEY_DOWN event.'
                        },
                        {
                            'label': 'on key up',
                            'type': 'string',
                            'value': self._on_key_up,
                            'name': 'on_key_up',
                            'help': 'handles wxEVT_KEY_UP event.'
                        },
                    ]
                },
                {
                    'label': 'mouse events',
                    'type': 'category',
                    'help': 'Events related with mouse',
                    'child': [
                        {
                            'label': 'on enter window',
                            'type': 'string',
                            'value': self._on_enter_window,
                            'name': 'on_enter_window',
                            'help': 'handles wxEVT_ENTER_WINDOW event.'
                        },
                        {
                            'label': 'on leave window',
                            'type': 'string',
                            'value': self._on_leave_window,
                            'name': 'on_leave_window',
                            'help': 'handles wxEVT_LEAVE_WINDOW event.'
                        },
                        {
                            'label': 'on left button double click',
                            'type': 'string',
                            'value': self._on_left_button_double_click,
                            'name': 'on_left_button_double_click',
                            'help': 'handles wxEVT_LEFT_DCLICK event.'
                        },
                        {
                            'label': 'on left button down',
                            'type': 'string',
                            'value': self._on_left_button_down,
                            'name': 'on_left_button_down',
                            'help': 'handles wxEVT_LEFT_DOWN event.'
                        },
                        {
                            'label': 'on left button up',
                            'type': 'string',
                            'value': self._on_left_button_up,
                            'name': 'on_left_button_up',
                            'help': 'handles wxEVT_LEFT_UP event.'
                        },
                        {
                            'label': 'on middle button double click',
                            'type': 'string',
                            'value': self._on_middle_button_double_click,
                            'name': 'on_middle_button_double_click',
                            'help': 'handles wxEVT_MIDDLE_DCLICK event.'
                        },
                        {
                            'label': 'on middle button down',
                            'type': 'string',
                            'value': self._on_middle_button_down,
                            'name': 'on_middle_button_down',
                            'help': 'handles wxEVT_MIDDLE_DOWN event.'
                        },
                        {
                            'label': 'on middle button up',
                            'type': 'string',
                            'value': self._on_middle_button_up,
                            'name': 'on_middle_button_up',
                            'help': 'handles wxEVT_MIDDLE_UP event.'
                        },
                        {
                            'label': 'on right button double click',
                            'type': 'string',
                            'value': self._on_right_button_double_click,
                            'name': 'on_right_button_double_click',
                            'help': 'handles wxEVT_RIGHT_DCLICK event.'
                        },
                        {
                            'label': 'on right button down',
                            'type': 'string',
                            'value': self._on_right_button_down,
                            'name': 'on_right_button_down',
                            'help': 'handles wxEVT_RIGHT_DOWN event.'
                        },
                        {
                            'label': 'on right button up',
                            'type': 'string',
                            'value': self._on_right_button_up,
                            'name': 'on_right_button_up',
                            'help': 'handles wxEVT_RIGHT_UP event.'
                        },
                        {
                            'label': 'on mouse wheel',
                            'type': 'string',
                            'value': self._on_mouse_wheel,
                            'name': 'on_mouse_wheel',
                            'help': 'handles wxEVT_MOUSEWHEEL event.'
                        },
                        {
                            'label': 'on mouse move',
                            'type': 'string',
                            'value': self._on_mouse_move,
                            'name': 'on_mouse_move',
                            'help': 'handles wxEVT_MOTION event.'
                        },
                        {
                            'label': 'on mouse events',
                            'type': 'string',
                            'value': self._on_mouse_events,
                            'name': 'on_mouse_events',
                            'help': 'handles all mouse events.'
                        },
                    ]
                },
                {
                    'label': 'window events',
                    'type': 'category',
                    'help': 'Events related with windows status',
                    'child': [
                        {
                            'label': 'on set focus',
                            'type': 'string',
                            'value': self._on_set_focus,
                            'name': 'on_set_focus',
                            'help': 'handles wxEVT_SET_FOCUS event.'
                        },
                        {
                            'label': 'on kill focus',
                            'type': 'string',
                            'value': self._on_kill_focus,
                            'name': 'on_kill_focus',
                            'help': 'handles wxEVT_KILL_FOCUS event.'
                        },
                        {
                            'label': 'on erase background',
                            'type': 'string',
                            'value': self._on_erase_background,
                            'name': 'on_erase_background',
                            'help': 'handles wxEVT_ERASE_BACKGROUND event.'
                        },
                        {
                            'label': 'on paint',
                            'type': 'string',
                            'value': self._on_paint,
                            'name': 'on_paint',
                            'help': 'handles wxEVT_PAINT event.'
                        },
                        {
                            'label': 'on size',
                            'type': 'string',
                            'value': self._on_size,
                            'name': 'on_size',
                            'help': 'handles wxEVT_SIZE event.'
                        },
                        {
                            'label': 'on update ui',
                            'type': 'string',
                            'value': self._on_update_ui,
                            'name': 'on_update_ui',
                            'help': 'handles wxEVT_UPDATE_UI event.'
                        },
                    ]
                }
            ]
        }]
        if self._subitem_adaptor is not None:
            _sizer_item_properties, _empty = self._subitem_adaptor.model_attributes
            _property += _sizer_item_properties
        return _property, _event

    def create_edition_window(self, parent):
        raise RuntimeError('Window class create_edition_window must be override.')

    def destroy_edition_window(self):
        raise RuntimeError('Window class destroy_edition_window must be override.')

    def add_to_sizer(self, container, window):
        """This method is invoked from specializations while
        creating implementation window for display edit"""
        if self._subitem_adaptor is not None:
            self._subitem_adaptor.Realize()
        return container

    def cc_add_to_sizer_code(self, container, window):
        """This method return the code required for adding the control
        to the sizer container (if any) or empty string"""
        if self._subitem_adaptor is None:
            return ''
        return self._subitem_adaptor.cc_realize_code(container, window)

    def py_add_to_sizer_code(self, container, window):
        """This method return the code required for adding the control
        to the sizer container (if any) or empty string"""
        if self._subitem_adaptor is None:
            return ''
        return self._subitem_adaptor.py_realize_code(container, window)

    def delete(self):
        """Do the deletion of the element.
        Must also delete the implementation class.
        Notes on transaction:
        This operation is first done on basis, because
        as it iterates on child deletion, that ensures
        the child windows (if any) will be destroyed
        before parent"""
        super(Window, self).delete()

    @property
    def style_value(self):
        """Return the value required for dynamic control creation"""
        return self.style_value_helper(Window.window_style_list, self._window_style)


    @property
    def extra_style_value(self):
        """Return the value required for dynamic control creation"""
        return self.style_value_helper(Window.window_extra_style_list, self._window_extra_style)

    @property
    def cc_style_str(self):
        """resturn a suitable string for the generated code in c++"""
        if self._window_style == 0:
            return '0'
        sz = len(Window.cc_window_style_list_text)
        return '|'.join(
            Window.cc_window_style_list_text[index] for index in range(sz)
            if self._window_style & (2 ** index)
        )

    @property
    def py_style_str(self):
        if self._window_style == 0:
            return '0'
        sz = len(Window.py_window_style_list_text)
        return '| '.join(
            Window.py_window_style_list_text[index] for index in range(sz)
            if self._window_style & (2 ** index)
        )

    @property
    def cc_extra_style_value_text(self):
        if self._window_extra_style == 0:
            return '0'
        sz = len(Window.cc_window_extra_style_list_text)
        return '| '.join(Window.cc_window_extra_style_list_text[i] for i in range(sz)
                         if self._window_extra_style & (2 ** i))

    @property
    def py_extra_style_value_text(self):
        if self._window_extra_style == 0:
            return '0'
        sz = len(Window.py_window_extra_style_list_text)
        return '| '.join(Window.py_window_extra_style_list_text[i] for i in range(sz)
                         if self._window_extra_style & (2 ** i))

    def destroy_edition_window(self):
        if self._edition_window is not None:
            reverse_sorted = reversed(self.sorted_wxui_child)
            for child in reverse_sorted:
                child.destroy_edition_window()
            self._edition_window.Destroy()
            self._edition_window = None

    @property
    def cc_pos_str(self):
        """return a text suitable for the construction of c++ control"""
        if self._pos == (-1, -1):
            return 'wxDefaultPosition'
        else:
            return 'wxPoint({p[0]}, {p[1]})'.format(p=self._pos)

    @property
    def py_pos_str(self):
        """return a text suitable for the construction of python control"""
        if self._pos == (-1, -1):
            return 'wx.DefaultPosition'
        else:
            return 'wx.Point({p[0]}, {p[1]})'.format(p=self._pos)

    @property
    def py_pos(self):
        """return a python value suitable for edition window creation"""
        if self._pos == (-1, -1):
            return wx.DefaultPosition
        else:
            return wx.Point(self._pos)

    @property
    def cc_size_str(self):
        """Return a text suitable for the construction of c++ control"""
        if self._size == (-1, -1):
            return 'wxDefaultSize'
        else:
            return'wxSize({s[0]}, {s[1]})'.format(s=self._size)

    @property
    def py_size_str(self):
        """Return a text suitable for the construction of python control"""
        if self._size == (-1, -1):
            return 'wx.DefaultSize'
        else:
            return'wx.Size({s[0]}, {s[1]})'.format(s=self._size)

    @property
    def py_size(self):
        """return a python value suitable for edition window creation"""
        if self._size == (-1, -1):
            return wx.DefaultSize
        else:
            return wx.Size(self._size)

    def standard_py_id(self, _id):
        know_map = {
            'wxID_ANY': wx.ID_ANY, 'wxID_ABORT': wx.ID_ABORT, 'wxID_ABOUT': wx.ID_ABOUT, 'wxID_ADD': wx.ID_ADD,
            'wxID_APPLY': wx.ID_APPLY, 'wxID_AUTO_HIGHEST': wx.ID_AUTO_HIGHEST,
            'wxID_AUTO_LOWEST': wx.ID_AUTO_LOWEST, 'wxID_BACKWARD': wx.ID_BACKWARD, 'wxID_BOLD': wx.ID_BOLD,
            'wxID_BOTTOM': wx.ID_BOTTOM, 'wxID_CANCEL': wx.ID_CANCEL, 'wxID_CDROM': wx.ID_CDROM,
            'wxID_CLEAR': wx.ID_CLEAR, 'wxID_CLOSE': wx.ID_CLOSE, 'wxID_CLOSE_ALL': wx.ID_CLOSE_ALL,
            'wxID_CLOSE_FRAME': wx.ID_CLOSE_FRAME, 'wxID_CONTEXT_HELP': wx.ID_CONTEXT_HELP,
            'wxID_CONVERT': wx.ID_CONVERT, 'wxID_COPY': wx.ID_COPY, 'wxID_CUT': wx.ID_CUT,
            'wxID_DEFAULT': wx.ID_DEFAULT, 'wxID_DELETE': wx.ID_DELETE, 'wxID_DOWN': wx.ID_DOWN,
            'wxID_DUPLICATE': wx.ID_DUPLICATE, 'wxID_EDIT': wx.ID_EDIT, 'wxID_EXECUTE': wx.ID_EXECUTE,
            'wxID_EXIT': wx.ID_EXIT, 'wxID_FILE': wx.ID_FILE, 'wxID_FILE1': wx.ID_FILE1,
            'wxID_FILE2': wx.ID_FILE2, 'wxID_FILE3': wx.ID_FILE3, 'wxID_FILE4': wx.ID_FILE4,
            'wxID_FILE5': wx.ID_FILE5, 'wxID_FILE6': wx.ID_FILE6, 'wxID_FILE7': wx.ID_FILE7,
            'wxID_FILE8': wx.ID_FILE8, 'wxID_FILE9': wx.ID_FILE9, 'wx.ID_FILECTRL': wx.ID_FILECTRL,
            'wxID_FILEDLGG': wx.ID_FILEDLGG, 'wxID_FIND': wx.ID_FIND, 'wxID_FIRST': wx.ID_FIRST,
            'wxID_FLOPPY': wx.ID_FLOPPY, 'wxID_FORWARD': wx.ID_FORWARD, 'wxID_HELP': wx.ID_HELP,
            'wxID_HARDDISK': wx.ID_HARDDISK, 'wxID_HELP_COMMANDS': wx.ID_HELP_COMMANDS,
            'wxID_HELP_CONTENTS': wx.ID_HELP_CONTENTS, 'wxID_HELP_CONTEXT': wx.ID_HELP_CONTEXT,
            'wxID_HELP_INDEX': wx.ID_HELP_INDEX, 'wxID_HELP_PROCEDURES': wx.ID_HELP_PROCEDURES,
            'wxID_HELP_SEARCH': wx.ID_HELP_SEARCH, 'wxID_HIGHEST': wx.ID_HIGHEST, 'wxID_HOME': wx.ID_HOME,
            'wxID_ICONIZE_FRAME': wx.ID_ICONIZE_FRAME, 'wxID_IGNORE': wx.ID_IGNORE, 'wxID_INDENT': wx.ID_INDENT,
            'wxID_INDEX': wx.ID_INDEX, 'wxID_INFO': wx.ID_INFO, 'wxID_ITALIC': wx.ID_ITALIC,
            'wxID_JUMP_TO': wx.ID_JUMP_TO, 'wxID_JUSTIFY_CENTER': wx.ID_JUSTIFY_CENTER,
            'wxID_JUSTIFY_FILL': wx.ID_JUSTIFY_FILL, 'wxID_JUSTIFY_LEFT': wx.ID_JUSTIFY_LEFT,
            'wxID_JUSTIFY_RIGHT': wx.ID_JUSTIFY_RIGHT, 'wxID_LAST': wx.ID_LAST, 'wxID_LOWEST': wx.ID_LOWEST,
            'wxID_MAXIMIZE_FRAME': wx.ID_MAXIMIZE_FRAME,
            'wxID_MDI_WINDOW_ARRANGE_ICONS': wx.ID_MDI_WINDOW_ARRANGE_ICONS,
            'wxID_MDI_WINDOW_CASCADE': wx.ID_MDI_WINDOW_CASCADE, 'wxID_MDI_WINDOW_FIRST': wx.ID_MDI_WINDOW_FIRST,
            'wxID_MDI_WINDOW_LAST': wx.ID_MDI_WINDOW_LAST, 'wxID_MDI_WINDOW_NEXT': wx.ID_MDI_WINDOW_NEXT,
            'wxID_MDI_WINDOW_PREV': wx.ID_MDI_WINDOW_PREV, 'wxID_MDI_WINDOW_TILE_HORZ': wx.ID_MDI_WINDOW_TILE_HORZ,
            'wxID_MDI_WINDOW_TILE_VERT': wx.ID_MDI_WINDOW_TILE_VERT, 'wxID_MORE': wx.ID_MORE,
            'wxID_MOVE_FRAME': wx.ID_MOVE_FRAME, 'wxID_NO': wx.ID_NO, 'wxID_NETWORK': wx.ID_NETWORK,
            'wxID_NEW': wx.ID_NEW, 'wxID_NONE': wx.ID_NONE, 'wxID_NOTOALL': wx.ID_NOTOALL, 'wxID_OK': wx.ID_OK,
            'wxID_OPEN': wx.ID_OPEN, 'wxID_PAGE_SETUP': wx.ID_PAGE_SETUP, 'wxID_PASTE': wx.ID_PASTE,
            'wxID_PREFERENCES': wx.ID_PREFERENCES, 'wxID_PREVIEW': wx.ID_PREVIEW,
            'wxID_PREVIEW_CLOSE': wx.ID_PREVIEW_CLOSE, 'wxID_PREVIEW_FIRST': wx.ID_PREVIEW_FIRST,
            'wxID_PREVIEW_GOTO': wx.ID_PREVIEW_GOTO, 'wxID_PREVIEW_LAST': wx.ID_PREVIEW_LAST,
            'wxID_PREVIEW_NEXT': wx.ID_PREVIEW_NEXT, 'wxID_PREVIEW_PREVIOUS': wx.ID_PREVIEW_PREVIOUS,
            'wxID_PREVIEW_PRINT': wx.ID_PREVIEW_PRINT, 'wxID_PREVIEW_ZOOM': wx.ID_PREVIEW_ZOOM,
            'wxID_PREVIEW_ZOOM_IN': wx.ID_PREVIEW_ZOOM_IN, 'wxID_PREVIEW_ZOOM_OUT': wx.ID_PREVIEW_ZOOM_OUT,
            'wxID_PRINT': wx.ID_PRINT, 'wxID_PRINT_SETUP': wx.ID_PRINT_SETUP, 'wxID_PROPERTIES': wx.ID_PROPERTIES,
            'wxID_REDO': wx.ID_REDO, 'wxID_REFRESH': wx.ID_REFRESH, 'wxID_REMOVE': wx.ID_REMOVE,
            'wxID_REPLACE': wx.ID_REPLACE, 'wxID_REPLACE_ALL': wx.ID_REPLACE_ALL, 'wxID_RESET': wx.ID_RESET,
            'wxID_RESIZE_FRAME': wx.ID_RESIZE_FRAME, 'wxID_RESTORE_FRAME': wx.ID_RESTORE_FRAME,
            'wxID_RETRY': wx.ID_RETRY, 'wxID_REVERT': wx.ID_REVERT, 'wxID_REVERT_TO_SAVED': wx.ID_REVERT_TO_SAVED,
            'wxID_SAVE': wx.ID_SAVE, 'wxID_SEPARATOR': wx.ID_SEPARATOR, 'wxID_SAVEAS': wx.ID_SAVEAS,
            'wxID_SELECT_COLOR': wx.ID_SELECT_COLOR, 'wxID_SELECT_FONT': wx.ID_SELECT_FONT,
            'wxID_SELECTALL': wx.ID_SELECTALL, 'wxID_SETUP': wx.ID_SETUP,
            'wxID_SORT_ASCENDING': wx.ID_SORT_ASCENDING, 'wxID_SORT_DESCENDING': wx.ID_SORT_DESCENDING,
            'wxID_SPELL_CHECK': wx.ID_SPELL_CHECK, 'wxID_STATIC': wx.ID_STATIC, 'wxID_STOP': wx.ID_STOP,
            'wxID_STRIKETHROUGH': wx.ID_STRIKETHROUGH, 'wxID_SYSTEM_MENU': wx.ID_SYSTEM_MENU,
            'wxID_TOP': wx.ID_TOP, 'wxID_UNDELETE': wx.ID_UNDELETE, 'wxID_UNDERLINE': wx.ID_UNDERLINE,
            'wxID_UNDO': wx.ID_UNDO, 'wxID_UNINDENT': wx.ID_UNINDENT, 'wxID_UP': wx.ID_UP,
            'wxID_VIEW_DETAILS': wx.ID_VIEW_DETAILS, 'wxID_VIEW_LARGEICONS': wx.ID_VIEW_LARGEICONS,
            'wxID_VIEW_LIST': wx.ID_VIEW_LIST, 'wxID_VIEW_SMALLICONS': wx.ID_VIEW_SMALLICONS,
            'wxID_VIEW_SORTDATE': wx.ID_VIEW_SORTDATE, 'wxID_VIEW_SORTNAME': wx.ID_VIEW_SORTNAME,
            'wxID_VIEW_SORTSIZE': wx.ID_VIEW_SORTSIZE, 'wxID_VIEW_SORTTYPE': wx.ID_VIEW_SORTTYPE,
            'wxID_YES': wx.ID_YES, 'wx.ID_YESTOALL': wx.ID_YESTOALL, 'wxID_ZOOM_100': wx.ID_ZOOM_100,
            'wxID_ZOOM_FIT': wx.ID_ZOOM_FIT, 'wxID_ZOOM_IN': wx.ID_ZOOM_IN, 'wxID_ZOOM_OUT': wx.ID_ZOOM_OUT,
        }
        if _id in know_map:
            return know_map[_id]
        return None

    @property
    def py_id(self):
        """return a window identifier suitable for edition window creation"""
        return self.standard_py_id(self._id) or identifier(self._id)

    @property
    def py_id_str(self):
        """return a window identifier suitable for edition window creation"""
        know_map = {
            'wxID_ANY': 'wx.ID_ANY', 'wxID_ABORT': 'wx.ID_ABORT', 'wxID_ABOUT': 'wx.ID_ABOUT', 'wxID_ADD': 'wx.ID_ADD',
            'wxID_APPLY': 'wx.ID_APPLY', 'wxID_AUTO_HIGHEST': 'wx.ID_AUTO_HIGHEST',
            'wxID_AUTO_LOWEST': 'wx.ID_AUTO_LOWEST', 'wxID_BACKWARD': 'wx.ID_BACKWARD', 'wxID_BOLD': 'wx.ID_BOLD',
            'wxID_BOTTOM': 'wx.ID_BOTTOM', 'wxID_CANCEL': 'wx.ID_CANCEL', 'wxID_CDROM': 'wx.ID_CDROM',
            'wxID_CLEAR': 'wx.ID_CLEAR', 'wxID_CLOSE': 'wx.ID_CLOSE', 'wxID_CLOSE_ALL': 'wx.ID_CLOSE_ALL',
            'wxID_CLOSE_FRAME': 'wx.ID_CLOSE_FRAME', 'wxID_CONTEXT_HELP': 'wx.ID_CONTEXT_HELP',
            'wxID_CONVERT': 'wx.ID_CONVERT', 'wxID_COPY': 'wx.ID_COPY', 'wxID_CUT': 'wx.ID_CUT',
            'wxID_DEFAULT': 'wx.ID_DEFAULT', 'wxID_DELETE': 'wx.ID_DELETE', 'wxID_DOWN': 'wx.ID_DOWN',
            'wxID_DUPLICATE': 'wx.ID_DUPLICATE', 'wxID_EDIT': 'wx.ID_EDIT', 'wxID_EXECUTE': 'wx.ID_EXECUTE',
            'wxID_EXIT': 'wx.ID_EXIT', 'wxID_FILE': 'wx.ID_FILE', 'wxID_FILE1': 'wx.ID_FILE1',
            'wxID_FILE2': 'wx.ID_FILE2', 'wxID_FILE3': 'wx.ID_FILE3', 'wxID_FILE4': 'wx.ID_FILE4',
            'wxID_FILE5': 'wx.ID_FILE5', 'wxID_FILE6': 'wx.ID_FILE6', 'wxID_FILE7': 'wx.ID_FILE7',
            'wxID_FILE8': 'wx.ID_FILE8', 'wxID_FILE9': 'wx.ID_FILE9', 'wx.ID_FILECTRL': 'wx.ID_FILECTRL',
            'wxID_FILEDLGG': 'wx.ID_FILEDLGG', 'wxID_FIND': 'wx.ID_FIND', 'wxID_FIRST': 'wx.ID_FIRST',
            'wxID_FLOPPY': 'wx.ID_FLOPPY', 'wxID_FORWARD': 'wx.ID_FORWARD', 'wxID_HELP': 'wx.ID_HELP',
            'wxID_HARDDISK': 'wx.ID_HARDDISK', 'wxID_HELP_COMMANDS': 'wx.ID_HELP_COMMANDS',
            'wxID_HELP_CONTENTS': 'wx.ID_HELP_CONTENTS', 'wxID_HELP_CONTEXT': 'wx.ID_HELP_CONTEXT',
            'wxID_HELP_INDEX': 'wx.ID_HELP_INDEX', 'wxID_HELP_PROCEDURES': 'wx.ID_HELP_PROCEDURES',
            'wxID_HELP_SEARCH': 'wx.ID_HELP_SEARCH', 'wxID_HIGHEST': 'wx.ID_HIGHEST', 'wxID_HOME': 'wx.ID_HOME',
            'wxID_ICONIZE_FRAME': 'wx.ID_ICONIZE_FRAME', 'wxID_IGNORE': 'wx.ID_IGNORE', 'wxID_INDENT': 'wx.ID_INDENT',
            'wxID_INDEX': 'wx.ID_INDEX', 'wxID_INFO': 'wx.ID_INFO', 'wxID_ITALIC': 'wx.ID_ITALIC',
            'wxID_JUMP_TO': 'wx.ID_JUMP_TO', 'wxID_JUSTIFY_CENTER': 'wx.ID_JUSTIFY_CENTER',
            'wxID_JUSTIFY_FILL': 'wx.ID_JUSTIFY_FILL', 'wxID_JUSTIFY_LEFT': 'wx.ID_JUSTIFY_LEFT',
            'wxID_JUSTIFY_RIGHT': 'wx.ID_JUSTIFY_RIGHT', 'wxID_LAST': 'wx.ID_LAST', 'wxID_LOWEST': 'wx.ID_LOWEST',
            'wxID_MAXIMIZE_FRAME': 'wx.ID_MAXIMIZE_FRAME',
            'wxID_MDI_WINDOW_ARRANGE_ICONS': 'wx.ID_MDI_WINDOW_ARRANGE_ICONS',
            'wxID_MDI_WINDOW_CASCADE': 'wx.ID_MDI_WINDOW_CASCADE', 'wxID_MDI_WINDOW_FIRST': 'wx.ID_MDI_WINDOW_FIRST',
            'wxID_MDI_WINDOW_LAST': 'wx.ID_MDI_WINDOW_LAST', 'wxID_MDI_WINDOW_NEXT': 'wx.ID_MDI_WINDOW_NEXT',
            'wxID_MDI_WINDOW_PREV': 'wx.ID_MDI_WINDOW_PREV', 'wxID_MDI_WINDOW_TILE_HORZ': 'wx.ID_MDI_WINDOW_TILE_HORZ',
            'wxID_MDI_WINDOW_TILE_VERT': 'wx.ID_MDI_WINDOW_TILE_VERT', 'wxID_MORE': 'wx.ID_MORE',
            'wxID_MOVE_FRAME': 'wx.ID_MOVE_FRAME', 'wxID_NO': 'wx.ID_NO', 'wxID_NETWORK': 'wx.ID_NETWORK',
            'wxID_NEW': 'wx.ID_NEW', 'wxID_NONE': 'wx.ID_NONE', 'wxID_NOTOALL': 'wx.ID_NOTOALL', 'wxID_OK': 'wx.ID_OK',
            'wxID_OPEN': 'wx.ID_OPEN', 'wxID_PAGE_SETUP': 'wx.ID_PAGE_SETUP', 'wxID_PASTE': 'wx.ID_PASTE',
            'wxID_PREFERENCES': 'wx.ID_PREFERENCES', 'wxID_PREVIEW': 'wx.ID_PREVIEW',
            'wxID_PREVIEW_CLOSE': 'wx.ID_PREVIEW_CLOSE', 'wxID_PREVIEW_FIRST': 'wx.ID_PREVIEW_FIRST',
            'wxID_PREVIEW_GOTO': 'wx.ID_PREVIEW_GOTO', 'wxID_PREVIEW_LAST': 'wx.ID_PREVIEW_LAST',
            'wxID_PREVIEW_NEXT': 'wx.ID_PREVIEW_NEXT', 'wxID_PREVIEW_PREVIOUS': 'wx.ID_PREVIEW_PREVIOUS',
            'wxID_PREVIEW_PRINT': 'wx.ID_PREVIEW_PRINT', 'wxID_PREVIEW_ZOOM': 'wx.ID_PREVIEW_ZOOM',
            'wxID_PREVIEW_ZOOM_IN': 'wx.ID_PREVIEW_ZOOM_IN', 'wxID_PREVIEW_ZOOM_OUT': 'wx.ID_PREVIEW_ZOOM_OUT',
            'wxID_PRINT': 'wx.ID_PRINT', 'wxID_PRINT_SETUP': 'wx.ID_PRINT_SETUP', 'wxID_PROPERTIES': 'wx.ID_PROPERTIES',
            'wxID_REDO': 'wx.ID_REDO', 'wxID_REFRESH': 'wx.ID_REFRESH', 'wxID_REMOVE': 'wx.ID_REMOVE',
            'wxID_REPLACE': 'wx.ID_REPLACE', 'wxID_REPLACE_ALL': 'wx.ID_REPLACE_ALL', 'wxID_RESET': 'wx.ID_RESET',
            'wxID_RESIZE_FRAME': 'wx.ID_RESIZE_FRAME', 'wxID_RESTORE_FRAME': 'wx.ID_RESTORE_FRAME',
            'wxID_RETRY': 'wx.ID_RETRY', 'wxID_REVERT': 'wx.ID_REVERT', 'wxID_REVERT_TO_SAVED': 'wx.ID_REVERT_TO_SAVED',
            'wxID_SAVE': 'wx.ID_SAVE', 'wxID_SEPARATOR': 'wx.ID_SEPARATOR', 'wxID_SAVEAS': 'wx.ID_SAVEAS',
            'wxID_SELECT_COLOR': 'wx.ID_SELECT_COLOR', 'wxID_SELECT_FONT': 'wx.ID_SELECT_FONT',
            'wxID_SELECTALL': 'wx.ID_SELECTALL', 'wxID_SETUP': 'wx.ID_SETUP',
            'wxID_SORT_ASCENDING': 'wx.ID_SORT_ASCENDING', 'wxID_SORT_DESCENDING': 'wx.ID_SORT_DESCENDING',
            'wxID_SPELL_CHECK': 'wx.ID_SPELL_CHECK', 'wxID_STATIC': 'wx.ID_STATIC', 'wxID_STOP': 'wx.ID_STOP',
            'wxID_STRIKETHROUGH': 'wx.ID_STRIKETHROUGH', 'wxID_SYSTEM_MENU': 'wx.ID_SYSTEM_MENU',
            'wxID_TOP': 'wx.ID_TOP', 'wxID_UNDELETE': 'wx.ID_UNDELETE', 'wxID_UNDERLINE': 'wx.ID_UNDERLINE',
            'wxID_UNDO': 'wx.ID_UNDO', 'wxID_UNINDENT': 'wx.ID_UNINDENT', 'wxID_UP': 'wx.ID_UP',
            'wxID_VIEW_DETAILS': 'wx.ID_VIEW_DETAILS', 'wxID_VIEW_LARGEICONS': 'wx.ID_VIEW_LARGEICONS',
            'wxID_VIEW_LIST': 'wx.ID_VIEW_LIST', 'wxID_VIEW_SMALLICONS': 'wx.ID_VIEW_SMALLICONS',
            'wxID_VIEW_SORTDATE': 'wx.ID_VIEW_SORTDATE', 'wxID_VIEW_SORTNAME': 'wx.ID_VIEW_SORTNAME',
            'wxID_VIEW_SORTSIZE': 'wx.ID_VIEW_SORTSIZE', 'wxID_VIEW_SORTTYPE': 'wx.ID_VIEW_SORTTYPE',
            'wxID_YES': 'wx.ID_YES', 'wx.ID_YESTOALL': 'wx.ID_YESTOALL', 'wxID_ZOOM_100': 'wx.ID_ZOOM_100',
            'wxID_ZOOM_FIT': 'wx.ID_ZOOM_FIT', 'wxID_ZOOM_IN': 'wx.ID_ZOOM_IN', 'wxID_ZOOM_OUT': 'wx.ID_ZOOM_OUT',
        }
        if self._id in know_map:
            return know_map[self._id]
        return self._id

    # Helpers
    def style_value_helper(self, style_list, internal_style):
        """Create the usable style for building the control in runtime.
        This method is used for create_edition_window."""
        control_style_value = 0
        mask = 1
        for index, style in enumerate(style_list):
            if internal_style & (2**index):
                control_style_value |= style
        return control_style_value

    def get_colour_helper(self, colour):
        """Transforms the colour parameter into a suitable
        colour for edition_window"""
        if type(colour) is wx.SystemColour:
            return wx.SystemSettings.GetColour(colour)
        return colour

    def declare_python_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # add a controls folder in order to avoid garbage
        self._implementation = py.MemberData(
            parent=container,
            scope='instance',
            value="None",
            name=self._name,
        )

    def implement_python_identifier(self):
        """This method is responsible for declaring the identifier
           used by wx.Python for custom identifiers"""
        self._custom_identifier = self.top_window.register_python_identifier(self._id)

    def update_py_event(self, container, event, name):
        if event in self._wxui_event_handlers:
            handler = self._wxui_event_handlers[event]
            if len(name):
                if handler.name != name:
                    handler.save_state()
                    handler.name = name
                    return True
            else:
                handler.delete()
                del self._wxui_event_handlers[event]
                return True
        elif name:
            # create a event handler
            handler = pyEventHandler(
                parent=container,
                parent_container=self,
                name=name,
                read_only=True,
                read_only_content=False,
                content='# write here the event handler',
                note="Event handler",
            )
            py.Argument(
                parent=handler,
                name='event',
                read_only=True
            )
            self._wxui_event_handlers[event] = handler
            return True
        return False

    def update_cc_event(self, container, event, name, update=False):
        if event in self._wxui_event_handlers:
            handler = self._wxui_event_handlers[event]
            if len(name):
                if handler.name != name:
                    handler.save_state()
                    handler.name = name
                    return True
            else:
                del self._wxui_event_handlers[event]
                handler.delete()
                return True
        elif name:
            # create a event handler
            main_window = self.top_window
            handler = ccEventHandler(
                parent=container,
                parent_container=self,
                parent_attribute='_{}'.format(event),
                name=name,
                type=cc.typeinst(type_alias='void'),
                read_only=False,
                access='private',
                note="Event handler"
            )
            setattr(handler, 'is_update', update)
            if update:
                cc.Argument(
                    parent=handler,
                    name='event',
                    type=cc.typeinst(type_alias='wxUpdateUIEvent', ref=True),
                    read_only=True
                )
            else:
                cc.Argument(
                    parent=handler,
                    name='event',
                    type=cc.typeinst(type_alias='wxEvent', ref=True),
                    read_only=True
                )
            self._wxui_event_handlers[event] = handler
            return True
        return False

    def update_python_event_handlers(self):
        """Do a  python event handler update."""
        container = self.top_window.event_handlers_folder
        changes = False
        event_dict = self.events
        for key in event_dict:
            changes = self.update_py_event(container, key, event_dict[key][0]) or changes
        for item in self.sorted_wxui_child:
            changes = item.update_python_event_handlers() or changes
        return changes

    def update_cpp_event_handlers(self):
        """Do a  python event handler update."""
        container = self.top_window.event_handlers_folder
        changes = False
        event_dict = self.events
        for key in event_dict:
            update = 'update' in key  # heuristic select kind of event
            changes = self.update_cc_event(container, key, event_dict[key][0], update) or changes
        for item in self.sorted_wxui_child:
            changes = item.update_cpp_event_handlers() or changes
        return changes

    def py_bind_text(self, event):
        """return the suitable code for binding an event"""
        if self.is_top_window:
            return 'self.Bind({event[1]},self.{event[0]})\n'.format(event=event)
        else:
            return 'self.{name}.Bind({event[1]},self.{event[0]})\n'.format(
                name=self._name, event=event
            )

    def py_unbind_text(self, event):
        """return the suitable code for binding an event"""
        if self.is_top_window:
            return 'self.Unbind({event[1]},self.{event[0]})\n'.format(event=event)
        else:
            return 'self.{name}.Unbind({event[1]},self.{event[0]})\n'.format(
                name=self._name, event=event
            )

    def py_bind_text_set(self):
        text = ''
        event_dict = self.events
        for item in event_dict:
            if item in self._wxui_event_handlers:
                text += self.py_bind_text(event_dict[item])
        for item in self.sorted_wxui_child:
            text += item.py_bind_text_set()
        return text

    def py_unbind_text_set(self):
        text = ''
        event_dict = self.events
        for item in event_dict:
            if item in self._wxui_event_handlers:
                text += self.py_unbind_text(event_dict[item])
        for item in self.sorted_wxui_child:
            text += item.py_unbind_text_set()
        return text

    def cc_bind_text(self, event, is_update=False):
        """return the suitable code for binding an event"""
        if self.is_top_window:
            cls=self.name
        else:
            cls = self.top_window.name
        if is_update:
            handler = 'wxUpdateUIEventHandler({cls}::{event[0]})'.format(cls=cls, event=event)
        else:
            handler = 'static_cast<wxObjectEventFunction>(&{cls}::{event[0]})'.format(cls=cls, event=event)
        if self.is_top_window:
            return 'Connect({event[2]},{handler});\n'.format(event=event, handler=handler)
        else:
            return 'Connect({name}->GetId(), {event[2]}, {handler});\n'.format(name=self._name, event=event, handler=handler)

    def cc_unbind_text(self, event, is_update=False):
        """return the suitable code for binding an event"""
        if self.is_top_window:
            cls=self.name
        else:
            cls = self.top_window.name
        if is_update:
            handler = 'wxUpdateUIEventHandler({cls}::{event[0]})'.format(cls=cls, event=event)
        else:
            handler = 'static_cast<wxObjectEventFunction>(&{cls}::{event[0]})'.format(cls=cls, event=event)
        if self.is_top_window:
            return 'Disconnect({event[2]},{handler});\n'.format(event=event, handler=handler)
        else:
            return 'Disconnect({name}->GetId(), {event[2]}, {handler});\n'.format(name=self._name, event=event, handler=handler)

    def cc_bind_text_set(self):
        text = ''
        event_dict = self.events
        for item in event_dict:
            if item in self._wxui_event_handlers:
                is_update = getattr(self._wxui_event_handlers[item], 'is_update', False)
                text += self.cc_bind_text(event_dict[item], is_update)
        for item in self.sorted_wxui_child:
            text += item.cc_bind_text_set()
        return text

    def cc_unbind_text_set(self):
        text = ''
        event_dict = self.events
        for item in event_dict:
            if item in self._wxui_event_handlers:
                is_update = getattr(self._wxui_event_handlers[item], 'is_update', False)
                text += self.cc_unbind_text(event_dict[item], is_update)
        for item in self.sorted_wxui_child:
            text += item.cc_unbind_text_set()
        return text

    def update_python_connect_event(self):
        """This method updates the content of the connect events method. """
        main_window = self.top_window
        container = main_window.connect_event_handlers
        container.set_content(main_window.py_bind_text_set())

    def update_python_disconnect_event(self):
        main_window = self.top_window
        container = main_window.disconnect_event_handlers
        container.set_content(main_window.py_unbind_text_set())

    def update_cpp_connect_event(self):
        main_window = self.top_window
        container = main_window.connect_event_handlers
        container.set_content(main_window.cc_bind_text_set())

    def update_cpp_disconnect_event(self):
        main_window = self.top_window
        container = main_window.disconnect_event_handlers
        container.set_content(main_window.cc_unbind_text_set())

    def save_state(self):
        """Utility for saving state"""
        if self._implementation is not None:
            self._implementation.save_state()
        if self._subitem_adaptor is not None:
            self._subitem_adaptor.save_state()
        super(Window, self).save_state()

    def delete(self):
        """Utility for saving state"""
        # if self._implementation is not None:
        #     # context.render_undo_redo_removing(self._implementation)
        #     self._implementation.on_undo_redo_removing()
        #     self._implementation.remove_relations()
        #     self._implementation = None
        # if self._subitem_adaptor is not None:
        #     context.render_undo_redo_removing(self._subitem_adaptor)
        #     self._subitem_adaptor.remove_relations()
        #     # self._subitem_adaptor = None intentionally dangling
        super(Window, self).delete()

    def on_undo_redo_removing(self):
        """Do the deletion of the element.
        Must also delete the implementation class."""
        super(Window, self).on_undo_redo_removing()
        if not self.in_undo_redo():
            event_handlers = copy.copy(self._wxui_event_handlers)
            for key in event_handlers:
                if key in self._wxui_event_handlers:
                    method = self._wxui_event_handlers[key]
                    if method is not None:
                        method.delete()
        # if self._implementation is not None:
        #     self._implementation.on_undo_redo_removing()
        #     self._implementation.remove_relations()
        #     self._implementation = None
        # if self._subitem_adaptor is not None:
        #     self._subitem_adaptor.on_undo_redo_removing()
        #     self._subitem_adaptor.remove_relations()
            # self._subitem_adaptor = None intentionally dangling

    def on_undo_redo_add(self):
        """The only thing here is that if the
        parent owner is visible, we must recreate the
        edition window"""
        if not self.is_top_window and getattr(self.parent, '_edition_window', False):
            view = self.document.view
            if view is not None:
                view.rebuild_edition_window()
        super(Window, self).on_undo_redo_add()

    def cpp_init_code(self, container='this'):
        from .controls.additional import StyledTextControl
        if self.is_top_window:
            this = ''
        else:
            this = '{name}->'.format(name=self._name)
        code = ''
        extra_style = self.cc_extra_style_value_text
        if extra_style != '0':
            code += '{this}SetExtraStyle({extra_style} );\n'.format(this=this, extra_style=extra_style)

        if self._minimum_size != (-1, -1):
            code += '{this}SetMinSize(wxSize{sz});\n'.format(this=this, sz=self._minimum_size)

        if self._maximum_size != (-1, -1):
            code += '{this}SetMaxSize(wxSize{sz});\n'.format(this=this, sz=self._maximum_size)

        if type(self) is not StyledTextControl:
            if self._font_info[0]:
                code += '{this}SetFont(wxFont("{font_info}"));\n'.format(this=this, font_info=self._font_info[1])

        background_color = get_colour_cc_text_from_tuple(self._background_colour)
        foreground_color = get_colour_cc_text_from_tuple(self._foreground_colour)
        if len(background_color):
            code += '{this}SetBackgroundColour({background_color});\n'.format(
                this=this, background_color=background_color
            )
        if len(foreground_color):
            code += '{this}SetForegroundColour({foreground_color});\n'.format(
                this=this, foreground_color=foreground_color
            )

        if len(self._tooltip):
            code += '{this}SetToolTip("{tip}");\n'.format(this=this, tip=self._tooltip)
        if len(self._context_help):
            code += '{this}SetHelpText("{text}");\n'.format(this=this, text=self._context_help)
        if not self._enabled:
            code += f'{this}Enable(false);\n'
        if self._hidden:
            code += f'{this}Show(false);\n'
        return code

    def python_init_control_colors(self, this):
        """Return the code for custom colors. This could be override by some
        custom controls that require different handling."""
        code = ''
        background_color = get_colour_py_text_from_tuple(self._background_colour)
        foreground_color = get_colour_py_text_from_tuple(self._foreground_colour)
        if len(background_color):
            code += '{this}.SetBackgroundColour({background_color})\n'.format(
                this=this, background_color=background_color
            )
        if len(foreground_color):
            code += '{this}.SetForegroundColour({foreground_color})\n'.format(
                this=this, foreground_color=foreground_color
            )
        return code

    def python_init_control_code(self, parent='self'):
        """Add extra code over the controls"""
        if self.is_top_window:
            this = 'self'
        else:
            this = 'self.{name}'.format(name=self._name)
        code = ''
        extra_style = self.py_extra_style_value_text
        if extra_style != '0':
            code += '{this}.SetExtraStyle({extra_style} )\n'.format(this=this, extra_style=extra_style)

        if self._minimum_size != (-1, -1):
            code += '{this}.SetMinSize(wx.Size{sz})\n'.format(this=this, sz=self._minimum_size)

        if self._maximum_size != (-1, -1):
            code += '{this}.SetMaxSize(wx.Size{sz})\n'.format(this=this, sz=self._maximum_size)

        if self._font_info[0]:
            code += '{this}.SetFont(wx.Font("{font_info}"))\n'.format(this=this, font_info=self._font_info[1])
        code += self.python_init_control_colors(this)

        if len(self._tooltip):
            code += '{this}.SetToolTip("{tip}")\n'.format(this=this, tip=self._tooltip)
        if len(self._context_help):
            code += '{this}.SetHelpText("{text}")\n'.format(this=this, text=self._context_help)
        if not self._enabled:
            code += '{this}.Enable(False)\n'.format(this=this)
        if self._hidden:
            code += '{this}.Show(False)\n'.format(this=this)
        return code
