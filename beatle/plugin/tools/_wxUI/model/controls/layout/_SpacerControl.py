import wx
from ._SizerControl import SizerControl
from beatle.model import cc


class SpacerControl(SizerControl):

    def __init__(self, **kwargs):
        self._width = 0
        self._height = 0
        super(SpacerControl, self).__init__(**kwargs)

    @property
    def is_sizer(self):
        return False

    @property
    def can_add_control(self):
        return False

    @property
    def can_add_layout_control(self):
        return False

    @property
    def local_spacercontrol_kwargs(self):
        return {
            'width': self._width,
            'height': self._height
        }

    @local_spacercontrol_kwargs.setter
    def local_spacercontrol_kwargs(self, kwargs):
        self._width = kwargs.get('width', self._width)
        self._height = kwargs.get('height', self._height)
        if self._edition_window is not None:
            self._edition_window.SetMinSize(self._width, self._height)

    @property
    def kwargs(self):
        value = self.local_spacercontrol_kwargs
        value.update(super(SpacerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_spacercontrol_kwargs = kwargs
        super(SpacerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('separator')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Spacer',
            'type': 'category',
            'help': 'Fill space inside sizer cell',
            'child': [
                {
                    'label': 'width',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._width,
                    'name': 'width',  # kwarg value
                    'help': "The spacer width."
                },
                {
                    'label': 'height',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._height,
                    'name': 'height',  # kwarg value
                    'help': 'The spacer height.',
                }],
        },]
        _event = []
        parent_property, parent_event = super(SpacerControl, self).model_attributes
        _property += parent_property
        _event += parent_event
        return _property, _event

    def create_edition_window(self, parent):
        container = parent.edition_window_client
        if container is None:
            return
        self._edition_window = wx.Size(self._width, self._height)
        self._edition_window = self.add_to_sizer(container, self._edition_window)
        if container is None:
            return  # weird!!!
        container.Layout()

    def destroy_edition_window(self):
        """Remove the spacer.
        The spacer is always inside a sizer and the _edition_window
        is not a control, but the sizer_item just required to remove
        it from the control."""
        if self._edition_window is not None:
            # No way to remove the sizer ...
            container = getattr(self.parent, '_edition_window', None)
            if container is not None:
                for idx, item in enumerate(container.Children):
                    if item is self._edition_window:
                        container.Detach(idx)
                        self._edition_window.DetachSizer()
                        # we can't destroy the sizer! --> raises an exception
                        # maybe the space sizer is tricky.
                        # TODO : replace space sizers with small panes for design purposes
                        # self._edition_window.Destroy()
            self._edition_window = None

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxSizerItem', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/sizer.h>')
        size = '{sx},{sy}'.format(sx=self._width, sy=self._height)
        return "{name} = {add_code}" .format(
            name=self._name, add_code=self.cc_add_to_sizer_code(container, size))

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self._name)
        size = 'wx.Size({sx},{sy})'.format(sx=self._width,sy=self._height)
        return "{this} = {add_code}" .format(
            this=this, add_code=self.py_add_to_sizer_code(parent, size))
