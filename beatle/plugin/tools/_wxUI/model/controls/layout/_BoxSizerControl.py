import wx
from ._SizerControl import SizerControl
from beatle.model import cc


class BoxSizerControl(SizerControl):

    def __init__(self, **kwargs):
        self._align = 'wxVERTICAL'
        super(BoxSizerControl, self).__init__(**kwargs)

    @property
    def local_boxsizercontrol_kwargs(self):
        return {
            'alignment': self._align,
        }

    @local_boxsizercontrol_kwargs.setter
    def local_boxsizercontrol_kwargs(self, kwargs):
        self._align = kwargs.get('alignment', self._align)

    @property
    def kwargs(self):
        value = self.local_boxsizercontrol_kwargs
        value.update(super(BoxSizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_boxsizercontrol_kwargs = kwargs
        super(BoxSizerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('sizer')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Box Sizer',
            'type': 'category',
            'help': 'Attributes of box sizer',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The box sizer name."
                },
                {
                    'label': 'alignment',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._align,
                    'name': 'alignment',  # kwarg value
                    'help': 'alignment of elements.',
                    'choices': ['wxVERTICAL', 'wxHORIZONTAL']
                }],
        },]
        _event = []
        parent_property, parent_event = super(BoxSizerControl, self).model_attributes
        _property += parent_property
        _event += parent_event
        return _property, _event

    def create_edition_window(self, parent):
        container = parent.edition_window_client
        if container is None:
            return
        align_map = {'wxVERTICAL': wx.VERTICAL, 'wxHORIZONTAL': wx.HORIZONTAL}
        self._edition_window = wx.BoxSizer(align_map[self._align])
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        container = self.add_to_sizer(container, self._edition_window)
        if container is None:
            return  # weird!!!
        container.Layout()

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxBoxSizer', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/sizer.h>')
        code = "{name} = new wxBoxSizer({align});\n" .format(name=self._name, align=self._align)
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        align_map = {'wxVERTICAL': 'wx.VERTICAL', 'wxHORIZONTAL': 'wx.HORIZONTAL'}
        code = '{this} = wx.BoxSizer({align})\n'.format(this=this, align=align_map[self._align])
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
