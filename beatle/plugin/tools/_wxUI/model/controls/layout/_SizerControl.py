import wx
from ..._wxUICommon import wxUICommon
from .._tools import *
from beatle.model import py
from beatle.lib.decorators import upgrade_version
from ..containers._CollapsiblePanelControl import CollapsiblePanelControl


class SizerControl(wxUICommon):
    """The class sizer control is not used directly in the designs.
    Instead, it is the base for other sizers. The main target is
    to handler common sizer properties and child sizer ones, just
    like the Window class for controls."""

    def __init__(self, *args, **kwargs):
        from ._GridBagSizerControl import GridBagSizerControl
        self._parent = kwargs.get('parent', None)
        # test now
        # if self._parent is not None and getattr(self._parent, 'is_sizer', False):
        #     if type(self._parent) is GridBagSizerControl:
        #         self._subitem_adaptor = BagSizerItemAdaptor(control=self)
        #     else:
        #         self._subitem_adaptor = SizerItemAdaptor(control=self)
        self._minimum_size = (-1, -1)
        super(SizerControl, self).__init__(*args, **kwargs)
        # test now
        #self.implement()


    @property
    def subitem_adaptor(self):
        return self._subitem_adaptor

    @property
    def minimum_size(self):
        return self._minimum_size

    @property
    def is_sizer(self):
        return True

    @property
    def can_add_control(self):
        return True

    @property
    def can_add_layout_control(self):
        return True

    @property
    def can_add_tool_bar_control(self):
        return True

    @property
    def local_sizercontrol_kwargs(self):
        from ._SpacerControl import SpacerControl
        if type(self) is not SpacerControl:
            value = {'minimum_size': self._minimum_size}
        else:
            value = {}
        return value

    @local_sizercontrol_kwargs.setter
    def local_sizercontrol_kwargs(self, kwargs):
        from ._SpacerControl import SpacerControl
        if type(self) is not SpacerControl:
            self._minimum_size = tuple(kwargs.get('minimum_size', self._minimum_size))

    @property
    def kwargs(self):
        value = self.local_sizercontrol_kwargs
        value.update(super(SizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        from ._SpacerControl import SpacerControl
        self.local_sizercontrol_kwargs = kwargs
        super(SizerControl, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            main_window = self.top_window
            if main_window is not None:
                main_window.update_implementation()
        if self._edition_window is None:
            return
        if type(self) is not SpacerControl:
            if 'minimum_size' in kwargs:
                self._edition_window.SetMinSize(kwargs['minimum_size'])
        top_window = self.top_window
        if top_window is None:
            return
        container = getattr(self.parent, '_edition_window', None)
        if container is not None:
            container.Layout()

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        from ._SpacerControl import SpacerControl
        if type(self) is not SpacerControl:
            _property = [{
                'label': 'Sizer',
                'type': 'category',
                'help': 'Attributes of sizer.',
                'child': [
                    {
                        'label': 'minimum_size',
                        'type': 'int_vector',
                        'value': self._minimum_size,
                        'name': 'minimum_size',
                        'labels': [
                            ('width', "sizer width"),
                            ('height', "sizer height")
                        ],
                        'help': 'Sets the minimum size.'
                    }],
            }]
        else:
            _property = []
        _event = []
        _base_property, _base_event = super(SizerControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def add_to_sizer(self, container, window):
        """This method is invoked from specializations while
        creating implementation window for display edit"""

        if type(window) is not wx.Size and self.minimum_size != (-1, -1):  # filter sizers
            window.SetMinSize(*self.minimum_size)

        if self._subitem_adaptor is not None:
            sizer_item = self._subitem_adaptor.Realize()
            if type(window) is wx.Size:
                return sizer_item
            top_window = self.sizer_root
            if top_window is not None:
                container = self.parent.edition_window_client
        elif self.parent.is_sizer_root:
            self.parent.edition_window_client.SetSizer(window)
            self.parent.edition_window_client.Layout()
            if type(self.parent) is CollapsiblePanelControl:
                window.Fit(self.parent.edition_window_client)
        return container

    def cc_add_to_sizer_code(self, container, window):
        from ._SpacerControl import SpacerControl
        code = ''
        if type(self) is not SpacerControl and self.minimum_size != (-1, -1):
            code += '{window}->SetMinSize({self.minimum_size});\n'.format(window=window, self=self)
        if self._subitem_adaptor is not None:
            code += self._subitem_adaptor.cc_realize_code(container, window)
        elif self.parent.is_sizer_root:
            if container == 'this':
                code += 'SetSizer({window});\n'.format(window=window)
            else:
                code += '{container}->SetSizer({window});\n'.format(container=container, window=window)
        return code

    def py_add_to_sizer_code(self, container, window):
        from ._SpacerControl import SpacerControl
        code = ''
        if type(self) is not SpacerControl and self.minimum_size != (-1, -1):
            code += '{window}.SetMinSize({self.minimum_size})\n'.format(window=window, self=self)
        if self._subitem_adaptor is not None:
            code += self._subitem_adaptor.py_realize_code(container, window)
        elif self.parent.is_sizer_root:
            code += '{container}.SetSizer({window})\n'.format(container=container, window=window)
        return code

    def remove_from(self, container):
        if self._subitem_adaptor is not None:
            self._subitem_adaptor.detach_from_parent()
        else:
            container.SetSizer(None, False)

    def save_state(self):
        """Do the deletion of the element.
        Must also delete the implementation class."""
        if self._implementation is not None:
            self._implementation.save_state()
        if self._subitem_adaptor is not None:
            self._subitem_adaptor.save_state()
        super(SizerControl, self).save_state()

    def delete(self):
        """Do the deletion of the element.
        Must also delete the implementation class."""
        # if self._implementation is not None:
        #     # context.render_undo_redo_removing(self._implementation)
        #     self._implementation.on_undo_redo_removing()
        #     self._implementation.remove_relations()
        #     self._implementation = None
        # if self._subitem_adaptor is not None:
        #     context.render_undo_redo_removing(self._subitem_adaptor)
        #     self._subitem_adaptor.remove_relations()
        #     # self._subitem_adaptor = None intentionally dangling
        super(SizerControl, self).delete()

    def on_undo_redo_removing(self):
        """This method handles the removing operation.
        The base call can trigger destroy_edition_window
        because tree selection change, but this is not
        guaranteed so we must call destroy_edition_window
        afterwards (it's safe even if already done)"""
        super(SizerControl, self).on_undo_redo_removing()
        if not self.is_top_window and getattr(self.parent, '_edition_window', False):
            view = self.document.view
            if view is not None:
                view.rebuild_edition_window()
        # previous
        # if self._implementation is not None:
        #     self._implementation.on_undo_redo_removing()
        #     self._implementation.remove_relations()
        #     self._implementation = None
        # if self._subitem_adaptor is not None:
        #     self._subitem_adaptor.on_undo_redo_removing()
        #     self._subitem_adaptor.remove_relations()
        #     # self._subitem_adaptor = None intentionally dangling

    def on_undo_redo_add(self):
        """The only thing here is that if the
        parent owner is visible, we must recreate the
        edition window"""
        super(SizerControl, self).on_undo_redo_add()
        #if not self.is_top_window and getattr(self.parent, '_edition_window', False):
        #    self.create_edition_window(self.parent)

    def destroy_edition_window(self):
        if self._edition_window is not None:
            for element in reversed(self.sorted_wxui_child):
                element.destroy_edition_window()
            container = self.parent.edition_window_client
            if container is not None:
                self.remove_from(container)
            self._edition_window.Destroy()
            self._edition_window = None

    def declare_python_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # add a controls folder in order to avoid garbage
        self._implementation = py.MemberData(
            parent=container,
            scope='instance',
            value="None",
            name=self.name,
        )

    def python_init_control_code(self, parent='self'):
        raise RuntimeError("missing python_init_control_code for sizer of type {}".format(str(type(self))))

    def toolbar_parent_and_sizer(self, **kwargs):
        """Toolbars can be added to sizers. Due to implementation details,
        related with mockup frames, we need this method for providing a sizer
        container when adding the toolbar to the frame. This method is required
        here but recovers default behaviour"""
        main_window = self.sizer_root
        if main_window:
            return main_window.edition_window_client, None

    def update_python_event_handlers(self):
        """Sizers doesn't have event handlers, but must propagate"""
        changes = False
        for item in self.sorted_wxui_child:
            changes = item.update_python_event_handlers() or changes
        return changes

    def update_cpp_event_handlers(self):
        """Sizers doesn't have event handlers, but must propagate"""
        changes = False
        for item in self.sorted_wxui_child:
            changes = item.update_cpp_event_handlers() or changes
        return changes

    def py_bind_text_set(self):
        text = ''
        for item in self.sorted_wxui_child:
            text += item.py_bind_text_set()
        return text

    def cc_bind_text_set(self):
        text = ''
        for item in self.sorted_wxui_child:
            text += item.cc_bind_text_set()
        return text

    def py_unbind_text_set(self):
        text = ''
        for item in self.sorted_wxui_child:
            text += item.py_unbind_text_set()
        return text

    def cc_unbind_text_set(self):
        text = ''
        for item in self.sorted_wxui_child:
            text += item.cc_unbind_text_set()
        return text

