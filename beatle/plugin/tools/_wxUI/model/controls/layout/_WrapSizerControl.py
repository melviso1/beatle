import wx
from ._SizerControl import SizerControl
from beatle.model import cc


class WrapSizerControl(SizerControl):
    """Wrap sizer is a sizer that aligns
    widgets on one direction until space exhausted
    and the create a line of controls in the
    complementary direction"""

    def __init__(self, **kwargs):
        self._orientation = 'wxVERTICAL'
        super(WrapSizerControl, self).__init__(**kwargs)

    @property
    def local_wrapsizercontrol_kwargs(self):
        return {
            'orientation': self._orientation,
        }

    @local_wrapsizercontrol_kwargs.setter
    def local_wrapsizercontrol_kwargs(self, kwargs):
        self._orientation = kwargs.get('orientation', self._orientation)

    @property
    def kwargs(self):
        value = self.local_wrapsizercontrol_kwargs
        value.update(super(WrapSizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_wrapsizercontrol_kwargs = kwargs
        super(WrapSizerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('wrap_sizer')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Wrap sizer',
            'type': 'category',
            'help': 'Attributes of wrap sizer',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The wrap sizer name."
                },
                {
                    'label': 'orientation',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._orientation,
                    'name': 'orientation',  # kwarg value
                    'help': 'main growing direction.',
                    'choices': ['wxHORIZONTAL', 'wxVERTICAL']
                }],
        },]
        _event = []
        parent_property, parent_event = super(WrapSizerControl, self).model_attributes
        _property += parent_property
        _event += parent_event
        return _property, _event

    def create_edition_window(self, parent):
        container = parent.edition_window_client
        if container is None:
            return
        orientation_map = {'wxHORIZONTAL':wx.HORIZONTAL, 'wxVERTICAL': wx.VERTICAL}
        self._edition_window = wx.WrapSizer( orientation_map[self._orientation])
        if container is None:
            return  # weird!!!
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self.add_to_sizer(container, self._edition_window)

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxWrapSizer', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/wrapsizer.h>')
        code = "{name} = new wxWrapSizer({align});\n" .format(name=self._name, align=self._orientation)
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        align_map = {'wxVERTICAL': 'wx.VERTICAL', 'wxHORIZONTAL': 'wx.HORIZONTAL'}
        code = '{this} = wx.WrapSizer({align})\n'.format(this=this, align=align_map[self._orientation])
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
