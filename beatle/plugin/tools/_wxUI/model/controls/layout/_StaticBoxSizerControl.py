import wx
from beatle.lib.handlers import identifier
from ._SizerControl import SizerControl
from beatle.model import cc


class StaticBoxSizerControl(SizerControl):
    """Wrap sizer is a sizer that aligns
    widgets on one direction until space exhausted
    and the create a line of controls in the
    complementary direction"""

    def __init__(self, **kwargs):
        self._orientation = 'wxVERTICAL'
        self._label = 'label'
        self._id_ = 'wxID_ANY'
        super(StaticBoxSizerControl, self).__init__(**kwargs)

    @property
    def local_staticsizercontrol_kwargs(self):
        return {
            'orientation': self._orientation,
            'label': self._label,
            'id': self._id_,
        }

    @local_staticsizercontrol_kwargs.setter
    def local_staticsizercontrol_kwargs(self, kwargs):
        self._orientation = kwargs.get('orientation', self._orientation)
        self._label = kwargs.get('label', self._label)
        self._id_ = kwargs.get('id', self._id_)

    @property
    def kwargs(self):
        value = self.local_staticsizercontrol_kwargs
        value.update(super(StaticBoxSizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_staticsizercontrol_kwargs = kwargs
        super(StaticBoxSizerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('static_box_sizer')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Wrap sizer',
            'type': 'category',
            'help': 'Attributes of wrap sizer',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The static box sizer name."
                },
                {
                    'label': 'orientation',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._orientation,
                    'name': 'orientation',  # kwarg value
                    'help': 'main growing direction.',
                    'choices': ['wxHORIZONTAL', 'wxVERTICAL']
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'The static box label'
                },
                {
                    'label': 'id',
                    'type': 'string',
                    'value': self._id_,
                    'name': 'id',
                    'help': 'window unique identifier'
                },
            ],
        },]
        _event = []
        parent_property, parent_event = super(StaticBoxSizerControl, self).model_attributes
        _property += parent_property
        _event += parent_event
        return _property, _event

    @property
    def py_id(self):
        """return a window identifier suitable for edition window creation"""
        if self._id_ == 'wxID_ANY':
            return wx.ID_ANY
        else:
            return identifier(self._id_)

    @property
    def py_id_str(self):
        """return a window identifier suitable for edition window creation"""
        if self._id_ == 'wxID_ANY':
            return 'wx.ID_ANY'
        else:
            return self._id_

    @property
    def py_orientation(self):
        return {'wxHORIZONTAL': wx.HORIZONTAL, 'wxVERTICAL': wx.VERTICAL}[self._orientation]

    @property
    def py_orientation_text(self):
        return {'wxHORIZONTAL': 'wx.HORIZONTAL', 'wxVERTICAL': 'wx.VERTICAL'}[self._orientation]

    def create_edition_window(self, parent):
        """Well, there are two methods for constructing the static box sizer.
        We will use the one used by wxformbuilder only because it provides a
        way to specify the box window identifier, although the other constructor
        is more simple"""
        container = parent.edition_window_client
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return  # TODO : trace this as error
        top_edition_window = top_window.edition_window_client
        if top_edition_window is None:
            return  # TODO : trace this as error
        self._edition_window = wx.StaticBoxSizer(
            wx.StaticBox(top_edition_window, self.py_id, self._label), self.py_orientation)
        if container is None:
            return  # weird!!!
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self.add_to_sizer(container, self._edition_window)

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxStaticBoxSizer', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('# include <wx/statbox.h>')
        self.add_cpp_required_header('#include <wx/sizer.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = '{name} = new wxStaticBoxSizer(new wxStaticBox({owner}, {id}, "{label}"), {orientation});\n' .format(
            name=self._name, owner=owner, id=self._id_, label=self._label, orientation=self._orientation)
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = '{this} = wx.StaticBoxSizer(wx.StaticBox({owner}, {id}, "{label}"), {orientation})\n'.format(
            this=this, owner=owner, id=self.py_id_str, label=self._label, orientation=self.py_orientation_text)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
