import wx
from ._SizerControl import SizerControl
from beatle.model import cc


class GridBagSizerControl(SizerControl):

    def __init__(self, **kwargs):
        self._empty_cell_size = (-1, -1)
        self._vgap = 0
        self._hgap = 0
        self._growablerows = ''
        self._growablecolumns = ''
        self._flexible_direction = 'wxBOTH'
        self._non_flexible_grow_mode = 'wxFLEX_GROWMODE_SPECIFIED'
        super(GridBagSizerControl, self).__init__(**kwargs)

    @property
    def local_gridbagsizercontrol_kwargs(self):
        return {
            'empty_cell_size': self._empty_cell_size,
            'vgap': self._vgap,
            'hgap': self._hgap,
            'growablerows': self._growablerows,
            'growablecolumns': self._growablecolumns,
            'flexible_direction': self._flexible_direction,
            'non_flexible_grow_mode': self._non_flexible_grow_mode,
        }

    @local_gridbagsizercontrol_kwargs.setter
    def local_gridbagsizercontrol_kwargs(self, kwargs):
        self._empty_cell_size = kwargs.get('empty_cell_size', self._empty_cell_size)
        self._vgap = kwargs.get('vgap', self._vgap)
        self._hgap = kwargs.get('hgap', self._hgap)
        self._growablerows = kwargs.get('growablerows', self._growablerows)
        self._growablecolumns = kwargs.get('growablecolumns', self._growablecolumns)
        self._flexible_direction = kwargs.get('flexible_direction', self._flexible_direction)
        self._non_flexible_grow_mode = kwargs.get('non_flexible_grow_mode', self._non_flexible_grow_mode)

    @property
    def kwargs(self):
        value = self.local_gridbagsizercontrol_kwargs
        value.update(super(GridBagSizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_gridbagsizercontrol_kwargs = kwargs
        super(GridBagSizerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('grid_bag_sizer')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Grid bag sizer',
            'type': 'category',
            'help': 'Attributes of grid bag sizer',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The grid bag sizer name."
                },
                {
                    'label': 'empty cell size',
                    'type': 'int_vector',
                    'read_only': False,
                    'value': self._empty_cell_size,
                    'name': 'empty_cell_size',  # kwarg value
                    'labels': [
                        ('width', "cell width"),
                        ('height', "cell height")
                    ],
                    'help': 'size applied for empty cells.'
                },
                {
                    'label': 'vgap',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._vgap,
                    'name': 'vgap',  # kwarg value
                    'help': 'vertical gap between cells in pixels.'
                },
                {
                    'label': 'hgap',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._hgap,
                    'name': 'hgap',  # kwarg value
                    'help': 'horizontal gap between cells in pixels.'
                },
                {
                    'label': 'growing rows',
                    'type': 'string',
                    'read_only': False,
                    'value': self._growablerows,
                    'name': 'growablerows',  # kwarg value
                    'help': 'comma delimited zero-based index of growing rows.'
                },
                {
                    'label': 'growing columns',
                    'type': 'string',
                    'read_only': False,
                    'value': self._growablecolumns,
                    'name': 'growablecolumns',  # kwarg value
                    'help': 'comma delimited zero-based index of growing columns.'
                },
                {
                    'label': 'flexible direction',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._flexible_direction,
                    'name': 'flexible_direction',  # kwarg value
                    'help': 'direction of flexible, unequal growing.',
                    'choices': ['wxBOTH', 'wxHORIZONTAL', 'wxVERTICAL']
                },
                {
                    'label': 'non flexible grow mode',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._non_flexible_grow_mode,
                    'name': 'non_flexible_grow_mode',  # kwarg value
                    'help': 'computing of non-flexible, equal growing.\n'
                            'wxFLEX_GROWMODE_SPECIFIED honors growing specifications (default).\n'
                            'wxFLEX_GROWMODE_NONE blocks growing on non-flexible direction.\n'
                            'wxFLEX_GROWMODE_ALL applies equal sizing.',
                    'choices': ['wxFLEX_GROWMODE_SPECIFIED', 'wxFLEX_GROWMODE_NONE', 'wxFLEX_GROWMODE_ALL']
                }],
        },]
        _event = []
        parent_property, parent_event = super(GridBagSizerControl, self).model_attributes
        _property += parent_property
        _event += parent_event
        return _property, _event

    def create_edition_window(self, parent):
        container = parent.edition_window_client
        if container is None:
            return
        flex_map = {'wxBOTH':wx.BOTH, 'wxHORIZONTAL':wx.HORIZONTAL, 'wxVERTICAL': wx.VERTICAL}
        nonflex_map = {'wxFLEX_GROWMODE_ALL':wx.FLEX_GROWMODE_ALL,'wxFLEX_GROWMODE_NONE':wx.FLEX_GROWMODE_NONE,
                       'wxFLEX_GROWMODE_SPECIFIED':wx.FLEX_GROWMODE_SPECIFIED}
        self._edition_window = wx.GridBagSizer(self._vgap, self._hgap)
        self._edition_window.SetFlexibleDirection(flex_map[self._flexible_direction])
        self._edition_window.SetNonFlexibleGrowMode(nonflex_map[self._non_flexible_grow_mode])
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self.add_to_sizer(container, self._edition_window)

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxGridBagSizer', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/sizer.h>')
        code = "{name} = new wxGridBagSizer({vgap}, {hgap});\n" .format(
            name=self._name, vgap=self._vgap, hgap=self._hgap)
        code += "{name}->SetFlexibleDirection({dir});\n".format(
            name=self._name, dir=self._flexible_direction
        )
        code += "{name}->SetNonFlexibleGrowMode( {mode);\n".format(
            name=self._name, mode=self._non_flexible_grow_mode
        )
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        flex_map = {'wxBOTH': 'wx.BOTH', 'wxHORIZONTAL': 'wx.HORIZONTAL', 'wxVERTICAL': 'wx.VERTICAL'}
        nonflex_map = {'wxFLEX_GROWMODE_ALL': 'wx.FLEX_GROWMODE_ALL','wxFLEX_GROWMODE_NONE': 'wx.FLEX_GROWMODE_NONE',
                       'wxFLEX_GROWMODE_SPECIFIED': 'wx.FLEX_GROWMODE_SPECIFIED'}

        this = 'self.{name}'.format(name=self.name)
        code = '{this} = wx.GridBagSizer({vgap}, {hgap})\n'.format(
            this=this, vgap=self._vgap, hgap=self._hgap)
        code += "self.{name}.SetFlexibleDirection({dir})\n".format(
            name=self._name, dir=flex_map[self._flexible_direction]
        )
        code += "self.{name}.SetNonFlexibleGrowMode({mode})\n".format(
            name=self._name, mode=nonflex_map[self._non_flexible_grow_mode]
        )
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
