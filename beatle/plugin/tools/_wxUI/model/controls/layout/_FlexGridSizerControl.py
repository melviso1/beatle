import wx
from ._SizerControl import SizerControl
from beatle.model import cc


class FlexGridSizerControl(SizerControl):

    def __init__(self, **kwargs):
        self._rows = 1
        self._columns = 1
        self._minimum_size = (-1, -1)
        self._vgap = 0
        self._hgap = 0
        self._growablerows = ''
        self._growablecolumns = ''
        self._flexible_direction = 'wxBOTH'
        self._non_flexible_grow_mode = 'wxFLEX_GROWMODE_SPECIFIED'
        super(FlexGridSizerControl, self).__init__(**kwargs)

    @property
    def local_flexgridsizercontrol_kwargs(self):
        return {
            'rows': self._rows,
            'columns': self._columns,
            'vgap': self._vgap,
            'hgap': self._hgap,
            'growablerows': self._growablerows,
            'growablecolumns': self._growablecolumns,
            'flexible_direction': self._flexible_direction,
            'non_flexible_grow_mode': self._non_flexible_grow_mode,
        }

    @local_flexgridsizercontrol_kwargs.setter
    def local_flexgridsizercontrol_kwargs(self, kwargs):
        self._rows = kwargs.get('rows', self._rows)
        self._columns = kwargs.get('columns', self._columns)
        self._vgap = kwargs.get('vgap', self._vgap)
        self._hgap = kwargs.get('hgap', self._hgap)
        self._growablerows = kwargs.get('growablerows', self._growablerows)
        self._growablecolumns = kwargs.get('growablecolumns', self._growablecolumns)
        self._flexible_direction = kwargs.get('flexible_direction', self._flexible_direction)
        self._non_flexible_grow_mode = kwargs.get('non_flexible_grow_mode', self._non_flexible_grow_mode)
        if self._edition_window:
            if self._edition_window.GetCols() != self._columns:
                self._edition_window.SetCols(self._columns)
            if self._edition_window.GetRows() != self._rows:
                self._edition_window.SetRows(self._rows)
            if self._edition_window.GetHGap() != self._hgap:
                self._edition_window.SetHGap(self._hgap)
            if self._edition_window.GetVGap() != self._vgap:
                self._edition_window.SetVGap(self._vgap)

            self._edition_window.SetFlexibleDirection(self.flexible_direction_value)
            self._edition_window.SetNonFlexibleGrowMode(self.non_flexible_direction_value)

            try:
                growable_cols = [int(x) for x in self._growablecolumns.split(',')]
                for i in range(self._columns):
                    if self._edition_window.IsColGrowable(i):
                        if i not in growable_cols:
                            self._edition_window.RemoveGrowableCol(i)
                    else:
                        if i in growable_cols:
                            self._edition_window.AddGrowableCol(i)
                growable_rows = [int(x) for x in self._growablerows.split(',')]
                for i in range(self._rows):
                    if self._edition_window.IsRowGrowable(i):
                        if i not in growable_rows:
                            self._edition_window.RemoveGrowableRow(i)
                    else:
                        if i in growable_rows:
                            self._edition_window.AddGrowableRow(i)
            except:
                pass
            try:
                growable_rows = [int(x) for x in self._growablerows.split(',')]
                for i in range(self._rows):
                    if self._edition_window.IsRowGrowable(i):
                        if i not in growable_rows:
                            self._edition_window.RemoveGrowableRow(i)
                    else:
                        if i in growable_rows:
                            self._edition_window.AddGrowableRi(i)
            except:
                pass

    @property
    def kwargs(self):
        value = self.local_flexgridsizercontrol_kwargs
        value.update(super(FlexGridSizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_flexgridsizercontrol_kwargs = kwargs
        super(FlexGridSizerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('flexible_sizer')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Flex grid sizer',
            'type': 'category',
            'help': 'Attributes of flex grid sizer',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The flex grid sizer name."
                },
                {
                    'label': 'rows',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._rows,
                    'name': 'rows',  # kwarg value
                    'help': 'the number of rows of the grid.'
                },
                {
                    'label': 'columns',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._columns,
                    'name': 'columns',  # kwarg value
                    'help': 'the number of columns of the grid.'
                },
                {
                    'label': 'vgap',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._vgap,
                    'name': 'vgap',  # kwarg value
                    'help': 'vertical gap between cells in pixels.'
                },
                {
                    'label': 'hgap',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._hgap,
                    'name': 'hgap',  # kwarg value
                    'help': 'horizontal gap between cells in pixels.'
                },
                {
                    'label': 'growing rows',
                    'type': 'string',
                    'read_only': False,
                    'value': self._growablerows,
                    'name': 'growablerows',  # kwarg value
                    'help': 'comma delimited zero-based index of growing rows.'
                },
                {
                    'label': 'growing columns',
                    'type': 'string',
                    'read_only': False,
                    'value': self._growablecolumns,
                    'name': 'growablecolumns',  # kwarg value
                    'help': 'comma delimited zero-based index of growing columns.'
                },
                {
                    'label': 'flexible direction',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._flexible_direction,
                    'name': 'flexible_direction',  # kwarg value
                    'help': 'direction of flexible, unequal growing.',
                    'choices': ['wxBOTH', 'wxHORIZONTAL', 'wxVERTICAL']
                },
                {
                    'label': 'non flexible grow mode',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._non_flexible_grow_mode,
                    'name': 'non_flexible_grow_mode',  # kwarg value
                    'help': 'computing of non-flexible, equal growing.\n'
                            'wxFLEX_GROWMODE_SPECIFIED honors growing specifications (default).\n'
                            'wxFLEX_GROWMODE_NONE blocks growing on non-flexible direction.\n'
                            'wxFLEX_GROWMODE_ALL applies equal sizing.',
                    'choices': ['wxFLEX_GROWMODE_SPECIFIED', 'wxFLEX_GROWMODE_NONE', 'wxFLEX_GROWMODE_ALL']
                }],
        },]
        _event = []
        parent_property, parent_event = super(FlexGridSizerControl, self).model_attributes
        _property += parent_property
        _event += parent_event
        return _property, _event

    @property
    def flexible_direction_value(self):
        flex_map = {'wxBOTH':wx.BOTH, 'wxHORIZONTAL':wx.HORIZONTAL, 'wxVERTICAL': wx.VERTICAL}
        return flex_map[self._flexible_direction]

    @property
    def py_flexible_direction_text(self):
        flex_map = {'wxBOTH': 'wx.BOTH', 'wxHORIZONTAL': 'wx.HORIZONTAL', 'wxVERTICAL': 'wx.VERTICAL'}
        return flex_map[self._flexible_direction]

    @property
    def non_flexible_direction_value(self):
        nonflex_map = {'wxFLEX_GROWMODE_ALL':wx.FLEX_GROWMODE_ALL,'wxFLEX_GROWMODE_NONE':wx.FLEX_GROWMODE_NONE,
                       'wxFLEX_GROWMODE_SPECIFIED':wx.FLEX_GROWMODE_SPECIFIED}
        return nonflex_map[self._non_flexible_grow_mode]

    @property
    def py_non_flexible_direction_text(self):
        nonflex_map = {'wxFLEX_GROWMODE_ALL': 'wx.FLEX_GROWMODE_ALL', 'wxFLEX_GROWMODE_NONE': 'wx.FLEX_GROWMODE_NONE',
                       'wxFLEX_GROWMODE_SPECIFIED': 'wx.FLEX_GROWMODE_SPECIFIED'}
        return nonflex_map[self._non_flexible_grow_mode]

    def create_edition_window(self, parent):
        container = parent.edition_window_client
        if container is None:
            return
        self._edition_window = wx.FlexGridSizer(self._rows, self._columns, self._vgap, self._hgap)
        self._edition_window.SetFlexibleDirection(self.flexible_direction_value)
        self._edition_window.SetNonFlexibleGrowMode(self.non_flexible_direction_value)
        try:
            growable_cols = [int(x) for x in self._growablecolumns.split(',') if len(x) > 0]
            for i in range(self._columns):
                if i in growable_cols:
                    self._edition_window.AddGrowableCol(i)
            growable_rows = [int(x) for x in self._growablerows.split(',') if len(x) > 0]
            for i in range(self._rows):
                if i in growable_rows:
                    self._edition_window.AddGrowableRow(i)
        except:
            pass
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self.add_to_sizer(container, self._edition_window)

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxFlexGridSizer', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/sizer.h>')
        code = "{name} = new wxFlexGridSizer({rows}, {cols}, {vgap}, {hgap});\n" .format(
            name=self._name, rows=self._rows, cols=self._columns, vgap=self._vgap, hgap=self._hgap)
        code += f'{self._name}->SetFlexibleDirection({self._flexible_direction});\n'
        code += f'{self._name}->SetNonFlexibleGrowMode({self._non_flexible_grow_mode});\n'
        growable_cols = [int(x) for x in self._growablecolumns.split(',') if len(x) > 0]
        for i in range(self._columns):
            if i in growable_cols:
                code += '{name}->AddGrowableCol({i});\n'.format(name=self._name, i=i)
        growable_rows = [int(x) for x in self._growablerows.split(',') if len(x) > 0]
        for i in range(self._rows):
            if i in growable_rows:
                code += '{name}->AddGrowableRow({i});\n'.format(name=self._name, i=i)

        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)

        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        code = '{this} = wx.FlexGridSizer({rows}, {cols}, {vgap}, {hgap})\n'.format(
            this=this, rows=self._rows, cols=self._columns, vgap=self._vgap, hgap=self._hgap)
        code += f'{this}.SetFlexibleDirection({self.py_flexible_direction_text})\n'
        code += f'{this}.SetNonFlexibleGrowMode({self.py_non_flexible_direction_text})\n'
        growable_cols = [int(x) for x in self._growablecolumns.split(',') if len(x) > 0]
        for i in range(self._columns):
            if i in growable_cols:
                code += '{this}.AddGrowableCol({i})\n'.format(this=this, i=i)
        growable_rows = [int(x) for x in self._growablerows.split(',') if len(x) > 0]
        for i in range(self._rows):
            if i in growable_rows:
                code += '{this}.AddGrowableRow({i})\n'.format(this=this, i=i)

        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
