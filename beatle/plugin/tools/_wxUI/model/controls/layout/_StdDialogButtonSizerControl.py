import wx
from beatle.lib.handlers import identifier
from ._SizerControl import SizerControl
from beatle.model import cc


class StdDialogButtonSizerControl(SizerControl):
    """StdDialogButton sizer is a special kind of sizer
    that contains several buttons"""

    _button_map = {
        1: wx.ID_OK,
        2: wx.ID_YES,
        4: wx.ID_SAVE,
        8: wx.ID_APPLY,
        16: wx.ID_NO,
        32: wx.ID_CANCEL,
        64: wx.ID_HELP,
        128: wx.ID_CONTEXT_HELP,
    }

    _cc_button_map_text = {
        1: 'wxID_OK',
        2: 'wxID_YES',
        4: 'wxID_SAVE',
        8: 'wxID_APPLY',
        16: 'wxID_NO',
        32: 'wxID_CANCEL',
        64: 'wxID_HELP',
        128: 'wxID_CONTEXT_HELP',
    }

    _py_button_map_text = {
        1: 'wx.ID_OK',
        2: 'wx.ID_YES',
        4: 'wx.ID_SAVE',
        8: 'wx.ID_APPLY',
        16: 'wx.ID_NO',
        32: 'wx.ID_CANCEL',
        64: 'wx.ID_HELP',
        128: 'wx.ID_CONTEXT_HELP',
    }

    def __init__(self, **kwargs):
        self._buttons = 33  # Ok and Cancel
        super(StdDialogButtonSizerControl, self).__init__(**kwargs)

    @property
    def local_staticsizercontrol_kwargs(self):
        return {'buttons': self._buttons}

    @local_staticsizercontrol_kwargs.setter
    def local_staticsizercontrol_kwargs(self, kwargs):
        self._buttons = kwargs.get('buttons', self._buttons)

    @property
    def kwargs(self):
        value = self.local_staticsizercontrol_kwargs
        value.update(super(StdDialogButtonSizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_staticsizercontrol_kwargs = kwargs
        super(StdDialogButtonSizerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('dialog_buttons_sizer')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'DialogButtonsSizer',
            'type': 'category',
            'help': 'Attributes of dialog buttons sizer',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The standard dialog sizer name."
                },
                {
                    'label': 'buttons',
                    'type': 'multi_choice',
                    'read_only': False,
                    'value': self._buttons,
                    'name': 'buttons',  # kwarg value
                    'help': 'choose the buttons to apear in the dialog.',
                    'options': [
                        ('Ok', 'Include Ok buttom.'),
                        ('Yes', 'Include Yes buttom.'),
                        ('Save', 'Include Save buttom.'),
                        ('Apply', 'Include Apply buttom.'),
                        ('No', 'Include No buttom.'),
                        ('Cancel', 'Include Cancel buttom.'),
                        ('Help', 'Include Help buttom.'),
                        ('Context Help', 'Include Context Help buttom.'),
                    ]
                }]
        }]
        _event = []
        parent_property, parent_event = super(StdDialogButtonSizerControl, self).model_attributes
        _property += parent_property
        _event += parent_event
        return _property, _event

    def create_edition_window(self, parent):
        """Well, there are two methods for constructing the static box sizer.
        We will use the one used by wxformbuilder only because it provides a
        way to specify the box window identifier, although the other constructor
        is more simple"""
        container = parent.edition_window_client
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # TODO : trace this as error
        top_edition_window = top_window.edition_window_client
        if top_edition_window is None:
            return  # TODO : trace this as error
        self._edition_window = wx.StdDialogButtonSizer()
        for k,w in StdDialogButtonSizerControl._button_map.items():
            if self._buttons & k:
                self._edition_window.AddButton(wx.Button(top_edition_window, w))
        self._edition_window.Realize()
        if container is None:
            return  # weird!!!
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self.add_to_sizer(container, self._edition_window)

    @property
    def can_add_layout_control(self):
        return False

    @property
    def can_add_control(self):
        return False

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxStdDialogButtonSizer', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/sizer.h>')
        self.add_cpp_required_header('#include <wx/button.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f"{self.name} = new wxStdDialogButtonSizer();\n"
        for k,w in StdDialogButtonSizerControl._cc_button_map_text.items():
            if self._buttons & k:
                code += '{name}->AddButton(new wxButton({owner},{kind}));\n'.format(
                    name=self._name, owner=owner, kind=w
                )
        code += f'{self._name}->Realize();\n'
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        code = "{this} = wx.StdDialogButtonSizer()\n".format(this=this)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        for k,w in StdDialogButtonSizerControl._py_button_map_text.items():
            if self._buttons & k:
                code += '{this}.AddButton(wx.Button({owner},{kind}))\n'.format(
                    this=this, owner=owner, kind=w
                )
        code += '{this}.Realize()\n'.format(this=this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
