import wx
from ._SizerControl import SizerControl
from beatle.model import cc
from ....lib import popup_errors_to_user


class GridSizerControl(SizerControl):

    def __init__(self, **kwargs):
        self._rows = 1
        self._columns = 1
        self._vgap = 0
        self._hgap = 0
        super(GridSizerControl, self).__init__(**kwargs)

    @property
    def local_gridsizercontrol_kwargs(self):
        return {
            'rows': self._rows,
            'columns': self._columns,
            'vgap': self._vgap,
            'hgap': self._hgap,
        }

    @local_gridsizercontrol_kwargs.setter
    def local_gridsizercontrol_kwargs(self, kwargs):
        self._rows = kwargs.get('rows', self._rows)
        self._columns = kwargs.get('columns', self._columns)
        self._vgap = kwargs.get('vgap', self._vgap)
        self._hgap = kwargs.get('hgap', self._hgap)

    @property
    def kwargs(self):
        value = self.local_gridsizercontrol_kwargs
        value.update(super(GridSizerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_gridsizercontrol_kwargs = kwargs
        if self._edition_window:
            if 'rows' in kwargs:
                self._edition_window.SetRows(kwargs['rows'])
            if 'columns' in kwargs:
                self._edition_window.SetCols(kwargs['columns'])
            if 'hgap' in kwargs:
                self._edition_window.SetHGap(kwargs['hgap'])
            if 'vgap' in kwargs:
                self._edition_window.SetVGap(kwargs['vgap'])
            if 'vgap' in kwargs:
                self._edition_window.SetVGap(kwargs['vgap'])
        super(GridSizerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('grid_sizer')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Grid sizer',
            'type': 'category',
            'help': 'Attributes of grid sizer',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The grid sizer name."
                },
                {
                    'label': 'rows',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._rows,
                    'name': 'rows',  # kwarg value
                    'help': 'rows in the grid.'
                },
                {
                    'label': 'columns',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._columns,
                    'name': 'columns',  # kwarg value
                    'help': 'columns in the grid.'
                },
                {
                    'label': 'vgap',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._vgap,
                    'name': 'vgap',  # kwarg value
                    'help': 'vertical gap between cells in pixels.'
                },
                {
                    'label': 'hgap',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._hgap,
                    'name': 'hgap',  # kwarg value
                    'help': 'horizontal gap between cells in pixels.'
                }],
        },]
        _event = []
        parent_property, parent_event = super(GridSizerControl, self).model_attributes
        _property += parent_property
        return _property, _event

    @popup_errors_to_user
    def create_edition_window(self, parent):
        container = parent.edition_window_client
        if container is None:
            return
        self._edition_window = wx.GridSizer(self._rows, self._columns, self._vgap, self._hgap)
        if container is None:
            return  # weird!!!
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self.add_to_sizer(container, self._edition_window)

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxGridSizer', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/sizer.h>')
        code = "{name} = new wxGridSizer({r}, {c}, {v}, {h});\n" .format(
            name=self._name, r=self._rows, c=self._columns, v=self._vgap, h=self._hgap)
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child."""
        this = 'self.{name}'.format(name=self.name)
        code = '{this} = wx.GridSizer({r}, {c}, {v}, {h})\n'.format(
            this=this, r=self._rows, c=self._columns, v=self._vgap, h=self._hgap)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
