import wx
from .._WindowControl import WindowControl
from beatle.lib.decorators import upgrade_version
from beatle.model import cc, py
from ....lib import register_cpp_class


class Button(WindowControl):
    """"This class represents a button"""

    def __init__(self, **kwargs):
        self._style = 0
        self._label = 'Press me'
        self._default = False
        # events
        self._on_button_click = ''
        super(Button, self).__init__(**kwargs)

    @property
    def local_buttoncontrol_kwargs(self):
        return {
            'style': self._style,
            'label': self._label,
            'default': self._default,
            'on_button_click': self._on_button_click
        }

    @local_buttoncontrol_kwargs.setter
    def local_buttoncontrol_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._label = kwargs.get('label', self._label)
        self._default = kwargs.get('default', self._default)
        self._on_button_click = kwargs.get('on_button_click', self._on_button_click)

    @property
    def local_check_box_events(self):
        return {
            'on_button_click': (self._on_button_click, 'wx.EVT_BUTTON', 'wxEVT_BUTTON'),
        }

    @property
    def events(self):
        value = self.local_check_box_events
        value.update(super(Button, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_buttoncontrol_kwargs
        value.update(super(Button, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_buttoncontrol_kwargs = kwargs
        super(Button, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('button')

    @property
    def style_attribute_options(self):
        """As the button is reused for bitmap button, we need to update the
        style attribute in case the button is a bitmap button one."""
        from ._BitmapButton import BitmapButton
        if type(self) is BitmapButton:
            first_option = ('wxBU_AUTODRAW', 'If this is specified, the button will be drawn automatically using the label bitmap only, providing a 3D-look border. If this style is not specified, the button will be drawn without borders and using all provided bitmaps. WIN32 only.')
        else:
            first_option = ('wxBU_EXACTFIT', 'Make the button as small as possible.')
        return [
            first_option,
            ('wxBU_LEFT', 'Align the label to the left of the button.'),
            ('wxBU_RIGHT', 'Align the label to the right of the button.'),
            ('wxBU_TOP', 'Align the label to the top of the button.'),
            ('wxBU_BOTTOM', 'Align the label to the bottom of the button.'),
            ('wxBU_NOTEXT', 'Disables the display of the text label.')
        ]

    @property
    def style_list_py_text(self):
        """This method is an exception because the styles
        may depend of if this is a button or a bitmap button.
        The rest of implementations are a class-level tuple"""
        from ._BitmapButton import BitmapButton
        if type(self) is BitmapButton:
            first_option = 'wx.BU_AUTODRAW'
        else:
            first_option = 'wx.BU_EXACTFIT'
        return first_option, 'wx.BU_LEFT', 'wx.BU_RIGHT', 'wx.BU_TOP', 'wx.BU_BOTTOM', 'wx.BU_NOTEXT'

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Button',
            'type': 'category',
            'help': 'Attributes of button.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The button instance name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'Text displayed in the button.',
                },
                {
                    'label': 'default',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._default,
                    'name': 'default',  # kwarg value
                    'help': 'Make the button the default.',
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets button style.'
                }
            ],
        },]
        _event = [{
            'label': 'Button',
            'type': 'category',
            'help': 'Events of button control',
            'child': [{
                'label': 'on click',
                'type': 'string',
                'read_only': False,
                'value': self._on_button_click,
                'name': 'on_button_click',  # kwarg value
                'help': 'Called when the button is clicked.'
                },],
            }]
        _base_property, _base_event = super(Button, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def style_value(self):
        """Return the value required for dynamic button creation"""
        from ._BitmapButton import BitmapButton
        if type(self) is BitmapButton:
            first_option = wx.BU_AUTODRAW
        else:
            first_option = wx.BU_EXACTFIT
        style_list = (first_option, wx.BU_LEFT, wx.BU_RIGHT, wx.BU_TOP, wx.BU_BOTTOM, wx.BU_NOTEXT)
        self_style_value = self.style_value_helper(style_list, self._style)
        return self_style_value | super(Button, self).style_value

    @property
    def py_style_str(self):
        if self._style == 0:
            return super(Button, self).py_style_str
        style_list_py_text = self.style_list_py_text
        sz = len(style_list_py_text)
        control_style_value = '|'.join(
            style_list_py_text[index] for index in range(sz)
            if self._style & (2 ** index)
        )
        base = super(Button, self).py_style_str
        if base == '0':
            return control_style_value
        return '|'.join((control_style_value, base))

    @property
    def cc_style_str(self):
        """Return a string suitable for the style declaration"""
        if self._style == 0:
            return super(Button, self).cc_style_str
        style_list_cc_text = self.style_list_cc_text
        sz = len(style_list_cc_text)
        control_style_value = '|'.join(
            style_list_cc_text[index] for index in range(sz)
            if self._style & (2 ** index)
        )
        base = super(Button, self).cc_style_str
        if base == '0':
            return control_style_value
        return '|'.join((control_style_value, base))

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.Button)(
                edition_frame, self.py_id, self._label, self.py_pos, self.py_size, self.style_value)
            if self._default:
                self._edition_window.SetDefault()
            self.add_to_sizer(container, self._edition_window)
        except Exception as exception:
            full_message = str(exception)
            index = full_message.index(':')
            message = full_message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise ValueError('')
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/button.h>
        cls = register_cpp_class(self.project, 'wxButton')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="button control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        self.add_cpp_required_header('#include <wx/button.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxButton({owner}, {self._id}, "{self._label}", {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        code += super(Button, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.Button({owner}, {self.py_id_str}, "{self._label}", {self.py_pos_str},' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        if self._default:
            code += f'{this}.SetDefault()\n'
        code += super(Button, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

    @upgrade_version
    def __setstate__(self, d):
        """Add the creation field if missing"""
        return {
            'add': {'_default': False},
        }
