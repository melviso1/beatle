import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class TextControl(WindowControl):
    """"This class represents a button"""

    style_list = (wx.TE_PROCESS_ENTER, wx.TE_PROCESS_TAB, wx.TE_MULTILINE, wx.TE_PASSWORD, wx.TE_READONLY, wx.TE_RICH,
                  wx.TE_RICH2, wx.TE_AUTO_URL, wx.TE_NOHIDESEL, wx.HSCROLL, wx.TE_NO_VSCROLL, wx.TE_LEFT, wx.TE_CENTRE,
                  wx.TE_RIGHT, wx.TE_DONTWRAP, wx.TE_CHARWRAP, wx.TE_WORDWRAP,wx.TE_BESTWRAP)

    style_list_py_text = ('wx.TE_PROCESS_ENTER', 'wx.TE_PROCESS_TAB', 'wx.TE_MULTILINE', 'wx.TE_PASSWORD',
                          'wx.TE_READONLY', 'wx.TE_RICH', 'wx.TE_RICH2', 'wx.TE_AUTO_URL', 'wx.TE_NOHIDESEL',
                          'wx.HSCROLL', 'wx.TE_NO_VSCROLL', 'wx.TE_LEFT', 'wx.TE_CENTRE', 'wx.TE_RIGHT',
                          'wx.TE_DONTWRAP', 'wx.TE_CHARWRAP', 'wx.TE_WORDWRAP', 'wx.TE_BESTWRAP')

    style_list_cc_text = ('wxTE_PROCESS_ENTER', 'wxTE_PROCESS_TAB', 'wxTE_MULTILINE', 'wxTE_PASSWORD',
                          'wxTE_READONLY', 'wxTE_RICH', 'wxTE_RICH2', 'wxTE_AUTO_URL', 'wxTE_NOHIDESEL',
                          'wxHSCROLL', 'wxTE_NO_VSCROLL', 'wxTE_LEFT', 'wxTE_CENTRE', 'wxTE_RIGHT',
                          'wxTE_DONTWRAP', 'wxTE_CHARWRAP', 'wxTE_WORDWRAP', 'wxTE_BESTWRAP')

    def __init__(self, **kwargs):
        self._style = 0
        self._text = 'text'
        self._max_length = 0
        # events
        self._on_text = ''
        self._on_text_enter = ''
        self._on_text_url = ''
        self._on_text_max_length = ''
        super(TextControl, self).__init__(**kwargs)

    @property
    def local_textcontrol_kwargs(self):
        return {
            'style': self._style,
            'text': self._text,
            'max_length': self._max_length,
            # events
            'on_text': self._on_text,
            'on_text_enter': self._on_text_enter,
            'on_text_url': self._on_text_url,
            'on_text_max_length': self._on_text_max_length
        }

    @local_textcontrol_kwargs.setter
    def local_textcontrol_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._text = kwargs.get('text', self._text)
        self._max_length = kwargs.get('max_length', self._max_length)
        # events
        self._on_text = kwargs.get('on_text', self._on_text)
        self._on_text_enter = kwargs.get('on_text_enter', self._on_text_enter)
        self._on_text_url = kwargs.get('on_text_url', self._on_text_url)
        self._on_text_max_length = kwargs.get('on_text_max_length', self._on_text_max_length)

    @property
    def local_textcontrol_events(self):
        return {
            'on_text': (self._on_text, 'wx.EVT_TEXT', 'wxEVT_TEXT'),
            'on_text_enter': (self._on_text_enter, 'wx.EVT_TEXT_ENTER', 'wxEVT_TEXT_ENTER'),
            'on_text_url': (self._on_text_url, 'wx.EVT_TEXT_URL', 'wxEVT_TEXT_URL'),
            'on_text_max_length': (self._on_text_max_length, 'wx.EVT_TEXT_MAXLEN', 'wxEVT_TEXT_MAXLEN'),
        }

    @property
    def events(self):
        value = self.local_textcontrol_events
        value.update(super(TextControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_textcontrol_kwargs
        value.update(super(TextControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_textcontrol_kwargs = kwargs
        super(TextControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('text_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxTE_PROCESS_ENTER',
             "The control will generate the event wxEVT_TEXT_ENTER. Take in account that if this "
             "event is handled and not performing default behavior, could break that default button, "
             "if any, receives the enter key."),
            ('wxTE_PROCESS_TAB',
             "Capture the TAB as text, but breaks tab-navigation over controls. Under wxGTK for single "
             "line text control, this style has not effect."),
            ('wxTE_MULTILINE',
             "The text control allows multiple lines.If this style is not specified, line break characters "
             "should not be used in the controls value."),
            ('wxTE_PASSWORD',
             "The text will be shown as asterisks."),
            ('wxTE_READONLY',
             "The text will not be editable."),
            ('wxTE_RICH',
             "Use rich text control under MSW, this allows having more than 64 KB of text in the control. "
             "This style is ignored under other platforms and it is recommended to use wx.TE_RICH2 instead "
             "of it under MSW."),
            ('wxTE_RICH2',
             "Use rich text control version 2.0 or higher under MSW, this style is ignored under other "
             "platforms. Note that this style may be turned on automatically even if it is not used "
             "explicitly when creating a text control with a long (i.e.much more than 64KiB) initial text, "
             "as creating the control would simply fail in this case under MSW if neither this style nor "
             "wxTE_RICH is used."),
            ('wxTE_AUTO_URL',
             "Highlight the URLs and generate the TextUrlEvents when mouse events occur over them."),
            ('wxTE_NOHIDESEL',
             "By default, the Windows text control doesn’t show the selection when it doesn’t have focus "
             "- use this style to force it to always show it. It doesn’t do anything under other platforms."),
            ('wxHSCROLL',
             "A horizontal scrollbar will be created and used, so that text won’t be wrapped."),
            ('wxTE_NO_VSCROLL',
             "For multiline controls only: vertical scrollbar will never be created. This limits the amount"
             "of text which can be entered into the control to what can be displayed in it under wxMSW but "
             "not under wxGTK or wxOSX. Currently not implemented for the other platforms."),
            ('wxTE_LEFT',
             "The text in the control will be left - justified(default)."),
            ('wxTE_CENTRE',
             "The text in the control will be centered(wxMSW, wxGTK, wxOSX)."),
            ('wxTE_RIGHT',
             "The text in the control will be right - justified(wxMSW, wxGTK, wxOSX)."),
            ('wxTE_DONTWRAP',
             "Same as wxHSCROLL style: don’t wrap at all, show horizontal scrollbar instead."),
            ('wxTE_CHARWRAP',
             "For multiline controls only: wrap the lines too long to be shown entirely at any "
             "position (wxUniv, wxGTK, wxOSX)."),
            ('wxTE_WORDWRAP',
             "For multiline controls only: wrap the lines too long to be shown entirely at word boundaries "
             "(wxUniv, wxMSW, wxGTK, wxOSX)."),
            ('wxTE_BESTWRAP',
             "For multiline controls only: wrap the lines at word boundaries or at any other character if there "
             "are words longer than the window width (this is the default)."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Text',
            'type': 'category',
            'help': 'Attributes of text.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The control name."
                },
                {
                    'label': 'text',
                    'type': 'string',
                    'read_only': False,
                    'value': self._text,
                    'name': 'text',  # kwarg value
                    'help': "The initial text."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'The text control style.'
                },
                {
                    'label': 'max length',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._max_length,
                    'name': 'max_length',  # kwarg value
                    'help': "Maximum number of characters the user can enter into the control. Value 0 "
                            "means use the underlying native text control widget supports limit "
                            "(typically at least 32Kb). A wxEVT_TEXT_MAXLEN event is sent to notify the program "
                            "when the user attempts to enter more text than possible (extra input is discarded). "
                            "Under wxGTK this only be used with single line text controls.",
                }]
        }]
        _event = [{
            'label': 'TextControl',
            'type': 'category',
            'help': 'Events of text control',
            'child': [
                {
                    'label': 'on text',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text,
                    'name': 'on_text',  # kwarg value
                    'help': 'Called when the text changes'
                },
                {
                    'label': 'on enter',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text_enter,
                    'name': 'on_text_enter',  # kwarg value
                    'help': 'Called when the user press enter (requires wxTE_PROCESS_ENTER style).'
                },
                {
                    'label': 'on url',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text_url,
                    'name': 'on_text_url',  # kwarg value
                    'help': 'Called when mouse acts over url text (wxGTK and wxMSW).'
                },
                {
                    'label': 'on max length',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text_max_length,
                    'name': 'on_text_max_length',  # kwarg value
                    'help': 'Called when user attempts to introduce more text than specified as max length.'
                },
            ]
        }]
        _base_property, _base_event = super(TextControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.TextCtrl)(
            edition_frame, self.py_id, self._text, self.py_pos, self.py_size, self.style_value)
        if self._max_length != 0:
            self._edition_window.SetMaxLength(self._max_length)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/textctrl.h>
        cls = register_cpp_class(self.project, 'wxTextCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="text control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/textctrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f'{self._name} = new wxTextCtrl({owner}, {self.py_id}, "{self._text}", {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        if self._max_length != 0:
            code += f'{self._name}->SetMaxLength({self._max_length});\n'

        code += super(TextControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.TextCtrl({owner}, {self.py_id_str}, "{self._text}", {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        if self._max_length != 0:
            code += f'{this}.SetMaxLength({self._max_length})\n'
        code += super(TextControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code



