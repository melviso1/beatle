import wx
from .._WindowControl import WindowControl
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class CheckBoxControl(WindowControl):
    """"This class represents a button"""

    style_list = (wx.CHK_2STATE, wx.CHK_3STATE, wx.CHK_ALLOW_3RD_STATE_FOR_USER, wx.ALIGN_RIGHT)
    style_list_py_text = ('wx.CHK_2STATE', 'wx.CHK_3STATE', 'wx.CHK_ALLOW_3RD_STATE_FOR_USER', 'wx.ALIGN_RIGHT')
    style_list_cc_text = ('wxCHK_2STATE', 'wxCHK_3STATE', 'wxCHK_ALLOW_3RD_STATE_FOR_USER', 'wxALIGN_RIGHT')

    def __init__(self, **kwargs):
        self._label = 'label'
        self._style = 1
        self._checked = False
        self._on_check_box = ''
        super(CheckBoxControl, self).__init__(**kwargs)

    @property
    def local_checkbox_kwargs(self):
        return {
            'label': self._label,
            'style': self._style,
            'checked': self._checked,
            # events
            'on_check_box': self._on_check_box
        }

    @local_checkbox_kwargs.setter
    def local_checkbox_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        prev_style = self._style
        self._style = kwargs.get('style', self._style)
        self._checked = kwargs.get('checked', self._checked)
        self._on_check_box = kwargs.get('on_check_box', self._on_check_box)
        # fix logical combinations
        if prev_style != self._style:
            if prev_style & 1:  # 2-state checkbox
                if self._style & 2: # 3-state
                    self._style ^= 1
                elif self._style & 4:
                    self._style ^= 1
                    self._style |= 2
                else:
                    self._style |= 1
            elif self._style & 1:
                if self._style & 2:
                    self._style ^= 2
                if self._style & 4:
                    self._style ^= 4
            elif self._style & 4:
                if not(self._style & 2):
                    self._style ^= 2
            elif not(self._style & 2):
                self._style |= 1

    @property
    def local_check_box_events(self):
        return {
            'on_check_box': (self._on_check_box, 'wx.EVT_CHECKBOX', 'wxEVT_CHECKBOX'),
        }

    @property
    def events(self):
        value = self.local_check_box_events
        value.update(super(CheckBoxControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_checkbox_kwargs
        value.update(super(CheckBoxControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_checkbox_kwargs = kwargs
        super(CheckBoxControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('check_box')

    @property
    def style_attribute_options(self):
        return [
            ('wxCHK_2STATE', "Create a 2-state checkbox. This is the default."),
            ('wxCHK_3STATE', 'Create a 3-state checkbox.'),
            ('wxCHK_ALLOW_3RD_STATE_FOR_USER', "By default a user can’t set a 3-state checkbox to the third state. "
                                               "It can only be done from code. Using this flags allows the user to set "
                                               "the checkbox to the third state by clicking."),
            ('wxALIGN_RIGHT', 'Makes the text appear on the left of the checkbox.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Check box',
            'type': 'category',
            'help': 'Attributes of check box',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The checkbox control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the checkbox style.'
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'Text displayed in the checkbox.',
                },
                {
                    'label': 'checked',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._checked,
                    'name': 'checked',  # kwarg value
                    'help': 'Initial status of the checkbox.',
                },]
        }]
        _event = [{
            'label': 'CheckBox',
            'type': 'category',
            'help': 'Events of checkbox control',
            'child': [{
                'label': 'on checkbox',
                'type': 'string',
                'read_only': False,
                'value': self._on_check_box,
                'name': 'on_check_box',  # kwarg value
                'help': 'Called when the checkbox is clicked.'
                },],
            }]
        _base_property, _base_event = super(CheckBoxControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def style_value(self):
        """Return the value required for dynamic button creation"""
        self_style_value = self.style_value_helper(CheckBoxControl.style_list, self._style)
        return self_style_value | super(CheckBoxControl, self).style_value

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.CheckBox)(
            edition_frame, self.py_id, self._label, self.py_pos, self.py_size, self.style_value)
        self._edition_window.SetValue(self._checked)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/checkbox.h>
        cls = register_cpp_class(self.project, 'wxCheckBox')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="checkbox control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    @property
    def py_style_str(self):
        if self._style == 0:
            return super(CheckBoxControl, self).py_style_str
        sz = len(CheckBoxControl.style_list_py_text)
        control_style_value = '|'.join(
            CheckBoxControl.style_list_py_text[index] for index in range(sz)
            if self._style & (2 ** index)
        )
        base = super(CheckBoxControl, self).py_style_str
        if base == '0':
            return control_style_value
        return '|'.join((control_style_value, base))

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/checkbox.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f'{self._name} = new wxCheckBox({owner}, {self._id}, "{self._label}", {self.cc_pos_str},' \
               f' {self.cc_size_str}, {self.cc_style_str});\n'
        value = (self._checked and 'true') or 'false'
        code += f'{self._name}->SetValue({value});\n'
        code += super(CheckBoxControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'

        code = f'{this} = wx.CheckBox({owner}, {self.py_id_str}, "{self._label}", {self.py_pos_str},' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        value = (self._checked and 'True') or 'False'
        code += f'{this}.SetValue({value})\n'
        code += super(CheckBoxControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

