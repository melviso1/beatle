import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class ChoiceControl(WindowControl):
    """"This class represents a choice control"""
    style_list = (wx.CB_SORT, )
    style_list_py_text = ('wx.CB_SORT',)
    style_list_cc_text = ('wxCB_SORT',)

    def __init__(self, **kwargs):
        self._choices = []
        self._selection = -1
        self._style = 0
        # events
        self._on_select = ''
        super(ChoiceControl, self).__init__(**kwargs)

    @property
    def local_choicecontrol_kwargs(self):
        return {
            'choices': self._choices,
            'selection': self._selection,
            'style': self._style,
            # events
            'on_select': self._on_select,
        }

    @local_choicecontrol_kwargs.setter
    def local_choicecontrol_kwargs(self, kwargs):
        self._choices = kwargs.get('choices', self._choices)
        self._selection = kwargs.get('selection', self._selection)
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_select = kwargs.get('on_select', self._on_select)

    @property
    def local_choice_events(self):
        return {
            'on_select': (self._on_select, 'wx.EVT_CHOICE', 'wxEVT_CHOICE'),
        }

    @property
    def events(self):
        value = self.local_choice_events
        value.update(super(ChoiceControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_choicecontrol_kwargs
        value.update(super(ChoiceControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_choicecontrol_kwargs = kwargs
        super(ChoiceControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('choice_box')

    @property
    def style_attribute_options(self):
        return [('wxCB_SORT', 'Sort the list alphabetically.'),]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Choice',
            'type': 'category',
            'help': 'Attributes of choice.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The choice control name."
                },
                {
                    'label': 'choices',
                    'type': 'string_array',
                    'read_only': False,
                    'value': self._choices,
                    'name': 'choices',  # kwarg value
                    'help': 'Contents of the combobox.',
                },
                {
                    'label': 'selection',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._selection,
                    'name': 'selection',  # kwarg value
                    'help': 'Index of initial selection.',
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets combobox style.'
                }]
        }]
        _event = [{
            'label': 'ChoiceControl',
            'type': 'category',
            'help': 'Events of choice control',
            'child': [
                {
                    'label': 'on select',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_select,
                    'name': 'on_select',  # kwarg value
                    'help': 'Called when an item in the list is selected.'
                },
            ],
        }]
        _base_property, _base_event = super(ChoiceControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!

        self._edition_window = uiMockupControl(wx.Choice)(
            edition_frame, self.py_id, self.py_pos, self.py_size, self._choices, self.style_value)

        self.add_to_sizer(container, self._edition_window)
        wx.YieldIfNeeded()
        if self._selection > -1:
            self._edition_window.SetSelection(int(self._selection))
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/choice.h>
        cls = register_cpp_class(self.project, 'wxChoice')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="choice control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/choice.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        n = len(self._choices)
        code = f'wxString {self._name}_choices[] = {{' + ','.join(f'"{text}"' for text in self._choices)+'};\n'
        code += f'{self._name} = new wxChoice({owner}, {self._id}, {self.cc_pos_str}, {self.cc_size_str}, ' \
                f'{n}, {self._name}_choices, {self.cc_style_str});\n'
        if self._selection in range(len(self._choices)):
            code += f'{self._name}->SetSelection({self._selection});\n'
        code += super(ChoiceControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = 'self.{name}'.format(name=window_parent.name)

        code = '{name}_choices = ['.format(name=self._name) + ','.join(
            '"{text}"'.format(text=text) for text in self._choices)+']\n'
        code += '{this} = wx.Choice({owner}, {id_}, {pos}, {size}, {name}_choices, {style})\n'.\
            format(this=this, owner=owner, id_=self.py_id_str, name=self._name,
                   pos=self.py_pos_str, size=self.py_size_str,
                   style=self.py_style_str)
        if self._selection in range(len(self._choices)):
            code += '{this}.SetSelection({selection})\n'.format(this=this, selection=self._selection)
        code += super(ChoiceControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

