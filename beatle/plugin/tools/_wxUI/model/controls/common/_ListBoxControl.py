import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class ListBoxControl(WindowControl):
    """"This class represents a listbox"""

    style_list = (wx.LB_SINGLE, wx.LB_MULTIPLE, wx.LB_EXTENDED, wx.LB_HSCROLL,
                  wx.LB_ALWAYS_SB, wx.LB_NEEDED_SB, wx.LB_NO_SB, wx.LB_SORT)

    style_list_py_text = ('wx.LB_SINGLE', 'wx.LB_MULTIPLE', 'wx.LB_EXTENDED', 'wx.LB_HSCROLL',
                          'wx.LB_ALWAYS_SB', 'wx.LB_NEEDED_SB', 'wx.LB_NO_SB', 'wx.LB_SORT')

    style_list_cc_text = ('wxLB_SINGLE', 'wxLB_MULTIPLE', 'wxLB_EXTENDED', 'wxLB_HSCROLL',
                          'wxLB_ALWAYS_SB', 'wxLB_NEEDED_SB', 'wxLB_NO_SB', 'wxLB_SORT')

    def __init__(self, **kwargs):
        self._choices = []
        self._style = 0
        # events
        self._on_select = ''
        self._on_double_click = ''
        super(ListBoxControl, self).__init__(**kwargs)

    @property
    def local_listboxctrl_kwargs(self):
        return {
            'choices': self._choices,
            'style': self._style,
            # events
            'on_select': self._on_select,
            'on_double_click': self._on_double_click,
        }

    @local_listboxctrl_kwargs.setter
    def local_listboxctrl_kwargs(self, kwargs):
        self._choices = kwargs.get('choices', self._choices)
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_select = kwargs.get('on_select', self._on_select)
        self._on_double_click = kwargs.get('on_double_click', self._on_double_click)

    @property
    def local_list_box_events(self):
        return {
            'on_select': (self._on_select, 'wx.EVT_LISTBOX', 'wxEVT_LISTBOX'),
            'on_double_click': (self._on_double_click, 'wx.EVT_LISTBOX_DCLICK', 'wxEVT_LISTBOX_DCLICK'),
        }

    @property
    def events(self):
        value = self.local_list_box_events
        value.update(super(ListBoxControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_listboxctrl_kwargs
        value.update(super(ListBoxControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_listboxctrl_kwargs = kwargs
        super(ListBoxControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('list_box')

    @property
    def style_attribute_options(self):
        return [
            ('wxLB_SINGLE', "Single - selection list."),
            ('wxLB_MULTIPLE', "Multiple - selection list: the user can toggle multiple items on and off. "
                              "This is the same as wx.LB_EXTENDED in wxGTK2 port."),
            ('wxLB_EXTENDED', "Extended - selection list: the user can extend the selection by using SHIFT or "
                              "CTRL keys together with the cursor movement keys or the mouse."),
            ('wxLB_HSCROLL', "Create horizontal scrollbar if contents are too wide ( Windows only )."),
            ('wxLB_ALWAYS_SB', "Always show a vertical scrollbar."),
            ('wxLB_NEEDED_SB', "Only create a vertical scrollbar if needed."),
            ('wxLB_NO_SB', "Don’t create vertical scrollbar (wxMSW and wxGTK only)."),
            ('wxLB_SORT', "The listbox contents are sorted in alphabetical order.")
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'List Box',
            'type': 'category',
            'help': 'Attributes of list box.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The listbox control name."
                },
                {
                    'label': 'choices',
                    'type': 'string_array',
                    'read_only': False,
                    'value': self._choices,
                    'name': 'choices',  # kwarg value
                    'help': 'Contents of the listbox.',
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets listbox style.'
                }]
        }]
        _event = [{
            'label': 'ListBoxControl',
            'type': 'category',
            'help': 'Events of listbox control',
            'child': [
                {
                    'label': 'on select',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_select,
                    'name': 'on_select',  # kwarg value
                    'help': 'Called when an item in the list is selected.'
                },
                {
                    'label': 'on double click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_double_click,
                    'name': 'on_double_click',  # kwarg value
                    'help': 'Called when double click on the listbox.'
                },
            ],
        }]

        _base_property, _base_event = super(ListBoxControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.ListBox)(
            edition_frame, self.py_id, self.py_pos, self.py_size, self._choices, self.style_value)

        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/listbox.h>
        class_ = register_cpp_class(self.project, 'wxListBox')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="listbox control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/listbox.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'wxString {self._name}_choices[] = {{' + ','.join(f'"{text}"' for text in self._choices)+'};\n'
        count = len(self._choices)
        code += f'{self._name} = new wxListBox({owner}, {self._id}, {self.cc_pos_str}, {self.cc_size_str}, ' \
                f'{count}, {self._name}_choices, {self.cc_style_str});\n'
        code += super(ListBoxControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'
        choices = '[' + ', '.join(f'"{x}"' for x in self._choices)+']'
        code = f'{this} = wx.ListBox({owner}, {self.py_id_str}, {self.py_pos_str}, {self.py_size_str}, ' \
               f'{choices},  {self.py_style_str})\n'
        code += super(ListBoxControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

