import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class RadioBoxControl(WindowControl):
    """"This class represents a combobox"""

    style_list = (wx.RA_SPECIFY_ROWS, wx.RA_SPECIFY_COLS)
    style_list_py_text = ('wx.RA_SPECIFY_ROWS', 'wx.RA_SPECIFY_COLS')
    style_list_cc_text = ('wxRA_SPECIFY_ROWS', 'wxRA_SPECIFY_COLS')

    def __init__(self, **kwargs):
        self._label = ''
        self._choices = []
        self._selection = 0
        self._major_dimension = 1
        self._style = 1
        # events
        self._on_radio_box = ''
        super(RadioBoxControl, self).__init__(**kwargs)

    @property
    def local_radiobox_kwargs(self):
        return {
            'label': self._label,
            'choices': self._choices,
            'selection': self._selection,
            'major_dimension': self._major_dimension,
            'style': self._style,
            # events
            'on_radio_box': self._on_radio_box,
        }

    @local_radiobox_kwargs.setter
    def local_radiobox_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._choices = kwargs.get('choices', self._choices)
        self._selection = kwargs.get('selection', self._selection)
        self._major_dimension = kwargs.get('major_dimension', self._major_dimension)
        prev_style = self._style
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_radio_box = kwargs.get('on_radio_box', self._on_radio_box)
        if prev_style != self._style:
            if prev_style == 1:
                self._style = 2
            else:
                self._style = 1

    @property
    def local_radiobutton_events(self):
        return {
            'on_radio_box': (self._on_radio_box, 'wx.EVT_RADIOBOX', 'wxEVT_RADIOBOX'),
        }

    @property
    def events(self):
        value = self.local_radiobutton_events
        value.update(super(RadioBoxControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_radiobox_kwargs
        value.update(super(RadioBoxControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_radiobox_kwargs = kwargs
        super(RadioBoxControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('radio_box')

    @property
    def style_attribute_options(self):
        return [
            ('wxRA_SPECIFY_ROWS', 'The major dimension parameter refers to the maximum number of rows.'),
            ('wxRA_SPECIFY_COLS', 'The major dimension parameter refers to the maximum number of columns.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Radio box',
            'type': 'category',
            'help': 'Attributes of radio box.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The combobox control name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'The label shown in the outer box.',
                },
                {
                    'label': 'choices',
                    'type': 'string_array',
                    'read_only': False,
                    'value': self._choices,
                    'name': 'choices',  # kwarg value
                    'help': 'Contents of the combobox.',
                },
                {
                    'label': 'selection',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._selection,
                    'name': 'selection',  # kwarg value
                    'help': 'Index selected button.',
                },
                {
                    'label': 'major dimension',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._major_dimension,
                    'name': 'major_dimension',  # kwarg value
                    'help': 'Specify the maximun number of elements in the dimension selected by style.',
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets combobox style.'
                }]
        }]
        _event = [{
            'label': 'RadioBoxControl',
            'type': 'category',
            'help': 'Events of combobox control',
            'child': [
                {
                    'label': 'on radio box',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_radio_box,
                    'name': 'on_radio_box',  # kwarg value
                    'help': 'Called when an item in the radio box is selected.'
                },
            ],
        }]

        _base_property, _base_event = super(RadioBoxControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!

        self._edition_window = uiMockupControl(wx.RadioBox)(
            edition_frame, self.py_id, self._label, self.py_pos, self.py_size, self._choices,
            self._major_dimension, self.style_value)
        if -1 < self._selection < len(self._choices):
            self._edition_window.SetSelection(self._selection)

        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/radiobox.h>
        cls = register_cpp_class(self.project, 'wxRadioBox')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="radio box control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/radiobox.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        n = len(self._choices)
        code = f"wxString {self._name}_choices[] = {{" + ', '.join(f'"{option}"' for option in self._choices) + "};\n"
        code += f'{self._name} = new wxRadioBox({owner}, {self._id}, "{self._label}", {self.cc_pos_str}, ' \
                f'{self.cc_size_str}, {n}, {self._name}_choices, {self._major_dimension}, {self.cc_style_str});\n'
        code += f'{self._name}->SetValue({self._value});\n'
        code += super(RadioBoxControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f"{self._name}_choices = [" + ', '.join(f'"{option}"' for option in self._choices) + "]\n"

        code += f'{this} = wx.RadioBox({owner}, {self.py_id_str}, "{self._label}", {self.py_pos_str}, ' \
                f'{self.py_size_str}, {self._name}_choices, {self._major_dimension}, {self.py_style_str})\n'

        code += super(RadioBoxControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code
