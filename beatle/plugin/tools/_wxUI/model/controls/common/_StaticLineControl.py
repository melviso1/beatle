import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class StaticLineControl(WindowControl):
    """"This class represents a button"""

    style_list = (wx.LI_HORIZONTAL, wx.LI_VERTICAL)
    style_list_py_text = ('wx.LI_HORIZONTAL', 'wx.LI_VERTICAL')
    style_list_cc_text = ('wxLI_HORIZONTAL', 'wxLI_VERTICAL')

    def __init__(self, **kwargs):
        self._label = 'label'
        self._style = 0
        self._value = False
        self._on_radio_button = ''
        super(StaticLineControl, self).__init__(**kwargs)

    @property
    def local_staticline_kwargs(self):
        return {
            'style': self._style,
        }

    @local_staticline_kwargs.setter
    def local_staticline_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)

    @property
    def kwargs(self):
        value = self.local_staticline_kwargs
        value.update(super(StaticLineControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_staticline_kwargs = kwargs
        super(StaticLineControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('static_line')

    @property
    def style_attribute_options(self):
        return [
            ('wxLI_HORIZONTAL', "Create a horizontal line."),
            ('wxLI_VERTICAL', 'Create a vertical line.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Static line',
            'type': 'category',
            'help': 'Attributes of static line',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The radiobutton control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the radiobutton style.'
                }]
        }]
        _event = []
        _base_property, _base_event = super(StaticLineControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.StaticLine)(
            edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/statline.h>
        cls = register_cpp_class(self.project, 'wxStaticLine')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="static line control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/statline.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f"{self._name} = new wxStaticLine({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"
        code += super(StaticLineControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.StaticLine({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(StaticLineControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

