import wx
import wx.adv
from .._WindowControl import WindowControl
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class AnimationControl(WindowControl):
    """"This class represents a animation control"""
    style_list = (wx.adv.AC_DEFAULT_STYLE, wx.adv.AC_NO_AUTORESIZE)
    style_list_py_text = ('wx.adv.AC_DEFAULT_STYLE', 'wx.adv.AC_NO_AUTORESIZE')
    style_list_cc_text = ('wxAC_DEFAULT_STYLE', 'wxAC_NO_AUTORESIZE')

    def __init__(self, **kwargs):
        self._style = 1
        self._animation = ''
        super(AnimationControl, self).__init__(**kwargs)

    @property
    def local_animationcontrol_kwargs(self):
        return {
            'style': self._style,
            'animation': self._animation
        }

    @local_animationcontrol_kwargs.setter
    def local_animationcontrol_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._animation = kwargs.get('animation', self._animation)

    @property
    def kwargs(self):
        value = self.local_animationcontrol_kwargs
        value.update(super(AnimationControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_animationcontrol_kwargs = kwargs
        super(AnimationControl, self).set_kwargs(kwargs)


    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('animation_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxAC_DEFAULT_STYLE', 'The default style, that is wxBORDER_NONE.'),
            ('wxAC_NO_AUTORESIZE', "By default, the control will adjust its size to exactly fit to the size of the "
                                   "animation when SetAnimation is called. If this style flag is given, the control "
                                   "will not change its size."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Animation',
            'type': 'category',
            'help': 'Attributes of animation.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The animation control instance name."
                },
                {
                    'label': 'animation',
                    'type': 'animation',
                    'read_only': False,
                    'value': self._animation,
                    'name': 'animation',  # kwarg value
                    'help': 'The animation file to be displayed.',
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets animation control style.'
                }
            ],
        },]
        _event = []
        _base_property, _base_event = super(AnimationControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.adv.AnimationCtrl)(
            edition_frame, self.py_id, wx.adv.NullAnimation, self.py_pos, self.py_size, self.style_value)
        if len(self._animation):
            self._edition_window.LoadFile(self._animation)
            self._edition_window.Play()
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/animate.h>
        class_ = register_cpp_class(self.project, 'wxAnimate')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="animation control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/animate.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f"{self._name} = new wxAnimationCtrl({owner}, {self._id}, wxNullAnimation, " \
               f"{self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n"
        code += super(AnimationControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.adv')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'

        code = f'{this} = wx.adv.AnimationCtrl({owner}, {self.py_id_str}, wx.adv.NullAnimation, ' \
               f'{self.py_pos_str},{self.py_size_str}, {self.py_style_str})\n'
        code += super(AnimationControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

