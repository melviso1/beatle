import wx
from ._Button import Button
from ....lib import BitmapProperty
from beatle.model import cc, py
from ....lib import register_cpp_class, get_bitmap_from_tuple, \
    get_bitmap_py_text_from_tuple, get_bitmap_cpp_text_from_tuple


class BitmapButton(Button):
    """"This class represents a bitmap button"""

    def __init__(self, **kwargs):
        self._bitmap = (0, '')
        self._disabled = (0, '')
        self._selected = (0, '')
        self._focus = (0, '')
        self._hover = (0, '')
        super(BitmapButton, self).__init__(**kwargs)
        # the bitmap button is implemented by the base button

    @property
    def local_bitmapbuttoncontrol_kwargs(self):
        return {
            'bitmap': self._bitmap,
            'disabled': self._disabled,
            'selected': self._selected,
            'focus': self._focus,
            'hover': self._hover,
        }

    @local_bitmapbuttoncontrol_kwargs.setter
    def local_bitmapbuttoncontrol_kwargs(self, kwargs):
        self._bitmap = kwargs.get('bitmap', self._bitmap)
        self._disabled = kwargs.get('disabled', self._disabled)
        self._selected = kwargs.get('selected', self._selected)
        self._focus = kwargs.get('focus', self._focus)
        self._hover = kwargs.get('hover', self._hover)

    @property
    def kwargs(self):
        value = self.local_bitmapbuttoncontrol_kwargs
        value.update(super(BitmapButton, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        """Set the bitmap button arguments.
        This button is generally hard to update so, if editing,
         we directly recreate it."""
        self.local_bitmapbuttoncontrol_kwargs = kwargs
        super(BitmapButton, self).set_kwargs(kwargs)


    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('bitmap_button')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Bitmap button',
            'type': 'category',
            'help': 'Attributes of bitmap button.',
            'child': [
                {
                    'label': 'bitmap',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._bitmap,
                    'name': 'bitmap',  # kwarg value
                    'help': "The base bitmap (required)."
                },
                {
                    'label': 'disabled',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._disabled,
                    'name': 'disabled',  # kwarg value
                    'help': "The bitmap to show when disabled (optional)."
                },
                {
                    'label': 'selected',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._selected,
                    'name': 'selected',  # kwarg value
                    'help': "The bitmap to show when selected (optional)."
                },
                {
                    'label': 'focus',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._focus,
                    'name': 'focus',  # kwarg value
                    'help': "The bitmap to show when focused (optional)."
                },
                {
                    'label': 'hover',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._hover,
                    'name': 'hover',  # kwarg value
                    'help': "The bitmap to show when mose hover (optional)."
                },
            ]
        },]
        _event = []
        _base_property, _base_event = super(BitmapButton, self).model_attributes

        # move the name of the control to top:
        _property[0]['child'].insert(0, _base_property[0]['child'][0])
        del _base_property[0]['child'][0]

        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        bitmap = get_bitmap_from_tuple(self._bitmap) or wx.NullBitmap
        self._edition_window = uiMockupControl(wx.BitmapButton)(
            edition_frame, self.py_id, bitmap,
            self.py_pos, self.py_size, self.style_value)
        bitmap = get_bitmap_from_tuple(self._disabled)
        if bitmap is not None:
            self._edition_window.SetBitmapDisabled(bitmap)
        bitmap = get_bitmap_from_tuple(self._selected)
        if bitmap is not None:
            self._edition_window.SetBitmapPressed(bitmap)
        bitmap = get_bitmap_from_tuple(self._focus)
        if bitmap is not None:
            self._edition_window.SetBitmapFocus(bitmap)
        bitmap = get_bitmap_from_tuple(self._hover)
        if bitmap is not None:
            self._edition_window.SetBitmapCurrent(bitmap)

        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        # requires #include <wx/bmpbuttn.h>
        cls = register_cpp_class(self.project, 'wxBitmapButton')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="bitmap button control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        self.add_cpp_required_header('#include <wx/bmpbuttn.h>')
        self.add_cpp_required_header('#include <wx/artprov.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        bitmap_text = get_bitmap_cpp_text_from_tuple(self._bitmap) or 'wxNullBitmap'
        code = f"{self._name} = new wxBitmapButton({owner}, {self._id}, {bitmap_text}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"
        bitmap_text = get_bitmap_cpp_text_from_tuple(self._disabled)
        if len(bitmap_text) > 0:
            code += f'{self._name}->SetBitmapDisabled({bitmap_text})'
        bitmap_text = get_bitmap_cpp_text_from_tuple(self._selected)
        if len(bitmap_text) > 0:
            code += f'{self._name}->GetBitmapPressed({bitmap_text})'
        bitmap_text = get_bitmap_cpp_text_from_tuple(self._focus)
        if len(bitmap_text) > 0:
            code += f'{self._name}->SetBitmapFocus({bitmap_text})'
        bitmap_text = get_bitmap_cpp_text_from_tuple(self._hover)
        if len(bitmap_text) > 0:
            code += f'{self._name}->SetBitmapCurrent({bitmap_text})'
        code += super(Button, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'
        bitmap_text = get_bitmap_py_text_from_tuple(self._bitmap) or 'wx.NullBitmap'
        code = f'{this} = wx.BitmapButton({owner}, {self.py_id_str}, {bitmap_text}, ' \
               f'{self.py_pos_str},{self.py_size_str}, {self.py_style_str})\n'
        bitmap_text = get_bitmap_py_text_from_tuple(self._disabled)
        if len(bitmap_text) > 0:
            code += f'{this}.SetBitmapDisabled({bitmap_text})'
        bitmap_text = get_bitmap_py_text_from_tuple(self._selected)
        if len(bitmap_text) > 0:
            code += f'{this}.GetBitmapPressed({bitmap_text})'
        bitmap_text = get_bitmap_py_text_from_tuple(self._focus)
        if len(bitmap_text) > 0:
            code += f'{this}.SetBitmapFocus({bitmap_text})'
        bitmap_text = get_bitmap_py_text_from_tuple(self._hover)
        if len(bitmap_text) > 0:
            code += f'{this}.SetBitmapCurrent({bitmap_text})'
        code += super(Button, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code

