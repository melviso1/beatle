import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class StaticText(WindowControl):
    """"This class represents a button"""

    style_list = (wx.ALIGN_CENTRE, wx.ALIGN_LEFT, wx.ALIGN_RIGHT, wx.ST_ELLIPSIZE_END,
                  wx.ST_ELLIPSIZE_MIDDLE, wx.ST_ELLIPSIZE_START, wx.ST_NO_AUTORESIZE)

    style_list_py_text = ('wx.ALIGN_CENTRE', 'wx.ALIGN_LEFT', 'wx.ALIGN_RIGHT', 'wx.ST_ELLIPSIZE_END',
                          'wx.ST_ELLIPSIZE_MIDDLE', 'wx.ST_ELLIPSIZE_START', 'wx.ST_NO_AUTORESIZE')

    style_list_cc_text = ('wxALIGN_CENTRE', 'wxALIGN_LEFT', 'wxALIGN_RIGHT', 'wxST_ELLIPSIZE_END',
                          'wxST_ELLIPSIZE_MIDDLE', 'wxST_ELLIPSIZE_START', 'wxST_NO_AUTORESIZE')

    def __init__(self, **kwargs):
        self._style = 0
        self._label = 'label'
        self._wrap = -1
        super(StaticText, self).__init__(**kwargs)

    @property
    def local_statictext_kwargs(self):
        return {
            'style': self._style,
            'label': self._label,
            'wrap': self._wrap
        }

    @local_statictext_kwargs.setter
    def local_statictext_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._label = kwargs.get('label', self._label)
        self._wrap = kwargs.get('wrap', self._wrap)

    @property
    def kwargs(self):
        value = self.local_statictext_kwargs
        value.update(super(StaticText, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_statictext_kwargs = kwargs
        super(StaticText, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('static_text')

    @property
    def style_attribute_options(self):
        return [
            ('wxALIGN_CENTRE', 'Center text horizontally.'),
            ('wxALIGN_LEFT', 'Align left text.'),
            ('wxALIGN_RIGHT', 'Align right  text.'),
            ('wxST_ELLIPSIZE_END', 'Replace end of text with ellipses when don\'t hold.'),
            ('wxST_ELLIPSIZE_MIDDLE', 'Replace middle of text with ellipses when don\'t hold.'),
            ('wxST_ELLIPSIZE_START', 'Replace start of text with ellipses when don\'t hold.'),
            ('wxST_NO_AUTORESIZE', 'Disable adapt control size to contents.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Static text',
            'type': 'category',
            'help': 'Attributes of static text.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The static text control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the static text style.'
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'Text displayed in the control.',
                },
                {
                    'label': 'wrap',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._wrap,
                    'name': 'wrap',  # kwarg value
                    'help': 'wraps the controls label so that each of its lines becomes at most width pixels wide '
                            'if possible (the lines are broken at words boundaries so it might not be the case if '
                            'words are too long). If width is negative, no wrapping is done.',
                }]
        }]
        _event = []
        _base_property, _base_event = super(StaticText, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.StaticText)(edition_frame, self.py_id, self._label, self.py_pos,
                                                self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/stattext.h>
        cls = register_cpp_class(self.project, 'wxStaticText')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="static text control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/stattext.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f'{self._name} = new wxStaticText({owner}, {self._id}, "{self._label}", {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        code += super(StaticText, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.StaticText({owner}, {self.py_id_str}, "{self._label}", {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(StaticText, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


