import wx
import wx.adv
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class BitmapComboBox(WindowControl):
    """"This class represents a bitmap combobox"""
    style_list = (wx.CB_READONLY, wx.CB_SORT, wx.TE_PROCESS_ENTER)
    style_list_py_text = ('wx.CB_READONLY', 'wx.CB_SORT', 'wx.TE_PROCESS_ENTER')
    style_list_cc_text = ('wxCB_READONLY', 'wxCB_SORT', 'wxTE_PROCESS_ENTER')

    def __init__(self, **kwargs):
        self._value = ''
        self._choices = []
        self._selection = -1
        self._style = 0

        # events

        self._on_select = ''
        self._on_text = ''
        self._on_enter = ''
        super(BitmapComboBox, self).__init__(**kwargs)

    @property
    def local_bitmapcombobox_kwargs(self):
        return {
            'value': self._value,
            'choices': self._choices,
            'selection': self._selection,
            'style': self._style,
            # events
            'on_select': self._on_select,
            'on_text': self._on_text,
            'on_enter': self._on_enter,
        }

    @local_bitmapcombobox_kwargs.setter
    def local_bitmapcombobox_kwargs(self, kwargs):
        self._value = kwargs.get('value', self._value)
        self._choices = kwargs.get('choices', self._choices)
        self._selection = kwargs.get('selection', self._selection)
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_select = kwargs.get('on_select', self._on_select)
        self._on_text = kwargs.get('on_text', self._on_text)
        self._on_enter = kwargs.get('on_enter', self._on_enter)

    @property
    def local_bitmap_combo_box_events(self):
        return {
            'on_select': (self._on_select, 'wx.EVT_COMBOBOX', 'wxEVT_COMBOBOX'),
            'on_text': (self._on_text, 'wx.EVT_TEXT', 'wxEVT_TEXT'),
            'on_enter': (self._on_enter, 'wx.EVT_TEXT_ENTER', 'wxEVT_TEXT_ENTER'),
        }

    @property
    def events(self):
        value = self.local_bitmap_combo_box_events
        value.update(super(BitmapComboBox, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_bitmapcombobox_kwargs
        value.update(super(BitmapComboBox, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_bitmapcombobox_kwargs = kwargs
        super(BitmapComboBox, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('bitmap_combo_box')

    @property
    def style_attribute_options(self):
        return [
            ('wxCB_READONLY', 'allows the user to choose from the list of options but doesn\'t allow to'
                              ' enter a nonmatching value.'),
            ('wxCB_SORT', 'Sort the list alphabetically.'),
            ('wxTE_PROCESS_ENTER', 'generate the event wxEVT_TEXT_ENTER that can be handled by the program.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Bitmap combo box',
            'type': 'category',
            'help': 'Attributes of bitmap combo box.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The bitmap combobox control name."
                },
                {
                    'label': 'value',
                    'type': 'string',
                    'read_only': False,
                    'value': self._value,
                    'name': 'value',  # kwarg value
                    'help': 'The displayed value.',
                },
                {
                    'label': 'choices',
                    'type': 'string_array',
                    'read_only': False,
                    'value': self._choices,
                    'name': 'choices',  # kwarg value
                    'help': 'Contents of the BitmapComboBox. This must be a list of strings with the form'
                            'bitmap_file : label',
                },
                {
                    'label': 'selection',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._selection,
                    'name': 'selection',  # kwarg value
                    'help': 'Index of initial selection.',
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets BitmapComboBox style.'
                }]
        }]
        _event = [{
            'label': 'BitmapComboBox',
            'type': 'category',
            'help': 'Events of bitmap combobox control',
            'child': [
                {
                    'label': 'on select',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_select,
                    'name': 'on_select',  # kwarg value
                    'help': 'Called when an item in the list is selected.'
                },
                {
                    'label': 'on text',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text,
                    'name': 'on_text',  # kwarg value
                    'help': 'Called when the text box content changed.'
                },
                {
                    'label': 'on enter',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_enter,
                    'name': 'on_enter',  # kwarg value
                    'help': 'Called when enter is pressed in the combobox.'
                },
            ],
        }]
        _base_property, _base_event = super(BitmapComboBox, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.adv.BitmapComboBox)(
            edition_frame, self.py_id, self._value, self.py_pos, self.py_size, [], self.style_value)
        for item in self._choices:
            bitmap = wx.Bitmap()
            try:
                image_file, text = item.split(':')
                bitmap.LoadFile(image_file)
                self._edition_window.Append(item, bitmap)
            except:
                self._edition_window.Append(item)

        if self._selection > -1:
            self._edition_window.SetSelection(self._selection)

        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/bmpcbox.h>
        cls = register_cpp_class(self.project, 'wxBitmapComboBox')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="bitmap combobox control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        self.add_cpp_required_header('#include <wx/bmpcbox.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxBitmapComboBox({owner}, {self._id}, "{self._value}", {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, 0, NULL, {self.cc_style_str});\n'
        if len(self._choices) > 0:
            for item in self._choices:
                image_file, text = item.split(':')
                code += f'{self._name}->Append("{text}", wxBitmap("{image_file}", wxBITMAP_TYPE_ANY));\n'
        code += super(BitmapComboBox, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.adv')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.adv.BitmapComboBox({owner}, {self.py_id_str}, "{self._value}", {self.py_pos_str},' \
               f'{self.py_size_str}, [], {self.py_style_str})\n'
        if len(self._choices) > 0:
            for item in self._choices:
                try:
                    image_file, text = item.split(':')
                    code += f'{this}.Append("{text}", wx.Bitmap("{image_file}", wx.BITMAP_TYPE_ANY))\n'
                except:
                    code += f'{this}.Append("{item}")\n'
        code += super(BitmapComboBox, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

