import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class ListControl(WindowControl):
    """"This class represents a list control"""

    style_list = (
        wx.LC_LIST, wx.LC_REPORT, wx.LC_VIRTUAL, wx.LC_ICON, wx.LC_SMALL_ICON, wx.LC_ALIGN_TOP, wx.LC_ALIGN_LEFT,
        wx.LC_AUTOARRANGE, wx.LC_EDIT_LABELS, wx.LC_NO_HEADER, wx.LC_NO_SORT_HEADER, wx.LC_SINGLE_SEL,
        wx.LC_SORT_ASCENDING, wx.LC_SORT_DESCENDING, wx.LC_HRULES, wx.LC_VRULES)

    style_list_py_text = (
        'wx.LC_LIST', 'wx.LC_REPORT', 'wx.LC_VIRTUAL', 'wx.LC_ICON', 'wx.LC_SMALL_ICON', 'wx.LC_ALIGN_TOP',
        'wx.LC_ALIGN_LEFT', 'wx.LC_AUTOARRANGE', 'wx.LC_EDIT_LABELS', 'wx.LC_NO_HEADER', 'wx.LC_NO_SORT_HEADER',
        'wx.LC_SINGLE_SEL', 'wx.LC_SORT_ASCENDING', 'wx.LC_SORT_DESCENDING', 'wx.LC_HRULES', 'wx.LC_VRULES'
    )

    style_list_cc_text = (
        'wxLC_LIST', 'wxLC_REPORT', 'wxLC_VIRTUAL', 'wxLC_ICON', 'wxLC_SMALL_ICON', 'wxLC_ALIGN_TOP',
        'wxLC_ALIGN_LEFT', 'wxLC_AUTOARRANGE', 'wxLC_EDIT_LABELS', 'wxLC_NO_HEADER', 'wxLC_NO_SORT_HEADER',
        'wxLC_SINGLE_SEL', 'wxLC_SORT_ASCENDING', 'wxLC_SORT_DESCENDING', 'wxLC_HRULES', 'wxLC_VRULES'
    )

    def __init__(self, **kwargs):
        self._style = 1
        # events
        self._on_begin_drag = ''
        self._on_right_begin_drag = ''
        self._on_begin_label_edit = ''
        self._on_end_label_edit = ''
        self._on_delete_item = ''
        self._on_delete_all_items = ''
        self._on_select_item = ''
        self._on_deselect_item = ''
        self._on_activate_item = ''
        self._on_focus_item = ''
        self._on_middle_click_item = ''
        self._on_right_click_item = ''
        self._on_listcontrol_key_down = ''
        self._on_insert_item = ''
        self._on_click_column = ''
        self._on_right_click_column = ''
        self._on_begin_drag_column = ''
        self._on_dragging_column = ''
        self._on_end_drag_column = ''
        self._on_cache_hint = ''
        self._on_item_checked = ''
        self._on_item_unchecked = ''
        super(ListControl, self).__init__(**kwargs)

    @property
    def local_listctrl_kwargs(self):
        return {
            'style': self._style,
            # events
            'on_begin_drag': self._on_begin_drag,
            'on_right_begin_drag': self._on_right_begin_drag,
            'on_begin_label_edit': self._on_begin_label_edit,
            'on_end_label_edit': self._on_end_label_edit,
            'on_delete_item': self._on_delete_item,
            'on_delete_all_items': self._on_delete_all_items,
            'on_select_item': self._on_select_item,
            'on_deselect_item': self._on_deselect_item,
            'on_activate_item': self._on_activate_item,
            'on_focus_item': self._on_focus_item,
            'on_middle_click_item': self._on_middle_click_item,
            'on_right_click_item': self._on_right_click_item,
            'on_listcontrol_key_down': self._on_listcontrol_key_down,
            'on_insert_item': self._on_insert_item,
            'on_click_column': self._on_click_column,
            'on_right_click_column': self._on_right_click_column,
            'on_begin_drag_column': self._on_begin_drag_column,
            'on_dragging_column': self._on_dragging_column,
            'on_end_drag_column': self._on_end_drag_column,
            'on_cache_hint': self._on_cache_hint,
            'on_item_checked': self._on_item_checked,
            'on_item_unchecked': self._on_item_unchecked,
        }

    @local_listctrl_kwargs.setter
    def local_listctrl_kwargs(self, kwargs):
        previous_control_style = self.style_value
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_begin_drag = kwargs.get('on_begin_drag', self._on_begin_drag)
        self._on_right_begin_drag = kwargs.get('on_right_begin_drag', self._on_right_begin_drag)
        self._on_begin_label_edit = kwargs.get('on_begin_label_edit', self._on_begin_label_edit)
        self._on_end_label_edit = kwargs.get('on_end_label_edit', self._on_end_label_edit)
        self._on_delete_item = kwargs.get('on_delete_item', self._on_delete_item)
        self._on_delete_all_items = kwargs.get('on_delete_all_items', self._on_delete_all_items)
        self._on_select_item = kwargs.get('on_select_item', self._on_select_item)
        self._on_deselect_item = kwargs.get('on_deselect_item', self._on_deselect_item)
        self._on_activate_item = kwargs.get('on_activate_item', self._on_activate_item)
        self._on_focus_item = kwargs.get('on_focus_item', self._on_focus_item)
        self._on_middle_click_item = kwargs.get('on_middle_click_item', self._on_middle_click_item)
        self._on_right_click_item = kwargs.get('on_right_click_item', self._on_right_click_item)
        self._on_listcontrol_key_down = kwargs.get('on_listcontrol_key_down', self._on_listcontrol_key_down)
        self._on_insert_item = kwargs.get('on_insert_item', self._on_insert_item)
        self._on_click_column = kwargs.get('on_click_column', self._on_click_column)
        self._on_right_click_column = kwargs.get('on_right_click_column', self._on_right_click_column)
        self._on_begin_drag_column = kwargs.get('on_begin_drag_column', self._on_begin_drag_column)
        self._on_dragging_column = kwargs.get('on_dragging_column', self._on_dragging_column)
        self._on_end_drag_column = kwargs.get('on_end_drag_column', self._on_end_drag_column)
        self._on_cache_hint = kwargs.get('on_cache_hint', self._on_cache_hint)
        self._on_item_checked = kwargs.get('on_item_checked', self._on_item_checked)
        self._on_item_unchecked = kwargs.get('on_item_unchecked', self._on_item_unchecked)

        # check incompatible styles and fix it
        has_basic_style = False
        control_style = self.style_value
        if control_style & wx.LC_LIST:
            if not (previous_control_style & wx.LC_LIST):
                if control_style & wx.LC_REPORT:
                    self._style ^= 2 ** self.style_list.index(wx.LC_REPORT)
                if control_style & wx.LC_VIRTUAL:
                    self._style ^= 2 ** self.style_list.index(wx.LC_VIRTUAL)
                elif control_style & wx.LC_ICON:
                    self._style ^= 2 ** self.style_list.index(wx.LC_ICON)
                elif control_style & wx.LC_SMALL_ICON:
                    self._style ^= 2 ** self.style_list.index(wx.LC_SMALL_ICON)
            elif control_style & (wx.LC_REPORT | wx.LC_VIRTUAL):
                self._style ^= 2 ** self.style_list.index(wx.LC_LIST)
                if control_style & wx.LC_VIRTUAL:
                    self._style |= 2 ** self.style_list.index(wx.LC_REPORT)  # ensure required style
            elif control_style & (wx.LC_ICON | wx.LC_SMALL_ICON):
                self._style ^= 2 ** self.style_list.index(wx.LC_LIST)
        elif control_style & wx.LC_REPORT:
            if not (previous_control_style & wx.LC_REPORT):
                if control_style & wx.LC_ICON:
                    self._style ^= 2 ** self.style_list.index(wx.LC_ICON)
                elif control_style & wx.LC_SMALL_ICON:
                    self._style ^= 2 ** self.style_list.index(wx.LC_SMALL_ICON)
            elif control_style & (wx.LC_ICON | wx.LC_SMALL_ICON):
                self._style ^= 2 ** self.style_list.index(wx.LC_REPORT)
                if control_style & wx.LC_VIRTUAL:
                    self._style ^= 2 ** self.style_list.index(wx.LC_VIRTUAL)
        elif control_style & wx.LC_VIRTUAL:
            # we dont need to check if it's new, as it is because previous if is false
            # and this style requires the other
            self._style |= 2 ** self.style_list.index(wx.LC_REPORT)
            if control_style & wx.LC_ICON:
                self._style ^= 2 ** self.style_list.index(wx.LC_ICON)
            elif control_style & wx.LC_SMALL_ICON:
                self._style ^= 2 ** self.style_list.index(wx.LC_SMALL_ICON)
        elif control_style & wx.LC_ICON:
            if not (previous_control_style & wx.LC_ICON):
                if control_style & wx.LC_SMALL_ICON:
                    self._style ^= 2 ** self.style_list.index(wx.LC_SMALL_ICON)
            elif control_style & wx.LC_SMALL_ICON:
                self._style ^= 2 ** self.style_list.index(wx.LC_ICON)
        elif not (control_style & wx.LC_SMALL_ICON):
            # this can only happen when the user attempts to remove all the styles
            self._style |= 2 ** self.style_list.index(wx.LC_LIST)

    @property
    def local_listcontrol_events(self):
        return {
            'on_begin_drag': (self._on_begin_drag, 'wx.EVT_LIST_BEGIN_DRAG', 'wxEVT_LIST_BEGIN_DRAG'),
            'on_right_begin_drag': (self._on_right_begin_drag, 'wx.EVT_LIST_BEGIN_RDRAG', 'wxEVT_LIST_BEGIN_RDRAG'),
            'on_begin_label_edit': (self._on_begin_label_edit, 'wx.EVT_LIST_BEGIN_LABEL_EDIT',
                                    'wxEVT_LIST_BEGIN_LABEL_EDIT'),
            'on_end_label_edit': (self._on_end_label_edit, 'wx.EVT_LIST_END_LABEL_EDIT', 'wxEVT_LIST_END_LABEL_EDIT'),
            'on_delete_item': (self._on_delete_item, 'wx.EVT_LIST_DELETE_ITEM', 'wxEVT_LIST_DELETE_ITEM'),
            'on_delete_all_items': (self._on_delete_all_items, 'wx.EVT_LIST_DELETE_ALL_ITEMS',
                                    'wxEVT_LIST_DELETE_ALL_ITEMS'),
            'on_select_item': (self._on_select_item, 'wx.EVT_LIST_ITEM_SELECTED', 'wxEVT_LIST_ITEM_SELECTED'),
            'on_deselect_item': (self._on_deselect_item, 'wx.EVT_LIST_ITEM_DESELECTED', 'wxEVT_LIST_ITEM_DESELECTED'),
            'on_activate_item': (self._on_activate_item, 'wx.EVT_LIST_ITEM_ACTIVATED', 'wxEVT_LIST_ITEM_ACTIVATED'),
            'on_focus_item': (self._on_focus_item, 'wx.EVT_LIST_ITEM_FOCUSED', 'wxEVT_LIST_ITEM_FOCUSED'),
            'on_middle_click_item': (self._on_middle_click_item, 'wx.EVT_LIST_ITEM_MIDDLE_CLICK',
                                     'wxEVT_LIST_ITEM_MIDDLE_CLICK'),
            'on_right_click_item': (self._on_right_click_item, 'wx.EVT_LIST_ITEM_RIGHT_CLICK',
                                    'wxEVT_LIST_ITEM_RIGHT_CLICK'),
            'on_listcontrol_key_down': (self._on_listcontrol_key_down, 'wx.EVT_LIST_KEY_DOWN', 'wxEVT_LIST_KEY_DOWN'),
            'on_insert_item': (self._on_insert_item, 'wx.EVT_LIST_INSERT_ITEM', 'wxEVT_LIST_INSERT_ITEM'),
            'on_click_column': (self._on_click_column, 'wx.EVT_LIST_COL_CLICK', 'wxEVT_LIST_COL_CLICK'),
            'on_right_click_column': (self._on_right_click_column, 'wx.EVT_LIST_COL_RIGHT_CLICK',
                                      'wxEVT_LIST_COL_RIGHT_CLICK'),
            'on_begin_drag_column': (self._on_begin_drag_column, 'wx.EVT_LIST_COL_BEGIN_DRAG',
                                     'wxEVT_LIST_COL_BEGIN_DRAG'),
            'on_dragging_column': (self._on_dragging_column, 'wx.EVT_LIST_COL_DRAGGING', 'wxEVT_LIST_COL_DRAGGING'),
            'on_end_drag_column': (self._on_end_drag_column, 'wx.EVT_LIST_COL_END_DRAG', 'wxEVT_LIST_COL_END_DRAG'),
            'on_cache_hint': (self._on_cache_hint, 'wx.EVT_LIST_CACHE_HINT', 'wxEVT_LIST_CACHE_HINT'),
            'on_item_checked': (self._on_item_checked, 'wx.EVT_LIST_ITEM_CHECKED', 'wxEVT_LIST_ITEM_CHECKED'),
            'on_item_unchecked': (self._on_item_unchecked, 'wx.EVT_LIST_ITEM_UNCHECKED', 'wxEVT_LIST_ITEM_UNCHECKED'),
        }

    @property
    def events(self):
        value = self.local_listcontrol_events
        value.update(super(ListControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_listctrl_kwargs
        value.update(super(ListControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_listctrl_kwargs = kwargs
        super(ListControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('list_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxLC_LIST', "Multicolumn list view, with optional small icons."
                          "Columns are computed automatically, i.e.you don’t set columns as in LC_REPORT. "
                          "In other words, the list wraps, unlike a wx.ListBox."),
            ('wxLC_REPORT', "Single or multicolumn report view, with optional header."),
            ('wxLC_VIRTUAL', "The application provides items text on demand. May only be used with LC_REPORT."),
            ('wxLC_ICON', "Large icon view, with optional labels."),
            ('wxLC_SMALL_ICON', "Small icon view, with optional labels."),
            ('wxLC_ALIGN_TOP', "Icons align to the top.Win32 default, Win32 only."),
            ('wxLC_ALIGN_LEFT', "Icons align to the left."),
            ('wxLC_AUTOARRANGE', "Icons arrange themselves. Win32 only."),
            ('wxLC_EDIT_LABELS', "Labels are editable. The application will be notified when editing starts."),
            ('wxLC_NO_HEADER', "No header in report mode."),
            ('wxLC_NO_SORT_HEADER', "Prevent headers acts as sort buttons. Windows only."),
            ('wxLC_SINGLE_SEL', "Single selection (default is multiple)."),
            ('wxLC_SORT_ASCENDING', "Sort in ascending order. (You must still supply a "
                                    "comparison callback in wx.ListCtrl.SortItems)."),
            ('wxLC_SORT_DESCENDING', "Sort in descending order. (You must still supply a "
                                     "comparison callback in wx.ListCtrl.SortItems)."),
            ('wxLC_HRULES', "Draws light horizontal rules between rows in report mode."),
            ('wxLC_VRULES', "Draws light vertical rules between columns in report mode.")
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'List Control',
            'type': 'category',
            'help': 'Attributes of list.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The list control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets list control style.'
                }]
        }]
        _event = [{
            'label': 'ListControl',
            'type': 'category',
            'help': 'Events of list control',
            'child': [
                {
                    'label': 'on begin drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_begin_drag,
                    'name': 'on_begin_drag',  # kwarg value
                    'help': 'Called when begins a left mouse item dragging.'
                },
                {
                    'label': 'on begin label edit',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_begin_label_edit,
                    'name': 'on_begin_label_edit',  # kwarg value
                    'help': 'Called when user starts to edit an item label.'
                },
                {
                    'label': 'on end label edit',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_end_label_edit,
                    'name': 'on_end_label_edit',  # kwarg value
                    'help': 'Called when the edition of an item label ends.'
                },
                {
                    'label': 'on delete item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_delete_item,
                    'name': 'on_delete_item',  # kwarg value
                    'help': 'Called an item is deleted.'
                },
                {
                    'label': 'on delete all items',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_delete_all_items,
                    'name': 'on_delete_all_items',  # kwarg value
                    'help': 'Called all the listbox items are deleted.'
                },
                {
                    'label': 'on select item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_select_item,
                    'name': 'on_select_item',  # kwarg value
                    'help': 'Called when the user selects an item.'
                },
                {
                    'label': 'on deselect item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_deselect_item,
                    'name': 'on_deselect_item',  # kwarg value
                    'help': 'Called when the user deselects an item.'
                },
                {
                    'label': 'on activate item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_activate_item,
                    'name': 'on_activate_item',  # kwarg value
                    'help': 'Called when an item becomes active (by double click or focused enter).'
                },
                {
                    'label': 'on focus item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_focus_item,
                    'name': 'on_focus_item',  # kwarg value
                    'help': 'Called when an item becomes focused.'
                },
                {
                    'label': 'on middle click item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_middle_click_item,
                    'name': 'on_middle_click_item',  # kwarg value
                    'help': 'Called when the user clicks an item with middle mouse button.'
                },
                {
                    'label': 'on right click item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_right_click_item,
                    'name': 'on_right_click_item',  # kwarg value
                    'help': 'Called when the user clicks an item with right mouse button.'
                },
                {
                    'label': 'on key down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_listcontrol_key_down,
                    'name': 'on_listcontrol_key_down',  # kwarg value
                    'help': 'Called when list control received key press.'
                },
                {
                    'label': 'on insert item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_insert_item,
                    'name': 'on_insert_item',  # kwarg value
                    'help': 'Called when an item has been inserted in the list control.'
                },
                {
                    'label': 'on click column',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_click_column,
                    'name': 'on_click_column',  # kwarg value
                    'help': 'Called when the user clicks over a column.'
                },
                {
                    'label': 'on right click column',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_right_click_column,
                    'name': 'on_right_click_column',  # kwarg value
                    'help': 'Called when the user clicks with right mouse button over a column.'
                },
                {
                    'label': 'on begin drag column',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_begin_drag_column,
                    'name': 'on_begin_drag_column',  # kwarg value
                    'help': 'Called when a column drag starts.'
                },
                {
                    'label': 'on drag column',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dragging_column,
                    'name': 'on_dragging_column',  # kwarg value
                    'help': 'Called while a column gets dragged.'
                },
                {
                    'label': 'on drag column',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_end_drag_column,
                    'name': 'on_end_drag_column',  # kwarg value
                    'help': 'Called when a column drag ends.'
                },
                {
                    'label': 'on cache hint',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_cache_hint,
                    'name': 'on_cache_hint',  # kwarg value
                    'help': 'Called for virtual list control to prepare data.'
                },
                {
                    'label': 'on item checked',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_item_checked,
                    'name': 'on_item_checked',  # kwarg value
                    'help': 'Called when a list control item gets checked.'
                },
                {
                    'label': 'on item unchecked',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_item_unchecked,
                    'name': 'on_item_unchecked',  # kwarg value
                    'help': 'Called when a list control item gets unchecked.'
                },
            ],
        }]

        _base_property, _base_event = super(ListControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.ListCtrl)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
        except Exception as exception:
            full_message = str(exception)
            index = full_message.index(':')
            message = full_message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            return
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/listctrl.h>
        cls = register_cpp_class(self.project, 'wxListCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="listctrl control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/listctrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
            # edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
        code = f'{self._name} = new wxListCtrl({owner}, {self._id}, {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        code += super(ListControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'

        code = f'{this} = wx.ListCtrl({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(ListControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

