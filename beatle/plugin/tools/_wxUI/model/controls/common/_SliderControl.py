import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class SliderControl(WindowControl):
    """"This class represents a slider"""

    style_list = (
        wx.SL_HORIZONTAL, wx.SL_VERTICAL, wx.SL_AUTOTICKS, wx.SL_MIN_MAX_LABELS, wx.SL_VALUE_LABEL,
        wx.SL_LABELS, wx.SL_LEFT, wx.SL_RIGHT, wx.SL_TOP, wx.SL_BOTTOM, wx.SL_BOTH, wx.SL_SELRANGE, wx.SL_INVERSE
    )
    style_list_py_text = (
        'wx.SL_HORIZONTAL', 'wx.SL_VERTICAL', 'wx.SL_AUTOTICKS', 'wx.SL_MIN_MAX_LABELS', 'wx.SL_VALUE_LABEL',
        'wx.SL_LABELS', 'wx.SL_LEFT', 'wx.SL_RIGHT', 'wx.SL_TOP', 'wx.SL_BOTTOM', 'wx.SL_BOTH',
        'wx.SL_SELRANGE', 'wx.SL_INVERSE'
    )
    style_list_cc_text = (
        'wxSL_HORIZONTAL', 'wxSL_VERTICAL', 'wxSL_AUTOTICKS', 'wxSL_MIN_MAX_LABELS', 'wxSL_VALUE_LABEL',
        'wxSL_LABELS', 'wxSL_LEFT', 'wxSL_RIGHT', 'wxSL_TOP', 'wxSL_BOTTOM', 'wxSL_BOTH',
        'wxSL_SELRANGE', 'wxSL_INVERSE'
    )

    def __init__(self, **kwargs):
        self._value = 50
        self._min_value = 0
        self._max_value = 100
        self._style = 1
        self._on_scroll = ''
        self._on_scroll_top = ''
        self._on_scroll_bottom = ''
        self._on_scroll_line_up = ''
        self._on_scroll_line_down = ''
        self._on_scroll_page_up = ''
        self._on_scroll_page_down = ''
        self._on_scroll_thumb_track = ''
        self._on_scroll_thumb_release = ''
        self._on_scroll_changed = ''
        self._on_command_scroll = ''
        self._on_command_scroll_top = ''
        self._on_command_scroll_bottom = ''
        self._on_command_scroll_line_up = ''
        self._on_command_scroll_line_down = ''
        self._on_command_scroll_page_up = ''
        self._on_command_scroll_page_down = ''
        self._on_command_scroll_thumb_track = ''
        self._on_command_scroll_thumb_release = ''
        self._on_command_scroll_changed = ''
        super(SliderControl, self).__init__(**kwargs)

    @property
    def local_slider_kwargs(self):
        return {
            'value': self._value,
            'min_value': self._min_value,
            'max_value': self._max_value,
            'style': self._style,
            'on_scroll': self._on_scroll,
            'on_scroll_top': self._on_scroll_top,
            'on_scroll_bottom': self._on_scroll_bottom,
            'on_scroll_line_up': self._on_scroll_line_up,
            'on_scroll_line_down': self._on_scroll_line_down,
            'on_scroll_page_up': self._on_scroll_page_up,
            'on_scroll_page_down': self._on_scroll_page_down,
            'on_scroll_thumb_track': self._on_scroll_thumb_track,
            'on_scroll_thumb_release': self._on_scroll_thumb_release,
            'on_scroll_changed': self._on_scroll_changed,
            'on_command_scroll': self._on_command_scroll,
            'on_command_scroll_top': self._on_command_scroll_top,
            'on_command_scroll_bottom': self._on_command_scroll_bottom,
            'on_command_scroll_line_up': self._on_command_scroll_line_up,
            'on_command_scroll_line_down': self._on_command_scroll_line_down,
            'on_command_scroll_page_up': self._on_command_scroll_page_up,
            'on_command_scroll_page_down': self._on_command_scroll_page_down,
            'on_command_scroll_thumb_track': self._on_command_scroll_thumb_track,
            'on_command_scroll_thumb_release': self._on_command_scroll_thumb_release,
            'on_command_scroll_changed': self._on_command_scroll_changed
        }

    @local_slider_kwargs.setter
    def local_slider_kwargs(self, kwargs):
        self._value = kwargs.get('value', self._value)
        self._min_value = kwargs.get('min_value', self._min_value)
        self._max_value = kwargs.get('max_value', self._max_value)
        self._style = kwargs.get('style', self._style)
        self._on_scroll = kwargs.get('on_scroll', self._on_scroll)
        self._on_scroll_top = kwargs.get('on_scroll_top', self._on_scroll_top)
        self._on_scroll_bottom = kwargs.get('on_scroll_bottom', self._on_scroll_bottom)
        self._on_scroll_line_up = kwargs.get('on_scroll_line_up', self._on_scroll_line_up)
        self._on_scroll_line_down = kwargs.get('on_scroll_line_down', self._on_scroll_line_down)
        self._on_scroll_page_up = kwargs.get('on_scroll_page_up', self._on_scroll_page_up)
        self._on_scroll_page_down = kwargs.get('on_scroll_page_down', self._on_scroll_page_down)
        self._on_scroll_thumb_track = kwargs.get('on_scroll_thumb_track', self._on_scroll_thumb_track)
        self._on_scroll_thumb_release = kwargs.get('on_scroll_thumb_release', self._on_scroll_thumb_release)
        self._on_scroll_changed = kwargs.get('on_scroll_changed', self._on_scroll_changed)
        self._on_command_scroll = kwargs.get('on_command_scroll', self._on_command_scroll)
        self._on_command_scroll_top = kwargs.get('on_command_scroll_top', self._on_command_scroll_top)
        self._on_command_scroll_bottom = kwargs.get('on_command_scroll_bottom', self._on_command_scroll_bottom)
        self._on_command_scroll_line_up = kwargs.get('on_command_scroll_line_up', self._on_command_scroll_line_up)
        self._on_command_scroll_line_down = kwargs.get('on_command_scroll_line_down', self._on_command_scroll_line_down)
        self._on_command_scroll_page_up = kwargs.get('on_command_scroll_page_up', self._on_command_scroll_page_up)
        self._on_command_scroll_page_down = kwargs.get('on_command_scroll_page_down', self._on_command_scroll_page_down)
        self._on_command_scroll_thumb_track = kwargs.get('on_command_scroll_thumb_track',
                                                         self._on_command_scroll_thumb_track)
        self._on_command_scroll_thumb_release = kwargs.get('on_command_scroll_thumb_release',
                                                           self._on_command_scroll_thumb_release)
        self._on_command_scroll_changed = kwargs.get('on_command_scroll_changed', self._on_command_scroll_changed)

    @property
    def local_slidercontrol_events(self):
        return {
            'on_scroll': (self._on_scroll, 'wx.EVT_SCROLL', 'wxEVT_SCROLL'),
            'on_scroll_top': (self._on_scroll_top, 'wx.EVT_SCROLL_TOP', 'wxEVT_SCROLL_TOP'),
            'on_scroll_bottom': (self._on_scroll_bottom, 'wx.EVT_SCROLL_BOTTOM', 'wxEVT_SCROLL_BOTTOM'),
            'on_scroll_line_up': (self._on_scroll_line_up, 'wx.EVT_SCROLL_LINEUP', 'wxEVT_SCROLL_LINEUP'),
            'on_scroll_line_down': (self._on_scroll_line_down, 'wx.EVT_SCROLL_LINEDOWN', 'wxEVT_SCROLL_LINEDOWN'),
            'on_scroll_page_up': (self._on_scroll_page_up, 'wx.EVT_SCROLL_PAGEUP', 'wxEVT_SCROLL_PAGEUP'),
            'on_scroll_page_down': (self._on_scroll_page_down, 'wx.EVT_SCROLL_PAGEDOWN', 'wxEVT_SCROLL_PAGEDOWN'),
            'on_scroll_thumb_track': (self._on_scroll_thumb_track, 'wx.EVT_SCROLL_THUMBTRACK',
                                      'wxEVT_SCROLL_THUMBTRACK'),
            'on_scroll_thumb_release': (self._on_scroll_thumb_release, 'wx.EVT_SCROLL_THUMBRELEASE',
                                        'wxEVT_SCROLL_THUMBRELEASE'),
            'on_scroll_changed': (self._on_scroll_changed, 'wx.EVT_SCROLL_CHANGED', 'wxEVT_SCROLL_CHANGED'),
            'on_command_scroll': (self._on_command_scroll, 'wx.EVT_COMMAND_SCROLL', 'wxEVT_COMMAND_SCROLL'),
            'on_command_scroll_top': (self._on_command_scroll_top, 'wx.EVT_COMMAND_SCROLL_TOP',
                                      'wxEVT_COMMAND_SCROLL_TOP'),
            'on_command_scroll_bottom': (self._on_command_scroll_bottom, 'wx.EVT_COMMAND_SCROLL_BOTTOM',
                                         'wxEVT_COMMAND_SCROLL_BOTTOM'),
            'on_command_scroll_line_up': (self._on_command_scroll_line_up, 'wx.EVT_COMMAND_SCROLL_LINEUP',
                                          'wxEVT_COMMAND_SCROLL_LINEUP'),
            'on_command_scroll_line_down': (self._on_command_scroll_line_down, 'wx.EVT_COMMAND_SCROLL_LINEDOWN',
                                            'wxEVT_COMMAND_SCROLL_LINEDOWN'),
            'on_command_scroll_page_up': (self._on_command_scroll_page_up, 'wx.EVT_COMMAND_SCROLL_PAGEUP',
                                          'wxEVT_COMMAND_SCROLL_PAGEUP'),
            'on_command_scroll_page_down': (self._on_command_scroll_page_down, 'wx.EVT_COMMAND_SCROLL_PAGEDOWN',
                                            'wxEVT_COMMAND_SCROLL_PAGEDOWN'),
            'on_command_scroll_thumb_track': (self._on_command_scroll_thumb_track, 'wx.EVT_COMMAND_SCROLL_THUMBTRACK',
                                              'wxEVT_COMMAND_SCROLL_THUMBTRACK'),
            'on_command_scroll_thumb_release': (self._on_command_scroll_thumb_release,
                                                'wx.EVT_COMMAND_SCROLL_THUMBRELEASE',
                                                'wxEVT_COMMAND_SCROLL_THUMBRELEASE'),
            'on_command_scroll_changed': (self._on_command_scroll_changed, 'wx.EVT_COMMAND_SCROLL_CHANGED',
                                          'wxEVT_COMMAND_SCROLL_CHANGED'),
        }

    @property
    def events(self):
        value = self.local_slidercontrol_events
        value.update(super(SliderControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_slider_kwargs
        value.update(super(SliderControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_slider_kwargs = kwargs
        super(SliderControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('slider')

    @property
    def style_attribute_options(self):
        return [
            ('wxSL_HORIZONTAL', "Displays the slider horizontally (this is the default)."),
            ('wxSL_VERTICAL', "Displays the slider vertically."),
            ('wxSL_AUTOTICKS', "Displays tick marks (Windows, GTK+ 2.16 and later)."),
            ('wxSL_MIN_MAX_LABELS', "Displays minimum, maximum labels (new since wxWidgets 2.9.1)."),
            ('wxSL_VALUE_LABEL', "Displays value label (new since wxWidgets 2.9.1)."),
            ('wxSL_LABELS',
             "Displays minimum, maximum and value labels (same as wx.SL_VALUE_LABEL and wx.SL_MIN_MAX_LABELS together)."),
            ('wxSL_LEFT', "Displays ticks on the left and forces the slider to be vertical (Windows and GTK+ 3 only)."),
            ('wxSL_RIGHT', "Displays ticks on the right and forces the slider to be vertical."),
            ('wxSL_TOP', "Displays ticks on the top (Windows and GTK+ 3 only)."),
            ('wxSL_BOTTOM', "Displays ticks on the bottom (this is the default)."),
            ('wxSL_BOTH', "Displays ticks on both sides of the slider. Windows only."),
            ('wxSL_SELRANGE', "Displays a highlighted selection range. Windows only."),
            ('wxSL_INVERSE',
             "Inverses the minimum and maximum endpoints on the slider. Not compatible with wx.SL_SELRANGE."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _base_property, _base_event = super(SliderControl, self).model_attributes
        _property = [{
            'label': 'Slider',
            'type': 'category',
            'help': 'Attributes of slider.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The slider control name."
                },
                {
                    'label': 'value',
                    'type': 'integer',
                    'value': self._value,
                    'name': 'value',
                    'help': 'Current slider value.'
                },
                {
                    'label': 'minimum value',
                    'type': 'integer',
                    'value': self._min_value,
                    'name': 'min_value',
                    'help': 'Minimum slider value.'
                },
                {
                    'label': 'maximum value',
                    'type': 'integer',
                    'value': self._max_value,
                    'name': 'max_value',
                    'help': 'Maximum slider value.'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the slider style.'
                }]
        }]
        _event = [{
            'label': 'Slider',
            'type': 'category',
            'help': 'Events of slider control',
            'child': [
                {
                    'label': 'on scroll',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll,
                    'name': 'on_scroll',  # kwarg value
                    'help': 'called for all scroll events.',
                },
                {
                    'label': 'on scroll top',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_top,
                    'name': 'on_scroll_top',  # kwarg value
                    'help': 'called when scroll to minimum.',
                },
                {
                    'label': 'on scroll bottom',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_bottom,
                    'name': 'on_scroll_bottom',  # kwarg value
                    'help': 'called when scroll to maximum.',
                },
                {
                    'label': 'on scroll line up',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_line_up,
                    'name': 'on_scroll_line_up',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on scroll line down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_line_down,
                    'name': 'on_scroll_line_down',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on scroll page up',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_page_up,
                    'name': 'on_scroll_page_up',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on scroll page down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_page_down,
                    'name': 'on_scroll_page_down',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on scroll thumb track',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_thumb_track,
                    'name': 'on_scroll_thumb_track',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on scroll thumb release',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_thumb_release,
                    'name': 'on_scroll_thumb_release',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on scroll changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_changed,
                    'name': 'on_scroll_changed',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll,
                    'name': 'on_command_scroll',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll top',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_top,
                    'name': 'on_command_scroll_top',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll bottom',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_bottom,
                    'name': 'on_command_scroll_bottom',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll line up',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_line_up,
                    'name': 'on_command_scroll_line_up',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll line down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_line_down,
                    'name': 'on_command_scroll_line_down',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll page up',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_page_up,
                    'name': 'on_command_scroll_page_up',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll page down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_page_down,
                    'name': 'on_command_scroll_page_down',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll thumb track',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_thumb_track,
                    'name': 'on_command_scroll_thumb_track',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll thumb release',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_thumb_release,
                    'name': 'on_command_scroll_thumb_release',  # kwarg value
                    'help': '',
                },
                {
                    'label': 'on command scroll changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_changed,
                    'name': 'on_command_scroll_changed',  # kwarg value
                    'help': '',
                }
            ],
        }]

        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.Slider)(
            edition_frame, self.py_id, self._value, self._min_value, self._max_value,
            self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/slider.h>
        cls = register_cpp_class(self.project, 'wxSlider')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="slider control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/slider.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f"{self._name} = new wxSlider({owner}, {self._id}, {self._value}, {self._min_value}, " \
               f"{self._max_value}, {self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n"
        code += super(SliderControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)

        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.Slider({owner}, {self.py_id_str}, {self._value}, {self._min_value}, ' \
               f'{self._max_value}, {self.py_pos_str},{self.py_size_str}, {self.py_style_str})\n'
        code += super(SliderControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

