import wx
from .._WindowControl import WindowControl
from ....lib import BitmapProperty
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, get_bitmap_from_tuple, \
    get_bitmap_py_text_from_tuple, get_bitmap_cpp_text_from_tuple


class StaticBitmapControl(WindowControl):
    """"This class represents a button"""

    def __init__(self, **kwargs):
        self._bitmap = (0, '')
        super(StaticBitmapControl, self).__init__(**kwargs)

    @property
    def local_staticbitmap_kwargs(self):
        return {
            'bitmap': self._bitmap,
        }

    @local_staticbitmap_kwargs.setter
    def local_staticbitmap_kwargs(self, kwargs):
        self._bitmap = kwargs.get('bitmap', self._bitmap)

    @property
    def kwargs(self):
        value = self.local_staticbitmap_kwargs
        value.update(super(StaticBitmapControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_staticbitmap_kwargs = kwargs
        super(StaticBitmapControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('static_bitmap')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Static bitmap',
            'type': 'category',
            'help': 'Attributes of static bitmap.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The control name."
                },
                {
                    'label': 'bitmap',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._bitmap,
                    'name': 'bitmap',  # kwarg value
                    'help': "The base bitmap (required)."
                }]
        }]
        _event = []
        _base_property, _base_event = super(StaticBitmapControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        bitmap = get_bitmap_from_tuple(self._bitmap) or wx.NullBitmap
        self._edition_window = uiMockupControl(wx.StaticBitmap)(
            edition_frame, self.py_id, bitmap, self.py_pos, self.py_size, 0)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/statbmp.h>
        cls = register_cpp_class(self.project, 'wxStaticBitmap')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="static bitmap control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/statbmp.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        image = get_bitmap_cpp_text_from_tuple(self._bitmap)
        code = f"{self._name} = new wxStaticBitmap({owner}, {self._id}, {image}, " \
               f"{self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n"
        code += super(StaticBitmapControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'
        image = get_bitmap_py_text_from_tuple(self._bitmap)
        code = f'{this} = wx.StaticBitmap({owner}, {self.py_id_str}, {image}, {self.py_pos_str},' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(StaticBitmapControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

