import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class GaugeControl(WindowControl):
    """"This class represents a slider"""
    if wx.version().split(' ')[-1] >= '3.1.0':
        style_list = (wx.GA_HORIZONTAL, wx.GA_VERTICAL, wx.GA_SMOOTH, wx.GA_TEXT, wx.GA_PROGRESS)
        style_list_py_text = ('wx.GA_HORIZONTAL', 'wx.GA_VERTICAL', 'wx.GA_SMOOTH', 'wx.GA_TEXT', 'wx.GA_PROGRESS')
        style_list_cc_text = ('wxGA_HORIZONTAL', 'wxGA_VERTICAL', 'wxGA_SMOOTH', 'wxGA_TEXT', 'wxGA_PROGRESS')
    else:
        style_list = (wx.GA_HORIZONTAL, wx.GA_VERTICAL, wx.GA_SMOOTH)
        style_list_py_text = ('wx.GA_HORIZONTAL', 'wx.GA_VERTICAL', 'wx.GA_SMOOTH')
        style_list_cc_text = ('wxGA_HORIZONTAL', 'wxGA_VERTICAL', 'wxGA_SMOOTH')

    def __init__(self, **kwargs):
        self._range = 100
        self._value = 50
        self._style = 1
        super(GaugeControl, self).__init__(**kwargs)

    @property
    def local_gauge_kwargs(self):
        return {
            'range': self._range,
            'value': self._value,
            'style': self._style,
        }

    @local_gauge_kwargs.setter
    def local_gauge_kwargs(self, kwargs):
        self._range = kwargs.get('range', self._range)
        self._value = kwargs.get('value', self._value)
        self._style = kwargs.get('style', self._style)

    @property
    def kwargs(self):
        value = self.local_gauge_kwargs
        value.update(super(GaugeControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_gauge_kwargs = kwargs
        super(GaugeControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('gauge')

    @property
    def style_attribute_options(self):
        if wx.version().split(' ')[-1] >= '3.1.0':
            return [
                ('wxGA_HORIZONTAL', "Create a horizontal gauge."),
                ('wxGA_VERTICAL', "Create a vertical gauge."),
                ('wxGA_SMOOTH', "Create a smooth progress bar 1 pixel wide update step (not all platforms)."),
                ('wxGA_TEXT', "Displays actual value in percents (only Qt widgets support that, i.e. KDE)."),
                ('wxGA_PROGRESS', "Reflect gauge value in status bar (OSX and Windows 7 and later)."),
            ]
        else:
            return [
                ('wxGA_HORIZONTAL', "Create a horizontal gauge."),
                ('wxGA_VERTICAL', "Create a vertical gauge."),
                ('wxGA_SMOOTH', "Create a smooth progress bar 1 pixel wide update step (not all platforms).")
            ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _base_property, _base_event = super(GaugeControl, self).model_attributes
        _property = [{
            'label': 'Gauge',
            'type': 'category',
            'help': 'Attributes of gauge.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The gauge control name."
                },
                {
                    'label': 'range',
                    'type': 'integer',
                    'value': self._range,
                    'name': 'range',
                    'help': 'The gauge max value.'
                },
                {
                    'label': 'value',
                    'type': 'integer',
                    'value': self._value,
                    'name': 'value',
                    'help': 'Current gauge value.'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the gauge style.'
                }]
        }]
        _event = []

        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.Gauge)(
            edition_frame, self.py_id, self._range, self.py_pos, self.py_size, self.style_value)
        self._edition_window.SetValue(self._value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/gauge.h>
        cls = register_cpp_class(self.project, 'wxGauge')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="gauge control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/gauge.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxGauge({owner}, {self._id}, {self._range}, {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        code += f'{self._name}->SetValue({self._value});\n'
        code += super(GaugeControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.Gauge({owner}, {self.py_id_str}, {self._range}, {self.py_pos_str},' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += f'{this}.SetValue({self._value})\n'
        code += super(GaugeControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

