import wx
from beatle.lib.handlers import identifier
from .._WindowControl import WindowControl


class WizardPage(WindowControl):
    """"This class represents a wizard page.
    This is not really a root class, but it's included with forms, because it acts
    as a container. The more appropiated handling here is the one of pane container."""

    def __init__(self, **kwargs):
        self._bitmap = (0, '')
        super(WizardPage, self).__init__(**kwargs)

    @property
    def local_wizardpage_kwargs(self):
        return {
            'bitmap': self._bitmap
        }

    @local_wizardpage_kwargs.setter
    def local_wizardpage_kwargs(self, kwargs):
        self._bitmap = kwargs.get('bitmap', self._bitmap)


    @property
    def kwargs(self):
        value = self.local_wizardpage_kwargs
        value.update(super(WizardPage, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_wizardpage_kwargs = kwargs
        super(WizardPage, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('wizard_page')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Wizard page',
            'type': 'category',
            'help': 'Attributes of wizard page.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The page instance name."
                },
                {
                    'label': 'bitmap',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._bitmap,
                    'name': 'bitmap',  # kwarg value
                    'help': "The page bitmap."
                },
            ],
        },]
        _event = []
        _base_property, _base_event = super(WizardPage, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def is_sizer_root(self):
        return False

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        from .. import layout
        layout_controls = [layout.WrapSizerControl, layout.StdDialogButtonSizerControl,
                           layout.StaticBoxSizerControl, layout.GridSizerControl,
                           layout.GridBagSizerControl, layout.FlexGridSizerControl,
                           layout.BoxSizerControl]
        try:
            return not next(x for x in layout_controls if len(self[x]) > 0)
        except StopIteration:
            return True

    def create_edition_window(self, parent):
        """For design purposes we use here a wx.Panel instead of wx.adv.WizardPageSimple."""
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.inner_true(lambda x: x.is_top_window)
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        if self._id == 'wxID_ANY':
            control_id = wx.ID_ANY
        else:
            control_id = identifier(self._id)
        if self._pos == (-1, -1):
            control_position = wx.DefaultPosition
        else:
            control_position = self._pos
        if self._size == (-1, -1):
            control_size = wx.DefaultSize
        else:
            control_size = self._size
        try:
            self._edition_window = uiMockupControl(wx.Panel)(
                edition_frame, control_id, control_position, control_size, self.style_value)
            child = self.sorted_wxui_child
            for item in child:
                item.create_edition_window(self)
            # The container here is a SimpleBook
            edition_frame.AddPage(self._edition_window, "", False)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self._edition_window.bind(self)

    def destroy_edition_window(self):
        if self._edition_window is not None:
            reverse_sorted = reversed(self.sorted_wxui_child)
            for child in reverse_sorted:
                child.destroy_edition_window()
            book = self.parent.edition_window_client
            if book is not None:
                index = book.FindPage(self._edition_window)
                if index != wx.NOT_FOUND:
                    book.RemovePage(index)
        super(WizardPage, self).destroy_edition_window()
