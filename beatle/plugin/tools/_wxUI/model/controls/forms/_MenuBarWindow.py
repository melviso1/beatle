from ..._TopWindow import Window


class MenuBarWindow(Window):
    """"This class represents an independent  menu bar"""

    def __init__(self, **kwargs):
        super(MenuBarWindow, self).__init__(**kwargs)

    @property
    def is_top_window(self):
        return True

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('menu_bar')


