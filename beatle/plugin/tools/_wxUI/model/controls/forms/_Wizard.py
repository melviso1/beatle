import wx
from beatle.model import cc, py
from beatle.lib import camel_to_snake
from ..._TopWindow import TopWindow
from ....lib import get_colour_py_text_from_tuple, with_style, with_extra_style

# TODO : Add styling

class Wizard(TopWindow):
    """"This class represents a button"""

    def __init__(self, **kwargs):
        self._title = 'wxWizard'

        #events
        self._on_init_dialog = ''
        self._on_wizard_page_changed = ''
        self._on_wizard_page_changing = ''
        self._on_wizard_cancel = ''
        self._on_wizard_help = ''
        self._on_wizard_finish = ''

        self._wxui_initialize_controls = None           # controls initialization method
        self._wxui_connect_event_handlers = None        # event_handlers method
        self._wxui_disconnect_event_handlers = None     # event_handlers method
        self._controls_folder = None                    # folder for windows controls
        self._event_handlers_folder = None              # folder for event handlers

        super(Wizard, self).__init__(**kwargs)

    @property
    def local_wizard_kwargs(self):
        return {
            'title': self._title,
            'on_wizard_page_changed': self._on_wizard_page_changed,
            '_on_wizard_page_changing': self._on_wizard_page_changing,
            'on_wizard_cancel': self._on_wizard_cancel,
            '_on_wizard_help': self._on_wizard_help,
            'on_wizard_finish': self._on_wizard_finish
        }

    @local_wizard_kwargs.setter
    def local_wizard_kwargs(self, kwargs):
        self._title = kwargs.get('title', self._title)
        self._on_wizard_page_changed = kwargs.get('on_wizard_page_changed', self._on_wizard_page_changed)
        self._on_wizard_page_changing = kwargs.get('_on_wizard_page_changing', self._on_wizard_page_changing)
        self._on_wizard_cancel = kwargs.get('on_wizard_cancel', self._on_wizard_cancel)
        self._on_wizard_help = kwargs.get('_on_wizard_help', self._on_wizard_help)
        self._on_wizard_finish = kwargs.get('on_wizard_finish', self._on_wizard_finish)

    @property
    def kwargs(self):
        value = self.local_wizard_kwargs
        value.update(super(Wizard, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_wizard_kwargs = kwargs
        super(Wizard, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('wizard')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Wizard',
            'type': 'category',
            'help': 'Attributes of Wizard dialog.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The wizard name"
                },
                {
                    'label': 'title',
                    'type': 'string',
                    'read_only': False,
                    'value': self._title,
                    'name': 'title',  # kwarg value
                    'help': 'The window caption'
                },
            ]
        }]
        _event = [{
            'label': 'Wizard',
            'type': 'category',
            'help': 'Events of wizard dialog',
            'child': [
                {
                    'label': 'on init dialog',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_init_dialog,
                    'name': 'on_init_dialog',  # kwarg value
                    'help': "Called when the dialog initialize. Process a wxEVENT_INIT_DIALOG event."
                },
                {
                    'label': 'on wizard page changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_wizard_page_changed,
                    'name': 'on_wizard_page_changed',  # kwarg value
                    'help': "Called when the wizard page changes."
                },
                {
                    'label': 'on wizard page changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_wizard_page_changing,
                    'name': 'on_wizard_page_changing',  # kwarg value
                    'help': "Called when the wizard page is about to change. This event can be vetoed."
                },
                {
                    'label': 'on wizard cancel',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_wizard_cancel,
                    'name': 'on_wizard_cancel',  # kwarg value
                    'help': "Called when the user attempts to cancel the wizard. This event can be vetoed."
                },
                {
                    'label': 'on wizard help',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_wizard_help,
                    'name': 'on_wizard_help',  # kwarg value
                    'help': "Called when the help button was pressed."
                },
                {
                    'label': 'on wizard finish',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_wizard_finish,
                    'name': 'on_wizard_finish',  # kwarg value
                    'help': "Called when the finish button was pressed."
                },

            ]
        }]
        _base_property, _base_event = super(Wizard, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def can_add_menu_bar(self):
        return False

    @property
    def can_add_status_bar(self):
        return False

    @property
    def can_add_tool_bar_control(self):
        return False

    @property
    def can_add_layout_control(self):
        return False

    @property
    def controls_folder(self):
        return self._controls_folder

    @property
    def event_handlers_folder(self):
        return self._event_handlers_folder

    def create_edition_window(self, parent):
        from ....ui import uiFakeWizard
        self._edition_window = uiFakeWizard(parent, **self.kwargs)
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self._edition_window.Layout()
        self._edition_window.Show()
        return self._edition_window

    @property
    def edition_window_client(self):
        """Return the control to be used as client container
        in the mockup visualization"""
        if self._edition_window is None:
            return None
        return self._edition_window.m_content_client

    def implement_cpp(self):
        """Create a c++ class with the sanitized_name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction."""
        sanitized_name = self.name.strip()
        sanitized_name = sanitized_name.replace(' ', '_')
        sanitized_name = sanitized_name.replace('\t', '_')
        self._implementation = cc.Class(
            parent=self.parent,
            read_only=True,
            name=sanitized_name,
            prefix='',
            note="This class implements a frame and was created by wxUI."
        )
        # Add required includes
        self._implementation.user_code_h1 = """#include <wx/frame.h>\n"""

        # Add a constructor that initialize the class

        # We need a wxFrame inheritance
        from ....lib import register_cpp_class
        cc.Inheritance(parent=self._implementation, ancestor=register_cpp_class(self.project, 'wxWizard'))

        # add a controls folder in order to avoid garbage
        self._controls_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )

        self._event_handlers_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )

        ctor = cc.Constructor(
            parent=self._implementation,
            read_only=True,
            note="Constructor for the frame window",
            content="wxui_initialize_controls();\n"
                    "wxui_connect_event_handlers();"
        )
        ctor.auto_args(force=True)
        ctor.auto_init(force=True)

        # Add a window parent default argument
        # cc.Argument(
        #     parent=ctor,
        #     read_only=True,
        #     name='parent',
        #     type=cc.typeinst(type_alias='wxWindow', ptr=True),
        #     default='nullptr'
        # )

        # Add handling methods

        self._wxui_initialize_controls = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_initialize_controls',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for initialize the wizard layout'
        )
        self._wxui_connect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_connect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_disconnect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        cc.Destructor(
            parent=self._implementation,
            read_only=True,
            note="Destructor for the frame window",
            content="wxui_disconnect_event_handlers();"
        )

    def implement_python(self):
        """Create a python class with sanitized name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction."""

        self._implementation = py.Module(
            parent=self._parent,
            read_only=True,
            name='_{class_name}'.format(class_name=camel_to_snake(self._name)),
            note='Module containing classes definitions',
        )
        # We need to import wx
        py.Import(parent=self._implementation, name='wx')
        py.Import(parent=self._implementation, name='wx.adv')

        # If this is python, maybe we are using beatle identifiers. If so,
        # we must import the identifier method:
        # from beatle.lib.handlers import identifier
        if self.document.use_beatle_identifiers:
            kwargs = {
                'parent': self._implementation,
                'name': 'beatle.lib.handlers'
            }
            py.Import(**kwargs)

        self.implement_python_identifier()

        the_class = py.Class(
            parent=self._implementation,
            raw=True,  # prevent automatic object inheritance
            read_only=True,
            name=self._name,
            note=self.python_description or "This class implements a wizard dialog and was created by wxUI."
        )
        # add inheritance
        py.Inheritance(parent=the_class, name='wx.adv.Wizard', ancestor=None)
        # add a controls folder in order to avoid garbage
        self._controls_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )
        self._event_handlers_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )
        init_content = \
            "super({name},self).__init__(parent, id={id_}, title = '{title}', pos={pos}, size={size}, " \
            "style={style})\n{window_init}" \
            "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
            "self.wxui_initialize_controls()\n" \
            "self.wxui_connect_event_handlers()\n".format(
                name=self._name, id_=self.py_id, pos=self.py_pos, size=self.py_size, style=self.py_style_str,
                title=self._title, window_init=super(Wizard, self).python_init_control_code())
        init_content += 'self.Layout()\n'

        ctor = py.InitMethod(
            parent=the_class,
            read_only=True,
            note="Constructor for the wizard dialog",
            content=init_content
        )
        py.Argument(
            parent=ctor,
            name='parent',
            default='None'
        )
        self._wxui_initialize_controls = py.MemberMethod(
            parent=the_class,
            name='wxui_initialize_controls',
            read_only=True,
            note='This method is responsible for initialize the form layout'
        )
        self._wxui_connect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_connect_event_handlers',
            read_only=True,
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_disconnect_event_handlers',
            read_only=True,
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        py.MemberMethod(
            parent=the_class,
            name='delete',
            read_only=True,
            note="Destructor for the wizard dialog",
            content="self.wxui_disconnect_event_handlers()"
        )

    #@DelayedMethod()
    def update_implementation(self):
        """This method is invoked under transaction from set_kwargs
        in order to update the code that create the interface"""
        language = self.project.language
        if language == 'c++':
            self.update_cpp_implementation()
        elif language == 'python':
            self.update_python_implementation()

    def update_cpp_init_code(self):
        """This method update the cpp implementation. It's
        called under transaction."""
        if self._implementation is None:
            wx.LogError("Unexpected missing wxUI Frame implementation for update")
            return

        # Update required includes
        self.add_cpp_required_header('#include <wx/frame.h>')

        if self._wxui_initialize_controls is None:
            wx.LogError("Unexpected missing wxUI implementation methods")
            return
        self._wxui_initialize_controls.save_state()

        from ..bar import MenuBarControl, ToolBarControl
        # Ok, generate the code from scratch

        code = 'SetSizeHints(wxDefaultSize, wxDefaultSize);\n' \
               'SetLabel("{self._title}");\n'.format(self=self)
        if self._background_colour[0]:
            if type(self._background_colour[1]) is wx.SystemColour:
                from ....lib import cpp_syscolour_name
                name = cpp_syscolour_name(self._background_colour[1])
                if name is not None:
                    code += 'SetBackgroundColour(wxSystemSettings::GetColour({name}));\n'.format(name=name)
                    self.add_cpp_required_header("#include <wx/settings.h>")
            else:
                code += 'SetBackgroundColour(wxColour{colour});\n'.format(colour=str(self._background_colour[1]))

        for menu_bar in self[MenuBarControl]:  # really no more than one
            code += menu_bar.cpp_init_code()

        for tool_bar in self[ToolBarControl]:  # really no more than one
            code += tool_bar.cpp_init_code()

        # ...
        self._wxui_initialize_controls.content = code

    def update_python_init_code(self):
        if self._implementation is None:
            wx.LogError("Unexpected missing wxUI Frame implementation for update")
            return
        _class = self._implementation[py.Class][0]
        ctor = _class(py.InitMethod, filter=lambda x: x.parent == _class)
        if len(ctor) == 0:
            return
        ctor = ctor[0]
        init_content = \
            "super({name},self).__init__(parent, id={id_}, title = '{title}', pos={pos}, size={size}, " \
            "style={style})\n{window_init}" \
            "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
            "self.wxui_initialize_controls()\n" \
            "self.wxui_connect_event_handlers()\n".format(
                name=self._name, id_=self.py_id, pos=self.py_pos, size=self.py_size, style=self.py_style_str,
                title=self._title, window_init=super(Wizard, self).python_init_control_code())
        init_content += 'self.Layout()\n'
        ctor.set_content(init_content)
        ctor.auto_init()
        self._wxui_initialize_controls.set_content(self.python_init_control_code())

    def update_cpp_implementation(self):
        self.update_cpp_init_code()
        self.update_cpp_event_handlers()
        self.update_cpp_connect_event()
        self.update_cpp_disconnect_event()
        super(Wizard, self).update_cpp_implementation()

    def update_python_implementation(self):
        """This method update the cpp implementation. It's
        called under transaction."""
        self.update_python_init_code()
        self.update_python_event_handlers()
        self.update_python_connect_event()
        self.update_python_disconnect_event()
        super(Wizard, self).update_python_implementation()

    def save_state(self):
        """Utility for saving state"""
        if self._implementation is not None:
            self._implementation.save_state()
        super(Wizard, self).save_state()
