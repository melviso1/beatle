"""
    Frame windows
    =============
    A frame window behaves as container for menus and toolbars (not even) and containers (not speaking about aui)
    The frames are an important class with some key points:

        * Triggers the construction of the visual environment.
        * Create the code implementation

    These keys are shared for all the top_main windows. These windows are found by outer is_top_window attribute search.

    Code implementation details
    ===========================
    Every frame, or better, each is_top_window == True object generates a class whose name derives from sanitized
    object name. These classes among C++ and python has the structure:

        * `implement` (construct structure): Called on init for ensuring the containers.
        * `update_implementation` (update structure): Called when the design evolves.

        implement function
        ======================
        Each model object call `implement` method at the end of the initialization.

        `implement` method  is defined at wxUICommon, the serializable base for the model.
                    the definition is to call, depending on language to

                        'implement_cpp` that must defined for c++ code implementation, or
                        'implement_python' : that do same for python counterpart.

        The language specific implmentation functions create a similar structure:

            + includes
            + class
                    + controls folder
                    + event handlers folder
                    + constructor (calling the next two)
                    * control_initialization method
                    * event_handler_connection method
                    * event_handler_disconnection method
                    * destructor (calling the first)

            details :
                for python the destructor must be a special function 'delete'

        Be aware that no dynamic code is generated at this phase, os exception of constructor
        and destructor methods.

        for other serializable model objects, the specific implement becomes a call to

            * declare variable method : `declare_cpp_variable` or `declare_python_variable` that creates the
                                        required variable inside the controls folder.

            * update implementation method: 'update_python_implementation' or 'update_cpp_implementation'

        details: The declare_python_variable has a default for Window, that create a sanitized object name
        instance variable inside the controls folder of the inner top_window class.

        update_implementation method
        ============================
        Called at several places, this method is a strong candidate to be a delayed method, for safety, but
        this requires more analysis as delayed methods are executed until now inside the transaction commit
        and this lead to missing ui updates and out-of-transaction context. This is still a weak point that
        must be revisited.

        The default implementation of this method, at wxUICommon is to call the override of inner is_top_window.
        The mission of this function is to efectively create the content code.
        The implementation, depending on language, is translated to 'update_python_implementation' and
        'update_cpp_implementation'.
        Each one of these do three things:
            -update the code of control_initialization methods : 'update_cpp_init_code' and 'update_python_init_code'
            -update the event handlers: 'update_python_event_handlers' and 'update_cpp_event_handlers'
            -update the code of event_handler_connection and event_handler_disconnection methods (TODO)

        update control_initialization methods
        =====================================
        They are basically nested string constructor for the operations required for frame window creation.
        The calls relay on cpp_init_code nested calls for c++ and python_init_code, despite some details
        specific to python, the update of this method is done by recurse on init control code methods that
        are python_init_control_code and cpp_init_code respectively.

        These code is responsible for doing the construction.

        python_init_control_code :
                receives a string argument that would be required by some nested constructs,
                like the subitems from a menu.

"""


import wx
from beatle.model import cc, py
from beatle.lib import camel_to_snake
from ..._TopWindow import TopWindow
from ....lib import get_colour_py_text_from_tuple, with_style, with_extra_style


@with_style
@with_extra_style
class Frame(TopWindow):

    """"This class represents a frame window"""
    style_list = (wx.CAPTION, wx.CLOSE_BOX, wx.DEFAULT_FRAME_STYLE, wx.FRAME_FLOAT_ON_PARENT,  wx.FRAME_NO_TASKBAR,
                  wx.FRAME_SHAPED, wx.FRAME_TOOL_WINDOW, wx.ICONIZE, wx.MAXIMIZE, wx.MAXIMIZE_BOX, wx.MINIMIZE,
                  wx.MINIMIZE_BOX, wx.RESIZE_BORDER, wx.STAY_ON_TOP, wx.SYSTEM_MENU)
    style_list_py_text = ('wx.CAPTION', 'wx.CLOSE_BOX', 'wx.DEFAULT_FRAME_STYLE', 'wx.FRAME_FLOAT_ON_PARENT',
                          'wx.FRAME_NO_TASKBAR', 'wx.FRAME_SHAPED', 'wx.FRAME_TOOL_WINDOW', 'wx.ICONIZE',
                          'wx.MAXIMIZE', 'wx.MAXIMIZE_BOX', 'wx.MINIMIZE', 'wx.MINIMIZE_BOX', 'wx.RESIZE_BORDER',
                          'wx.STAY_ON_TOP', 'wx.SYSTEM_MENU')
    style_list_cc_text = ('wxCAPTION', 'wxCLOSE_BOX', 'wxDEFAULT_FRAME_STYLE', 'wxFRAME_FLOAT_ON_PARENT',
                          'wxFRAME_NO_TASKBAR', 'wxFRAME_SHAPED', 'wxFRAME_TOOL_WINDOW', 'wxICONIZE',
                          'wxMAXIMIZE', 'wxMAXIMIZE_BOX', 'wxMINIMIZE', 'wxMINIMIZE_BOX', 'wxRESIZE_BORDER',
                          'wxSTAY_ON_TOP', 'wxSYSTEM_MENU')

    extra_style_list = (wx.FRAME_EX_CONTEXTHELP, wx.FRAME_EX_METAL)
    extra_style_list_py_text = ('wx.FRAME_EX_CONTEXTHELP', 'wx.FRAME_EX_METAL')

    def __init__(self, **kwargs):
        # -- initialize data with default values
        self._title = 'wxFrame'
        self._style = 4
        self._extra_style = 0
        self._center = 0
        self._aui_managed = False
        self._aui_manager_style = 0

        self._wxui_initialize_controls = None           # controls initialization method
        self._wxui_connect_event_handlers = None        # event_handlers method
        self._wxui_disconnect_event_handlers = None     # event_handlers method
        self._controls_folder = None                    # folder for windows controls
        self._event_handlers_folder = None              # folder for event handlers

        super(Frame, self).__init__(**kwargs)

    @property
    def local_frame_kwargs(self):
        return {
            'title': self._title,
            'style': self._style,
            'extra_style': self._extra_style,
            'center': self._center,
            'aui_managed': self._aui_managed,
            'aui_manager_style': self._aui_manager_style,
        }

    @local_frame_kwargs.setter
    def local_frame_kwargs(self, kwargs):
        self._title = kwargs.get('title', self._title)
        self._style = kwargs.get('style', self._style)
        self._extra_style = kwargs.get('extra_style', self._extra_style)
        self._center = kwargs.get('center', self._center)
        self._aui_managed = kwargs.get('aui_managed', self._aui_managed)
        self._aui_manager_style = kwargs.get('aui_manager_style', self._aui_manager_style)

    @property
    def kwargs(self):
        value = self.local_frame_kwargs
        value.update(super(Frame, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_frame_kwargs = kwargs
        super(Frame, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            if self.project.language == 'python':
                self._implementation.name = '_{class_name}'.format(class_name=camel_to_snake(self._name))
                class_ = self._implementation[py.Class][0]
                class_.save_state()
                class_.name = self._name
            else:  # c++
                self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('frame')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Frame',
            'type': 'category',
            'help': 'Attributes of frame.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The frame name"
                },
                {
                    'label': 'title',
                    'type': 'string',
                    'read_only': False,
                    'value': self._title,
                    'name': 'title',  # kwarg value
                    'help': 'The window caption'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',  # kwarg key
                    'help': 'The frame style',
                    'options': [
                        ('wxCAPTION', 'Show the window caption'),
                        ('wxCLOSE_BOX', 'Show the window close button'),
                        ('wxDEFAULT_FRAME_STYLE', 'This is a combination of the styles used frequently:\n' \
                                                  'wxMIMIMIZE_BOX, wxMXIMIZE_BOX, wxRESIZE_BORDER, wxSYSTEM_MENU, \n' \
                                                  'wxCAPTION, wxCLOSE_BOX and wxCLIP_CHILDREN'),
                        ('wxFRAME_FLOAT_ON_PARENT', 'This frame will be always on top of his parent (required).'),
                        ('wxFRAME_NO_TASKBAR', 'Prevent the frame will be shown in the task bar.'),
                        ('wxFRAME_SHAPED', ''),
                        ('wxFRAME_TOOL_WINDOW', ''),
                        ('wxICONIZE', ''),
                        ('wxMAXIMIZE', ''),
                        ('wxMAXIMIZE_BOX', ''),
                        ('wxMINIMIZE', ''),
                        ('wxMINIMIZE_BOX', ''),
                        ('wxRESIZE_BORDER', ''),
                        ('wxSTAY_ON_TOP', ''),
                        ('wxSYSTEM_MENU', '')
                    ]
                },
                {
                    'label': 'extra style',
                    'type': 'multi_choice',
                    'value': self._extra_style,
                    'name': 'extra_style',  # kwarg key
                    'options': [
                        ('wxFRAME_EX_CONTEXTHELP', ''),
                        ('wxFRAME_EX_METAL', '')
                    ]
                },
                {
                    'label': 'center',
                    'type': "enum",
                    'value': self._center,
                    'name': 'center',
                    'help': 'Center the dialog in the display',
                    'options': [
                        '', 'wxBOTH', 'wxHORIZONTAL', 'wxVERTICAL'
                    ]
                },
                {
                    'label': 'aui managed',
                    'type': 'boolean',
                    'value': self._aui_managed,
                    'name': 'aui_managed'
                },
                {
                    'label': 'manager style',
                    'type': 'multi_choice',
                    'value': self._aui_manager_style,
                    'name': 'aui_manager_style',
                    'options': [
                        ('wxAUI_MGR_ALLOW_ACTIVE_PANE', ''),
                        ('wxAUI_MGR_ALLOW_FLOATING', ''),
                        ('wxAUI_MGR_DEFAULT', ''),
                        ('wxAUI_MGR_HINT_FADE', ''),
                        ('wxAUI_MGR_LIVE_RESIZE', ''),
                        ('wxAUI_MGR_NO_VENETIAN_BLINDS_FADE', ''),
                        ('wxAUI_MGR_RECTANGLE_HINT', ''),
                        ('wxAUI_MGR_TRANSPARENT_DRAG', ''),
                        ('wxAUI_MGR_TRANSPARENT_HINT', ''),
                        ('wxAUI_MGR_VENETIAN_BLINDS_HINT', '')
                    ]
                }
            ]
        }]
        _event = []
        _base_property, _base_event = super(Frame, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def can_add_menu_bar(self):
        from .. import bar
        return (self._edition_window is not None) and len(self[bar.MenuBarControl]) == 0

    @property
    def can_add_status_bar(self):
        from .. import bar
        return (self._edition_window is not None) and len(self[bar.StatusBarControl]) == 0

    @property
    def can_add_tool_bar_control(self):
        from .. import bar
        return (self._edition_window is not None) and len(self[bar.ToolBarControl]) == 0 and \
            len(self[bar.AuiToolBarControl]) == 0

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        from .. import layout
        layout_controls = [layout.WrapSizerControl, layout.StdDialogButtonSizerControl,
                           layout.StaticBoxSizerControl, layout.GridSizerControl,
                           layout.GridBagSizerControl, layout.FlexGridSizerControl,
                           layout.BoxSizerControl]
        try:
            return not next(x for x in layout_controls if len(self[x]) > 0)
        except StopIteration:
            return True

    @property
    def controls_folder(self):
        return self._controls_folder

    @property
    def event_handlers_folder(self):
        return self._event_handlers_folder

    @property
    def connect_event_handlers(self):
        return self._wxui_connect_event_handlers

    @property
    def disconnect_event_handlers(self):
        return self._wxui_disconnect_event_handlers

    def create_edition_window(self, parent):
        from ....ui import uiFakeForm
        self._edition_window = uiFakeForm(parent, self)
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self._edition_window.Layout()
        self._edition_window.Show()
        return self._edition_window

    @property
    def edition_window_client(self):
        """Return the control to be used as client container
        in the mockup visualization"""
        if self._edition_window is None:
            return None
        return self._edition_window.m_content_client

    def create_menu_bar(self, model):
        if self._edition_window is None:
            return None
        return self._edition_window.create_menu_bar(model)

    def create_status_bar(self):
        if self._edition_window is None:
            return None
        return self._edition_window.create_status_bar()

    def implement_cpp(self):
        """Create a c++ class with sanitized_name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction."""
        self._implementation = cc.Class(
            parent=self.parent,
            read_only=True,
            name=self._name,
            prefix='',
            note="This class implements a frame and was created by wxUI."
        )
        # Add required includes
        self._implementation.user_code_h1 = """#include <wx/frame.h>\n"""

        # Add a constructor that initialize the class

        # We need a wxFrame inheritance
        from ....lib import register_cpp_class
        cc.Inheritance(parent=self._implementation, ancestor=register_cpp_class(self.project, 'wxFrame'))

        # add a controls folder in order to avoid garbage
        self._controls_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )
        self._event_handlers_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )

        ctor = cc.Constructor(
            parent=self._implementation,
            read_only=True,
            note="Constructor for the frame window",
            content="wxui_initialize_controls();\n"
                    "wxui_connect_event_handlers();"
        )
        ctor.auto_args(force=True)
        ctor.auto_init(force=True)
        ctor[cc.Argument][2].default = '"wxFrame"'

        self._wxui_initialize_controls = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_initialize_controls',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for initialize the form layout'
        )
        self._wxui_connect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_connect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_disconnect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        cc.Destructor(
            parent=self._implementation,
            read_only=True,
            note="Destructor for the frame window",
            content="wxui_disconnect_event_handlers();"
        )

    def implement_python(self):
        """Create a python class with sanitized name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction.
        Modification on 14/02/23: We must create the python class inside
        a module because of imports. Thus, implementation will become
        a module."""
        self._implementation = py.Module(
            parent=self._parent,
            read_only=True,
            name='_{class_name}'.format(class_name=camel_to_snake(self._name)),
            note='Module containing classes definitions',
        )
        # We need to import wx
        py.Import(parent=self._implementation, name='wx')

        # If this is python, maybe we are using beatle identifiers. If so,
        # we must import the identifier method:
        # from beatle.lib.handlers import identifier
        if self.document.use_beatle_identifiers:
            kwargs = {
                'parent': self._implementation,
                'name': 'beatle.lib.handlers',
            }
            py.Import(**kwargs)

        self.implement_python_identifier()

        the_class = py.Class(
            parent=self._implementation,
            raw=True,  # prevent automatic object inheritance
            read_only=True,
            name=self._name,
            note=self.python_description or "This class implements a frame and was created by wxUI."
        )
        # add inheritance
        py.Inheritance(parent=the_class, name='wx.Frame', ancestor=None)

        # add a controls folder in order to avoid garbage
        self._controls_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )
        self._event_handlers_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )
        init_content = \
            "super({name},self).__init__(parent, id={id_}, title = '{title}', pos={pos}, size={size}, " \
            "style={style})\n{window_init}" \
            "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
            "self.wxui_initialize_controls()\n" \
            "self.wxui_connect_event_handlers()\n".format(
                name=self._name, id_=self.py_id, pos=self.py_pos, size=self.py_size, style=self.py_style_str,
                title=self._title, window_init=super(Frame, self).python_init_control_code())
        if self._center:
            center_flag = {1: 'wx.BOTH', 2: 'wx.HORIZONTAL', 3: 'wx.VERTICAL'}
            init_content += 'self.Centre({flag})\n'.format(flag=center_flag[self._center])
        init_content += 'self.Layout()\n'
        ctor = py.InitMethod(
            parent=the_class,
            read_only=True,
            note="Constructor for the frame window",
            content=init_content
        )
        py.Argument(
            parent=ctor,
            name='parent',
            default='None'
        )
        self._wxui_initialize_controls = py.MemberMethod(
            parent=the_class,
            name='wxui_initialize_controls',
            read_only=True,
            note='This method is responsible for initialize the form layout'
        )
        self._wxui_connect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_connect_event_handlers',
            read_only=True,
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_disconnect_event_handlers',
            read_only=True,
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        py.MemberMethod(
            parent=the_class,
            name='delete',
            read_only=True,
            note="Destructor for the frame window",
            content="self.wxui_disconnect_event_handlers()"
        )

    #@DelayedMethod()
    def update_implementation(self):
        """This method is invoked under transaction from set_kwargs
        in order to update the code that create the interface"""
        language = self.project.language
        if language == 'c++':
            self.update_cpp_implementation()
        elif language == 'python':
            self.update_python_implementation()

    def update_cpp_implementation(self):
        self.update_cpp_init_code()
        self.update_cpp_event_handlers()
        self.update_cpp_connect_event()
        self.update_cpp_disconnect_event()
        super(Frame, self).update_cpp_implementation()

    def update_python_implementation(self):
        """This method update the cpp implementation."""
        self.update_python_init_code()
        self.update_python_event_handlers()
        self.update_python_connect_event()
        self.update_python_disconnect_event()
        super(Frame, self).update_python_implementation()

    def update_cpp_init_code(self):
        """This method update the cpp implementation. It's
        called under transaction."""
        if self._implementation is None:
            wx.LogError("Unexpected missing wxUI Frame implementation for update")
            return
        self.add_cpp_required_header('#include <wx/frame.h>')
        ctor = self._implementation[cc.Constructor]
        # update the initialization variables
        if len(ctor) > 0:
            ctor = ctor[0]
            #update also the ctor argument defaults
            parent, windowId, caption, pos, size, style, unused = tuple(ctor[cc.Argument])
            if self._id and windowId.default != self._id:
                windowId.save_state()
                windowId.default = str(self._id)
            if self._title and '"{}"'.format(self._title) != caption.default:
                caption.save_state()
                caption.default = '"{}"'.format(self._title)
            pos_str = self.cc_pos_str
            if pos.default != pos_str:
                pos.save_state()
                pos.default = self.cc_pos_str
            size_str = self.cc_size_str
            if size.default != size_str:
                size.save_state()
                size.default = self.cc_size_str
            style_str = self.cc_style_str
            if style.default != style_str:
                style.save_state()
                style.default = style_str
            ctor.save_state()
            ctor.auto_init(force=True)

        if self._wxui_initialize_controls is None:
            wx.LogError("Unexpected missing wxUI implementation methods")
            return
        self._wxui_initialize_controls.save_state()

        from ..bar import MenuBarControl, ToolBarControl, StatusBarControl
        # Ok, generate the code from scratch

        code = 'SetSizeHints(wxDefaultSize, wxDefaultSize);\n'
        wx_setting_h_required = False
        if self._foreground_colour[0]:
            if type(self._foreground_colour[1]) is wx.SystemColour:
                from ....lib import cpp_syscolour_name
                name = cpp_syscolour_name(self._foreground_colour[1])
                if name is not None:
                    code += 'SetForegroundColour(wxSystemSettings::GetColour({name}));\n'.format(name=name)
                    wx_setting_h_required = True
            else:
                code += 'SetForegroundColour(wxColour{colour});\n'.format(colour=str(self._foreground_colour[1]))
        if self._background_colour[0]:
            if type(self._background_colour[1]) is wx.SystemColour:
                from ....lib import cpp_syscolour_name
                name = cpp_syscolour_name(self._background_colour[1])
                if name is not None:
                    code += 'SetBackgroundColour(wxSystemSettings::GetColour({name}));\n'.format(name=name)
                    wx_setting_h_required = True
            else:
                code += 'SetBackgroundColour(wxColour{colour});\n'.format(colour=str(self._background_colour[1]))
        if wx_setting_h_required:
            self.add_cpp_required_header("#include <wx/settings.h>")

        controls = self.sorted_wxui_child
        # first, search if some child is of type menubar
        menu_bar_list = [(index, element) for (index, element) in enumerate(controls)
                         if type(element) is MenuBarControl]
        if len(menu_bar_list):
            index, menu_bar = menu_bar_list[0]
            code += menu_bar.cpp_init_code()
            del controls[index]
        # next search for a status bar, if any
        status_bar_code = ''
        status_bar_list = [(index, element) for (index, element) in enumerate(controls)
                           if type(element) is StatusBarControl]
        if len(status_bar_list):
            index, status_bar = status_bar_list[0]
            status_bar_code = status_bar.cpp_init_code()
            del controls[index]

        for control in controls:
            code += control.cpp_init_code()

        code += status_bar_code
        code += 'Layout();\n'

        if self._center:
            center_flag = {1: 'wxBOTH', 2: 'wxHORIZONTAL', 3: 'wxVERTICAL'}
            code += 'Centre({flag});\n'.format(flag=center_flag[self._center])

        # falta centrar
        self._wxui_initialize_controls.content = code

    def update_python_init_code(self):
        _class = self._implementation[py.Class][0]
        ctor = _class(py.InitMethod, filter=lambda x: x.parent == _class)
        if len(ctor) == 0:
            return
        ctor = ctor[0]

        init_content = \
            "super({name},self).__init__(parent, id={id_}, title = '{title}', pos={pos}, size={size}, " \
            "style={style})\n{window_init}" \
            "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
            "self.wxui_initialize_controls()\n" \
            "self.wxui_connect_event_handlers()\n".format(
                name=self._name, id_=self.py_id, pos=self.py_pos, size=self.py_size, style=self.py_style_str,
                title=self._title, window_init=super(Frame, self).python_init_control_code())
        if self._center:
            center_flag = {1: 'wx.BOTH', 2: 'wx.HORIZONTAL', 3: 'wx.VERTICAL'}
            init_content += 'self.Centre({flag})\n'.format(flag=center_flag[self._center])
        init_content += 'self.Layout()\n'
        ctor.set_content(init_content)
        ctor.auto_init()
        self._wxui_initialize_controls.set_content(self.python_init_control_code())

    def python_init_control_code(self, parent='self'):
        """This is a special case of implementing the code,
        because the code is not intended to create a variable
        but to initialize child."""
        from ..bar import MenuBarControl, StatusBarControl
        new_code = ''
        controls = self.sorted_wxui_child
        # first, search if some child is of type menubar
        menu_bar_list = [(index, element) for (index, element) in enumerate(controls)
                         if type(element) is MenuBarControl]
        if len(menu_bar_list):
            index, menu_bar = menu_bar_list[0]
            new_code += menu_bar.python_init_control_code()
            del controls[index]
        # next search for a status bar, if any
        status_bar_code = ''
        status_bar_list = [(index, element) for (index, element) in enumerate(controls)
                           if type(element) is StatusBarControl]
        if len(status_bar_list):
            index, status_bar = status_bar_list[0]
            status_bar_code = status_bar.python_init_control_code()
            del controls[index]
        # then, the next of the code
        for control in controls:
            new_code += control.python_init_control_code()
        new_code += status_bar_code
        new_code += 'self.Layout()\n'
        return new_code

    def save_state(self):
        """Utility for saving state"""
        if self._implementation is not None:
            self._implementation.save_state()
        super(Frame, self).save_state()

    def toolbar_parent_and_sizer(self, **kwargs):
        frame = self._edition_window
        if frame is None:
            return None, None
        return frame.toolbar_parent_and_sizer(**kwargs)

