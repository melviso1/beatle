import wx.core
from beatle.model import cc, py
from beatle.lib import camel_to_snake
from ..._TopWindow import TopWindow
from ....lib import get_colour_py_text_from_tuple, with_style, with_extra_style


@with_style
@with_extra_style
class Dialog(TopWindow):

    """"This class represents a dialog"""
    style_list = (wx.CAPTION, wx.CLOSE_BOX, wx.DEFAULT_DIALOG_STYLE, wx.DIALOG_NO_PARENT, wx.MAXIMIZE_BOX,
                  wx.MINIMIZE_BOX, wx.RESIZE_BORDER, wx.STAY_ON_TOP, wx.SYSTEM_MENU)
    style_list_py_text = ('wx.CAPTION', 'wx.CLOSE_BOX', 'wx.DEFAULT_DIALOG_STYLE', 'wx.DIALOG_NO_PARENT',
                          'wx.MAXIMIZE_BOX', 'wx.MINIMIZE_BOX', 'wx.RESIZE_BORDER', 'wx.STAY_ON_TOP', 'wx.SYSTEM_MENU')
    style_list_cc_text = ('wxCAPTION', 'wxCLOSE_BOX', 'wxDEFAULT_DIALOG_STYLE', 'wxDIALOG_NO_PARENT',
                          'wxMAXIMIZE_BOX', 'wxMINIMIZE_BOX', 'wxRESIZE_BORDER', 'wxSTAY_ON_TOP', 'wxSYSTEM_MENU')

    extra_style_list = (wx.DIALOG_EX_CONTEXTHELP, wx.DIALOG_EX_METAL)
    extra_style_list_py_text = ('wx.DIALOG_EX_CONTEXTHELP', 'wx.DIALOG_EX_METAL')

    def __init__(self, **kwargs):
        # -- initialize data with default values
        self._title = 'wx.Dialog'
        self._style = 4
        self._extra_style = 0
        self._center = 0
        self._aui_managed = False
        self._aui_manager_style = 0

        # -- initialize events with default values
        self._on_init_dialog = ''

        self._wxui_initialize_controls = None           # controls initialization method
        self._wxui_connect_event_handlers = None        # event_handlers method
        self._wxui_disconnect_event_handlers = None     # event_handlers method
        self._controls_folder = None                    # folder for windows controls
        self._event_handlers_folder = None              # folder for event handlers

        super(Dialog, self).__init__(**kwargs)

    @property
    def local_dialog_kwargs(self):
        return {
            'title': self._title,
            'style': self._style,
            'extra_style': self._extra_style,
            'center': self._center,
            'aui_managed': self._aui_managed,
            'aui_manager_style': self._aui_manager_style,
            'on_init_dialog': self._on_init_dialog,
        }

    @local_dialog_kwargs.setter
    def local_dialog_kwargs(self, kwargs):
        self._title = kwargs.get('title', self._title)
        self._style = kwargs.get('style', self._style)
        self._extra_style = kwargs.get('extra_style', self._extra_style)
        self._center = kwargs.get('center', self._center)
        self._aui_managed = kwargs.get('aui_managed', self._aui_managed)
        self._aui_manager_style = kwargs.get('aui_manager_style', self._aui_manager_style)
        self._on_init_dialog = kwargs.get('on_init_dialog', self._on_init_dialog)

    @property
    def local_dialog_events(self):
        return {
            'on_init_dialog': (self._on_init_dialog, 'wx.EVT_INIT_DIALOG', 'wxEVT_INIT_DIALOG')
        }

    @property
    def events(self):
        value = self.local_dialog_events
        value.update(super(Dialog, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_dialog_kwargs
        value.update(super(Dialog, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_dialog_kwargs = kwargs
        super(Dialog, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            if self.project.language == 'python':
                self._implementation.name = '_{class_name}'.format(class_name=camel_to_snake(self._name))
                class_ = self._implementation[py.Class][0]
                class_.save_state()
                class_.name = self._name
            else:  # c++
                self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('dialog')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Dialog',
            'type': 'category',
            'help': 'Attributes of dialog.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The dialog name"
                },
                {
                    'label': 'title',
                    'type': 'string',
                    'read_only': False,
                    'value': self._title,
                    'name': 'title',  # kwarg value
                    'help': 'The window caption'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',  # kwarg key
                    'help': 'The dialog style',
                    'options': [
                        ('wxCAPTION', 'Show the window caption'),
                        ('wxCLOSE_BOX', 'Show the window close button'),
                        ('wxDEFAULT_DIALOG_STYLE', 'This is a combination of the styles used frequently:\n' \
                                                   'wxCAPTION, wxCLOSE_BOX and  wxSYSTEM_MENU.'),
                        ('wxDIALOG_NO_PARENT', 'Not recommended for modal dialog. Create a orphan dialog,\n' \
                                               'preventing the main application window gets parent of the \n' \
                                               'dialog by default.'),
                        ('wxMAXIMIZE_BOX', ''),
                        ('wxMINIMIZE_BOX', ''),
                        ('wxRESIZE_BORDER', ''),
                        ('wxSTAY_ON_TOP', ''),
                        ('wxSYSTEM_MENU', '')
                    ]
                },
                {
                    'label': 'extra style',
                    'type': 'multi_choice',
                    'value': self._extra_style,
                    'name': 'extra_style',  # kwarg key
                    'options': [
                        ('wxDIALOG_EX_CONTEXTHELP', ''),
                        ('wxDIALOG_EX_METAL', '')
                    ]
                },
                {
                    'label': 'center',
                    'type': "enum",
                    'value': self._center,
                    'name': 'center',
                    'help': 'Center the dialog in the display',
                    'options': [
                        '', 'wxBOTH', 'wxHORIZONTAL', 'wxVERTICAL'
                    ]
                },
                {
                    'label': 'aui managed',
                    'type': 'boolean',
                    'value': self._aui_managed,
                    'name': 'aui_managed'
                },
                {
                    'label': 'manager style',
                    'type': 'multi_choice',
                    'value': self._aui_manager_style,
                    'name': 'aui_manager_style',
                    'options': [
                        ('wxAUI_MGR_ALLOW_ACTIVE_PANE', ''),
                        ('wxAUI_MGR_ALLOW_FLOATING', ''),
                        ('wxAUI_MGR_DEFAULT', ''),
                        ('wxAUI_MGR_HINT_FADE', ''),
                        ('wxAUI_MGR_LIVE_RESIZE', ''),
                        ('wxAUI_MGR_NO_VENETIAN_BLINDS_FADE', ''),
                        ('wxAUI_MGR_RECTANGLE_HINT', ''),
                        ('wxAUI_MGR_TRANSPARENT_DRAG', ''),
                        ('wxAUI_MGR_TRANSPARENT_HINT', ''),
                        ('wxAUI_MGR_VENETIAN_BLINDS_HINT', '')
                    ]
                }
            ]
        }]
        _event = [{
            'label': 'Dialog',
            'type': 'category',
            'help': 'Events of dialog window',
            'child': [{
                'label': 'on_init_dialog',
                'type': 'string',
                'read_only': False,
                'value': self._on_init_dialog,
                'name': 'on_init_dialog',  # kwarg value
                }]}]
        _base_property, _base_event = super(Dialog, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        from .. import layout
        layout_controls = [layout.WrapSizerControl, layout.StdDialogButtonSizerControl,
                           layout.StaticBoxSizerControl, layout.GridSizerControl,
                           layout.GridBagSizerControl, layout.FlexGridSizerControl,
                           layout.BoxSizerControl]
        try:
            return not next(x for x in layout_controls if len(self[x]) > 0)
        except StopIteration:
            return True

    @property
    def controls_folder(self):
        return self._controls_folder

    @property
    def event_handlers_folder(self):
        return self._event_handlers_folder

    @property
    def connect_event_handlers(self):
        return self._wxui_connect_event_handlers

    @property
    def disconnect_event_handlers(self):
        return self._wxui_disconnect_event_handlers

    def create_edition_window(self, parent):
        from ....ui import uiFakeDialog
        self._edition_window = uiFakeDialog(parent, **self.kwargs)
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self._edition_window.Layout()
        self._edition_window.Show()
        return self._edition_window

    @property
    def edition_window_client(self):
        """Return the control to be used as client container
        in the mockup visualization"""
        if self._edition_window is None:
            return None
        return self._edition_window.m_content_client

    def implement_cpp(self):
        """Create a c++ class with the sanitized_name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction."""
        sanitized_name = self.name.strip()
        sanitized_name = sanitized_name.replace(' ', '_')
        sanitized_name = sanitized_name.replace('\t', '_')
        self._implementation = cc.Class(
            parent=self.parent,
            read_only=True,
            name=sanitized_name,
            prefix='',
            note="This class implements a dialog and was created by wxUI."
        )
        # Add required includes
        self._implementation.user_code_h1 = """#include <wx/dialog.h>\n"""

        # Add a constructor that initialize the class

        # We need a wxFrame inheritance
        from ....lib import register_cpp_class
        cc.Inheritance(parent=self._implementation, ancestor=register_cpp_class(self.project, 'wxDialog'))

        # add a controls folder in order to avoid garbage
        self._controls_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )

        self._event_handlers_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )

        ctor = cc.Constructor(
            parent=self._implementation,
            read_only=True,
            note="Constructor for the frame window",
            content="wxui_initialize_controls();\n"
                    "wxui_connect_event_handlers();"
        )
        ctor.auto_args(force=True)
        ctor.auto_init(force=True)

        # Add a window parent default argument
        # cc.Argument(
        #     parent=ctor,
        #     read_only=True,
        #     name='parent',
        #     type=cc.typeinst(type_alias='wxWindow', ptr=True),
        #     default='nullptr'
        # )

        # Add handling methods

        self._wxui_initialize_controls = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_initialize_controls',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for initialize the form layout'
        )
        self._wxui_connect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_connect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_disconnect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        cc.Destructor(
            parent=self._implementation,
            read_only=True,
            note="Destructor for the frame window",
            content="wxui_disconnect_event_handlers();"
        )

    def implement_python(self):
        """Create a python class with sanitized name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction."""
        self._implementation = py.Module(
            parent=self._parent,
            read_only=True,
            name='_{class_name}'.format(class_name=camel_to_snake(self._name)),
            note='Module containing classes definitions',
        )
        # We need to import wx
        py.Import(parent=self._implementation, name='wx')

        # If this is python, maybe we are using beatle identifiers. If so,
        # we must import the identifier method:
        # from beatle.lib.handlers import identifier
        if self.document.use_beatle_identifiers:
            kwargs = {
                'parent': self._implementation,
                'name': 'beatle.lib.handlers'
            }
            py.Import(**kwargs)

        self.implement_python_identifier()

        the_class = py.Class(
            parent=self._implementation,
            raw=True,  # prevent automatic object inheritance
            read_only=True,
            name=self._name,
            note=self.python_description or "This class implements a dialog and was created by wxUI."
        )
        # add inheritance
        py.Inheritance(parent=the_class, name='wx.Dialog', ancestor=None)
        # add a controls folder in order to avoid garbage
        self._controls_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )
        self._event_handlers_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )

        init_content = \
            "super({name},self).__init__(parent, id={id_}, title = '{title}', pos={pos}, size={size}, " \
            "style={style})\n{window_init}" \
            "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
            "self.wxui_initialize_controls()\n" \
            "self.wxui_connect_event_handlers()\n".format(
                name=self._name, id_=self.py_id, pos=self.py_pos, size=self.py_size, style=self.py_style_str,
                title=self._title, window_init=super(Dialog, self).python_init_control_code())
        if self._center:
            center_flag = {1: 'wx.BOTH', 2: 'wx.HORIZONTAL', 3: 'wx.VERTICAL'}
            init_content += 'self.Centre({flag})\n'.format(flag=center_flag[self._center])
        init_content += 'self.Layout()\n'

        ctor = py.InitMethod(
            parent=the_class,
            read_only=True,
            note="Constructor for the dialog window",
            content=init_content
        )
        py.Argument(
            parent=ctor,
            name='parent',
            default='None'
        )
        self._wxui_initialize_controls = py.MemberMethod(
            parent=the_class,
            name='wxui_initialize_controls',
            read_only=True,
            note='This method is responsible for initialize the form layout'
        )
        self._wxui_connect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_connect_event_handlers',
            read_only=True,
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_disconnect_event_handlers',
            read_only=True,
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        py.MemberMethod(
            parent=the_class,
            name='delete',
            read_only=True,
            note="Destructor for the frame window",
            content="self.wxui_disconnect_event_handlers()"
        )

    #@DelayedMethod()
    def update_implementation(self):
        """This method is invoked under transaction from set_kwargs
        in order to update the code that create the interface"""
        language = self.project.language
        if language == 'c++':
            self.update_cpp_implementation()
        elif language == 'python':
            self.update_python_implementation()

    def update_cpp_implementation(self):
        self.update_cpp_init_code()
        self.update_cpp_event_handlers()
        self.update_cpp_connect_event()
        self.update_cpp_disconnect_event()
        super(Dialog, self).update_cpp_implementation()

    def update_python_implementation(self):
        """This method update the cpp implementation. It's
        called under transaction."""
        self.update_python_init_code()
        self.update_python_event_handlers()
        self.update_python_connect_event()
        self.update_python_disconnect_event()
        super(Dialog, self).update_python_implementation()

    def update_cpp_init_code(self):
        """This method update the cpp implementation. It's
        called under transaction."""
        if self._implementation is None:
            wx.LogError("Unexpected missing wxUI Dialog implementation for update")
            return
        self.add_cpp_required_header('#include <wx/dialog.h>')
        ctor = self._implementation[cc.Constructor]
        if len(ctor) > 0:
            ctor = ctor[0]
            # update also the ctor argument defaults
            parent, windowId, caption, pos, size, style, unused = tuple(ctor[cc.Argument])
            if self._id and windowId.default != self._id:
                windowId.save_state()
                windowId.default = str(self._id)
            if self._title and '"{}"'.format(self._title) != caption.default:
                caption.save_state()
                caption.default = '"{}"'.format(self._title)
            pos_str = self.cc_pos_str
            if pos.default != pos_str:
                pos.save_state()
                pos.default = self.cc_pos_str
            size_str = self.cc_size_str
            if size.default != size_str:
                size.save_state()
                size.default = self.cc_size_str
            style_str = self.cc_style_str
            if style.default != style_str:
                style.save_state()
                style.default = style_str
            ctor.save_state()
            ctor.auto_init(force=True)

        if self._wxui_initialize_controls is None:
            wx.LogError("Unexpected missing wxUI implementation methods")
            return
        self._wxui_initialize_controls.save_state()

        code = 'SetSizeHints(wxDefaultSize, wxDefaultSize);\n'
        wx_setting_h_required = False
        if self._foreground_colour[0]:
            if type(self._foreground_colour[1]) is wx.SystemColour:
                from ....lib import cpp_syscolour_name
                name = cpp_syscolour_name(self._foreground_colour[1])
                if name is not None:
                    code += 'SetForegroundColour(wxSystemSettings::GetColour({name}));\n'.format(name=name)
                    wx_setting_h_required = True
            else:
                code += 'SetForegroundColour(wxColour{colour});\n'.format(colour=str(self._foreground_colour[1]))
        if self._background_colour[0]:
            if type(self._background_colour[1]) is wx.SystemColour:
                from ....lib import cpp_syscolour_name
                name = cpp_syscolour_name(self._background_colour[1])
                if name is not None:
                    code += 'SetBackgroundColour(wxSystemSettings::GetColour({name}));\n'.format(name=name)
                    wx_setting_h_required = True
            else:
                code += 'SetBackgroundColour(wxColour{colour});\n'.format(colour=str(self._background_colour[1]))
        if wx_setting_h_required:
            self.add_cpp_required_header("#include <wx/settings.h>")

        controls = self.sorted_wxui_child
        for control in controls:
            code += control.cpp_init_code()
        code += 'Layout();\n'

        if self._center:
            center_flag = {1: 'wxBOTH', 2: 'wxHORIZONTAL', 3: 'wxVERTICAL'}
            code += 'Centre({flag});\n'.format(flag=center_flag[self._center])

        # ...
        self._wxui_initialize_controls.content = code

    def update_python_init_code(self):
        _class = self._implementation[py.Class][0]
        ctor = _class(py.InitMethod, filter=lambda x: x.parent == _class)
        if len(ctor) == 0:
            return
        ctor = ctor[0]
        init_content = \
            "super({name},self).__init__(parent, id={id_}, title = '{title}', pos={pos}, size={size}, " \
            "style={style})\n{window_init}" \
            "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
            "self.wxui_initialize_controls()\n" \
            "self.wxui_connect_event_handlers()\n".format(
                name=self._name, id_=self.py_id, pos=self.py_pos, size=self.py_size, style=self.py_style_str,
                title=self._title, window_init=super(Dialog, self).python_init_control_code())
        if self._center:
            center_flag = {1: 'wx.BOTH', 2: 'wx.HORIZONTAL', 3: 'wx.VERTICAL'}
            init_content += 'self.Centre({flag})\n'.format(flag=center_flag[self._center])
        init_content += 'self.Layout()\n'
        ctor.set_content(init_content)
        ctor.auto_init()
        self._wxui_initialize_controls.set_content(self.python_init_control_code())

    def python_init_control_code(self, parent='self'):
        """This is a special case of implementing the code,
        because the code is not intended to create a variable
        but to initialize child."""
        new_code = ''
        # then, the next of the code
        for control in self.sorted_wxui_child:
            new_code += control.python_init_control_code()
        new_code += 'self.Layout()\n'
        return new_code

    def save_state(self):
        """Utility for saving state"""
        if self._implementation is not None:
            self._implementation.save_state()
        super(Dialog, self).save_state()
