from ..._TopWindow import Window


class ToolBarWindow(Window):
    """"This class represents a toolbar window"""

    def __init__(self, **kwargs):
        super(ToolBarWindow, self).__init__(**kwargs)

    @property
    def is_top_window(self):
        return True

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('toolbar')
