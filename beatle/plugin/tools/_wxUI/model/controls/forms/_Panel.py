import wx.core
from beatle.model import cc, py
from beatle.lib import camel_to_snake
from beatle.lib.decorators import upgrade_version
from ..._TopWindow import Window
from ....lib import get_colour_py_text_from_tuple


class Panel(Window):
    """"This class represents a pane as independent window.
    Take in account that the TopWindow represents here a
    window that can be root in the tree, not a really always
    TopWindow"""
    # TODO Change the name TopWindow for RootWindow is more appropiate?

    def __init__(self, **kwargs):
        # -- initialize data with default values
        self._aui_managed = False
        self._aui_manager_style = 0

        # -- initialize events with default values
        self._on_init_dialog = ''

        self._wxui_initialize_controls = None           # controls initialization method
        self._wxui_connect_event_handlers = None        # event_handlers method
        self._wxui_disconnect_event_handlers = None     # event_handlers method

        self._controls_folder = None                    # folder for windows controls
        self._event_handlers_folder = None              # folder for event handlers

        # -- initialize properties
        self._registered_identifiers = {}
        self._brief_description = None
        self._full_description = None

        super(Panel, self).__init__(**kwargs)

    @upgrade_version
    def __setstate__(self, data_dict):
        return {
            'add': {
                '_registered_identifiers': {},
                '_brief_description': None,
                '_full_description': None
            },
        }

    @property
    def local_panel_kwargs(self):
        return {
            'aui_managed': self._aui_managed,
            'aui_manager_style': self._aui_manager_style,
            'on_init_dialog': self._on_init_dialog,
            'brief_description': self._brief_description,
            'full_description': self._full_description
        }

    @local_panel_kwargs.setter
    def local_panel_kwargs(self, kwargs):
        self._aui_managed = kwargs.get('aui_managed', self._aui_managed)
        self._aui_manager_style = kwargs.get('aui_manager_style', self._aui_manager_style)
        self._on_init_dialog = kwargs.get('on_init_dialog', self._on_init_dialog)
        self._brief_description = kwargs.get('brief_description', self._brief_description)
        self._full_description = kwargs.get('full_description', self._full_description)

    @property
    def local_panel_events(self):
        return {
            'on_init_dialog': (self._on_init_dialog, 'wx.EVT_INIT_DIALOG', 'wxEVT_INIT_DIALOG')
        }

    @property
    def events(self):
        value = self.local_panel_events
        value.update(super(Panel, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_panel_kwargs
        value.update(super(Panel, self).kwargs)
        return value

    @property
    def python_description(self):
        if self._brief_description:
            if self._full_description:
                return """{self._brief_description}\n{self._full_description}""".format(self=self)
            else:
                return """{self._brief_description}""".format(self=self)
        else:
            if self._full_description:
                return """{self._full_description}""".format(self=self)
            else:
                return None

    def set_kwargs(self, kwargs):
        self.local_panel_kwargs = kwargs
        super(Panel, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            if self.project.language == 'python':
                self._implementation.name = '_{class_name}'.format(class_name=camel_to_snake(self._name))
                class_ = self._implementation[py.Class][0]
                class_.save_state()
                class_.name = self._name
            else:  # c++
                self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('panel')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Panel',
            'type': 'category',
            'help': 'Attributes of panel.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The panel name"
                },
                {
                    'label': 'aui_managed',
                    'type': 'boolean',
                    'value': self._aui_managed,
                    'name': 'aui_managed'
                },
                {
                    'label': 'aui_manager_style',
                    'type': 'multi_choice',
                    'value': self._aui_manager_style,
                    'name': 'aui_manager_style',
                    'options': [
                        ('wxAUI_MGR_ALLOW_ACTIVE_PANE', ''),
                        ('wxAUI_MGR_ALLOW_FLOATING', ''),
                        ('wxAUI_MGR_DEFAULT', ''),
                        ('wxAUI_MGR_HINT_FADE', ''),
                        ('wxAUI_MGR_LIVE_RESIZE', ''),
                        ('wxAUI_MGR_NO_VENETIAN_BLINDS_FADE', ''),
                        ('wxAUI_MGR_RECTANGLE_HINT', ''),
                        ('wxAUI_MGR_TRANSPARENT_DRAG', ''),
                        ('wxAUI_MGR_TRANSPARENT_HINT', ''),
                        ('wxAUI_MGR_VENETIAN_BLINDS_HINT', '')
                    ]
                },
                {
                    'label': 'brief description',
                    'type': 'string',
                    'read_only': False,
                    'value': self._brief_description or '',
                    'name': 'brief_description',  # kwarg value
                    'help': "A short description about this window."
                },
                {
                    'label': 'full description',
                    'type': 'long_string',
                    'read_only': False,
                    'value': self._full_description or '',
                    'name': 'full_description',  # kwarg value
                    'caption': 'Long description',
                    'help': 'A detailed description about this window.',
                },
            ]
        }]
        _event = [{
            'label': 'Panel',
            'type': 'category',
            'help': 'Events of panel window',
            'child': [{
                'label': 'on_init_dialog',
                'type': 'string',
                'read_only': False,
                'value': self._on_init_dialog,
                'name': 'on_init_dialog',  # kwarg value
                }]},]
        _base_property, _base_event = super(Panel, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def register_python_identifier(self, ident):
        if ident in self._registered_identifiers:
            identifier_object = self._registered_identifiers[ident]
            count = getattr(identifier_object,'shared_usage', 0)
            setattr(identifier_object, 'shared_usage', count+1)
        else:
            if self.standard_py_id(ident) is not None:
                return None
            if self.document.use_beatle_identifiers:
                identifier_object = py.Data(
                    parent=self._implementation,
                    name=ident, value='identifier("{key}")'.format(key=ident))
            else:
                identifier_object = py.Data(
                    parent=self._implementation,
                    name=ident, value='{value}'.format(value=self.document.identifier(ident)))
            setattr(identifier_object, 'shared_usage', 1)
            self._registered_identifiers[ident] = identifier_object
        return identifier_object

    def unregister_python_identifier(self, ident):
        if ident in self._registered_identifiers:
            identifier_object = self._registered_identifiers[ident]
            count = getattr(identifier_object, 'shared_usage', 1)
            if count < 2:
                identifier_object.delete()
            else:
                setattr(identifier_object, 'shared_usage', count-1)

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        from .. import layout
        layout_controls = [layout.WrapSizerControl, layout.StdDialogButtonSizerControl,
                           layout.StaticBoxSizerControl, layout.GridSizerControl,
                           layout.GridBagSizerControl, layout.FlexGridSizerControl,
                           layout.BoxSizerControl]
        try:
            return not next(x for x in layout_controls if len(self[x]) > 0)
        except StopIteration:
            return True

    @property
    def controls_folder(self):
        return self._controls_folder

    @property
    def event_handlers_folder(self):
        return self._event_handlers_folder

    @property
    def connect_event_handlers(self):
        return self._wxui_connect_event_handlers

    @property
    def disconnect_event_handlers(self):
        return self._wxui_disconnect_event_handlers

    @property
    def is_top_window(self):
        return True

    def create_edition_window(self, parent):
        from ....ui import uiFakePane
        self._edition_window = uiFakePane(parent, **self.kwargs)
        child = self.sorted_wxui_child
        for item in child:
            item.create_edition_window(self)
        self._edition_window.Layout()
        self._edition_window.Show()
        return self._edition_window

    @property
    def edition_window_client(self):
        """Return the control to be used as client container
        in the mockup visualization"""
        return self._edition_window

    def implement_cpp(self):
        """Create a c++ class with the sanitized_name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction."""
        self._implementation = cc.Class(
            parent=self.parent,
            read_only=True,
            name=self._name,
            prefix='',
            note="This class implements a dialog and was created by wxUI."
        )
        # Add required includes
        self._implementation.user_code_h1 = """#include <wx/panel.h>\n"""

        # Add a constructor that initialize the class

        # We need a wxFrame inheritance
        from ....lib import register_cpp_class
        cc.Inheritance(parent=self._implementation, ancestor=register_cpp_class(self.project, 'wxPanel'))

        # add a controls folder in order to avoid garbage
        self._controls_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )

        self._event_handlers_folder = cc.Folder(
            parent=self._implementation,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )
        # We create the constructor here.
        # read-only constructors prevent auto_args or auto_init
        # if not explicit called with force=True
        # Thus, we call these two later
        ctor = cc.Constructor(
            parent=self._implementation,
            read_only=True,
            note="Constructor for the panel window",
            content="wxui_initialize_controls();\n"
                    "wxui_connect_event_handlers();"
        )
        ctor.auto_args(force=True)
        ctor.auto_init(force=True)


        # Add handling methods

        self._wxui_initialize_controls = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_initialize_controls',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for initialize the form layout'
        )
        self._wxui_connect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_connect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = cc.MemberMethod(
            parent=self._implementation,
            name='wxui_disconnect_event_handlers',
            type=cc.typeinst(type_alias='void'),
            read_only=True,
            access='private',
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        cc.Destructor(
            parent=self._implementation,
            read_only=True,
            note="Destructor for the frame window",
            content="wxui_disconnect_event_handlers();"
        )

    def implement_python(self):
        """Create a python class with sanitized name.
        The class will be created as read-only and child of the model.
        This method is called from the constructor of form and must
        be inside a transaction.
        The class will be created inside a python module"""
        self._implementation = py.Module(
            parent=self._parent,
            read_only=True,
            name='_{class_name}'.format(class_name=camel_to_snake(self._name)),
            note='Module containing classes definitions',
        )
        # We need to import wx
        py.Import(parent=self._implementation, name='wx')

        # If this is python, maybe we are using beatle identifiers. If so,
        # we must import the identifier method:
        # from beatle.lib.handlers import identifier
        if self.document.use_beatle_identifiers:
            kwargs = {
                'parent': self._implementation,
                'from': 'beatle.lib.handlers',
                'name': 'identifier'
            }
            py.Import(**kwargs)

        self.implement_python_identifier()

        the_class = py.Class(
            parent=self._implementation,
            raw=True,  # prevent automatic object inheritance
            read_only=True,
            name=self._name,
            note="This class implements a pane and was created by wxUI."
        )
        # add inheritance
        py.Inheritance(parent=the_class, name='wx.Panel', ancestor=None)
        # add a controls folder in order to avoid garbage
        self._controls_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds the controls inside the window",
            name="controls"
        )
        self._event_handlers_folder = py.Folder(
            parent=the_class,
            read_only=True,
            note="This folder holds event handlers for this window",
            name="events"
        )
        init_content = "super({name}, self).__init__(parent, id={id_}, " \
                       "    pos = {pos}, size = {size}, style={style})\n" \
                       "{window_init}" \
                       "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
                       "self.wxui_initialize_controls()\n" \
                       "self.wxui_connect_event_handlers()\n".format(
                            name=self._name, id_=self.py_id, style=self.py_style_str,
                            window_init=super(Panel, self).python_init_control_code(),
                            pos=self.py_pos_str, size=self.py_size_str)

        ctor = py.InitMethod(
            parent=the_class,
            read_only=True,
            note="Constructor for the pane window",
            content=init_content
        )
        py.Argument(
            parent=ctor,
            read_only=True,
            name='parent',
            note="Parent for the frame window",
        )
        self._wxui_initialize_controls = py.MemberMethod(
            parent=the_class,
            name='wxui_initialize_controls',
            read_only=True,
            note='This method is responsible for initialize the form layout'
        )
        self._wxui_connect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_connect_event_handlers',
            read_only=True,
            note='This method is responsible for connect the event handlers'
        )
        self._wxui_disconnect_event_handlers = py.MemberMethod(
            parent=the_class,
            name='wxui_disconnect_event_handlers',
            read_only=True,
            note='This method is responsible for disconnect the event handlers'
        )

        # Add a destructor
        py.MemberMethod(
            parent=the_class,
            name='delete',
            read_only=True,
            note="Destructor for the frame window",
            content="self.wxui_disconnect_event_handlers();"
        )

    def update_implementation(self):
        """This method is invoked under transaction from set_kwargs
        in order to update the code that create the interface"""
        language = self.project.language
        if language == 'c++':
            self.update_cpp_implementation()
        elif language == 'python':
            self.update_python_implementation()

    def update_cpp_implementation(self):
        self.update_cpp_init_code()
        self.update_cpp_event_handlers()
        self.update_cpp_connect_event()
        self.update_cpp_disconnect_event()

    def update_python_implementation(self):
        """This method update the cpp implementation."""
        self.update_python_init_code()
        self.update_python_event_handlers()
        self.update_python_connect_event()
        self.update_python_disconnect_event()
        _class = self._implementation[py.Class][0]
        _class.note = self.python_description or "This class implements a dialog and was created by wxUI."

    def update_cpp_init_code(self):
        """This method update the cpp implementation. It's
        called under transaction."""
        if self._implementation is None:
            wx.LogError("Unexpected missing wxUI Dialog implementation for update")
            return

        self.add_cpp_required_header('#include <wx/panel.h>')

        ctor = self._implementation[cc.Constructor]
        if len(ctor) > 0:
            ctor = ctor[0]
            # update also the ctor argument defaults
            parent, window_id, pos, size, style, unused = tuple(ctor[cc.Argument])
            if self._id and window_id.default != self._id:
                window_id.save_state()
                window_id.default = str(self._id)
            pos_str = self.cc_pos_str
            if pos.default != pos_str:
                pos.save_state()
                pos.default = self.cc_pos_str
            size_str = self.cc_size_str
            if size.default != size_str:
                size.save_state()
                size.default = self.cc_size_str
            style_str = self.cc_style_str
            if style.default != style_str:
                style.save_state()
                style.default = style_str
            ctor.save_state()
            ctor.auto_init(force=True)

        if self._wxui_initialize_controls is None:
            wx.LogError("Unexpected missing wxUI implementation methods")
            return
        self._wxui_initialize_controls.save_state()

        code = 'SetSizeHints(wxDefaultSize, wxDefaultSize);\n'
        wx_setting_h_required = False
        if self._foreground_colour[0]:
            if type(self._foreground_colour[1]) is wx.SystemColour:
                from ....lib import cpp_syscolour_name
                name = cpp_syscolour_name(self._foreground_colour[1])
                if name is not None:
                    code += 'SetForegroundColour(wxSystemSettings::GetColour({name}));\n'.format(name=name)
                    wx_setting_h_required = True
            else:
                code += 'SetForegroundColour(wxColour{colour});\n'.format(colour=str(self._foreground_colour[1]))
        if self._background_colour[0]:
            if type(self._background_colour[1]) is wx.SystemColour:
                from ....lib import cpp_syscolour_name
                name = cpp_syscolour_name(self._background_colour[1])
                if name is not None:
                    code += 'SetBackgroundColour(wxSystemSettings::GetColour({name}));\n'.format(name=name)
                    wx_setting_h_required = True
            else:
                code += 'SetBackgroundColour(wxColour{colour});\n'.format(colour=str(self._background_colour[1]))
        if wx_setting_h_required:
            self.add_cpp_required_header("#include <wx/settings.h>")

        controls = self.sorted_wxui_child
        for control in controls:
            code += control.cpp_init_code()
        code += 'Layout();\n'

        self._wxui_initialize_controls.content = code

    def update_python_init_code(self):
        _class = self._implementation[py.Class][0]
        ctor = _class(py.InitMethod, filter=lambda x: x.parent == _class)
        if len(ctor) == 0:
            return
        ctor = ctor[0]
        init_content = "super({name}, self).__init__(parent, id={id_}, " \
                       "    pos = {pos}, size = {size}, style={style})\n" \
                       "{window_init}" \
                       "self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)\n" \
                       "self.wxui_initialize_controls()\n" \
                       "self.wxui_connect_event_handlers()\n".format(
                            name=self._name, id_=self.py_id, style=self.py_style_str,
                            window_init=super(Panel, self).python_init_control_code(),
                            pos=self.py_pos_str, size=self.py_size_str)
        ctor.set_content(init_content)
        ctor.auto_init()
        self._wxui_initialize_controls.set_content(self.python_init_control_code())

    def python_init_control_code(self, parent='self'):
        """This is a special case of implementing the code,
        because the code is not intended to create a variable
        but to initialize child."""
        from ..bar import MenuBarControl, StatusBarControl
        new_code = ''
        controls = self.sorted_wxui_child
        # first, search if some child is of type menubar
        menu_bar_list = [(index, element) for (index, element) in enumerate(controls)
                         if type(element) is MenuBarControl]
        if len(menu_bar_list):
            index, menu_bar = menu_bar_list[0]
            new_code += menu_bar.python_init_control_code()
            del controls[index]
        # next search for a status bar, if any
        status_bar_code = ''
        status_bar_list = [(index, element) for (index, element) in enumerate(controls)
                           if type(element) is StatusBarControl]
        if len(status_bar_list):
            index, status_bar = status_bar_list[0]
            status_bar_code = status_bar.python_init_control_code()
            del controls[index]
        # then, the next of the code
        for control in controls:
            new_code += control.python_init_control_code()
        new_code += status_bar_code
        new_code += 'self.Layout()\n'
        return new_code

    def save_state(self):
        """Utility for saving state"""
        if self._implementation is not None:
            self._implementation.save_state()
        super(Panel, self).save_state()

