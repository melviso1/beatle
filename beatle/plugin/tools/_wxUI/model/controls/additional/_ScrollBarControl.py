import os
import wx
import wx.adv
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


@with_style
@extend_events('scroll_bar_events')
class ScrollBarControl(WindowControl):
    """"This class represents a scroll bar"""

    style_list = (wx.SB_HORIZONTAL, wx.SB_VERTICAL, )
    style_list_py_text = ('wx.SB_HORIZONTAL', 'wx.SB_VERTICAL', )
    style_list_cc_text = ('wxSB_HORIZONTAL', 'wxSB_VERTICAL', )

    def __init__(self, **kwargs):
        self._value = 0
        self._range = 100
        self._thumb_size = 1
        self._page_size = 1
        self._style = 1
        # events
        self._on_scroll = ''
        self._on_scroll_top = ''
        self._on_scroll_bottom = ''
        self._on_scroll_lineup = ''
        self._on_scroll_linedown = ''
        self._on_scroll_pageup = ''
        self._on_scroll_pagedown = ''
        self._on_scroll_thumbtrack = ''
        self._on_scroll_thumbrelease = ''
        self._on_scroll_changed = ''
        self._on_command_scroll = ''
        self._on_command_scroll_top = ''
        self._on_command_scroll_bottom = ''
        self._on_command_scroll_lineup = ''
        self._on_command_scroll_linedown = ''
        self._on_command_scroll_pageup = ''
        self._on_command_scroll_pagedown = ''
        self._on_command_scroll_thumbtrack = ''
        self._on_command_scroll_thumbrelease = ''
        self._on_command_scroll_changed = ''
        super(ScrollBarControl, self).__init__(**kwargs)

    @property
    def local_scroll_bar_kwargs(self):
        return {
            'value': self._value,
            'range': self._range,
            'thumb_size': self._thumb_size,
            'page_size': self._page_size,
            'style': self._style,
            # events
            'on_scroll': self._on_scroll,
            'on_scroll_top': self._on_scroll_top,
            'on_scroll_bottom': self._on_scroll_bottom,
            'on_scroll_lineup': self._on_scroll_lineup,
            'on_scroll_linedown': self._on_scroll_linedown,
            'on_scroll_pageup': self._on_scroll_pageup,
            'on_scroll_pagedown': self._on_scroll_pagedown,
            'on_scroll_thumbtrack': self._on_scroll_thumbtrack,
            'on_scroll_thumbrelease': self._on_scroll_thumbrelease,
            'on_scroll_changed': self._on_scroll_changed,
            'on_command_scroll': self._on_command_scroll,
            'on_command_scroll_top': self._on_command_scroll_top,
            'on_command_scroll_bottom': self._on_command_scroll_bottom,
            'on_command_scroll_lineup': self._on_command_scroll_lineup,
            'on_command_scroll_linedown': self._on_command_scroll_linedown,
            'on_command_scroll_pageup': self._on_command_scroll_pageup,
            'on_command_scroll_pagedown': self._on_command_scroll_pagedown,
            'on_command_scroll_thumbtrack': self._on_command_scroll_thumbtrack,
            'on_command_scroll_thumbrelease': self._on_command_scroll_thumbrelease,
            'on_command_scroll_changed': self._on_command_scroll_changed
        }

    @local_scroll_bar_kwargs.setter
    def local_scroll_bar_kwargs(self, kwargs):
        self._value = kwargs.get('value', self._value)
        self._range = kwargs.get('range', self._range)
        self._thumb_size = kwargs.get('thumb_size', self._thumb_size)
        self._page_size = kwargs.get('page_size', self._page_size)
        self._style = kwargs.get('style', self._style)
        # events
        self._on_scroll = kwargs.get('on_scroll', self._on_scroll)
        self._on_scroll_top = kwargs.get('on_scroll_top', self._on_scroll_top)
        self._on_scroll_bottom = kwargs.get('on_scroll_bottom', self._on_scroll_bottom)
        self._on_scroll_lineup = kwargs.get('on_scroll_lineup', self._on_scroll_lineup)
        self._on_scroll_linedown = kwargs.get('on_scroll_linedown', self._on_scroll_linedown)
        self._on_scroll_pageup = kwargs.get('on_scroll_pageup', self._on_scroll_pageup)
        self._on_scroll_pagedown = kwargs.get('on_scroll_pagedown', self._on_scroll_pagedown)
        self._on_scroll_thumbtrack = kwargs.get('on_scroll_thumbtrack', self._on_scroll_thumbtrack)
        self._on_scroll_thumbrelease = kwargs.get('on_scroll_thumbrelease', self._on_scroll_thumbrelease)
        self._on_scroll_changed = kwargs.get('on_scroll_changed', self._on_scroll_changed)
        self._on_command_scroll = kwargs.get('on_command_scroll', self._on_command_scroll)
        self._on_command_scroll_top = kwargs.get('on_command_scroll_top', self._on_command_scroll_top)
        self._on_command_scroll_bottom = kwargs.get('on_command_scroll_bottom', self._on_command_scroll_bottom)
        self._on_command_scroll_lineup = kwargs.get('on_command_scroll_lineup', self._on_command_scroll_lineup)
        self._on_command_scroll_linedown = kwargs.get('on_command_scroll_linedown', self._on_command_scroll_linedown)
        self._on_command_scroll_pageup = kwargs.get('on_command_scroll_pageup', self._on_command_scroll_pageup)
        self._on_command_scroll_pagedown = kwargs.get('on_command_scroll_pagedown', self._on_command_scroll_pagedown)
        self._on_command_scroll_thumbtrack = kwargs.get('on_command_scroll_thumbtrack',
                                                        self._on_command_scroll_thumbtrack)
        self._on_command_scroll_thumbrelease = kwargs.get('on_command_scroll_thumbrelease',
                                                          self._on_command_scroll_thumbrelease)
        self._on_command_scroll_changed = kwargs.get('on_command_scroll_changed', self._on_command_scroll_changed)

    @property
    def scroll_bar_events(self):
        return {
            'on_scroll': (self._on_scroll, 'wx.EVT_SCROLL', 'wxEVT_SCROLL'),
            'on_scroll_top': (self._on_scroll_top, 'wx.EVT_SCROLL_TOP', 'wxEVT_SCROLL_TOP'),
            'on_scroll_bottom': (self._on_scroll_bottom, 'wx.EVT_SCROLL_BOTTOM', 'wxEVT_SCROLL_BOTTOM'),
            'on_scroll_lineup': (self._on_scroll_lineup, 'wx.EVT_SCROLL_LINEUP', 'wxEVT_SCROLL_LINEUP'),
            'on_scroll_linedown': (self._on_scroll_linedown, 'wx.EVT_SCROLL_LINEDOWN', 'wxEVT_SCROLL_LINEDOWN'),
            'on_scroll_pageup': (self._on_scroll_pageup, 'wx.EVT_SCROLL_PAGEUP', 'wxEVT_SCROLL_PAGEUP'),
            'on_scroll_pagedown': (self._on_scroll_pagedown, 'wx.EVT_SCROLL_PAGEDOWN', 'wxEVT_SCROLL_PAGEDOWN'),
            'on_scroll_thumbtrack': (self._on_scroll_thumbtrack, 'wx.EVT_SCROLL_THUMBTRACK', 'wxEVT_SCROLL_THUMBTRACK'),
            'on_scroll_thumbrelease': (self._on_scroll_thumbrelease, 'wx.EVT_SCROLL_THUMBRELEASE',
                                       'wxEVT_SCROLL_THUMBRELEASE'),
            'on_scroll_changed': (self._on_scroll_changed, 'wx.EVT_SCROLL_CHANGED', 'wxEVT_SCROLL_CHANGED'),
            'on_command_scroll': (self._on_command_scroll, 'wx.EVT_COMMAND_SCROLL', 'wxEVT_COMMAND_SCROLL'),
            'on_command_scroll_top': (self._on_command_scroll_top, 'wx.EVT_COMMAND_SCROLL_TOP',
                                      'wxEVT_COMMAND_SCROLL_TOP'),
            'on_command_scroll_bottom': (self._on_command_scroll_bottom, 'wx.EVT_COMMAND_SCROLL_BOTTOM',
                                         'wxEVT_COMMAND_SCROLL_BOTTOM'),
            'on_command_scroll_lineup': (self._on_command_scroll_lineup, 'wx.EVT_COMMAND_SCROLL_LINEUP',
                                         'wxEVT_COMMAND_SCROLL_LINEUP'),
            'on_command_scroll_linedown': (self._on_command_scroll_linedown, 'wx.EVT_COMMAND_SCROLL_LINEDOWN',
                                           'wxEVT_COMMAND_SCROLL_LINEDOWN'),
            'on_command_scroll_pageup': (self._on_command_scroll_pageup, 'wx.EVT_COMMAND_SCROLL_PAGEUP',
                                         'wxEVT_COMMAND_SCROLL_PAGEUP'),
            'on_command_scroll_pagedown': (self._on_command_scroll_pagedown, 'wx.EVT_COMMAND_SCROLL_PAGEDOWN',
                                           'wxEVT_COMMAND_SCROLL_PAGEDOWN'),
            'on_command_scroll_thumbtrack': (self._on_command_scroll_thumbtrack, 'wx.EVT_COMMAND_SCROLL_THUMBTRACK',
                                             'wxEVT_COMMAND_SCROLL_THUMBTRACK'),
            'on_command_scroll_thumbrelease': (
                self._on_command_scroll_thumbrelease, 'wx.EVT_COMMAND_SCROLL_THUMBRELEASE',
                'wxEVT_COMMAND_SCROLL_THUMBRELEASE'),
            'on_command_scroll_changed': (self._on_command_scroll_changed, 'wx.EVT_COMMAND_SCROLL_CHANGED',
                                          'wxEVT_COMMAND_SCROLL_CHANGED'),
        }

    @property
    def kwargs(self):
        value = self.local_scroll_bar_kwargs
        value.update(super(ScrollBarControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_scroll_bar_kwargs = kwargs
        super(ScrollBarControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('scroll_bar')

    @property
    def style_attribute_options(self):
        return [
            ('wxSB_HORIZONTAL', 'Specifies a horizontal scrollbar.'),
            ('wxSB_VERTICAL', 'Specifies a vertical scrollbar.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Scroll',
            'type': 'category',
            'help': 'Attributes of scroll control.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The calendar control name."
                },
                {
                    'label': 'value',
                    'type': 'integer',
                    'value': self._value,
                    'name': 'value',
                    'help': 'The scrollbar value by position.'
                },
                {
                    'label': 'range',
                    'type': 'integer',
                    'value': self._range,
                    'name': 'range',
                    'help': 'Maximum scroll bar range.'
                },
                {
                    'label': 'thumb size',
                    'type': 'integer',
                    'value': self._thumb_size,
                    'name': 'thumb_size',
                    'help': 'Size of the thumb, or visible portion of the scrollbar, in scroll units.'
                },
                {
                    'label': 'page size',
                    'type': 'integer',
                    'value': self._page_size,
                    'name': 'page_size',
                    'help': 'Size of the thumb, or visible portion of the scrollbar, in scroll units.'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Set the scrollbar style.'
                },
            ]
        }]
        _event = [{
            'label': 'Calendar',
            'type': 'category',
            'help': 'Events of scroll bar',
            'child': [
                {
                    'label': 'on scroll',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll,
                    'name': 'on_scroll',
                    'help': 'Process all scroll events.'
                },
                {
                    'label': 'on scroll_top',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_top,
                    'name': 'on_scroll_top',
                    'help': 'Process wxEVT_SCROLL_TOP scroll to top or leftmost (minimum) position events.'
                },
                {
                    'label': 'on scroll_bottom',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_bottom,
                    'name': 'on_scroll_bottom',
                    'help': 'Process wxEVT_SCROLL_BOTTOM scroll to bottom or rightmost (maximum) position events.'
                },
                {
                    'label': 'on scroll_lineup',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_lineup,
                    'name': 'on_scroll_lineup',
                    'help': 'Process wxEVT_SCROLL_LINEUP line up or left events.'
                },
                {
                    'label': 'on scroll_linedown',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_linedown,
                    'name': 'on_scroll_linedown',
                    'help': 'Process wxEVT_SCROLL_LINEDOWN line down or right events.'
                },
                {
                    'label': 'on scroll_pageup',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_pageup,
                    'name': 'on_scroll_pageup',
                    'help': 'Process wxEVT_SCROLL_PAGEUP page up or left events.'
                },
                {
                    'label': 'on scroll_pagedown',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_pagedown,
                    'name': 'on_scroll_pagedown',
                    'help': 'Process wxEVT_SCROLL_PAGEDOWN page down or right events.'
                },
                {
                    'label': 'on scroll_thumbtrack',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_thumbtrack,
                    'name': 'on_scroll_thumbtrack',
                    'help': 'Process wxEVT_SCROLL_THUMBTRACK thumbtrack events (frequent events sent as the user '
                            'drags the thumbtrack).'
                },
                {
                    'label': 'on scroll_thumbrelease',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_thumbrelease,
                    'name': 'on_scroll_thumbrelease',
                    'help': 'Process wxEVT_SCROLL_THUMBRELEASE thumb release events.'
                },
                {
                    'label': 'on scroll_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_scroll_changed,
                    'name': 'on_scroll_changed',
                    'help': 'Process wxEVT_SCROLL_CHANGED end of scrolling events (MSW only).'
                },
                {
                    'label': 'on command_scroll',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll,
                    'name': 'on_command_scroll',
                    'help': 'Process all scroll events.'
                },
                {
                    'label': 'on command_scroll_top',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_top,
                    'name': 'on_command_scroll_top',
                    'help': 'Process wxEVT_SCROLL_TOP scroll to top or leftmost (minimum) position events.'
                },
                {
                    'label': 'on command_scroll_bottom',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_bottom,
                    'name': 'on_command_scroll_bottom',
                    'help': 'Process wxEVT_SCROLL_BOTTOM scroll to bottom or rightmost (maximum) position events.'
                },
                {
                    'label': 'on command_scroll_lineup',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_lineup,
                    'name': 'on_command_scroll_lineup',
                    'help': 'Process wxEVT_SCROLL_LINEUP line up or left events.'
                },
                {
                    'label': 'on command_scroll_linedown',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_linedown,
                    'name': 'on_command_scroll_linedown',
                    'help': 'Process wxEVT_SCROLL_LINEDOWN line down or right events.'
                },
                {
                    'label': 'on command_scroll_pageup',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_pageup,
                    'name': 'on_command_scroll_pageup',
                    'help': 'Process wxEVT_SCROLL_PAGEUP page up or left events.'
                },
                {
                    'label': 'on command_scroll_pagedown',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_pagedown,
                    'name': 'on_command_scroll_pagedown',
                    'help': 'Process wxEVT_SCROLL_PAGEDOWN page down or right events.'
                },
                {
                    'label': 'on command_scroll_thumbtrack',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_thumbtrack,
                    'name': 'on_command_scroll_thumbtrack',
                    'help': 'Process wxEVT_SCROLL_THUMBTRACK thumbtrack events (frequent events sent as the user drags the thumbtrack).'
                },
                {
                    'label': 'on command_scroll_thumbrelease',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_thumbrelease,
                    'name': 'on_command_scroll_thumbrelease',
                    'help': 'Process wxEVT_SCROLL_THUMBRELEASE thumb release events.'
                },
                {
                    'label': 'on command_scroll_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_command_scroll_changed,
                    'name': 'on_command_scroll_changed',
                    'help': 'Process wxEVT_SCROLL_CHANGED end of scrolling events (MSW only). '
                },],
            }]
        _base_property, _base_event = super(ScrollBarControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.ScrollBar)(
            edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
        self._edition_window.SetScrollbar(self._value, self._thumb_size, self._range,
                                          self._page_size, refresh=True)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # required #include <wx/scrolbar.h>
        class_ = register_cpp_class(self.project, 'wxScrollBar')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="scroll bar control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/scrolbar.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxScrollBar({owner}, {self._id}, {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        code += f'{self._name}->SetScrollbar({self._value}, {self._thumb_size}, {self._range}, ' \
                f'{self._page_size}, true);\n'
        code += super(ScrollBarControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.ScrollBar({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += f'{this}.SetScrollbar({self._value}, {self._thumb_size}, {self._range}, ' \
                f'{self._page_size}, refresh=True)\n'

        code += super(ScrollBarControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


