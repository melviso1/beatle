import os
import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


@with_style
@extend_events('generic_dir_events')
class GenericDirControl(WindowControl):
    """"This class represents a colour picker"""

    style_list = (wx.DIRCTRL_DIR_ONLY, wx.DIRCTRL_3D_INTERNAL, wx.DIRCTRL_SELECT_FIRST, wx.DIRCTRL_SHOW_FILTERS,
                  wx.DIRCTRL_EDIT_LABELS, wx.DIRCTRL_MULTIPLE)
    style_list_py_text = ('wx.DIRCTRL_DIR_ONLY', 'wx.DIRCTRL_3D_INTERNAL', 'wx.DIRCTRL_SELECT_FIRST',
                          'wx.DIRCTRL_SHOW_FILTERS', 'wx.DIRCTRL_EDIT_LABELS', 'wx.DIRCTRL_MULTIPLE')
    style_list_cc_text = ('wxDIRCTRL_DIR_ONLY', 'wxDIRCTRL_3D_INTERNAL', 'wxDIRCTRL_SELECT_FIRST',
                          'wxDIRCTRL_SHOW_FILTERS', 'wxDIRCTRL_EDIT_LABELS', 'wxDIRCTRL_MULTIPLE')

    def __init__(self, **kwargs):
        self._style = 2
        self._dir = ''
        self._filter = ''
        self._filter_index = 0
        self._show_hidden = True
        # events
        self._on_dir_selection_changed = ''
        self._on_file_activated = ''
        super(GenericDirControl, self).__init__(**kwargs)

    @property
    def local_generic_dir_kwargs(self):
        return {
            'style': self._style,
            'dir': self._dir,
            'filter': self._filter,
            'filter_index': self._filter_index,
            'show_hidden': self._show_hidden,
            # events
            'on_dir_selection_changed': self._on_dir_selection_changed,
            'on_file_activated': self._on_file_activated,
        }

    @local_generic_dir_kwargs.setter
    def local_generic_dir_kwargs(self, kwargs):
        # events
        self._style = kwargs.get('style', self._style)
        self._dir = kwargs.get('dir', self._dir)
        self._filter = kwargs.get('filter', self._filter)
        self._filter_index = kwargs.get('filter_index', self._filter_index)
        self._show_hidden = kwargs.get('show_hidden', self._show_hidden)
        self._on_dir_selection_changed = kwargs.get('on_dir_selection_changed', self._on_dir_selection_changed)
        self._on_file_activated = kwargs.get('on_file_activated', self._on_file_activated)

    @property
    def generic_dir_events(self):
        return {
            'on_dir_selection_changed': (self._on_dir_selection_changed, 'wx.EVT_DIRCTRL_SELECTIONCHANGED'),
            'on_file_activated': (self._on_file_activated, 'wx.EVT_DIRCTRL_FILEACTIVATED',
                                  'wxEVT_DIRCTRL_FILEACTIVATED'),
        }

    @property
    def kwargs(self):
        value = self.local_generic_dir_kwargs
        value.update(super(GenericDirControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_generic_dir_kwargs = kwargs
        super(GenericDirControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('generic_dir_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxDIRCTRL_DIR_ONLY', "Only show directories, and not files."),
            ('wxDIRCTRL_3D_INTERNAL', "Use 3D borders for internal controls. This is the default."),
            ('wxDIRCTRL_SELECT_FIRST', "When setting the default path, select the first file in the directory."),
            ('wxDIRCTRL_SHOW_FILTERS', "Show the drop-down filter list."),
            ('wxDIRCTRL_EDIT_LABELS', "Allow the folder and file labels to be editable."),
            ('wxDIRCTRL_MULTIPLE', "Allows multiple files and folders to be selected."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Generic dir',
            'type': 'category',
            'help': 'Attributes of generic dir.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The generic dir control name."
                },
                {
                    'label': 'default dir',
                    'type': 'string',
                    'read_only': False,
                    'value': self._dir,
                    'name': 'dir',  # kwarg value
                    'help': "The start directory to browse."
                },
                {
                    'label': 'filter',
                    'type': 'string',
                    'read_only': False,
                    'value': self._filter,
                    'name': 'filter',  # kwarg value
                    'help': "The filters to apply for output."
                },
                {
                    'label': 'filter index',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._filter_index,
                    'name': 'filter_index',  # kwarg value
                    'help': "The initial filter to apply."
                },
                {
                    'label': 'show hidden',
                    'type': 'boolean',
                    'value': self._show_hidden,
                    'name': 'show_hidden',
                    'help': 'Enable show hidden files.'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the colour picker style.'
                },
            ]
        }]
        _event = [{
            'label': 'Generic dir',
            'type': 'category',
            'help': 'Events of generic dir',
            'child': [
                {
                    'label': 'on dir changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dir_selection_changed,
                    'name': 'on_dir_selection_changed',
                    'help': ' Selected directory has changed. Processes a wxEVT_DIRCTRL_SELECTIONCHANGED event type. '
                            'Notice that this event is generated even for the changes done by the program itself and '
                            'not only those done by the user. Available since wxWidgets 2.9.5.'
                },
                {
                    'label': 'on file activated',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_file_activated,
                    'name': 'on_file_activated',
                    'help': ' The user activated a file by double-clicking or pressing Enter. '
                            'Available since wxWidgets 2.9.5. '
                }],
            }]
        _base_property, _base_event = super(GenericDirControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.GenericDirCtrl)(
            edition_frame, self.py_id, self._dir, self.py_pos, self.py_size,
            self.style_value, self._filter, self._filter_index)
        self._edition_window.ShowHidden(self._show_hidden)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/dirctrl.h>
        class_ = register_cpp_class(self.project, 'wxGenericDirCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="generic dir control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/dirctrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxGenericDirCtrl({owner}, {self._id}, "{self._dir}", ' \
               f'{self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str}, ' \
               f'"{self._filter}", {self._filter_index});\n'
        show = (self._show_hidden and 'true') or 'false'
        code += f"{self._name}->ShowHidden({show});\n"
        code += super(GenericDirControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.GenericDirCtrl({owner}, {self.py_id_str}, "{self._dir}", ' \
               f'{self.py_pos_str}, {self.py_size_str}, {self.py_style_str}, "{filter}", {self._filter_index})\n'
        code += f"{this}.ShowHidden({self._show_hidden})\n"
        code += super(GenericDirControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


