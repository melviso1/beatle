import wx
import wx.stc
from copy import copy
from beatle.model import cc
from beatle.lib.wxx.py.editwindow import FACES
from .._WindowControl import WindowControl
from ....lib import register_cpp_class, get_colour, get_colour_from_tuple
from wx import stc

# missing definitions
wx.STC_TD_LONGARROW = 0
wx.STC_TD_STRIKEOUT = 1


class StyledTextControl(WindowControl):
    """"
    This class represents a styled text control, that
    wraps a Scintilla text editor.
    This kind of control is almost completely different to the
    ones defined by wxwidgets.
    In the initial version, we simply add it and dont do any
    customization. We will rework that in a second iteration
    because there is a lot of work here.
    Also, customization done in that way is not specially
    useful for general purpose editors, even it can be used
    to guide developers for quick handle desired customization.
    This control has not special properties or styles.
    """

    def __init__(self, **kwargs):
        super(StyledTextControl, self).__init__(**kwargs)

    @property
    def local_styled_text_kwargs(self):
        return {}

    @local_styled_text_kwargs.setter
    def local_styled_text_kwargs(self, kwargs):
        pass

    @property
    def kwargs(self):
        value = self.local_styled_text_kwargs
        value.update(super(StyledTextControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_styled_text_kwargs = kwargs
        super(StyledTextControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('styled_text')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Styled text',
            'type': 'category',
            'help': 'Attributes of styled text.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The richtext control name."
                }]
        }]
        _event = []
        _base_property, _base_event = super(StyledTextControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.stc.StyledTextCtrl)(
            edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value, wx.EmptyString)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)
        faces = copy(FACES)
        back = get_colour('background_colour', self.kwargs)
        if back:
            faces['backcol'] = back.GetAsString(wx.C2S_HTML_SYNTAX)
        fore = get_colour('foreground_colour', self.kwargs)
        if fore:
            faces['forecol'] = fore.GetAsString(wx.C2S_HTML_SYNTAX)
        if fore or back:
            self._edition_window.SetLexer(stc.STC_LEX_NULL)
            self._edition_window.SetCodePage(stc.STC_CP_UTF8)
            self._edition_window.SetStyleBits(self._edition_window.GetStyleBitsNeeded())
            self._edition_window.StyleSetSpec(stc.STC_STYLE_DEFAULT,
                                              "face:%(mono)s,size:%(size)d,fore:%(forecol)s,back:%(backcol)s" % faces)
            self._edition_window.StyleSetSpec(stc.STC_P_DEFAULT,
                                              "face:%(mono)s" % faces)
            self._edition_window.StyleClearAll()
            self._edition_window.SetSelForeground(True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))
            self._edition_window.SetSelBackground(True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT))

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/stc/stc.h>
        class_ = register_cpp_class(self.project, 'wxStyledTextCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="styled text control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/stc/stc.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f"{self._name} = new wxStyledTextCtrl({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str}, wxEmptyString);\n"
        code += super(StyledTextControl, self).cpp_init_code()

        if self._font_info[0]:
            code += f'{self._name}->StyleSetFont(wxSTC_STYLE_DEFAULT, wxFont("{self._font_info[1]}"));\n'

        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_colors(self, this):
        spec = []
        back = get_colour_from_tuple(self._background_colour)
        fore = get_colour_from_tuple(self._foreground_colour)
        if back:
            spec.append(f"back:{back.GetAsString(wx.C2S_HTML_SYNTAX)}s")
        if fore:
            spec.append(f"fore:{fore.GetAsString(wx.C2S_HTML_SYNTAX)}s")
        if fore or back:
            spec = ','.join(spec)
            return f'{this}.StyleSetSpec(wx.stc.STC_STYLE_DEFAULT, "{spec}")\n' \
                   f'{this}.StyleSetSpec(wx.stc.STC_P_DEFAULT, "{spec}")\n' \
                   f'{this}.StyleClearAll()\n'
        return ''

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.stc')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent._name}'

        code = f'{this} = wx.stc.StyledTextCtrl({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str}, wx.EmptyString)\n'
        code += super(StyledTextControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


