import os
import wx
import wx.adv
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


class CustomControl(WindowControl):
    """"This class represents a custom user control. This style of class
    is useful for inserting a kind of controls that aren't included in
    Beatle"""

    def __init__(self, **kwargs):
        self._class = ''
        self._declaration = ''
        self._construction = ''
        self._include = ''
        self._settings = ''
        # events
        super(CustomControl, self).__init__(**kwargs)

    @property
    def local_custom_control_kwargs(self):
        return {
            'class': self._class,
            'declaration': self._declaration,
            'construction': self._construction,
            'include': self._include,
            'settings': self._settings,
        }

    @local_custom_control_kwargs.setter
    def local_custom_control_kwargs(self, kwargs):
        self._class = kwargs.get('class', self._class)
        self._declaration = kwargs.get('declaration', self._declaration)
        self._construction = kwargs.get('construction', self._construction)
        self._include = kwargs.get('include', self._include)
        self._settings = kwargs.get('settings', self._settings)

    @property
    def kwargs(self):
        value = self.local_custom_control_kwargs
        value.update(super(CustomControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_custom_control_kwargs = kwargs
        super(CustomControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('custom_control')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Custom control',
            'type': 'category',
            'help': 'Attributes of custom control.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The custom control name."
                },
                {
                    'label': 'class',
                    'type': 'string',
                    'read_only': False,
                    'value': self._class,
                    'name': 'class',  # kwarg value
                    'help': "The custom control class name."
                },
                {
                    'label': 'declaration',
                    'type': 'string',
                    'read_only': False,
                    'value': self._declaration,
                    'name': 'declaration',  # kwarg value
                    'help': "The custom control declaration."
                },
                {
                    'label': 'implementation',
                    'type': 'string',
                    'read_only': False,
                    'value': self._construction,
                    'name': 'construction',  # kwarg value
                    'help': "The custom control creation code."
                },
                {
                    'label': 'include',
                    'type': 'string',
                    'read_only': False,
                    'value': self._include,
                    'name': 'include',  # kwarg value
                    'help': "The required includes for the control."
                },
                {
                    'label': 'settings',
                    'type': 'string',
                    'read_only': False,
                    'value': self._settings,
                    'name': 'settings',  # kwarg value
                    'help': "Extra code for setup control settings. This code will be inserted "
                            "after the implementation code"
                },
            ]
        }]
        _event = []
        _base_property, _base_event = super(CustomControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.StaticBitmap)(
            edition_frame, self.py_id, wx.NullBitmap, self.py_pos, self.py_size, 0)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # required #include <wx/calctrl.h>
        class_ = register_cpp_class(self.project, self._class)
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="calendar control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        for line in self._include.splitlines():
            self.add_cpp_required_header(line)
        code = f'{self._construction}\n'
        code += super(CustomControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        for line in self._include.splitlines():
            self.add_python_import(line)
        code = f'{self._construction}\n'
        this = f'self.{self._name}'
        code += super(CustomControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


