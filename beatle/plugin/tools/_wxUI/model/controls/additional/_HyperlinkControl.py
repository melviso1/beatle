import os
import wx
import wx.adv
from .._WindowControl import WindowControl
from beatle.model import cc
from ....lib import register_cpp_class, with_style, extend_events, popup_errors_to_user


@with_style
@extend_events('hyperlink_events')
class HyperlinkControl(WindowControl):
    """"This class represents a button"""

    style_list = (wx.adv.HL_DEFAULT_STYLE, wx.adv.HL_ALIGN_LEFT, wx.adv.HL_ALIGN_RIGHT,
                  wx.adv.HL_ALIGN_CENTRE, wx.adv.HL_CONTEXTMENU)
    style_list_py_text = ('wx.adv.HL_DEFAULT_STYLE', 'wx.adv.HL_ALIGN_LEFT', 'wx.adv.HL_ALIGN_RIGHT',
                          'wx.adv.HL_ALIGN_CENTRE', 'wx.adv.HL_CONTEXTMENU')
    style_list_cc_text = ('wxHL_DEFAULT_STYLE', 'wxHL_ALIGN_LEFT', 'wxHL_ALIGN_RIGHT',
                          'wxHL_ALIGN_CENTRE', 'wxHL_CONTEXTMENU')

    def __init__(self, **kwargs):
        self._label = 'Emotional Operating System'
        self._url = 'https://emos.dev'
        self._style = 1
        # events
        self._on_hyperlink = ''

        super(HyperlinkControl, self).__init__(**kwargs)
        self.set_kwargs(kwargs)

    @property
    def local_hyperlink_kwargs(self):
        return {
            'style': self._style,
            'label': self._label,
            'url': self._url,
            'on_hyperlink': self._on_hyperlink,
        }

    @local_hyperlink_kwargs.setter
    def local_hyperlink_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._label = kwargs.get('label', self._label)
        self._url = kwargs.get('url', self._url)
        self._on_hyperlink = kwargs.get('on_hyperlink', self._on_hyperlink)

    @property
    def hyperlink_events(self):
        return {
            'on_hyperlink': (self._on_hyperlink, 'wx.adv.EVT_HYPERLINK', 'wxEVT_HYPERLINK'),
        }

    @property
    def kwargs(self):
        value = self.local_hyperlink_kwargs
        value.update(super(HyperlinkControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_hyperlink_kwargs = kwargs
        super(HyperlinkControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('hyperlink_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxHL_DEFAULT_STYLE', ' The default style for wxHyperlinkCtrl: '
                                   'BORDER_NONE|wxHL_CONTEXTMENU|wxHL_ALIGN_CENTRE.'),
            ('wxHL_ALIGN_LEFT', 'Align the text to the left.'),
            ('wxHL_ALIGN_RIGHT', 'Align the text to the right.'),
            ('wxHL_ALIGN_CENTRE', 'Centers the text.'),
            ('wxHL_CONTEXTMENU', 'Pop up a context menu when the hyperlink is right-clicked. '
                                 'The context menu contains a “Copy URL” menu item which is automatically '
                                 'handled by the hyperlink and which just copies in the clipboard the URL '
                                 '(not the label) of the control.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Hyperlink',
            'type': 'category',
            'help': 'Attributes of Hyperlink',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The control name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'value': self._label,
                    'name': 'label',
                    'help': 'Sets the hyperlink visual text.'
                },
                {
                    'label': 'url',
                    'type': 'string',
                    'value': self._url,
                    'name': 'url',
                    'help': 'Sets the hyperlink target url.'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the control style.'
                }]
        }]
        _event = [{
            'label': 'Hyperlink',
            'type': 'category',
            'help': 'Events of hyperlink',
            'child': [
                {
                    'label': 'on hyperlink',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_hyperlink,
                    'name': 'on_hyperlink',
                    'help': "Called when the hyperlink is cllicked."
                },
            ],
        }]
        _base_property, _base_event = super(HyperlinkControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @popup_errors_to_user
    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.adv.HyperlinkCtrl)(
            edition_frame, self.py_id, self._label, self._url, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/html/hyperlink.h>
        cls = register_cpp_class(self.project, 'wxHyperlinkCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="hyperlink control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        self.add_cpp_required_header('#include <wx/hyperlink.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxHyperlinkCtrl({owner}, {self._id}, {self.cc_pos_str}, ' \
               f'"{self._label}", "{self._url}", {self.cc_size_str}, {self.cc_style_str});\n'
        code += super(HyperlinkControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        self.add_python_import('wx.adv')
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.adv.HyperlinkCtrl({owner}, {self.py_id_str}, "{self._label}", "{self._url}", ' \
               f'{self.py_pos_str}, {self.py_size_str}, {self.py_style_str})\n'
        code += super(HyperlinkControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


