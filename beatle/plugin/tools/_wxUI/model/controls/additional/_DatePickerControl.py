import os
import wx
import wx.adv
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


@with_style
@extend_events('date_picker_events')
class DatePickerControl(WindowControl):
    """"This class represents a dir picker"""

    style_list = (wx.adv.DP_DEFAULT, wx.adv.DP_SPIN, wx.adv.DP_DROPDOWN, wx.adv.DP_ALLOWNONE, wx.adv.DP_SHOWCENTURY)
    style_list_py_text = ('wx.adv.DP_DEFAULT', 'wx.adv.DP_SPIN', 'wx.adv.DP_DROPDOWN',
                          'wx.adv.DP_ALLOWNONE', 'wx.adv.DP_SHOWCENTURY')
    style_list_cc_text = ('wxDP_DEFAULT', 'wxDP_SPIN', 'wxDP_DROPDOWN', 'wxDP_ALLOWNONE', 'wxDP_SHOWCENTURY')

    def __init__(self, **kwargs):
        self._style = 1
        # events
        self._on_date_changed = ''
        super(DatePickerControl, self).__init__(**kwargs)

    @property
    def local_date_picker_kwargs(self):
        return {
            'style': self._style,
            # events
            'on_date_changed': self._on_date_changed,
        }

    @local_date_picker_kwargs.setter
    def local_date_picker_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._on_date_changed = kwargs.get('on_date_changed', self._on_date_changed)

    @property
    def date_picker_events(self):
        return {
            'on_date_changed': (self._on_date_changed, 'wx.EVT_DATE_CHANGED', 'wxEVT_DATE_CHANGED'),
        }

    @property
    def kwargs(self):
        value = self.local_date_picker_kwargs
        value.update(super(DatePickerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_date_picker_kwargs = kwargs
        super(DatePickerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('date_picker')

    @property
    def style_attribute_options(self):
        return [
            ('wxDP_DEFAULT', "Creates a control with the style that is best supported for the current platform "
                             "(currently wxDP_SPIN under Windows and OSX/Cocoa and wxDP_DROPDOWN elsewhere)."),
            ('wxDP_SPIN',  "Creates a control without a month calendar drop down but with spin-control-like "
                           "arrows to change individual date components. This style is not supported by the "
                           "generic version."),
            ('wxDP_DROPDOWN', "Creates a control with a month calendar drop-down part from which the user can "
                              "select a date. This style is not supported in OSX/Cocoa native version."),
            ('wxDP_ALLOWNONE', "With this style, the control allows the user to not enter any valid date at all. "
                               "Without it - the default - the control always has some valid date. This style is "
                               "not supported in OSX/Cocoa native version."),
            ('wxDP_SHOWCENTURY', 'Forces display of the century in the default date format. Without this style the '
                                 'century could be displayed, or not, depending on the default date representation '
                                 'in the system. This style is not supported in OSX/Cocoa native version currently.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Date picker',
            'type': 'category',
            'help': 'Attributes of date picker.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The dir picker control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the file picker style.'
                },
            ]
        }]
        _event = [{
            'label': 'Date picker',
            'type': 'category',
            'help': 'Events of date picker',
            'child': [
                {
                    'label': 'on date changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_date_changed,
                    'name': 'on_date_changed',
                    'help': '  Process a wxEVT_DATE_CHANGED event, which fires when the user changes the current'
                            ' selection in the control.'
                }],
            }]
        _base_property, _base_event = super(DatePickerControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.adv.DatePickerCtrl)(
            edition_frame, self.py_id, wx.DefaultDateTime, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/datectrl.h>
        # requires #include <wx/dateevt.h>

        class_ = register_cpp_class(self.project, 'wxDatePickerCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="date picker control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/datectrl.h>')
        self.add_cpp_required_header('#include <wx/dateevt.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxDatePickerCtrl({owner}, {self._id}, ' \
               f'wxDefaultDateTime, {self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n'
        code += super(DatePickerControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""

        self.add_python_import('wx.adv')

        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.adv.DatePickerCtrl({owner}, {self.py_id_str}, wx.DefaultDateTime, ' \
               f'{self.py_pos_str}, {self.py_size_str}, {self.py_style_str})\n'
        code += super(DatePickerControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


