import os
import wx
import wx.html
from .._WindowControl import WindowControl
from beatle.model import cc
from ....lib import register_cpp_class, with_style, extend_events


@with_style
@extend_events('html_window_events')
class HtmlWindowControl(WindowControl):
    """"This class represents a button"""

    style_list = (wx.html.HW_SCROLLBAR_NEVER, wx.html.HW_SCROLLBAR_AUTO, wx.html.HW_NO_SELECTION)
    style_list_py_text = ('wx.html.HW_SCROLLBAR_NEVER', 'wx.html.HW_SCROLLBAR_AUTO', 'wx.html.HW_NO_SELECTION',)
    style_list_cc_text = ('wxHW_SCROLLBAR_NEVER', 'wxHW_SCROLLBAR_AUTO', 'wxHW_NO_SELECTION',)

    def __init__(self, **kwargs):
        self._style = 2
        # events
        self._on_cell_clicked = ''
        self._on_cell_hover = ''
        self._on_link_clicked = ''

        super(HtmlWindowControl, self).__init__(**kwargs)
        self.set_kwargs(kwargs)

    @property
    def local_htmlwindow_kwargs(self):
        return {
            'style': self._style,
            'on_cell_clicked': self._on_cell_clicked,
            'on_cell_hover': self._on_cell_hover,
            'on_link_clicked': self._on_link_clicked,
        }

    @local_htmlwindow_kwargs.setter
    def local_htmlwindow_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._on_cell_clicked = kwargs.get('on_cell_clicked', self._on_cell_clicked)
        self._on_cell_hover = kwargs.get('on_cell_hover', self._on_cell_hover)
        self._on_link_clicked = kwargs.get('on_link_clicked', self._on_link_clicked)

    @property
    def html_window_events(self):
        return {
            'on_cell_clicked': (self._on_cell_clicked, 'wx.EVT_COMMAND_HTML_CELL_CLICKED',
                                'wxEVT_COMMAND_HTML_CELL_CLICKED'),
            'on_cell_hover': (self._on_cell_hover, 'wx.EVT_COMMAND_HTML_CELL_HOVER',
                              'wxEVT_COMMAND_HTML_CELL_HOVER'),
            'on_link_clicked': (self._on_link_clicked, 'wx.EVT_COMMAND_HTML_LINK_CLICKED',
                                'wxEVT_COMMAND_HTML_LINK_CLICKED'),
        }

    @property
    def kwargs(self):
        value = self.local_htmlwindow_kwargs
        value.update(super(HtmlWindowControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_htmlwindow_kwargs = kwargs
        super(HtmlWindowControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('html_window')

    @property
    def style_attribute_options(self):
        return [
            ('wxHW_SCROLLBAR_NEVER', 'Never display scrollbars, not even when the page is larger than the window.'),
            ('wxHW_SCROLLBAR_AUTO', 'Display scrollbars only if page’s size exceeds window’s size..'),
            ('wxHW_NO_SELECTION', 'Don’t allow the user to select text.'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'HTML window',
            'type': 'category',
            'help': 'Attributes of HTML window',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the control style.'
                }]
        }]
        _event = [{
            'label': 'HTML window',
            'type': 'category',
            'help': 'Events of HTML  window',
            'child': [
                {
                    'label': 'on cell clicked',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_cell_clicked,
                    'name': 'on_cell_clicked',
                    'help': "Called when a html cell object was clicked."
                },
                {
                    'label': 'on cell hover',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_cell_hover,
                    'name': 'on_cell_hover',
                    'help': "called when the mouse passed over a html cell."
                },
                {
                    'label': 'on link clicked',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_link_clicked,
                    'name': 'on_link_clicked',
                    'help': "called when a html link was clicked."
                },
            ],
        }]
        _base_property, _base_event = super(HtmlWindowControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_sample_content(self):
        from .... import res
        res_dir = os.path.split(os.path.abspath(res.__file__))[0]
        cwd = os.getcwd()
        os.chdir(res_dir)
        self._edition_window.LoadFile('_sample.html')
        os.chdir(cwd)

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.html.HtmlWindow)(
            edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self.create_sample_content()
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/html/htmlwin.h>
        cls = register_cpp_class(self.project, 'wxHtmlWindow')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="HTML window",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/html/htmlwin.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f"{self._name} = new wxHtmlWindow({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"
        code += super(HtmlWindowControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.html.HtmlWindow({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(HtmlWindowControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


