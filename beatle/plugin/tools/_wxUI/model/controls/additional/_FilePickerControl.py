import os
import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


@with_style
@extend_events('file_picker_events')
class FilePickerControl(WindowControl):
    """"This class represents a colour picker"""

    style_list = (wx.FLP_DEFAULT_STYLE, wx.FLP_USE_TEXTCTRL, wx.FLP_OPEN, wx.FLP_SAVE,
                  wx.FLP_OVERWRITE_PROMPT, wx.FLP_FILE_MUST_EXIST, wx.FLP_CHANGE_DIR,
                  wx.FLP_SMALL)
    style_list_py_text = (
        'wx.FLP_DEFAULT_STYLE', 'wx.FLP_USE_TEXTCTRL', 'wx.FLP_OPEN', 'wx.FLP_SAVE',
        'wx.FLP_OVERWRITE_PROMPT', 'wx.FLP_FILE_MUST_EXIST', 'wx.FLP_CHANGE_DIR',
        'wx.FLP_SMALL')
    style_list_cc_text = (
        'wxFLP_DEFAULT_STYLE', 'wxFLP_USE_TEXTCTRL', 'wxFLP_OPEN', 'wxFLP_SAVE',
        'wxFLP_OVERWRITE_PROMPT', 'wxFLP_FILE_MUST_EXIST', 'wxFLP_CHANGE_DIR',
        'wxFLP_SMALL')

    def __init__(self, **kwargs):
        self._value = ''
        self._message = ''
        self._wildcard = ''
        self._style = 1
        # events
        self._on_file_changed = ''
        super(FilePickerControl, self).__init__(**kwargs)

    @property
    def local_file_picker_kwargs(self):
        return {
            'value': self._value,
            'message': self._message,
            'wildcard': self._wildcard,
            'style': self._style,
            # events
            'on_file_changed': self._on_file_changed,
        }

    @local_file_picker_kwargs.setter
    def local_file_picker_kwargs(self, kwargs):
        self._value = kwargs.get('value', self._value)
        self._message = kwargs.get('message', self._message)
        self._wildcard = kwargs.get('wildcard', self._wildcard)
        self._style = kwargs.get('style', self._style)
        self._on_file_changed = kwargs.get('on_file_changed', self._on_file_changed)

    @property
    def file_picker_events(self):
        return {
            'on_file_changed': (self._on_file_changed, 'wx.EVT_FILEPICKER_CHANGED', 'wxEVT_FILEPICKER_CHANGED'),
        }

    @property
    def kwargs(self):
        value = self.local_file_picker_kwargs
        value.update(super(FilePickerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_file_picker_kwargs = kwargs
        super(FilePickerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('file_picker')

    @property
    def style_attribute_options(self):
        return [
            ('wxFLP_DEFAULT_STYLE', "The default style: includes wxFLP_OPEN | wxFLP_FILE_MUST_EXIST and, "
                                    "under wxMSW and wxOSX, wxFLP_USE_TEXTCTRL."),
            ('wxFLP_USE_TEXTCTRL',  "Creates a text control to the left of the picker button which is completely "
                                    "managed by the wx.FilePickerCtrl and which can be used by the user to specify "
                                    "a path (see SetPath). The text control is automatically synchronized with "
                                    "button’s value. Use functions defined in wx.PickerBase to modify the text"
                                    " control."),
            ('wxFLP_OPEN', "Creates a picker which allows the user to select a file to open.."),
            ('wxFLP_SAVE', "Creates a picker which allows the user to select a file to save."),
            ('wxFLP_OVERWRITE_PROMPT', 'Can be combined with wx.FLP_SAVE only: ask confirmation to the user '
                                       'before selecting a file.'),
            ('wxFLP_FILE_MUST_EXIST', 'Can be combined with wx.FLP_OPEN only: the file selected in the popup '
                                      'wx.FileDialog must be an existing file. Notice that it still remains possible '
                                      'for the user to enter a non-existent file name in the text control if '
                                      'FLP_USE_TEXTCTRL is also used, this flag is a hint for the user rather than '
                                      'a guarantee that the selected file does exist for the program.'),
            ('wxFLP_CHANGE_DIR', 'Change current working directory on each user file selection change.'),
            ('wxFLP_SMALL', 'Use smaller version of the control with a small “…” button instead of the normal '
                            '“Browse” one. This flag is new since wxWidgets 2.9.3.')
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'File picker',
            'type': 'category',
            'help': 'Attributes of file picker.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The file picker control name."
                },
                {
                    'label': 'value',
                    'type': 'string',
                    'read_only': False,
                    'value': self._value,
                    'name': 'value',  # kwarg value
                    'help': "The default file value."
                },
                {
                    'label': 'message',
                    'type': 'string',
                    'read_only': False,
                    'value': self._message,
                    'name': 'message',  # kwarg value
                    'help': "The message shown in the file dialog caption."
                },
                {
                    'label': 'wildcard',
                    'type': 'string',
                    'read_only': False,
                    'value': self._wildcard,
                    'name': 'wildcard',  # kwarg value
                    'help': "The wildcard file pattern."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the file picker style.'
                },
            ]
        }]
        _event = [{
            'label': 'Font picker',
            'type': 'category',
            'help': 'Events of font picker',
            'child': [
                {
                    'label': 'on file changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_file_changed,
                    'name': 'on_file_changed',
                    'help': '  The user changed the file selected in the control either using the button or using '
                            'text control (see wx.FLP_USE_TEXTCTRL; note that in this case the event is fired only '
                            'if the user’s input is valid, e.g. an existing file path if wx.FLP_FILE_MUST_EXIST '
                            'was given).'
                }],
            }]
        _base_property, _base_event = super(FilePickerControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.FilePickerCtrl)(
            edition_frame, self.py_id, self._value, self._message, self._wildcard, self.py_pos,
            self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/filepicker.h>
        class_ = register_cpp_class(self.project, 'wxFilePickerCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="file picker control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/filepicker.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        value = self._value.strip()
        if len(value):
            value = f'"{value}"'
        else:
            value = 'wxEmptyString'
        message = self._message.strip()
        if len(message):
            message = f'"{message}"'
        else:
            message = 'wxEmptyString'
        wildcard = self._wildcard.strip()
        if len(wildcard):
            wildcard = f'"{wildcard}"'
        else:
            wildcard = 'wxEmptyString'
        code = f'{self._name} = new wxFilePickerCtrl({owner}, {self._id}, {value}, {message}, ' \
               f'{wildcard}, {self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n'
        code += super(FilePickerControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        value = self._value.strip()
        if len(value):
            value = f'"{value}"'
        else:
            value = 'wx.EmptyString'
        message = self._message.strip()
        if len(message):
            message = f'"{message}"'
        else:
            message = 'wx.EmptyString'
        wildcard = self._wildcard.strip()
        if len(wildcard):
            wildcard = f'"{wildcard}"'
        else:
            wildcard = 'wx.EmptyString'
        code = f'{this} = wx.FilePickerCtrl({owner}, {self.py_id_str}, {value}, {message}, ' \
               f'{wildcard}, {self.py_pos_str}, {self.py_size_str}, {self.py_style_str})\n'
        code += super(FilePickerControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code
