import os
import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


@with_style
@extend_events('font_picker_events')
class FontPickerControl(WindowControl):
    """"This class represents a colour picker"""

    style_list = (wx.FNTP_DEFAULT_STYLE, wx.FNTP_USE_TEXTCTRL, wx.FNTP_FONTDESC_AS_LABEL, wx.FNTP_USEFONT_FOR_LABEL)
    style_list_py_text = ('wx.FNTP_DEFAULT_STYLE', 'wx.FNTP_USE_TEXTCTRL',
                          'wx.FNTP_FONTDESC_AS_LABEL', 'wx.FNTP_USEFONT_FOR_LABEL')
    style_list_cc_text = ('wxFNTP_DEFAULT_STYLE', 'wxFNTP_USE_TEXTCTRL',
                          'wxFNTP_FONTDESC_AS_LABEL', 'wxFNTP_USEFONT_FOR_LABEL')

    def __init__(self, **kwargs):
        self._value = (False, '-1; Default; ; Normal; Normal; Not Underlined')
        self._style = 1
        # events
        self._on_font_changed = ''
        super(FontPickerControl, self).__init__(**kwargs)

    @property
    def local_font_picker_kwargs(self):
        return {
            'value': self._value,
            'style': self._style,
            # events
            'on_font_changed': self._on_font_changed,
        }

    @local_font_picker_kwargs.setter
    def local_font_picker_kwargs(self, kwargs):
        self._value = tuple(kwargs.get('value', self._value))
        self._style = kwargs.get('style', self._style)
        self._on_font_changed = kwargs.get('on_font_changed', self._on_font_changed)

    @property
    def font_picker_events(self):
        return {
            'on_font_changed': (self._on_font_changed, 'wx.EVT_FONTPICKER_CHANGED', 'wxEVT_FONTPICKER_CHANGED'),
        }

    @property
    def kwargs(self):
        value = self.local_font_picker_kwargs
        value.update(super(FontPickerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_font_picker_kwargs = kwargs
        super(FontPickerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('font_picker')

    @property
    def style_attribute_options(self):
        return [
            ('wxFNTP_DEFAULT_STYLE', "The default style: wxFNTP_FONTDESC_AS_LABEL | wxFNTP_USEFONT_FOR_LABEL."),
            ('wxFNTP_USE_TEXTCTRL', "Creates a text control to the left of the picker button which is completely "
                                    "managed by the wx.FontPickerCtrl and which can be used by the user to specify "
                                    "a font (see SetSelectedFont). The text control is automatically synchronized "
                                    "with button’s value. Use functions defined in wx.PickerBase to modify the text"
                                    " control."),
            ('wxFNTP_FONTDESC_AS_LABEL', "Keeps the label of the button updated with the fontface name and the font "
                                         "size. E.g. choosing “Times New Roman bold, italic with size 10” from the "
                                         "fontdialog, will update the label (overwriting any previous label) with the"
                                         " “Times New Roman, 10” text."),
            ('wxFNTP_USEFONT_FOR_LABEL', "Uses the currently selected font to draw the label of the button."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Font picker',
            'type': 'category',
            'help': 'Attributes of font picker.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The font picker control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the colour picker style.'
                },
                {
                    'label': 'font',
                    'type': 'conditional font',
                    'read_only': False,
                    'value': self._value,
                    'name': 'value',
                    'help': 'Sets the start font value.'
                },
            ]
        }]
        _event = [{
            'label': 'Font picker',
            'type': 'category',
            'help': 'Events of font picker',
            'child': [
                {
                    'label': 'on font changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_font_changed,
                    'name': 'on_font_changed',
                    'help': ' The user changed the font selected in the control either using the button or using'
                            'text control (see wx.FNTP_USE_TEXTCTRL; note that in this case the event is fired only '
                            'if the user’s input is valid, i.e. recognizable).'
                }],
            }]
        _base_property, _base_event = super(FontPickerControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        if self._value[0]:
            font = wx.Font(self._value[1])
        else:
            font = wx.NullFont
        self._edition_window = uiMockupControl(wx.FontPickerCtrl)(
            edition_frame, self.py_id, font, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # required #include <wx/font.h>
        # requires #include <wx/fontpicker.h>
        class_ = register_cpp_class(self.project, 'wxFontPickerCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="font picker control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/font.h>')
        self.add_cpp_required_header('#include <wx/fontpicker.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        if self._value[0]:
            font = f'wxFont("{self._value[1]}")'
        else:
            font = "wxNullFont"
        code = f'{self._name} = new wxFontPickerCtrl({owner}, {self._id}, {font}, ' \
               f'{self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n'
        code += super(FontPickerControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        if self._value[0]:
            font = f'wx.Font("{self._value[1]}")'
        else:
            font = "wx.NullFont"
        code = f'{this} = wx.FontPickerCtrl({owner}, {self.py_id_str}, {font}, ' \
               f'{self.py_pos_str}, {self.py_size_str}, {self.py_style_str})\n'
        code += super(FontPickerControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


