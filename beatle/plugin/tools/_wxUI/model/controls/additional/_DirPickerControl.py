import os
import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


@with_style
@extend_events('dir_picker_events')
class DirPickerControl(WindowControl):
    """"This class represents a dir picker"""

    style_list = (wx.DIRP_DEFAULT_STYLE, wx.DIRP_USE_TEXTCTRL,
                  wx.DIRP_DIR_MUST_EXIST, wx.DIRP_CHANGE_DIR,
                  wx.DIRP_SMALL)
    style_list_py_text = (
        'wx.DIRP_DEFAULT_STYLE', 'wx.DIRP_USE_TEXTCTRL',
        'wx.DIRP_DIR_MUST_EXIST', 'wx.DIRP_CHANGE_DIR',
        'wx.DIRP_SMALL')
    style_list_cc_text = (
        'wxDIRP_DEFAULT_STYLE', 'wxDIRP_USE_TEXTCTRL',
        'wxDIRP_DIR_MUST_EXIST', 'wxDIRP_CHANGE_DIR',
        'wxDIRP_SMALL')

    def __init__(self, **kwargs):
        self._value = ''
        self._message = ''
        self._style = 1
        # events
        self._on_dir_changed = ''
        super(DirPickerControl, self).__init__(**kwargs)

    @property
    def local_dir_picker_kwargs(self):
        return {
            'value': self._value,
            'message': self._message,
            'style': self._style,
            # events
            'on_dir_changed': self._on_dir_changed,
        }

    @local_dir_picker_kwargs.setter
    def local_dir_picker_kwargs(self, kwargs):
        self._value = kwargs.get('value', self._value)
        self._message = kwargs.get('message', self._message)
        self._style = kwargs.get('style', self._style)
        self._on_dir_changed = kwargs.get('on_dir_changed', self._on_dir_changed)

    @property
    def dir_picker_events(self):
        return {
            'on_dir_changed': (self._on_dir_changed, 'wx.EVT_DIRPICKER_CHANGED', 'wxEVT_DIRPICKER_CHANGED'),
        }

    @property
    def kwargs(self):
        value = self.local_dir_picker_kwargs
        value.update(super(DirPickerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_dir_picker_kwargs = kwargs
        super(DirPickerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('dir_picker')

    @property
    def style_attribute_options(self):
        return [
            ('wxDIRP_DEFAULT_STYLE', "The default style: includes wxDIRP_DIR_MUST_EXIST and, under wxMSW only, "
                                     "wxDIRP_USE_TEXTCTRL."),
            ('wxDIRP_USE_TEXTCTRL',  "Creates a text control to the left of the picker button which is completely "
                                     "managed by the wx.DirPickerCtrl and which can be used by the user to specify"
                                     " a path (see SetPath). The text control is automatically synchronized with"
                                     " button’s value. Use functions defined in wx.PickerBase to modify the text"
                                     " control."),
            ('wxDIRP_DIR_MUST_EXIST', "Creates a picker which allows selecting only existing directories in the "
                                      "popup wxDirDialog. Notice that, as with FLP_FILE_MUST_EXIST ,"
                                      " it is still possible to enter a non-existent directory even when this "
                                      "file is specified if DIRP_USE_TEXTCTRL style is also used. Also note "
                                      "that if DIRP_USE_TEXTCTRL is not used, the native wxGTK implementation "
                                      "always uses this style as it doesn’t support selecting "
                                      "non-existent directories."),
            ('wxDIRP_CHANGE_DIR', "Change current working directory on each user directory selection change."),
            ('wxDIRP_SMALL', 'Use smaller version of the control with a small “…” button instead of the normal '
                             '“Browse” one. This flag is new since wxWidgets 2.9.3.'),

        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Dir picker',
            'type': 'category',
            'help': 'Attributes of dir picker.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The dir picker control name."
                },
                {
                    'label': 'value',
                    'type': 'string',
                    'read_only': False,
                    'value': self._value,
                    'name': 'value',  # kwarg value
                    'help': "The default dir value."
                },
                {
                    'label': 'message',
                    'type': 'string',
                    'read_only': False,
                    'value': self._message,
                    'name': 'message',  # kwarg value
                    'help': "The message shown in the dir dialog caption."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the file picker style.'
                },
            ]
        }]
        _event = [{
            'label': 'Dir picker',
            'type': 'category',
            'help': 'Events of dir picker',
            'child': [
                {
                    'label': 'on dir changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dir_changed,
                    'name': 'on_dir_changed',
                    'help': '  The user changed the directory selected in the control either using the button or '
                            'using text control (see wxDIRP_USE_TEXTCTRL; note that in this case the event is fired '
                            'only if the user’s input is valid, e.g. an existing directory path).'
                }],
            }]
        _base_property, _base_event = super(DirPickerControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.DirPickerCtrl)(
            edition_frame, self.py_id, self._value, self._message, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/filepicker.h>
        class_ = register_cpp_class(self.project, 'wxDirPickerCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="dir picker control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/filepicker.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        value = self._value.strip()
        if len(value):
            value = f'"{value}"'
        else:
            value = 'wxEmptyString'
        message = self._message.strip()
        if len(message):
            message = f'"{message}"'
        else:
            message = 'wxEmptyString'
        code = f'{self._name} = new wxDirPickerCtrl({owner}, {self._id}, {value}, {message}, ' \
               f'{self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n'
        code += super(DirPickerControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        value = self._value.strip()
        if len(value):
            value = f'"{value}"'
        else:
            value = 'wx.EmptyString'
        message = self._message.strip()
        if len(message):
            message = f'"{message}"'
        else:
            message = 'wx.EmptyString'
        code = f'{this} = wx.DirPickerCtrl({owner}, {self.py_id_str}, {value}, {message}, ' \
               f'{self.py_pos_str}, {self.py_size_str}, {self.py_style_str})\n'
        code += super(DirPickerControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


