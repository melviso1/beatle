import os
import wx
import wx.richtext as wre
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import register_cpp_class, with_style, extend_events


@with_style
@extend_events('rich_text_events')
class RichTextControl(WindowControl):
    """"This class represents a button"""

    style_list = (wre.RE_CENTER_CARET, wx.TE_PROCESS_ENTER, wre.RE_READONLY, )
    style_list_py_text = ('wx.richtext.RE_CENTER_CARET', 'wx.TE_PROCESS_ENTER', 'wx.richtext.RE_READONLY', )
    style_list_cc_text = ('wxRE_CENTER_CARET', 'wxTE_PROCESS_ENTER', 'wxRE_READONLY', )

    def __init__(self, **kwargs):
        self._style = 1
        # events
        self._on_text = ''
        self._on_text_enter = ''
        self._on_text_url = ''
        self._on_left_click = ''
        self._on_right_click = ''
        self._on_middle_click = ''
        self._on_left_dclick = ''
        self._on_return = ''
        self._on_character = ''
        self._on_consuming_character = ''
        self._on_delete = ''
        self._on_style_changed = ''
        self._on_stylesheet_changed = ''
        self._on_stylesheet_replacing = ''
        self._on_stylesheet_replaced = ''
        self._on_properties_changed = ''
        self._on_content_inserted = ''
        self._on_content_deleted = ''
        self._on_buffer_reset = ''
        self._on_selection_changed = ''
        self._on_focus_object_changed = ''
        super(RichTextControl, self).__init__(**kwargs)
        # use window default styles
        self._window_style = 4096 | 16 | 32 | 8192
        # self.subitem_adaptor.proportion = 1

    @property
    def local_rich_text_kwargs(self):
        return {
            'style': self._style,
            # events
            'on_text': self._on_text,
            'on_text_enter': self._on_text_enter,
            'on_text_url': self._on_text_url,
            'on_left_click': self._on_left_click,
            'on_right_click': self._on_right_click,
            'on_middle_click': self._on_middle_click,
            'on_left_dclick': self._on_left_dclick,
            'on_return': self._on_return,
            'on_character': self._on_character,
            'on_consuming_character': self._on_consuming_character,
            'on_delete': self._on_delete,
            'on_style_changed': self._on_style_changed,
            'on_stylesheet_changed': self._on_stylesheet_changed,
            'on_stylesheet_replacing': self._on_stylesheet_replacing,
            'on_stylesheet_replaced': self._on_stylesheet_replaced,
            'on_properties_changed': self._on_properties_changed,
            'on_content_inserted': self._on_content_inserted,
            'on_content_deleted': self._on_content_deleted,
            'on_buffer_reset': self._on_buffer_reset,
            'on_selection_changed': self._on_selection_changed,
            'on_focus_object_changed': self._on_focus_object_changed,
        }

    @local_rich_text_kwargs.setter
    def local_rich_text_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._on_text_enter = kwargs.get('on_text_enter', self._on_text_enter)
        self._on_text = kwargs.get('on_text', self._on_text)
        self._on_text_url = kwargs.get('on_text_url', self._on_text_url)
        self._on_left_click = kwargs.get('on_left_click', self._on_left_click)
        self._on_right_click = kwargs.get('on_right_click', self._on_right_click)
        self._on_middle_click = kwargs.get('on_middle_click', self._on_middle_click)
        self._on_left_dclick = kwargs.get('on_left_dclick', self._on_left_dclick)
        self._on_return = kwargs.get('on_return', self._on_return)
        self._on_character = kwargs.get('on_character', self._on_character)
        self._on_consuming_character = kwargs.get('on_consuming_character', self._on_consuming_character)
        self._on_delete = kwargs.get('on_delete', self._on_delete)
        self._on_style_changed = kwargs.get('on_style_changed', self._on_style_changed)
        self._on_stylesheet_changed = kwargs.get('on_stylesheet_changed', self._on_stylesheet_changed)
        self._on_stylesheet_replacing = kwargs.get('on_stylesheet_replacing', self._on_stylesheet_replacing)
        self._on_stylesheet_replaced = kwargs.get('on_stylesheet_replaced', self._on_stylesheet_replaced)
        self._on_properties_changed = kwargs.get('on_properties_changed', self._on_properties_changed)
        self._on_content_inserted = kwargs.get('on_content_inserted', self._on_content_inserted)
        self._on_content_deleted = kwargs.get('on_content_deleted', self._on_content_deleted)
        self._on_buffer_reset = kwargs.get('on_buffer_reset', self._on_buffer_reset)
        self._on_selection_changed = kwargs.get('on_selection_changed', self._on_selection_changed)
        self._on_focus_object_changed = kwargs.get('on_focus_object_changed', self._on_focus_object_changed)

    @property
    def rich_text_events(self):
        return {
            'on_text': (self._on_text, 'wx.EVT_TEXT', 'wxEVT_TEXT'),
            'on_text_enter': (self._on_text_enter, 'wx.EVT_TEXT_ENTER', 'wxEVT_TEXT_ENTER'),
            'on_text_url': (self._on_text_url, 'wx.EVT_TEXT_URL', 'wxEVT_TEXT_URL'),
            'on_left_click': (self._on_left_click, 'wx.EVT_RICHTEXT_LEFT_CLICK', 'wxEVT_RICHTEXT_LEFT_CLICK'),
            'on_right_click': (self._on_right_click, 'wx.EVT_RICHTEXT_RIGHT_CLICK', 'wxEVT_RICHTEXT_RIGHT_CLICK'),
            'on_middle_click': (self._on_middle_click, 'wx.EVT_RICHTEXT_MIDDLE_CLICK', 'wxEVT_RICHTEXT_MIDDLE_CLICK'),
            'on_left_dclick': (self._on_left_dclick, 'wx.EVT_RICHTEXT_LEFT_DCLICK', 'wxEVT_RICHTEXT_LEFT_DCLICK'),
            'on_return': (self._on_return, 'wx.EVT_RICHTEXT_RETURN', 'wxEVT_RICHTEXT_RETURN'),
            'on_character': (self._on_character, 'wx.EVT_RICHTEXT_CHARACTER', 'wxEVT_RICHTEXT_CHARACTER'),
            'on_consuming_character': (self._on_consuming_character, 'wx.EVT_RICHTEXT_CONSUMING_CHARACTER',
                                       'wxEVT_RICHTEXT_CONSUMING_CHARACTER'),
            'on_delete': (self._on_delete, 'wx.EVT_RICHTEXT_DELETE', 'wxEVT_RICHTEXT_DELETE'),
            'on_style_changed': (self._on_style_changed, 'wx.EVT_RICHTEXT_STYLE_CHANGED',
                                 'wxEVT_RICHTEXT_STYLE_CHANGED'),
            'on_stylesheet_changed': (self._on_stylesheet_changed, 'wx.EVT_RICHTEXT_STYLESHEET_CHANGED',
                                      'wxEVT_RICHTEXT_STYLESHEET_CHANGED'),
            'on_stylesheet_replacing': (self._on_stylesheet_replacing, 'wx.EVT_RICHTEXT_STYLESHEET_REPLACING',
                                         'wxEVT_RICHTEXT_STYLESHEET_REPLACING'),
            'on_stylesheet_replaced': (self._on_stylesheet_replaced, 'wx.EVT_RICHTEXT_STYLESHEET_REPLACED',
                                       'wxEVT_RICHTEXT_STYLESHEET_REPLACED'),
            'on_properties_changed': (self._on_properties_changed, 'wx.EVT_RICHTEXT_PROPERTIES_CHANGED',
                                      'wxEVT_RICHTEXT_PROPERTIES_CHANGED'),
            'on_content_inserted': (self._on_content_inserted, 'wx.EVT_RICHTEXT_CONTENT_INSERTED',
                                    'wxEVT_RICHTEXT_CONTENT_INSERTED'),
            'on_content_deleted': (self._on_content_deleted, 'wx.EVT_RICHTEXT_CONTENT_DELETED',
                                   'wxEVT_RICHTEXT_CONTENT_DELETED'),
            'on_buffer_reset': (self._on_buffer_reset, 'wx.EVT_RICHTEXT_BUFFER_RESET',
                                'wxEVT_RICHTEXT_BUFFER_RESET'),
            'on_selection_changed': (self._on_selection_changed, 'wx.EVT_RICHTEXT_SELECTION_CHANGED',
                                     'wxEVT_RICHTEXT_SELECTION_CHANGED'),
            'on_focus_object_changed': ( self._on_focus_object_changed, 'wx.EVT_RICHTEXT_FOCUS_OBJECT_CHANGED',
                                         'wxEVT_RICHTEXT_FOCUS_OBJECT_CHANGED'),
        }

    @property
    def kwargs(self):
        value = self.local_rich_text_kwargs
        value.update(super(RichTextControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_rich_text_kwargs = kwargs
        super(RichTextControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('rich_text')

    @property
    def style_attribute_options(self):
        return [
            ('wxRE_CENTRE_CARET', "The control will try to keep the caret line centred vertically while editing. "
                                  "wxRE_CENTER_CARET is a synonym for this style."),
            ('wxTE_PROCESS_ENTER',
             "The control will generate the event wxEVT_TEXT_ENTER. Take in account that if this "
             "event is handled and not performing default behavior, could break that default button, "
             "if any, receives the enter key."),
            ('wxRE_READONLY', "The control will not be editable."),

        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Rich text',
            'type': 'category',
            'help': 'Attributes of rich text.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The richtext control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the richtext style.'
                }]
        }]
        _event = [{
            'label': 'RichText',
            'type': 'category',
            'help': 'Events of checkbox control',
            'child': [
                {
                    'label': 'on_text',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text,
                    'name': 'on_text',
                    'help': 'Called when the text changes.'
                },
                {
                    'label': 'on_text_enter',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text_enter,
                    'name': 'on_text_enter',
                    'help': 'Called when the user press enter (requires wxTE_PROCESS_ENTER style).'
                },
                {
                    'label': 'on_text_url',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_text_url,
                    'name': 'on_text_url',
                    'help': 'Called when mouse acts over url text (wxGTK and wxMSW).'
                },
                {
                    'label': 'on_left_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_left_click,
                    'name': 'on_left_click',
                    'help': 'Called when the user releases the left mouse button over an object.'
                },
                {
                    'label': 'on_right_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_right_click,
                    'name': 'on_right_click',
                    'help': 'Called when the user releases the right mouse button over an object.'
                },
                {
                    'label': 'on_middle_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_middle_click,
                    'name': 'on_middle_click',
                    'help': 'Called when the user releases the middle mouse button over an object.'
                },
                {
                    'label': 'on_left_dclick',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_left_dclick,
                    'name': 'on_left_dclick',
                    'help': 'Called when the user double-clicks an object.'
                },
                {
                    'label': 'on_return',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_return,
                    'name': 'on_return',
                    'help': 'Called when the user presses the return key. Valid event functions: GetFlags, GetPosition.'
                },
                {
                    'label': 'on_character',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_character,
                    'name': 'on_character',
                    'help': 'Called when the user presses a character key. '
                            'Valid event functions: GetFlags, GetPosition, GetCharacter.'
                },
                {
                    'label': 'on_consuming_character',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_consuming_character,
                    'name': 'on_consuming_character',
                    'help': 'Called when the user presses a character key but before it is processed and inserted into'
                            ' the control. Call Veto to prevent normal processing. Valid event functions: GetFlags, '
                            'GetPosition, GetCharacter, Veto.'
                },
                {
                    'label': 'on_delete',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_delete,
                    'name': 'on_delete',
                    'help': 'Called when the user presses the backspace or delete key.'
                            ' Valid event functions: GetFlags, GetPosition.'
                },
                {
                    'label': 'on_style_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_style_changed,
                    'name': 'on_style_changed',
                    'help': 'Called when styling has been applied to the control. '
                            'Valid event functions: GetPosition, GetRange.'
                },
                {
                    'label': 'on_stylesheet_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_stylesheet_changed,
                    'name': 'on_stylesheet_changed',
                    'help': 'Called when the controlâ€™s stylesheet has changed, for example the user added, '
                            'edited or deleted a style. Valid event functions: GetRange, GetPosition.'
                },
                {
                    'label': 'on_stylesheet_replacing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_stylesheet_replacing,
                    'name': 'on_stylesheet_replacing',
                    'help': 'Called when the controlâ€™s stylesheet is about to be replaced, for example '
                            'when a file is loaded into the control. Valid event functions: '
                            'Veto, GetOldStyleSheet, GetNewStyleSheet.'
                },
                {
                    'label': 'on_stylesheet_replaced',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_stylesheet_replaced,
                    'name': 'on_stylesheet_replaced',
                    'help': 'Called when the controlâ€™s stylesheet has been replaced, for example when a '
                            'file is loaded into the control. Valid event functions: '
                            'GetOldStyleSheet, GetNewStyleSheet.'
                },
                {
                    'label': 'on_properties_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_properties_changed,
                    'name': 'on_properties_changed',
                    'help': 'Called when properties have been applied to the control. '
                            'Valid event functions: GetPosition, GetRange.'
                },
                {
                    'label': 'on_content_inserted',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_content_inserted,
                    'name': 'on_content_inserted',
                    'help': 'Called when content has been inserted into the control. '
                            'Valid event functions: GetPosition, GetRange.'
                },
                {
                    'label': 'on_content_deleted',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_content_deleted,
                    'name': 'on_content_deleted',
                    'help': 'Called when content has been deleted from the control.'
                            ' Valid event functions: GetPosition, GetRange.'
                },
                {
                    'label': 'on_buffer_reset',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_buffer_reset,
                    'name': 'on_buffer_reset',
                    'help': 'Called when the buffer has been reset by deleting all content.'
                            ' You can use this to set a default style for the first new paragraph.'
                },
                {
                    'label': 'on_selection_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_selection_changed,
                    'name': 'on_selection_changed',
                    'help': 'Called when the selection range has changed.'
                },
                {
                    'label': 'on_focus_object_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_focus_object_changed,
                    'name': 'on_focus_object_changed',
                    'help': 'Called when the current focus object has changed. '
                }],
            }]
        _base_property, _base_event = super(RichTextControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_sample_content(self):
        """Create a sample content for the control"""
        from .... import res
        cwd = os.getcwd()
        res_dir = os.path.split(os.path.abspath(res.__file__))[0]
        r = self._edition_window

        textFont = wx.Font(12, wx.ROMAN, wx.NORMAL, wx.NORMAL)
        boldFont = wx.Font(12, wx.ROMAN, wx.NORMAL, wx.BOLD)
        italicFont = wx.Font(12, wx.ROMAN, wx.ITALIC, wx.NORMAL)
        font = wx.Font(12, wx.ROMAN, wx.NORMAL, wx.NORMAL)
        r.SetFont(font)
        r.BeginSuppressUndo()
        r.BeginTextColour(wx.Colour(0, 0, 0))
        r.BeginParagraphSpacing(0, 20)
        r.BeginAlignment(wx.TEXT_ALIGNMENT_CENTRE)
        r.BeginBold()
        r.BeginFontSize(14)
        r.WriteText("Welcome to wxRichTextCtrl, a wxWidgets control for editing and presenting styled text and images")
        r.EndFontSize()
        r.Newline()
        r.BeginItalic()
        r.WriteText("by Julian Smart")
        r.EndItalic()
        r.EndBold()
        r.Newline()
        os.chdir(res_dir)
        r.WriteImage(wx.Bitmap('zebra.xpm'))
        os.chdir(cwd)
        r.EndAlignment()
        r.Newline()
        r.Newline()
        r.WriteText("What can you do with this thing? ")
        os.chdir(res_dir)
        r.WriteImage(wx.Bitmap('smiley.xpm'))
        os.chdir(cwd)
        r.WriteText(" Well, you can change text ")
        r.BeginTextColour(wx.Colour(255, 0, 0))
        r.WriteText("colour, like this red bit.")
        r.EndTextColour()
        r.BeginTextColour(wx.Colour(0, 0, 255))
        r.WriteText(" And this blue bit.")
        r.EndTextColour()
        r.WriteText(" Naturally you can make things ")
        r.BeginBold()
        r.WriteText("bold ")
        r.EndBold()
        r.BeginItalic()
        r.WriteText("or italic ")
        r.EndItalic()
        r.BeginUnderline()
        r.WriteText("or underlined.")
        r.EndUnderline()
        r.BeginFontSize(14)
        r.WriteText(" Different font sizes on the same line is allowed, too.")
        r.EndFontSize()
        r.WriteText(" Next we'll show an indented paragraph.")
        r.BeginLeftIndent(60)
        r.Newline()
        r.WriteText("Indented paragraph.")
        r.EndLeftIndent()
        r.Newline()
        r.WriteText("Next, we'll show a first-line indent, achieved using BeginLeftIndent(100, -40).")
        r.BeginLeftIndent(100, -40)
        r.Newline()
        r.WriteText("It was in January, the most down-trodden month of an Edinburgh winter.")
        r.EndLeftIndent()
        r.Newline()
        r.WriteText("Numbered bullets are possible, again using subindents:")
        r.BeginNumberedBullet(1, 100, 60)
        r.Newline()
        r.WriteText("This is my first item. Note that wxRichTextCtrl doesn't automatically do numbering, "
                    "but this will be added later.")
        r.EndNumberedBullet()
        r.BeginNumberedBullet(2, 100, 60)
        r.Newline()
        r.WriteText("This is my second item.")
        r.EndNumberedBullet()
        r.Newline()
        r.WriteText("The following paragraph is right-indented:")
        r.BeginRightIndent(200)
        r.Newline()
        r.WriteText("It was in January, the most down-trodden month of an Edinburgh winter. An attractive woman "
                    "came into the cafe, which is nothing remarkable.")
        r.EndRightIndent()
        r.Newline()
        attr = wx.TextAttr()
        attr.SetFlags(wx.TEXT_ATTR_TABS)
        attr.SetTabs([400, 600, 800, 1000]);
        r.SetDefaultStyle(attr)
        r.WriteText("This line contains tabs:\tFirst tab\tSecond tab\tThird tab")
        r.Newline();
        r.WriteText("Other notable features of wxRichTextCtrl include:")
        r.BeginSymbolBullet('*', 100, 60)
        r.Newline()
        r.WriteText("Compatibility with wxTextCtrl API")
        r.EndSymbolBullet()
        r.WriteText("Note: this sample content was generated programmatically from within the MyFrame constructor "
                    "in the demo. The images were loaded from inline XPMs. Enjoy wxRichTextCtrl!")
        r.EndTextColour()
        r.EndSuppressUndo()

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wre.RichTextCtrl)(
            edition_frame, self.py_id, wx.EmptyString, self.py_pos, self.py_size, self.style_value)
        self._edition_window.ShouldInheritColours = lambda *args: False
        self.add_to_sizer(container, self._edition_window)
        self.create_sample_content()
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/richtext/richtextctrl.h>
        class_ = register_cpp_class(self.project, 'wxRichTextCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="rich text control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/richtext/richtextctrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f"{self._name} = new wxRichTextCtrl({owner}, {self._id}, wxEmptyString, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"
        code += super(RichTextControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.richtext')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = 'self.{name}'.format(name=window_parent.name)

        code = f'{this} = wx.richtext.RichTextCtrl({owner}, {self.py_id_str}, wx.EmptyString, ' \
               f'{self.py_pos_str}, {self.py_size_str}, {self.py_style_str})\n'
        code += super(RichTextControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


