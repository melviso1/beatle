import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import register_cpp_class, with_style, extend_events


@with_style
@extend_events('spin_button_events')
class SpinButtonControl(WindowControl):
    """"This class represents a colour picker"""

    style_list = (wx.SP_HORIZONTAL, wx.SP_VERTICAL, wx.SP_ARROW_KEYS, wx.SP_WRAP)
    style_list_py_text = ('wx.SP_HORIZONTAL', 'wx.SP_VERTICAL', 'wx.SP_ARROW_KEYS', 'wx.SP_WRAP')
    style_list_cc_text = ('wxSP_HORIZONTAL', 'wxSP_VERTICAL', 'wxSP_ARROW_KEYS', 'wxSP_WRAP')

    def __init__(self, **kwargs):
        self._style = 2
        # events
        self._on_spin = ''
        self._on_spin_up = ''
        self._on_spin_down = ''
        super(SpinButtonControl, self).__init__(**kwargs)

    @property
    def local_spin_button_kwargs(self):
        return {
            'style': self._style,
            # events
            'on_spin': self._on_spin,
            'on_spin_up': self._on_spin_up,
            'on_spin_down': self._on_spin_down,
        }

    @local_spin_button_kwargs.setter
    def local_spin_button_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._on_spin = kwargs.get('on_spin', self._on_spin)
        self._on_spin_up = kwargs.get('on_spin_up', self._on_spin_up)
        self._on_spin_down = kwargs.get('on_spin_down', self._on_spin_down)

    @property
    def spin_button_events(self):
        return {
            'on_spin': (self._on_spin, 'wx.EVT_SPIN', 'wxEVT_SPIN'),
            'on_spin_up': (self._on_spin_up, 'wx.EVT_SPIN_UP', 'wxEVT_SPIN_UP'),
            'on_spin_down': (self._on_spin_down, 'wx.EVT_SPIN_DOWN', 'wxEVT_SPIN_DOWN'),
        }

    @property
    def kwargs(self):
        value = self.local_spin_button_kwargs
        value.update(super(SpinButtonControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_spin_button_kwargs = kwargs
        super(SpinButtonControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('spin_button')

    @property
    def style_attribute_options(self):
        return [
            ('wxSP_HORIZONTAL', "Specifies a horizontal spin button (note that this style is not supported in wxGTK)."),
            ('wxSP_VERTICAL', "Specifies a vertical spin button."),
            ('wxSP_ARROW_KEYS', "The user can use arrow keys to change the value."),
            ('wxSP_WRAP', "The value wraps at the minimum and maximum. "),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Spin button',
            'type': 'category',
            'help': 'Attributes of spin button.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The spin button name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the colour picker style.'
                },]
        }]
        _event = [{
            'label': 'Spin',
            'type': 'category',
            'help': 'Events of spin',
            'child': [
                {
                    'label': 'on spin',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_spin,
                    'name': 'on_spin',
                    'help': ' Generated whenever pressing an arrow changed the spin button value.'
                },
                {
                    'label': 'on spin up',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_spin_up,
                    'name': 'on_spin_up',
                    'help': ' Generated whenever pressing left/up arrow changed the spin button value.'
                },
                {
                    'label': 'on spin down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_spin_down,
                    'name': 'on_spin_down',
                    'help': ' Generated whenever pressing right/down arrow changed the spin button value.'
                },
            ],
            }]
        _base_property, _base_event = super(SpinButtonControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.SpinButton)(
            edition_frame, self.py_id,  self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # required #include <wx/spinbutt.h>
        class_ = register_cpp_class(self.project, 'wxSpinButton')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="spin button",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/spinbutt.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxSpinButton({owner}, {self._id}, {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        code += super(SpinButtonControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.SpinButton({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(SpinButtonControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


