import os
import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import register_cpp_class, with_style


@with_style
class SearchControl(WindowControl):
    """"This class represents a button"""

    style_list = (wx.TE_PROCESS_TAB, wx.TE_NOHIDESEL, wx.TE_LEFT, wx.TE_CENTRE, wx.TE_RIGHT, )
    style_list_py_text = ('wx.TE_PROCESS_TAB', 'wx.TE_NOHIDESEL', 'wx.TE_LEFT', 'wx.TE_CENTRE', 'wx.TE_RIGHT', )
    style_list_cc_text = ('wxTE_PROCESS_TAB', 'wxTE_NOHIDESEL', 'wxTE_LEFT', 'wxTE_CENTRE', 'wxTE_RIGHT', )

    def __init__(self, **kwargs):
        self._search_button = True
        self._cancel_button = True
        self._descriptive_text = ''
        self._hint = ''
        self._value = ''
        self._style = 4
        # events
        self._on_search = ''
        self._on_cancel_search = ''
        super(SearchControl, self).__init__(**kwargs)

    @property
    def local_search_kwargs(self):
        return {
            'search_button': self._search_button,
            'cancel_button': self._cancel_button,
            'descriptive_text': self._descriptive_text,
            'hint': self._hint,
            'value': self._value,
            'style': self._style,
            # events
            'on_search': self._on_search,
            'on_cancel_search': self._on_cancel_search,
        }

    @local_search_kwargs.setter
    def local_search_kwargs(self, kwargs):
        self._search_button = kwargs.get('search_button', self._search_button)
        self._cancel_button = kwargs.get('cancel_button', self._cancel_button)
        self._descriptive_text = kwargs.get('descriptive_text', self._descriptive_text)
        self._hint = kwargs.get('hint', self._hint)
        self._value = kwargs.get('value', self._value)
        self._style = kwargs.get('style', self._style)
        self._on_search = kwargs.get('on_search', self._on_search)
        self._on_cancel_search = kwargs.get('on_cancel_search', self._on_cancel_search)

    @property
    def local_search_events(self):
        return {
            'on_search': (self._on_search, 'wx.EVT_SEARCH', 'wxEVT_SEARCH'),
            'on_cancel_search': (self._on_cancel_search, 'wx.EVT_SEARCH_CANCEL', 'wxEVT_SEARCH_CANCEL'),
        }

    @property
    def events(self):
        value = self.local_search_events
        value.update(super(SearchControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_search_kwargs
        value.update(super(SearchControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_search_kwargs = kwargs
        super(SearchControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('search_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxTE_PROCESS_TAB:', "The control will receive wxEVT_CHAR events for TAB pressed - normally, "
                                  "TAB is used for passing to the next control in a dialog instead. "
                                  "For the control created with this style, you can still use Ctrl-Enter to pass "
                                  "to the next control from the keyboard."),
            ('wxTE_NOHIDESEL', "By default, the Windows text control doesn’t show the selection when it doesn’t "
                               "have focus - use this style to force it to always show it. It doesn’t do anything "
                               "under other platforms."),
            ('wxTE_LEFT', "The text in the control will be left-justified (default)."),
            ('wxTE_CENTRE', "The text in the control will be centered (currently wxMSW and wxGTK2 only)."),
            ('wxTE_RIGHT', "The text in the control will be right-justified (currently wxMSW and wxGTK2 only)."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Search',
            'type': 'category',
            'help': 'Attributes of search.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The search control name."
                },
                {
                    'label': 'search button',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._search_button,
                    'name': 'search_button',  # kwarg value
                    'help': "Show the search button."
                },
                {
                    'label': 'cancel button',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._cancel_button,
                    'name': 'cancel_button',  # kwarg value
                    'help': "Show the cancel button."
                },
                {
                    'label': 'descriptive text',
                    'type': 'string',
                    'read_only': False,
                    'value': self._descriptive_text,
                    'name': 'descriptive_text',  # kwarg value
                    'help': "Text to show when the search control is empty."
                },
                {
                    'label': 'hint',
                    'type': 'string',
                    'read_only': False,
                    'value': self._hint,
                    'name': 'hint',  # kwarg value
                    'help': "Sets a hint shown in an empty unfocused text control."
                },
                {
                    'label': 'value',
                    'type': 'string',
                    'read_only': False,
                    'value': self._value,
                    'name': 'value',  # kwarg value
                    'help': "Sets a value to look for."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the richtext style.'
                }]
        }]
        _event = [{
            'label': 'Search',
            'type': 'category',
            'help': 'Events of search',
            'child': [
                {
                    'label': 'on_search',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_search,
                    'name': 'on_text',
                    'help': 'Called when the search button is clicked.'
                },
                {
                    'label': 'on_cancel_search',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_cancel_search,
                    'name': 'on_text_enter',
                    'help': 'Called when the cancel search button is clicked.'
                }],
            }]
        _base_property, _base_event = super(SearchControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.SearchCtrl)(
            edition_frame, self.py_id, self._value, self.py_pos, self.py_size, self.style_value)
        self._edition_window.ShowSearchButton(self._search_button)
        self._edition_window.ShowCancelButton(self._cancel_button)
        self._edition_window.SetDescriptiveText(self._descriptive_text)
        self._edition_window.SetHint(self._hint)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/srchctrl.h>
        class_ = register_cpp_class(self.project, 'wxSearchCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="search control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/srchctrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxSearchCtrl({owner}, {self._id}, "{self._value}", ' \
               f'{self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n'
        code += f'{self._name}->ShowSearchButton({self._search_button});\n'
        code += f'{self._name}->ShowCancelButton({self._cancel_button});\n'
        code += f'{self._name}->SetDescriptiveText("{self._descriptive_text}");\n'
        code += f'{self._name}->SetHint("{self._hint}");\n'
        code += super(SearchControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.SearchCtrl({owner}, {self.py_id_str}, "{self._value}", {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += f'{this}.ShowSearchButton({self._search_button})\n'
        code += f'{this}.ShowCancelButton({self._cancel_button})\n'
        code += f'{this}.SetDescriptiveText("{self._descriptive_text}")\n'
        code += f'{this}.SetHint("{self._hint}")\n'
        code += super(SearchControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


