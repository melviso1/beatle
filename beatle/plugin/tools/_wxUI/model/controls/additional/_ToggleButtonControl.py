import wx
from .._WindowControl import WindowControl
from beatle.model import cc, py
from ....lib import register_cpp_class


class ToggleButtonControl(WindowControl):
    """"This class represents a toggle button"""

    def __init__(self, **kwargs):
        self._label = 'label'
        self._checked = False
        self._on_toggle = ''
        super(ToggleButtonControl, self).__init__(**kwargs)

    @property
    def local_toggle_button_kwargs(self):
        return {
            'label': self._label,
            'checked': self._checked,
            # events
            'on_toggle': self._on_toggle
        }

    @local_toggle_button_kwargs.setter
    def local_toggle_button_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._checked = kwargs.get('checked', self._checked)
        self._on_toggle = kwargs.get('on_toggle', self._on_toggle)


    @property
    def local_toggle_button_events(self):
        return {
            'on_toggle': (self._on_toggle, 'wx.EVT_TOGGLEBUTTON', 'wxEVT_TOGGLEBUTTON'),
        }

    @property
    def events(self):
        value = self.local_toggle_button_events
        value.update(super(ToggleButtonControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_toggle_button_kwargs
        value.update(super(ToggleButtonControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_toggle_button_kwargs = kwargs
        super(ToggleButtonControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('toggle_button')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Toggle button',
            'type': 'category',
            'help': 'Attributes of toggle button',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The toggle button name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'Text displayed in the checkbox.',
                },
                {
                    'label': 'checked',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._checked,
                    'name': 'checked',  # kwarg value
                    'help': 'Initial status of the checkbox.',
                },]
        }]
        _event = [{
            'label': 'Toggle button',
            'type': 'category',
            'help': 'Events of toggle button',
            'child': [{
                'label': 'on toggle',
                'type': 'string',
                'read_only': False,
                'value': self._on_toggle,
                'name': 'on_toggle',  # kwarg value
                'help': 'Called when the button is toggled.'
                },],
            }]
        _base_property, _base_event = super(ToggleButtonControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.ToggleButton)(
            edition_frame, self.py_id, self._label, self.py_pos, self.py_size, self.style_value)
        self._edition_window.SetValue(self._checked)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/tglbtn.h>
        cls = register_cpp_class(self.project, 'wxToggleButton')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="toggle button control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/tglbtn.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f'{self._name} = new wxToggleButton({owner}, {self._id}, "{self._label}", {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        value = (self._checked and 'true') or 'false'
        code += f'{self._name}->SetValue({value});\n'
        code += super(ToggleButtonControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{self._name}'

        code = f'{this} = wx.ToggleButton({owner}, {self.py_id_str}, "{self._label}", {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        value = (self._checked and 'True') or 'False'
        code += f'{this}.SetValue({value})\n'
        code += super(ToggleButtonControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

