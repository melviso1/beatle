import os
import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events

# looks undefined?
wx.CLRP_SHOW_ALPHA = 0x0010


@with_style
@extend_events('colour_picker_events')
class ColourPickerControl(WindowControl):
    """"This class represents a colour picker"""

    style_list = (wx.CLRP_DEFAULT_STYLE, wx.CLRP_USE_TEXTCTRL, wx.CLRP_SHOW_LABEL, wx.CLRP_SHOW_ALPHA)
    style_list_py_text = ('wx.CLRP_DEFAULT_STYLE', 'wx.CLRP_USE_TEXTCTRL', 'wx.CLRP_SHOW_LABEL', 'wx.CLRP_SHOW_ALPHA')
    style_list_cc_text = ('wxCLRP_DEFAULT_STYLE', 'wxCLRP_USE_TEXTCTRL', 'wxCLRP_SHOW_LABEL', 'wxCLRP_SHOW_ALPHA' )

    def __init__(self, **kwargs):
        self._colour_value = (False, wx.SYS_COLOUR_WINDOWTEXT)
        self._style = 4
        # events
        self._on_colourpicker_changed = ''
        self._on_colourpicker_current_changed = ''
        self._on_colourpicker_dialog_cancelled = ''
        super(ColourPickerControl, self).__init__(**kwargs)

    @property
    def local_colourpicker_kwargs(self):
        return {
            'colour_value': self._colour_value,
            'style': self._style,
            # events
            'on_colourpicker_changed': self._on_colourpicker_changed,
            'on_colourpicker_current_changed': self._on_colourpicker_current_changed,
            'on_colourpicker_dialog_cancelled': self._on_colourpicker_dialog_cancelled,
        }

    @local_colourpicker_kwargs.setter
    def local_colourpicker_kwargs(self, kwargs):
        self._colour_value = tuple(kwargs.get('colour_value', self._colour_value))
        self._style = kwargs.get('style', self._style)
        self._on_colourpicker_changed = kwargs.get('on_colourpicker_changed', self._on_colourpicker_changed)
        self._on_colourpicker_current_changed = kwargs.get(
            'on_colourpicker_current_changed', self._on_colourpicker_current_changed)
        self._on_colourpicker_dialog_cancelled = kwargs.get(
            'on_colourpicker_dialog_cancelled', self._on_colourpicker_dialog_cancelled)

    @property
    def colour_picker_events(self):
        return {
            'on_colourpicker_changed': (self._on_colourpicker_changed,
                                        'wx.EVT_COLOURPICKER_CHANGED', 'wxEVT_COLOURPICKER_CHANGED'),
            'on_colourpicker_current_changed': (self._on_colourpicker_current_changed,
                                                'wx.EVT_COLOURPICKER_CURRENT_CHANGED',
                                                'wxEVT_COLOURPICKER_CURRENT_CHANGED'),
            'on_colourpicker_dialog_cancelled': (self._on_colourpicker_dialog_cancelled,
                                                 'wx.EVT_COLOURPICKER_DIALOG_CANCELLED',
                                                 'wxEVT_COLOURPICKER_DIALOG_CANCELLED'),
        }

    @property
    def kwargs(self):
        value = self.local_colourpicker_kwargs
        value.update(super(ColourPickerControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_colourpicker_kwargs = kwargs
        super(ColourPickerControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('colour_picker')

    @property
    def style_attribute_options(self):
        return [
            ('wxCLRP_DEFAULT_STYLE', "The default style"),
            ('wxCLRP_USE_TEXTCTRL', "Creates a text control to the left of the picker button which is completely "
                                    "managed by the wx.ColourPickerCtrl and which can be used by the user to specify "
                                    "a colour (see SetColour). The text control is automatically synchronized with "
                                    "button's value. Use functions defined in wx.PickerBase to modify the text "
                                    "control."),
            ('wxCLRP_SHOW_LABEL', "Shows the colour in HTML form (AABBCC) as colour button label "
                                  "(instead of no label at all)."),
            ('wxCLRP_SHOW_ALPHA', "Allows selecting opacity in the colour-chooser (effective under wxGTK and wxOSX)."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Colour picker',
            'type': 'category',
            'help': 'Attributes of colour picker.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The colour picker control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the colour picker style.'
                },
                {
                    'label': 'initial colour',
                    'type': 'conditional colour',
                    'read_only': False,
                    'value': self._colour_value,
                    'name': 'colour_value',
                    'help': 'Sets the colour picker initial colour.'
                },
            ]
        }]
        _event = [{
            'label': 'Colour picker',
            'type': 'category',
            'help': 'Events of colour picker',
            'child': [
                {
                    'label': 'on colour changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_colourpicker_changed,
                    'name': 'on_colourpicker_changed',
                    'help': ' The user changed the colour selected in the control either using the button or using '
                            'text control (see CLRP_USE_TEXTCTRL ; note that in this case the event is fired only'
                            ' if the user\'s input is valid, i.e. recognizable). When using a popup dialog for '
                            'changing the colour, this event is sent only when the changes in the dialog '
                            'are accepted by the user, unlike the next event.'
                },
                {
                    'label': 'on current colour changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_colourpicker_current_changed,
                    'name': 'on_colourpicker_current_changed',
                    'help': 'The user changed the currently selected colour in the dialog associated with the '
                            'control. This event is sent immediately when the selection changes and you must '
                            'also handle EVT_COLOUR_CANCELLED to revert to the previously selected colour if the '
                            'selection ends up not being accepted. This event is new since wxWidgets 3.1.3 '
                            'and currently is only implemented in wxMSW.'
                },
                {
                    'label': 'on colour dialog cancelled',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_colourpicker_dialog_cancelled,
                    'name': 'on_colourpicker_dialog_cancelled',
                    'help': 'The user cancelled the colour dialog associated with the control, i.e. closed it '
                            'without accepting the selection. This event is new since wxWidgets 3.1.3 and '
                            'currently is only implemented in wxMSW.'
                }],
            }]
        _base_property, _base_event = super(ColourPickerControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        colour = get_colour_from_tuple(self._colour_value)
        if colour is None:
            colour = wx.WHITE
        self._edition_window = uiMockupControl(wx.ColourPickerCtrl)(
            edition_frame, self.py_id, colour, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/colour.h>
        # required #include <wx/clrpicker.h>
        class_ = register_cpp_class(self.project, 'wxColourPickerCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="colour picker control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/colour.h>')
        self.add_cpp_required_header('#include <wx/clrpicker.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        colour = get_colour_from_tuple(self._colour_value) or '(255,255,255)'
        if colour is None:
            colour = wx.WHITE
        code = f'{self._name} = new wxColourPickerCtrl({owner}, {self._id}, ' \
               f'wxColour{colour}, {self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n'
        code += super(ColourPickerControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        colour = get_colour_py_text_from_tuple(self._colour_value) or 'wx.WHITE'
        code = f'{this} = wx.ColourPickerCtrl({owner}, {self.py_id_str}, {colour}, ' \
               f'{self.py_pos_str}, {self.py_size_str}, {self.py_style_str})\n'
        code += super(ColourPickerControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


