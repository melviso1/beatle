import wx
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import register_cpp_class, with_style, extend_events


@with_style
@extend_events('spin_events')
class SpinControl(WindowControl):
    """"This class represents a colour picker"""

    style_list = (wx.SP_ARROW_KEYS, wx.SP_WRAP, wx.TE_PROCESS_ENTER, wx.ALIGN_LEFT,
                  wx.ALIGN_CENTRE_HORIZONTAL, wx.ALIGN_RIGHT)
    style_list_py_text = ('wx.SP_ARROW_KEYS', 'wx.SP_WRAP', 'wx.TE_PROCESS_ENTER', 'wx.ALIGN_LEFT',
                          'wx.ALIGN_CENTRE_HORIZONTAL', 'wx.ALIGN_RIGHT')
    style_list_cc_text = ('wxSP_ARROW_KEYS', 'wxSP_WRAP', 'wxTE_PROCESS_ENTER', 'wxALIGN_LEFT',
                          'wxALIGN_CENTRE_HORIZONTAL', 'wxALIGN_RIGHT')

    def __init__(self, **kwargs):
        self._value = 0
        self._min = 0
        self._max = 100
        self._style = 4
        # events
        self._on_spin = ''
        super(SpinControl, self).__init__(**kwargs)

    @property
    def local_spin_kwargs(self):
        return {
            'value': self._value,
            'min': self._min,
            'max': self._max,
            'style': self._style,
            # events
            'on_spin': self._on_spin,
        }

    @local_spin_kwargs.setter
    def local_spin_kwargs(self, kwargs):
        self._value = kwargs.get('value', self._value)
        self._min = kwargs.get('min', self._min)
        self._max = kwargs.get('max', self._max)
        self._style = kwargs.get('style', self._style)
        self._on_spin = kwargs.get('on_spin', self._on_spin)

    @property
    def spin_events(self):
        return {
            'on_spin': (self._on_spin, 'wx.EVT_SPINCTRL', 'wxEVT_SPINCTRL'),
        }

    @property
    def kwargs(self):
        value = self.local_spin_kwargs
        value.update(super(SpinControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_spin_kwargs = kwargs
        super(SpinControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('spin_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxSP_ARROW_KEYS', "The user can use arrow keys to change the value."),
            ('wxSP_WRAP', "The value wraps at the minimum and maximum."),
            ('wxTE_PROCESS_ENTER',
             "Indicates that the control should generate wxEVT_TEXT_ENTER events. "
             "Using this style will prevent the user from using the Enter key for dialog navigation "
             "(e.g. activating the default button in the dialog) under MSW."),
            ('wxALIGN_LEFT', "Same as wx.TE_LEFT for wx.TextCtrl: the text is left aligned (this is the default)."),
            ('wxALIGN_CENTRE_HORIZONTAL', "Same as wx.TE_CENTRE for wx.TextCtrl: the text is centered."),
            ('wxALIGN_RIGHT', "Same as wx.TE_RIGHT for wx.TextCtrl: the text is right aligned. "),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Spin',
            'type': 'category',
            'help': 'Attributes of spin.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The spin control name."
                },
                {
                    'label': 'value',
                    'type': 'integer',
                    'value': self._value,
                    'name': 'value',
                    'help': 'The selected value.'
                },
                {
                    'label': 'minimum',
                    'type': 'integer',
                    'value': self._min,
                    'name': 'min',
                    'help': 'Sets the minimum value of the spin.'
                },
                {
                    'label': 'maximum',
                    'type': 'integer',
                    'value': self._max,
                    'name': 'max',
                    'help': 'Sets the maximum valueof the spin.'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the colour picker style.'
                },]
        }]
        _event = [{
            'label': 'Spin',
            'type': 'category',
            'help': 'Events of spin',
            'child': [
                {
                    'label': 'on spin',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_spin,
                    'name': 'on_spin',
                    'help': ' Process a wxEVT_SPINCTRL event, which is generated whenever '
                            'the numeric value of the spin control is updated.'
                }],
            }]
        _base_property, _base_event = super(SpinControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.SpinCtrl)(
            edition_frame, self.py_id, str(self._value), self.py_pos, self.py_size, self.style_value,
            self._min, self._max, self._value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # required #include <wx/spinctrl.h>
        class_ = register_cpp_class(self.project, 'wxSpinCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="spin control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/spinctrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxSpinCtrl({owner}, {self._id}, "{self._value}", {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str}, {self._min}, {self._max}, {self._value});\n'
        code += super(SpinControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.SpinCtrl({owner}, {self.py_id_str}, "{self._value}", {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str}, {self._min}, {self._max}, {self._value})\n'
        code += super(SpinControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


