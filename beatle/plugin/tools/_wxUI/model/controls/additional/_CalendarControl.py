import os
import wx
import wx.adv
from beatle.model import cc
from .._WindowControl import WindowControl
from ....lib import get_colour_from_tuple, get_colour_py_text_from_tuple,  \
    register_cpp_class, with_style, extend_events


@with_style
@extend_events('calendar_events')
class CalendarControl(WindowControl):
    """"This class represents a colour picker"""

    style_list = (wx.adv.CAL_SUNDAY_FIRST, wx.adv.CAL_MONDAY_FIRST, wx.adv.CAL_SHOW_HOLIDAYS,
                  wx.adv.CAL_NO_YEAR_CHANGE, wx.adv.CAL_NO_MONTH_CHANGE, wx.adv.CAL_SHOW_SURROUNDING_WEEKS,
                  wx.adv.CAL_SEQUENTIAL_MONTH_SELECTION, wx.adv.CAL_SHOW_WEEK_NUMBERS, )
    style_list_py_text = ('wx.adv.CAL_SUNDAY_FIRST', 'wx.adv.CAL_MONDAY_FIRST', 'wx.adv.CAL_SHOW_HOLIDAYS',
                          'wx.adv.CAL_NO_YEAR_CHANGE', 'wx.adv.CAL_NO_MONTH_CHANGE',
                          'wx.adv.CAL_SHOW_SURROUNDING_WEEKS', 'wx.adv.CAL_SEQUENTIAL_MONTH_SELECTION',
                          'wx.adv.CAL_SHOW_WEEK_NUMBERS', )
    style_list_cc_text = ('wxCAL_SUNDAY_FIRST', 'wxCAL_MONDAY_FIRST', 'wxCAL_SHOW_HOLIDAYS',
                          'wxCAL_NO_YEAR_CHANGE', 'wxCAL_NO_MONTH_CHANGE',
                          'wxCAL_SHOW_SURROUNDING_WEEKS', 'wxCAL_SEQUENTIAL_MONTH_SELECTION',
                          'wxCAL_SHOW_WEEK_NUMBERS', )

    def __init__(self, **kwargs):
        self._style = 0
        # events
        self._on_calendar = ''
        self._on_calendar_sel_changed = ''
        self._on_calendar_page_changed = ''
        self._on_calendar_weekday_clicked = ''
        self._on_calendar_week_clicked = ''
        super(CalendarControl, self).__init__(**kwargs)

    @property
    def local_calendar_kwargs(self):
        return {
            'style': self._style,
            # events
            'on_calendar': self._on_calendar,
            'on_calendar_sel_changed': self._on_calendar_sel_changed,
            'on_calendar_page_changed': self._on_calendar_page_changed,
            'on_calendar_weekday_clicked': self._on_calendar_weekday_clicked,
            'on_calendar_week_clicked': self._on_calendar_week_clicked,
        }

    @local_calendar_kwargs.setter
    def local_calendar_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._on_calendar = kwargs.get('on_calendar', self._on_calendar)
        self._on_calendar_sel_changed = kwargs.get('on_calendar_sel_changed', self._on_calendar_sel_changed)
        self._on_calendar_page_changed = kwargs.get('on_calendar_page_changed', self._on_calendar_page_changed)
        self._on_calendar_weekday_clicked = kwargs.get(
            'on_calendar_weekday_clicked', self._on_calendar_weekday_clicked)
        self._on_calendar_week_clicked = kwargs.get('on_calendar_week_clicked', self._on_calendar_week_clicked)

    @property
    def calendar_events(self):
        return {
            'on_calendar': (self._on_calendar, 'wx.adv.EVT_CALENDAR', 'wxEVT_CALENDAR_DOUBLECLICKED'),
            'on_calendar_sel_changed': (self._on_calendar_sel_changed, 'wx.adv.EVT_CALENDAR_SEL_CHANGED',
                                        'wxEVT_CALENDAR_SEL_CHANGED'),
            'on_calendar_page_changed': (self._on_calendar_page_changed, 'wx.adv.EVT_CALENDAR_PAGE_CHANGED',
                                         'wxEVT_CALENDAR_PAGE_CHANGED'),
            'on_calendar_weekday_clicked': (self._on_calendar_weekday_clicked, 'wx.adv.EVT_CALENDAR_WEEKDAY_CLICKED',
                                            'wxEVT_CALENDAR_WEEKDAY_CLICKED'),
            'on_calendar_week_clicked': (self._on_calendar_week_clicked, 'wx.adv.EVT_CALENDAR_WEEK_CLICKED',
                                         'wxEVT_CALENDAR_WEEK_CLICKED'),
        }

    @property
    def kwargs(self):
        value = self.local_calendar_kwargs
        value.update(super(CalendarControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_calendar_kwargs = kwargs
        super(CalendarControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('calendar_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxCAL_SUNDAY_FIRST', 'Show Sunday as the first day in the week (not in wxGTK)'),
            ('wxCAL_MONDAY_FIRST', 'Show Monday as the first day in the week (not in wxGTK)'),
            ('wxCAL_SHOW_HOLIDAYS', 'Highlight holidays in the calendar (only generic)'),
            ('wxCAL_NO_YEAR_CHANGE', 'Disable the year changing (deprecated, only generic)'),
            ('wxCAL_NO_MONTH_CHANGE', 'Disable the month (and, implicitly, the year) changing'),
            ('wxCAL_SHOW_SURROUNDING_WEEKS', 'Show the neighbouring weeks in the previous and next months'
                                             ' (only generic, always on for the native controls)'),
            ('wxCAL_SEQUENTIAL_MONTH_SELECTION',
             'Use alternative, more compact, style for the month and year selection controls. (only generic)'),
            ('wxCAL_SHOW_WEEK_NUMBERS', 'Show week numbers on the left side of the calendar. (not in generic).'),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Calendar',
            'type': 'category',
            'help': 'Attributes of calendar.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The calendar control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Set the calendar style.'
                },
            ]
        }]
        _event = [{
            'label': 'Calendar',
            'type': 'category',
            'help': 'Events of calendar',
            'child': [
                {
                    'label': 'on calendar',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_calendar,
                    'name': 'on_calendar',
                    'help': '  Called when a day was double clicked in the calendar.'
                },
                {
                    'label': 'on selection changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_calendar_sel_changed,
                    'name': 'on_calendar_sel_changed',
                    'help': '  Called when the selected date is changed.'
                },
                {
                    'label': 'on page changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_calendar_page_changed,
                    'name': 'on_calendar_page_changed',
                    'help': '  Called when month or year changed.'
                },
                {
                    'label': 'on week day clicked',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_calendar_weekday_clicked,
                    'name': 'on_calendar_weekday_clicked',
                    'help': '  Called when user clicked on the week day header (only generic).'
                },
                {
                    'label': 'on week clicked',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_calendar_week_clicked,
                    'name': 'on_calendar_week_clicked',
                    'help': '  Called when user clicked on the week of the year number (only generic).'
                }],
            }]
        _base_property, _base_event = super(CalendarControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        self._edition_window = uiMockupControl(wx.adv.CalendarCtrl)(
            edition_frame, self.py_id, wx.DefaultDateTime, self.py_pos, self.py_size, self.style_value)
        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # required #include <wx/calctrl.h>
        class_ = register_cpp_class(self.project, 'wxCalendarCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="calendar control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the rich text implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/calctrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxCalendarCtrl({owner}, {self._id}, wxDefaultDateTime, ' \
               f'{self.cc_pos_str}, {self.cc_size_str}, {self.cc_style_str});\n'
        code += super(CalendarControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.adv')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.adv.CalendarCtrl({owner}, {self.py_id_str}, wx.DefaultDateTime, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(CalendarControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


