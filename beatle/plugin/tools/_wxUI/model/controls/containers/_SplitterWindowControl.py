import wx
from .._WindowControl import WindowControl
from beatle.lib.tran import TransactionalMethod, format_current_transaction_name
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class SplitterWindowControl(WindowControl):
    """"This class represents a panel"""

    style_list = (wx.SP_3D, wx.SP_THIN_SASH, wx.SP_3DSASH, wx.SP_3DBORDER, wx.SP_BORDER,
                  wx.SP_NOBORDER, wx.SP_NO_XP_THEME, wx.SP_PERMIT_UNSPLIT, wx.SP_LIVE_UPDATE)

    style_list_py_text = ('wx.SP_3D', 'wx.SP_THIN_SASH', 'wx.SP_3DSASH', 'wx.SP_3DBORDER', 'wx.SP_BORDER',
                  'wx.SP_NOBORDER', 'wx.SP_NO_XP_THEME', 'wx.SP_PERMIT_UNSPLIT', 'wx.SP_LIVE_UPDATE')

    style_list_cc_text = ('wxSP_3D', 'wxSP_THIN_SASH', 'wxSP_3DSASH', 'wxSP_3DBORDER', 'wxSP_BORDER',
                          'wxSP_NOBORDER', 'wxSP_NO_XP_THEME', 'wxSP_PERMIT_UNSPLIT', 'wxSP_LIVE_UPDATE')

    def __init__(self, **kwargs):
        self._style = 0
        self._split_mode = 'wxSPLIT_VERTICAL'
        self._sashpos = 0
        self._sashsize = -1
        self._sashgravity = 0
        self._min_pane_size = 0
        # events
        self._on_splitter_sash_pos_changing = ''
        self._on_splitter_sash_pos_resize = ''
        self._on_splitter_sash_pos_changed = ''
        self._on_splitter_unsplit = ''
        self._on_splitter_dclick = ''
        super(SplitterWindowControl, self).__init__(**kwargs)

    @property
    def local_splitterwindowcontrol_kwargs(self):
        return {
            'style': self._style,
            'split_mode': self._split_mode,
            'sashpos': self._sashpos,
            'sashsize': self._sashsize,
            'sashgravity': self._sashgravity,
            'min_pane_size': self._min_pane_size,
            #events
            'on_splitter_sash_pos_changing': self._on_splitter_sash_pos_changing,
            'on_splitter_sash_pos_resize': self._on_splitter_sash_pos_resize,
            'on_splitter_sash_pos_changed': self._on_splitter_sash_pos_changed,
            'on_splitter_unsplit': self._on_splitter_unsplit,
            'on_splitter_dclick': self._on_splitter_dclick,
        }

    @local_splitterwindowcontrol_kwargs.setter
    def local_splitterwindowcontrol_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._split_mode = kwargs.get('split_mode', self._split_mode)
        self._sashpos = kwargs.get('sashpos', self._sashpos)
        self._sashsize = kwargs.get('sashsize', self._sashsize)
        self._sashgravity = kwargs.get('sashgravity', self._sashgravity)
        self._min_pane_size = kwargs.get('min_pane_size', self._min_pane_size)
        self._on_splitter_sash_pos_changing = kwargs.get('on_splitter_sash_pos_changing',
                                                         self._on_splitter_sash_pos_changing)
        self._on_splitter_sash_pos_resize = kwargs.get('on_splitter_sash_pos_resize', self._on_splitter_sash_pos_resize)
        self._on_splitter_sash_pos_changed = kwargs.get('on_splitter_sash_pos_changed',
                                                        self._on_splitter_sash_pos_changed)
        self._on_splitter_unsplit = kwargs.get('on_splitter_unsplit', self._on_splitter_unsplit)
        self._on_splitter_dclick = kwargs.get('on_splitter_dclick', self._on_splitter_dclick)

    @property
    def kwargs(self):
        value = self.local_splitterwindowcontrol_kwargs
        value.update(super(SplitterWindowControl, self).kwargs)
        return value

    @property
    def local_splitter_window_events(self):
        return {
            'on_splitter_sash_pos_changing': (self._on_splitter_sash_pos_changing, 'wx.EVT_SPLITTER_SASH_POS_CHANGING',
                                              'wxEVT_SPLITTER_SASH_POS_CHANGING'),
            'on_splitter_sash_pos_resize': (self._on_splitter_sash_pos_resize, 'wx.EVT_SPLITTER_SASH_POS_RESIZE',
                                            'wxEVT_SPLITTER_SASH_POS_RESIZE'),
            'on_splitter_sash_pos_changed': (self._on_splitter_sash_pos_changed, 'wx.EVT_SPLITTER_SASH_POS_CHANGED',
                                             'wxEVT_SPLITTER_SASH_POS_CHANGED'),
            'on_splitter_unsplit': (self._on_splitter_unsplit, 'wx.EVT_SPLITTER_UNSPLIT', 'wxEVT_SPLITTER_UNSPLIT'),
            'on_splitter_dclick': (self._on_splitter_dclick, 'wx.EVT_SPLITTER_DCLICK', 'wxEVT_SPLITTER_DCLICK'),
        }

    @property
    def events(self):
        value = self.local_splitter_window_events
        value.update(super(SplitterWindowControl, self).events)
        return value

    def set_kwargs(self, kwargs):
        self.local_splitterwindowcontrol_kwargs = kwargs
        super(SplitterWindowControl, self).set_kwargs(kwargs)

    @TransactionalMethod('change sas position of {0}')
    def on_sas_pos_changed(self, event):
        """This method is called when the design window splitter is resized"""
        format_current_transaction_name(self._name)
        self.save_state()
        self.set_kwargs({'sashpos': event.GetSashPosition()})
        return True

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('splitter_window')

    @property
    def style_attribute_options(self):
        return [
            ('wxSP_3D', "Draws a 3D effect border and sash."),
            ('wxSP_THIN_SASH', "Draws a thin sash."),
            ('wxSP_3DSASH', "Draws a 3D effect sash (part of default style)."),
            ('wxSP_3DBORDER', "Synonym for wx.SP_BORDER."),
            ('wxSP_BORDER', "Draws a standard border."),
            ('wxSP_NOBORDER', "No border (default)."),
            ('wxSP_NO_XP_THEME',
             "Under Windows, switches off the attempt to draw the splitter using Windows theming, so the borders and "
             "sash will take on the pre-XP look."),
            ('wxSP_PERMIT_UNSPLIT', "Always allow to unsplit, even with the minimum pane size other than zero."),
            ('wxSP_LIVE_UPDATE', "Don't draw wx.XOR line but resize the child windows immediately. "),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Splitter window',
            'type': 'category',
            'help': 'Attributes of splitter window',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The panel instance name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'read_only': False,
                    'value': self._style,
                    'name': 'style',  # kwarg value
                    'options': self.style_attribute_options,
                    'help': "The splitter window style."
                },
                {
                    'label': 'split mode',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._split_mode,
                    'name': 'split_mode',  # kwarg value
                    'help': 'Split window direction.',
                    'choices': ['wxSPLIT_HORIZONTAL', 'wxSPLIT_VERTICAL']
                },
                {
                    'label': 'sash position',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._sashpos,
                    'name': 'sashpos',  # kwarg value
                    'help': 'split sash position.',
                },
                {
                    'label': 'sash size',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._sashsize,
                    'name': 'sashsize',  # kwarg value
                    'help': 'split sash size.',
                },
                {
                    'label': 'sash gravity',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._sashgravity,
                    'name': 'sashgravity',  # kwarg value
                    'help': 'Left/Top pane grow factor while resizing.',
                },
                {
                    'label': 'min pane size',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._min_pane_size,
                    'name': 'min_pane_size',  # kwarg value
                    'help': 'Minimun pane width/height for vertical/horizontal splitter window.',
                },
            ],
        },]
        _event = [{
            'label': 'SplitterWindow',
            'type': 'category',
            'help': 'Events of splitter window',
            'child': [
                {
                    'label': 'on_splitter_sash_pos_changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_splitter_sash_pos_changing,
                    'name': 'on_splitter_sash_pos_changing',
                    'help': "The sash position is in the process of being changed. May be used to modify the position "
                            "of the tracking bar to properly reflect the position that would be set if the drag were "
                            "to be completed at this point. Processes a wxEVT_SPLITTER_SASH_POS_CHANGING event.",
                },
                {
                    'label': 'on_splitter_sash_pos_resize',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_splitter_sash_pos_resize,
                    'name': 'on_splitter_sash_pos_resize',
                    'help': "The sash position is in the process of being updated. May be used to modify the position "
                            "of the tracking bar to properly reflect the position that would be set if the update were "
                            "to be completed. This can happen e.g. when the window is resized and the sash is moved "
                            "according to the gravity setting. This event is sent when the window is resized and "
                            "allows the application to select the desired new sash position. If it doesnâ€™t process "
                            "the event, the position is determined by the gravity setting. Processes a "
                            "wxEVT_SPLITTER_SASH_POS_RESIZE event and is only available in wxWidgets 3.1.6 or newer.",
                },
                {
                    'label': 'on_splitter_sash_pos_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_splitter_sash_pos_changed,
                    'name': 'on_splitter_sash_pos_changed',
                    'help': "The sash position was changed. May be used to modify the sash position before it is set, "
                            "or to prevent the change from taking place. Processes a "
                            "wxEVT_SPLITTER_SASH_POS_CHANGED event.",
                },
                {
                    'label': 'on_splitter_unsplit',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_splitter_unsplit,
                    'name': 'on_splitter_unsplit',
                    'help': "The splitter has been just unsplit. Processes a wxEVT_SPLITTER_UNSPLIT event.",
                },
                {
                    'label': 'on_splitter_dclick',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_splitter_dclick,
                    'name': 'on_splitter_dclick',
                    'help': "The sash was double clicked. The default behaviour is to unsplit the window when this "
                            "happens (unless the minimum pane size has been set to a value greater than zero). "
                            "Processes a wxEVT_SPLITTER_DOUBLECLICKED event.",
                },
            ]
        }]
        _base_property, _base_event = super(SplitterWindowControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def is_sizer_root(self):
        return True

    @property
    def can_add_layout_control(self):
        return False

    @property
    def can_add_control(self):
        """Only two controls can be added. We fork here from traditional wxformbuilder behaviour
        because there is no restriction about what kind of window you can add. """
        return len(self.sorted_wxui_child) < 2

    @property
    def edition_window_client(self):
        """Return the control to be used as client container
        in the mockup visualization"""
        return self._edition_window

    def set_sash_on_idle(self, event):
        """A little tricky initialization"""
        self._edition_window.SetSashPosition(self._sashpos)
        self._edition_window.Unbind(wx.EVT_IDLE)

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.SplitterWindow)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            child = self.sorted_wxui_child
            for item in child:
                item.create_edition_window(self)
            if len(child) == 1:
                self._edition_window.Initialize(child[0].edition_window)
            elif len(child) == 2:
                if self._split_mode == 'wxSPLIT_VERTICAL':
                    self._edition_window.SplitVertically(child[0].edition_window, child[1].edition_window)
                else:
                    self._edition_window.SplitHorizontally(child[0].edition_window, child[1].edition_window)
                self._edition_window.SetMinimumPaneSize(self._min_pane_size)
                self._edition_window.SetSashGravity(self._sashgravity)
                self._edition_window.Bind(wx.EVT_IDLE, self.set_sash_on_idle)
                wx.PostEvent(self._edition_window, wx.IdleEvent())
            self.add_to_sizer(container, self._edition_window)
            self._edition_window.Bind(wx.EVT_SPLITTER_SASH_POS_CHANGED, self.on_sas_pos_changed)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/splitter.h>
        class_ = register_cpp_class(self.project, 'wxSplitterWindow')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="splitter window control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/splitter.h>')
        self.add_cpp_required_header('#include <functional>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f"{self._name} = new wxSplitterWindow({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n" \
               f"/* this hangs on windows (why?) " \
               f"std::function<void(wxEvent&)> {self._name}_init_sash = [&](wxEvent& unused) -> void {{\n" \
               f"    {self._name}->SetSashPosition( {self._sashpos} );\n" \
               f"    {self._name}->Unbind(wxEVT_IDLE, {self._name}_init_sash);\n" \
               "}};*/\n"
        code += super(SplitterWindowControl, self).cpp_init_code()
        child = self.sorted_wxui_child
        for element in child:
            code += element.cpp_init_code(self._name)
        if len(child) == 1:
            code += f'{self._name}->Initialize({child[0].name});\n'
        elif len(child) == 2:
            if self._split_mode == 'wxSPLIT_VERTICAL':
                code += f'{self._name}->SplitVertically({child[0].name}, {child[1].name});\n'
            else:
                code += f'{self._name}->SplitHorizontally({child[0].name}, {child[1].name});\n'
            code += f'{self._name}->SetMinimumPaneSize({self._min_pane_size});\n' \
                    f'{self._name}->SetSashGravity({self._sashgravity});\n' \
                    f'{self._name}->SetSashPosition({self._sashpos});\n' \
                    f'//{self._name}->Bind(wxEVT_IDLE, {self._name}_init_sash);\n'

        code += self.cc_add_to_sizer_code(container, self.name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'

        code = f'{this} = wx.SplitterWindow({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(SplitterWindowControl, self).python_init_control_code(parent)
        child = self.sorted_wxui_child
        for element in child:
            code += element.python_init_control_code(this)

        if len(child) == 1:
            code += f'{this}.Initialize(self.{child[0].name})\n'
        elif len(child) == 2:
            if self._split_mode == 'wxSPLIT_VERTICAL':
                code += f'{this}.SplitVertically(self.{child[0].name}, self.{child[1].name})\n'
            else:
                code += f'{this}.SplitHorizontally(self.{child[0].name}, self.{child[1].name})\n'
            code += f'{this}.SetMinimumPaneSize({self._min_pane_size})\n'
            code += f'{this}.SetSashGravity({self._sashgravity})\n'
            code += f'{this}.SetSashPosition({self._sashpos})\n'

        code += self.py_add_to_sizer_code(parent, this)
        return code
