import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class


class SimplebookControl(WindowControl):
    """"This class represents a simple book"""

    def __init__(self, **kwargs):
        # events
        self._on_page_changed = ''
        self._on_page_changing = ''
        super(SimplebookControl, self).__init__(**kwargs)

    @property
    def local_simplebookcontrol_kwargs(self):
        return {
            'on_page_changed': self._on_page_changed,
            'on_page_changing': self._on_page_changing,
        }

    @local_simplebookcontrol_kwargs.setter
    def local_simplebookcontrol_kwargs(self, kwargs):
        self._on_page_changed = kwargs.get('on_page_changed', self._on_page_changed)
        self._on_page_changing = kwargs.get('on_page_changing', self._on_page_changing)

    @property
    def kwargs(self):
        value = self.local_simplebookcontrol_kwargs
        value.update(super(SimplebookControl, self).kwargs)
        return value

    @property
    def local_simple_book_events(self):
        return {
            'on_page_changed': (self._on_page_changed, 'wx.EVT_BOOKCTRL_PAGE_CHANGED',
                                'wxEVT_COMMAND_BOOKCTRL_PAGE_CHANGED'),
            'on_page_changing': (self._on_page_changing, 'wx.EVT_BOOKCTRL_PAGE_CHANGING',
                                 'wxEVT_COMMAND_BOOKCTRL_PAGE_CHANGING'),
        }

    @property
    def events(self):
        value = self.local_simple_book_events
        value.update(super(SimplebookControl, self).events)
        return value

    def set_kwargs(self, kwargs):
        self.local_simplebookcontrol_kwargs = kwargs
        super(SimplebookControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        # TODO : find a better image
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('simple_book')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Simple book',
            'type': 'category',
            'help': 'Attributes of simple book.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The simplebook instance name."
                },
            ],
        },]
        _event = [{
            'label': 'Simplebook',
            'type': 'category',
            'help': 'Events of simplebook control',
            'child': [{
                'label': 'on page changed',
                'type': 'string',
                'read_only': False,
                'value': self._on_page_changed,
                'name': 'on_page_changed',  # kwarg value
                'help': 'Called when the simplebook page has changed.'
                },
                {
                    'label': 'on page changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_page_changing,
                    'name': 'on_page_changing',  # kwarg value
                    'help': 'Called when the simplebook page is about to change.'
                },
            ],
            }]
        _base_property, _base_event = super(SimplebookControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def is_sizer_root(self):
        return True

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        return False

    @property
    def can_add_control(self):
        return True

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.Simplebook)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            child = self.sorted_wxui_child
            for item in child:
                item.create_edition_window(self)
            self.add_to_sizer(container, self._edition_window)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/simplebook.h>
        class_ = register_cpp_class(self.project, 'wxSimplebook')
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="aui simple book window",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/simplebook.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f"{self._name} = new wxSimplebook({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"

        code += super(SimplebookControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.Simplebook({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(SimplebookControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code

