import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


class PanelControl(WindowControl):
    """"This class represents a panel"""

    def __init__(self, **kwargs):

        super(PanelControl, self).__init__(**kwargs)

    @property
    def local_panelcontrol_kwargs(self):
        return {}

    @local_panelcontrol_kwargs.setter
    def local_panelcontrol_kwargs(self, kwargs):
        pass

    @property
    def kwargs(self):
        value = self.local_panelcontrol_kwargs
        value.update(super(PanelControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_panelcontrol_kwargs = kwargs
        super(PanelControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('panel')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Panel',
            'type': 'category',
            'help': 'Attributes of panel',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The panel instance name."
                },
            ],
        },]
        _event = []
        _base_property, _base_event = super(PanelControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def is_sizer_root(self):
        """The sizer owner is the flatten parent of all sizers (even nested) in a window hierarchy.
        This is usually a frame, a dialog or a pane."""
        return True

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        from .. import layout
        layout_controls = [layout.WrapSizerControl, layout.StdDialogButtonSizerControl,
                           layout.StaticBoxSizerControl, layout.GridSizerControl,
                           layout.GridBagSizerControl, layout.FlexGridSizerControl,
                           layout.BoxSizerControl]
        try:
            return not next(x for x in layout_controls if len(self[x]) > 0)
        except StopIteration:
            return True

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.Panel)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            for item in self.sorted_wxui_child:
                item.create_edition_window(self)
            self.add_to_sizer(container, self._edition_window)
            self._edition_window.set_kwargs(**self.kwargs)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/panel.h>
        cls = register_cpp_class(self.project, 'wxPanel')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="panel window",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/panel.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f"{self._name} = new wxPanel({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"
        code += super(PanelControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'

        code = f'{this} = wx.Panel({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(PanelControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code
