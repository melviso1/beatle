import wx
from .._WindowControl import WindowControl
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style, extend_events


@with_style
@extend_events('collapsible_pane_events')
class CollapsiblePanelControl(WindowControl):
    """"This class represents a panel"""

    style_list = (wx.CP_DEFAULT_STYLE, wx.CP_NO_TLW_RESIZE)
    style_list_py_text = ('wx.CP_DEFAULT_STYLE', 'wx.CP_NO_TLW_RESIZE')
    style_list_cc_text = ('wxCP_DEFAULT_STYLE', 'wxCP_NO_TLW_RESIZE')

    def __init__(self, **kwargs):
        self._label = ''
        self._style = 0
        # events
        self._on_collapsible_toggled = ''
        self._on_navigation_key = ''
        super(CollapsiblePanelControl, self).__init__(**kwargs)

    @property
    def edition_window_client(self):
        """Collapsible windows works this way"""
        if self.edition_window is None:
            return None
        return self.edition_window.GetPane()

    @property
    def local_collapsible_panel_kwargs(self):
        return {
            'label': self._label,
            'style': self._style,
            #events
            'on_collapsible_toggled': self._on_collapsible_toggled,
            'on_navigation_key': self._on_navigation_key,
        }

    @local_collapsible_panel_kwargs.setter
    def local_collapsible_panel_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._style = kwargs.get('style', self._style)
        self._on_collapsible_toggled = kwargs.get('on_collapsible_toggled', self._on_collapsible_toggled)
        self._on_navigation_key = kwargs.get('on_navigation_key', self._on_navigation_key)

    @property
    def kwargs(self):
        value = self.local_collapsible_panel_kwargs
        value.update(super(CollapsiblePanelControl, self).kwargs)
        return value

    @property
    def collapsible_pane_events(self):
        return {
            'on_collapsible_toggled': (self._on_collapsible_toggled,
                                       'wx.EVT_COLLAPSIBLEPANE_CHANGED', 'wxEVT_COLLAPSIBLEPANE_CHANGED'),
            'on_navigation_key': (self._on_navigation_key, 'wx.EVT_NAVIGATION_KEY', 'wxEVT_NAVIGATION_KEY'),
        }

    def set_kwargs(self, kwargs):
        self.local_collapsible_panel_kwargs = kwargs
        super(CollapsiblePanelControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('collapsible_panel')

    @property
    def style_attribute_options(self):
        return [
            ('wxCP_DEFAULT_STYLE', "The default style. It includes wx.TAB_TRAVERSAL and wx.BORDER_NONE."),
            ('wxCP_NO_TLW_RESIZE', "By default wx.CollapsiblePane resizes the top level window containing it when its "
                               "own size changes. This allows easily implementing dialogs containing an optionally "
                               "shown part, for example, and so is the default behaviour but can be inconvenient "
                               "in some specific cases. This style is not active in the preview design window."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Collapsile panel',
            'type': 'category',
            'help': 'Attributes of collapsible panel',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The panel instance name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',
                    'help': " The label shown in the collapse button.",
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'read_only': False,
                    'value': self._style,
                    'name': 'style',  # kwarg value
                    'options': self.style_attribute_options,
                    'help': "The splitter window style."
                },
            ],
        },]
        _event = [{
            'label': 'Collapsible panel',
            'type': 'category',
            'help': 'Events of collapsible panel',
            'on_collapsible_toggled': self._on_collapsible_toggled,
            'on_navigation_key': self._on_navigation_key,

            'child': [
                {
                    'label': 'on toggled',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_collapsible_toggled,
                    'name': 'on_collapsible_toggled',
                    'help': " The user expanded or collapsed the collapsible pane.",
                },
                {
                    'label': 'on navigation',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_navigation_key,
                    'name': 'on_navigation_key',
                    'help': "Process a navigation key event.",
                },
            ]
        }]
        _base_property, _base_event = super(CollapsiblePanelControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def is_sizer_root(self):
        return True

    @property
    def can_add_layout_control(self):
        return True

    @property
    def can_add_control(self):
        """Only two controls can be added. We fork here from traditional wxformbuilder behaviour
        because there is no restriction about what kind of window you can add. """
        return False

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            # we must add wx.CP_NO_TLW_RESIZE, because this window
            # originate full beatle resize (very ugly)
            self._edition_window = uiMockupControl(wx.CollapsiblePane)(
                edition_frame, self.py_id, self._label, self.py_pos, self.py_size,
                self.style_value | wx.CP_NO_TLW_RESIZE)
            self._edition_window.Collapse(False)
            child = self.sorted_wxui_child
            for item in child:
                item.create_edition_window(self)
            self.add_to_sizer(container, self._edition_window)
            self._edition_window.bind(self)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/collpane.h>
        class_ = register_cpp_class(self.project, 'wxCollapsiblePane')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="collapsible pane",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/collpane.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = f'{self._name} = new wxCollapsiblePane({owner}, {self._id}, "{self._label}", {self.cc_pos_str}, ' \
               f'{self.cc_size_str}, {self.cc_style_str});\n'
        code += super(CollapsiblePanelControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    @property
    def py_owner_text(self):
        """Return suitable test for using as owner"""
        if self.is_top_window:
            return 'self.GetPane()'
        else:
            return 'self.{name}.GetPane()'.format(name=self._name)

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.CollapsiblePane({owner}, {self.py_id_str}, "{self._label}", {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += f'{this}.Collapse(False)\n'
        code += super(CollapsiblePanelControl, self).python_init_control_code(parent)
        child = self.sorted_wxui_child
        for element in child:
            code += element.python_init_control_code(self.py_owner_text)
        code += self.py_add_to_sizer_code(parent, this)
        return code

