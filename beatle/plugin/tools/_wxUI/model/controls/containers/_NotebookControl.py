import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style


@with_style
class NotebookControl(WindowControl):
    """"This class represents a panel"""

    style_list = (wx.NB_TOP, wx.NB_LEFT, wx.NB_RIGHT, wx.NB_BOTTOM,
                  wx.NB_FIXEDWIDTH, wx.NB_MULTILINE, wx.NB_NOPAGETHEME)

    style_list_py_text = ('wx.NB_TOP', 'wx.NB_LEFT', 'wx.NB_RIGHT', 'wx.NB_BOTTOM',
                          'wx.NB_FIXEDWIDTH', 'wx.NB_MULTILINE', 'wx.NB_NOPAGETHEME',)

    style_list_cc_text = ('wxNB_TOP', 'wxNB_LEFT', 'wxNB_RIGHT', 'wxNB_BOTTOM',
                          'wxNB_FIXEDWIDTH', 'wxNB_MULTILINE', 'wxNB_NOPAGETHEME',)

    def __init__(self, **kwargs):
        self._bitmap_size = (-1, -1)
        self._style = 1
        # events
        self._on_page_changed = ''
        self._on_page_changing = ''
        super(NotebookControl, self).__init__(**kwargs)

    @property
    def local_note_book_control_kwargs(self):
        return {
            'bitmap_size': self._bitmap_size,
            'style': self._style,
            'on_page_changed': self._on_page_changed,
            'on_page_changing': self._on_page_changing,
        }

    @local_note_book_control_kwargs.setter
    def local_note_book_control_kwargs(self, kwargs):
        self._bitmap_size = kwargs.get('bitmap_size', self._bitmap_size)
        self._style = kwargs.get('style', self._style)
        self._on_page_changed = kwargs.get('on_page_changed', self._on_page_changed)
        self._on_page_changing = kwargs.get('on_page_changing', self._on_page_changing)

    @property
    def kwargs(self):
        value = self.local_note_book_control_kwargs
        value.update(super(NotebookControl, self).kwargs)
        return value

    @property
    def local_note_book_events(self):
        return {
            'on_page_changed': (self._on_page_changed, 'wx.EVT_NOTEBOOK_PAGE_CHANGED', 'wxEVT_NOTEBOOK_PAGE_CHANGED'),
            'on_page_changing': (self._on_page_changing, 'wx.EVT_NOTEBOOK_PAGE_CHANGING',
                                 'wxEVT_NOTEBOOK_PAGE_CHANGING'),
        }

    @property
    def events(self):
        value = self.local_note_book_events
        value.update(super(NotebookControl, self).events)
        return value

    def set_kwargs(self, kwargs):
        self.local_note_book_control_kwargs = kwargs
        super(NotebookControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('notebook')

    @property
    def style_attribute_options(self):
        return [
            ('wxNB_TOP', "Place tabs on the top side."),
            ('wxNB_LEFT', "Place tabs on the left side."),
            ('wxNB_RIGHT', "Place tabs on the right side."),
            ('wxNB_BOTTOM', "Place tabs under instead of above the notebook pages."),
            ('wxNB_FIXEDWIDTH', "(Windows only) All tabs will have same width."),
            ('wxNB_MULTILINE', "(Windows only) There can be several rows of tabs."),
            ('wxNB_NOPAGETHEME',  "(Windows only) Display a solid colour on notebook pages, and not a gradient, "
                                  "which can reduce performance.")
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Notebook',
            'type': 'category',
            'help': 'Attributes of notebook.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The notebook instance name."
                },
                {
                    'label': 'bitmap size',
                    'type': 'int_vector',
                    'read_only': False,
                    'value': self._bitmap_size,
                    'name': 'bitmap_size',
                    'labels': [
                        ('width', "bitmap width"),
                        ('height', "bitmap height")
                    ],
                    'help': "Size of page images."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': "The notebook styles."
                },
            ],
        },]
        _event = [{
            'label': 'Notebook',
            'type': 'category',
            'help': 'Events of notebook control',
            'child': [{
                'label': 'on page changed',
                'type': 'string',
                'read_only': False,
                'value': self._on_page_changed,
                'name': 'on_page_changed',  # kwarg value
                'help': 'Called when the notebook page has changed.'
                },
                {
                    'label': 'on page changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_page_changing,
                    'name': 'on_page_changing',  # kwarg value
                    'help': 'Called when the notebook page is about to change.'
                },
            ],
            }]
        _base_property, _base_event = super(NotebookControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def is_sizer_root(self):
        return True

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        return False

    @property
    def can_add_control(self):
        return True

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.Notebook)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            if self._bitmap_size[0] != -1 and self._bitmap_size[1] != -1:
                self.m_notebook2.AssignImageList(wx.ImageList(self._bitmap_size[0], self._bitmap_size[1]))
            child = self.sorted_wxui_child
            for item in child:
                item.create_edition_window(self)
            self.add_to_sizer(container, self._edition_window)
            self._edition_window.set_kwargs(**self.kwargs)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/notebook.h>
        class_ = register_cpp_class(self.project, 'wxNotebook')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="note book window",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/notebook.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f"{self._name} = new wxNotebook({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"
        code += super(NotebookControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.Notebook({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(NotebookControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code


