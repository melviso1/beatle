import wx
import wx.aui
from .._WindowControl import WindowControl
from ....lib import popup_errors
from beatle.model import cc, py
from ....lib import register_cpp_class, with_style, extend_events


@with_style
@extend_events('aui_note_book_events')
class AuiNotebookControl(WindowControl):
    """"This class represents a panel"""

    style_list = (
        wx.aui.AUI_NB_DEFAULT_STYLE, wx.aui.AUI_NB_TAB_SPLIT, wx.aui.AUI_NB_TAB_MOVE, wx.aui.AUI_NB_TAB_EXTERNAL_MOVE,
        wx.aui.AUI_NB_TAB_FIXED_WIDTH, wx.aui.AUI_NB_SCROLL_BUTTONS, wx.aui.AUI_NB_WINDOWLIST_BUTTON,
        wx.aui.AUI_NB_CLOSE_BUTTON, wx.aui.AUI_NB_CLOSE_ON_ACTIVE_TAB, wx.aui.AUI_NB_CLOSE_ON_ALL_TABS,
        wx.aui.AUI_NB_MIDDLE_CLICK_CLOSE, wx.aui.AUI_NB_TOP, wx.aui.AUI_NB_BOTTOM,
    )

    style_list_py_text = (
        'wx.aui.AUI_NB_DEFAULT_STYLE', 'wx.aui.AUI_NB_TAB_SPLIT', 'wx.aui.AUI_NB_TAB_MOVE',
        'wx.aui.AUI_NB_TAB_EXTERNAL_MOVE', 'wx.aui.AUI_NB_TAB_FIXED_WIDTH', 'wx.aui.AUI_NB_SCROLL_BUTTONS',
        'wx.aui.AUI_NB_WINDOWLIST_BUTTON', 'wx.aui.AUI_NB_CLOSE_BUTTON', 'wx.aui.AUI_NB_CLOSE_ON_ACTIVE_TAB',
        'wx.aui.AUI_NB_CLOSE_ON_ALL_TABS', 'wx.aui.AUI_NB_MIDDLE_CLICK_CLOSE', 'wx.aui.AUI_NB_TOP',
        'wx.aui.AUI_NB_BOTTOM',
    )

    style_list_cc_text = (
        'wxAUI_NB_DEFAULT_STYLE', 'wxAUI_NB_TAB_SPLIT', 'wxAUI_NB_TAB_MOVE',
        'wxAUI_NB_TAB_EXTERNAL_MOVE', 'wxAUI_NB_TAB_FIXED_WIDTH', 'wxAUI_NB_SCROLL_BUTTONS',
        'wxAUI_NB_WINDOWLIST_BUTTON', 'wxAUI_NB_CLOSE_BUTTON', 'wxAUI_NB_CLOSE_ON_ACTIVE_TAB',
        'wxAUI_NB_CLOSE_ON_ALL_TABS', 'wxAUI_NB_MIDDLE_CLICK_CLOSE', 'wxAUI_NB_TOP',
        'wxAUI_NB_BOTTOM',
    )

    def __init__(self, **kwargs):
        self._style = 1
        self._tab_ctrl_height = -1
        self._uniform_bitmap_size = (-1, -1)
        # events
        self._on_auinotebook_page_close = ''
        self._on_auinotebook_page_closed = ''
        self._on_auinotebook_page_changed = ''
        self._on_auinotebook_page_changing = ''
        self._on_auinotebook_button = ''
        self._on_auinotebook_begin_drag = ''
        self._on_auinotebook_end_drag = ''
        self._on_auinotebook_drag_motion = ''
        self._on_auinotebook_allow_dnd = ''
        self._on_auinotebook_drag_done = ''
        self._on_auinotebook_tab_middle_down = ''
        self._on_auinotebook_tab_middle_up = ''
        self._on_auinotebook_tab_right_down = ''
        self._on_auinotebook_tab_right_up = ''
        self._on_auinotebook_bg_dclick = ''
        super(AuiNotebookControl, self).__init__(**kwargs)

    @property
    def local_auinotebookcontrol_kwargs(self):
        return {
            'style': self._style,
            'tab_ctrl_height': self._tab_ctrl_height,
            'uniform_bitmap_size': self._uniform_bitmap_size,
            # events
            'on_auinotebook_page_close': self._on_auinotebook_page_close,
            'on_auinotebook_page_closed': self._on_auinotebook_page_closed,
            'on_auinotebook_page_changed': self._on_auinotebook_page_changed,
            'on_auinotebook_page_changing': self._on_auinotebook_page_changing,
            'on_auinotebook_button': self._on_auinotebook_button,
            'on_auinotebook_begin_drag': self._on_auinotebook_begin_drag,
            'on_auinotebook_end_drag': self._on_auinotebook_end_drag,
            'on_auinotebook_drag_motion': self._on_auinotebook_drag_motion,
            'on_auinotebook_allow_dnd': self._on_auinotebook_allow_dnd,
            'on_auinotebook_drag_done': self._on_auinotebook_drag_done,
            'on_auinotebook_tab_middle_down': self._on_auinotebook_tab_middle_down,
            'on_auinotebook_tab_middle_up': self._on_auinotebook_tab_middle_up,
            'on_auinotebook_tab_right_down': self._on_auinotebook_tab_right_down,
            'on_auinotebook_tab_right_up': self._on_auinotebook_tab_right_up,
            'on_auinotebook_bg_dclick': self._on_auinotebook_bg_dclick,
        }

    @local_auinotebookcontrol_kwargs.setter
    def local_auinotebookcontrol_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        self._tab_ctrl_height = kwargs.get('tab_ctrl_height', self._tab_ctrl_height)
        self._uniform_bitmap_size = tuple(kwargs.get('uniform_bitmap_size', self._uniform_bitmap_size))
        # events
        self._on_auinotebook_page_close = kwargs.get('on_auinotebook_page_close', self._on_auinotebook_page_close)
        self._on_auinotebook_page_closed = kwargs.get('on_auinotebook_page_closed', self._on_auinotebook_page_closed)
        self._on_auinotebook_page_changed = kwargs.get('on_auinotebook_page_changed', self._on_auinotebook_page_changed)
        self._on_auinotebook_page_changing = kwargs.get('on_auinotebook_page_changing',
                                                        self._on_auinotebook_page_changing)
        self._on_auinotebook_button = kwargs.get('on_auinotebook_button', self._on_auinotebook_button)
        self._on_auinotebook_begin_drag = kwargs.get('on_auinotebook_begin_drag', self._on_auinotebook_begin_drag)
        self._on_auinotebook_end_drag = kwargs.get('on_auinotebook_end_drag', self._on_auinotebook_end_drag)
        self._on_auinotebook_drag_motion = kwargs.get('on_auinotebook_drag_motion', self._on_auinotebook_drag_motion)
        self._on_auinotebook_allow_dnd = kwargs.get('on_auinotebook_allow_dnd', self._on_auinotebook_allow_dnd)
        self._on_auinotebook_drag_done = kwargs.get('on_auinotebook_drag_done', self._on_auinotebook_drag_done)
        self._on_auinotebook_tab_middle_down = kwargs.get('on_auinotebook_tab_middle_down',
                                                          self._on_auinotebook_tab_middle_down)
        self._on_auinotebook_tab_middle_up = kwargs.get('on_auinotebook_tab_middle_up',
                                                        self._on_auinotebook_tab_middle_up)
        self._on_auinotebook_tab_right_down = kwargs.get('on_auinotebook_tab_right_down',
                                                         self._on_auinotebook_tab_right_down)
        self._on_auinotebook_tab_right_up = kwargs.get('on_auinotebook_tab_right_up', self._on_auinotebook_tab_right_up)
        self._on_auinotebook_bg_dclick = kwargs.get('on_auinotebook_bg_dclick', self._on_auinotebook_bg_dclick)

    @property
    def aui_note_book_events(self):
        return {
            'on_auinotebook_page_close': (self._on_auinotebook_page_close, 'wx.aui.EVT_AUINOTEBOOK_PAGE_CLOSE'),
            'on_auinotebook_page_closed': (self._on_auinotebook_page_closed, 'wx.aui.EVT_AUINOTEBOOK_PAGE_CLOSED'),
            'on_auinotebook_page_changed': (self._on_auinotebook_page_changed, 'wx.aui.EVT_AUINOTEBOOK_PAGE_CHANGED'),
            'on_auinotebook_page_changing': (self._on_auinotebook_page_changing,
                                             'wx.aui.EVT_AUINOTEBOOK_PAGE_CHANGING'),
            'on_auinotebook_button': (self._on_auinotebook_button, 'wx.aui.EVT_AUINOTEBOOK_BUTTON'),
            'on_auinotebook_begin_drag': (self._on_auinotebook_begin_drag, 'wx.aui.EVT_AUINOTEBOOK_BEGIN_DRAG'),
            'on_auinotebook_end_drag': (self._on_auinotebook_end_drag, 'wx.aui.EVT_AUINOTEBOOK_END_DRAG'),
            'on_auinotebook_drag_motion': (self._on_auinotebook_drag_motion, 'wx.aui.EVT_AUINOTEBOOK_DRAG_MOTION'),
            'on_auinotebook_allow_dnd': (self._on_auinotebook_allow_dnd, 'wx.aui.EVT_AUINOTEBOOK_ALLOW_DND'),
            'on_auinotebook_drag_done': (self._on_auinotebook_drag_done, 'wx.aui.EVT_AUINOTEBOOK_DRAG_DONE'),
            'on_auinotebook_tab_middle_down': (self._on_auinotebook_tab_middle_down,
                                               'wx.aui.EVT_AUINOTEBOOK_TAB_MIDDLE_DOWN'),
            'on_auinotebook_tab_middle_up': (self._on_auinotebook_tab_middle_up,
                                             'wx.aui.EVT_AUINOTEBOOK_TAB_MIDDLE_UP'),
            'on_auinotebook_tab_right_down': (self._on_auinotebook_tab_right_down,
                                              'wx.aui.EVT_AUINOTEBOOK_TAB_RIGHT_DOWN'),
            'on_auinotebook_tab_right_up': (self._on_auinotebook_tab_right_up,
                                            'wx.aui.EVT_AUINOTEBOOK_TAB_RIGHT_UP'),
            'on_auinotebook_bg_dclick': (self._on_auinotebook_bg_dclick, 'wx.aui.EVT_AUINOTEBOOK_BG_DCLICK'),
        }

    @property
    def kwargs(self):
        value = self.local_auinotebookcontrol_kwargs
        value.update(super(AuiNotebookControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_auinotebookcontrol_kwargs = kwargs
        page_index = wx.NOT_FOUND
        if self._edition_window:
            page = self._edition_window.GetCurrentPage()
            if page is not None:
                page_index = self._edition_window.GetPageIndex(page)
        super(AuiNotebookControl, self).set_kwargs(kwargs)
        if self.edition_window:
            self._edition_window.set_kwargs(**kwargs)
            if page_index != wx.NOT_FOUND:
                try:
                    page = self._edition_window.GetPage(page_index)
                    self._edition_window.SetSelection(page)
                except:
                    pass

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('aui_notebook')

    @property
    def style_attribute_options(self):
        return [
            ('wx.AUI_NB_DEFAULT_STYLE',
             "Defined as wx.aui.AUI_NB_TOP | wx.aui.AUI_NB_TAB_SPLIT | wx.aui.AUI_NB_TAB_MOVE | "
             "wx.aui.AUI_NB_SCROLL_BUTTONS | wx.aui.AUI_NB_CLOSE_ON_ACTIVE_TAB | wx.aui.AUI_NB_MIDDLE_CLICK_CLOSE."),
            ('wx.AUI_NB_TAB_SPLIT', "Allows the tab control to be split by dragging a tab."),
            ('wx.AUI_NB_TAB_MOVE', "Allows a tab to be moved horizontally by dragging."),
            ('wx.AUI_NB_TAB_EXTERNAL_MOVE', "Allows a tab to be moved to another tab control."),
            ('wx.AUI_NB_TAB_FIXED_WIDTH', "With this style, all tabs have the same width."),
            ('wx.AUI_NB_SCROLL_BUTTONS', "With this style, left and right scroll buttons are displayed."),
            ('wx.AUI_NB_WINDOWLIST_BUTTON', "With this style, a drop-down list of windows is available."),
            ('wx.AUI_NB_CLOSE_BUTTON', "With this style, a close button is available on the tab bar."),
            ('wx.AUI_NB_CLOSE_ON_ACTIVE_TAB', "With this style, the close button is visible on the active tab."),
            ('wx.AUI_NB_CLOSE_ON_ALL_TABS', "With this style, the close button is visible on all tabs."),
            ('wx.AUI_NB_MIDDLE_CLICK_CLOSE', "With this style, middle click on a tab closes the tab."),
            ('wx.AUI_NB_TOP', "With this style, tabs are drawn along the top of the notebook."),
            ('wx.AUI_NB_BOTTOM', "With this style, tabs are drawn along the bottom of the notebook."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Aui notebook',
            'type': 'category',
            'help': 'Attributes of advanced user interface (aui) notebook.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The notebook instance name."
                },
                {
                    'label': 'tab height',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._tab_ctrl_height,
                    'name': 'tab_ctrl_height',
                    'help': "Set the tab height."
                },
                {
                    'label': 'bitmap size',
                    'type': 'int_vector',
                    'read_only': False,
                    'value': self._uniform_bitmap_size,
                    'name': 'uniform_bitmap_size',
                    'labels': [
                        ('width', "bitmap width"),
                        ('height', "bitmap height")
                    ],
                    'help': "Set uniform bitmap size for all the tabs, effectively providing a uniform tab size."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': "The aui notebook styles."
                },
            ],
        },]
        _event = [{
            'label': 'AuiNotebook',
            'type': 'category',
            'help': 'Events of aui notebook control',
            'child': [
                {
                    'label': 'on page close',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_page_close,
                    'name': 'on_auinotebook_page_close',
                    'help': "A page is about to be closed. Processes a wxEVT_AUINOTEBOOK_PAGE_CLOSE event.",
                },
                {
                    'label': 'on page closed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_page_closed,
                    'name': 'on_auinotebook_page_closed',
                    'help': "A page has been closed. Processes a wxEVT_AUINOTEBOOK_PAGE_CLOSED event.",
                },
                {
                    'label': 'on page changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_page_changed,
                    'name': 'on_auinotebook_page_changed',
                    'help': "The page selection was changed. Processes a wxEVT_AUINOTEBOOK_PAGE_CHANGED event.",
                },
                {
                    'label': 'on page changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_page_changing,
                    'name': 'on_auinotebook_page_changing',
                    'help': "The page selection is about to be changed. Processes a wxEVT_AUINOTEBOOK_PAGE_CHANGING "
                            "event. This event can be vetoed.",
                },
                {
                    'label': 'on button',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_button,
                    'name': 'on_auinotebook_button',
                    'help': "The window list button has been pressed. Processes a wxEVT_AUINOTEBOOK_BUTTON event.",
                },
                {
                    'label': 'on begin drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_begin_drag,
                    'name': 'on_auinotebook_begin_drag',
                    'help': "Dragging is about to begin. Processes a wxEVT_AUINOTEBOOK_BEGIN_DRAG event.",
                },
                {
                    'label': 'on end drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_end_drag,
                    'name': 'on_auinotebook_end_drag',
                    'help': "Dragging has ended. Processes a wxEVT_AUINOTEBOOK_END_DRAG event.",
                },
                {
                    'label': 'on drag motion',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_drag_motion,
                    'name': 'on_auinotebook_drag_motion',
                    'help': "Emitted during a drag and drop operation. Processes a "
                            "wxEVT_AUINOTEBOOK_DRAG_MOTION event.",
                },
                {
                    'label': 'on allow dnd',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_allow_dnd,
                    'name': 'on_auinotebook_allow_dnd',
                    'help': "Whether to allow a tab to be dropped. Processes a wxEVT_AUINOTEBOOK_ALLOW_DND event. "
                            "This event must be specially allowed.",
                },
                {
                    'label': 'on drag done',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_drag_done,
                    'name': 'on_auinotebook_drag_done',
                    'help': "Notify that the tab has been dragged.Processes a wxEVT_AUINOTEBOOK_DRAG_DONE event.",
                },
                {
                    'label': 'on tab middle down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_tab_middle_down,
                    'name': 'on_auinotebook_tab_middle_down',
                    'help': "The middle mouse button is pressed on a tab. Processes a "
                            "wxEVT_AUINOTEBOOK_TAB_MIDDLE_DOWN event.",
                },
                {
                    'label': 'on tab middle up',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_tab_middle_up,
                    'name': 'on_auinotebook_tab_middle_up',
                    'help': "The middle mouse button is released on a tab. Processes a "
                            "wxEVT_AUINOTEBOOK_TAB_MIDDLE_UP event.",
                },
                {
                    'label': 'on tab right down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_tab_right_down,
                    'name': 'on_auinotebook_tab_right_down',
                    'help': "The right mouse button is pressed on a tab. Processes a "
                            "wxEVT_AUINOTEBOOK_TAB_RIGHT_DOWN event.",
                },
                {
                    'label': 'on tab right up',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_tab_right_up,
                    'name': 'on_auinotebook_tab_right_up',
                    'help': "The right mouse button is released on a tab. Processes a "
                            "wxEVT_AUINOTEBOOK_TAB_RIGHT_UP event.",
                },
                {
                    'label': 'on bg dclick',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_auinotebook_bg_dclick,
                    'name': 'on_auinotebook_bg_dclick',
                    'help': "Double clicked on the tabs background area. Processes a "
                            "wxEVT_AUINOTEBOOK_BG_DCLICK event",
                },]
        }]
        _base_property, _base_event = super(AuiNotebookControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    @property
    def is_sizer_root(self):
        return True

    @property
    def can_add_layout_control(self):
        # Only one layout element can be dropped
        return False

    @property
    def can_add_control(self):
        return True

    @property
    def edition_window_client(self):
        """Return the control to be used as client container
        in the mockup visualization"""
        return self._edition_window

    def veto_close_page(self, event):
        wx.MessageBox("Close pages is disabled in design mode","Design mode")
        event.Veto()

    @popup_errors
    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.parent.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.aui.AuiNotebook)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            if self._tab_ctrl_height > -1:
                self._edition_window.SetTabCtrlHeight(self._tab_ctrl_height)
            if self._uniform_bitmap_size[0] > -1 and self._uniform_bitmap_size[1] > -1:
                self._edition_window.SetUniformBitmapSize(
                    wx.Size(self._tab_ctrl_height[0], self._tab_ctrl_height[1]))
            child = self.sorted_wxui_child
            for item in child:
                item.create_edition_window(self)
            self.add_to_sizer(container, self._edition_window)
            self._edition_window.Bind(wx.aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.veto_close_page)
        except Exception as exception:
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        if len(child) > 0:
            self._edition_window.SetSelection(0)
        self._edition_window.Show()

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/aui/auibook.h>
        class_ = register_cpp_class(self.project, 'wxAuiNotebook')
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="aui note book window",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/aui/auibook.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name

        code = f"{self._name} = new wxAuiNotebook({owner}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"
        code += super(AuiNotebookControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.aui')
        this = f'self.{self.name}'
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = f'{this} = wx.aui.AuiNotebook({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(AuiNotebookControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code


