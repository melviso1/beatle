"""This file introduces the class WindowControl. The mission of this class
is to help dry style and avoid spawning the code shared by controls.
Controls are windows that are not containers for other windows and also
share behaviours"""

from .._Window import Window


class WindowControl(Window):

    """Common base for window controls."""

    def __init__(self, *args, **kwargs):
        super(WindowControl, self).__init__(*args, **kwargs)

    def set_kwargs(self, kwargs):
        super(WindowControl, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            main_window = self.top_window
            if main_window is not None:
                main_window.update_implementation()


    @property
    def can_add_control(self):
        """By default, any control can hold another control inside."""
        return False

    @property
    def can_add_layout_control(self):
        """By default, any control can hold a sizer inside.
        An exception for this behavior are the container controls."""
        return False

    def destroy_edition_window(self):
        if self._edition_window is not None:
            reverse_sorted = reversed(self.sorted_wxui_child)
            for child in reverse_sorted:
                child.destroy_edition_window()
            if self.subitem_adaptor is not None:
                self.subitem_adaptor.detach_from_parent()
            self._edition_window.Destroy()
            self._edition_window = None

    def delete(self):
        """Do the deletion of the element.
        Must also delete the implementation class."""
        # if self._implementation is not None:
        #     # context.render_undo_redo_removing(self._implementation)
        #     self._implementation.on_undo_redo_removing()
        #     self._implementation.remove_relations()
        #     self._implementation = None
        # if self._subitem_adaptor is not None:
        #     context.render_undo_redo_removing(self._subitem_adaptor)
        #     self._subitem_adaptor.remove_relations()
        #     # self._subitem_adaptor = None intentionally dangling
        super(WindowControl, self).delete()

    def delete_children(self):
        """Ensure remove first graphical child in reverse order"""
        reverse_sorted = reversed(self.sorted_wxui_child)
        for child in reverse_sorted:
            child.delete()
        super(WindowControl, self).delete_children()
