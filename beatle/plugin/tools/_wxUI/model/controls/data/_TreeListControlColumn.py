import wx
import wx.dataview
from beatle.model import cc
from beatle.lib.utils import cached_type
from .._WindowControl import WindowControl
from ._TreeListControl import TreeListControl


class TreeListControlColumn(WindowControl):
    """"This class represents a tree list control column.
    This is not a real control, so his _edition_window will be
    only a None or a numeric index of the column inside the control"""

    def __init__(self, **kwargs):
        self._header = 'Column header'
        self._width = 'wxCOL_WIDTH_DEFAULT'
        self._alignment = 'wxALIGN_LEFT'
        self._flag = 1
        # events
        super(TreeListControlColumn, self).__init__(**kwargs)
        self.set_kwargs(kwargs)

    @property
    def local_treelistctrlcolumn_kwargs(self):
        return {
            'header': self._header,
            'width': self._width,
            'alignment': self._alignment,
            'flag': self._flag
        }

    @local_treelistctrlcolumn_kwargs.setter
    def local_treelistctrlcolumn_kwargs(self, kwargs):
        self._header= kwargs.get('header', self._header)
        self._width = kwargs.get('width', self._width)
        self._alignment = kwargs.get('alignment', self._alignment)
        self._flag = kwargs.get('flag', self._flag)

    @property
    def kwargs(self):
        value = self.local_treelistctrlcolumn_kwargs
        value.update(super(TreeListControlColumn, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_treelistctrlcolumn_kwargs = kwargs
        super(TreeListControlColumn, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('tree_list_control_column')

    @property
    def flag_attribute_options(self):
        return [
            ('wxCOL_RESIZABLE', "Allow column resizing."),
            ('wxCOL_SORTABLE', "Allow sort the control content by clicking in the column."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Tree list column',
            'type': 'category',
            'help': 'Attributes of tree list column.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The tree control column name."
                },
                {
                    'label': 'header',
                    'type': 'string',
                    'read_only': False,
                    'value': self._header,
                    'name': 'header',  # kwarg value
                    'help': "The tree control column header."
                },
                {
                    'label': 'width',
                    'type': 'edit-choice',
                    'value': self._width,
                    'name': 'width',
                    'help': 'Sets the column width.',
                    'choices': ['wxCOL_WIDTH_AUTOSIZE', 'wxCOL_WIDTH_DEFAULT'],
                },
                {
                    'label': 'alignment',
                    'type': 'choice',
                    'value': self._alignment,
                    'name': 'alignment',
                    'help': 'Sets the column alignment.',
                    'choices': ['wxALIGN_LEFT', 'wxALIGN_CENTER', 'wxALIGN_RIGHT'],
                },
                {
                    'label': 'flag',
                    'type': 'multi_choice',
                    'value': self._flag,
                    'name': 'flag',
                    'options': self.flag_attribute_options,
                    'help': 'Sets the tree list control style.'
                }]
        }]
        _event = []

        return _property, _event

    @property
    def flag_value(self):
        """Return the value required for dynamic column creation"""
        self_flag_value = 0
        if self._flag & 1:
            self_flag_value |= wx.COL_RESIZABLE
        if self._flag & 2:
            self_flag_value |= wx.COL_SORTABLE
        return self_flag_value

    @property
    def flag_value_py_text(self):
        """Return the value required for dynamic column creation"""
        if self._flag == 0:
            return '0'
        values = ('wx.COL_RESIZABLE', 'wx.COL_SORTABLE')
        return '|'.join(values[index] for index in (0,1) if self._flag & (2**index))

    def create_edition_window(self, parent):
        container = getattr(self.parent, '_edition_window', None)
        if type(self.parent) is not TreeListControl:
            return
        if self._width == 'wxCOL_WIDTH_AUTOSIZE':
            width = wx.COL_WIDTH_AUTOSIZE
        elif self._width == 'wxCOL_WIDTH_DEFAULT':
            width = wx.COL_WIDTH_DEFAULT
        else:
            width = int(self._width)

        if self._alignment == 'wxALIGN_LEFT':
            alignment = wx.ALIGN_LEFT
        elif self._alignment == 'wxALIGN_CENTER':
            alignment = wx.ALIGN_CENTER
        elif self._alignment == 'wxALIGN_RIGHT':
            alignment = wx.ALIGN_RIGHT
        else:
            alignment = wx.ALIGN_CENTER   # error

        self._edition_window = container.AppendColumn(self._header, width, alignment, self.flag_value)
        if self._edition_window == -1:
            self._edition_window = None
            raise RuntimeError('Failed to add column to TreeListCtrl')

    def destroy_edition_window(self):
        if self._edition_window is not None:
            container = getattr(self.parent, '_edition_window', None)
            if type(self.parent) is TreeListControl:
                container.DeleteColumn(self._edition_window)
        self._edition_window = None

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # add a controls folder in order to avoid garbage
        int_ = cached_type(self.project, 'int')
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="tree list control column index",
            name=self._name,
            type=cc.typeinst(type=int_),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/treelist.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = '{name} = {owner}->AppendColumn("{header}", {width}, {align}, {flag});\n' .format(
            name=self.name, owner=owner, header=self._header, width=self._width,
            align=self._alignment, flag=self.flag_value)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = 'self.{name}'.format(name=window_parent.name)

        if self._alignment == 'wxALIGN_LEFT':
            alignment = wx.ALIGN_LEFT
        elif self._alignment == 'wxALIGN_CENTER':
            alignment = wx.ALIGN_CENTER
        elif self._alignment == 'wxALIGN_RIGHT':
            alignment = wx.ALIGN_RIGHT
        else:
            alignment = wx.ALIGN_CENTER   # error

        if self._width == 'wxCOL_WIDTH_AUTOSIZE':
            width = 'wx.COL_WIDTH_AUTOSIZE'
        elif self._width == 'wxCOL_WIDTH_DEFAULT':
            width = 'wx.COL_WIDTH_DEFAULT'
        else:
            width = int(self._width)

        code = '{this} = {owner}.AppendColumn("{header}", {width}, {align}, {flag});\n' .format(
            this=this, owner=owner, header=self._header, width=self._width,
            align=alignment, flag=self.flag_value_py_text)
        return code
