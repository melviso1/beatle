import wx
import wx.grid as wxg
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc, py
from ....lib import register_cpp_class


class GridControl(WindowControl):
    """"This class represents a grid control"""

    style_list = ()

    def __init__(self, **kwargs):
        # grid properties
        self._rows = 5
        self._columns = 5
        self._editing = True
        self._grid_lines = True
        self._grid_line_color = wx.SYS_COLOUR_WINDOWTEXT
        self._drag_grid_size = False
        self._margin_width = 0
        self._margin_height = 0
        # columns properties
        self._column_sizes = []
        self._autosize_columns = False
        self._drag_column_move = False
        self._drag_column_size = True
        self._column_label_size = 30
        self._column_labels = []
        self._column_label_horizontal_alignment = 'wxALIGN_CENTRE'
        self._column_label_vertical_alignment = 'wxALIGN_CENTRE'
        # rows properties
        self._row_sizes = []
        self._autosize_rows = False
        self._drag_row_move = False
        self._drag_row_size = True
        self._row_label_size = 80
        self._row_labels = []
        self._row_label_horizontal_alignment = 'wxALIGN_CENTRE'
        self._row_label_vertical_alignment = 'wxALIGN_CENTRE'
        # label properties
        self._label_background_colour = wx.SYS_COLOUR_WINDOW
        self._label_font = '-1; Default; ; Normal; Normal; Not Underlined'
        self._label_text_colour = wx.SYS_COLOUR_WINDOWTEXT
        # cell properties
        self._cell_background_colour = wx.SYS_COLOUR_WINDOW
        self._cell_font = '-1; Default; ; Normal; Normal; Not Underlined'
        self._cell_text_colour = wx.SYS_COLOUR_WINDOWTEXT
        self._cell_horizontal_alignment = 'wxALIGN_LEFT'
        self._cell_vertical_alignment = 'wxALIGN_TOP'

        # grid events
        self._on_grid_cell_changing = ''
        self._on_grid_cell_changed = ''
        self._on_grid_cell_left_click = ''
        self._on_grid_cell_left_dclick = ''
        self._on_grid_cell_right_click = ''
        self._on_grid_cell_right_dclick = ''
        self._on_grid_editor_hidden = ''
        self._on_grid_editor_shown = ''
        self._on_grid_label_left_click = ''
        self._on_grid_label_left_dclick = ''
        self._on_grid_label_right_click = ''
        self._on_grid_label_right_dclick = ''
        self._on_grid_select_cell = ''
        self._on_grid_row_move = ''
        self._on_grid_col_move = ''
        self._on_grid_col_sort = ''
        self._on_grid_tabbing = ''
        # grid size events
        self._on_grid_cmd_col_size = ''
        self._on_grid_cmd_row_size = ''
        self._on_grid_row_auto_size = ''
        self._on_grid_col_size = ''
        self._on_grid_col_auto_size = ''
        self._on_grid_row_size = ''
        # grid range events
        self._on_grid_range_selecting = ''
        self._on_grid_cmd_range_selecting = ''
        self._on_grid_range_selected = ''
        self._on_grid_cmd_range_selected = ''
        # grid editor events
        self._on_grid_editor_created = ''
        self._on_grid_cmd_editor_created = ''
        super(GridControl, self).__init__(**kwargs)

    @property
    def local_gridcontrol_kwargs(self):
        return {
            # grid properties
            'rows': self._rows,
            'columns': self._columns,
            'editing': self._editing,
            'grid_lines': self._grid_lines,
            'grid_line_color': self._grid_line_color,
            'drag_grid_size': self._drag_grid_size,
            'margin_width': self._margin_width,
            'margin_height': self._margin_height,
            # columns properties
            'column_sizes': self._column_sizes,
            'autosize_columns': self._autosize_columns,
            'drag_column_move': self._drag_column_move,
            'drag_column_size': self._drag_column_size,
            'column_label_size': self._column_label_size,
            'column_labels': self._column_labels,
            'column_label_horizontal_alignment': self._column_label_horizontal_alignment,
            'column_label_vertical_alignment': self._column_label_vertical_alignment,
            # rows properties
            'row_sizes': self._row_sizes,
            'autosize_rows': self._autosize_rows,
            'drag_row_move': self._drag_row_move,
            'drag_row_size': self._drag_row_size,
            'row_label_size': self._row_label_size,
            'row_labels': self._row_labels,
            'row_label_horizontal_alignment': self._row_label_horizontal_alignment,
            'row_label_vertical_alignment': self._row_label_vertical_alignment,
            # label properties
            'label_background_colour': self._label_background_colour,
            'label_font': self._label_font,
            'label_text_colour': self._label_text_colour,
            # cell properties
            'cell_background_colour': self._cell_background_colour,
            'cell_font': self._cell_font,
            'cell_text_colour': self._cell_text_colour,
            'cell_horizontal_alignment': self._cell_horizontal_alignment,
            'cell_vertical_alignment': self._cell_vertical_alignment,
            # grid events
            'on_grid_cell_changing': self._on_grid_cell_changing,
            'on_grid_cell_changed': self._on_grid_cell_changed,
            'on_grid_cell_left_click': self._on_grid_cell_left_click,
            'on_grid_cell_left_dclick': self._on_grid_cell_left_dclick,
            'on_grid_cell_right_click': self._on_grid_cell_right_click,
            'on_grid_cell_right_dclick': self._on_grid_cell_right_dclick,
            'on_grid_editor_hidden': self._on_grid_editor_hidden,
            'on_grid_editor_shown': self._on_grid_editor_shown,
            'on_grid_label_left_click': self._on_grid_label_left_click,
            'on_grid_label_left_dclick': self._on_grid_label_left_dclick,
            'on_grid_label_right_click': self._on_grid_label_right_click,
            'on_grid_label_right_dclick': self._on_grid_label_right_dclick,
            'on_grid_select_cell': self._on_grid_select_cell,
            'on_grid_row_move': self._on_grid_row_move,
            'on_grid_col_move': self._on_grid_col_move,
            'on_grid_col_sort': self._on_grid_col_sort,
            'on_grid_tabbing': self._on_grid_tabbing,
            # grid size events
            'on_grid_cmd_col_size': self._on_grid_cmd_col_size,
            'on_grid_cmd_row_size': self._on_grid_cmd_row_size,
            'on_grid_row_auto_size': self._on_grid_row_auto_size,
            'on_grid_col_size': self._on_grid_col_size,
            'on_grid_col_auto_size': self._on_grid_col_auto_size,
            'on_grid_row_size': self._on_grid_row_size,
            # grid range events
            'on_grid_range_selecting': self._on_grid_range_selecting,
            'on_grid_cmd_range_selecting': self._on_grid_cmd_range_selecting,
            'on_grid_range_selected': self._on_grid_range_selected,
            'on_grid_cmd_range_selected': self._on_grid_cmd_range_selected,
            # grid editor events
            'on_grid_editor_created': self._on_grid_editor_created,
            'on_grid_cmd_editor_created': self._on_grid_cmd_editor_created
        }

    @local_gridcontrol_kwargs.setter
    def local_gridcontrol_kwargs(self, kwargs):
        # grid properties
        self._rows = kwargs.get('rows', self._rows)
        self._columns = kwargs.get('columns', self._columns)
        self._editing = kwargs.get('editing', self._editing)
        self._grid_lines = kwargs.get('grid_lines', self._grid_lines)
        self._grid_line_color = kwargs.get('grid_line_color', self._grid_line_color)
        self._drag_grid_size = kwargs.get('drag_grid_size', self._drag_grid_size)
        self._margin_width = kwargs.get('margin_width', self._margin_width)
        self._margin_height = kwargs.get('margin_height', self._margin_height)
        # columns properties
        self._column_sizes = kwargs.get('column_sizes', self._column_sizes)
        self._autosize_columns = kwargs.get('autosize_columns', self._autosize_columns)
        self._drag_column_move = kwargs.get('drag_column_move', self._drag_column_move)
        self._drag_column_size = kwargs.get('drag_column_size', self._drag_column_size)
        self._column_label_size = kwargs.get('column_label_size', self._column_label_size)
        self._column_labels = kwargs.get('column_labels', self._column_labels)
        self._column_label_horizontal_alignment = kwargs.get('column_label_horizontal_alignment',
                                                             self._column_label_horizontal_alignment)
        self._column_label_vertical_alignment = kwargs.get('column_label_vertical_alignment',
                                                           self._column_label_vertical_alignment)
        # rows properties
        self._row_sizes = kwargs.get('row_sizes', self._row_sizes)
        self._autosize_rows = kwargs.get('autosize_rows', self._autosize_rows)
        self._drag_row_move = kwargs.get('drag_row_move', self._drag_row_move)
        self._drag_row_size = kwargs.get('drag_row_size', self._drag_row_size)
        self._row_label_size = kwargs.get('row_label_size', self._row_label_size)
        self._row_labels = kwargs.get('row_labels', self._row_labels)
        self._row_label_horizontal_alignment = kwargs.get('row_label_horizontal_alignment',
                                                          self._row_label_horizontal_alignment)
        self._row_label_vertical_alignment = kwargs.get('row_label_vertical_alignment',
                                                        self._row_label_vertical_alignment)
        # label properties
        self._label_background_colour = kwargs.get('label_background_colour', self._label_background_colour)
        self._label_font = kwargs.get('label_font', self._label_font)
        self._label_text_colour = kwargs.get('label_text_colour', self._label_text_colour)
        # cell properties
        self._cell_background_colour = kwargs.get('cell_background_colour', self._cell_background_colour)
        self._cell_font = kwargs.get('cell_font', self._cell_font)
        self._cell_text_colour = kwargs.get('cell_text_colour', self._cell_text_colour)
        self._cell_horizontal_alignment = kwargs.get('cell_horizontal_alignment', self._cell_horizontal_alignment)
        self._cell_vertical_alignment = kwargs.get('cell_vertical_alignment', self._cell_vertical_alignment)
        # grid events
        self._on_grid_cell_changing = kwargs.get('on_grid_cell_changing', self._on_grid_cell_changing)
        self._on_grid_cell_changed = kwargs.get('on_grid_cell_changed', self._on_grid_cell_changed)
        self._on_grid_cell_left_click = kwargs.get('on_grid_cell_left_click', self._on_grid_cell_left_click)
        self._on_grid_cell_left_dclick = kwargs.get('on_grid_cell_left_dclick', self._on_grid_cell_left_dclick)
        self._on_grid_cell_right_click = kwargs.get('on_grid_cell_right_click', self._on_grid_cell_right_click)
        self._on_grid_cell_right_dclick = kwargs.get('on_grid_cell_right_dclick', self._on_grid_cell_right_dclick)
        self._on_grid_editor_hidden = kwargs.get('on_grid_editor_hidden', self._on_grid_editor_hidden)
        self._on_grid_editor_shown = kwargs.get('on_grid_editor_shown', self._on_grid_editor_shown)
        self._on_grid_label_left_click = kwargs.get('on_grid_label_left_click', self._on_grid_label_left_click)
        self._on_grid_label_left_dclick = kwargs.get('on_grid_label_left_dclick', self._on_grid_label_left_dclick)
        self._on_grid_label_right_click = kwargs.get('on_grid_label_right_click', self._on_grid_label_right_click)
        self._on_grid_label_right_dclick = kwargs.get('on_grid_label_right_dclick', self._on_grid_label_right_dclick)
        self._on_grid_select_cell = kwargs.get('on_grid_select_cell', self._on_grid_select_cell)
        self._on_grid_row_move = kwargs.get('on_grid_row_move', self._on_grid_row_move)
        self._on_grid_col_move = kwargs.get('on_grid_col_move', self._on_grid_col_move)
        self._on_grid_col_sort = kwargs.get('on_grid_col_sort', self._on_grid_col_sort)
        self._on_grid_tabbing = kwargs.get('on_grid_tabbing', self._on_grid_tabbing)
        # grid size events
        self._on_grid_cmd_col_size = kwargs.get('on_grid_cmd_col_size', self._on_grid_cmd_col_size)
        self._on_grid_cmd_row_size = kwargs.get('on_grid_cmd_row_size', self._on_grid_cmd_row_size)
        self._on_grid_row_auto_size = kwargs.get('on_grid_row_auto_size', self._on_grid_row_auto_size)
        self._on_grid_col_size = kwargs.get('on_grid_col_size', self._on_grid_col_size)
        self._on_grid_col_auto_size = kwargs.get('on_grid_col_auto_size', self._on_grid_col_auto_size)
        self._on_grid_row_size = kwargs.get('on_grid_row_size', self._on_grid_row_size)
        # grid range events
        self._on_grid_range_selecting = kwargs.get('on_grid_range_selecting', self._on_grid_range_selecting)
        self._on_grid_cmd_range_selecting = kwargs.get('on_grid_cmd_range_selecting', self._on_grid_cmd_range_selecting)
        self._on_grid_range_selected = kwargs.get('on_grid_range_selected', self._on_grid_range_selected)
        self._on_grid_cmd_range_selected = kwargs.get('on_grid_cmd_range_selected', self._on_grid_cmd_range_selected)
        # grid editor events
        self._on_grid_editor_created = kwargs.get('on_grid_editor_created', self._on_grid_editor_created)
        self._on_grid_cmd_editor_created = kwargs.get('on_grid_cmd_editor_created', self._on_grid_cmd_editor_created)

    @property
    def local_grid_events(self):
        return {
            'on_grid_cell_changing': (self._on_grid_cell_changing, 'wx.EVT_GRID_CELL_CHANGING',
                                      'wxEVT_GRID_CELL_CHANGING'),
            'on_grid_cell_changed': (self._on_grid_cell_changed, 'wx.EVT_GRID_CELL_CHANGED',
                                     'wxEVT_GRID_CELL_CHANGED'),
            'on_grid_cell_left_click': (self._on_grid_cell_left_click, 'wx.EVT_GRID_CELL_LEFT_CLICK',
                                        'wxEVT_GRID_CELL_LEFT_CLICK'),
            'on_grid_cell_left_dclick': (self._on_grid_cell_left_dclick, 'wx.EVT_GRID_CELL_LEFT_DCLICK',
                                         'wxEVT_GRID_CELL_LEFT_DCLICK'),
            'on_grid_cell_right_click': (self._on_grid_cell_right_click, 'wx.EVT_GRID_CELL_RIGHT_CLICK',
                                         'wxEVT_GRID_CELL_RIGHT_CLICK'),
            'on_grid_cell_right_dclick': (self._on_grid_cell_right_dclick, 'wx.EVT_GRID_CELL_RIGHT_DCLICK',
                                          'wxEVT_GRID_CELL_RIGHT_DCLICK'),
            'on_grid_editor_hidden': (self._on_grid_editor_hidden, 'wx.EVT_GRID_EDITOR_HIDDEN',
                                      'wxEVT_GRID_EDITOR_HIDDEN'),
            'on_grid_editor_shown': (self._on_grid_editor_shown, 'wx.EVT_GRID_EDITOR_SHOWN', 'wxEVT_GRID_EDITOR_SHOWN'),
            'on_grid_label_left_click': (self._on_grid_label_left_click, 'wx.EVT_GRID_LABEL_LEFT_CLICK',
                                         'wxEVT_GRID_LABEL_LEFT_CLICK'),
            'on_grid_label_left_dclick': (self._on_grid_label_left_dclick, 'wx.EVT_GRID_LABEL_LEFT_DCLICK',
                                          'wxEVT_GRID_LABEL_LEFT_DCLICK'),
            'on_grid_label_right_click': (self._on_grid_label_right_click, 'wx.EVT_GRID_LABEL_RIGHT_CLICK',
                                          'wxEVT_GRID_LABEL_RIGHT_CLICK'),
            'on_grid_label_right_dclick': (self._on_grid_label_right_dclick, 'wx.EVT_GRID_LABEL_RIGHT_DCLICK',
                                           'wxEVT_GRID_LABEL_RIGHT_DCLICK'),
            'on_grid_select_cell': (self._on_grid_select_cell, 'wx.EVT_GRID_SELECT_CELL', 'wxEVT_GRID_SELECT_CELL'),
            'on_grid_row_move': (self._on_grid_row_move, 'wx.EVT_GRID_ROW_MOVE', 'wxEVT_GRID_ROW_MOVE'),
            'on_grid_col_move': (self._on_grid_col_move, 'wx.EVT_GRID_COL_MOVE', 'wxEVT_GRID_COL_MOVE'),
            'on_grid_col_sort': (self._on_grid_col_sort, 'wx.EVT_GRID_COL_SORT', 'wxEVT_GRID_COL_SORT'),
            'on_grid_tabbing': (self._on_grid_tabbing, 'wx.EVT_GRID_TABBING', 'wxEVT_GRID_TABBING'),
            'on_grid_cmd_col_size': (self._on_grid_cmd_col_size, 'wx.EVT_GRID_COL_SIZE', 'wxEVT_GRID_COL_SIZE'),
            'on_grid_cmd_row_size': (self._on_grid_cmd_row_size, 'wx.EVT_GRID_ROW_SIZE', 'wxEVT_GRID_ROW_SIZE'),
            'on_grid_row_auto_size': (self._on_grid_row_auto_size, 'wx.EVT_GRID_ROW_AUTO_SIZE',
                                      'wxEVT_GRID_ROW_AUTO_SIZE'),
            'on_grid_col_size': (self._on_grid_col_size, 'wx.EVT_GRID_COL_SIZE', 'wxEVT_GRID_COL_SIZE'),
            'on_grid_col_auto_size': (self._on_grid_col_auto_size, 'wx.EVT_GRID_COL_AUTO_SIZE',
                                      'wxEVT_GRID_COL_AUTO_SIZE'),
            'on_grid_row_size': (self._on_grid_row_size, 'wx.EVT_GRID_ROW_SIZE', 'wxEVT_GRID_ROW_SIZE'),
            'on_grid_range_selecting': (self._on_grid_range_selecting, 'wx.EVT_GRID_RANGE_SELECTING',
                                        'wxEVT_GRID_RANGE_SELECTING'),
            'on_grid_cmd_range_selecting': (self._on_grid_cmd_range_selecting, 'wx.EVT_GRID_RANGE_SELECTING',
                                            'wxEVT_GRID_RANGE_SELECTING'),
            'on_grid_range_selected': (self._on_grid_range_selected, 'wx.EVT_GRID_RANGE_SELECTED',
                                       'wxEVT_GRID_RANGE_SELECTED'),
            'on_grid_cmd_range_selected': (self._on_grid_cmd_range_selected, 'wx.EVT_GRID_RANGE_SELECTED',
                                           'wxEVT_GRID_RANGE_SELECTED'),
            'on_grid_editor_created': (self._on_grid_editor_created, 'wx.EVT_GRID_EDITOR_CREATED',
                                       'wxEVT_GRID_EDITOR_CREATED'),
            'on_grid_cmd_editor_created': (self._on_grid_cmd_editor_created, 'wx.EVT_GRID_EDITOR_CREATED',
                                           'wxEVT_GRID_EDITOR_CREATED'),
        }

    @property
    def events(self):
        value = self.local_grid_events
        value.update(super(GridControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_gridcontrol_kwargs
        value.update(super(GridControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_gridcontrol_kwargs = kwargs
        super(GridControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('grid_control')

    @property
    def style_attribute_options(self):
        return []

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Grid',
            'type': 'category',
            'help': 'Attributes of grid.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The grid control name."
                },
                {
                    'label': 'General',
                    'type': 'category',
                    'help': 'General grid properties.',
                    'child': [
                        {
                            'label': 'rows',
                            'type': 'integer',
                            'read_only': False,
                            'value': self._rows,
                            'name': 'rows',
                            'help': 'The number of grid rows.'
                        },
                        {
                            'label': 'columns',
                            'type': 'integer',
                            'read_only': False,
                            'value': self._columns,
                            'name': 'columns',
                            'help': 'The number of grid columns'
                        },
                        {
                            'label': 'editing',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._editing,
                            'name': 'editing',
                            'help': 'editing'
                        },
                        {
                            'label': 'grid_lines',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._grid_lines,
                            'name': 'grid_lines',
                            'help': 'grid_lines'
                        },
                        {
                            'label': 'grid_line_color',
                            'type': 'colour',
                            'read_only': False,
                            'value': self._grid_line_color,
                            'name': 'grid_line_color',
                            'help': 'grid_line_color'
                        },
                        {
                            'label': 'drag_grid_size',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._drag_grid_size,
                            'name': 'drag_grid_size',
                            'help': 'Allow drag grid lines'
                        },
                        {
                            'label': 'margin_width',
                            'type': 'integer',
                            'read_only': False,
                            'value': self._margin_width,
                            'name': 'margin_width',
                            'help': 'extra horizontal space occupied by the control outside the grid.'
                        },
                        {
                            'label': 'margin_height',
                            'type': 'integer',
                            'read_only': False,
                            'value': self._margin_height,
                            'name': 'margin_height',
                            'help': 'extra vertical space occupied by the control outside the grid.'
                        },
                    ]
                },
                {
                    'label': 'Columns',
                    'type': 'category',
                    'help': 'Columns properties.',
                    'child': [
                        {
                            'label': 'column_sizes',
                            'type': 'string_array',
                            'read_only': False,
                            'value': self._column_sizes,
                            'name': 'column_sizes',
                            'help': 'Comma delimited list of column widths.'
                        },
                        {
                            'label': 'autosize_columns',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._autosize_columns,
                            'name': 'autosize_columns',
                            'help': 'If set, the column sizes will be computed automatically based on their contents.'
                        },
                        {
                            'label': 'drag_column_move',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._drag_column_move,
                            'name': 'drag_column_move',
                            'help': 'Allow reordering columns by dragging.'
                        },
                        {
                            'label': 'drag_column_size',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._drag_column_size,
                            'name': 'drag_column_size',
                            'help': 'Allow resize columns by dragging.'
                        },
                        {
                            'label': 'column_label_size',
                            'type': 'integer',
                            'read_only': False,
                            'value': self._column_label_size,
                            'name': 'column_label_size',
                            'help': 'Height of column labels.'
                        },
                        {
                            'label': 'column_labels',
                            'type': 'string_array',
                            'read_only': False,
                            'value': self._column_labels,
                            'name': 'column_labels',
                            'help': 'List of column labels'
                        },
                        {
                            'label': 'column_label_horizontal_alignment',
                            'type': 'choice',
                            'read_only': False,
                            'value': self._column_label_horizontal_alignment,
                            'name': 'column_label_horizontal_alignment',
                            'choices': ['wxALIGN_LEFT', 'wxALIGN_CENTRE', 'wxALIGN_RIGHT'],
                            'help': 'Horizontal alignment of column label.'
                        },
                        {
                            'label': 'column_label_vertical_alignment',
                            'type': 'choice',
                            'read_only': False,
                            'value': self._column_label_vertical_alignment,
                            'name': 'column_label_vertical_alignment',
                            'choices': ['wxALIGN_TOP', 'wxALIGN_CENTRE', 'wxALIGN_BOTTOM'],
                            'help': 'Horizontal alignment of column label.'
                        },

                    ]
                },
                {
                    'label': 'Rows',
                    'type': 'category',
                    'help': 'Rows properties.',
                    'child': [
                        {
                            'label': 'row_sizes',
                            'type': 'string_array',
                            'read_only': False,
                            'value': self._row_sizes,
                            'name': 'row_sizes',
                            'help': 'Comma delimited list of row heights.'
                        },
                        {
                            'label': 'autosize_rows',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._autosize_rows,
                            'name': 'autosize_rows',
                            'help': 'If enabled, row sizes are computed automatically to fit their content.'
                        },
                        # not yet available?
                        # {
                        #     'label': 'drag_row_move',
                        #     'type': 'boolean',
                        #     'read_only': False,
                        #     'value': self._drag_row_move,
                        #     'name': 'drag_column_move',
                        #     'help': 'Allow reordering rows by dragging.'
                        # },
                        {
                            'label': 'drag_row_size',
                            'type': 'boolean',
                            'read_only': False,
                            'value': self._drag_row_size,
                            'name': 'drag_row_size',
                            'help': 'Allow resize rows by dragging.'
                        },
                        {
                            'label': 'row_label_size',
                            'type': 'integer',
                            'read_only': False,
                            'value': self._row_label_size,
                            'name': 'row_label_size',
                            'help': 'Width of row labels.'
                        },
                        {
                            'label': 'row_labels',
                            'type': 'string_array',
                            'read_only': False,
                            'value': self._row_labels,
                            'name': 'row_labels',
                            'help': 'String list of row labels'
                        },
                        {
                            'label': 'row_label_horizontal_alignment',
                            'type': 'choice',
                            'read_only': False,
                            'value': self._row_label_horizontal_alignment,
                            'name': 'row_label_horizontal_alignment',
                            'choices': ['wxALIGN_LEFT', 'wxALIGN_CENTRE', 'wxALIGN_RIGHT'],
                            'help': 'Horizontal alignment of row labels.'
                        },
                        {
                            'label': 'row_label_vertical_alignment',
                            'type': 'choice',
                            'read_only': False,
                            'value': self._row_label_vertical_alignment,
                            'name': 'row_label_vertical_alignment',
                            'choices': ['wxALIGN_TOP', 'wxALIGN_CENTRE', 'wxALIGN_BOTTOM'],
                            'help': 'Vertical alignment of row labels.'
                        },
                    ]
                },
                {
                    'label': 'Labels',
                    'type': 'category',
                    'help':  'Labels appearance properties',
                    'child': [
                        {
                            'label': 'label_font',
                            'type': 'font',
                            'read_only': False,
                            'value': self._label_font,
                            'name': 'label_font',
                            'help': 'The label used font.'
                        },
                        {
                            'label': 'label_text_colour',
                            'type': 'colour',
                            'read_only': False,
                            'value': self._label_text_colour,
                            'name': 'label_text_colour',
                            'help': 'The label text colour.'
                        },
                        {
                            'label': 'label_background_colour',
                            'type': 'colour',
                            'read_only': False,
                            'value': self._label_background_colour,
                            'name': 'label_background_colour',
                            'help': 'Backgroud label colour.'
                        },
                    ]
                },
                {
                    'label': 'Cells',
                    'type': 'category',
                    'help': 'Cell appearance properties',
                    'child': [
                        {
                            'label': 'cell_font',
                            'type': 'string',
                            'read_only': False,
                            'value': self._cell_font,
                            'name': 'cell_font',
                            'help': 'Cell text used font.'
                        },
                        {
                            'label': 'cell_text_colour',
                            'type': 'colour',
                            'read_only': False,
                            'value': self._cell_text_colour,
                            'name': 'cell_text_colour',
                            'help': 'Cell text colour.'
                        },
                        {
                            'label': 'cell_background_colour',
                            'type': 'colour',
                            'read_only': False,
                            'value': self._cell_background_colour,
                            'name': 'cell_background_colour',
                            'help': 'Cell background colour.'
                        },
                        {
                            'label': 'cell_horizontal_alignment',
                            'type': 'choice',
                            'read_only': False,
                            'value': self._cell_horizontal_alignment,
                            'name': 'cell_horizontal_alignment',
                            'choices': ['wxALIGN_LEFT', 'wxALIGN_CENTRE', 'wxALIGN_RIGHT'],
                            'help': 'Cells content horizontal alignment.'
                        },
                        {
                            'label': 'cell_vertical_alignment',
                            'type': 'choice',
                            'read_only': False,
                            'value': self._cell_vertical_alignment,
                            'name': 'cell_vertical_alignment',
                            'choices': ['wxALIGN_TOP', 'wxALIGN_CENTRE', 'wxALIGN_BOTTOM'],
                            'help': 'Cells content vertical alignment.'
                        },

                    ]
                }
            ]
        }]
        _event = [{
            'label': 'Grid',
            'type': 'category',
            'help': 'Events of grid',
            'child': [
                {
                    'label': 'Grid events',
                    'type': 'category',
                    'help': "General grid content events ",
                    'child': [
                        {
                            'label': 'on_grid_cell_changing',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cell_changing,
                            'name': 'on_grid_cell_changing',
                            'help': "The user is about to change the data in a cell. The new cell value as string is "
                                    "available from GetString event object method. This event can be vetoed if the "
                                    "change is not allowed. Processes a wxEVT_GRID_CELL_CHANGING event type.",
                        },
                        {
                            'label': 'on_grid_cell_changed',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cell_changed,
                            'name': 'on_grid_cell_changed',
                            'help': "The user changed the data in a cell. The old cell value as string is available "
                                    "from GetString event object method. Notice that vetoing this event still works "
                                    "for backwards compatibility reasons but any new code should only veto "
                                    "EVT_GRID_CELL_CHANGING event and not this one. "
                                    "Processes a wxEVT_GRID_CELL_CHANGED event type.",
                        },
                        {
                            'label': 'on_grid_cell_left_click',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cell_left_click,
                            'name': 'on_grid_cell_left_click',
                            'help': "The user clicked a cell with the left mouse button. Processes a "
                                    "wxEVT_GRID_CELL_LEFT_CLICK event type.",
                        },
                        {
                            'label': 'on_grid_cell_left_dclick',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cell_left_dclick,
                            'name': 'on_grid_cell_left_dclick',
                            'help': "The user double-clicked a cell with the left mouse button. "
                                    "Processes a wxEVT_GRID_CELL_LEFT_DCLICK event type.",
                        },
                        {
                            'label': 'on_grid_cell_right_click',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cell_right_click,
                            'name': 'on_grid_cell_right_click',
                            'help': "The user clicked a cell with the right mouse button. "
                                    "Processes a wxEVT_GRID_CELL_RIGHT_CLICK event type.",
                        },
                        {
                            'label': 'on_grid_cell_right_dclick',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cell_right_dclick,
                            'name': 'on_grid_cell_right_dclick',
                            'help': "The user double-clicked a cell with the right mouse button. "
                                    "Processes a wxEVT_GRID_CELL_RIGHT_DCLICK event type.",
                        },
                        {
                            'label': 'on_grid_editor_hidden',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_editor_hidden,
                            'name': 'on_grid_editor_hidden',
                            'help': "The editor for a cell was hidden. "
                                    "Processes a wxEVT_GRID_EDITOR_HIDDEN event type.",
                        },
                        {
                            'label': 'on_grid_editor_shown',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_editor_shown,
                            'name': 'on_grid_editor_shown',
                            'help': "The editor for a cell was shown. "
                                    "Processes a wxEVT_GRID_EDITOR_SHOWN event type.",
                        },
                        {
                            'label': 'on_grid_label_left_click',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_label_left_click,
                            'name': 'on_grid_label_left_click',
                            'help': "The user clicked a label with the left mouse button. "
                                    "Processes a wxEVT_GRID_LABEL_LEFT_CLICK event type.",
                        },
                        {
                            'label': 'on_grid_label_left_dclick',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_label_left_dclick,
                            'name': 'on_grid_label_left_dclick',
                            'help': "The user double-clicked a label with the left mouse button. "
                                    "Processes a wxEVT_GRID_LABEL_LEFT_DCLICK event type.",
                        },
                        {
                            'label': 'on_grid_label_right_click',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_label_right_click,
                            'name': 'on_grid_label_right_click',
                            'help': "The user clicked a label with the right mouse button. "
                                    "Processes a wxEVT_GRID_LABEL_RIGHT_CLICK event type.",
                        },
                        {
                            'label': 'on_grid_label_right_dclick',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_label_right_dclick,
                            'name': 'on_grid_label_right_dclick',
                            'help': "The user double-clicked a label with the right mouse button. "
                                    "Processes a wxEVT_GRID_LABEL_RIGHT_DCLICK event type.",
                        },
                        {
                            'label': 'on_grid_select_cell',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_select_cell,
                            'name': 'on_grid_select_cell',
                            'help': "The given cell was made current, either by user or by the program via a call "
                                    "to SetGridCursor() or GoToCell(). Processes a wxEVT_GRID_SELECT_CELL event type.",
                        },
                        {
                            'label': 'on_grid_row_move',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_row_move,
                            'name': 'on_grid_row_move',
                            'help': "The user tries to change the order of the rows in the grid by dragging the row "
                                    "specified by GetRow. This event can be vetoed to either prevent the user from "
                                    "reordering the row change completely (but notice that if you don't want to "
                                    "allow it at all, you simply shouldn't call wx.grid.Grid.EnableDragRowMove in "
                                    "the first place), vetoed but handled in some way in the handler, e.g. by really "
                                    "moving the row to the new position at the associated table level, or allowed to "
                                    "proceed in which case wx.grid.Grid.SetRowPos is used to reorder the rows display "
                                    "order without affecting the use of the row indices otherwise. This event macro "
                                    "corresponds to wxEVT_GRID_ROW_MOVE event type. It is only available "
                                    "since wxWidgets 3.1.7.",
                        },
                        {
                            'label': 'on_grid_col_move',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_col_move,
                            'name': 'on_grid_col_move',
                            'help': "The user tries to change the order of the columns in the grid by dragging the "
                                    "column specified by GetCol. This event can be vetoed to either prevent the user "
                                    "from reordering the column change completely (but notice that if you don't want "
                                    "to allow it at all, you simply shouldn't call wx.grid.Grid.EnableDragColMove in "
                                    "the first place), vetoed but handled in some way in the handler, e.g. by really "
                                    "moving the column to the new position at the associated table level, or allowed "
                                    "to proceed in which case wx.grid.Grid.SetColPos is used to reorder the columns "
                                    "display order without affecting the use of the column indices otherwise. "
                                    "This event macro corresponds to wxEVT_GRID_COL_MOVE event type.",
                        },
                        {
                            'label': 'on_grid_col_sort',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_col_sort,
                            'name': 'on_grid_col_sort',
                            'help': "This event is generated when a column is clicked by the user and its name is "
                                    "explained by the fact that the custom reaction to a click on a column is to sort "
                                    "the grid contents by this column. However the grid itself has no special support "
                                    "for sorting and it's up to the handler of this event to update the associated "
                                    "table. But if the event is handled (and not vetoed) the grid supposes that the "
                                    "table was indeed resorted and updates the column to indicate the new sort order "
                                    "and refreshes itself. "
                                    "This event macro corresponds to wxEVT_GRID_COL_SORT event type.",
                        },
                        {
                            'label': 'on_grid_tabbing',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_tabbing,
                            'name': 'on_grid_tabbing',
                            'help': "This event is generated when the user presses TAB or Shift-TAB in the grid. "
                                    "It can be used to customize the simple default TAB handling logic, e.g. to go to "
                                    "the next non-empty cell instead of just the next cell. "
                                    "See also wx.grid.Grid.SetTabBehaviour . This event is new since wxWidgets 2.9.5.",
                        },
                    ]
                },
                {
                    'label': 'Grid size events',
                    'type': 'category',
                    'help': "Events generated from changes on sizes ",
                    'child': [
                        {
                            'label': 'on_evt_grid_cmd_col_size',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cmd_col_size,
                            'name': 'on_evt_grid_cmd_col_size',
                            'help': "The user resized a column, corresponds to wxEVT_GRID_COL_SIZE event type."
                        },
                        {
                            'label': 'on_evt_grid_cmd_row_size',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cmd_row_size,
                            'name': 'on_evt_grid_cmd_row_size',
                            'help': "The user resized a row, corresponds to wxEVT_GRID_ROW_SIZE event type."
                        },
                        {
                            'label': 'on_grid_row_auto_size',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_row_auto_size,
                            'name': 'on_grid_row_auto_size',
                            'help': "This event is sent when a row must be resized to its best size, e.g. when the "
                                    "user double clicks the row divider. The default implementation simply resizes "
                                    "the row to fit the row label (but not its contents as this could be too slow for "
                                    "big grids). This macro corresponds to wxEVT_GRID_ROW_AUTO_SIZE event type and is "
                                    "new since wxWidgets 3.1.7."
                        },
                        {
                            'label': 'on_grid_col_size',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_col_size,
                            'name': 'on_grid_col_size',
                            'help': "Same as EVT_GRID_CMD_COL_SIZE() but uses ID_ANY id."
                        },
                        {
                            'label': 'on_grid_col_auto_size',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_col_auto_size,
                            'name': 'on_grid_col_auto_size',
                            'help': "This event is sent when a column must be resized to its best size, e.g. when the "
                                    "user double clicks the column divider. The default implementation simply resizes "
                                    "the column to fit the column label (but not its contents as this could be too "
                                    "slow for big grids). This macro corresponds to wxEVT_GRID_COL_AUTO_SIZE event "
                                    "type and is new since wxWidgets 2.9.5."
                        },
                        {
                            'label': 'on_grid_row_size',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_row_size,
                            'name': 'on_grid_row_size',
                            'help': "Same as EVT_GRID_CMD_ROW_SIZE() but uses ID_ANY id. "
                        },
                    ]
                },
                {
                    'label': 'Grid range events',
                    'type': 'category',
                    'help': "Events generated from changes on cell ranges",
                    'child': [
                        {
                            'label': 'on_grid_range_selecting',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_range_selecting,
                            'help': "The user is selecting a group of contiguous cells. Processes a "
                                    "wxEVT_GRID_RANGE_SELECTING event type. This event is available in wxWidgets 3.1.5 "
                                    "and later only.",
                        },
                        {
                            'label': 'on_grid_cmd_range_selecting',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cmd_range_selecting,
                            'help': "The user is selecting a group of contiguous cells; variant taking a window "
                                    "identifier. Processes a wxEVT_GRID_RANGE_SELECTING event type. This event is "
                                    "available in wxWidgets 3.1.5 and later only.",
                        },
                        {
                            'label': 'on_grid_range_selected',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_range_selected,
                            'help': "The user selected a group of contiguous cells. Processes a "
                                    "wxEVT_GRID_RANGE_SELECTED event type. This event is available in wxWidgets 3.1.5 "
                                    "and later only and was called wxEVT_GRID_RANGE_SELECT in the previous versions.",
                        },
                        {
                            'label': 'on_grid_cmd_range_selected',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cmd_range_selected,
                            'help': "The user selected a group of contiguous cells; variant taking a window "
                                    "identifier. Processes a wxEVT_GRID_RANGE_SELECTED event type. This event is "
                                    "available in wxWidgets 3.1.5 and later only and was called "
                                    "wxEVT_GRID_CMD_RANGE_SELECT in the previous versions.",
                        },
                    ]
                },
                {
                    'label': 'Grid editor events',
                    'type': 'category',
                    'help': "Events generated when cell editors are created",
                    'child': [
                        {
                            'label': 'on_grid_editor_created',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_editor_created,
                            'help': "The editor for a cell was created. "
                                    "Processes a wxEVT_GRID_EDITOR_CREATED event type.",
                        },
                        {
                            'label': 'on_grid_cmd_editor_created',
                            'type': 'string',
                            'read_only': False,
                            'value': self._on_grid_cmd_editor_created,
                            'help': "The editor for a cell was created; variant taking a window identifier. "
                                    "Processes a wxEVT_GRID_EDITOR_CREATED event type.",
                        },
                    ]
                }
            ],
        }]

        _base_property, _base_event = super(GridControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_sample_content(self):
        pass

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            grid = self._edition_window = uiMockupControl(wxg.Grid)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)

            horz_map = {'wxALIGN_LEFT': wx.ALIGN_LEFT, 'wxALIGN_CENTRE': wx.ALIGN_CENTRE,
                        'wxALIGN_RIGHT': wx.ALIGN_RIGHT}
            vert_map = {'wxALIGN_TOP': wx.ALIGN_TOP, 'wxALIGN_CENTRE': wx.ALIGN_CENTRE,
                        'wxALIGN_BOTTOM': wx.ALIGN_BOTTOM}
            # Grid
            grid.CreateGrid(self._rows, self._columns)
            grid.EnableEditing(self._editing)
            grid.EnableGridLines(self._grid_lines)
            grid.SetGridLineColour(self.get_colour_helper(self._grid_line_color))
            grid.EnableDragGridSize(self._drag_grid_size)
            grid.SetMargins(self._margin_width, self._margin_height)

            # Label Appearance
            grid.SetLabelBackgroundColour(self.get_colour_helper(self._label_background_colour))
            grid.SetLabelTextColour(self.get_colour_helper(self._label_text_colour))
            # grid.SetLabelFont(wx.Font(self._label_font))

            # Cell Defaults
            # grid.SetDefaultCellFont(wx.Font(self._cell_font))
            grid.SetDefaultCellBackgroundColour(self.get_colour_helper(self._cell_background_colour))
            grid.SetDefaultCellTextColour(self.get_colour_helper(self._cell_text_colour))
            grid.SetDefaultCellAlignment(
                horz_map[self._cell_horizontal_alignment],
                vert_map[self._cell_vertical_alignment]
            )

            # Columns
            grid.EnableDragColMove(self._drag_column_move)
            grid.EnableDragColSize(self._drag_column_size)
            grid.SetColLabelSize(self._column_label_size)
            grid.SetColLabelAlignment(
                horz_map[self._column_label_horizontal_alignment],
                vert_map[self._column_label_vertical_alignment]
            )
            for index, value in enumerate(self._column_labels):
                if index < self._columns:
                    grid.SetColLabelValue(index, value)
            for index, value in enumerate(self._column_sizes):
                if index < self._columns:
                    grid.SetColSize(index, int(value))
            if self._autosize_columns:
                grid.AutoSizeColumns()

            # Rows
            grid.EnableDragRowSize(self._drag_row_size)
            grid.SetRowLabelSize(self._row_label_size)
            grid.SetRowLabelAlignment(
                horz_map[self._row_label_horizontal_alignment],
                vert_map[self._row_label_vertical_alignment]
            )
            for index, value in enumerate(self._row_labels):
                if index < self._rows:
                    grid.SetRowLabelValue(index, value)
            for index, value in enumerate(self._row_sizes):
                if index < self._rows:
                    grid.SetRowSize(index, int(value))
            if self._autosize_rows:
                grid.AutoSizeRows()
            self.add_to_sizer(container, self._edition_window)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK | wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self.create_sample_content()
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/grid.h>
        cls = register_cpp_class(self.project, 'wxGrid')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="grid control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/grid.h>')
        window_parent = self.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = "{name} = new wxGrid({owner}, {id_}, {pos}, {size}, {style});\n" .format(
            name=self._name, owner=owner, id_=self._id, pos=self.cc_pos_str,
            size=self.cc_size_str, style=self.cc_style_str)
        code += "{name}->CreateGrid({r}, {c});\n".format(name=self.name, r=self._rows, c=self._columns)
        code += "{name}->EnableEditing({f});\n".format(name=self.name, f=(self._editing and 'true') or 'false')
        code += "{name}->EnableGridLines({f});\n".format(name=self.name, f=(self._grid_lines and 'true') or 'false')
        code += "{name}->EnableDragGridSize({f});\n".format(
            name=self.name, f=(self._drag_grid_size and 'true') or 'false')
        code += "{name}->SetMargins({w}, {h});\n".format(
            name=self.name, w=self._margin_width, h=self._margin_height)

        for index, value in enumerate(self._column_sizes):
            if index < self._columns:
                code += "{name}->SetColSize({i}, {w});\n".format(name=self.name, i=index, w=value)

        code += "{name}->EnableDragColMove({f});\n".format(
            name=self.name, f=(self._drag_column_move and 'true') or 'false')
        code += "{name}->EnableDragColSize({f});\n".format(
            name=self.name, f=(self._drag_column_size and 'true') or 'false')
        code += "{name}->SetColLabelSize({s});\n".format(
            name=self.name, s=self._column_label_size)
        code += "{name}->SetColLabelAlignment({h}, {v});\n".format(
            name=self.name, h=self._column_label_horizontal_alignment, v=self._column_label_vertical_alignment)

        code += "{name}->EnableDragRowSize({f});\n".format(
            name=self.name, f=(self._drag_row_size and 'true') or 'false')
        code += "{name}->SetRowLabelSize({s});\n".format(
            name=self.name, s=self._row_label_size)
        code += "{name}->SetRowLabelAlignment({h}, {v});\n".format(
            name=self.name, h=self._row_label_horizontal_alignment, v=self._row_label_vertical_alignment)

        code += "{name}->SetDefaultCellAlignment({h}, {v});\n".format(
            name=self.name, h=self._cell_horizontal_alignment, v=self._cell_vertical_alignment)

        code += super(GridControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.grid')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        horz_map = {'wxALIGN_LEFT': 'wx.ALIGN_LEFT', 'wxALIGN_CENTRE': 'wx.ALIGN_CENTRE',
                    'wxALIGN_RIGHT': 'wx.ALIGN_RIGHT'}
        vert_map = {'wxALIGN_TOP': 'wx.ALIGN_TOP', 'wxALIGN_CENTRE': 'wx.ALIGN_CENTRE',
                    'wxALIGN_BOTTOM': 'wx.ALIGN_BOTTOM'}

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = 'self.{name}'.format(name=window_parent.name)

        code = '{this} = wx.grid.Grid({owner}, {id_}, {pos}, {size}, {style})\n'.\
            format(this=this, owner=owner, id_=self.py_id_str, pos=self.py_pos_str, size=self.py_size_str,
                   style=self.py_style_str)
        code += "{this}.CreateGrid({r}, {c})\n".format(this=this, r=self._rows, c=self._columns)
        code += "{this}.EnableEditing({f})\n".format(this=this, f=self._editing)
        code += "{this}.EnableGridLines({f})\n".format(this=this, f=self._grid_lines)
        code += "{this}.EnableDragGridSize({f})\n".format(this=this, f=self._drag_grid_size)
        code += "{this}.SetMargins({w}, {h})\n".format(
            this=this, w=self._margin_width, h=self._margin_height)

        for index, value in enumerate(self._column_sizes):
            if index < self._columns:
                code += "{this}.SetColSize({i}, {w})\n".format( this=this, i=index, w=value)

        code += "{this}.EnableDragColMove({f})\n".format(this=this, f=self._drag_column_move)
        code += "{this}.EnableDragColSize({f})\n".format(this=this, f=self._drag_column_size)
        code += "{this}.SetColLabelSize({s})\n".format(this=this, s=self._column_label_size)
        code += "{this}.SetColLabelAlignment({h}, {v})\n".format(
            this=this,
            h=horz_map[self._column_label_horizontal_alignment],
            v=vert_map[self._column_label_vertical_alignment])

        code += "{this}.EnableDragRowSize({f})\n".format(this=this, f=self._drag_row_size)
        code += "{this}.SetRowLabelSize({s})\n".format(this=this, s=self._row_label_size)
        code += "{this}.SetRowLabelAlignment({h}, {v})\n".format(
            this=this,
            h=horz_map[self._row_label_horizontal_alignment],
            v=vert_map[self._row_label_vertical_alignment])

        code += "{this}.SetDefaultCellAlignment({h}, {v})\n".format(
            this=this,
            h=horz_map[self._cell_horizontal_alignment],
            v=vert_map[self._cell_vertical_alignment])

        for index, value in enumerate(self._column_labels):
            if index < self._columns:
                code += '{this}.SetColLabelValue({i}, "{v}")\n'.format(
                    this=this, i=index, v=value)

        code += super(GridControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

