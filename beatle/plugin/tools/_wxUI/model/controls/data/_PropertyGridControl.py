import wx
import wx.propgrid as wpg
from .._WindowControl import WindowControl
from beatle.model import cc
from ....lib import register_cpp_class, with_style, with_extra_style


@with_style
@with_extra_style
class PropertyGridControl(WindowControl):
    """"This class represents a property grid"""

    style_list = (wpg.PG_DEFAULT_STYLE, wpg.PG_AUTO_SORT, wpg.PG_HIDE_CATEGORIES, wpg.PG_ALPHABETIC_MODE,
                  wpg.PG_BOLD_MODIFIED, wpg.PG_SPLITTER_AUTO_CENTER, wpg.PG_TOOLTIPS, wpg.PG_HIDE_MARGIN,
                  wpg.PG_STATIC_SPLITTER, wpg.PG_STATIC_LAYOUT, wpg.PG_LIMITED_EDITING, wpg.PG_TOOLBAR,
                  wpg.PG_DESCRIPTION, wpg.PG_NO_INTERNAL_BORDER)

    style_list_py_text = (
        'wx.propgrid.PG_DEFAULT_STYLE', 'wx.propgrid.PG_AUTO_SORT', 'wx.propgrid.PG_HIDE_CATEGORIES',
        'wx.propgrid.PG_ALPHABETIC_MODE', 'wx.propgrid.PG_BOLD_MODIFIED', 'wx.propgrid.PG_SPLITTER_AUTO_CENTER',
        'wx.propgrid.PG_TOOLTIPS', 'wx.propgrid.PG_HIDE_MARGIN', 'wx.propgrid.PG_STATIC_SPLITTER',
        'wx.propgrid.PG_STATIC_LAYOUT', 'wx.propgrid.PG_LIMITED_EDITING', 'wx.propgrid.PG_TOOLBAR',
        'wx.propgrid.PG_DESCRIPTION', 'wx.propgrid.PG_NO_INTERNAL_BORDER',
    )

    extra_style_list = (
        wpg.PG_EX_INIT_NOCAT, wpg.PG_EX_NO_FLAT_TOOLBAR, wpg.PG_EX_MODE_BUTTONS, wpg.PG_EX_HELP_AS_TOOLTIPS,
        wpg.PG_EX_NATIVE_DOUBLE_BUFFERING, wpg.PG_EX_AUTO_UNSPECIFIED_VALUES, wpg.PG_EX_WRITEONLY_BUILTIN_ATTRIBUTES,
        wpg.PG_EX_HIDE_PAGE_BUTTONS, wpg.PG_EX_MULTIPLE_SELECTION, wpg.PG_EX_ENABLE_TLP_TRACKING,
        wpg.PG_EX_NO_TOOLBAR_DIVIDER, wpg.PG_EX_TOOLBAR_SEPARATOR,
    )

    extra_style_list_py_text = (
        'wx.propgrid.PG_EX_INIT_NOCAT', 'wx.propgrid.PG_EX_NO_FLAT_TOOLBAR', 'wx.propgrid.PG_EX_MODE_BUTTONS',
        'wx.propgrid.PG_EX_HELP_AS_TOOLTIPS', 'wx.propgrid.PG_EX_NATIVE_DOUBLE_BUFFERING',
        'wx.propgrid.PG_EX_AUTO_UNSPECIFIED_VALUES', 'wx.propgrid.PG_EX_WRITEONLY_BUILTIN_ATTRIBUTES',
        'wx.propgrid.PG_EX_HIDE_PAGE_BUTTONS', 'wx.propgrid.PG_EX_MULTIPLE_SELECTION',
        'wx.propgrid.PG_EX_ENABLE_TLP_TRACKING', 'wx.propgrid.PG_EX_NO_TOOLBAR_DIVIDER',
        'wx.propgrid.PG_EX_TOOLBAR_SEPARATOR',
    )

    def __init__(self, **kwargs):
        # properties
        self._bitmap = (0, '')
        self._style = 1
        self._extra_style = 0
        self._include_advanced = True
        # events
        self._on_property_selected = ''
        self._on_property_changed = ''
        self._on_property_changing = ''
        self._on_property_highlighted = ''
        self._on_property_right_click = ''
        self._on_property_double_click = ''
        self._on_property_item_collapsed = ''
        self._on_property_item_expanded = ''
        self._on_property_label_edit_begin = ''
        self._on_property_label_edit_ending = ''
        self._on_property_col_begin_drag = ''
        self._on_property_col_dragging = ''
        self._on_property_col_end_drag = ''
        super(PropertyGridControl, self).__init__(**kwargs)

    @property
    def local_property_grid_kwargs(self):
        return {
            # properties
            'bitmap': self._bitmap,
            'style': self._style,
            'extra_style': self._extra_style,
            'include_advanced': self._include_advanced,
            # events
            'on_property_selected': self._on_property_selected,
            'on_property_changed': self._on_property_changed,
            'on_property_changing': self._on_property_changing,
            'on_property_highlighted': self._on_property_highlighted,
            'on_property_right_click': self._on_property_right_click,
            'on_property_double_click': self._on_property_double_click,
            'on_property_item_collapsed': self._on_property_item_collapsed,
            'on_property_item_expanded': self._on_property_item_expanded,
            'on_property_label_edit_begin': self._on_property_label_edit_begin,
            'on_property_label_edit_ending': self._on_property_label_edit_ending,
            'on_property_col_begin_drag': self._on_property_col_begin_drag,
            'on_property_col_dragging': self._on_property_col_dragging,
            'on_property_col_end_drag': self._on_property_col_end_drag,
        }

    @local_property_grid_kwargs.setter
    def local_property_grid_kwargs(self, kwargs):

        #properties
        self._bitmap = kwargs.get('bitmap', self._bitmap)
        self._style = kwargs.get('style', self._style)
        self._extra_style = kwargs.get('extra_style', self._extra_style)
        self._include_advanced = kwargs.get('include_advanced', self._include_advanced)

        #events
        self._on_property_selected = kwargs.get(
            'on_property_selected', self._on_property_selected)
        self._on_property_changed = kwargs.get(
            'on_property_changed', self._on_property_changed)
        self._on_property_changing = kwargs.get(
            'on_property_changing', self._on_property_changing)
        self._on_property_highlighted = kwargs.get(
            'on_property_highlighted', self._on_property_highlighted)
        self._on_property_right_click = kwargs.get(
            'on_property_right_click', self._on_property_right_click)
        self._on_property_double_click = kwargs.get(
            'on_property_double_click', self._on_property_double_click)
        self._on_property_item_collapsed = kwargs.get(
            'on_property_item_collapsed', self._on_property_item_collapsed)
        self._on_property_item_expanded = kwargs.get(
            'on_property_item_expanded', self._on_property_item_expanded)
        self._on_property_label_edit_begin = kwargs.get(
            'on_property_label_edit_begin', self._on_property_label_edit_begin)
        self._on_property_label_edit_ending = kwargs.get(
            'on_property_label_edit_ending', self._on_property_label_edit_ending)
        self._on_property_col_begin_drag = kwargs.get(
            'on_property_col_begin_drag', self._on_property_col_begin_drag)
        self._on_property_col_dragging = kwargs.get(
            'on_property_col_dragging', self._on_property_col_dragging)
        self._on_property_col_end_drag = kwargs.get(
            'on_property_col_end_drag', self._on_property_col_end_drag)

    @property
    def local_property_grid_events(self):
        return {
            'on_property_selected': (self._on_property_selected, 'wx.EVT_PG_SELECTED', 'wxEVT_PG_SELECTED'),
            'on_property_changed': (self._on_property_changed, 'wx.EVT_PG_CHANGED', 'wxEVT_PG_CHANGED'),
            'on_property_changing': (self._on_property_changing, 'wx.EVT_PG_CHANGING', 'wxEVT_PG_CHANGING'),
            'on_property_highlighted': (self._on_property_highlighted, 'wx.EVT_PG_HIGHLIGHTED', 'wxEVT_PG_HIGHLIGHTED'),
            'on_property_right_click': (self._on_property_right_click, 'wx.EVT_PG_RIGHT_CLICK', 'wxEVT_PG_RIGHT_CLICK'),
            'on_property_double_click': (self._on_property_double_click, 'wx.EVT_PG_DOUBLE_CLICK',
                                         'wxEVT_PG_DOUBLE_CLICK'),
            'on_property_item_collapsed': (self._on_property_item_collapsed, 'wx.EVT_PG_ITEM_COLLAPSED',
                                           'wxEVT_PG_ITEM_COLLAPSED'),
            'on_property_item_expanded': (self._on_property_item_expanded, 'wx.EVT_PG_ITEM_EXPANDED',
                                          'wxEVT_PG_ITEM_EXPANDED'),
            'on_property_label_edit_begin': (self._on_property_label_edit_begin, 'wx.EVT_PG_LABEL_EDIT_BEGIN',
                                             'wxEVT_PG_LABEL_EDIT_BEGIN'),
            'on_property_label_edit_ending': (self._on_property_label_edit_ending, 'wx.EVT_PG_LABEL_EDIT_ENDING',
                                              'wxEVT_PG_LABEL_EDIT_ENDING'),
            'on_property_col_begin_drag': (self._on_property_col_begin_drag, 'wx.EVT_PG_COL_BEGIN_DRAG',
                                           'wxEVT_PG_COL_BEGIN_DRAG'),
            'on_property_col_dragging': (self._on_property_col_dragging, 'wx.EVT_PG_COL_DRAGGING',
                                         'wxEVT_PG_COL_DRAGGING'),
            'on_property_col_end_drag': (self._on_property_col_end_drag, 'wx.EVT_PG_COL_END_DRAG',
                                         'wxEVT_PG_COL_END_DRAG'),
        }

    @property
    def events(self):
        value = self.local_property_grid_events
        value.update(super(PropertyGridControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_property_grid_kwargs
        value.update(super(PropertyGridControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_property_grid_kwargs = kwargs
        super(PropertyGridControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('property_grid')

    @property
    def style_attribute_options(self):
        return [
            ('wPG_DEFAULT_STYLE', "Default property grid style."),
            ('wxPG_AUTO_SORT', "This will cause Sort() automatically after an item is added. When inserting a lot of "
                               "items in this mode, it may make sense to use Freeze() before operations and Thaw() "
                               "afterwards to increase performance."),
            ('wxPG_HIDE_CATEGORIES', "Categories are not initially shown (even if added)."),
            ('wxPG_ALPHABETIC_MODE', "This style combines non-categoric mode and automatic sorting."),
            ('wxPG_BOLD_MODIFIED', "Modified values are shown in bold font. Changing this requires Refresh() to "
                                   "show changes."),
            ('wxPG_SPLITTER_AUTO_CENTER', "When wxPropertyGrid is resized, splitter moves to the center. This "
                                          "behaviour stops once the user manually moves the splitter."),
            ('wxPG_TOOLTIPS', "Display tool tips for cell text that cannot be shown completely. If wxUSE_TOOLTIPS is "
                              "0, then this doesn't have any effect."),
            ('wxPG_HIDE_MARGIN', "Disables margin and hides all expand/collapse buttons that would appear outside "
                                 "the margin (for sub-properties). Toggling this style automatically expands all "
                                 "collapsed items."),
            ('wxPG_STATIC_SPLITTER', "This style prevents user from moving the splitter."),
            ('wxPG_STATIC_LAYOUT', "Combination of other styles that make it impossible for user to modify "
                                   "the layout."),
            ('wxPG_LIMITED_EDITING', "Disables wxTextCtrl based editors for properties which can be edited in "
                                     "another way. Equals calling wxPropertyGrid::LimitPropertyEditing() for all "
                                     "added properties."),
            ('wxPG_TOOLBAR', "wxPropertyGridManager only: Show tool bar for mode and page selection."),
            ('wxPG_DESCRIPTION', "wxPropertyGridManager only: Show adjustable text box showing description or "
                                 "help text, if available, for currently selected property."),
            ('wxPG_NO_INTERNAL_BORDER', "wxPropertyGridManager only: don't show an internal border around the "
                                        "property grid. Recommended if you use a header."),
        ]

    @property
    def extra_style_attribute_options(self):
        return [
            ('wxPG_EX_INIT_NOCAT',
             "Speeds up switching to wxPG_HIDE_CATEGORIES mode. Initially, if wxPG_HIDE_CATEGORIES is not defined, "
             "the non-categorized data storage is not activated, and switching the mode first time becomes somewhat "
             "slower. wxPG_EX_INIT_NOCAT activates the non-categorized data storage right away."),
            ('wxPG_EX_NO_FLAT_TOOLBAR',
             "Extended window style that sets wxPropertyGridManager tool bar to not use flat style."),
            ('wxPG_EX_MODE_BUTTONS', "Shows alphabetic/categoric mode buttons on wxPropertyGridManager tool bar."),
            ('wxPG_EX_HELP_AS_TOOLTIPS',
             "Show property help strings as tool tips instead as text on the status bar. You can set the help strings "
             "using SetPropertyHelpString member function."),
            ('wxPG_EX_NATIVE_DOUBLE_BUFFERING', "Allows relying on native double-buffering."),
            ('wxPG_EX_AUTO_UNSPECIFIED_VALUES',
             "Set this style to let user have ability to set values of properties to unspecified state. Same as "
             "setting wxPG_PROP_AUTO_UNSPECIFIED for all properties."),
            ('wxPG_EX_WRITEONLY_BUILTIN_ATTRIBUTES',
             "If this style is used, built-in attributes (such as wxPG_FLOAT_PRECISION and wxPG_STRING_PASSWORD) are "
             "not stored into property's attribute storage (thus they are not readable). This option is global, and "
             "applies to all wxPG property containers."),
            ('wxPG_EX_HIDE_PAGE_BUTTONS', "Hides page selection buttons from wxPropertyGridManager tool bar."),
            ('wxPG_EX_MULTIPLE_SELECTION',
             "Allows multiple properties to be selected by user (by pressing SHIFT when clicking on a property, or by "
             "dragging with left mouse button down). You can get array of selected properties with "
             "wxPropertyGridInterface::GetSelectedProperties(). In multiple selection mode "
             "wxPropertyGridInterface::GetSelection() returns property which has editor active "
             "(usually the first one selected)."),
            ('wxPG_EX_ENABLE_TLP_TRACKING',
             "This enables top-level window tracking which allows wxPropertyGrid to notify the application of "
             "last-minute property value changes by user. This style is not enabled by default because it may cause "
             "crashes when wxPropertyGrid is used in with wxAUI or similar system. If you are not in fact using any "
             "system that may change wxPropertyGrid's top-level parent window on its own, then you are recommended to "
             "enable this style."),
            ('wxPG_EX_NO_TOOLBAR_DIVIDER', "Don't show divider above wxPropertyGridManager toolbar (wxMSW only)."),
            ('wxPG_EX_TOOLBAR_SEPARATOR', "Show a separator below the wxPropertyGridManager toolbar."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Property grid',
            'type': 'category',
            'help': 'Attributes of property grid',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The property control name."
                },
                # -- This attribute is used in wxformbuilder, but really looks as a bug
                #    be cause we didn't found any valid usage for. Until decide delete
                #    comment out the code
                #{
                #    'label': 'bitmap',
                #    'type': 'bitmap',
                #    'read_only': False,
                #    'value': self._bitmap,
                #    'name': 'bitmap',  # kwarg value
                #    'help': "The property grid bitmap."
                #},
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'read_only': False,
                    'value': self._style,
                    'name': 'style',  # kwarg value
                    'options': self.style_attribute_options,
                    'help': "The property grid extra style."
                },
                {
                    'label': 'extra_style',
                    'type': 'multi_choice',
                    'read_only': False,
                    'value': self._extra_style,
                    'name': 'extra_style',  # kwarg value
                    'options': self.extra_style_attribute_options,
                    'help': "The property grid style."
                },
                {
                    'label': 'include_advanced',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._include_advanced,
                    'name': 'include_advanced',  # kwarg value
                    'help': "Use property grid advanced properties."
                },
            ]
        }]
        _event = [{
            'label': 'Property grid',
            'type': 'category',
            'help': 'Events of property grid.',
            'child': [
                {
                    'label': 'on_property_selected',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_selected,
                    'name': 'on_property_selected',
                    'help': "Respond to wxEVT_PG_SELECTED event, generated when a property selection has been changed, "
                            "either by user action or by indirect program function. For instance, collapsing a parent "
                            "property programmatically causes any selected child property to become unselected, and "
                            "may therefore cause this event to be generated.",
                },
                {
                    'label': 'on_property_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_changed,
                    'name': 'on_property_changed',
                    'help': "Respond to wxEVT_PG_CHANGED event, generated when property value has been "
                            "changed by the user.",
                },
                {
                    'label': 'on_property_changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_changing,
                    'name': 'on_property_changing',
                    'help': "Respond to wxEVT_PG_CHANGING event, generated when property value is about to be changed "
                            "by user. Use wxPropertyGridEvent::GetValue() to take a peek at the pending value, and "
                            "wxPropertyGridEvent::Veto() to prevent change from taking place, if necessary.",
                },
                {
                    'label': 'on_property_highlighted',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_highlighted,
                    'name': 'on_property_highlighted',
                    'help': "Respond to wxEVT_PG_HIGHLIGHTED event, which occurs when mouse moves over a property. "
                            "Event's property is nullptr if hovered area does not belong to any property.",
                },
                {
                    'label': 'on_property_right_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_right_click,
                    'name': 'on_property_right_click',
                    'help': "Respond to wxEVT_PG_RIGHT_CLICK event, which occurs when property is clicked on with "
                            "right mouse button.",
                },
                {
                    'label': 'on_property_double_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_double_click,
                    'name': 'on_property_double_click',
                    'help': "Respond to wxEVT_PG_DOUBLE_CLICK event, which occurs when property is double-clicked on "
                            "with left mouse button.",
                },
                {
                    'label': 'on_property_item_collapsed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_item_collapsed,
                    'name': 'on_property_item_collapse',
                    'help': "Respond to wxEVT_PG_ITEM_COLLAPSED event, generated when user collapses a property "
                            "or category.",
                },
                {
                    'label': 'on_property_item_expanded',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_item_expanded,
                    'name': 'on_property_item_expande',
                    'help': "Respond to wxEVT_PG_ITEM_EXPANDED event, generated when user expands a property or "
                            "category.",
                },
                {
                    'label': 'on_property_label_edit_begin',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_label_edit_begin,
                    'name': 'on_property_label_edit_begi',
                    'help': "Respond to wxEVT_PG_LABEL_EDIT_BEGIN event, generated when user is about to begin "
                            "editing a property label. You can veto this event to prevent the action.",
                },
                {
                    'label': 'on_property_label_edit_ending',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_label_edit_ending,
                    'name': 'on_property_label_edit_endin',
                    'help': "Respond to wxEVT_PG_LABEL_EDIT_ENDING event, generated when user is about to end editing "
                            "of a property label. You can veto this event to prevent the action.",
                },
                {
                    'label': 'on_property_col_begin_drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_col_begin_drag,
                    'name': 'on_property_col_begin_dra',
                    'help': "Respond to wxEVT_PG_COL_BEGIN_DRAG event, generated when user starts resizing a "
                            "column - can be vetoed.",
                },
                {
                    'label': 'on_property_col_dragging',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_col_dragging,
                    'name': 'on_property_col_draggin',
                    'help': "Respond to wxEVT_PG_COL_DRAGGING, event, generated when a column resize by user is in "
                            "progress. This event is also generated when user double-clicks the splitter in order to "
                            "recenter it.",
                },
                {
                    'label': 'on_property_col_end_drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_col_end_drag,
                    'name': 'on_property_col_end_dra',
                    'help': "Respond to wxEVT_PG_COL_END_DRAG event, generated after column resize by user has "
                            "finished.",
                },
            ],
        }]

        _base_property, _base_event = super(PropertyGridControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_sample_content(self):
        pass

    @property
    def extra_style_value(self):
        """Return the value required for dynamic button creation"""
        self_extra_style_value = self.style_value_helper(PropertyGridControl.extra_style_list, self._extra_style)
        return self_extra_style_value | super(PropertyGridControl, self).extra_style_value

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            grid = self._edition_window = uiMockupControl(wpg.PropertyGrid)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            grid.SetExtraStyle(self.extra_style_value)
            self.add_to_sizer(container, self._edition_window)
        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK | wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self.create_sample_content()
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/propgrid/propgrid.h>
        cls = register_cpp_class(self.project, 'wxPropertyGrid')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="property grid control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/propgrid/propgrid.h>')
        window_parent = self.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = "{name} = new wxPropertyGrid({owner}, {id_}, {pos}, {size}, {style});\n" .format(
            name=self._name, owner=owner, id_=self._id, pos=self.cc_pos_str,
            size=self.cc_size_str, style=self.cc_style_str)
        code += super(PropertyGridControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.propgrid')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = '{this} = wx.propgrid.PropertyGrid({owner}, {id_}, {pos}, {size}, {style})\n'.\
            format(this=this, owner=owner, id_=self.py_id_str, pos=self.py_pos_str, size=self.py_size_str,
                   style=self.py_style_str)
        code += super(PropertyGridControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code

