import wx
from .._WindowControl import WindowControl
from ._PropertyGridManagerControl import PropertyGridManagerControl
from beatle.model import cc
from ....lib import register_cpp_class, get_bitmap_from_tuple, get_bitmap_py_text_from_tuple


class PropertyGridPageControl(WindowControl):
    """"This class represents a property grid page"""

    def __init__(self, **kwargs):
        self._label = ''
        self._bitmap = (0, '')
        # events
        self._on_property_selected = ''
        self._on_property_changed = ''
        self._on_property_changing = ''
        self._on_property_highlighted = ''
        self._on_property_right_click = ''
        self._on_property_double_click = ''
        self._on_property_item_collapsed = ''
        self._on_property_item_expanded = ''
        self._on_property_label_edit_begin = ''
        self._on_property_label_edit_ending = ''
        self._on_property_col_begin_drag = ''
        self._on_property_col_dragging = ''
        self._on_property_col_end_drag = ''
        super(PropertyGridPageControl, self).__init__(**kwargs)

    @property
    def local_property_grid_page_kwargs(self):
        return {
            'label': self._label,
            'bitmap': self._bitmap,
            # events
            'on_property_selected': self._on_property_selected,
            'on_property_changed': self._on_property_changed,
            'on_property_changing': self._on_property_changing,
            'on_property_highlighted': self._on_property_highlighted,
            'on_property_right_click': self._on_property_right_click,
            'on_property_double_click': self._on_property_double_click,
            'on_property_item_collapsed': self._on_property_item_collapsed,
            'on_property_item_expanded': self._on_property_item_expanded,
            'on_property_label_edit_begin': self._on_property_label_edit_begin,
            'on_property_label_edit_ending': self._on_property_label_edit_ending,
            'on_property_col_begin_drag': self._on_property_col_begin_drag,
            'on_property_col_dragging': self._on_property_col_dragging,
            'on_property_col_end_drag': self._on_property_col_end_drag,
        }

    @local_property_grid_page_kwargs.setter
    def local_property_grid_page_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._bitmap = kwargs.get('bitmap', self._bitmap)
        #events
        self._on_property_selected = kwargs.get(
            'on_property_selected', self._on_property_selected)
        self._on_property_changed = kwargs.get(
            'on_property_changed', self._on_property_changed)
        self._on_property_changing = kwargs.get(
            'on_property_changing', self._on_property_changing)
        self._on_property_highlighted = kwargs.get(
            'on_property_highlighted', self._on_property_highlighted)
        self._on_property_right_click = kwargs.get(
            'on_property_right_click', self._on_property_right_click)
        self._on_property_double_click = kwargs.get(
            'on_property_double_click', self._on_property_double_click)
        self._on_property_item_collapsed = kwargs.get(
            'on_property_item_collapsed', self._on_property_item_collapsed)
        self._on_property_item_expanded = kwargs.get(
            'on_property_item_expanded', self._on_property_item_expanded)
        self._on_property_label_edit_begin = kwargs.get(
            'on_property_label_edit_begin', self._on_property_label_edit_begin)
        self._on_property_label_edit_ending = kwargs.get(
            'on_property_label_edit_ending', self._on_property_label_edit_ending)
        self._on_property_col_begin_drag = kwargs.get(
            'on_property_col_begin_drag', self._on_property_col_begin_drag)
        self._on_property_col_dragging = kwargs.get(
            'on_property_col_dragging', self._on_property_col_dragging)
        self._on_property_col_end_drag = kwargs.get(
            'on_property_col_end_drag', self._on_property_col_end_drag)

    @property
    def local_property_grid_page_events(self):
        return {
            'on_property_selected': (self._on_property_selected, 'wx.EVT_PG_SELECTED', 'wxEVT_PG_SELECTED'),
            'on_property_changed': (self._on_property_changed, 'wx.EVT_PG_CHANGED', 'wxEVT_PG_CHANGED'),
            'on_property_changing': (self._on_property_changing, 'wx.EVT_PG_CHANGING', 'wxEVT_PG_CHANGING'),
            'on_property_highlighted': (self._on_property_highlighted, 'wx.EVT_PG_HIGHLIGHTED', 'wxEVT_PG_HIGHLIGHTED'),
            'on_property_right_click': (self._on_property_right_click, 'wx.EVT_PG_RIGHT_CLICK', 'wxEVT_PG_RIGHT_CLICK'),
            'on_property_double_click': (self._on_property_double_click, 'wx.EVT_PG_DOUBLE_CLICK',
                                         'wxEVT_PG_DOUBLE_CLICK'),
            'on_property_item_collapsed': (self._on_property_item_collapsed, 'wx.EVT_PG_ITEM_COLLAPSED',
                                           'wxEVT_PG_ITEM_COLLAPSED'),
            'on_property_item_expanded': (self._on_property_item_expanded, 'wx.EVT_PG_ITEM_EXPANDED',
                                          'wxEVT_PG_ITEM_EXPANDED'),
            'on_property_label_edit_begin': (self._on_property_label_edit_begin, 'wx.EVT_PG_LABEL_EDIT_BEGIN',
                                             'wxEVT_PG_LABEL_EDIT_BEGIN'),
            'on_property_label_edit_ending': (self._on_property_label_edit_ending, 'wx.EVT_PG_LABEL_EDIT_ENDING',
                                              'wxEVT_PG_LABEL_EDIT_ENDING'),
            'on_property_col_begin_drag': (self._on_property_col_begin_drag, 'wx.EVT_PG_COL_BEGIN_DRAG',
                                           'wxEVT_PG_COL_BEGIN_DRAG'),
            'on_property_col_dragging': (self._on_property_col_dragging, 'wx.EVT_PG_COL_DRAGGING',
                                         'wxEVT_PG_COL_DRAGGING'),
            'on_property_col_end_drag': (self._on_property_col_end_drag, 'wx.EVT_PG_COL_END_DRAG',
                                         'wxEVT_PG_COL_END_DRAG'),
        }

    @property
    def events(self):
        value = self.local_property_grid_page_events
        value.update(super(PropertyGridPageControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_property_grid_page_kwargs
        value.update(super(PropertyGridPageControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        """Set the bitmap button arguments.
        This button is generally hard to update so, if editing,
         we directly recreate it."""
        self.local_property_grid_page_kwargs = kwargs
        super(PropertyGridPageControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('property_grid_page')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Property-grid page',
            'type': 'category',
            'help': 'Attributes of property-grid page',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The property page control name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': "The property page label."
                },
                {
                    'label': 'bitmap',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._bitmap,
                    'name': 'bitmap',  # kwarg value
                    'help': "The property page bitmap."
                },
            ]
        },]
        _event = [{
            'label': 'Property-grid page',
            'type': 'category',
            'help': 'Events of property-grid page',
            'child': [
                {
                    'label': 'on_property_selected',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_selected,
                    'name': 'on_property_selected',
                    'help': "Respond to wxEVT_PG_SELECTED event, generated when a property selection has been changed, "
                            "either by user action or by indirect program function. For instance, collapsing a parent "
                            "property programmatically causes any selected child property to become unselected, and "
                            "may therefore cause this event to be generated.",
                },
                {
                    'label': 'on_property_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_changed,
                    'name': 'on_property_changed',
                    'help': "Respond to wxEVT_PG_CHANGED event, generated when property value has been "
                            "changed by the user.",
                },
                {
                    'label': 'on_property_changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_changing,
                    'name': 'on_property_changing',
                    'help': "Respond to wxEVT_PG_CHANGING event, generated when property value is about to be changed "
                            "by user. Use wxPropertyGridEvent::GetValue() to take a peek at the pending value, and "
                            "wxPropertyGridEvent::Veto() to prevent change from taking place, if necessary.",
                },
                {
                    'label': 'on_property_highlighted',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_highlighted,
                    'name': 'on_property_highlighted',
                    'help': "Respond to wxEVT_PG_HIGHLIGHTED event, which occurs when mouse moves over a property. "
                            "Event's property is nullptr if hovered area does not belong to any property.",
                },
                {
                    'label': 'on_property_right_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_right_click,
                    'name': 'on_property_right_click',
                    'help': "Respond to wxEVT_PG_RIGHT_CLICK event, which occurs when property is clicked on with "
                            "right mouse button.",
                },
                {
                    'label': 'on_property_double_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_double_click,
                    'name': 'on_property_double_click',
                    'help': "Respond to wxEVT_PG_DOUBLE_CLICK event, which occurs when property is double-clicked on "
                            "with left mouse button.",
                },
                {
                    'label': 'on_property_item_collapsed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_item_collapsed,
                    'name': 'on_property_item_collapse',
                    'help': "Respond to wxEVT_PG_ITEM_COLLAPSED event, generated when user collapses a property "
                            "or category.",
                },
                {
                    'label': 'on_property_item_expanded',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_item_expanded,
                    'name': 'on_property_item_expande',
                    'help': "Respond to wxEVT_PG_ITEM_EXPANDED event, generated when user expands a property or "
                            "category.",
                },
                {
                    'label': 'on_property_label_edit_begin',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_label_edit_begin,
                    'name': 'on_property_label_edit_begi',
                    'help': "Respond to wxEVT_PG_LABEL_EDIT_BEGIN event, generated when user is about to begin "
                            "editing a property label. You can veto this event to prevent the action.",
                },
                {
                    'label': 'on_property_label_edit_ending',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_label_edit_ending,
                    'name': 'on_property_label_edit_endin',
                    'help': "Respond to wxEVT_PG_LABEL_EDIT_ENDING event, generated when user is about to end editing "
                            "of a property label. You can veto this event to prevent the action.",
                },
                {
                    'label': 'on_property_col_begin_drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_col_begin_drag,
                    'name': 'on_property_col_begin_dra',
                    'help': "Respond to wxEVT_PG_COL_BEGIN_DRAG event, generated when user starts resizing a "
                            "column - can be vetoed.",
                },
                {
                    'label': 'on_property_col_dragging',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_col_dragging,
                    'name': 'on_property_col_draggin',
                    'help': "Respond to wxEVT_PG_COL_DRAGGING, event, generated when a column resize by user is in "
                            "progress. This event is also generated when user double-clicks the splitter in order to "
                            "recenter it.",
                },
                {
                    'label': 'on_property_col_end_drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_property_col_end_drag,
                    'name': 'on_property_col_end_dra',
                    'help': "Respond to wxEVT_PG_COL_END_DRAG event, generated after column resize by user has "
                            "finished.",
                },
            ],
        },]
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        bitmap = get_bitmap_from_tuple(self._bitmap) or wx.NullBitmap
        self._edition_window = container.AddPage(self._label, bitmap)
        for child in self.sorted_wxui_child:
            child.create_edition_window(self)
        edition_frame.Layout()

    def destroy_edition_window(self):
        if self._edition_window is not None:
            reverse_sorted = reversed(self.sorted_wxui_child)
            for child in reverse_sorted:
                child.destroy_edition_window()
            container = getattr(self.parent, '_edition_window', None)
            if container is not None and type(self.parent) is PropertyGridManagerControl:
                container.RemovePage(self._edition_window.GetIndex())
            else:
                self._edition_window.Destroy()
        self._edition_window = None

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/propgrid/manager.h>
        cls = register_cpp_class(self.project, 'wxPropertyGridPage')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="property grid page control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/propgrid/propgrid.h>')
        self.add_cpp_required_header('#include <wx/propgrid/manager.h>')
        window_parent = self.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = '{name} = {owner}->AddPage( "{page}", wxNullBitmap );\n'.format(
            name=self._name, owner=owner, page=self._label)
        code += super(PropertyGridPageControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent
        owner = window_parent.py_owner_text
        bitmap_text = get_bitmap_py_text_from_tuple(self._bitmap) or 'wx.NullBitmap'
        code = '{this} = {owner}.AddPage( "{page}", {bitmap} )\n'.format(
            this=this, owner=owner, page=self._label, bitmap=bitmap_text)
        code += super(PropertyGridPageControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        return code
