import wx
import wx.dataview
from .._WindowControl import WindowControl
from beatle.model import cc
from ....lib import register_cpp_class, with_style


@with_style
class TreeListControl(WindowControl):
    """"This class represents a tree list control"""

    style_list = (
        wx.dataview.TL_SINGLE, wx.dataview.TL_MULTIPLE, wx.dataview.TL_CHECKBOX, wx.dataview.TL_3STATE,
        wx.dataview.TL_USER_3STATE, wx.dataview.TL_NO_HEADER, wx.dataview.TL_DEFAULT_STYLE
    )

    style_list_py_text = (
        'wx.dataview.TL_SINGLE', 'wx.dataview.TL_MULTIPLE', 'wx.dataview.TL_CHECKBOX', 'wx.dataview.TL_3STATE',
        'wx.dataview.TL_USER_3STATE', 'wx.dataview.TL_NO_HEADER', 'wx.dataview.TL_DEFAULT_STYLE',
    )

    style_list_cc_text = (
        'wxTL_SINGLE', 'wxTL_MULTIPLE', 'wxTL_CHECKBOX', 'wxTL_3STATE',
        'wxTL_USER_3STATE', 'wxTL_NO_HEADER', 'wxTL_DEFAULT_STYLE',
    )

    def __init__(self, **kwargs):
        self._style = 1
        # events
        self._on_treelist_selection_changed = ''
        self._on_treelist_item_expanding = ''
        self._on_treelist_item_expanded = ''
        self._on_treelist_item_checked = ''
        self._on_treelist_item_activated = ''
        self._on_treelist_item_context_menu = ''
        self._on_treelist_column_sorted = ''
        super(TreeListControl, self).__init__(**kwargs)

    @property
    def local_treelistctrl_kwargs(self):
        return {
            'style': self._style,
            # events
        }

    @local_treelistctrl_kwargs.setter
    def local_treelistctrl_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        #  events

    @property
    def kwargs(self):
        value = self.local_treelistctrl_kwargs
        value.update(super(TreeListControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_treelistctrl_kwargs = kwargs
        super(TreeListControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('tree_list_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxTL_SINGLE', "Single selection, this is the default."),
            ('wxTL_MULTIPLE', "Allow multiple selection, see GetSelections()."),
            ('wxTL_CHECKBOX', "Show the usual, 2 state, checkboxes for the items in the first column."),
            ('wxTL_3STATE', "Show the checkboxes that can possibly be set by the program, but not the user, to a "
                            "third, undetermined, state, for the items in the first column. Implies wxTL_CHECKBOX."),
            ('wxTL_USER_3STATE', "Same as wxTL_3STATE but the user can also set the checkboxes to the undetermined "
                                 "state. Implies wxTL_3STATE."),
            ('wxTL_NO_HEADER', "Don't show the column headers, that are shown by default. Notice that this style is "
                               "only available since wxWidgets 2.9.5."),
            ('wxTL_DEFAULT_STYLE', "Style used by the control by default, just wxTL_SINGLE currently."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Tree list',
            'type': 'category',
            'help': 'Attributes of tree list.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The tree control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the tree list control style.'
                }]
        }]
        _event = [{
            'label': 'TreeListControl',
            'type': 'category',
            'help': 'Events of tree list control',
            'child': [
                {
                    'label': 'on_treelist_selection_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_treelist_selection_changed,
                    'name': 'on_treelist_selection_changed',
                    'help': "Process wxEVT_TREELIST_SELECTION_CHANGED event and notifies about the selection change "
                            "in the control. In the single selection case the item indicated by the event has been "
                            "selected and previously selected item, if any, was deselected. In multiple selection "
                            "case, the selection of this item has just changed (it may have been either selected or "
                            "deselected) but notice that the selection of other items could have changed as well, use "
                            "wx.dataview.TreeListCtrl.GetSelections to retrieve the new selection if necessary.",
                },
                {
                    'label': 'on_treelist_item_expanding',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_treelist_item_expanding,
                    'name': 'on_treelist_item_expanding',
                    'help': "Process wxEVT_TREELIST_ITEM_EXPANDING event notifying about the given branch being "
                            "expanded. This event is sent before the expansion occurs and can be vetoed to prevent "
                            "it from happening.",
                },
                {
                    'label': 'on_treelist_item_expanded',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_treelist_item_expanded,
                    'name': 'on_treelist_item_expanded',
                    'help': "Process wxEVT_TREELIST_ITEM_EXPANDED event notifying about the expansion of the given "
                            "branch. This event is sent after the expansion occurs and canâ€™t be vetoed.",
                },
                {
                    'label': 'on_treelist_item_checked',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_treelist_item_checked,
                    'name': 'on_treelist_item_checked',
                    'help': "Process wxEVT_TREELIST_ITEM_CHECKED event notifying about the user checking or unchecking "
                            "the item. You can use wx.dataview.TreeListCtrl.GetCheckedState to retrieve the new item "
                            "state and wx.dataview.TreeListEvent.GetOldCheckedState to get the previous one.",
                },
                {
                    'label': 'on_treelist_item_activated',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_treelist_item_activated,
                    'name': 'on_treelist_item_activated',
                    'help': "Process wxEVT_TREELIST_ITEM_ACTIVATED event notifying about the user double clicking the "
                            "item or activating it from keyboard.",
                },
                {
                    'label': 'on_treelist_item_context_menu',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_treelist_item_context_menu,
                    'name': 'on_treelist_item_context_menu',
                    'help': "Process wxEVT_TREELIST_ITEM_CONTEXT_MENU event indicating that the popup menu for the "
                            "given item should be displayed.",
                },
                {
                    'label': 'on_treelist_column_sorted',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_treelist_column_sorted,
                    'name': 'on_treelist_column_sorted',
                    'help': "Process wxEVT_TREELIST_COLUMN_SORTED event indicating that the control contents has just "
                            "been resorted using the specified column. The event doesnâ€™t carry the sort direction, "
                            "use GetSortColumn method if you need to know it.",
                },
            ],
        }]

        _base_property, _base_event = super(TreeListControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.dataview.TreeListCtrl)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            # add tree pseudo controls
            for child in self.sorted_wxui_child:
                child.create_edition_window(self)
            self.add_to_sizer(container, self._edition_window)
            edition_frame.Layout()

        except Exception as exception:
            message = str(exception)
            if ':' in message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/treelist.h>
        cls = register_cpp_class(self.project, 'wxTreeListCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="tree list control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/treelist.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = "{name} = new wxTreeListCtrl({owner}, {id_}, {pos}, {size}, {style});\n" .format(
            name=self._name, owner=owner, id_=self._id, pos=self.cc_pos_str,
            size=self.cc_size_str, style=self.cc_style_str)
        code += super(TreeListControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.dataview')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = 'self.{name}'.format(name=window_parent.name)

        code = '{this} = wx.dataview.TreeListCtrl({owner}, {id_}, {pos}, {size}, {style})\n'.\
            format(this=this, owner=owner, id_=self.py_id_str, pos=self.py_pos_str, size=self.py_size_str,
                   style=self.py_style_str)
        code += super(TreeListControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code


