import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc
from ....lib import register_cpp_class, with_style


@with_style
class TreeControl(WindowControl):
    """"This class represents a tree control"""

    style_list = (
        wx.TR_DEFAULT_STYLE, wx.TR_EDIT_LABELS, wx.TR_NO_BUTTONS, wx.TR_HAS_BUTTONS, wx.TR_TWIST_BUTTONS,
        wx.TR_NO_LINES, wx.TR_FULL_ROW_HIGHLIGHT, wx.TR_LINES_AT_ROOT, wx.TR_HIDE_ROOT, wx.TR_ROW_LINES,
        wx.TR_HAS_VARIABLE_ROW_HEIGHT, wx.TR_SINGLE, wx.TR_MULTIPLE)

    style_list_py_text = (
        'wx.TR_DEFAULT_STYLE', 'wx.TR_EDIT_LABELS', 'wx.TR_NO_BUTTONS', 'wx.TR_HAS_BUTTONS', 'wx.TR_TWIST_BUTTONS',
        'wx.TR_NO_LINES', 'wx.TR_FULL_ROW_HIGHLIGHT', 'wx.TR_LINES_AT_ROOT', 'wx.TR_HIDE_ROOT', 'wx.TR_ROW_LINES',
        'wx.TR_HAS_VARIABLE_ROW_HEIGHT', 'wx.TR_SINGLE', 'wx.TR_MULTIPLE',)

    style_list_cc_text = (
        'wxTR_DEFAULT_STYLE', 'wxTR_EDIT_LABELS', 'wxTR_NO_BUTTONS', 'wxTR_HAS_BUTTONS', 'wxTR_TWIST_BUTTONS',
        'wxTR_NO_LINES', 'wxTR_FULL_ROW_HIGHLIGHT', 'wxTR_LINES_AT_ROOT', 'wxTR_HIDE_ROOT', 'wxTR_ROW_LINES',
        'wxTR_HAS_VARIABLE_ROW_HEIGHT', 'wxTR_SINGLE', 'wxTR_MULTIPLE',)

    def __init__(self, **kwargs):
        self._style = 1
        # events
        self._on_tree_begin_drag = ''
        self._on_tree_begin_rdrag = ''
        self._on_tree_end_drag = ''
        self._on_tree_begin_label_edit = ''
        self._on_tree_end_label_edit = ''
        self._on_tree_delete_item = ''
        self._on_tree_get_info = ''
        self._on_tree_set_info = ''
        self._on_tree_item_activated = ''
        self._on_tree_item_collapsed = ''
        self._on_tree_item_collapsing = ''
        self._on_tree_item_expanded = ''
        self._on_tree_item_expanding = ''
        self._on_tree_item_right_click = ''
        self._on_tree_item_middle_click = ''
        self._on_tree_sel_changed = ''
        self._on_tree_sel_changing = ''
        self._on_tree_key_down = ''
        self._on_tree_item_gettooltip = ''
        self._on_tree_item_menu = ''
        self._on_tree_state_image_click = ''
        super(TreeControl, self).__init__(**kwargs)

    @property
    def local_treectrl_kwargs(self):
        return {
            'style': self._style,
            # events
            'on_tree_begin_drag': self._on_tree_begin_drag,
            'on_tree_begin_rdrag': self._on_tree_begin_rdrag,
            'on_tree_end_drag': self._on_tree_end_drag,
            'on_tree_begin_label_edit': self._on_tree_begin_label_edit,
            'on_tree_end_label_edit': self._on_tree_end_label_edit,
            'on_tree_delete_item': self._on_tree_delete_item,
            'on_tree_get_info': self._on_tree_get_info,
            'on_tree_set_info': self._on_tree_set_info,
            'on_tree_item_activated': self._on_tree_item_activated,
            'on_tree_item_collapsed': self._on_tree_item_collapsed,
            'on_tree_item_collapsing': self._on_tree_item_collapsing,
            'on_tree_item_expanded': self._on_tree_item_expanded,
            'on_tree_item_expanding': self._on_tree_item_expanding,
            'on_tree_item_right_click': self._on_tree_item_right_click,
            'on_tree_item_middle_click': self._on_tree_item_middle_click,
            'on_tree_sel_changed': self._on_tree_sel_changed,
            'on_tree_sel_changing': self._on_tree_sel_changing,
            'on_tree_key_down': self._on_tree_key_down,
            'on_tree_item_gettooltip': self._on_tree_item_gettooltip,
            'on_tree_item_menu': self._on_tree_item_menu,
            'on_tree_state_image_click': self._on_tree_state_image_click
        }

    @local_treectrl_kwargs.setter
    def local_treectrl_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_tree_begin_drag = kwargs.get('on_tree_begin_drag', self._on_tree_begin_drag)
        self._on_tree_begin_rdrag = kwargs.get('on_tree_begin_rdrag', self._on_tree_begin_rdrag)
        self._on_tree_end_drag = kwargs.get('on_tree_end_drag', self._on_tree_end_drag)
        self._on_tree_begin_label_edit = kwargs.get('on_tree_begin_label_edit', self._on_tree_begin_label_edit)
        self._on_tree_end_label_edit = kwargs.get('on_tree_end_label_edit', self._on_tree_end_label_edit)
        self._on_tree_delete_item = kwargs.get('on_tree_delete_item', self._on_tree_delete_item)
        self._on_tree_get_info = kwargs.get('on_tree_get_info', self._on_tree_get_info)
        self._on_tree_set_info = kwargs.get('on_tree_set_info', self._on_tree_set_info)
        self._on_tree_item_activated = kwargs.get('on_tree_item_activated', self._on_tree_item_activated)
        self._on_tree_item_collapsed = kwargs.get('on_tree_item_collapsed', self._on_tree_item_collapsed)
        self._on_tree_item_collapsing = kwargs.get('on_tree_item_collapsing', self._on_tree_item_collapsing)
        self._on_tree_item_expanded = kwargs.get('on_tree_item_expanded', self._on_tree_item_expanded)
        self._on_tree_item_expanding = kwargs.get('on_tree_item_expanding', self._on_tree_item_expanding)
        self._on_tree_item_right_click = kwargs.get('on_tree_item_right_click', self._on_tree_item_right_click)
        self._on_tree_item_middle_click = kwargs.get('on_tree_item_middle_click', self._on_tree_item_middle_click)
        self._on_tree_sel_changed = kwargs.get('on_tree_sel_changed', self._on_tree_sel_changed)
        self._on_tree_sel_changing = kwargs.get('on_tree_sel_changing', self._on_tree_sel_changing)
        self._on_tree_key_down = kwargs.get('on_tree_key_down', self._on_tree_key_down)
        self._on_tree_item_gettooltip = kwargs.get('on_tree_item_gettooltip', self._on_tree_item_gettooltip)
        self._on_tree_item_menu = kwargs.get('on_tree_item_menu', self._on_tree_item_menu)
        self._on_tree_state_image_click = kwargs.get('on_tree_state_image_click', self._on_tree_state_image_click)

    @property
    def local_tree_events(self):
        return {
            'on_tree_begin_drag': (self._on_tree_begin_drag, 'wx.EVT_TREE_BEGIN_DRAG',
                                   'wxEVT_TREE_BEGIN_DRAG'),
            'on_tree_begin_rdrag': (self._on_tree_begin_rdrag, 'wx.EVT_TREE_BEGIN_RDRAG',
                                    'wxEVT_TREE_BEGIN_RDRAG'),
            'on_tree_end_drag': (self._on_tree_end_drag, 'wx.EVT_TREE_END_DRAG',
                                 'wxEVT_TREE_END_DRAG'),
            'on_tree_begin_label_edit': (self._on_tree_begin_label_edit, 'wx.EVT_TREE_BEGIN_LABEL_EDIT',
                                         'wxEVT_TREE_BEGIN_LABEL_EDIT'),
            'on_tree_end_label_edit': (self._on_tree_end_label_edit, 'wx.EVT_TREE_END_LABEL_EDIT',
                                       'wxEVT_TREE_END_LABEL_EDIT'),
            'on_tree_delete_item': (self._on_tree_delete_item, 'wx.EVT_TREE_DELETE_ITEM',
                                    'wxEVT_TREE_DELETE_ITEM'),
            'on_tree_get_info': (self._on_tree_get_info, 'wx.EVT_TREE_GET_INFO', 'wxEVT_TREE_GET_INFO'),
            'on_tree_set_info': (self._on_tree_set_info, 'wx.EVT_TREE_SET_INFO', 'wxEVT_TREE_SET_INFO'),
            'on_tree_item_activated': (self._on_tree_item_activated, 'wx.EVT_TREE_ITEM_ACTIVATED',
                                       'wxEVT_TREE_ITEM_ACTIVATED'),
            'on_tree_item_collapsed': (self._on_tree_item_collapsed, 'wx.EVT_TREE_ITEM_COLLAPSED',
                                       'wxEVT_TREE_ITEM_COLLAPSED'),
            'on_tree_item_collapsing': (self._on_tree_item_collapsing, 'wx.EVT_TREE_ITEM_COLLAPSING',
                                        'wxEVT_TREE_ITEM_COLLAPSING'),
            'on_tree_item_expanded': (self._on_tree_item_expanded, 'wx.EVT_TREE_ITEM_EXPANDED',
                                      'wxEVT_TREE_ITEM_EXPANDED'),
            'on_tree_item_expanding': (self._on_tree_item_expanding, 'wx.EVT_TREE_ITEM_EXPANDING',
                                       'wxEVT_TREE_ITEM_EXPANDING'),
            'on_tree_item_right_click': (self._on_tree_item_right_click, 'wx.EVT_TREE_ITEM_RIGHT_CLICK',
                                         'wxEVT_TREE_ITEM_RIGHT_CLICK'),
            'on_tree_item_middle_click': (self._on_tree_item_middle_click, 'wx.EVT_TREE_ITEM_MIDDLE_CLICK',
                                          'wxEVT_TREE_ITEM_MIDDLE_CLICK'),
            'on_tree_sel_changed': (self._on_tree_sel_changed, 'wx.EVT_TREE_SEL_CHANGED',
                                    'wxEVT_TREE_SEL_CHANGED'),
            'on_tree_sel_changing': (self._on_tree_sel_changing, 'wx.EVT_TREE_SEL_CHANGING',
                                     'wxEVT_TREE_SEL_CHANGING'),
            'on_tree_key_down': (self._on_tree_key_down, 'wx.EVT_TREE_KEY_DOWN', 'wxEVT_TREE_KEY_DOWN'),
            'on_tree_item_gettooltip': (self._on_tree_item_gettooltip, 'wx.EVT_TREE_ITEM_GETTOOLTIP',
                                        'wxEVT_TREE_ITEM_GETTOOLTIP'),
            'on_tree_item_menu': (self._on_tree_item_menu, 'wx.EVT_TREE_ITEM_MENU', 'wxEVT_TREE_ITEM_MENU'),
            'on_tree_state_image_click': (self._on_tree_state_image_click, 'wx.EVT_TREE_STATE_IMAGE_CLICK',
                                          'wxEVT_TREE_STATE_IMAGE_CLICK'),
        }

    @property
    def events(self):
        value = self.local_tree_events
        value.update(super(TreeControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_treectrl_kwargs
        value.update(super(TreeControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_treectrl_kwargs = kwargs
        super(TreeControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('tree_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxTR_DEFAULT_STYLE',
             "The set of flags that are closest to the defaults for the native control for a particular toolkit."),
            ('wxTR_EDIT_LABELS', "Use this style if you wish the user to be able to edit labels in the tree control."),
            ('wxTR_NO_BUTTONS', "For convenience to document that no buttons are to be drawn."),
            ('wxTR_HAS_BUTTONS', "Use this style to show + and - buttons to the left of parent items."),
            ('wxTR_TWIST_BUTTONS',
             "Selects alternative style of +/ - buttons and shows rotating ('twisting') arrows instead. "
             "Currently this style is only implemented under Microsoft Windows Vista and later Windows versions and is "
             "ignored under the other platforms as enabling it is equivalent to using "
             "SystemThemedControl.EnableSystemTheme ."),
            ('wxTR_NO_LINES', "Use this style to hide vertical level connectors."),
            ('wxTR_FULL_ROW_HIGHLIGHT',
             "Use this style to have the background colour and the selection highlight extend over the entire "
             "horizontal row of the tree control window. (This flag is ignored under Windows unless you specify "
             "TR_NO_LINES as well.)"),
            ('wxTR_LINES_AT_ROOT',
             "Use this style to show lines leading to the root nodes (unless no TR_NO_LINES is also used, in which "
             "case no lines are shown). Note that in the MSW version, if this style is omitted, not only the lines, "
             "but also the button used for expanding the root item is not shown, which can be unexpected, so it is "
             "recommended to always use it."),
            ('wxTR_HIDE_ROOT',
             "Use this style to suppress the display of the root node, effectively causing the first-level nodes to "
             "appear as a series of root nodes."),
            ('wxTR_ROW_LINES', "Use this style to draw a contrasting border between displayed rows."),
            ('wxTR_HAS_VARIABLE_ROW_HEIGHT',
             "Use this style to cause row heights to be just big enough to fit the content. If not set, all rows use "
             "the largest row height. The default is that this flag is unset. Generic only."),
            ('wxTR_SINGLE',
             "For convenience to document that only one item may be selected at a time. Selecting another item causes "
             "the current selection, if any, to be deselected. This is the default."),
            ('wxTR_MULTIPLE',
             "Use this style to allow a range of items to be selected. If a second range is selected, the current "
             "range, if any, is deselected."),
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Tree',
            'type': 'category',
            'help': 'Attributes of tree.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The tree control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the tree control style.'
                }]
        }]
        _event = [{
            'label': 'Tree',
            'type': 'category',
            'help': 'Events of tree control',
            'child': [
                {
                    'label': 'on_tree_begin_drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_begin_drag,
                    'name': 'on_tree_begin_drag',
                    'help': "Begin dragging with the left mouse button. If you want to enable left-dragging you need "
                            "to intercept this event and explicitly call wx.TreeEvent.Allow , as itâ€™s vetoed by "
                            "default. Processes a wxEVT_TREE_BEGIN_DRAG event type."
                },
                {
                    'label': 'on_tree_begin_rdrag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_begin_rdrag,
                    'name': 'on_tree_begin_rdrag',
                    'help': "Begin dragging with the right mouse button. If you want to enable right-dragging you "
                            "need to intercept this event and explicitly call wx.TreeEvent.Allow , as itâ€™s vetoed "
                            "by default. Processes a wxEVT_TREE_BEGIN_RDRAG event type."
                },
                {
                    'label': 'on_tree_end_drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_end_drag,
                    'name': 'on_tree_end_drag',
                    'help': "End dragging with the left or right mouse button. Processes a wxEVT_TREE_END_DRAG "
                            "event type."
                },
                {
                    'label': 'on_tree_begin_label_edit',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_begin_label_edit,
                    'name': 'on_tree_begin_label_edit',
                    'help': "Begin editing a label. This can be prevented by calling Veto(). Processes a "
                            "wxEVT_TREE_BEGIN_LABEL_EDIT event type."
                },
                {
                    'label': 'on_tree_end_label_edit',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_end_label_edit,
                    'name': 'on_tree_end_label_edit',
                    'help': "Finish editing a label. This can be prevented by calling Veto(). Processes a "
                            "wxEVT_TREE_END_LABEL_EDIT event type."
                },
                {
                    'label': 'on_tree_delete_item',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_delete_item,
                    'name': 'on_tree_delete_item',
                    'help': "An item was deleted. Processes a wxEVT_TREE_DELETE_ITEM event type."
                },
                {
                    'label': 'on_tree_get_info',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_get_info,
                    'name': 'on_tree_get_info',
                    'help': "Request information from the application. Processes a wxEVT_TREE_GET_INFO event type."
                },
                {
                    'label': 'on_tree_set_info',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_set_info,
                    'name': 'on_tree_set_info',
                    'help': "Information is being supplied. Processes a wxEVT_TREE_SET_INFO event type."
                },
                {
                    'label': 'on_tree_item_activated',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_activated,
                    'name': 'on_tree_item_activated',
                    'help': "The item has been activated, i.e. chosen by double clicking it with mouse or from "
                            "keyboard. Processes a wxEVT_TREE_ITEM_ACTIVATED event type."
                },
                {
                    'label': 'on_tree_item_collapsed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_collapsed,
                    'name': 'on_tree_item_collapsed',
                    'help': "The item has been collapsed. Processes a wxEVT_TREE_ITEM_COLLAPSED event type."
                },
                {
                    'label': 'on_tree_item_collapsing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_collapsing,
                    'name': 'on_tree_item_collapsing',
                    'help': "The item is being collapsed. This can be prevented by calling Veto(). Processes a "
                            "wxEVT_TREE_ITEM_COLLAPSING event type."
                },
                {
                    'label': 'on_tree_item_expanded',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_expanded,
                    'name': 'on_tree_item_expanded',
                    'help': "The item has been expanded. Processes a wxEVT_TREE_ITEM_EXPANDED event type."
                },
                {
                    'label': 'on_tree_item_expanding',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_expanding,
                    'name': 'on_tree_item_expanding',
                    'help': "The item is being expanded. This can be prevented by calling Veto(). Processes a "
                            "wxEVT_TREE_ITEM_EXPANDING event type."
                },
                {
                    'label': 'on_tree_item_right_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_right_click,
                    'name': 'on_tree_item_right_click',
                    'help': "The user has clicked the item with the right mouse button. Processes a "
                            "wxEVT_TREE_ITEM_RIGHT_CLICK event type."
                },
                {
                    'label': 'on_tree_item_middle_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_middle_click,
                    'name': 'on_tree_item_middle_click',
                    'help': "The user has clicked the item with the middle mouse button. This is only supported by "
                            "the generic control. Processes a wxEVT_TREE_ITEM_MIDDLE_CLICK event type."
                },
                {
                    'label': 'on_tree_sel_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_sel_changed,
                    'name': 'on_tree_sel_changed',
                    'help': "Selection has changed. Processes a wxEVT_TREE_SEL_CHANGED event type."
                },
                {
                    'label': 'on_tree_sel_changing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_sel_changing,
                    'name': 'on_tree_sel_changing',
                    'help': "Selection is changing. This can be prevented by calling Veto(). Processes a "
                            "wxEVT_TREE_SEL_CHANGING event type."
                },
                {
                    'label': 'on_tree_key_down',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_key_down,
                    'name': 'on_tree_key_down',
                    'help': "A key has been pressed. Processes a wxEVT_TREE_KEY_DOWN event type."
                },
                {
                    'label': 'on_tree_item_gettooltip',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_gettooltip,
                    'name': 'on_tree_item_gettooltip',
                    'help': "The opportunity to set the item tooltip is being given to the application "
                            "(call wx.TreeEvent.SetToolTip ). Windows only. Processes a wxEVT_TREE_ITEM_GETTOOLTIP "
                            "event type."
                },
                {
                    'label': 'on_tree_item_menu',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_item_menu,
                    'name': 'on_tree_item_menu',
                    'help': "The context menu for the selected item has been requested, either by a right click or "
                            "by using the menu key. Notice that these events always carry a valid tree item and so are "
                            "not generated when (right) clicking outside of the items area. If you need to handle such "
                            "events, consider using wxEVT_CONTEXT_MENU instead. Processes a wxEVT_TREE_ITEM_MENU event "
                            "type."
                },
                {
                    'label': 'tree_state_image_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tree_state_image_click,
                    'name': 'on_tree_state_image_click',
                    'help': "The state image has been clicked. Processes a wxEVT_TREE_STATE_IMAGE_CLICK event type."
                },
            ],
        }]

        _base_property, _base_event = super(TreeControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_sample_content(self):
        t = self._edition_window
        root_id = t.AddRoot("Root")
        t.AppendItem(root_id, "Node 1")
        node2_id = t.AppendItem(root_id, "Node 2")
        t.AppendItem(node2_id, "Child of node 2")
        t.AppendItem(root_id, "Node 3")
        t.ExpandAll()

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        if self._id == 'wxID_ANY':
            control_id = wx.ID_ANY
        else:
            control_id = identifier(self._id)
        if self._pos == (-1, -1):
            control_position = wx.DefaultPosition
        else:
            control_position = self._pos
        if self._size == (-1, -1):
            control_size = wx.DefaultSize
        else:
            control_size = self._size
        try:
            self._edition_window = uiMockupControl(wx.TreeCtrl)(
                edition_frame, control_id, control_position, control_size, self.style_value)
        except Exception as exception:
            full_message = str(exception)
            index = full_message.index(':')
            message = full_message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            return
        self.add_to_sizer(container, self._edition_window)
        self.create_sample_content()
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/treectrl.h>
        cls = register_cpp_class(self.project, 'wxTreeCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="tree control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/treectrl.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = "{name} = new wxTreeCtrl({owner}, {id_}, {pos}, {size}, {style});\n" .format(
            name=self._name, owner=owner, id_=self._id, pos=self.cc_pos_str,
            size=self.cc_size_str, style=self.cc_style_str)
        code += super(TreeControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root
        owner = window_parent.py_owner_text
        code = '{this} = wx.TreeCtrl({owner}, {id_}, {pos}, {size}, {style})\n'.\
            format(this=this, owner=owner, id_=self.py_id_str, pos=self.py_pos_str, size=self.py_size_str,
                   style=self.py_style_str)
        code += super(TreeControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code


