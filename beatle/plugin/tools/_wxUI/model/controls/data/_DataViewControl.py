import wx
import wx.dataview
from .._WindowControl import WindowControl
from ._CustomDataViewModel import CustomDataViewModel
from beatle.model import cc
from ....lib import register_cpp_class, with_style


@with_style
class DataViewControl(WindowControl):
    """"This class represents a dataview control"""

    style_list = (
        wx.dataview.DV_SINGLE, wx.dataview.DV_MULTIPLE, wx.dataview.DV_ROW_LINES, wx.dataview.DV_HORIZ_RULES,
        wx.dataview.DV_VERT_RULES, wx.dataview.DV_VARIABLE_LINE_HEIGHT, wx.dataview.DV_NO_HEADER
    )

    style_list_py_text = (
        'wx.dataview.DV_SINGLE', 'wx.dataview.DV_MULTIPLE', 'wx.dataview.DV_ROW_LINES', 'wx.dataview.DV_HORIZ_RULES',
        'wx.dataview.DV_VERT_RULES', 'wx.dataview.DV_VARIABLE_LINE_HEIGHT', 'wx.dataview.DV_NO_HEADER',
    )

    style_list_cc_text = (
        'wxDV_SINGLE', 'wxDV_MULTIPLE', 'wxDV_ROW_LINES', 'wxDV_HORIZ_RULES',
        'wxDV_VERT_RULES', 'wxDV_VARIABLE_LINE_HEIGHT', 'wxDV_NO_HEADER',
    )

    def __init__(self, **kwargs):
        self._style = 1
        # events
        self._on_dataview_selection_changed = ''
        self._on_dataview_item_activated = ''
        self._on_dataview_item_start_editing = ''
        self._on_dataview_item_editing_started = ''
        self._on_dataview_item_editing_done = ''
        self._on_dataview_item_collapsing = ''
        self._on_dataview_item_collapsed = ''
        self._on_dataview_item_expanding = ''
        self._on_dataview_item_expanded = ''
        self._on_dataview_item_value_changed = ''
        self._on_dataview_item_context_menu = ''
        self._on_dataview_column_header_click = ''
        self._on_dataview_column_header_right_click = ''
        self._on_dataview_column_sorted = ''
        self._on_dataview_column_reordered = ''
        self._on_dataview_item_begin_drag = ''
        self._on_dataview_item_drop_possible = ''
        self._on_dataview_item_drop = ''
        super(DataViewControl, self).__init__(**kwargs)

    @property
    def local_dataviewctrl_kwargs(self):
        return {
            'style': self._style,
            # events
            'on_dataview_selection_changed': self._on_dataview_selection_changed,
            'on_dataview_item_activated': self._on_dataview_item_activated,
            'on_dataview_item_start_editing': self._on_dataview_item_start_editing,
            'on_dataview_item_editing_started': self._on_dataview_item_editing_started,
            'on_dataview_item_editing_done': self._on_dataview_item_editing_done,
            'on_dataview_item_collapsing': self._on_dataview_item_collapsing,
            'on_dataview_item_collapsed': self._on_dataview_item_collapsed,
            'on_dataview_item_expanding': self._on_dataview_item_expanding,
            'on_dataview_item_expanded': self._on_dataview_item_expanded,
            'on_dataview_item_value_changed': self._on_dataview_item_value_changed,
            'on_dataview_item_context_menu': self._on_dataview_item_context_menu,
            'on_dataview_column_header_click': self._on_dataview_column_header_click,
            'on_dataview_column_header_right_click': self._on_dataview_column_header_right_click,
            'on_dataview_column_sorted': self._on_dataview_column_sorted,
            'on_dataview_column_reordered': self._on_dataview_column_reordered,
            'on_dataview_item_begin_drag': self._on_dataview_item_begin_drag,
            'on_dataview_item_drop_possible': self._on_dataview_item_drop_possible,
            'on_dataview_item_drop': self._on_dataview_item_drop,
        }

    @local_dataviewctrl_kwargs.setter
    def local_dataviewctrl_kwargs(self, kwargs):
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_dataview_selection_changed = kwargs.get(
            'on_dataview_selection_changed', self._on_dataview_selection_changed)
        self._on_dataview_item_activated = kwargs.get('on_dataview_item_activated', self._on_dataview_item_activated)
        self._on_dataview_item_start_editing = kwargs.get(
            'on_dataview_item_start_editing', self._on_dataview_item_start_editing)
        self._on_dataview_item_editing_started = kwargs.get(
            'on_dataview_item_editing_started', self._on_dataview_item_editing_started)
        self._on_dataview_item_editing_done = kwargs.get(
            'on_dataview_item_editing_done', self._on_dataview_item_editing_done)
        self._on_dataview_item_collapsing = kwargs.get('on_dataview_item_collapsing', self._on_dataview_item_collapsing)
        self._on_dataview_item_collapsed = kwargs.get('on_dataview_item_collapsed', self._on_dataview_item_collapsed)
        self._on_dataview_item_expanding = kwargs.get('on_dataview_item_expanding', self._on_dataview_item_expanding)
        self._on_dataview_item_expanded = kwargs.get('on_dataview_item_expanded', self._on_dataview_item_expanded)
        self._on_dataview_item_value_changed = kwargs.get(
            'on_dataview_item_value_changed', self._on_dataview_item_value_changed)
        self._on_dataview_item_context_menu = kwargs.get(
            'on_dataview_item_context_menu', self._on_dataview_item_context_menu)
        self._on_dataview_column_header_click = kwargs.get(
            'on_dataview_column_header_click', self._on_dataview_column_header_click)
        self._on_dataview_column_header_right_click = kwargs.get(
            'on_dataview_column_header_right_click', self._on_dataview_column_header_right_click)
        self._on_dataview_column_sorted = kwargs.get('on_dataview_column_sorted', self._on_dataview_column_sorted)
        self._on_dataview_column_reordered = kwargs.get(
            'on_dataview_column_reordered', self._on_dataview_column_reordered)
        self._on_dataview_item_begin_drag = kwargs.get(
            'on_dataview_item_begin_drag', self._on_dataview_item_begin_drag)
        self._on_dataview_item_drop_possible = kwargs.get(
            'on_dataview_item_drop_possible', self._on_dataview_item_drop_possible)
        self._on_dataview_item_drop = kwargs.get('on_dataview_item_drop', self._on_dataview_item_drop)

    @property
    def local_data_view_events(self):
        return {
            'on_dataview_selection_changed': (
                self._on_dataview_selection_changed, 'wx.EVT_DATAVIEW_SELECTION_CHANGED',
                'wxEVT_DATAVIEW_SELECTION_CHANGED'),
            'on_dataview_item_activated': (
                self._on_dataview_item_activated, 'wx.EVT_DATAVIEW_ITEM_ACTIVATED',
                'wxEVT_DATAVIEW_ITEM_ACTIVATED'),
            'on_dataview_item_start_editing': (
                self._on_dataview_item_start_editing, 'wx.EVT_DATAVIEW_ITEM_START_EDITING',
                'wxEVT_DATAVIEW_ITEM_START_EDITING'),
            'on_dataview_item_editing_started': (
                self._on_dataview_item_editing_started, 'wx.EVT_DATAVIEW_ITEM_EDITING_STARTED',
                'wxEVT_DATAVIEW_ITEM_EDITING_STARTED'),
            'on_dataview_item_editing_done': (
                self._on_dataview_item_editing_done, 'wx.EVT_DATAVIEW_ITEM_EDITING_DONE',
                'wxEVT_DATAVIEW_ITEM_EDITING_DONE'),
            'on_dataview_item_collapsing': (
                self._on_dataview_item_collapsing, 'wx.EVT_DATAVIEW_ITEM_COLLAPSING',
                'wxEVT_DATAVIEW_ITEM_COLLAPSING'),
            'on_dataview_item_collapsed': (
                self._on_dataview_item_collapsed, 'wx.EVT_DATAVIEW_ITEM_COLLAPSED',
                'wxEVT_DATAVIEW_ITEM_COLLAPSED'),
            'on_dataview_item_expanding': (
                self._on_dataview_item_expanding, 'wx.EVT_DATAVIEW_ITEM_EXPANDING',
                'wxEVT_DATAVIEW_ITEM_EXPANDING'),
            'on_dataview_item_expanded': (
                self._on_dataview_item_expanded, 'wx.EVT_DATAVIEW_ITEM_EXPANDED',
                'wxEVT_DATAVIEW_ITEM_EXPANDED'),
            'on_dataview_item_value_changed': (
                self._on_dataview_item_value_changed, 'wx.EVT_DATAVIEW_ITEM_VALUE_CHANGED',
                'wxEVT_DATAVIEW_ITEM_VALUE_CHANGED'),
            'on_dataview_item_context_menu': (
                self._on_dataview_item_context_menu, 'wx.EVT_DATAVIEW_ITEM_CONTEXT_MENU',
                'wxEVT_DATAVIEW_ITEM_CONTEXT_MENU'),
            'on_dataview_column_header_click': (
                self._on_dataview_column_header_click, 'wx.EVT_DATAVIEW_COLUMN_HEADER_CLICK',
                'wxEVT_DATAVIEW_COLUMN_HEADER_CLICK'),
            'on_dataview_column_header_right_click': (
                self._on_dataview_column_header_right_click, 'wx.EVT_DATAVIEW_COLUMN_HEADER_RIGHT_CLICK',
                'wxEVT_DATAVIEW_COLUMN_HEADER_RIGHT_CLICK'),
            'on_dataview_column_sorted': (
                self._on_dataview_column_sorted, 'wx.EVT_DATAVIEW_COLUMN_SORTED',
                'wxEVT_DATAVIEW_COLUMN_SORTED'),
            'on_dataview_column_reordered': (
                self._on_dataview_column_reordered, 'wx.EVT_DATAVIEW_COLUMN_REORDERED',
                'wxEVT_DATAVIEW_COLUMN_REORDERED'),
            'on_dataview_item_begin_drag': (
                self._on_dataview_item_begin_drag, 'wx.EVT_DATAVIEW_ITEM_BEGIN_DRAG',
                'wxEVT_DATAVIEW_ITEM_BEGIN_DRAG'),
            'on_dataview_item_drop_possible': (
                self._on_dataview_item_drop_possible, 'wx.EVT_DATAVIEW_ITEM_DROP_POSSIBLE',
                'wxEVT_DATAVIEW_ITEM_DROP_POSSIBLE'),
            'on_dataview_item_drop': (
                self._on_dataview_item_drop, 'wx.EVT_DATAVIEW_ITEM_DROP',
                'wxEVT_DATAVIEW_ITEM_DROP'),
        }

    @property
    def events(self):
        value = self.local_data_view_events
        value.update(super(DataViewControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_dataviewctrl_kwargs
        value.update(super(DataViewControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_dataviewctrl_kwargs = kwargs
        super(DataViewControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('data_view_control')

    @property
    def style_attribute_options(self):
        return [
            ('wxDV_SINGLE', "Single selection mode."),
            ('wxDV_MULTIPLE', "Multiple selection mode."),
            ('wxDV_ROW_LINES', "Use alternating colours for odd and even rows."),
            ('wxDV_HORIZ_RULES', "Display the separator lines between rows."),
            ('wxDV_VERT_RULES', "Display the separator lines between columns."),
            ('wxDV_VARIABLE_LINE_HEIGHT',
             "Allow variable line heights. This can be inefficient when displaying large number of items."),
            ('wxDV_NO_HEADER', "Do not show column headers (which are shown by default).")
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Data view',
            'type': 'category',
            'help': 'Attributes of data view.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The dataview control name."
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets the data view control style.'
                }]
        }]
        _event = [{
            'label': 'DataViewControl',
            'type': 'category',
            'help': 'Events of data view control',
            'child': [
                {
                    'label': 'on_dataview_selection_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_selection_changed,
                    'name': 'on_dataview_selection_changed',
                    'help': "Process a wxEVT_DATAVIEW_SELECTION_CHANGED event.",
                },
                {
                    'label': 'on_dataview_item_activated',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_activated,
                    'name': 'on_dataview_item_activated',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_ACTIVATED event. This event is triggered by double clicking "
                            "an item or pressing some special key (usually â€œEnterâ€) when it is focused.",
                },
                {
                    'label': 'on_dataview_item_start_editing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_start_editing,
                    'name': 'on_dataview_item_start_editing',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_START_EDITING event. This event can be vetoed in order to "
                            "prevent editing on an item by item basis.",
                },
                {
                    'label': 'on_dataview_item_editing_started',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_editing_started,
                    'name': 'on_dataview_item_editing_started',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_EDITING_STARTED event.",
                },
                {
                    'label': 'on_dataview_item_editing_done',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_editing_done,
                    'name': 'on_dataview_item_editing_done',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_EDITING_DONE event.",
                },
                {
                    'label': 'on_dataview_item_collapsing',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_collapsing,
                    'name': 'on_dataview_item_collapsing',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_COLLAPSING event.",
                },
                {
                    'label': 'on_dataview_item_collapsed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_collapsed,
                    'name': 'on_dataview_item_collapsed',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_COLLAPSED event.",
                },
                {
                    'label': 'on_dataview_item_expanding',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_expanding,
                    'name': 'on_dataview_item_expanding',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_EXPANDING event.",
                },
                {
                    'label': 'on_dataview_item_expanded',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_expanded,
                    'name': 'on_dataview_item_expanded',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_EXPANDED event.",
                },
                {
                    'label': 'on_dataview_item_value_changed',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_value_changed,
                    'name': 'on_dataview_item_value_changed',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_VALUE_CHANGED event.",
                },
                {
                    'label': 'on_dataview_item_context_menu',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_context_menu,
                    'name': 'on_dataview_item_context_menu',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_CONTEXT_MENU event generated when the user right clicks "
                            "inside the control. Notice that this menu is generated even if the click didnâ€™t occur "
                            "on any valid item, in this case wx.dataview.DataViewEvent.GetItem simply returns an "
                            "invalid item.",
                },
                {
                    'label': 'on_dataview_column_header_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_column_header_click,
                    'name': 'on_dataview_column_header_click',
                    'help': "Process a wxEVT_DATAVIEW_COLUMN_HEADER_CLICK event.",
                },
                {
                    'label': 'on_dataview_column_header_right_click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_column_header_right_click,
                    'name': 'on_dataview_column_header_right_click',
                    'help': "Process a wxEVT_DATAVIEW_COLUMN_HEADER_RIGHT_CLICK event. Notice that currently this "
                            "event is not generated in the native macOS versions of the control.",
                },
                {
                    'label': 'on_dataview_column_sorted',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_column_sorted,
                    'name': 'on_dataview_column_sorted',
                    'help': "Process a wxEVT_DATAVIEW_COLUMN_SORTED event.",
                },
                {
                    'label': 'on_dataview_column_reordered',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_column_reordered,
                    'name': 'on_dataview_column_reordered',
                    'help': "Process a wxEVT_DATAVIEW_COLUMN_REORDERED event.",
                },
                {
                    'label': 'on_dataview_item_begin_drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_begin_drag,
                    'name': 'on_dataview_item_begin_drag',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_BEGIN_DRAG event which is generated when the user starts "
                            "dragging a valid item. This event must be processed and "
                            "wxDataViewEvent.SetDataObject must be called to actually start dragging the item.",
                },
                {
                    'label': 'on_dataview_item_drop_possible',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_drop_possible,
                    'name': 'on_dataview_item_drop_possible',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_DROP_POSSIBLE event.",
                },
                {
                    'label': 'on_dataview_item_drop',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_dataview_item_drop,
                    'name': 'on_dataview_item_drop',
                    'help': "Process a wxEVT_DATAVIEW_ITEM_DROP event.",
                },
            ],
        }]
        _base_property, _base_event = super(DataViewControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!
        try:
            self._edition_window = uiMockupControl(wx.dataview.DataViewCtrl)(
                edition_frame, self.py_id, self.py_pos, self.py_size, self.style_value)
            self._edition_window.AssociateModel(CustomDataViewModel())
            # add tree pseudo controls before adding to sizer!
            for child in self.sorted_wxui_child:
                child.create_edition_window(self)
            self.add_to_sizer(container, self._edition_window)

        except Exception as exception:
            message = str(exception)
            if ':' in  message:
                index = message.index(':')
                message = message[index+1:].strip()
            wx.MessageBox(message, 'Error', wx.OK|wx.ICON_ERROR)
            if self._edition_window:
                self.destroy_edition_window()
            raise exception
        self._edition_window.bind(self)
        self._edition_window.PostSizeEvent()

    def destroy_edition_window(self):
        """We must specialize this in order to properly destroy columns"""
        reverse_sorted = reversed(self.sorted_wxui_child)
        for child in reverse_sorted:
            child.destroy_edition_window()
        super(DataViewControl, self).destroy_edition_window()

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/dataview.h>
        cls = register_cpp_class(self.project, 'wxDataViewCtrl')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="dataview control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/dataview.h>')
        window_parent = self.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        code = "{name} = new wxDataViewCtrl({owner}, {id_}, {pos}, {size}, {style});\n" .format(
            name=self._name, owner=owner, id_=self._id, pos=self.cc_pos_str,
            size=self.cc_size_str, style=self.cc_style_str)
        code += super(DataViewControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        self.add_python_import('wx.dataview')
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = 'self.{name}'.format(name=window_parent.name)

        code = '{this} = wx.dataview.DataViewCtrl({owner}, {id_}, {pos},{size}, {style})\n'.\
            format(this=this, owner=owner, id_=self.py_id_str, pos=self.py_pos_str, size=self.py_size_str,
                   style=self.py_style_str)
        code += super(DataViewControl, self).python_init_control_code(parent)
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += self.py_add_to_sizer_code(parent, this)
        return code

