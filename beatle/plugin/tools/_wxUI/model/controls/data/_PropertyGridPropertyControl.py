import wx.propgrid as pg
from .._WindowControl import WindowControl
from ._PropertyGridPageControl import PropertyGridPageControl
from beatle.model import cc, py
from ....lib import register_cpp_class


class PropertyGridPropertyControl(WindowControl):
    """"This class represents a property grid property"""
    property_name_counter = 0

    def __init__(self, **kwargs):
        PropertyGridPropertyControl.property_name_counter += 1
        self._label = 'Name_{}'.format(PropertyGridPropertyControl.property_name_counter)
        self._type = 'StringProperty'
        super(PropertyGridPropertyControl, self).__init__(**kwargs)
        self.set_kwargs(kwargs)

    @property
    def local_propertygridproperty_kwargs(self):
        return {
            'label': self._label,
            'type': self._type,
        }

    @local_propertygridproperty_kwargs.setter
    def local_propertygridproperty_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._type = kwargs.get('type', self._type)

    @property
    def kwargs(self):
        value = self.local_propertygridproperty_kwargs
        value.update(super(PropertyGridPropertyControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        """Set the property grid property attributes."""
        self.local_propertygridproperty_kwargs = kwargs
        super(PropertyGridPropertyControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('property_grid_item')

    @property
    def property_types(self):
        """Return the list of available property types"""
        return [
            'ArrayStringProperty', 'BoolProperty', 'ColourProperty', 'CursorProperty', 'DateProperty', 'DirProperty',
            'EditEnumProperty', 'EnumProperty', 'FileProperty', 'FlagsProperty', 'FloatProperty',
            'FontProperty', 'ImageFileProperty', 'IntProperty', 'LongStringProperty', 'MultiChoiceProperty',
            'PropertyCategory', 'StringProperty', 'SystemColourProperty',  'UIntProperty',
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Property-grid property',
            'type': 'category',
            'help': 'Attributes of property-grid property',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The property name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': "The property label."
                },
                {
                    'label': 'type',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._type,
                    'name': 'type',  # kwarg value
                    'choices': self.property_types,
                    'help': "The property type."
                },
            ]
        },]
        _event = []
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        if self._type == 'ArrayStringProperty':
            self._edition_window = container.Append(pg.ArrayStringProperty(self._label, value=[
                'string 1', 'string 2', 'string 3']))
        elif self._type == 'BoolProperty':
            self._edition_window = container.Append(pg.BoolProperty(self._label, self._label))
        elif self._type == 'ColourProperty':
            self._edition_window = container.Append(pg.ColourProperty(self._label, self._label))
        elif self._type == 'CursorProperty':
            self._edition_window = container.Append(pg.CursorProperty(self._label, self._label))
        elif self._type == 'DateProperty':
            self._edition_window = container.Append(pg.DateProperty(self._label, self._label))
        elif self._type == 'DirProperty':
            self._edition_window = container.Append(pg.DirProperty(self._label, self._label))
        elif self._type == 'EditEnumProperty':
            self._edition_window = container.Append(pg.EditEnumProperty(self._label, self._label))
        elif self._type == 'EnumProperty':
            self._edition_window = container.Append(pg.EnumProperty(self._label, self._label))
        elif self._type == 'FileProperty':
            self._edition_window = container.Append(pg.FileProperty(self._label, self._label))
        elif self._type == 'FlagsProperty':
            self._edition_window = container.Append(pg.FlagsProperty(self._label, self._label))
        elif self._type == 'FloatProperty':
            self._edition_window = container.Append(pg.FloatProperty(self._label, self._label))
        elif self._type == 'FontProperty':
            self._edition_window = container.Append(pg.FontProperty(self._label, self._label))
        elif self._type == 'ImageFileProperty':
            self._edition_window = container.Append(pg.ImageFileProperty(self._label, self._label))
        elif self._type == 'IntProperty':
            self._edition_window = container.Append(pg.IntProperty(self._label, self._label))
        elif self._type == 'LongStringProperty':
            self._edition_window = container.Append(pg.LongStringProperty(self._label, self._label))
        elif self._type == 'MultiChoiceProperty':
            self._edition_window = container.Append(pg.MultiChoiceProperty(
                self._label, self._label, choices=["choice 1", "choice 2", "choice 3"], value=["1", "2", "3"]))
        elif self._type == 'PropertyCategory':
            self._edition_window = container.Append(pg.PropertyCategory(self._label, self._label))
        elif self._type == 'StringProperty':
            self._edition_window = container.Append(pg.StringProperty(self._label, self._label))
        elif self._type == 'SystemColourProperty':
            self._edition_window = container.Append(pg.SystemColourProperty(self._label, self._label))
        elif self._type == 'UIntProperty':
            self._edition_window = container.Append(pg.UIntProperty(self._label, self._label))
        else:
            self._edition_window = None  # can't happen

    def destroy_edition_window(self):
        if self._edition_window is not None:
            container = getattr(self.parent, '_edition_window', None)
            if type(self.parent) is PropertyGridPageControl:
                container.RemoveProperty(self._edition_window)
        self._edition_window = None

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/propgrid/property.h>
        cls = register_cpp_class(self.project, 'wxPGProperty')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="property grid property control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/propgrid/propgrid.h>')
        self.add_cpp_required_header('#include <wx/propgrid/manager.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        inner_code = 'wx{type}("{label}","{label}")'.format(type=self._type, label=self._label)
        code = '{name} = {owner}->Append({inner_code});\n'.format(name=self._name, owner=owner,inner_code=inner_code)
        code += super(PropertyGridPropertyControl, self).cpp_init_code()
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent
        owner = window_parent.py_owner_text
        inner_code = 'wx.propgrid.{type}("{label}","{label}")'.format(type=self._type, label=self._label)
        code = '{this} = {owner}.Append({inner_code});\n'.format(this=this, owner=owner,inner_code=inner_code)
        code += super(PropertyGridPropertyControl, self).python_init_control_code()
        return code
