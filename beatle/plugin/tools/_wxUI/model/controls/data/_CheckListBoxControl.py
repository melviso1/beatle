import wx
from .._WindowControl import WindowControl
from beatle.lib.handlers import identifier
from beatle.model import cc
from ....lib import register_cpp_class, with_style


@with_style
class CheckListBoxControl(WindowControl):
    """"This class represents a listbox"""

    style_list = (wx.LB_SINGLE, wx.LB_MULTIPLE, wx.LB_EXTENDED, wx.LB_HSCROLL,
                  wx.LB_ALWAYS_SB, wx.LB_NEEDED_SB, wx.LB_NO_SB, wx.LB_SORT)

    style_list_py_text = ('wx.LB_SINGLE', 'wx.LB_MULTIPLE', 'wx.LB_EXTENDED', 'wx.LB_HSCROLL',
                          'wx.LB_ALWAYS_SB', 'wx.LB_NEEDED_SB', 'wx.LB_NO_SB', 'wx.LB_SORT',)

    style_list_cc_text = ('wxLB_SINGLE', 'wxLB_MULTIPLE', 'wxLB_EXTENDED', 'wxLB_HSCROLL',
                          'wxLB_ALWAYS_SB', 'wxLB_NEEDED_SB', 'wxLB_NO_SB', 'wxLB_SORT',)

    def __init__(self, **kwargs):
        self._choices = []
        self._style = 0
        # events
        self._on_toggled = ''
        self._on_select = ''
        self._on_double_click = ''
        super(CheckListBoxControl, self).__init__(**kwargs)


    @property
    def local_checklistboxctrl_kwargs(self):
        return {
            'choices': self._choices,
            'style': self._style,
            # events
            'on_toggled': self._on_toggled,
            'on_select': self._on_select,
            'on_double_click': self._on_double_click,
        }

    @local_checklistboxctrl_kwargs.setter
    def local_checklistboxctrl_kwargs(self, kwargs):
        self._choices = kwargs.get('choices', self._choices)
        self._style = kwargs.get('style', self._style)
        #  events
        self._on_toggled = kwargs.get('on_toggled', self._on_toggled)
        self._on_select = kwargs.get('on_select', self._on_select)
        self._on_double_click = kwargs.get('on_double_click', self._on_double_click)

    @property
    def local_check_list_box_events(self):
        return {
            'on_toggled': (self._on_toggled, 'wx.EVT_CHECKLISTBOX', 'wxEVT_CHECKLISTBOX'),
            'on_select': (self._on_select, 'wx.EVT_LISTBOX', 'wxEVT_LISTBOX'),
            'on_double_click': (self._on_double_click, 'wx.EVT_LISTBOX_DCLICK', 'wxEVT_LISTBOX_DCLICK'),
        }

    @property
    def events(self):
        value = self.local_check_list_box_events
        value.update(super(CheckListBoxControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_checklistboxctrl_kwargs
        value.update(super(CheckListBoxControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_checklistboxctrl_kwargs = kwargs
        super(CheckListBoxControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('check_listbox')

    @property
    def style_attribute_options(self):
        return [
            ('wxLB_SINGLE', "Single - selection list."),
            ('wxLB_MULTIPLE', "Multiple - selection list: the user can toggle multiple items on and off. "
                              "This is the same as wx.LB_EXTENDED in wxGTK2 port."),
            ('wxLB_EXTENDED', "Extended - selection list: the user can extend the selection by using SHIFT or "
                              "CTRL keys together with the cursor movement keys or the mouse."),
            ('wxLB_HSCROLL', "Create horizontal scrollbar if contents are too wide ( Windows only )."),
            ('wxLB_ALWAYS_SB', "Always show a vertical scrollbar."),
            ('wxLB_NEEDED_SB', "Only create a vertical scrollbar if needed."),
            ('wxLB_NO_SB', "Don’t create vertical scrollbar (wxMSW and wxGTK only)."),
            ('wxLB_SORT', "The listbox contents are sorted in alphabetical order.")
        ]

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Check list box',
            'type': 'category',
            'help': 'Attributes of check list box',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The checklistbox control name."
                },
                {
                    'label': 'choices',
                    'type': 'string_array',
                    'read_only': False,
                    'value': self._choices,
                    'name': 'choices',  # kwarg value
                    'help': 'Contents of the checklistbox.',
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._style,
                    'name': 'style',
                    'options': self.style_attribute_options,
                    'help': 'Sets combobox style.'
                }]
        }]
        _event = [{
            'label': 'CheckListBoxControl',
            'type': 'category',
            'help': 'Events of combobox control',
            'child': [
                {
                    'label': 'on toggled',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_toggled,
                    'name': 'on_toggled',  # kwarg value
                    'help': 'Called when an item is checked or unchecked.'
                },
                {
                    'label': 'on select',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_select,
                    'name': 'on_select',  # kwarg value
                    'help': 'Called when an item in the list is selected.'
                },
                {
                    'label': 'on double click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_double_click,
                    'name': 'on_double_click',  # kwarg value
                    'help': 'Called when double click on the listbox.'
                },
            ],
        }]

        _base_property, _base_event = super(CheckListBoxControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container = getattr(self.parent, '_edition_window', None)
        if container is None:
            return
        top_window = self.sizer_root
        if top_window is None:
            return  # weird!!!
        edition_frame = top_window.edition_window_client
        if edition_frame is None:
            return  # weird!!!

        self._edition_window = uiMockupControl(wx.CheckListBox)(
            edition_frame, self.py_id, self.py_pos, self.py_size, self._choices, self.style_value)

        self.add_to_sizer(container, self._edition_window)
        self._edition_window.bind(self)

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/checklst.h>
        cls = register_cpp_class(self.project, 'wxCheckListBox')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="checklistbox control",
            name=self._name,
            type=cc.typeinst(type=cls, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/checklst.h>')
        window_parent = self.parent.sizer_root
        if window_parent.is_top_window:
            owner = 'this'
        else:
            owner = window_parent.name
        choices_list = ', '.join(f'"{x}"' for x in self._choices)
        n = len(self._choices)
        code = f'wxString {self._name}_choices[] = {{ {choices_list} }};\n'
        code += f"{self._name} = new wxCheckListBox({owner}, {self._id}, {self.cc_pos_str}, " \
                f"{self.cc_size_str}, {n}, {self._name}_choices, {self.cc_style_str});\n"
        code += super(CheckListBoxControl, self).cpp_init_code()
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent.sizer_root

        if window_parent.is_top_window:
            owner = 'self'
        else:
            owner = f'self.{window_parent.name}'

        choices_list = ', '.join(f'"{x}"' for x in self._choices)
        code = f'{self._name}_choices = [{choices_list}]\n'
        code += f'{this} = wx.CheckListBox({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
                f'{self.py_size_str}, {self._name}_choices, {self.py_style_str})\n'
        code += super(CheckListBoxControl, self).python_init_control_code(parent)
        code += self.py_add_to_sizer_code(parent, this)
        return code

