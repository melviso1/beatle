from beatle.model import cc
from .._WindowControl import WindowControl
from ._DataViewControl import DataViewControl
from ....lib import register_cpp_class


class DataViewColumnControl(WindowControl):
    """"This class represents a data view control column.
    This is not a real control, but a DataViewColumnControl object,
    that is not really a window nor a control"""

    def __init__(self, **kwargs):
        self._label = 'Name'
        self._model_column = 0
        self._type = 'Text'
        # events
        super(DataViewColumnControl, self).__init__(**kwargs)
        self.set_kwargs(kwargs)

    @property
    def local_dataviewcolumn_kwargs(self):
        return {
            'label': self._label,
            'model_column': self._model_column,
            'type': self._type,
        }

    @local_dataviewcolumn_kwargs.setter
    def local_dataviewcolumn_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._model_column = kwargs.get('model_column', self._model_column)
        self._type = kwargs.get('type', self._type)

    @property
    def kwargs(self):
        value = self.local_dataviewcolumn_kwargs
        value.update(super(DataViewColumnControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_dataviewcolumn_kwargs = kwargs
        super(DataViewColumnControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('data_view_column')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Data view column',
            'type': 'category',
            'help': 'Attributes of data view column.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The data view column name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': "The data view column label."
                },
                {
                    'label': 'model_column',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._model_column,
                    'name': 'model_column',  # kwarg value
                    'help': "The data view model column index."
                },
                {
                    'label': 'type',
                    'type': 'choice',
                    'value': self._type,
                    'name': 'type',
                    'help': 'The data view column type.',
                    'choices': ['Bitmap', 'Date', 'IconText', 'Progress', 'Text', 'Toggle'],
                }]
        }]
        _event = []

        return _property, _event

    def create_edition_window(self, parent):
        container = getattr(self.parent, '_edition_window', None)
        if container is None or type(self.parent) is not DataViewControl:
            return
        type_map = {
            'Bitmap': container.AppendBitmapColumn,
            'Date': container.AppendDateColumn,
            'IconText': container.AppendIconTextColumn,
            'Progress': container.AppendProgressColumn,
            'Text': container.AppendTextColumn,
            'Toggle': container.AppendToggleColumn,
        }
        if self._type not in type_map:
            raise ValueError('invalid column type {}'.format(self._type))
        self._edition_window = type_map[self._type](self._label, self._model_column)

    def destroy_edition_window(self):
        if self._edition_window is not None:
            container = getattr(self.parent, '_edition_window', None)
            if type(self.parent) is DataViewControl:
                container.DeleteColumn(self._edition_window)
        self._edition_window = None

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/dataview.h>
        class_ = register_cpp_class(self.project, 'wxDataViewColumn')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="dataview column control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/dataview.h>')
        if self._type == 'Bitmap':
            code = '{name} = {parent}->AppendBitmapColumn("{label}", {col} );\n'.format(
                name=self.name, parent=self.parent.name, label=self._label, col=self._model_column
            )
        elif self._type == 'Date':
            code = '{name} = {parent}->AppendDateColumn("{label}", {col} );\n'.format(
                name=self.name, parent=self.parent.name, label=self._label, col=self._model_column
            )
        elif self._type == 'IconText':
            code = '{name} = {parent}->AppendIconTextColumn("{label}", {col} );\n'.format(
                name=self.name, parent=self.parent.name, label=self._label, col=self._model_column
            )
        elif self._type == 'Progress':
            code = '{name} = {parent}->AppendProgressColumn("{label}", {col} );\n'.format(
                name=self.name, parent=self.parent.name, label=self._label, col=self._model_column
            )
        elif self._type == 'Text':
            code = '{name} = {parent}->AppendTextColumn("{label}", {col} );\n'.format(
                name=self.name, parent=self.parent.name, label=self._label, col=self._model_column
            )
        elif self._type == 'Toggle':
            code = '{name} = {parent}->AppendToggleColumn("{label}", {col} );\n'.format(
                name=self.name, parent=self.parent.name, label=self._label, col=self._model_column
            )
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent
        owner = window_parent.py_owner_text
        if self._type == 'Bitmap':
            code = '{this} = {owner}.AppendBitmapColumn("{label}", {col} );\n'.format(
                this=this, owner=owner, label=self._label, col=self._model_column
            )
        elif self._type == 'Date':
            code = '{this} = {owner}.AppendDateColumn("{label}", {col} );\n'.format(
                this=this, owner=owner, label=self._label, col=self._model_column
            )
        elif self._type == 'IconText':
            code = '{this} = {owner}.AppendIconTextColumn("{label}", {col} );\n'.format(
                this=this, owner=owner, label=self._label, col=self._model_column
            )
        elif self._type == 'Progress':
            code = '{this} = {owner}.AppendProgressColumn("{label}", {col} );\n'.format(
                this=this, owner=owner, label=self._label, col=self._model_column
            )
        elif self._type == 'Text':
            code = '{this} = {owner}.AppendTextColumn("{label}", {col} );\n'.format(
                this=this, owner=owner, label=self._label, col=self._model_column
            )
        elif self._type == 'Toggle':
            code = '{this} = {owner}.AppendToggleColumn("{label}", {col} );\n'.format(
                this=this, owner=owner, label=self._label, col=self._model_column
            )
        return code

