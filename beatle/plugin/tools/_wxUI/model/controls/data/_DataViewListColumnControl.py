from beatle.model import cc
from .._WindowControl import WindowControl
from ._DataViewListControl import DataViewListControl
from ....lib import register_cpp_class


class DataViewListColumnControl(WindowControl):
    """"This class represents a data view list control column.
    This is not a real control, but a DataViewListColumnControl object,
    that is not really a window nor a control"""

    def __init__(self, **kwargs):
        self._label = 'Name'
        self._type = 'Text'
        # events
        super(DataViewListColumnControl, self).__init__(**kwargs)
        self.set_kwargs(kwargs)

    @property
    def local_dataviewlistcolumn_kwargs(self):
        return {
            'label': self._label,
            'type': self._type,
        }

    @local_dataviewlistcolumn_kwargs.setter
    def local_dataviewlistcolumn_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._type = kwargs.get('type', self._type)

    @property
    def kwargs(self):
        value = self.local_dataviewlistcolumn_kwargs
        value.update(super(DataViewListColumnControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_dataviewlistcolumn_kwargs = kwargs
        super(DataViewListColumnControl, self).set_kwargs(kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('data_view_list_column')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Data view list column',
            'type': 'category',
            'help': 'Attributes of data view list column',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The data view list column name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': "The data view list column label."
                },
                {
                    'label': 'type',
                    'type': 'choice',
                    'value': self._type,
                    'name': 'type',
                    'help': 'The data view list column type.',
                    'choices': ['IconText', 'Progress', 'Text', 'Toggle'],
                }]
        }]
        _event = []

        return _property, _event

    def create_edition_window(self, parent):
        container = getattr(self.parent, '_edition_window', None)
        if container is None or type(self.parent) is not DataViewListControl:
            return
        type_map = {
            'IconText': container.AppendIconTextColumn,
            'Progress': container.AppendProgressColumn,
            'Text': container.AppendTextColumn,
            'Toggle': container.AppendToggleColumn,
        }
        if self._type not in type_map:
            raise ValueError('invalid column type {}'.format(self._type))
        self._edition_window = type_map[self._type](self._label)

    def destroy_edition_window(self):
        if self._edition_window is not None:
            container = getattr(self.parent, '_edition_window', None)
            if type(self.parent) is DataViewListControl:
                container.DeleteColumn(self._edition_window)
        self._edition_window = None

    def declare_cpp_variable(self, container):
        """Declare a variable with sanitized name.
        This method is invoked from the container in order
        to declare the variable that will hold the instance."""
        # requires #include <wx/dataview.h>
        class_ = register_cpp_class(self.project, 'wxDataViewColumn')
        # add a controls folder in order to avoid garbage
        self._implementation = cc.MemberData(
            parent=container,
            read_only=True,
            note="dataview list column control",
            name=self._name,
            type=cc.typeinst(type=class_, ptr=True),
            access='private',
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/dataview.h>')
        if self._type == 'IconText':
            code = '{name} = {parent}->AppendIconTextColumn("{label}");\n'.format(
                name=self.name, parent=self.parent.name, label=self._label
            )
        elif self._type == 'Progress':
            code = '{name} = {parent}->AppendProgressColumn("{label}");\n'.format(
                name=self.name, parent=self.parent.name, label=self._label
            )
        elif self._type == 'Text':
            code = '{name} = {parent}->AppendTextColumn("{label}");\n'.format(
                name=self.name, parent=self.parent.name, label=self._label
            )
        elif self._type == 'Toggle':
            code = '{name} = {parent}->AppendToggleColumn("{label}");\n'.format(
                name=self.name, parent=self.parent.name, label=self._label
            )
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self.name)
        window_parent = self.parent
        owner = window_parent.py_owner_text
        if self._type == 'IconText':
            code = '{this} = {owner}.AppendIconTextColumn("{label}");\n'.format(
                this=this, owner=owner, label=self._label
            )
        elif self._type == 'Progress':
            code = '{this} = {owner}.AppendProgressColumn("{label}");\n'.format(
                this=this, owner=owner, label=self._label
            )
        elif self._type == 'Text':
            code = '{this} = {owner}.AppendTextColumn("{label}");\n'.format(
                this=this, owner=owner, label=self._label
            )
        elif self._type == 'Toggle':
            code = '{this} = {owner}.AppendToggleColumn("{label}");\n'.format(
                this=this, owner=owner, label=self._label
            )
        return code

