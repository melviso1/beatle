import wx.dataview


class CustomDataViewModel(wx.dataview.DataViewModel):

    """Implement a dummy dataview model required for edition windows."""

    def __init__(self):
        super(CustomDataViewModel, self).__init__()

    def GetChildren(self, item, children):
        return 0

    def GetColumnCount(self):
        return 0

    def GetParent(self, item):
        return wx.dataview.DataViewItem(None)

    def IsContainer(self, item):
        return False

    def GetValue(self, variant, item, col):
        return None

    def SetValue(self, variant, item, col):
        return True

