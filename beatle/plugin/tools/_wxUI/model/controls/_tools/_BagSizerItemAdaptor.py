import wx
from ._SizerItemAdaptor import SizerItemAdaptor


class BagSizerItemAdaptor(SizerItemAdaptor):
    """This class adds extra parameters required when the container
    is a GridBagSizerControl."""

    def __init__(self, **kwargs):
        """Initialize a bag sizer item.
        The child argument represents the control handled by the
        SizerItem."""
        self._row = 0
        self._column = 0
        self._row_span = 1
        self._col_span = 1
        super(BagSizerItemAdaptor, self).__init__(**kwargs)
        # when we add a bag sizer we must initialize the
        # row and column
        container = self.control.parent # this must be a GridBagSizer
        # create a list of objects with sizer_item
        previous = [x for y in container.children for x in container[y] if
                    hasattr(x, 'subitem_adaptor') and type(x.subitem_adaptor) is BagSizerItemAdaptor]
        # get max row
        if len(previous) > 0:
            self._row = max([x.subitem_adaptor.row for x in previous])
            self._column = max([x.subitem_adaptor.column + x.subitem_adaptor.col_span for x in previous
                               if x.subitem_adaptor.row == self._row ])

    @property
    def kwargs(self):
        value = {
            'row': self._row,
            'column': self._column,
            'row_span': self._row_span,
            'col_span': self._col_span,
        }
        value.update(super(BagSizerItemAdaptor, self).kwargs)
        del value['proportion']
        return value

    @kwargs.setter
    def kwargs(self, value):
        self._row = value.get('row', self._row)
        self._column = value.get('column', self._column)
        self._row_span = value.get('row_span', self._row_span)
        self._col_span = value.get('col_span', self._col_span)
        super(BagSizerItemAdaptor, self.__class__).kwargs.fset(self, value)
        if self.control is None:
            return
        edition_window = self.control.edition_window
        if edition_window is None:
            return
        if not self.control.is_sizer:
            return
        parent_window = self.control.parent.edition_window_client
        if parent_window is None:
            return # weird
        sizer_item = parent_window.GetItem(edition_window)
        if 'row' in value or 'column' in value:
            sizer_item.SetPos(wx.GBPosition(self.row, self.column))
        if 'row_span' in value or 'col_span' in value:
            sizer_item.SetSpan(wx.GBSpan(self.row_span, self.col_span))

    @property
    def model_attributes(self):
        _property = [{
                'label': 'Grid bag sizer item',
                'type': 'category',
                'help': 'Attributes of grid bag sizer item.',
                'child': [
                    {
                        'label': 'row',
                        'type': 'integer',
                        'read_only': False,
                        'value': self._row,
                        'name': 'row',  # kwarg value
                        'help': 'row of the grid bag sizer this item is placed.',
                    },
                    {
                        'label': 'column',
                        'type': 'integer',
                        'read_only': False,
                        'value': self._column,
                        'name': 'column',  # kwarg value
                        'help': 'column of the grid bag sizer this item is placed.',
                    },
                    {
                        'label': 'row span',
                        'type': 'integer',
                        'read_only': False,
                        'value': self._row_span,
                        'name': 'row_span',  # kwarg value
                        'help': 'number of rows  of the grid bag sizer this item occupies.',
                    },
                    {
                        'label': 'col span',
                        'type': 'integer',
                        'read_only': False,
                        'value': self._row_span,
                        'name': 'col_span',  # kwarg value
                        'help': 'number of columns  of the grid bag sizer this item occupies.',
                    },
                ]}]
        _event = []
        super_property, super_event = super(BagSizerItemAdaptor, self).model_attributes
        properties = super_property[0]['child']
        del properties[0]  # remove unneeded proportion
        super_property[0]['child'] = properties
        _property += super_property
        _event += super_event
        return _property,_event

    @property
    def row(self):
        return self._row

    @property
    def column(self):
        return self._column

    @property
    def row_span(self):
        return self._row_span

    @property
    def col_span(self):
        return self._col_span

    def Realize(self):
        """Finishes the addition of the edition window to the sizer"""
        client = self.control.edition_window
        if client is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')
        container = self.control.parent.edition_window_client
        if container is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')

        return container.Add(
            client, wx.GBPosition(self.row, self.column), wx.GBSpan(self.row_span, self.col_span),
            self.alignment_value, self.border)

    def cc_realize_code(self, container, window):
        """Return the code required for inserting the element into the sizer in c++"""
        return f'{container}->Add({window}, wxGBPosition({self.row},{self.column}), ' \
               f'wxGBSpan({self.row_span}, {self.col_span}), {self.cc_aligment_str}, {self.border});\n'

    def py_realize_code(self, container, window):
        """Return the code required for inserting the element into the sizer in python"""
        return f'{container}.Add({window}, wx.GBPosition({self.row},{self.column}), ' \
               f'wx.GBSpan({self.row_span}, {self.col_span}), {self.py_aligment_str}, {self.border})\n'
