import wx
from ..._wxUICommon import wxUICommon


class SimplebookContainerAdaptor(wxUICommon):
    """This class acts as a helper for handling the windows that belongs to
    tab container and whose properties that must be 'injected' in a control when his parent
    is a simplebook control."""

    def __init__(self, **kwargs):
        """Initialize a sizer item.
        The control argument represents the control handled by the
        TabControl. Take in consideration that these have no parent"""
        self._tab_label = 'a page'
        kwargs['visibleInTree'] = False
        self._control = kwargs.get('control', None)
        super(SimplebookContainerAdaptor, self).__init__(**kwargs)

    @property
    def control(self):
        return self._control

    @property
    def kwargs(self):
        return {
            'tab_label': self._tab_label,
        }

    @kwargs.setter
    def kwargs(self, value):
        from ..containers._SimplebookControl import SimplebookControl

        self._tab_label = value.get('tab_label', self._tab_label)
        if self.control is None:
            return
        edition_window = self.control.edition_window
        if edition_window is None:
            return
        parent_window = getattr(self.control.parent, '_edition_window', None)
        if not type(parent_window) is SimplebookControl:
            return
        book = parent_window.edition_window
        if book is None:
            return
        index = book.FindPage(self.edition_window)
        if index == wx.NOT_FOUND:
            return
        book.SetPageText(index, self._tab_label)

    @property
    def model_attributes(self):
        _property = [{
                'label': 'Simple book page',
                'type': 'category',
                'help': 'Attributes of simple book page.',
                'child': [
                    {
                        'label': 'tab label',
                        'type': 'string',
                        'read_only': False,
                        'value': self._tab_label,
                        'name': 'tab_label',  # kwarg value
                        'help': 'The tab label of this page.',
                    },
                ]}]
        _event = []
        return _property, _event

    @property
    def tab_label(self):
        return self._tab_label

    @tab_label.setter
    def tab_label(self, value):
        self._tab_label = value

    def detach_from_parent(self):
        """Called during the removal of the edition window"""
        this_window = self.control.edition_window
        if this_window is None:
            return
        book = self.control.parent.edition_window
        if book is None:
            return
        index = book.FindPage(this_window)
        if index != wx.NOT_FOUND:
            book.RemovePage(index)

    def Realize(self):
        client = self.control.edition_window
        if client is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')
        book = self.control.parent.edition_window
        if book is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')
        index = book.AddPage(client, self._tab_label, False)
        return index

    def cc_realize_code(self, container, window):
        """Return the code required for inserting the element into the size in python"""
        return f'{container}->AddPage({window}, "{self._tab_label}", false );\n'

    def py_realize_code(self, container, window):
        """Return the code required for inserting the element into the size in python"""
        return f'{container}.AddPage({window}, "{self._tab_label}", False )\n'

