import wx
from ..._wxUICommon import wxUICommon


class SizerItemAdaptor(wxUICommon):
    """This class acts as a helper for handling the sizeritem
    properties that must be 'injected' in a control when his parent
    is a sizer."""

    def __init__(self, **kwargs):
        """Initialize a sizer item.
        The control argument represents the control handled by the
        SizerItem. As we dont want interference with transactional
        here, these kind of instances have no parent, as they are
        really members of a transactional class."""
        self._proportion = 0
        self._border = 5
        self._flag = 0
        self._control = kwargs.get('control', None)
        kwargs['visibleInTree'] = False
        super(SizerItemAdaptor, self).__init__(**kwargs)

    @property
    def control(self):
        return self._control

    @property
    def kwargs(self):
        return {
            'proportion': self._proportion,
            'border': self._border,
            'flag': self._flag,
        }

    @kwargs.setter
    def kwargs(self, value):
        self._proportion = value.get('proportion', self._proportion)
        self._border = value.get('border', self._border)
        self._flag = value.get('flag', self._flag)
        if self.control is None:
            return
        edition_window = self.control.edition_window
        if edition_window is None:
            return
        if not self.control.is_sizer:
            return
        parent_window = self.control.parent.edition_window_client
        if parent_window is None:
            return # weird
        sizer_item = parent_window.GetItem(edition_window)
        if 'proportion' in value:
            sizer_item.SetProportion(self.proportion)
        if 'border' in value:
            sizer_item.SetBorder(self.border)
        if 'flag' in value:
            sizer_item.SetFlag(self.alignment_value)

        if 'row' in value or 'column' in value:
            sizer_item.SetPos(wx.GBPosition(self.row, self.column))
        if 'row_span' in value or 'col_span' in value:
            sizer_item.SetSpan(wx.GBSpan(self.row_span, self.col_span))

    @property
    def model_attributes(self):
        _property=[{
                'label': 'Sizer item',
                'type': 'category',
                'help': 'Attributes of sizer item.',
                'child': [
                    {
                        'label': 'proportion',
                        'type': 'integer',
                        'read_only': False,
                        'value': self._proportion,
                        'name': 'proportion',  # kwarg value
                        'help': 'stretching factor in container.',
                    },
                    {
                        'label': 'border',
                        'type': 'integer',
                        'read_only': False,
                        'value': self._border,
                        'name': 'border',  # kwarg value
                        'help': 'container border.',
                    },
                    {
                        'label': 'flag',
                        'type': 'multi_choice',
                        'value': self._flag,
                        'name': 'flag',
                        'help': 'The sizer style.',
                        'options': [
                            ('wxALIGN_BOTTOM', 'Align the item to the top of the space.'),
                            ('wxALIGN_CENTER', 'Align the item to the center of the space.'),
                            ('wxALIGN_CENTER_HORIZONTAL', 'Align the item horizontally.'),
                            ('wxALIGN_CENTER_VERTICAL', 'Align the item vertically.'),
                            ('wxALIGN_LEFT', 'Align the item to left.'),
                            ('wxALIGN_RIGHT', 'Align the item to right.'),
                            ('wxALIGN_TOP', 'Align the item to the top.'),
                            ('wxALIGN_ALL', 'Enable all borders.'),
                            ('wxBOTTOM', 'Eanble bottom border.'),
                            ('wxEXPAND', 'Expand the item to available space.'),
                            ('wxFIXED_MINSIZE', 'Set fixed, not computed min size.'),
                            ('wxLEFT', 'Enable left border.'),
                            ('wxRESERVE_SPACE_EVEM_IF_HIDDEN', 'Make the item takes place even invisible.'),
                            ('wxRIGHT', 'Enable right border.'),
                            ('wxSHAPED', 'Expand item maintaining aspect ratio.'),
                            ('wxTOP', 'Enable top border')
                        ]
                    },
                ]}]
        _event=[]
        return _property,_event

    @property
    def proportion(self):
        return self._proportion

    @proportion.setter
    def proportion(self, value):
        self._proportion = value

    @property
    def alignment_value(self):
        """Return a suitable value corresponding to the selected
        alignment, that can be used for creating dynamically a control"""
        align_map = {
            1: wx.ALIGN_BOTTOM, 2: wx.ALIGN_CENTER, 4: wx.ALIGN_CENTER_HORIZONTAL, 8: wx.ALIGN_CENTER_VERTICAL,
            16: wx.ALIGN_LEFT, 32: wx.ALIGN_RIGHT, 64: wx.ALIGN_TOP, 128: wx.ALL, 256: wx.BOTTOM, 512: wx.EXPAND,
            1024: wx.FIXED_MINSIZE, 2048: wx.LEFT, 4096: wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 8192: wx.RIGHT,
            16384: wx.SHAPED, 32768: wx.TOP
        }
        flags = 0
        for index in range(33):
            if self._flag & 2 ** index:
                flags |= align_map[2 ** index]
        return flags

    @property
    def py_aligment_str(self):
        align_map = {
            1: 'wx.ALIGN_BOTTOM', 2: 'wx.ALIGN_CENTER', 4: 'wx.ALIGN_CENTER_HORIZONTAL', 8: 'wx.ALIGN_CENTER_VERTICAL',
            16: 'wx.ALIGN_LEFT', 32: 'wx.ALIGN_RIGHT', 64: 'wx.ALIGN_TOP', 128: 'wx.ALL', 256: 'wx.BOTTOM',
            512: 'wx.EXPAND', 1024: 'wx.FIXED_MINSIZE', 2048: 'wx.LEFT', 4096: 'wx.RESERVE_SPACE_EVEN_IF_HIDDEN',
            8192: 'wx.RIGHT', 16384: 'wx.SHAPED', 32768: 'wx.TOP'
        }
        return '| '.join(align_map[k] for k in align_map.keys() if self._flag & k) or '0'

    @property
    def cc_aligment_str(self):
        align_map = {
            1: 'wxALIGN_BOTTOM', 2: 'wxALIGN_CENTER', 4: 'wxALIGN_CENTER_HORIZONTAL', 8: 'wxALIGN_CENTER_VERTICAL',
            16: 'wxALIGN_LEFT', 32: 'wxALIGN_RIGHT', 64: 'wxALIGN_TOP', 128: 'wxALL', 256: 'wxBOTTOM',
            512: 'wxEXPAND', 1024: 'wxFIXED_MINSIZE', 2048: 'wxLEFT', 4096: 'wxRESERVE_SPACE_EVEN_IF_HIDDEN',
            8192: 'wxRIGHT', 16384: 'wxSHAPED', 32768: 'wxTOP'
        }
        return '| '.join(align_map[k] for k in align_map.keys() if self._flag & k) or '0'

    @property
    def border(self):
        return self._border

    @property
    def flag(self):
        return self._flag

    def detach_from_parent(self):
        """Called during the removal of the edition window"""
        this_window = self.control.edition_window
        if this_window is None:
            return
        sizer_window = self.control.parent.edition_window_client
        if sizer_window is None:
            return
        sizer_window.Detach(this_window)

    def Realize(self):
        """Finishes the addition of the edition window to the sizer"""
        client = self.control.edition_window
        if client is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')
        container = self.control.parent.edition_window_client
        if container is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')
        return container.Add(client, self.proportion, self.alignment_value, self.border)

    def cc_realize_code(self, container, window):
        """Return the code required for inserting the element into the sizer in c++"""
        return f'{container}->Add({window}, {self.proportion}, {self.cc_aligment_str}, {self.border});\n'

    def py_realize_code(self, container, window):
        """Return the code required for inserting the element into the sizer in python"""
        return f'{container}.Add({window}, {self.proportion}, {self.py_aligment_str}, {self.border})\n'

