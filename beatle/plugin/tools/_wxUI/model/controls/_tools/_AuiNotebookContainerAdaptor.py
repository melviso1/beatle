import wx
from ..._wxUICommon import wxUICommon
from ....lib._BitmapProperty import BitmapProperty
from ....lib import get_bitmap_py_text_from_tuple, get_bitmap_cpp_text_from_tuple, get_bitmap_from_tuple


class AuiNotebookContainerAdaptor(wxUICommon):
    """This class acts as a helper for handling the windows that belongs to
    tab container and whose properties that must be 'injected' in a control when his parent
    is an auinotebook control."""

    def __init__(self, **kwargs):
        """Initialize a sizer item.
        The control argument represents the control handled by the
        TabControl. Take in consideration that these have no parent"""
        self._tab_label = 'a page'
        self._tab_bitmap = (0,'')
        kwargs['visibleInTree'] = False
        self._control = kwargs.get('control', None)
        super(AuiNotebookContainerAdaptor, self).__init__(**kwargs)

    @property
    def control(self):
        return self._control

    @property
    def kwargs(self):
        return {
            'tab_label': self._tab_label,
            'tab_bitmap': self._tab_bitmap,
        }

    def get_bitmap(self):
        t = self._tab_bitmap
        if t[0] == 0:
            path = t[1]
            if len(path) == 0:
                bitmap = wx.Bitmap(1, 1)
            else:
                bitmap = wx.Bitmap(path, wx.BITMAP_TYPE_ANY)
                if not bitmap.IsOk():
                    bitmap = wx.Bitmap(1,1)
        elif t[0] == 1:
            bitmap = wx.ArtProvider.GetBitmap(
                BitmapProperty.artid_value(t[1]),
                BitmapProperty.artclientid_value(t[2]))
            if not bitmap.IsOk():
                bitmap = wx.Bitmap(1,1)
        else:
            bitmap = wx.Bitmap(1,1)
        return bitmap

    @kwargs.setter
    def kwargs(self, value):
        from ..containers._AuiNotebookControl import AuiNotebookControl

        self._tab_label = value.get('tab_label', self._tab_label)
        self._tab_bitmap = value.get('tab_bitmap', self._tab_bitmap)
        if self.control is None:
            return
        edition_window = self.control.edition_window
        if edition_window is None:
            return
        parent_window = getattr(self.control.parent, '_edition_window', None)
        if not type(parent_window) is AuiNotebookControl:
            return
        book = parent_window.edition_window
        if book is None:
            return
        index = book.GetPageIndex(self.edition_window)
        if index == wx.NOT_FOUND:
            return
        book.SetPageText(index, self._tab_label)
        if self._tab_bitmap != (0, ''):
            book.SetPageBitmap(index, self.get_bitmap())

    @property
    def model_attributes(self):
        _property = [{
                'label': 'aui notebook page',
                'type': 'category',
                'help': 'Attributes of aui notebook page',
                'child': [
                    {
                        'label': 'tab label',
                        'type': 'string',
                        'read_only': False,
                        'value': self._tab_label,
                        'name': 'tab_label',  # kwarg value
                        'help': 'The tab label of this page.',
                    },
                    {
                        'label': 'tab bitmap',
                        'type': 'bitmap',
                        'read_only': False,
                        'value': self._tab_bitmap,
                        'name': 'tab_bitmap',  # kwarg value
                        'help': 'the tab bitmap.',
                    },
                ]}]
        _event = []
        return _property, _event

    @property
    def tab_label(self):
        return self._tab_label

    @tab_label.setter
    def tab_label(self, value):
        self._tab_label = value

    def detach_from_parent(self):
        """Called during the removal of the edition window"""
        this_window = self.control.edition_window
        if this_window is None:
            return
        book = self.control.parent.edition_window
        if book is None:
            return
        index = book.GetPageIndex(this_window)
        if index != wx.NOT_FOUND:
            book.RemovePage(index)

    def Realize(self):
        client = self.control.edition_window
        if client is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')
        book = self.control.parent.edition_window
        if book is None:
            # this is an error
            raise RuntimeError('edition window must exist before call Realize()')
        if not book.AddPage(client, self._tab_label, True):
            return
        index = book.GetPageIndex(client)
        if index != wx.NOT_FOUND:
            bitmap = get_bitmap_from_tuple(self._tab_bitmap)
            if bitmap is not None:
                    book.SetPageBitmap(index, self.get_bitmap())
        return index

    def cc_realize_code(self, container, window):
        """Return the code required for inserting the element into the sizer in c++"""
        code = f'{container}->AddPage({window}, "{self._tab_label}", false);\n'
        bitmap_ = get_bitmap_cpp_text_from_tuple(self._tab_bitmap)
        if len(bitmap_):
            code += f'{{\n    auto index = {container}->GetPageIndex({window});\n'
            code += f'    {container}->SetPageBitmap(index, {bitmap_});\n}}\n'
        return code

    def py_realize_code(self, container, window):
        """Return the code required for inserting the element into the size in python"""
        code = f'{container}.AddPage({window}, "{self._tab_label}", False )\n'
        bitmap_ = get_bitmap_py_text_from_tuple(self._tab_bitmap)
        if len(bitmap_):
            code += f'index = {container}.GetPageIndex({window})\n'
            code += f'{container}.SetPageBitmap(index, {bitmap_})\n'
        return code

