import wx
from ..._Window import Window
from beatle.model import cc, py


class ToolBarControl(Window):
    """"This class represents a menu bar as control"""
    style_list = (wx.TB_DOCKABLE, wx.TB_FLAT, wx.TB_HORIZONTAL, wx.TB_VERTICAL,
                  wx.TB_HORZ_LAYOUT, wx.TB_HORZ_TEXT, wx.TB_NOALIGN, wx.TB_NODIVIDER,
                  wx.TB_NOICONS, wx.TB_TEXT)

    style_list_py_text = (
        'wx.TB_DOCKABLE', 'wx.TB_FLAT', 'wx.TB_HORIZONTAL', 'wx.TB_VERTICAL',
        'wx.TB_HORZ_LAYOUT', 'wx.TB_HORZ_TEXT', 'wx.TB_NOALIGN', 'wx.TB_NODIVIDER',
        'wx.TB_NOICONS', 'wx.TB_TEXT')

    style_list_cc_text = (
        'wxTB_DOCKABLE', 'wxTB_FLAT', 'wxTB_HORIZONTAL', 'wxTB_VERTICAL',
        'wxTB_HORZ_LAYOUT', 'wxTB_HORZ_TEXT', 'wxTB_NOALIGN', 'wxTB_NODIVIDER',
        'wxTB_NOICONS', 'wxTB_TEXT')

    def __init__(self, **kwargs):
        # -- initialize data with default value
        self._bitmap_size = (-1, -1)
        self._margins = (-1, -1)
        self._packing = 1
        self._separation = 5
        self._tool_bar_style = 4  # wxTB_HORIZONTAL
        super(ToolBarControl, self).__init__(**kwargs)

    @property
    def local_toolbarcontrol_kwargs(self):
        return {
            'bitmap_size': self._bitmap_size,
            'margins': self._margins,
            'packing': self._packing,
            'separation': self._separation,
            'tool_bar_style': self._tool_bar_style
        }

    @local_toolbarcontrol_kwargs.setter
    def local_toolbarcontrol_kwargs(self, kwargs):
        self._bitmap_size = kwargs.get('fields', self._bitmap_size)
        self._margins = kwargs.get('status_bar_style', self._margins)
        self._packing = kwargs.get('packing', self._packing)
        self._separation = kwargs.get('separation', self._separation)
        self._tool_bar_style = kwargs.get('tool_bar_style', self._tool_bar_style)

    @property
    def kwargs(self):
        value = self.local_toolbarcontrol_kwargs
        value.update(super(ToolBarControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_toolbarcontrol_kwargs = kwargs
        # prevent the base class for update the edition window
        # because it's more easy to recreate it
        edition_window = self._edition_window
        self._edition_window = None
        super(ToolBarControl, self).set_kwargs(kwargs)
        self._edition_window = edition_window
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.parent.update_implementation()

        if self._edition_window is not None:
            self.destroy_edition_window()
            self.create_edition_window(self.parent)

    @property
    def style_value(self):
        """Return the value required for dynamic button creation"""
        return self.style_value_helper(ToolBarControl.style_list, self._tool_bar_style
                                       ) | super(ToolBarControl, self).style_value

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container, sizer = parent.toolbar_parent_and_sizer(**self.kwargs)
        if container is None:
            return
        #if sizer:
        #    sizer.Clear()
        self._edition_window = uiMockupControl(wx.ToolBar)(
            container, self.py_id, self.py_pos,self.py_size, self.style_value)
        for child in self.sorted_wxui_child:
            child.create_edition_window(self)
        self._edition_window.Realize()
        if sizer is not None:
            sizer.Add(self._edition_window, 1, wx.EXPAND, 0)
            sizer.Fit(container)
            container.Layout()
        else:
            self.add_to_sizer(parent, self._edition_window)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('toolbar')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Tool bar',
            'type': 'category',
            'help': 'Attributes of tool bar.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The tool bar variable name"
                },
                {
                    'label': 'bitmap size',
                    'type': 'int_vector',
                    'read_only': False,
                    'value': self._bitmap_size,
                    'name': 'bitmap_size',  # kwarg value
                    'labels': [
                        ('width', 'bitmap width.'),
                        ('height', 'bitmap height.')
                    ],
                    'help': 'The tool bar expected bitmap size'
                },
                {
                    'label': 'margins',
                    'type': 'int_vector',
                    'read_only': False,
                    'value': self._margins,
                    'name': 'margins',  # kwarg value
                    'labels': [
                        ('width', 'bitmap width.'),
                        ('height', 'bitmap height.')
                    ],
                    'help': 'The tool bar margins'
                },
                {
                    'label': 'packing',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._packing,
                    'name': 'packing',  # kwarg value
                    'help': 'The space between tool rows (horizontal toolbar) or columns (vertical toolbar)'
                },
                {
                    'label': 'separation',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._separation,
                    'name': 'separation',  # kwarg value
                    'help': 'The space between tools '
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._tool_bar_style,
                    'name': 'tool_bar_style',
                    'help': 'The tool bar applicable styles',
                    'options': [
                        ('wxTB_DOCKABLE', 'Is the tool bar dockable?'),
                        ('wxTB_FLAT', 'GTK-only flat style'),
                        ('wxTB_HORIZONTAL', 'Horizontal layout'),
                        ('wxTB_VERTICAL', 'Vertical layout'),
                        ('wxTB_HORZ_LAYOUT', 'Show text and icons not stacked'),
                        ('wxTB_HORZ_TEXT', 'wxTB_HORZ_LAYOUT | wxTB_TEXT'),
                        ('wxTB_NOALIGN', ''),
                        ('wxTB_NODIVIDER', ''),
                        ('wxTB_NOICONS', ''),
                        ('wxTB_TEXT', ''),
                    ]
                }
            ]
        }]
        _event = []
        _base_property, _base_event = super(ToolBarControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxAuiToolBar', ptr=True),
            default='nullptr'
        )

    @property
    def cc_style_str(self):
        """Return a string suitable for the styke declaration"""
        if self._tool_bar_style == 0:
            return super(ToolBarControl, self).cc_style_str
        sz = len(ToolBarControl.style_list_cc_text)
        control_style_value = '|'.join(
            ToolBarControl.style_list_cc_text[index] for index in range(sz)
            if self._tool_bar_style & (2 ** index)
        )
        base = super(ToolBarControl, self).cc_style_str
        if base == '0':
            return control_style_value
        return '|'.join(control_style_value, base)

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole menubar implementation.
        Also, update required includes"""
        self.add_cpp_required_header('#include <wx/toolbar.h>')

        if container == 'this':
            code = f"{self._name} = CreateToolBar( {self.cc_style_str}, {self._id} );\n"
        else:
            code = f"{self._name} = {container}->CreateToolBar( {self.cc_style_str}, {self._id} );\n"
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)
        code += f'{self._name}.Realize();\n'
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    @property
    def py_style_str(self):
        if self._tool_bar_style == 0:
            return super(ToolBarControl, self).py_style_str
        sz = len(ToolBarControl.style_list_py_text)
        control_style_value = '|'.join(
            ToolBarControl.style_list_py_text[index] for index in range(sz)
            if self._tool_bar_style & (2 ** index)
        )
        base = super(ToolBarControl, self).py_style_str
        if base == '0':
            return control_style_value
        return '|'.join((control_style_value, base))

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self._name)
        if self.parent.is_top_window:
            code = f'{this} = {parent}.CreateToolBar({self.py_style_str}, {self.py_id})\n'
        else:
            owner = self.parent.sizer_root.py_owner_text
            code = f'{this} = wx.ToolBar({owner}, {self.py_id_str}, {self.py_pos_str}, ' \
                   f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(ToolBarControl, self).python_init_control_code()
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += f'{this}.Realize()\n'
        code += self.py_add_to_sizer_code(parent, this)
        return code

