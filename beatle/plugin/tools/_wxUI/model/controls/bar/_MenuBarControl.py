from ..._Window import Window
from ..bar._MenuControl import MenuControl
from beatle.model import cc, py


class MenuBarControl(Window):
    """"This class represents a menu bar as control"""

    def __init__(self, **kwargs):
        # -- initialize data with default value
        self._label = 'MyMenuBar'
        self._menu_style = 0
        super(MenuBarControl, self).__init__(**kwargs)

    @property
    def local_menubarcontrol_kwargs(self):
        return {
            'label': self._label,
            'menu_style': self._menu_style,
        }

    @local_menubarcontrol_kwargs.setter
    def local_menubarcontrol_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._menu_style = kwargs.get('menu_style', self._menu_style)

    @property
    def kwargs(self):
        value = self.local_menubarcontrol_kwargs
        value.update(super(MenuBarControl, self).kwargs)
        return value

    @property
    def can_add_menu_control(self):
        return self._edition_window is not None

    def set_kwargs(self, kwargs):
        self.local_menubarcontrol_kwargs = kwargs
        super(MenuBarControl, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.update_implementation()
        if self._edition_window:
            self._edition_window.set_kwargs(**kwargs)

    def create_edition_window(self, parent):
        from ._MenuControl import MenuControl
        self._edition_window = parent.create_menu_bar(self)
        if self._edition_window is not None:
            self._edition_window.set_kwargs(**self.kwargs)
            menu_list = sorted(self[MenuControl], key=lambda x: x.wxui_child_index)
            for menu in menu_list:  # really only one
                menu.create_edition_window(self)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('menu_bar')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Menu bar',
            'type': 'category',
            'help': 'Attributes of menu bar.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The menu name"
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'The menu label'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._menu_style,
                    'name': 'menu_style',
                    'help': 'The menu style',
                    'options': [
                        ('wxMB_DOCKABLE', 'Allows the menu bar to be detached (wxGTK only)')
                    ]
                }
            ]
        }]
        _base_property, _base_event = super(MenuBarControl, self).model_attributes
        _property += _base_property
        _event = []
        return _property, _event

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxMenuBar', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self):
        """This method must return an string corresponding
        to the whole menubar implementation.
        Also, update required includes"""
        self.add_cpp_required_header('#include <wx/menu.h>')

        # prolog
        code = f"{self.name} = new wxMenuBar(0);\n"

        # travel through the menus and add generated code
        for mnu in self[MenuControl]:
            code += mnu.cpp_init_code(self)

        # epilog
        code += f"SetMenuBar({self.name});\n"
        code += super(MenuBarControl, self).cpp_init_code()
        return code

    def python_init_control_code(self, parent='self'):
        this = f'self.{self._name}'
        code = f"{this} = wx.MenuBar(0)\n"

        # travel through the menus and add generated code
        menus = [x for x in self.sorted_wxui_child if type(x) is MenuControl]
        for mnu in menus:
            code += mnu.python_init_control_code(this)
        # epilog
        code += f"{parent}.SetMenuBar({this})\n"
        code += super(MenuBarControl, self).python_init_control_code()
        return code
