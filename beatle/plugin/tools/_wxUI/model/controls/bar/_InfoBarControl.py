import wx
import wx.aui
from ..._Window import Window
from ....lib import popup_errors_to_user
from beatle.model import cc, py


class InfoBarControl(Window):
    """"This class represents a info bar control"""

    infobar_effects = [
        'wxSHOW_EFFECT_NONE', 'wxSHOW_EFFECT_ROLL_TO_LEFT', 'wxSHOW_EFFECT_ROLL_TO_RIGHT', 'wxSHOW_EFFECT_ROLL_TO_TOP',
        'wxSHOW_EFFECT_ROLL_TO_BOTTOM', 'wxSHOW_EFFECT_SLIDE_TO_LEFT', 'wxSHOW_EFFECT_SLIDE_TO_RIGHT',
        'wxSHOW_EFFECT_SLIDE_TO_TOP', 'wxSHOW_EFFECT_SLIDE_TO_BOTTOM', 'wxSHOW_EFFECT_BLEND', 'wxSHOW_EFFECT_EXPAND',
    ]

    py_info_bar_effect = {
        'wxSHOW_EFFECT_NONE': wx.SHOW_EFFECT_NONE,
        'wxSHOW_EFFECT_ROLL_TO_LEFT': wx.SHOW_EFFECT_ROLL_TO_LEFT,
        'wxSHOW_EFFECT_ROLL_TO_RIGHT': wx.SHOW_EFFECT_ROLL_TO_RIGHT,
        'wxSHOW_EFFECT_ROLL_TO_TOP': wx.SHOW_EFFECT_ROLL_TO_TOP,
        'wxSHOW_EFFECT_ROLL_TO_BOTTOM': wx.SHOW_EFFECT_ROLL_TO_BOTTOM,
        'wxSHOW_EFFECT_SLIDE_TO_LEFT': wx.SHOW_EFFECT_SLIDE_TO_LEFT,
        'wxSHOW_EFFECT_SLIDE_TO_RIGHT': wx.SHOW_EFFECT_SLIDE_TO_RIGHT,
        'wxSHOW_EFFECT_SLIDE_TO_TOP': wx.SHOW_EFFECT_SLIDE_TO_TOP,
        'wxSHOW_EFFECT_SLIDE_TO_BOTTOM': wx.SHOW_EFFECT_SLIDE_TO_BOTTOM,
        'wxSHOW_EFFECT_BLEND': wx.SHOW_EFFECT_BLEND,
        'wxSHOW_EFFECT_EXPAND': wx.SHOW_EFFECT_EXPAND,
    }

    def __init__(self, **kwargs):
        # -- initialize data with default value
        self._show_effect = 'wxSHOW_EFFECT_NONE'
        self._hide_effect = 'wxSHOW_EFFECT_NONE'
        self._duration = 500
        super(InfoBarControl, self).__init__(**kwargs)

    @property
    def local_aui_info_bar_control_kwargs(self):
        return {
            'show_effect': self._show_effect,
            'hide_effect': self._hide_effect,
            'duration': self._duration
        }

    @local_aui_info_bar_control_kwargs.setter
    def local_aui_info_bar_control_kwargs(self, kwargs):
        self._show_effect = kwargs.get('show_effect', self._show_effect)
        self._hide_effect = kwargs.get('hide_effect', self._hide_effect)
        self._duration = kwargs.get('duration', self._duration)

    @property
    def kwargs(self):
        value = self.local_aui_info_bar_control_kwargs
        value.update(super(InfoBarControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_aui_info_bar_control_kwargs = kwargs
        edition_window = self._edition_window
        self._edition_window = None
        super(InfoBarControl, self).set_kwargs(kwargs)
        self._edition_window = edition_window
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.update_implementation()
        # if self._edition_window is not None:
        #    self.destroy_edition_window()
        #    self.create_edition_window(self.parent)

    @property
    def py_show_effect_style(self):
        return InfoBarControl.py_info_bar_effect[self._show_effect]

    @property
    def py_hide_effect_style(self):
        return InfoBarControl.py_info_bar_effect[self._hide_effect]

    def destroy_edition_window(self):
        from ._ToolBarControl import ToolBarControl
        from ._AuiToolBarControl import AuiToolBarControl
        if self._edition_window is not None:
            for kind in self.children:
                for child in self.children[kind]:
                    child.destroy_edition_window()
            if type(self._parent) in [ToolBarControl, AuiToolBarControl]:
                self._parent.edition_window.DeleteTool(self._edition_window.GetId())
            else:
                self._edition_window.Destroy()
            self._edition_window = None

    @popup_errors_to_user
    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        from ._ToolBarControl import ToolBarControl
        from ._AuiToolBarControl import AuiToolBarControl

        parent = parent.inner_false(lambda x : x.is_sizer)
        parent_window = parent and parent.edition_window_client
        if parent_window is None:
            return
        control = uiMockupControl(wx.InfoBar)(parent_window)
        control.SetShowHideEffects(self.py_show_effect_style, self.py_hide_effect_style)
        control.SetEffectDuration(self._duration)
        if type(parent) in [ToolBarControl, AuiToolBarControl]:
            self._edition_window = parent.edition_window.AddControl(control)
        else:
            self._edition_window = control
            self.add_to_sizer(parent.edition_window, control)
        control.ShowMessage('message')
        control.bind(self)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('info_bar')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Info bar',
            'type': 'category',
            'help': 'Attributes of info bar.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The tool bar variable name"
                },
                {
                    'label': 'show effect',
                    'type': 'choice',
                    'value': self._show_effect,
                    'name': 'show_effect',
                    'help': 'The effect used while showing the info bar.',
                    'choices':  InfoBarControl.infobar_effects
                },
                {
                    'label': 'hide effect',
                    'type': 'choice',
                    'value': self._hide_effect,
                    'name': 'hide_effect',
                    'help': 'The effect used while hidding the info bar.',
                    'choices':  InfoBarControl.infobar_effects
                },
                {
                    'label': 'duration',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._duration,
                    'name': 'duration',  # kwarg value
                    'help': 'The time used by transition effect. '
                },
            ]
        }]
        _event = []
        _base_property, _base_event = super(InfoBarControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxInfoBar', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        from ._ToolBarControl import ToolBarControl
        from ._AuiToolBarControl import AuiToolBarControl
        self.add_cpp_required_header('#include <wx/infobar.h>')
        code = f"{self._name} = new wxInfoBar({container});\n"
        code += f"{self._name}->SetShowHideEffects({self._show_effect},{self._hide_effect});\n"
        code += f"{self._name}->SetEffectDuration( {self._duration} );\n"
        code += super(InfoBarControl, self).python_init_control_code()
        if type(self._parent) in [ToolBarControl, AuiToolBarControl]:
            code += f'{container}->AddControl({self._name})\n'
        else:
            code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        from ._ToolBarControl import ToolBarControl
        from ._AuiToolBarControl import AuiToolBarControl
        this = 'self.{name}'.format(name=self._name)
        owner = self._parent.inner_false(lambda x: x.is_sizer)
        if owner == self.top_window:
            this_owner = 'self'
        else:
            this_owner = f'self.{owner.name}'
        code = f'{this} = wx.InfoBar({this_owner})\n'
        code += super(InfoBarControl, self).python_init_control_code()
        if type(self._parent) in [ToolBarControl, AuiToolBarControl]:
            code += f'{parent}.AddControl({this})\n'
        else:
            code += self.py_add_to_sizer_code(parent, this)
        return code

