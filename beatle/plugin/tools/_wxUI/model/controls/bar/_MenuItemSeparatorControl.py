import wx
from ..._Window import Window
from beatle.model import cc, py


class MenuItemSeparatorControl(Window):

    def __init__(self, **kwargs):
        """Creates a new menu item separator"""
        super(MenuItemSeparatorControl, self).__init__(**kwargs)

    def set_kwargs(self, kwargs):
        super(MenuItemSeparatorControl, self).set_kwargs(kwargs)
        if self._implementation:
            sanitized_name = self.name.strip()
            self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('menu_separator')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'separator',
            'type': 'category',
            'help': '',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The variable name."
                },
            ]
        }]
        _event = []
        # For this kind of object the inherited stuff could be skipped
        # _base_property, _base_event = super(MenuItemSeparatorControl, self).model_attributes
        # _property += _base_property
        return _property, _event

    def declare_cpp_variable(self, container):
        """Create the data objects that implement the element"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxMenuItem', ptr=True),
            default='nullptr'
        )

    def python_init_control_code(self, parent='self'):
        this = 'self.{name}'.format(name=self._name)
        return f'{this} = {parent}.AppendSeparator()\n'

    def cpp_init_code(self, container):
        self.add_cpp_required_header('#include <wx/menuitem.h>')
        return f'{self._name} =  {container.name}->AppendSeparator();\n'

    def create_edition_window(self, parent):
        from ....ui import uiFakeMenuItem
        menu = getattr(self.parent, '_edition_window', None)
        if menu is not None:
            self._edition_window = uiFakeMenuItem(menu, id=wx.ID_SEPARATOR, kind=wx.ITEM_SEPARATOR)
            menu.AppendItem(self._edition_window)

    def destroy_edition_window(self):
        """Menuitems aren't real windows. We must delete it by special method"""
        menu = getattr(self.parent, '_edition_window', None)
        if menu is not None:
            menu.DestroyItem(self._edition_window)
            self._edition_window = None
