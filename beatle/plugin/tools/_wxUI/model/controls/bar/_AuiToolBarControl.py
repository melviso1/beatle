import wx
import wx.aui
from ..._Window import Window
from beatle.model import cc
from ....lib import register_cpp_class, with_style, extend_events


@with_style
class AuiToolBarControl(Window):
    """"This class represents a menu bar as control"""

    style_list = (
        wx.aui.AUI_TB_DEFAULT_STYLE , wx.aui.AUI_TB_GRIPPER, wx.aui.AUI_TB_HORIZONTAL,
        wx.aui.AUI_TB_HORZ_TEXT, wx.aui.AUI_TB_NO_AUTORESIZE,wx.aui.AUI_TB_NO_TOOLTIPS,
        wx.aui.AUI_TB_OVERFLOW, wx.aui.AUI_TB_PLAIN_BACKGROUND,wx.aui.AUI_TB_TEXT,
        wx.aui.AUI_TB_VERTICAL
    )

    style_list_py_text = (
        'wx.aui.AUI_TB_DEFAULT_STYLE', 'wx.aui.AUI_TB_GRIPPER', 'wx.aui.AUI_TB_HORZ_LAYOUT',
        'wx.aui.AUI_TB_HORZ_TEXT', 'wx.aui.AUI_TB_NO_AUTORESIZE', 'wx.aui.AUI_TB_NO_TOOLTIPS',
        'wx.aui.AUI_TB_OVERFLOW', 'wx.aui.AUI_TB_PLAIN_BACKGROUND', 'wx.aui.AUI_TB_TEXT',
        'wx.aui.AUI_TB_VERTICAL'
    )
    style_list_cc_text = (
        'wxAUI_TB_DEFAULT_STYLE', 'wxAUI_TB_GRIPPER', 'wxAUI_TB_HORZ_LAYOUT',
        'wxAUI_TB_HORZ_TEXT', 'wxAUI_TB_NO_AUTORESIZE', 'wxAUI_TB_NO_TOOLTIPS',
        'wxAUI_TB_OVERFLOW', 'wxAUI_TB_PLAIN_BACKGROUND', 'wxAUI_TB_TEXT',
        'wxAUI_TB_VERTICAL'
    )

    def __init__(self, **kwargs):
        # -- initialize data with default value
        self._bitmap_size = (-1, -1)
        self._margins = (-1, -1)
        self._packing = 1
        self._separation = 5
        self._aui_tool_bar_style = 1  # wx.aui.AUI_TB_DEFAULT_STYLE
        self._label_visible = False
        self._toolbar_label = 'label'
        # events
        self._on_tool_dropdown = ''
        self._on_overflow_click = ''
        self._on_right_click = ''
        self._on_middle_click = ''
        self._on_begin_drag = ''
        super(AuiToolBarControl, self).__init__(**kwargs)

    @property
    def local_aui_toolbarcontrol_kwargs(self):
        return {
            'bitmap_size': self._bitmap_size,
            'margins': self._margins,
            'packing': self._packing,
            'separation': self._separation,
            'aui_tool_bar_style': self._aui_tool_bar_style,
            'label_visible': self._label_visible,
            'toolbar_label': self._toolbar_label,
            # events
            'on_tool_dropdown': self._on_tool_dropdown,
            'on_overflow_click': self._on_overflow_click,
            'on_right_click': self._on_right_click,
            'on_middle_click': self._on_middle_click,
            'on_begin_drag': self._on_begin_drag,
        }

    @local_aui_toolbarcontrol_kwargs.setter
    def local_aui_toolbarcontrol_kwargs(self, kwargs):
        self._bitmap_size = kwargs.get('fields', self._bitmap_size)
        self._margins = kwargs.get('status_bar_style', self._margins)
        self._packing = kwargs.get('packing', self._packing)
        self._separation = kwargs.get('separation', self._separation)
        self._aui_tool_bar_style = kwargs.get('aui_tool_bar_style', self._aui_tool_bar_style)
        self._label_visible = kwargs.get('label_visible', self._label_visible)
        self._toolbar_label = kwargs.get('toolbar_label', self._toolbar_label)
        #events
        self._on_tool_dropdown = kwargs.get('on_tool_dropdown', self._on_tool_dropdown)
        self._on_overflow_click = kwargs.get('on_overflow_click', self._on_overflow_click)
        self._on_right_click = kwargs.get('on_right_click', self._on_right_click)
        self._on_middle_click = kwargs.get('on_middle_click', self._on_middle_click)
        self._on_begin_drag = kwargs.get('on_begin_drag', self._on_begin_drag)

    @property
    def local_aui_tool_bar_control_events(self):
        return {
            'on_tool_dropdown': (self._on_tool_dropdown, 'wx.aui.EVT_AUITOOLBAR_TOOL_DROPDOWN',
                                 'wxEVT_AUITOOLBAR_TOOL_DROPDOWN'),
            'on_overflow_click': (self._on_overflow_click, 'wx.aui.EVT_AUITOOLBAR_OVERFLOW_CLICK',
                                  'wxEVT_AUITOOLBAR_OVERFLOW_CLICK'),
            'on_right_click': (self._on_right_click, 'wx.aui.EVT_AUITOOLBAR_RIGHT_CLICK',
                               'wxEVT_AUITOOLBAR_RIGHT_CLICK'),
            'on_middle_click': (self._on_middle_click, 'wx.aui.EVT_AUITOOLBAR_MIDDLE_CLICK',
                                'wxEVT_AUITOOLBAR_MIDDLE_CLICK'),
            'on_begin_drag': (self._on_begin_drag, 'wx.aui.EVT_AUITOOLBAR_BEGIN_DRAG',
                              'wxEVT_AUITOOLBAR_BEGIN_DRAG'),
        }

    @property
    def events(self):
        value = self.local_aui_tool_bar_control_events
        value.update(super(AuiToolBarControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_aui_toolbarcontrol_kwargs
        value.update(super(AuiToolBarControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_aui_toolbarcontrol_kwargs = kwargs
        edition_window = self._edition_window
        self._edition_window = None
        super(AuiToolBarControl, self).set_kwargs(kwargs)
        self._edition_window = edition_window
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.parent.update_implementation()
        # if self._edition_window is not None:
        #    self.destroy_edition_window()
        #    self.create_edition_window(self.parent)

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        container, sizer = parent.toolbar_parent_and_sizer(**self.kwargs)
        if container is None:
            return
        if sizer:
            sizer.Clear()
        self._edition_window = uiMockupControl(wx.aui.AuiToolBar)(
            container, self.py_id, self.py_pos, self.py_size, self.style_value)

        for child in self.sorted_wxui_child:
            child.create_edition_window(self)

        self._edition_window.Realize()
        if sizer is not None:
            sizer.Add(self._edition_window, 1, wx.EXPAND, 0)
            sizer.Fit(container)
            container.Layout()
        else:
            self.add_to_sizer(parent, self._edition_window)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('aui_toolbar')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Aui tool bar',
            'type': 'category',
            'help': 'Attributes of advanced user interface (aui) tool bar.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The tool bar variable name"
                },
                {
                    'label': 'bitmap size',
                    'type': 'int_vector',
                    'read_only': False,
                    'value': self._bitmap_size,
                    'name': 'bitmap_size',  # kwarg value
                    'labels': [
                        ('width', 'bitmap width.'),
                        ('height', 'bitmap height.')
                    ],
                    'help': 'The tool bar expected bitmap size'
                },
                {
                    'label': 'margins',
                    'type': 'int_vector',
                    'read_only': False,
                    'value': self._margins,
                    'name': 'margins',  # kwarg value
                    'labels': [
                        ('width', 'bitmap width.'),
                        ('height', 'bitmap height.')
                    ],
                    'help': 'The tool bar margins'
                },
                {
                    'label': 'packing',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._packing,
                    'name': 'packing',  # kwarg value
                    'help': 'The space between tool rows (horizontal tool bar) or columns (vertical tool bar)'
                },
                {
                    'label': 'separation',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._separation,
                    'name': 'separation',  # kwarg value
                    'help': 'The space between tools '
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._aui_tool_bar_style,
                    'name': 'aui_tool_bar_style',
                    'help': 'The tool bar applicable styles',
                    'options': [
                        ('wxAUI_TB_DEFAULT_STYLE', "The default is to have no styles."),
                        ('wxAUI_TB_GRIPPER',
                         "Show the toolbar’s gripper control. If the toolbar is added to an AUI pane that contains"
                         " a gripper, this style will be automatically set."),
                        ('wxAUI_TB_HORIZONTAL',
                         "Analogous to wx.aui.AUI_TB_VERTICAL, but forces the toolbar to be horizontal."),
                        ('wxAUI_TB_HORZ_TEXT', "Equivalent to wxAUI_TB_HORIZONTAL | wxAUI_TB_TEXT"),
                        ('wxAUI_TB_NO_AUTORESIZE',
                         "Do not automatically resize the toolbar when new tools are added."),
                        ('wxAUI_TB_NO_TOOLTIPS', "Do not show tooltips for the toolbar items."),
                        ('wxAUI_TB_OVERFLOW',
                         "Show an overflow menu containing toolbar items that can’t fit on the toolbar if it is"
                         " too small."),
                        ('wxAUI_TB_PLAIN_BACKGROUND',
                         "Draw a plain background (based on parent) instead of the default gradient background."),
                        ('wxAUI_TB_TEXT', "Display the label strings on the toolbar buttons."),
                        ('wxAUI_TB_VERTICAL',
                         "Using this style forces the toolbar to be vertical and be only dock-able to the left or"
                         " right sides of the window whereas by default it can be horizontal or vertical and be"
                         " docked anywhere."),
                    ]
                }
            ]
        }]
        _event = [{
            'label': 'AuiToolBar',
            'type': 'category',
            'help': 'Events of aui tool bar control',
            'child': [

                {
                    'label': 'on tool dropdown',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tool_dropdown,
                    'name': 'on_tool_dropdown',
                    'help': "Process a wxEVT_AUITOOLBAR_TOOL_DROPDOWN event",
                },
                {
                    'label': 'on overflow click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_overflow_click,
                    'name': 'on_overflow_click',
                    'help': "Process a wxEVT_AUITOOLBAR_OVERFLOW_CLICK event",
                },
                {
                    'label': 'on right click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_right_click,
                    'name': 'on_right_click',
                    'help': "Process a wxEVT_AUITOOLBAR_RIGHT_CLICK event",
                },
                {
                    'label': 'on middle click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_middle_click,
                    'name': 'on_middle_click',
                    'help': "Process a wxEVT_AUITOOLBAR_MIDDLE_CLICK event",
                },
                {
                    'label': 'on begin drag',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_begin_drag,
                    'name': 'on_begin_drag',
                    'help': "Process a wxEVT_AUITOOLBAR_BEGIN_DRAG event",
                },
            ]
        }]
        _base_property, _base_event = super(AuiToolBarControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def declare_cpp_variable(self, container):
        """We create a pointer to menu"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxToolBar', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container='this'):
        """This method must return an string corresponding
        to the whole aui menu bar implementation.
        Also, update required includes."""
        self.add_cpp_required_header('#include <wx/aui/aui.h>')
        self.add_cpp_required_header('#include <wx/aui/auibar.h>')

        code = f"{self._name} = new wxAuiToolBar({container}, {self._id}, {self.cc_pos_str}, " \
               f"{self.cc_size_str}, {self.cc_style_str});\n"

        code += super(AuiToolBarControl, self).cpp_init_code()
        for element in self.sorted_wxui_child:
            code += element.cpp_init_code(self._name)

        code += f"{self._name}->Realize();\n"
        code += self.cc_add_to_sizer_code(container, self._name)
        return code

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        this = 'self.{name}'.format(name=self._name)
        code = f'{this} = wx.aui.AuiToolBar(self,{self.py_id_str}, {self.py_pos_str}, ' \
               f'{self.py_size_str}, {self.py_style_str})\n'
        code += super(AuiToolBarControl, self).python_init_control_code()
        for element in self.sorted_wxui_child:
            code += element.python_init_control_code(this)
        code += f'{this}.Realize()\n'
        code += self.py_add_to_sizer_code(parent, this)
        return code

    def py_bind_text(self, event):
        """return the suitable code for binding an event"""
        return 'self.Bind({event[1]},self.{event[0]}, id=self.{name}.GetId())\n'.format(
            event=event, name=self._name
        )

    def py_unbind_text(self, event):
        """return the suitable code for binding an event"""
        return 'self.Unbind({event[1]},self.{event[0]}, id=self.{name}.GetId())\n'.format(
            event=event, name=self._name
        )
