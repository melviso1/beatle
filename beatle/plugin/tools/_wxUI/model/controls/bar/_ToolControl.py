import wx
from beatle.lib.tran import TransactionStack
from beatle.model import cc
from ..._Window import Window
from ....lib import get_bitmap_py_text_from_tuple, get_bitmap_cpp_text_from_tuple, get_bitmap_from_tuple


class ToolControl(Window):
    """This class represents a tool inside the toolbar. This control
    is not recursing on windows properties, because this is a very special kind
    of control. For commodity, the dictionaries are updated as usual, but grid
    properties edition dont recurse on bases this case."""

    def __init__(self, **kwargs):
        # -- initialize data with default value
        self._label = 'tool'
        self._bitmap = (0, '')
        self._bitmap_disabled = (0, '')
        self._kind = 'wxITEM_NORMAL'
        self._tooltip = ''
        self._status_bar = ''
        #events
        self._on_tool_clicked = ''
        self._on_tool_r_clicked = ''
        self._on_tool_enter = ''
        self._on_update_ui = ''
        super(ToolControl, self).__init__(**kwargs)

    @property
    def local_tool_kwargs(self):
        return {
            'label': self._label,
            'bitmap': self._bitmap,
            'bitmap_disabled': self._bitmap_disabled,
            'kind': self._kind,
            'tooltip': self._tooltip,
            'status_bar': self._status_bar,
            'on_tool_clicked': self._on_tool_clicked,
            'on_tool_r_clicked': self._on_tool_r_clicked,
            'on_tool_enter': self._on_tool_enter,
            'on_update_ui': self._on_update_ui,
        }

    @local_tool_kwargs.setter
    def local_tool_kwargs(self, kwargs):
        self._label= kwargs.get('label', self._label)
        self._bitmap = kwargs.get('bitmap', self._bitmap)
        self._bitmap_disabled = kwargs.get('bitmap_disabled', self._bitmap_disabled)
        self._kind= kwargs.get('kind', self._kind)
        self._tooltip = kwargs.get('tooltip', self._tooltip)
        self._status_bar = kwargs.get('status_bar', self._status_bar)
        self._on_tool_clicked = kwargs.get('on_tool_clicked', self._on_tool_clicked)
        self._on_tool_r_clicked = kwargs.get('on_tool_r_clicked', self._on_tool_r_clicked)
        self._on_tool_enter = kwargs.get('on_tool_enter', self._on_tool_enter)
        self._on_update_ui =kwargs.get('on_update_ui', self._on_update_ui)

    @property
    def local_tool_control_events(self):
        """return the toolbar control events mapping"""
        return {
            'on_tool_clicked':(self._on_tool_clicked, 'wx.EVT_TOOL', 'wxEVT_TOOL'),
            'on_tool_r_clicked':(self._on_tool_r_clicked,'wx.EVT_TOOL_RCLICKED', 'wxEVT_TOOL_RCLICKED'),
            'on_tool_enter': (self._on_tool_enter,'wx.EVT_TOOL_ENTER', 'wxEVT_TOOL_ENTER'),
            'on_update_ui': (self._on_update_ui,'wx.EVT_UPDATE_UI', 'wxEVT_UPDATE_UI')
        }

    @property
    def events(self):
        value = self.local_tool_control_events
        value.update(super(ToolControl, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_tool_kwargs
        value.update(super(ToolControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_tool_kwargs = kwargs
        super(ToolControl, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('tool')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Tool bar item',
            'type': 'category',
            'help': 'Attributes of tool bar item.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The menu name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'The menu label.'
                },
                {
                    'label': 'bitmap',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._bitmap,
                    'name': 'bitmap',  # kwarg value
                    'help': "The tool bitmap."
                },
                {
                    'label': 'bitmap disabled',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._bitmap_disabled,
                    'name': 'bitmap_disabled',  # kwarg value
                    'help': "The tool bitmap when disabled. This is often not specified and computed automatically."
                },
                {
                    'label': 'tooltip',
                    'type': 'string',
                    'read_only': False,
                    'value': self._tooltip,
                    'name': 'tooltip',  # kwarg value
                    'help': 'The tooltip show when the mouse hoover.'
                },
                {
                    'label': 'status bar',
                    'type': 'string',
                    'read_only': False,
                    'value': self._status_bar,
                    'name': 'status_bar',  # kwarg value
                    'help': 'The text shown when in the status bar.'
                },
                {
                    'label': 'id',
                    'type': 'string',
                    'read_only': False,
                    'value': self._id,
                    'name': 'id',  # kwarg value
                    'help': 'Control identifier.'
                },
                {
                    'label': 'kind',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._kind,
                    'name': 'kind',  # kwarg value
                    'help': 'enabled status.',
                    'choices': ['wxITEM_CHECK', 'wxITEM_NORMAL', 'wxITEM_RADIO']
                }
            ]
        }]
        _event = [{
            'label': 'toolbar item',
            'type': 'category',
            'help': 'Events of tool element',
            'child': [
                {
                    'label': 'on click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tool_clicked,
                    'name': 'on_tool_clicked',  # kwarg value
                    'help': "Handles the click over the tool. If the tool identifier matches a menu item"
                            "identifier, this method must be the same of the menu item."
                },
                {
                    'label': 'on right click',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tool_r_clicked,
                    'name': 'on_tool_r_clicked',  # kwarg value
                    'help': 'Handles the click with the right button of the mouse.'
                },
                {
                    'label': 'on enter',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_tool_enter,
                    'name': 'on_tool_enter',  # kwarg value
                    'help': 'Handles a enter command.'
                },
                {
                    'label': 'on update ui',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_update_ui,
                    'name': 'on_update_ui',  # kwarg value
                    'help': 'Handles a ui update invoked periodically for updating the ui state.'
                },
            ]

        }]
        # For this kind of object the inherited stuff could be skipped
        # _base_property, _base_event = super(MenuItemControl, self).model_attributes
        # _property += _base_property
        return _property, _event

    @property
    def kind_value(self):
        kind_dict = {'wxITEM_CHECK': wx.ITEM_CHECK, 'wxITEM_NORMAL': wx.ITEM_NORMAL, 'wxITEM_RADIO': wx.ITEM_RADIO}
        return kind_dict.get(self._kind, wx.ITEM_NORMAL)

    def declare_cpp_variable(self, container):
        """Create the data objects that implement the element"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxToolBarToolBase', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container):
        self.add_cpp_required_header('#include <wx/toolbar.h>')
        bitmap = get_bitmap_cpp_text_from_tuple(self._bitmap) or 'wxNullBitmap'
        bitmap_disabled = get_bitmap_cpp_text_from_tuple(self._bitmap_disabled) or 'wxNullBitmap'
        tooltip = (self._tooltip and '"{s}"'.format(s=self._tooltip)) or 'wxEmptyString'
        status_bar = (self._status_bar and '"{s}"'.format(s=self._status_bar)) or 'wxEmptyString'
        return f'{self._name} = {container}->AddTool({self._id}, wxT("{self._label}"), {bitmap}, {bitmap_disabled}, ' \
               f'{self._kind}, {tooltip}, {status_bar}, NULL );\n'

    def python_init_control_code(self, container='self'):
        kind_map = {
            'wxITEM_CHECK': 'wx.ITEM_CHECK',
            'wxITEM_NORMAL': 'wx.ITEM_NORMAL',
            'wxITEM_RADIO': 'wx.ITEM_RADIO'
        }
        tooltip = (self._tooltip and '"{s}"'.format(s=self._tooltip)) or 'wx.EmptyString'
        status_bar = (self._status_bar and '"{s}"'.format(s=self._status_bar)) or 'wx.EmptyString'
        bitmap = get_bitmap_py_text_from_tuple(self._bitmap) or 'wx.NullBitmap'
        bitmap_disabled = get_bitmap_cpp_text_from_tuple(self._bitmap_disabled) or 'wx.NullBitmap'
        return f'self.{self._name} = {container}.AddTool({self.py_id_str}, "{self._label}", {bitmap}, ' \
               f'{bitmap_disabled}, {kind_map[self._kind]}, {tooltip},{status_bar}, None )\n'

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        tool_bar = getattr(self.parent, '_edition_window', None)
        if tool_bar is None:
            return
        kind_map = {
            'wxITEM_CHECK': wx.ITEM_CHECK,
            'wxITEM_NORMAL': wx.ITEM_NORMAL,
            'wxITEM_RADIO': wx.ITEM_RADIO
        }
        bitmap = get_bitmap_from_tuple(self._bitmap, self.project) or wx.ArtProvider.GetBitmap(wx.ART_MISSING_IMAGE, wx.ART_TOOLBAR)
        bitmap_disabled = get_bitmap_from_tuple(self._bitmap_disabled, self.project) or wx.NullBitmap
        tooltip = self._tooltip or wx.EmptyString
        status_bar = self._status_bar or wx.EmptyString
        self._edition_window = tool_bar.AddTool(self.py_id, self._label, bitmap, bitmap_disabled,
                                                kind_map[self._kind],
                                                tooltip, status_bar, None)

    def destroy_edition_window(self):
        if self._edition_window is None:
            return
        tool_bar = getattr(self.parent, '_edition_window', None)
        if tool_bar is None:
            return
        tool_bar.DeleteTool(self._edition_window.GetId())
        self._edition_window = None

    def py_bind_text(self, event):
        """return the suitable code for binding an event"""
        return 'self.Bind({event[1]},self.{event[0]}, id=self.{name}.GetId())\n'.format(
            event=event, name=self._name
        )

    def py_unbind_text(self, event):
        """return the suitable code for binding an event"""
        return 'self.Unbind({event[1]},self.{event[0]}, id=self.{name}.GetId())\n'.format(
            event=event, name=self._name
        )

