from ..._Window import Window
from beatle.model import cc, py


class MenuControl(Window):
    """This class represents a menu"""
    def __init__(self, **kwargs):
        self._label = 'MyMenu'
        super(MenuControl, self).__init__(**kwargs)

    @property
    def children_count(self):
        """Get the total count of elements"""
        return sum((len(self.children[x]) for x in self.children))

    @property
    def can_add_submenu_control(self):
        return self._edition_window is not None

    @property
    def can_add_menu_item_control(self):
        return self._edition_window is not None

    @property
    def local_menucontrol_kwargs(self):
        return {'label': self._label}

    @local_menucontrol_kwargs.setter
    def local_menucontrol_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)

    @property
    def kwargs(self):
        value = self.local_menucontrol_kwargs
        value.update(super(MenuControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_menucontrol_kwargs = kwargs
        super(MenuControl, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.update_implementation()


    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('menu')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Menu',
            'type': 'category',
            'help': 'Attributes of menu.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The menu name"
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'The menu label'
                },
            ]
        }]
        return _property, []

    def declare_cpp_variable(self, container):
        """Create the data objects that implement the element"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxMenu', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container):
        self.add_cpp_required_header('#include <wx/menu.h>')
        code = f'{self._name} =  new wxMenu();\n'
        # get the children sorted by name
        child = self.sorted_wxui_child
        for item in child:
            code += item.cpp_init_code(self)
        code += f'{container.name}->Append({self._name}, wxT("{self._label}"));\n'
        return code

    def python_init_control_code(self, parent='self'):
        this = f'self.{self._name}'
        code = '{this} = wx.Menu()\n'.format(this=this)
        for item in self.sorted_wxui_child:
            code += item.python_init_control_code(this)
        code += f'{parent}.Append({this}, "{self._label}")\n'
        return code

    def create_edition_window(self, parent):
        from ....ui import uiFakeMenu
        menu_bar = getattr(self.parent, '_edition_window', None)
        if menu_bar is None:
            return
        self._edition_window = uiFakeMenu(menu_bar, self)
        menu_bar.Append(self._edition_window, self._label)

        # get the children sorted by name
        child = self.sorted_wxui_child
        # now, create child elements
        for item in child:
            item.create_edition_window(self)
