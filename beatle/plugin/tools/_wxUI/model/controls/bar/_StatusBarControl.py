import wx
from ..._Window import Window
from beatle.model import cc
from ....lib import get_colour_cc_text_from_tuple

class StatusBarControl(Window):
    """"This class represents a menu bar as control"""
    style_list =  ( wx.STB_SIZEGRIP, wx.STB_SHOW_TIPS, wx.STB_ELLIPSIZE_START, wx.STB_ELLIPSIZE_MIDDLE,
                   wx.STB_ELLIPSIZE_END, wx.STB_DEFAULT_STYLE)

    style_list_py_text = (
        'wx.STB_SIZEGRIP', 'wx.STB_SHOW_TIPS', 'wx.STB_ELLIPSIZE_START', 'wx.STB_ELLIPSIZE_MIDDLE',
        'wx.STB_ELLIPSIZE_END', 'wx.STB_DEFAULT_STYLE')

    style_list_cc_text = (
        'wxSTB_SIZEGRIP', 'wxSTB_SHOW_TIPS', 'wxSTB_ELLIPSIZE_START', 'wxSTB_ELLIPSIZE_MIDDLE',
        'wxSTB_ELLIPSIZE_END', 'wxSTB_DEFAULT_STYLE')

    def __init__(self, **kwargs):
        # -- initialize data with default value
        self._fields = 1
        self._status_bar_style = 1  # default to size_grip
        super(StatusBarControl, self).__init__(**kwargs)

    @property
    def local_statusbarcontrol_kwargs(self):
        return {
            'fields': self._fields,
            'status_bar_style': self._status_bar_style,
        }

    @local_statusbarcontrol_kwargs.setter
    def local_statusbarcontrol_kwargs(self, kwargs):
        self._fields = kwargs.get('fields', self._fields)
        self._status_bar_style = kwargs.get('status_bar_style', self._status_bar_style)

    @property
    def kwargs(self):
        value = self.local_statusbarcontrol_kwargs
        value.update(super(StatusBarControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_statusbarcontrol_kwargs = kwargs
        super(StatusBarControl, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.update_implementation()

    def declare_cpp_variable(self, container):
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxStatusBar', ptr=True),
            default='nullptr'
        )

    def python_init_control_code(self, parent='self'):
        """This is the code for creating the status bar"""
        code = f'self.{self._name} = {parent}.CreateStatusBar({self._fields}, ' \
               f'{self.py_style_str}, {self.py_id_str})\n'
        code += super(StatusBarControl, self).python_init_control_code()
        return code

    def cpp_init_code(self):
        self.add_cpp_required_header('#include <wx/statusbr.h>')
        code = f'{self._name} =  CreateStatusBar({self._fields}, {self.cc_style_str}, {self._id});\n'
        code += super(StatusBarControl, self).cpp_init_code()
        return code

    @property
    def style_value(self):
        """Return the value required for dynamic button creation"""
        control_style_value = 0
        mask = 1
        for index in range(len(StatusBarControl.style_list)):
            if self._style & mask:
                control_style_value |= StatusBarControl.style_list[index]
            mask *= 2
        return control_style_value | super(StatusBarControl, self).style_value

    @property
    def py_style_str(self):
        if self._status_bar_style == 0:
            return super(StatusBarControl, self).py_style_str
        sz = len(StatusBarControl.style_list_py_text)
        control_style_value = '|'.join(
            StatusBarControl.style_list_py_text[index] for index in range(sz)
            if self._status_bar_style & (2 ** index)
        )
        base = super(StatusBarControl, self).py_style_str
        if base == '0':
            return control_style_value
        return '|'.join((control_style_value, base))

    def create_edition_window(self, parent):
        self._edition_window = parent.create_status_bar()
        self._edition_window.bind(self)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('status_bar')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Status bar',
            'type': 'category',
            'help': 'Attributes of status bar.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The status bar variable name"
                },
                {
                    'label': 'fields',
                    'type': 'integer',
                    'read_only': False,
                    'value': self._fields,
                    'name': 'fields',  # kwarg value
                    'help': 'The status bar number of fields'
                },
                {
                    'label': 'style',
                    'type': 'multi_choice',
                    'value': self._status_bar_style,
                    'name': 'status_bar_style',
                    'help': 'The status bar style',
                    'options': [
                        ('wxSTB_SIZEGRIP', 'Displays a gripper at the right-hand side of the status bar which '
                                           'can be used to resize the parent window.'),
                        ('wxSTB_SHOW_TIPS', 'Displays tooltips for those panes whose status text has been '
                                             'ellipsized/truncated because the status text doesn’t fit the pane '
                                             'width. Note that this style has effect only on wxGTK (with GTK+ >= 2.12) '
                                             'currently.'),
                        ('wxSTB_ELLIPSIZE_START', 'Replace the beginning of the status texts with an ellipsis when '
                                                   'the status text widths exceed the status bar pane’s widths '
                                                   '(uses wx.Control.Ellipsize ).'),
                        ('wxSTB_ELLIPSIZE_MIDDLE', 'Replace the middle of the status texts with an ellipsis when the '
                                                    'status text widths exceed the status bar pane\’s widths '
                                                    '(uses wx.Control.Ellipsize ).'),
                        ('wxSTB_ELLIPSIZE_END', 'Replace the end of the status texts with an ellipsis when the status '
                                                 'text widths exceed the status bar pane’s widths '
                                                 '(uses wx.Control.Ellipsize ).'),
                        ('wxSTB_DEFAULT_STYLE', 'The default style: includes STB_SIZEGRIP | wxSTB_SHOW_TIPS | '
                                                 'wxSTB_ELLIPSIZE_END | wxFULL_REPAINT_ON_RESIZE .')

                    ]
                }
            ]
        }]
        _event = []
        _base_property, _base_event = super(StatusBarControl, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event
