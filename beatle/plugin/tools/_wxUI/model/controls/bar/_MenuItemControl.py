import wx
from ..._Window import Window
from beatle.model import cc, py
from beatle.lib.tran import TransactionStack
from ....lib import get_bitmap_py_text_from_tuple, get_bitmap_cpp_text_from_tuple, get_bitmap_from_tuple


class MenuItemControl(Window):

    def __init__(self, **kwargs):
        """Creates a new menu item"""
        self._label = 'MyMenuItem'
        self._shortcut = ''
        self._help = ''
        self._bitmap = (0, '')
        self._unchecked_bitmap = ''
        self._checked = False
        self._enabled = True
        self._kind = 'wxITEM_NORMAL'

        # update ui
        self._on_menu_selection = ''
        self._on_update_ui = ''

        self._on_menu_selection_method = None
        self._on_menu_update_ui_method = None
        super(MenuItemControl, self).__init__(**kwargs)

    @property
    def local_menuitem_kwargs(self):
        return {
            'label': self._label,
            'shortcut': self._shortcut,
            'help': self._help,
            'bitmap': self._bitmap,
            'checked': self._checked,
            'enabled': self._enabled,
            'kind': self._kind,
            'on_menu_selection': self._on_menu_selection,
            'on_update_ui': self._on_update_ui,
        }

    @property
    def local_menuitem_events(self):
        return {
            'on_menu_selection': (self._on_menu_selection, 'wx.EVT_MENU', 'wxEVT_MENU'),
            'on_update_ui': (self._on_update_ui, 'wx.EVT_UPDATE_UI', 'wxEVT_UPDATE_UI'),
        }

    @property
    def events(self):
        return self.local_menuitem_events

    @local_menuitem_kwargs.setter
    def local_menuitem_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)
        self._shortcut = kwargs.get('shortcut', self._shortcut)
        self._help = kwargs.get('help', self._help)
        self._bitmap = kwargs.get('bitmap', self._bitmap)
        self._checked = kwargs.get('checked', self._checked)
        self._enabled = kwargs.get('enabled', self._enabled)
        self._kind = kwargs.get('kind', self._kind)

        self._on_menu_selection = kwargs.get('on_menu_selection', self._on_menu_selection)
        self._on_update_ui = kwargs.get('on_update_ui', self._on_update_ui)

    @property
    def kwargs(self):
        value = self.local_menuitem_kwargs
        value.update(super(MenuItemControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_menuitem_kwargs = kwargs
        super(MenuItemControl, self).set_kwargs(kwargs)
        if self._implementation:
            self._implementation.save_state()
            self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('menu_item')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Menu item',
            'type': 'category',
            'help': 'Attributes of menu item.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The menu name."
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'The menu label.'
                },
                {
                    'label': 'shortcut',
                    'type': 'string',
                    'read_only': False,
                    'value': self._shortcut,
                    'name': 'shortcut',  # kwarg value
                    'help': 'optional keyboard shortcut.'
                },
                {
                    'label': 'help',
                    'type': 'string',
                    'read_only': False,
                    'value': self._help,
                    'name': 'help',  # kwarg value
                    'help': 'Help that will be shown on status bar.'
                },
                {
                    'label': 'id',
                    'type': 'string',
                    'read_only': False,
                    'value': self._id,
                    'name': 'id',  # kwarg value
                    'help': 'Control identifier.'
                },
                {
                    'label': 'bitmap',
                    'type': 'bitmap',
                    'read_only': False,
                    'value': self._bitmap,
                    'name': 'bitmap',  # kwarg value
                    'help': "The menu item bitmap."
                },
                {
                    'label': 'checked',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._checked,
                    'name': 'checked',  # kwarg value
                    'help': 'checking status.'
                },
                {
                    'label': 'enabled',
                    'type': 'boolean',
                    'read_only': False,
                    'value': self._enabled,
                    'name': 'enabled',  # kwarg value
                    'help': 'enabled status.'
                },
                {
                    'label': 'kind',
                    'type': 'choice',
                    'read_only': False,
                    'value': self._kind,
                    'name': 'kind',  # kwarg value
                    'help': 'enabled status.',
                    'choices': ['wxITEM_CHECK', 'wxITEM_NORMAL', 'wxITEM_RADIO']
                }
            ]
        }]
        _event = [{
            'label': 'Menu item',
            'type': 'category',
            'help': 'Events of menu item.',
            'child': [
                {
                    'label': 'on menu selection',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_menu_selection,
                    'name': 'on_menu_selection',  # kwarg value
                    'help': "Handles menu item selection"
                },
                {
                    'label': 'on update ui',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_update_ui,
                    'name': 'on_update_ui',  # kwarg value
                    'help': 'Updates menu item visual status'
                },

            ]

        }]
        # For this kind of object the inherited stuff could be skipped
        # _base_property, _base_event = super(MenuItemControl, self).model_attributes
        # _property += _base_property
        return _property, _event

    @property
    def kind_value(self):
        kind_dict = {'wxITEM_CHECK': wx.ITEM_CHECK, 'wxITEM_NORMAL': wx.ITEM_NORMAL, 'wxITEM_RADIO': wx.ITEM_RADIO}
        return kind_dict.get(self._kind, wx.ITEM_NORMAL)

    def declare_cpp_variable(self, container):
        """Create the data objects that implement the element"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxMenuItem', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container):
        self.add_cpp_required_header('#include <wx/menuitem.h>')
        label = self._label
        if len(self._shortcut) > 0:
            label += r"\t"+self._shortcut

        code = f'{self._name} =  new wxMenuItem({container.name}, {self._id}, ' \
               f'wxString(wxT("{label}")),wxT("{self._help}"),{self._kind});\n'

        bitmap_text = get_bitmap_cpp_text_from_tuple(self._bitmap)
        if len(bitmap_text):
            self.add_cpp_required_header('#include <wx/artprov.h>')
            code += "#ifdef __WXMSW__\n" \
                    "{self._name}->SetBitmaps({bitmap_text});\n" \
                    "#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))\n" \
                    "{self._name}->SetBitmap({bitmap_text});\n" \
                    "#endif\n".format(self=self, bitmap_text=bitmap_text)

        if self._checked:
            code += f'{self.name}->Check( true );\n'
        if self._kind != 'wxITEM_NORMAL':
            if self._checked:
                code += f'{self.name}->Check( true );\n'
        if not self._enabled:
            code += f'{self.name}->Enable( false );\n'

        code += '{container.name}->Append({self._name});\n'.format(self=self, container=container)
        return code

    def python_init_control_code(self, parent='self'):
        kind_map = {
            'wxITEM_CHECK': 'wx.ITEM_CHECK',
            'wxITEM_NORMAL': 'wx.ITEM_NORMAL',
            'wxITEM_RADIO': 'wx.ITEM_RADIO'
        }
        this = 'self.{name}'.format(name=self._name)
        if len(self._shortcut):
            text = '"{self._label}\\t{self._shortcut}"'.format(self=self)
        else:
            text = '"{self._label}"'.format(self=self)
        if len(self._help):
            help_ = '"{self._help}"'.format(self=self)
        else:
            help_ = "wx.EmptyString"

        bitmap_text = get_bitmap_py_text_from_tuple(self._bitmap)
        if len(bitmap_text):
            code = '{this} = wx.MenuItem({parent}, {sid}, {text}, {help}, {kind})\n' \
                   '{this}.SetBitmap({bitmap_text})\n' \
                   '{parent}.Append({this})\n'.format(
                        this=this, parent=parent, sid=self.py_id_str, self=self, text=text, help=help_,
                        kind=kind_map[self._kind], bitmap_text=bitmap_text
            )
        else:
            code = '{this} = wx.MenuItem({parent}, {sid}, {text}, {help}, {kind})\n' \
                   '{parent}.Append({this})\n'.format(
                        this=this, parent=parent, sid=self.py_id_str, self=self, text=text, help=help_,
                        kind=kind_map[self._kind]
            )
        if self._kind != 'wxITEM_NORMAL':
            if self._checked:
                code += '{this}.Check(True)\n'.format(this=this)
        if not self._enabled:
            code += '{this}.Enable(False)\n'.format(this=this)
        return code

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl, uiFakeMenuItem
        menu = getattr(self.parent, '_edition_window', None)
        if menu is not None:
            bitmap = get_bitmap_from_tuple(self._bitmap)
            self._edition_window = uiFakeMenuItem(menu, label=self._label, kind=self.kind_value)
            # self._edition_window = uiMockupControl(wxx.MenuItem)(
            #    menu, label=self._label, helpString=self._help) # , kind=self.kind_value)
            if bitmap is not None:
                self._edition_window.SetBitmap(bitmap)
            menu.AppendItem(self._edition_window)
            if self._checked and self.kind_value != wx.ITEM_NORMAL:
                self._edition_window.Check(True)
            self._edition_window.bind(self)

    def destroy_edition_window(self):
        """Menuitems aren't real windows. We must delete it by special method"""
        menu = getattr(self.parent, '_edition_window', None)
        if menu is not None:
            menu.DestroyItem(self._edition_window)
            self._edition_window = None

    def py_bind_text(self, event):
        """return the suitable code for binding an event"""
        return 'self.Bind({event[1]},self.{event[0]}, id=self.{name}.GetId())\n'.format(
            event=event, name=self._name
        )

    def py_unbind_text(self, event):
        """return the suitable code for binding an event"""
        return 'self.Unbind({event[1]},self.{event[0]}, id=self.{name}.GetId())\n'.format(
            event=event, name=self._name
        )
