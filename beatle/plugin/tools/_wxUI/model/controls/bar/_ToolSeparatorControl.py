from beatle.model import cc
from ..._Window import Window


class ToolSeparatorControl(Window):
    """This class represents a tool separator inside the toolbar.
    This kind of object is fake in wxformbuilder, in the sense of that
    there is not storage for it. In wxUI this is a little different from
    now and we follow the standard procedure."""

    def __init__(self, **kwargs):
        super(ToolSeparatorControl, self).__init__(**kwargs)

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('tool_separator')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'toolbar item',
            'type': 'category',
            'help': 'Attributes of toolbar item',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The menu name."
                }]
        },]
        return _property, []

    def declare_cpp_variable(self, container):
        """Create the data objects that implement the element"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxToolBarToolBase', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container):
        self.add_cpp_required_header('#include <wx/toolbar.h>')
        return f'{self._name} = {container}->AddSeparator();\n'

    def python_init_control_code(self, container='self'):
        return f'self.{self._name} = {container}.AddSeparator()\n'

    def create_edition_window(self, parent):
        from ....ui import uiMockupControl
        tool_bar = getattr(self.parent, '_edition_window', None)
        if tool_bar is None:
            return
        this = 'self._name'.format(self=self)
        self._edition_window = tool_bar.AddSeparator()

    def destroy_edition_window(self):
        if self._edition_window is None:
            return
        tool_bar = getattr(self.parent, '_edition_window', None)
        if tool_bar is None:
            return
        tool_bar.DeleteTool(self._edition_window.GetId())
        self._edition_window = None





