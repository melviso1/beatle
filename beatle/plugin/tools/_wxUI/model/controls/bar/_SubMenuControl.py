from beatle.lib import wxx
from ..._Window import Window
from beatle.model import cc, py


class SubMenuControl(Window):
    """This class represents a menu"""
    def __init__(self, **kwargs):
        self._label = 'MyMenu'
        super(SubMenuControl, self).__init__(**kwargs)

    @property
    def children_count(self):
        """Get the total count of elements"""
        return sum((len(self.children[x]) for x in self.children))

    @property
    def can_add_submenu_control(self):
        return self._edition_window is not None

    @property
    def can_add_menu_item_control(self):
        return self._edition_window is not None

    @property
    def local_submenucontrol_kwargs(self):
        return {'label': self._label}

    @local_submenucontrol_kwargs.setter
    def local_submenucontrol_kwargs(self, kwargs):
        self._label = kwargs.get('label', self._label)

    @property
    def kwargs(self):
        value = self.local_submenucontrol_kwargs
        value.update(super(SubMenuControl, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_submenucontrol_kwargs = kwargs
        super(SubMenuControl, self).set_kwargs(kwargs)
        if self._implementation:
            sanitized_name = self.name.strip()
            self._implementation.name = self._name
            self.update_implementation()

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('submenu')

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'Menu',
            'type': 'category',
            'help': 'Attributes of menu.',
            'child': [
                {
                    'label': 'name',
                    'type': 'string',
                    'read_only': False,
                    'value': self._name,
                    'name': 'name',  # kwarg value
                    'help': "The menu name"
                },
                {
                    'label': 'label',
                    'type': 'string',
                    'read_only': False,
                    'value': self._label,
                    'name': 'label',  # kwarg value
                    'help': 'The menu label'
                },
            ]
        }]
        _event = []
        _base_property, _base_event = super(SubMenuControl, self).model_attributes
        _property += _base_property
        return _property, _event

    def declare_cpp_variable(self, container):
        """Create the data objects that implement the element"""
        self._implementation = cc.MemberData(
            parent=container,
            name=self.name,
            read_only=True,
            access='private',
            type=cc.typeinst(type_alias='wxMenu', ptr=True),
            default='nullptr'
        )

    def cpp_init_code(self, container):
        self.add_cpp_required_header('#include <wx/menu.h>')
        code = f'{self._name} =  new wxMenu();\n'
        code += f'wxMenuItem * {self._name}_item = new wxMenuItem({container.name}, ' \
                f'wxID_ANY, wxT("{self._label}"), wxEmptyString, wxITEM_NORMAL, ' \
                f'{self._name});\n'
        # get the children sorted by name
        child = self.sorted_wxui_child
        for item in child:
            code += item.cpp_init_code(self)
        code += f'{container.name}->Append({self._name}_item);\n'
        return code

    def python_init_control_code(self, parent='self'):
        this = 'self.{name}'.format(name=self._name)
        code = '{this} =  wx.Menu()\n'.format(this=this)
        for item in self.sorted_wxui_child:
            code += item.python_init_control_code(this)
        code += '{parent}.AppendSubMenu({this},"{self._label}")\n'.format(parent=parent, this=this, self=self)
        return code

    def create_edition_window(self, parent):
        from ....ui import uiFakeMenu
        menu_parent = getattr(self.parent, '_edition_window', None)
        if menu_parent is None:
            return
        self._edition_window = uiFakeMenu(menu_parent, self)
        menu_parent.AppendSubMenu(self._edition_window, self._label)

        # get the children sorted
        child = self.sorted_wxui_child

        # now, create child elements
        for item in child:
            item.create_edition_window(self)

    def destroy_edition_window(self):
        """Submenu controls are deleted by removing the internal attach item"""
        menu = getattr(self.parent, '_edition_window', None)
        if menu is not None:
            if self._edition_window is not None:
                menu.DestroyItem(self._edition_window.submenu_menuitem)
            self._edition_window = None
