from beatle.model.cc import MemberMethod


# noinspection PyPep8Naming
class ccEventHandler(MemberMethod):
    """This class extends MemberMethod for synchronize changes with ui model"""

    def __init__(self, **kwargs):
        self._parent_container = kwargs.get('parent_container', None)
        self._parent_attribute = kwargs.get('parent_attribute', None)
        super(ccEventHandler, self).__init__(**kwargs)

    def on_undo_redo_changed(self):
        if self._parent_container and self._parent_attribute:
            setattr(self._parent_container, self._parent_attribute, self.name)
        super(ccEventHandler, self).on_undo_redo_changed()

    def delete(self):
        if getattr(self, 'recursive_call', False):
            return
        setattr(self, 'recursive_call', True)
        super(ccEventHandler, self).delete()
        if self._parent_container and self._parent_attribute:
            self._parent_container.save_state()
            setattr(self._parent_container, self._parent_attribute, '')
            self._parent_container.update_cpp_event_handlers()
            self._parent_container.update_cpp_connect_event()
            self._parent_container.update_cpp_disconnect_event()
        delattr(self, 'recursive_call')


