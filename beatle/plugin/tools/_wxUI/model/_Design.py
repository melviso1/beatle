# -*- coding: utf-8 -*-
import copy
from ._wxUICommon import wxUICommon
from ..lib import register_cpp_dependencies
from beatle.lib.tran import TransactionalMoveObject, DelayedMethod
from beatle.lib.api import context
from beatle.lib.decorators import upgrade_version


class Design(wxUICommon):
    
    """This class represents a UI Design"""

    def drop(self, to):
        """This is an interesting feature, allowing the model to be moved
        between different projects. This is not as easy to implement as
        their usage final. The key point is to remove all the
        implementation (do it as transactional move, saving state) and
        take account of the types and also recreate the structure on final
        target."""
        """Drops data-member inside project or another folder """
        target = to.inner_module_container
        if not target or self.project != target.project:
            return False  # avoid move classes between projects
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target, index=target.index(to))
        return True

    def __init__(self, **kwargs):
        self._use_beatle_identifiers = False
        self._first_identifier_value = 1000
        self._next_identifier_value = self._first_identifier_value
        self._internationalize = True
        self._control_counters = {}
        self._identifier_map = {}
        super(Design, self).__init__(**kwargs)
        self.local_design_kwargs = kwargs
        if self.project.language == 'c++':
            register_cpp_dependencies(self.project)

    def __getstate__(self):
        # The edition windows can't be pickled
        state = copy.copy(self.__dict__)
        if '_pane' in state:
            del state['_pane']
        return state

    @upgrade_version
    def __setstate__(self, data_dict):
        return {
            'add': {
                '_next_identifier_value': 1000,
                '_identifier_map' : {}
            },
        }

    def identifier(self, key):
        if key not in self._identifier_map:
            self._identifier_map[key] = self._next_identifier_value
            self._next_identifier_value += 1
        return self._identifier_map[key]

    @property
    def use_beatle_identifiers(self):
        return self._use_beatle_identifiers

    @use_beatle_identifiers.setter
    def use_beatle_identifiers(self, value):
        self._use_beatle_identifiers = value

    @property
    def first_identifier_value(self):
        return self._first_identifier_value

    @first_identifier_value.setter
    def first_identifier_value(self, value):
        self._first_identifier_value = value

    @property
    def dir(self):
        """return the container dir"""
        return self.parent.dir

    @property
    def document(self):
        return self

    @property
    def view(self):
        return getattr(self, '_pane', None)

    @property
    def local_design_kwargs(self):
        return {
            'use_beatle_identifiers': self._use_beatle_identifiers,
            'first_identifier_value': self._first_identifier_value,
            'internationalize': self._internationalize
        }

    @local_design_kwargs.setter
    def local_design_kwargs(self, kwargs):
        self._use_beatle_identifiers = kwargs.get('use_beatle_identifiers', self._use_beatle_identifiers)
        self._first_identifier_value = kwargs.get('first_identifier_value', self._first_identifier_value)
        self._internationalize = kwargs.get('internationalize', self._internationalize)

    @property
    def kwargs(self):
        value = self.local_design_kwargs
        value.update(super(Design, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_design_kwargs = kwargs
        super(Design, self).set_kwargs(kwargs)

    def next_count(self, class_type):
        """Return a self incremented counter based on type"""
        value = self._control_counters.get(class_type, 1)
        self._control_counters[class_type] = value + 1
        return value

    def implement(self):
        pass

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        The returned informtation is composed by two arrays, properties and events.
        Each array is a dictionary. The properties array has the following structure:

            {(<category-label-1>, <category-help-1>):<property-list-1>,
            (<category-label-2>, <category-help-2>):<property-list-2>,...}

        where

            category-label-x is a string used as label of the category
            category-help-x is a string that would be displayed in the property grid editor
            property-list-x is a dict of properties with the structure

            {(<property-name-1>, <property-help-1>):<value-edition-1>,
             (<property-name-2>, <property-help-2>):<value-edition-2>,...}

        where property-name-x and property-help-x plays similar role as category-label-x and
        category-help-x and value-edition-x is a list that depends on the values, but the
        basic structure is
            [ read_only_flag, current_value_string, value_type ]

        for read_only_flag = True, this list don't need other values. For read_only_flag = False,
        we can have other fields, but at least we need the following fields:

            [ read_only_flag, current_value_string, value_type, modification_callback ]

        The modification callback will receive a modification for the value and will be an object
        callback (transactional recommended).

        For value_type like 'string_options' we can also have a alternate_values field.
        """
        _property = [
            {
                'label': 'Project',
                'type': 'category',
                'help': 'Attributes of this wxUI model.',
                'child': [
                    {
                        'label': 'name',
                        'type': 'string',
                        'value': self.name,
                        'name': 'name',
                        'help': "ui model name"
                    },
                    {
                        'label': 'use beatle indentifiers',
                        'type': 'boolean',
                        'value': self._use_beatle_identifiers,
                        'name': 'use_beatle_identifiers',
                        'help':  '(python only) Use beatle identifiers.'
                    },
                    {
                        'label': 'first identifier value',
                        'type': 'integer',
                        'value': self._first_identifier_value,
                        'name': 'first_identifier_value',
                        'help': 'The starting value required for numerical identifiers.'
                    },
                    {
                        'label': 'intenationalize',
                        'type': 'boolean',
                        'value': self._internationalize,
                        'name': 'internationalize',
                        'help': 'Do a internationalization support on generated interfaces.'
                    }
                ]
            }
        ]
        _event = {}
        return _property, _event

    @DelayedMethod()
    def export_code_files(self, force=True, logger=None):
        """Export code files is called everywhere on changes. Design
        could support that at the future, as abstraction of the generation
        of inner classes as a whole set.
        For the moment, we rely on top_window containers as code generator.
        """
        pass

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("wx_design")

    @property
    def design_bitmap_index(self):
        from beatle.plugin.tools import wxUI
        return list(wxUI.image_map.keys()).index('wx_design')


    @property
    def inner_class(self):
        """Get the innermost class container"""
        return None

    @property
    def types(self):
        """This property is required to redirect queries to parent,
        because Design acts as a holder for generated classes."""
        return self.parent.types

    def on_undo_redo_changed(self):
        """Update from app"""
        self.update_tree_item(self)
        super(Design, self).on_undo_redo_changed()

    @DelayedMethod()
    def update_tree_item(self, item):
        """Called from on undo redo changed, this method allows for the
        change of the label if the pane is opened for this design"""
        if getattr(self, '_pane', False):
            self._pane.update_tree_item(item)

    @DelayedMethod()
    def update_grid_item(self, item):
        """Called from on undo redo changed, this method allows for the
        change of the properties"""
        if getattr(self, '_pane', False):
            selected_object = self._pane.selected_object
            hierarchy_item = item
            while hierarchy_item is not None:
                if selected_object is hierarchy_item:
                    self._pane.update_edit_properties()
                    return
                hierarchy_item = hierarchy_item.parent

    def on_undo_redo_removing(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            if index == book.GetSelection():
                setattr(self, '_page_index', -index)
            else:
                setattr(self, '_page_index', index)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Design, self).on_undo_redo_removing()

    def on_undo_redo_unloaded(self):
        """Prepare for delete"""
        this_pane = getattr(self, '_pane', None)
        if this_pane is not None:
            book = context.get_frame().docBook
            delattr(self, '_pane')
            index = book.GetPageIndex(this_pane)
            book.RemovePage(index)
            this_pane.pre_delete()    # avoid gtk-critical
            this_pane.Destroy()
        super(Design, self).on_undo_redo_unloaded()

    def on_undo_redo_add(self):
        """Restore object from undo"""
        super(Design, self).on_undo_redo_add()
        index = getattr(self, '_page_index', None)
        if index is not None:
            from ..ui import uiDesign
            frame = context.get_frame()
            book = frame.docBook
            this_pane = uiDesign(book, frame, self)
            setattr(self, '_pane',  this_pane)
            activate = False
            if index < 0:
                index = -index
                activate = True
            book.InsertPage(index, this_pane, self.tab_label, activate, self.bitmap_index)
            delattr(self, '_page_index')
