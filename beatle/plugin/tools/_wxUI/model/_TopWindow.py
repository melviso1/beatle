from beatle.model import cc, py
from beatle.lib.decorators import upgrade_version
from ._Window import Window


class TopWindow(Window):
    """This class behaves as base class of top windows."""

    def __init__(self, **kwargs):

        # -- initialize events with default values
        self._on_activate = ''
        self._on_activate_app = ''
        self._on_close = ''
        self._on_hibernate = ''
        self._on_iconize = ''
        self._on_idle = ''
        # -- initialize properties
        self._registered_identifiers = {}
        self._brief_description = None
        self._full_description = None
        super(TopWindow, self).__init__(**kwargs)

    @upgrade_version
    def __setstate__(self, data_dict):
        return {
            'add': {
                '_registered_identifiers': {},
                '_brief_description': None,
                '_full_description': None
            },
        }

    @property
    def local_topwindow_kwargs(self):
        return {
            'on_activate': self._on_activate,
            'on_activate_app': self._on_activate_app,
            'on_close': self._on_close,
            'on_hibernate': self._on_hibernate,
            'on_iconize': self._on_iconize,
            'on_idle': self._on_idle,
            'brief_description': self._brief_description,
            'full_description': self._full_description
        }

    @local_topwindow_kwargs.setter
    def local_topwindow_kwargs(self, kwargs):
        self._on_activate = kwargs.get('on_activate', self._on_activate)
        self._on_activate_app = kwargs.get('on_activate_app', self._on_activate_app)
        self._on_close = kwargs.get('on_close', self._on_close)
        self._on_hibernate = kwargs.get('on_hibernate', self._on_hibernate)
        self._on_iconize = kwargs.get('on_iconize', self._on_iconize)
        self._on_idle = kwargs.get('on_idle', self._on_idle)
        self._brief_description = kwargs.get('brief_description', self._brief_description)
        self._full_description = kwargs.get('full_description', self._full_description)

    @property
    def local_topwindow_events(self):
        return {
            'on_activate': (self._on_activate, 'wx.EVT_ACTIVATE', 'wxEVT_ACTIVATE'),
            'on_activate_app': (self._on_activate_app, 'wx.EVT_ACTIVATE_APP', 'wxEVT_ACTIVATE_APP'),
            'on_close': (self._on_close, 'wx.EVT_CLOSE', 'wxEVT_CLOSE'),
            'on_hibernate': (self._on_hibernate, 'wx.EVT_HIBERNATE', 'wxEVT_HIBERNATE'),
            'on_iconize': (self._on_iconize, 'wx.EVT_ICONIZE', 'wxEVT_ICONIZE'),
            'on_idle': (self._on_idle, 'wx.EVT_IDLE', 'wxEVT_IDLE')
        }

    @property
    def python_description(self):
        if self._brief_description:
            if self._full_description:
                return """{self._brief_description}\n{self._full_description}""".format(self=self)
            else:
                return """{self._brief_description}""".format(self=self)
        else:
            if self._full_description:
                return """{self._full_description}""".format(self=self)
            else:
                return None

    @property
    def events(self):
        value = self.local_topwindow_events
        value.update(super(TopWindow, self).events)
        return value

    @property
    def kwargs(self):
        value = self.local_topwindow_kwargs
        value.update(super(TopWindow, self).kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_topwindow_kwargs = kwargs
        super(TopWindow, self).set_kwargs(kwargs)

    @property
    def is_top_window(self):
        return True

    @property
    def edition_window_client(self):
        """This property must return the control to be used
        as container for the client area. This method must be
        implemented in each top window, because we are working
        with fake windows for emulation of final result, and
        the emulation container is not the real window"""
        assert "Please implement edition_window_client for the class {}".format(str(type(self)))

    @property
    def model_attributes(self):
        """This return a structure suitable for editing properties.
        """
        _property = [{
            'label': 'TopLevelWindow',
            'type': 'category',
            'help': 'Attributes of top level window.',
            'child': [
                {
                    'label': 'brief description',
                    'type': 'string',
                    'read_only': False,
                    'value': self._brief_description or '',
                    'name': 'brief_description',  # kwarg value
                    'help': "A short description about this window."
                },
                {
                    'label': 'full description',
                    'type': 'long_string',
                    'read_only': False,
                    'value': self._full_description or '',
                    'name': 'full_description',  # kwarg value
                    'caption': 'Long description',
                    'help': 'A detailed description about this window.',
                },
            ],
        },]
        _event = [{
            'label': 'TopLevelWindow',
            'type': 'category',
            'help': 'Events of top level window',
            'child': [
                {
                    'label': 'on_activate',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_activate,
                    'name': 'on_activate',  # kwarg value
                    'help': "Called when the window receives a wxEVT_ACTIVATE event."
                },
                {
                    'label': 'on_activate_app',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_activate_app,
                    'name': 'on_activate_app',  # kwarg value
                    'help': "Called when the window receives a wxEVT_ACTIVATE_APP event."
                },
                {
                    'label': 'on_close',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_close,
                    'name': 'on_close',  # kwarg value
                    'help': "Called when the window receives a close event."
                },
                {
                    'label': 'on_hibernate',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_hibernate,
                    'name': 'on_hibernate',  # kwarg value
                    'help': "Called when the window receives a hibernate event."
                },
                {
                    'label': 'on_iconize',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_iconize,
                    'name': 'on_iconize',  # kwarg value
                    'help': "Called when the window receives a wxEVT_ICONIZE event."
                },
                {
                    'label': 'on_idle',
                    'type': 'string',
                    'read_only': False,
                    'value': self._on_idle,
                    'name': 'on_idle',  # kwarg value
                    'help': "Called when the window receives a wxEVT_IDLE event."
                }
            ]
        }]
        _base_property, _base_event = super(TopWindow, self).model_attributes
        _property += _base_property
        _event += _base_event
        return _property, _event

    def register_python_identifier(self, ident):
        if ident in self._registered_identifiers:
            identifier_object = self._registered_identifiers[ident]
            count = getattr(identifier_object,'shared_usage', 0)
            setattr(identifier_object, 'shared_usage', count+1)
        else:
            if self.standard_py_id(ident) is not None:
                return None
            if self.document.use_beatle_identifiers:
                identifier_object = py.Data(
                    parent=self._implementation,
                    name=ident, value='identifier("{key}")'.format(key=ident))
            else:
                identifier_object = py.Data(
                    parent=self._implementation,
                    name=ident, value='{value}'.format(value=self.document.identifier(ident)))
            setattr(identifier_object, 'shared_usage', 1)
            self._registered_identifiers[ident] = identifier_object
        return identifier_object

    def unregister_python_identifier(self, ident):
        if ident in self._registered_identifiers:
            identifier_object = self._registered_identifiers[ident]
            count = getattr(identifier_object, 'shared_usage', 1)
            if count < 2:
                identifier_object.delete()
            else:
                setattr(identifier_object, 'shared_usage', count-1)

    def update_cpp_implementation(self):
        pass  # pending

    def update_python_implementation(self):
        _class = self._implementation[py.Class][0]
        _class.note = self.python_description or "This class implements a dialog and was created by wxUI."