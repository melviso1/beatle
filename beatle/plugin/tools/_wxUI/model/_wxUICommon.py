# -*- coding: utf-8 -*-
import copy
import wx.propgrid as pg
from beatle.model import TComponent, py
from beatle.lib.decorators import upgrade_version
from beatle.lib.tran import TransactionStack, DelayedMethod, TransactionalMethod, TransactionalMoveObject
from beatle.lib.api import context


# noinspection PyPep8Naming
class wxUICommon(TComponent):
    """This class represents a common base for wxUI components model"""

    def __init__(self, **kwargs):
        self._edition_window = None
        self._volatile_attributes = ['_edition_window']
        self._wxui_child_index = kwargs.get('wxui_child_index', -1)
        self._implementation = None
        self._subitem_adaptor = None
        super(wxUICommon, self).__init__(**kwargs)
        if self._wxui_child_index == -1 and isinstance(self.parent, wxUICommon):
            self._wxui_child_index = self.parent.last_wxui_child_index + 1

    @TransactionalMethod('move {0}')
    def drop(self, target):
        """Drops class inside project or another folder """
        TransactionalMoveObject(
            object=self, origin=self.parent, target=target)
        return True

    @property
    def wxui_child_index(self):
        return self._wxui_child_index

    @wxui_child_index.setter
    def wxui_child_index(self, value):
        self._wxui_child_index = value

    @property
    def cc_style_str(self):
        return '0'

    @property
    def py_style_str(self):
        return '0'


    def exchange_wxui_index(self, obj, first=True):
        """Call it as self.exchange_wxui_index(other), never use the last
        parameter: it is intended for internal use only!"""
        other_index = obj.wxui_child_index
        if first:
            obj.exchange_wxui_index(self, False)
        self._wxui_child_index = other_index

    def implement(self):
        """Create the implementation"""
        language = getattr(self.project, 'language', None)
        if language == 'c++':
            self.implement_cpp()
        elif language == 'python':
            self.implement_python()

    #@DelayedMethod()
    def update_implementation(self):
        main_window = self.top_window
        if main_window is not None:
            assert(main_window != self)
            main_window.update_implementation()

    def implement_cpp(self):
        main_window = self.top_window
        if main_window is not None:
            self.declare_cpp_variable(main_window.controls_folder)
            main_window.update_implementation()

    def implement_python(self):
        main_window = self.top_window
        if main_window is not None:
            if hasattr(self, '_id'):
                self.implement_python_identifier()
            self.declare_python_variable(main_window.controls_folder)
            main_window.update_implementation()

    def declare_cpp_variable(self, container):
        """This must be implemented for all the components, declaring a c++ variable
        inside the container (usually, a folder inside a class)"""
        raise RuntimeError('missing declaration for type {}'.format(type(self)))

    def declare_python_variable(self, container):
        """This must be implemented for all the components, declaring a c++ variable
        inside the container (usually, a folder inside a class)"""
        raise RuntimeError('missing declaration for type {}'.format(type(self)))

    def add_cpp_required_header(self, include_string):
        """If this is a top window, add the include to the class. If not,
        doit at the inner top_window."""
        if self.is_top_window:
            includes = self.implementation.user_code_h1
            if include_string not in includes:
                self.implementation.user_code_h1 += f"{include_string}\n"
            elif self.implementation.user_code_h1.count(include_string) > 1:  # fix past repeated includes
                target = f"{include_string}\n"
                self.implementation.user_code_h1 = self.implementation.user_code_h1.replace(target, '')
                self.implementation.user_code_h1 += target
        else:
            top_window = self.top_window
            if top_window:
                top_window.add_cpp_required_header(include_string)

    def add_python_import(self, name):
        """Add simple import xxx"""
        if self.is_top_window:
            imports = self.implementation[py.Import]
            if len([x for x in imports if x.name == name]) > 0:
                return  # already imported
            py.Import(parent=self.implementation, name=name)
        else:
            top_window = self.top_window
            if top_window:
                top_window.add_python_import(name)

    def python_init_control_code(self, parent='self'):
        """This method must be implemented for all components, providing the code
        that initializes the instance variable of that control. This must include
        also the code generated from the child"""
        raise RuntimeError('missing declaration for type {}'.format(type(self)))

    @property
    def sorted_wxui_child(self):
        """Return the list of children sorted by index.
        Foreign child (like autogenerated classes and members) are omitted."""
        return sorted([x for y in self.children for x in self.children[y] if hasattr(x, 'wxui_child_index')],
                      key=lambda x: x.wxui_child_index)

    @property
    def last_wxui_child_index(self):
        a = self.sorted_wxui_child
        if len(a) == 0:
            return -1
        return a[-1].wxui_child_index

    @upgrade_version
    def __setstate__(self, d):
        """Add the creation field if missing"""
        return {
            'add': {'_edition_window': None,
                    '_child_index': -1,
                    '_subitem_adaptor': None,
                    '_implementation': None
                    },
        }

    def __getstate__(self):
        # The edition windows can't be pickled
        state = copy.copy(self.__dict__)
        if '_edition_window' in state:
            del state['_edition_window']
        return state

    @property
    def document(self):
        """Return the instance of Design to which this element belong"""
        return self.parent and self.parent.document

    @property
    def view(self):
        document = self.document
        return document and document.view

    @property
    def edition_window(self):
        return self._edition_window

    @property
    def edition_window_client(self):
        """Edition window client is overriden for mockup implementations"""
        return self._edition_window

    @property
    def local_kwargs(self):
        return {'name': self._name}

    @local_kwargs.setter
    def local_kwargs(self, kwargs):
        self._name = kwargs.get('name', self._name)

    @property
    def kwargs(self):
        value = self.local_kwargs
        if self._subitem_adaptor is not None:
            value.update(self._subitem_adaptor.kwargs)
        return value

    def set_kwargs(self, kwargs):
        self.local_kwargs = kwargs
        if self._subitem_adaptor is not None:
            self._subitem_adaptor.kwargs = kwargs


    @property
    def is_top_window(self):
        return False

    @property
    def is_sizer_root(self):
        """Return information about if a window is a sizer grand parent. By default a top window is a sizer root,
        but not always. For example, wizard dialogs."""
        return self.is_top_window

    @property
    def sizer_root(self):
        return self.inner_true(lambda x: x.is_sizer_root)

    @property
    def top_window(self):
        return self.inner_true(lambda x: x.is_top_window)

    def create_edition_window(self, parent):
        pass

    def destroy_edition_window(self):
        pass

    @property
    def model_attributes(self):
        property_list = []
        event_list = []
        if self._subitem_adaptor is not None:
            property_list, event_list = self._subitem_adaptor.model_attributes
        return property_list, event_list

    @DelayedMethod()
    def export_code_files(self, force=True, logger=None):
        """Do code generation"""
        pass

    @property
    def inner_class(self):
        """Get the innermost class container"""
        return None

    def on_undo_redo_removing(self):
        """This method handles the removing operation."""
        super(wxUICommon, self).on_undo_redo_removing()
        if self._edition_window:
            self.destroy_edition_window()
        from ._Design import Design
        pane = getattr(self.inner(Design), '_pane', None)
        if pane is not None:
            pane.tree_remove(self)
        if not self.in_undo_redo():
            self.save_state()
            if getattr(self, '_implementation', None):
                self._implementation.delete()
            if getattr(self, '_subitem_adaptor', None):
                self._subitem_adaptor.delete()

    def create_sizer_adaptor(self):
        from .controls.layout import GridBagSizerControl
        from .controls.containers import NotebookControl, AuiNotebookControl, ListbookControl, \
            ChoicebookControl, SimplebookControl
        from .controls import _tools as subitem
        self._subitem_adaptor = None
        if self._parent is not None:
            if getattr(self._parent, 'is_sizer', False):
                if type(self._parent) is GridBagSizerControl:
                    self._subitem_adaptor = subitem.BagSizerItemAdaptor(control=self)
                else:
                    self._subitem_adaptor = subitem.SizerItemAdaptor(control=self)
            elif type(self._parent) is NotebookControl:
                self._subitem_adaptor = subitem.NotebookContainerAdaptor(control=self)
            elif type(self._parent) is ListbookControl:
                self._subitem_adaptor = subitem.ListbookContainerAdaptor(control=self)
            elif type(self._parent) is AuiNotebookControl:
                self._subitem_adaptor = subitem.AuiNotebookContainerAdaptor(control=self)
            elif type(self._parent) is ChoicebookControl:
                self._subitem_adaptor = subitem.ChoicebookContainerAdaptor(control=self)
            elif type(self._parent) is SimplebookControl:
                self._subitem_adaptor = subitem.SimplebookContainerAdaptor(control=self)

    def on_undo_redo_add(self):
        from ._Design import Design
        pane = getattr(self.inner(Design), '_pane', None)
        if pane is not None:
            pane.tree_add(self)
        # test now
        if not self.in_undo_redo():
            self.save_state()
            self.create_sizer_adaptor()
            self.implement()

        super(wxUICommon, self).on_undo_redo_add()

    # def set_kwargs(self, **kwargs):
    #     """This class method is the bottom end of nested set_kwargs.
    #     It acts populating some common base attributes."""
    #     self._name = kwargs.get('name', self._name)

    def on_undo_redo_changed(self):
        """Update from app"""
        from ._Design import Design
        ui_design = self.inner(Design)
        if ui_design is not None:
            ui_design.update_tree_item(self)
            if TransactionStack.get_key() != pg.EVT_PG_CHANGED:
                ui_design.update_grid_item(self)
        super(wxUICommon, self).on_undo_redo_changed()

    @property
    def events(self):
        return {}

    @property
    def py_owner_text(self):
        """Return suitable test for using as owner of controls"""
        if self.is_top_window:
            return 'self'
        else:
            return 'self.{name}'.format(name=self._name)

