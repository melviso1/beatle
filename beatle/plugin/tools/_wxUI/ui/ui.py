# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jul 11 2021)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
from beatle.lib import wxx
import wx.xrc
import wx.richtext
import wx.propgrid as pg
import wx.aui

# special import for beatle development
from beatle.lib.handlers import identifier
###########################################################################
## Class DialogBase
###########################################################################

class DialogBase ( wx.Dialog ):
    
    def __init__( self, parent ):
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"new wxUI", pos = wx.DefaultPosition, size = wx.Size( 706,453 ), style = wx.DEFAULT_DIALOG_STYLE )
        
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle( self.GetExtraStyle() | wx.WS_EX_BLOCK_EVENTS|wx.WS_EX_PROCESS_IDLE|wx.WS_EX_PROCESS_UI_UPDATES|wx.WS_EX_TRANSIENT|wx.WS_EX_VALIDATE_RECURSIVELY )
        
        fgSizer1 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer1.AddGrowableCol( 0 )
        fgSizer1.AddGrowableRow( 0 )
        fgSizer1.SetFlexibleDirection( wx.BOTH )
        fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        fgSizer2 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer2.AddGrowableCol( 0 )
        fgSizer2.AddGrowableRow( 1 )
        fgSizer2.SetFlexibleDirection( wx.BOTH )
        fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        fgSizer3 = wx.FlexGridSizer( 3, 2, 0, 0 )
        fgSizer3.AddGrowableCol( 1 )
        fgSizer3.SetFlexibleDirection( wx.BOTH )
        fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        fgSizer3.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
        
        self.name = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer3.Add( self.name, 0, wx.ALL|wx.EXPAND, 5 )
        
        self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"first identifier", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        self.m_staticText3.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW ) )
        
        fgSizer3.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
        
        self.m_identifier = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer3.Add( self.m_identifier, 0, wx.ALL, 5 )
        
        
        fgSizer3.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        self.m_use_beatle = wx.CheckBox( self, wx.ID_ANY, u"Use &beatle identifiers", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer3.Add( self.m_use_beatle, 0, wx.ALL, 5 )
        
        
        fgSizer2.Add( fgSizer3, 1, wx.EXPAND, 5 )
        
        sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Note" ), wx.VERTICAL )
        
        self.m_comments = wx.richtext.RichTextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
        sbSizer1.Add( self.m_comments, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        fgSizer2.Add( sbSizer1, 1, wx.EXPAND|wx.ALL, 5 )
        
        
        fgSizer1.Add( fgSizer2, 1, wx.EXPAND|wx.ALL, 5 )
        
        m_sdbSizer1 = wx.StdDialogButtonSizer()
        self.m_sdbSizer1OK = wx.Button( self, wx.ID_OK )
        m_sdbSizer1.AddButton( self.m_sdbSizer1OK )
        self.m_sdbSizer1Cancel = wx.Button( self, wx.ID_CANCEL )
        m_sdbSizer1.AddButton( self.m_sdbSizer1Cancel )
        m_sdbSizer1.Realize();
        
        fgSizer1.Add( m_sdbSizer1, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( fgSizer1 )
        self.Layout()
        
        self.Centre( wx.BOTH )
        
        # Connect Events
        self.m_use_beatle.Bind( wx.EVT_CHECKBOX, self.on_use_beatle_identifiers )
        self.m_sdbSizer1OK.Bind( wx.EVT_BUTTON, self.on_ok )
    
    def __del__( self ):
        pass
    
    
    # Virtual event handlers, overide them in your derived class
    def on_use_beatle_identifiers( self, event ):
        event.Skip()
    
    def on_ok( self, event ):
        event.Skip()
    

###########################################################################
## Class DesignBaseStd
###########################################################################

class DesignBaseStd ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 1106,695 ), style = wx.WANTS_CHARS )
        
        self.SetExtraStyle( wx.WS_EX_PROCESS_UI_UPDATES )
        
        fgSizer4 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer4.AddGrowableCol( 0 )
        fgSizer4.AddGrowableRow( 0 )
        fgSizer4.SetFlexibleDirection( wx.BOTH )
        fgSizer4.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_splitter2 = wx.SplitterWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D|wx.SP_3DBORDER )
        self.m_splitter2.Bind( wx.EVT_IDLE, self.m_splitter2OnIdle )
        self.m_splitter2.SetMinimumPaneSize( 200 )
        
        self.m_panel11 = wx.Panel( self.m_splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        gSizer1 = wx.GridSizer( 1, 1, 0, 0 )
        
        gSizer1.SetMinSize( wx.Size( 50,-1 ) ) 
        self.m_treeCtrl = wx.TreeCtrl( self.m_panel11, wx.ID_ANY, wx.DefaultPosition, wx.Size( 200,-1 ), wx.TR_HAS_BUTTONS|wx.TR_LINES_AT_ROOT|wx.TR_SINGLE|wx.WANTS_CHARS )
        self.m_treeCtrl.SetMinSize( wx.Size( 200,-1 ) )
        
        gSizer1.Add( self.m_treeCtrl, 0, wx.ALL|wx.EXPAND, 5 )
        
        
        self.m_panel11.SetSizer( gSizer1 )
        self.m_panel11.Layout()
        gSizer1.Fit( self.m_panel11 )
        self.m_panel26 = wx.Panel( self.m_splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        fgSizer25 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer25.AddGrowableCol( 0 )
        fgSizer25.AddGrowableRow( 0 )
        fgSizer25.SetFlexibleDirection( wx.BOTH )
        fgSizer25.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_splitter1 = wx.SplitterWindow( self.m_panel26, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
        self.m_splitter1.SetSashGravity( 1 )
        self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )
        self.m_splitter1.SetMinimumPaneSize( 100 )
        
        self.m_panel24 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        fgSizer6 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer6.AddGrowableCol( 0 )
        fgSizer6.AddGrowableRow( 1 )
        fgSizer6.SetFlexibleDirection( wx.BOTH )
        fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_toolBar9 = wx.ToolBar( self.m_panel24, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_align_left = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_left.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align left", wx.EmptyString, None ) 
        
        self.m_align_center = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_center.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align center", wx.EmptyString, None ) 
        
        self.m_align_right = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_right.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align right", wx.EmptyString, None ) 
        
        self.m_toolBar9.AddSeparator()
        
        self.m_align_top = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_top.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align top", wx.EmptyString, None ) 
        
        self.m_align_middle = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_middle.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align middle", wx.EmptyString, None ) 
        
        self.m_align_bottom = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_bottom.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align bottom", wx.EmptyString, None ) 
        
        self.m_toolBar9.AddSeparator()
        
        self.m_expand = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/expand.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Expand", wx.EmptyString, None ) 
        
        self.m_stretch = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/stretch.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Stretch", wx.EmptyString, None ) 
        
        self.m_toolBar9.AddSeparator()
        
        self.m_left_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/left_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Left border", wx.EmptyString, None ) 
        
        self.m_right_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/right_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Right border", wx.EmptyString, None ) 
        
        self.m_top_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/top_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Top border", wx.EmptyString, None ) 
        
        self.m_bottom_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/bottom_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Bottom border", wx.EmptyString, None ) 
        
        self.m_toolBar9.Realize() 
        
        fgSizer6.Add( self.m_toolBar9, 0, wx.EXPAND, 5 )
        
        fgSizer22 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer22.AddGrowableCol( 0 )
        fgSizer22.AddGrowableRow( 0 )
        fgSizer22.SetFlexibleDirection( wx.BOTH )
        fgSizer22.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_panel22 = wx.Panel( self.m_panel24, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_panel22.SetBackgroundColour( wx.Colour(60, 100, 127) )
        
        fgSizer20 = wx.FlexGridSizer( 3, 3, 0, 0 )
        fgSizer20.AddGrowableCol( 1 )
        fgSizer20.AddGrowableRow( 1 )
        fgSizer20.SetFlexibleDirection( wx.BOTH )
        fgSizer20.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        fgSizer20.Add( ( 12, 12), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 12, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 12, 0), 1, wx.EXPAND, 5 )
        
        self.m_container = wx.ScrolledWindow( self.m_panel22, wx.ID_ANY, wx.Point( -1,-1 ), wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
        self.m_container.SetScrollRate( 5, 5 )
        self.m_container.SetBackgroundColour( wx.Colour(60, 100, 127) )
        
        fgSizer20.Add( self.m_container, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 12), 1, wx.EXPAND, 5 )
        
        
        self.m_panel22.SetSizer( fgSizer20 )
        self.m_panel22.Layout()
        fgSizer20.Fit( self.m_panel22 )
        fgSizer22.Add( self.m_panel22, 1, wx.EXPAND |wx.ALL, 5 )
        
        self.m_notebook1 = wx.Notebook( self.m_panel24, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NB_BOTTOM )
        self.m_notebook1.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT ) )
        self.m_notebook1.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU ) )
        
        self.m_panel_forms = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer3 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_toolBar4 = wx.ToolBar( self.m_panel_forms, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_tool26 = self.m_toolBar4.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/frame.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxFrame", wx.EmptyString, None ) 
        
        self.m_tool27 = self.m_toolBar4.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/panel.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxPanel", wx.EmptyString, None ) 
        
        self.m_tool28 = self.m_toolBar4.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/dialog.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxDialog", wx.EmptyString, None ) 
        
        self.m_tool29 = self.m_toolBar4.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/wizard.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxWizard", wx.EmptyString, None ) 
        
        self.m_tool30 = self.m_toolBar4.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/wizard_page.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxWizardPageSimple", wx.EmptyString, None ) 
        
        self.m_tool31 = self.m_toolBar4.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/menu_bar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxMenuBar", wx.EmptyString, None ) 
        
        self.m_tool32 = self.m_toolBar4.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/toolbar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxToolBar", wx.EmptyString, None ) 
        
        self.m_toolBar4.Realize() 
        
        bSizer3.Add( self.m_toolBar4, 0, wx.EXPAND, 5 )
        
        
        self.m_panel_forms.SetSizer( bSizer3 )
        self.m_panel_forms.Layout()
        bSizer3.Fit( self.m_panel_forms )
        self.m_notebook1.AddPage( self.m_panel_forms, u"Forms", True )
        self.m_panel_menu_toolbar = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer5 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_toolBar5 = wx.ToolBar( self.m_panel_menu_toolbar, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_tool41 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/status_bar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxStatusBar", wx.EmptyString, None ) 
        
        self.m_tool42 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/menu_bar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxMenuBar", wx.EmptyString, None ) 
        
        self.m_tool43 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/menu.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxMenu", wx.EmptyString, None ) 
        
        self.m_tool44 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/submenu.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"submenu", wx.EmptyString, None ) 
        
        self.m_tool45 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/menu_item.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxMenuItem", wx.EmptyString, None ) 
        
        self.m_tool46 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/menu_separator.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"separator", wx.EmptyString, None ) 
        
        self.m_tool471 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/toolbar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxToolBar", wx.EmptyString, None ) 
        
        self.m_tool48 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/aui_toolbar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxAuiToolBar", wx.EmptyString, None ) 
        
        self.m_tool49 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/tool.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"tool", wx.EmptyString, None ) 
        
        self.m_tool50 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/tool_separator.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"tool separator", wx.EmptyString, None ) 
        
        self.m_tool51 = self.m_toolBar5.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/info_bar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxInfoBar", wx.EmptyString, None ) 
        
        self.m_toolBar5.Realize() 
        
        bSizer5.Add( self.m_toolBar5, 0, wx.EXPAND, 5 )
        
        
        self.m_panel_menu_toolbar.SetSizer( bSizer5 )
        self.m_panel_menu_toolbar.Layout()
        bSizer5.Fit( self.m_panel_menu_toolbar )
        self.m_notebook1.AddPage( self.m_panel_menu_toolbar, u"Menu/Toolbar", False )
        self.m_panel_layout = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer31 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_toolBar41 = wx.ToolBar( self.m_panel_layout, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_tool261 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/sizer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxBoxSizer", wx.EmptyString, None ) 
        
        self.m_tool271 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/sizer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxWrapSizer", wx.EmptyString, None ) 
        
        self.m_tool281 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/static_box_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxStaticBoxSizer", wx.EmptyString, None ) 
        
        self.m_tool291 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/grid_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxGridSizer", wx.EmptyString, None ) 
        
        self.m_tool301 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/flexible_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxFlexGridSizer", wx.EmptyString, None ) 
        
        self.m_tool311 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/grid_bag_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxGridBagSizer", wx.EmptyString, None ) 
        
        self.m_tool321 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/dialog_buttons_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxStdDialogButtonSizer", wx.EmptyString, None ) 
        
        self.m_tool47 = self.m_toolBar41.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/separator.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"spacer", wx.EmptyString, None ) 
        
        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button( self.m_toolBar41, wx.ID_OK )
        m_sdbSizer2.AddButton( self.m_sdbSizer2OK )
        self.m_sdbSizer2Cancel = wx.Button( self.m_toolBar41, wx.ID_CANCEL )
        m_sdbSizer2.AddButton( self.m_sdbSizer2Cancel )
        m_sdbSizer2.Realize();
        
        self.m_toolBar41.SetSizer( m_sdbSizer2 )
        self.m_toolBar41.Layout()
        m_sdbSizer2.Fit( self.m_toolBar41 )
        
        self.m_toolBar41.Realize() 
        
        bSizer31.Add( self.m_toolBar41, 0, wx.EXPAND, 5 )
        
        
        self.m_panel_layout.SetSizer( bSizer31 )
        self.m_panel_layout.Layout()
        bSizer31.Fit( self.m_panel_layout )
        self.m_notebook1.AddPage( self.m_panel_layout, u"Layout", False )
        self.m_panel_containers = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer2 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_toolBar3 = wx.ToolBar( self.m_panel_containers, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_tool18 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/panel.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxPanel", wx.EmptyString, None ) 
        
        self.m_tool19 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/splitter_window.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxSplitterWindow", wx.EmptyString, None ) 
        
        self.m_tool20 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/scrolled_window.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxScrolledWindow", wx.EmptyString, None ) 
        
        self.m_tool21 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/notebook.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxNotebook", wx.EmptyString, None ) 
        
        self.m_tool22 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/aui_notebook.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxAuiNotebook", wx.EmptyString, None ) 
        
        self.m_tool23 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/list_book.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxListbook", wx.EmptyString, None ) 
        
        self.m_tool24 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/choice_book.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxChoicebook", wx.EmptyString, None ) 
        
        self.m_tool25 = self.m_toolBar3.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/panel.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxSimpleBook", wx.EmptyString, None ) 
        
        self.m_toolBar3.Realize() 
        
        bSizer2.Add( self.m_toolBar3, 0, wx.EXPAND, 5 )
        
        
        self.m_panel_containers.SetSizer( bSizer2 )
        self.m_panel_containers.Layout()
        bSizer2.Fit( self.m_panel_containers )
        self.m_notebook1.AddPage( self.m_panel_containers, u"Containers", False )
        self.m_panel_common = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_panel_common.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU ) )
        
        bSizer1 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_toolBar2 = wx.ToolBar( self.m_panel_common, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_tool1 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/button.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxButton", wx.EmptyString, None ) 
        
        self.m_tool2 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/bitmap_button.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxBitmapButton", wx.EmptyString, None ) 
        
        self.m_tool3 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/static_text.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxStaticText", wx.EmptyString, None ) 
        
        self.m_tool4 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/text_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxTextCtrl", wx.EmptyString, None ) 
        
        self.m_tool5 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/static_bitmap.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxStaticBitmap", wx.EmptyString, None ) 
        
        self.m_tool6 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/animation_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxAnimationCtrl", wx.EmptyString, None ) 
        
        self.m_tool7 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/combo_box.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxComboBox", wx.EmptyString, None ) 
        
        self.m_tool8 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/combo_box.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxBitmapComboBox", wx.EmptyString, None ) 
        
        self.m_tool9 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/choice_box.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxChoice", wx.EmptyString, None ) 
        
        self.m_tool10 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/list_box.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxListBox", wx.EmptyString, None ) 
        
        self.m_tool11 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/list_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxListCtrl", wx.EmptyString, None ) 
        
        self.m_tool12 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/check_box.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxCheckBox", wx.EmptyString, None ) 
        
        self.m_tool13 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/radio_box.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxRadioBox", wx.EmptyString, None ) 
        
        self.m_tool14 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/radio_button.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxRadioButton", wx.EmptyString, None ) 
        
        self.m_tool15 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/static_line.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxStaticLine", wx.EmptyString, None ) 
        
        self.m_tool16 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/slider.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxSlider", wx.EmptyString, None ) 
        
        self.m_tool17 = self.m_toolBar2.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/gauge.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxGauge", wx.EmptyString, None ) 
        
        self.m_toolBar2.Realize() 
        
        bSizer1.Add( self.m_toolBar2, 0, wx.EXPAND, 5 )
        
        
        self.m_panel_common.SetSizer( bSizer1 )
        self.m_panel_common.Layout()
        bSizer1.Fit( self.m_panel_common )
        self.m_notebook1.AddPage( self.m_panel_common, u"Common", False )
        self.m_panel_data = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer6 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_toolBar6 = wx.ToolBar( self.m_panel_data, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_tool52 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/tree_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxTreeCtrl", wx.EmptyString, None ) 
        
        self.m_tool53 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/grid_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxGrid", wx.EmptyString, None ) 
        
        self.m_tool54 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/check_listbox.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxCheckListBox", wx.EmptyString, None ) 
        
        self.m_tool55 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/tree_list_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxTreeListCtrl", wx.EmptyString, None ) 
        
        self.m_tool56 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/tree_list_control_column.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxTreeListCtrlColumn", wx.EmptyString, None ) 
        
        self.m_tool57 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/data_view_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxDataViewCtrl", wx.EmptyString, None ) 
        
        self.m_tool58 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/data_view_tree_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxDataViewTreeCtrl", wx.EmptyString, None ) 
        
        self.m_tool59 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/data_view_list_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxDataViewListCtrl", wx.EmptyString, None ) 
        
        self.m_tool60 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/data_view_column.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"dataViewColumn", wx.EmptyString, None ) 
        
        self.m_tool61 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/data_view_column.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxDataViewListColumn", wx.EmptyString, None ) 
        
        self.m_tool62 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/property_grid.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxPropertyGrid", wx.EmptyString, None ) 
        
        self.m_tool63 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/property_grid_manager.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxPropertyGridManager", wx.EmptyString, None ) 
        
        self.m_tool64 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/property_grid_page.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"propGridPage", wx.EmptyString, None ) 
        
        self.m_tool65 = self.m_toolBar6.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/property_grid_item.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"propGridItem", wx.EmptyString, None ) 
        
        self.m_toolBar6.Realize() 
        
        bSizer6.Add( self.m_toolBar6, 0, wx.EXPAND, 5 )
        
        
        self.m_panel_data.SetSizer( bSizer6 )
        self.m_panel_data.Layout()
        bSizer6.Fit( self.m_panel_data )
        self.m_notebook1.AddPage( self.m_panel_data, u"Data", False )
        self.m_panel_additional = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer7 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_toolBar7 = wx.ToolBar( self.m_panel_additional, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_tool66 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/html_window.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxHtmlWindow", wx.EmptyString, None ) 
        
        self.m_tool67 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/rich_text.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxRichTextCtrl", wx.EmptyString, None ) 
        
        self.m_tool68 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/styled_text.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxStyledTextCtrl", wx.EmptyString, None ) 
        
        self.m_tool69 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/toggle_button.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxToggleButton", wx.EmptyString, None ) 
        
        self.m_tool70 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/search_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxSearchCtrl", wx.EmptyString, None ) 
        
        self.m_tool71 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/colour_picker.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxColourPickerCtrl", wx.EmptyString, None ) 
        
        self.m_tool72 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/font_picker.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxFontPickerCtrl", wx.EmptyString, None ) 
        
        self.m_tool73 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/file_picker.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxFilePickerCtrl", wx.EmptyString, None ) 
        
        self.m_tool74 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/dir_picker.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxDirPickerCtrl", wx.EmptyString, None ) 
        
        self.m_tool75 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/date_picker.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxDatePickerCtrl", wx.EmptyString, None ) 
        
        self.m_tool76 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/calendar_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxCalendarCtrl", wx.EmptyString, None ) 
        
        self.m_tool77 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/scroll_bar.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxScrollBar", wx.EmptyString, None ) 
        
        self.m_tool78 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/spin_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxSpinCtrl", wx.EmptyString, None ) 
        
        self.m_tool79 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/spin_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxSpinCtrlDouble", wx.EmptyString, None ) 
        
        self.m_tool80 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/spin_button.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxSpinButton", wx.EmptyString, None ) 
        
        self.m_tool81 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/hyperlink_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxHyperlinkCtrl", wx.EmptyString, None ) 
        
        self.m_tool82 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/generic_dir_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxGenericDirCtrl", wx.EmptyString, None ) 
        
        self.m_tool83 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/custom_control.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"CustomControl", wx.EmptyString, None ) 
        
        self.m_tool84 = self.m_toolBar7.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/timer.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"wxTimer", wx.EmptyString, None ) 
        
        self.m_toolBar7.Realize() 
        
        bSizer7.Add( self.m_toolBar7, 0, wx.EXPAND, 5 )
        
        
        self.m_panel_additional.SetSizer( bSizer7 )
        self.m_panel_additional.Layout()
        bSizer7.Fit( self.m_panel_additional )
        self.m_notebook1.AddPage( self.m_panel_additional, u"Additional", False )
        
        fgSizer22.Add( self.m_notebook1, 1, wx.ALL|wx.EXPAND, 5 )
        
        
        fgSizer6.Add( fgSizer22, 1, wx.EXPAND, 5 )
        
        
        self.m_panel24.SetSizer( fgSizer6 )
        self.m_panel24.Layout()
        fgSizer6.Fit( self.m_panel24 )
        self.m_panel8 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.WANTS_CHARS )
        self.m_panel8.SetMinSize( wx.Size( 300,-1 ) )
        
        fgSizer5 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer5.AddGrowableCol( 0 )
        fgSizer5.AddGrowableRow( 0 )
        fgSizer5.SetFlexibleDirection( wx.BOTH )
        fgSizer5.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_propertyGridManager = pg.PropertyGridManager(self.m_panel8, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.propgrid.PGMAN_DEFAULT_STYLE|wx.propgrid.PG_BOLD_MODIFIED|wx.propgrid.PG_DESCRIPTION|wx.propgrid.PG_TOOLBAR|wx.WANTS_CHARS)
        self.m_propertyGridManager.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Liberation Sans" ) )
        self.m_propertyGridManager.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT ) )
        self.m_propertyGridManager.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW ) )
        self.m_propertyGridManager.SetMinSize( wx.Size( 300,-1 ) )
        
        
        self.m_properties = self.m_propertyGridManager.AddPage( u"Properties", wx.Bitmap(u"../res/properties_text.xpm", wx.BITMAP_TYPE_ANY) );
        
        self.m_events = self.m_propertyGridManager.AddPage( u"Events", wx.Bitmap(u"../res/events_text.xpm", wx.BITMAP_TYPE_ANY) );
        fgSizer5.Add( self.m_propertyGridManager, 0, wx.ALL|wx.EXPAND, 5 )
        
        
        self.m_panel8.SetSizer( fgSizer5 )
        self.m_panel8.Layout()
        fgSizer5.Fit( self.m_panel8 )
        self.m_splitter1.SplitVertically( self.m_panel24, self.m_panel8, 0 )
        fgSizer25.Add( self.m_splitter1, 1, wx.EXPAND, 5 )
        
        
        self.m_panel26.SetSizer( fgSizer25 )
        self.m_panel26.Layout()
        fgSizer25.Fit( self.m_panel26 )
        self.m_splitter2.SplitVertically( self.m_panel11, self.m_panel26, 255 )
        fgSizer4.Add( self.m_splitter2, 0, wx.EXPAND, 5 )
        
        
        self.SetSizer( fgSizer4 )
        self.Layout()
        
        # Connect Events
        self.Bind( wx.EVT_KILL_FOCUS, self.OnKillFocus )
        self.Bind( wx.EVT_SET_FOCUS, self.OnGetFocus )
        self.m_treeCtrl.Bind( wx.EVT_KEY_DOWN, self.on_tree_key_down )
        self.m_treeCtrl.Bind( wx.EVT_TREE_SEL_CHANGED, self.on_model_item_selected )
        self.Bind( wx.EVT_TOOL, self.on_align_left, id = self.m_align_left.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_left, id = self.m_align_left.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_center, id = self.m_align_center.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_center, id = self.m_align_center.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_right, id = self.m_align_right.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_right, id = self.m_align_right.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_top, id = self.m_align_top.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_top, id = self.m_align_top.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_middle, id = self.m_align_middle.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_middle, id = self.m_align_middle.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_bottom, id = self.m_align_bottom.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_bottom, id = self.m_align_bottom.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_expand, id = self.m_expand.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_expand, id = self.m_expand.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_stretch, id = self.m_stretch.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_stretch, id = self.m_stretch.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_left_border, id = self.m_left_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_left_border, id = self.m_left_border.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_right_border, id = self.m_right_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_right_border, id = self.m_right_border.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_top_border, id = self.m_top_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_top_border, id = self.m_top_border.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_bottom_border, id = self.m_bottom_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_bottom_border, id = self.m_bottom_border.GetId() )
        self.m_container.Bind( wx.EVT_LEFT_DOWN, self.on_design_mouse_left_click )
        self.m_container.Bind( wx.EVT_MOTION, self.on_design_mouse_move )
        self.m_container.Bind( wx.EVT_PAINT, self.on_design_paint )
        self.Bind( wx.EVT_TOOL, self.on_add_frame_window, id = self.m_tool26.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_pane_window, id = self.m_tool27.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dialog, id = self.m_tool28.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_wizard, id = self.m_tool29.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_wizard_page, id = self.m_tool30.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_wizard_page, id = self.m_tool30.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_menu_bar_window, id = self.m_tool31.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_toolbar_window, id = self.m_tool32.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_status_bar_control, id = self.m_tool41.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_status_bar_control, id = self.m_tool41.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_menu_bar_control, id = self.m_tool42.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menu_bar_control, id = self.m_tool42.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_menu_control, id = self.m_tool43.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menu_control, id = self.m_tool43.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_submenu_control, id = self.m_tool44.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_submenu_control, id = self.m_tool44.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_menu_item_control, id = self.m_tool45.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menu_item_control, id = self.m_tool45.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_menuitem_separator_control, id = self.m_tool46.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menuitem_separator_control, id = self.m_tool46.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_tool_bar_control, id = self.m_tool471.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tool_bar_control, id = self.m_tool471.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_auitoolbar, id = self.m_tool48.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_auitoolbar, id = self.m_tool48.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_tool, id = self.m_tool49.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tool, id = self.m_tool49.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_tool_separator, id = self.m_tool50.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tool_separator, id = self.m_tool50.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_infobar, id = self.m_tool51.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_infobar, id = self.m_tool51.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_box_sizer, id = self.m_tool261.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_box_sizer, id = self.m_tool261.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_wrap_sizer, id = self.m_tool271.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_wrap_sizer, id = self.m_tool271.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_staticbox_sizer, id = self.m_tool281.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_staticbox_sizer, id = self.m_tool281.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_grid_sizer, id = self.m_tool291.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_grid_sizer, id = self.m_tool291.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_flexgrid_sizer, id = self.m_tool301.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_flexgrid_sizer, id = self.m_tool301.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_gridbag_sizer, id = self.m_tool311.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_gridbag_sizer, id = self.m_tool311.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dialogbuttons_sizer, id = self.m_tool321.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dialogbuttons_sizer, id = self.m_tool321.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_spacer, id = self.m_tool47.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spacer, id = self.m_tool47.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_panel, id = self.m_tool18.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_panel, id = self.m_tool18.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_splitter_window, id = self.m_tool19.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_splitter_window, id = self.m_tool19.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_scrolled_window, id = self.m_tool20.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_scrolled_window, id = self.m_tool20.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_notebook, id = self.m_tool21.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_notebook, id = self.m_tool21.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_aui_notebook, id = self.m_tool22.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_aui_notebook, id = self.m_tool22.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_listbook, id = self.m_tool23.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_listbook, id = self.m_tool23.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_choicebook, id = self.m_tool24.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_choicebook, id = self.m_tool24.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_simplebook, id = self.m_tool25.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_simplebook, id = self.m_tool25.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_button, id = self.m_tool1.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_button, id = self.m_tool1.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_bitmap_button, id = self.m_tool2.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_bitmap_button, id = self.m_tool2.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_static_text, id = self.m_tool3.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_static_text, id = self.m_tool3.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_edit_text, id = self.m_tool4.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_edit_text, id = self.m_tool4.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_static_bitmap, id = self.m_tool5.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_static_bitmap, id = self.m_tool5.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_animation_control, id = self.m_tool6.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_animation_control, id = self.m_tool6.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_combo_box, id = self.m_tool7.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_combo_box, id = self.m_tool7.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_bitmap_combo_box, id = self.m_tool8.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_bitmap_combo_box, id = self.m_tool8.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_choice_box, id = self.m_tool9.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_choice_box, id = self.m_tool9.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_list_box, id = self.m_tool10.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_list_box, id = self.m_tool10.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_list_control, id = self.m_tool11.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_list_control, id = self.m_tool11.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_check_box, id = self.m_tool12.GetId() )
        self.Bind( wx.EVT_TOOL_ENTER, self.a, id = self.m_tool12.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_check_box, id = self.m_tool12.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_radio_box, id = self.m_tool13.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_radio_box, id = self.m_tool13.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_radio_button, id = self.m_tool14.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_radio_button, id = self.m_tool14.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_static_line, id = self.m_tool15.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_static_line, id = self.m_tool15.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_slider, id = self.m_tool16.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_slider, id = self.m_tool16.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_gauge, id = self.m_tool17.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_gauge, id = self.m_tool17.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_tree_control, id = self.m_tool52.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tree_control, id = self.m_tool52.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_grid, id = self.m_tool53.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_grid, id = self.m_tool53.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_check_listbox, id = self.m_tool54.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_check_listbox, id = self.m_tool54.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_tree_listcontrol, id = self.m_tool55.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tree_listcontrol, id = self.m_tool55.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_tree_listcontrol_column, id = self.m_tool56.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tree_listcontrol_column, id = self.m_tool56.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dataview_control, id = self.m_tool57.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_control, id = self.m_tool57.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dataview_tree_control, id = self.m_tool58.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_tree_control, id = self.m_tool58.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dataview_listcontrol, id = self.m_tool59.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_listcontrol, id = self.m_tool59.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dataview_column, id = self.m_tool60.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_column, id = self.m_tool60.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dataview_listcolumn, id = self.m_tool61.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_listcolumn, id = self.m_tool61.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_property_grid, id = self.m_tool62.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_grid, id = self.m_tool62.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_property_grid_manager, id = self.m_tool63.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_grid_manager, id = self.m_tool63.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_property_page, id = self.m_tool64.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_page, id = self.m_tool64.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_property_grid_item, id = self.m_tool65.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_grid_item, id = self.m_tool65.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_html_window, id = self.m_tool66.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_html_window, id = self.m_tool66.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_rich_text_control, id = self.m_tool67.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_rich_text_control, id = self.m_tool67.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_styledtext_control, id = self.m_tool68.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_styledtext_control, id = self.m_tool68.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_toggle_button, id = self.m_tool69.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_toggle_button, id = self.m_tool69.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_search_control, id = self.m_tool70.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_search_control, id = self.m_tool70.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_color_picker_control, id = self.m_tool71.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_color_picker_control, id = self.m_tool71.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_font_picker_control, id = self.m_tool72.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_font_picker_control, id = self.m_tool72.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_file_picker_control, id = self.m_tool73.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_file_picker_control, id = self.m_tool73.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_dir_picker_control, id = self.m_tool74.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dir_picker_control, id = self.m_tool74.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_date_picker_control, id = self.m_tool75.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_date_picker_control, id = self.m_tool75.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_calendar_control, id = self.m_tool76.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_calendar_control, id = self.m_tool76.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_scrollbar_control, id = self.m_tool77.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_scrollbar_control, id = self.m_tool77.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_spin_control, id = self.m_tool78.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spin_control, id = self.m_tool78.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_spin_double_control, id = self.m_tool79.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spin_double_control, id = self.m_tool79.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_spin_button, id = self.m_tool80.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spin_button, id = self.m_tool80.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_hyperlink_control, id = self.m_tool81.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_hyperlink_control, id = self.m_tool81.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_generic_dir_control, id = self.m_tool82.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_generic_dir_control, id = self.m_tool82.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_custom_control, id = self.m_tool83.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_custom_control, id = self.m_tool83.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_add_timer_control, id = self.m_tool84.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_add_timer_control, id = self.m_tool84.GetId() )
        self.m_propertyGridManager.Bind( pg.EVT_PG_CHANGED, self.on_property_changed )
        self.m_propertyGridManager.Bind( pg.EVT_PG_CHANGING, self.on_property_changing )
    
    def __del__( self ):
        pass
    
    
    # Virtual event handlers, overide them in your derived class
    def OnKillFocus( self, event ):
        event.Skip()
    
    def OnGetFocus( self, event ):
        event.Skip()
    
    def on_tree_key_down( self, event ):
        event.Skip()
    
    def on_model_item_selected( self, event ):
        event.Skip()
    
    def on_align_left( self, event ):
        event.Skip()
    
    def on_update_align_left( self, event ):
        event.Skip()
    
    def on_align_center( self, event ):
        event.Skip()
    
    def on_update_align_center( self, event ):
        event.Skip()
    
    def on_align_right( self, event ):
        event.Skip()
    
    def on_update_align_right( self, event ):
        event.Skip()
    
    def on_align_top( self, event ):
        event.Skip()
    
    def on_update_align_top( self, event ):
        event.Skip()
    
    def on_align_middle( self, event ):
        event.Skip()
    
    def on_update_align_middle( self, event ):
        event.Skip()
    
    def on_align_bottom( self, event ):
        event.Skip()
    
    def on_update_align_bottom( self, event ):
        event.Skip()
    
    def on_expand( self, event ):
        event.Skip()
    
    def on_update_expand( self, event ):
        event.Skip()
    
    def on_stretch( self, event ):
        event.Skip()
    
    def on_update_stretch( self, event ):
        event.Skip()
    
    def on_left_border( self, event ):
        event.Skip()
    
    def on_update_left_border( self, event ):
        event.Skip()
    
    def on_right_border( self, event ):
        event.Skip()
    
    def on_update_right_border( self, event ):
        event.Skip()
    
    def on_top_border( self, event ):
        event.Skip()
    
    def on_update_top_border( self, event ):
        event.Skip()
    
    def on_bottom_border( self, event ):
        event.Skip()
    
    def on_update_bottom_border( self, event ):
        event.Skip()
    
    def on_design_mouse_left_click( self, event ):
        event.Skip()
    
    def on_design_mouse_move( self, event ):
        event.Skip()
    
    def on_design_paint( self, event ):
        event.Skip()
    
    def on_add_frame_window( self, event ):
        event.Skip()
    
    def on_add_pane_window( self, event ):
        event.Skip()
    
    def on_add_dialog( self, event ):
        event.Skip()
    
    def on_add_wizard( self, event ):
        event.Skip()
    
    def on_add_wizard_page( self, event ):
        event.Skip()
    
    def on_update_add_wizard_page( self, event ):
        event.Skip()
    
    def on_add_menu_bar_window( self, event ):
        event.Skip()
    
    def on_add_toolbar_window( self, event ):
        event.Skip()
    
    def on_add_status_bar_control( self, event ):
        event.Skip()
    
    def on_update_add_status_bar_control( self, event ):
        event.Skip()
    
    def on_add_menu_bar_control( self, event ):
        event.Skip()
    
    def on_update_add_menu_bar_control( self, event ):
        event.Skip()
    
    def on_add_menu_control( self, event ):
        event.Skip()
    
    def on_update_add_menu_control( self, event ):
        event.Skip()
    
    def on_add_submenu_control( self, event ):
        event.Skip()
    
    def on_update_add_submenu_control( self, event ):
        event.Skip()
    
    def on_add_menu_item_control( self, event ):
        event.Skip()
    
    def on_update_add_menu_item_control( self, event ):
        event.Skip()
    
    def on_add_menuitem_separator_control( self, event ):
        event.Skip()
    
    def on_update_add_menuitem_separator_control( self, event ):
        event.Skip()
    
    def on_add_tool_bar_control( self, event ):
        event.Skip()
    
    def on_update_add_tool_bar_control( self, event ):
        event.Skip()
    
    def on_add_auitoolbar( self, event ):
        event.Skip()
    
    def on_update_add_auitoolbar( self, event ):
        event.Skip()
    
    def on_add_tool( self, event ):
        event.Skip()
    
    def on_update_add_tool( self, event ):
        event.Skip()
    
    def on_add_tool_separator( self, event ):
        event.Skip()
    
    def on_update_add_tool_separator( self, event ):
        event.Skip()
    
    def on_add_infobar( self, event ):
        event.Skip()
    
    def on_update_add_infobar( self, event ):
        event.Skip()
    
    def on_add_box_sizer( self, event ):
        event.Skip()
    
    def on_update_add_box_sizer( self, event ):
        event.Skip()
    
    def on_add_wrap_sizer( self, event ):
        event.Skip()
    
    def on_update_add_wrap_sizer( self, event ):
        event.Skip()
    
    def on_add_staticbox_sizer( self, event ):
        event.Skip()
    
    def on_update_add_staticbox_sizer( self, event ):
        event.Skip()
    
    def on_add_grid_sizer( self, event ):
        event.Skip()
    
    def on_update_add_grid_sizer( self, event ):
        event.Skip()
    
    def on_add_flexgrid_sizer( self, event ):
        event.Skip()
    
    def on_update_add_flexgrid_sizer( self, event ):
        event.Skip()
    
    def on_add_gridbag_sizer( self, event ):
        event.Skip()
    
    def on_update_add_gridbag_sizer( self, event ):
        event.Skip()
    
    def on_add_dialogbuttons_sizer( self, event ):
        event.Skip()
    
    def on_update_add_dialogbuttons_sizer( self, event ):
        event.Skip()
    
    def on_add_spacer( self, event ):
        event.Skip()
    
    def on_update_add_spacer( self, event ):
        event.Skip()
    
    def on_add_panel( self, event ):
        event.Skip()
    
    def on_update_add_panel( self, event ):
        event.Skip()
    
    def on_add_splitter_window( self, event ):
        event.Skip()
    
    def on_update_add_splitter_window( self, event ):
        event.Skip()
    
    def on_add_scrolled_window( self, event ):
        event.Skip()
    
    def on_update_add_scrolled_window( self, event ):
        event.Skip()
    
    def on_add_notebook( self, event ):
        event.Skip()
    
    def on_update_add_notebook( self, event ):
        event.Skip()
    
    def on_add_aui_notebook( self, event ):
        event.Skip()
    
    def on_update_add_aui_notebook( self, event ):
        event.Skip()
    
    def on_add_listbook( self, event ):
        event.Skip()
    
    def on_update_add_listbook( self, event ):
        event.Skip()
    
    def on_add_choicebook( self, event ):
        event.Skip()
    
    def on_update_add_choicebook( self, event ):
        event.Skip()
    
    def on_add_simplebook( self, event ):
        event.Skip()
    
    def on_update_add_simplebook( self, event ):
        event.Skip()
    
    def on_add_button( self, event ):
        event.Skip()
    
    def on_update_add_button( self, event ):
        event.Skip()
    
    def on_add_bitmap_button( self, event ):
        event.Skip()
    
    def on_update_add_bitmap_button( self, event ):
        event.Skip()
    
    def on_add_static_text( self, event ):
        event.Skip()
    
    def on_update_add_static_text( self, event ):
        event.Skip()
    
    def on_add_edit_text( self, event ):
        event.Skip()
    
    def on_update_add_edit_text( self, event ):
        event.Skip()
    
    def on_add_static_bitmap( self, event ):
        event.Skip()
    
    def on_update_add_static_bitmap( self, event ):
        event.Skip()
    
    def on_add_animation_control( self, event ):
        event.Skip()
    
    def on_update_add_animation_control( self, event ):
        event.Skip()
    
    def on_add_combo_box( self, event ):
        event.Skip()
    
    def on_update_add_combo_box( self, event ):
        event.Skip()
    
    def on_add_bitmap_combo_box( self, event ):
        event.Skip()
    
    def on_update_add_bitmap_combo_box( self, event ):
        event.Skip()
    
    def on_add_choice_box( self, event ):
        event.Skip()
    
    def on_update_add_choice_box( self, event ):
        event.Skip()
    
    def on_add_list_box( self, event ):
        event.Skip()
    
    def on_update_add_list_box( self, event ):
        event.Skip()
    
    def on_add_list_control( self, event ):
        event.Skip()
    
    def on_update_add_list_control( self, event ):
        event.Skip()
    
    def on_add_check_box( self, event ):
        event.Skip()
    
    def a( self, event ):
        event.Skip()
    
    def on_update_add_check_box( self, event ):
        event.Skip()
    
    def on_add_radio_box( self, event ):
        event.Skip()
    
    def on_update_add_radio_box( self, event ):
        event.Skip()
    
    def on_add_radio_button( self, event ):
        event.Skip()
    
    def on_update_add_radio_button( self, event ):
        event.Skip()
    
    def on_add_static_line( self, event ):
        event.Skip()
    
    def on_update_add_static_line( self, event ):
        event.Skip()
    
    def on_add_slider( self, event ):
        event.Skip()
    
    def on_update_add_slider( self, event ):
        event.Skip()
    
    def on_add_gauge( self, event ):
        event.Skip()
    
    def on_update_add_gauge( self, event ):
        event.Skip()
    
    def on_add_tree_control( self, event ):
        event.Skip()
    
    def on_update_add_tree_control( self, event ):
        event.Skip()
    
    def on_add_grid( self, event ):
        event.Skip()
    
    def on_update_add_grid( self, event ):
        event.Skip()
    
    def on_add_check_listbox( self, event ):
        event.Skip()
    
    def on_update_add_check_listbox( self, event ):
        event.Skip()
    
    def on_add_tree_listcontrol( self, event ):
        event.Skip()
    
    def on_update_add_tree_listcontrol( self, event ):
        event.Skip()
    
    def on_add_tree_listcontrol_column( self, event ):
        event.Skip()
    
    def on_update_add_tree_listcontrol_column( self, event ):
        event.Skip()
    
    def on_add_dataview_control( self, event ):
        event.Skip()
    
    def on_update_add_dataview_control( self, event ):
        event.Skip()
    
    def on_add_dataview_tree_control( self, event ):
        event.Skip()
    
    def on_update_add_dataview_tree_control( self, event ):
        event.Skip()
    
    def on_add_dataview_listcontrol( self, event ):
        event.Skip()
    
    def on_update_add_dataview_listcontrol( self, event ):
        event.Skip()
    
    def on_add_dataview_column( self, event ):
        event.Skip()
    
    def on_update_add_dataview_column( self, event ):
        event.Skip()
    
    def on_add_dataview_listcolumn( self, event ):
        event.Skip()
    
    def on_update_add_dataview_listcolumn( self, event ):
        event.Skip()
    
    def on_add_property_grid( self, event ):
        event.Skip()
    
    def on_update_add_property_grid( self, event ):
        event.Skip()
    
    def on_add_property_grid_manager( self, event ):
        event.Skip()
    
    def on_update_add_property_grid_manager( self, event ):
        event.Skip()
    
    def on_add_property_page( self, event ):
        event.Skip()
    
    def on_update_add_property_page( self, event ):
        event.Skip()
    
    def on_add_property_grid_item( self, event ):
        event.Skip()
    
    def on_update_add_property_grid_item( self, event ):
        event.Skip()
    
    def on_add_html_window( self, event ):
        event.Skip()
    
    def on_update_add_html_window( self, event ):
        event.Skip()
    
    def on_add_rich_text_control( self, event ):
        event.Skip()
    
    def on_update_add_rich_text_control( self, event ):
        event.Skip()
    
    def on_add_styledtext_control( self, event ):
        event.Skip()
    
    def on_update_add_styledtext_control( self, event ):
        event.Skip()
    
    def on_add_toggle_button( self, event ):
        event.Skip()
    
    def on_update_add_toggle_button( self, event ):
        event.Skip()
    
    def on_add_search_control( self, event ):
        event.Skip()
    
    def on_update_add_search_control( self, event ):
        event.Skip()
    
    def on_add_color_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_color_picker_control( self, event ):
        event.Skip()
    
    def on_add_font_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_font_picker_control( self, event ):
        event.Skip()
    
    def on_add_file_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_file_picker_control( self, event ):
        event.Skip()
    
    def on_add_dir_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_dir_picker_control( self, event ):
        event.Skip()
    
    def on_add_date_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_date_picker_control( self, event ):
        event.Skip()
    
    def on_add_calendar_control( self, event ):
        event.Skip()
    
    def on_update_add_calendar_control( self, event ):
        event.Skip()
    
    def on_add_scrollbar_control( self, event ):
        event.Skip()
    
    def on_update_add_scrollbar_control( self, event ):
        event.Skip()
    
    def on_add_spin_control( self, event ):
        event.Skip()
    
    def on_update_add_spin_control( self, event ):
        event.Skip()
    
    def on_add_spin_double_control( self, event ):
        event.Skip()
    
    def on_update_add_spin_double_control( self, event ):
        event.Skip()
    
    def on_add_spin_button( self, event ):
        event.Skip()
    
    def on_update_add_spin_button( self, event ):
        event.Skip()
    
    def on_add_hyperlink_control( self, event ):
        event.Skip()
    
    def on_update_add_hyperlink_control( self, event ):
        event.Skip()
    
    def on_add_generic_dir_control( self, event ):
        event.Skip()
    
    def on_update_add_generic_dir_control( self, event ):
        event.Skip()
    
    def on_add_custom_control( self, event ):
        event.Skip()
    
    def on_update_add_custom_control( self, event ):
        event.Skip()
    
    def on_add_timer_control( self, event ):
        event.Skip()
    
    def on_update_add_timer_control( self, event ):
        event.Skip()
    
    def on_property_changed( self, event ):
        event.Skip()
    
    def on_property_changing( self, event ):
        event.Skip()
    
    def m_splitter2OnIdle( self, event ):
        self.m_splitter2.SetSashPosition( 255 )
        self.m_splitter2.Unbind( wx.EVT_IDLE )
    
    def m_splitter1OnIdle( self, event ):
        self.m_splitter1.SetSashPosition( 0 )
        self.m_splitter1.Unbind( wx.EVT_IDLE )
    

###########################################################################
## Class FakeFormBase
###########################################################################

class FakeFormBase ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 717,448 ), style = 0 )
        
        self.m_content = wx.FlexGridSizer( 5, 1, 0, 0 )
        self.m_content.AddGrowableCol( 0 )
        self.m_content.AddGrowableRow( 3 )
        self.m_content.SetFlexibleDirection( wx.BOTH )
        self.m_content.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_caption = wx.Panel( self, wx.ID_ANY, wx.Point( -1,-1 ), wx.DefaultSize, wx.NO_BORDER )
        self.m_caption.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_CAPTIONTEXT ) )
        self.m_caption.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_ACTIVECAPTION ) )
        
        fgSizer14 = wx.FlexGridSizer( 1, 5, 0, 0 )
        fgSizer14.AddGrowableCol( 1 )
        fgSizer14.AddGrowableRow( 0 )
        fgSizer14.SetFlexibleDirection( wx.BOTH )
        fgSizer14.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_bitmap6 = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_app_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( -1,-1 ), wx.Size( 24,24 ), 0 )
        self.m_bitmap6.SetMinSize( wx.Size( 24,24 ) )
        self.m_bitmap6.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_bitmap6, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_title = wx.StaticText( self.m_caption, wx.ID_ANY, u"wx.Frame", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE|wx.NO_BORDER )
        self.m_title.Wrap( -1 )
        base_colour = wx.SystemSettings.GetColour(wx.SYS_COLOUR_ACTIVECAPTION)
        alternate_colour = wx.Colour(
            255-base_colour.Red(),
            255-base_colour.Green(),
            255-base_colour.Green()
        )
        self.m_title.SetForegroundColour(alternate_colour)
        self.m_title.SetBackgroundColour(base_colour)
        
        fgSizer14.Add( self.m_title, 1, wx.LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_minimize_box = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_minimize_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( 0,0 ), wx.Size( 24,24 ), 0 )
        self.m_minimize_box.SetMinSize( wx.Size( 24,24 ) )
        self.m_minimize_box.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_minimize_box, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_maximize_box = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_restore_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( 0,0 ), wx.Size( 24,24 ), 0 )
        self.m_maximize_box.SetMinSize( wx.Size( 24,24 ) )
        self.m_maximize_box.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_maximize_box, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_close_box = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_close_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( 0,0 ), wx.Size( 24,24 ), 0 )
        self.m_close_box.SetMinSize( wx.Size( 24,24 ) )
        self.m_close_box.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_close_box, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        
        self.m_caption.SetSizer( fgSizer14 )
        self.m_caption.Layout()
        fgSizer14.Fit( self.m_caption )
        self.m_content.Add( self.m_caption, 1, wx.EXPAND, 5 )
        
        self.m_menu_bar = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_menu_bar_sizer = wx.FlexGridSizer( 1, 1, 0, 0 )
        self.m_menu_bar_sizer.AddGrowableCol( 0 )
        self.m_menu_bar_sizer.AddGrowableRow( 0 )
        self.m_menu_bar_sizer.SetFlexibleDirection( wx.BOTH )
        self.m_menu_bar_sizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        self.m_menu_bar.SetSizer( self.m_menu_bar_sizer )
        self.m_menu_bar.Layout()
        self.m_menu_bar_sizer.Fit( self.m_menu_bar )
        self.m_content.Add( self.m_menu_bar, 1, wx.EXPAND, 0 )
        
        self.m_toolbar = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
        self.m_toolbar.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW ) )
        self.m_toolbar.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW ) )
        
        self.m_toolbar_sizer = wx.FlexGridSizer( 1, 1, 0, 0 )
        self.m_toolbar_sizer.AddGrowableCol( 0 )
        self.m_toolbar_sizer.AddGrowableRow( 0 )
        self.m_toolbar_sizer.SetFlexibleDirection( wx.BOTH )
        self.m_toolbar_sizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        self.m_toolbar.SetSizer( self.m_toolbar_sizer )
        self.m_toolbar.Layout()
        self.m_toolbar_sizer.Fit( self.m_toolbar )
        self.m_content.Add( self.m_toolbar, 0, wx.EXPAND, 5 )
        
        self.m_aux = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_aux.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME ) )
        
        self.m_aux_sizer = wx.FlexGridSizer( 1, 2, 0, 0 )
        self.m_aux_sizer.AddGrowableCol( 1 )
        self.m_aux_sizer.AddGrowableRow( 0 )
        self.m_aux_sizer.SetFlexibleDirection( wx.BOTH )
        self.m_aux_sizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_vertical_toolbar = wx.Panel( self.m_aux, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_vertical_toolbar_sizer = wx.FlexGridSizer( 1, 1, 0, 0 )
        self.m_vertical_toolbar_sizer.AddGrowableCol( 0 )
        self.m_vertical_toolbar_sizer.AddGrowableRow( 0 )
        self.m_vertical_toolbar_sizer.SetFlexibleDirection( wx.BOTH )
        self.m_vertical_toolbar_sizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        self.m_vertical_toolbar.SetSizer( self.m_vertical_toolbar_sizer )
        self.m_vertical_toolbar.Layout()
        self.m_vertical_toolbar_sizer.Fit( self.m_vertical_toolbar )
        self.m_aux_sizer.Add( self.m_vertical_toolbar, 1, wx.EXPAND, 5 )
        
        self.m_content_client = wx.Panel( self.m_aux, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_aux_sizer.Add( self.m_content_client, 0, wx.EXPAND, 5 )
        
        
        self.m_aux.SetSizer( self.m_aux_sizer )
        self.m_aux.Layout()
        self.m_aux_sizer.Fit( self.m_aux )
        self.m_content.Add( self.m_aux, 1, wx.EXPAND, 5 )
        
        self.m_status_bar = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
        self.m_status_bar_sizer = wx.FlexGridSizer( 1, 1, 0, 0 )
        self.m_status_bar_sizer.AddGrowableCol( 0 )
        self.m_status_bar_sizer.AddGrowableRow( 0 )
        self.m_status_bar_sizer.SetFlexibleDirection( wx.BOTH )
        self.m_status_bar_sizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        self.m_status_bar.SetSizer( self.m_status_bar_sizer )
        self.m_status_bar.Layout()
        self.m_status_bar_sizer.Fit( self.m_status_bar )
        self.m_content.Add( self.m_status_bar, 1, wx.EXPAND, 0 )
        
        
        self.SetSizer( self.m_content )
        self.Layout()
    
    def __del__( self ):
        pass
    

###########################################################################
## Class FakeDialogBase
###########################################################################

class FakeDialogBase ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
        
        self.m_content = wx.FlexGridSizer( 2, 1, 0, 0 )
        self.m_content.AddGrowableCol( 0 )
        self.m_content.AddGrowableRow( 1 )
        self.m_content.SetFlexibleDirection( wx.BOTH )
        self.m_content.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_caption = wx.Panel( self, wx.ID_ANY, wx.Point( -1,-1 ), wx.DefaultSize, wx.NO_BORDER )
        self.m_caption.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT ) )
        self.m_caption.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_ACTIVECAPTION ) )
        
        fgSizer14 = wx.FlexGridSizer( 1, 5, 0, 0 )
        fgSizer14.AddGrowableCol( 1 )
        fgSizer14.AddGrowableRow( 0 )
        fgSizer14.SetFlexibleDirection( wx.BOTH )
        fgSizer14.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_bitmap6 = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_app_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( -1,-1 ), wx.Size( 24,24 ), 0 )
        self.m_bitmap6.SetMinSize( wx.Size( 24,24 ) )
        self.m_bitmap6.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_bitmap6, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_title = wx.StaticText( self.m_caption, wx.ID_ANY, u"wx.Dialog", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE|wx.NO_BORDER )
        self.m_title.Wrap( -1 )
        self.m_title.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_CAPTIONTEXT ) )
        self.m_title.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_ACTIVECAPTION ) )
        
        fgSizer14.Add( self.m_title, 1, wx.LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_bitmap3 = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_minimize_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( 0,0 ), wx.Size( 24,24 ), 0 )
        self.m_bitmap3.SetMinSize( wx.Size( 24,24 ) )
        self.m_bitmap3.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_bitmap3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_bitmap4 = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_restore_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( 0,0 ), wx.Size( 24,24 ), 0 )
        self.m_bitmap4.SetMinSize( wx.Size( 24,24 ) )
        self.m_bitmap4.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_bitmap4, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_bitmap5 = wx.StaticBitmap( self.m_caption, wx.ID_ANY, wx.Bitmap(u"../res/_close_caption.xpm", wx.BITMAP_TYPE_ANY), wx.Point( 0,0 ), wx.Size( 24,24 ), 0 )
        self.m_bitmap5.SetMinSize( wx.Size( 24,24 ) )
        self.m_bitmap5.SetMaxSize( wx.Size( 24,24 ) )
        
        fgSizer14.Add( self.m_bitmap5, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        
        self.m_caption.SetSizer( fgSizer14 )
        self.m_caption.Layout()
        fgSizer14.Fit( self.m_caption )
        self.m_content.Add( self.m_caption, 1, wx.EXPAND, 5 )
        
        self.m_content_client = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_content_client.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME ) )
        
        self.m_content.Add( self.m_content_client, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( self.m_content )
        self.Layout()
    
    def __del__( self ):
        pass
    

###########################################################################
## Class FakePaneBase
###########################################################################

class FakePaneBase ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.Point( -1,-1 ), size = wx.Size( 831,400 ), style = wx.TAB_TRAVERSAL|wx.WANTS_CHARS )
        
        self.SetExtraStyle( wx.WS_EX_PROCESS_IDLE|wx.WS_EX_PROCESS_UI_UPDATES )
        self.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME ) )
        self.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME ) )
        
        
        # Connect Events
        self.Bind( wx.EVT_RIGHT_UP, self.aaa )
    
    def __del__( self ):
        pass
    
    
    # Virtual event handlers, overide them in your derived class
    def aaa( self, event ):
        event.Skip()
    

###########################################################################
## Class FakeWizardBase
###########################################################################

class FakeWizardBase ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 467,401 ), style = wx.TAB_TRAVERSAL )
        
        fgSizer17 = wx.FlexGridSizer( 4, 1, 0, 0 )
        fgSizer17.AddGrowableCol( 0 )
        fgSizer17.AddGrowableRow( 1 )
        fgSizer17.SetFlexibleDirection( wx.BOTH )
        fgSizer17.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_panel21 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,30 ), wx.TAB_TRAVERSAL )
        self.m_panel21.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_ACTIVECAPTION ) )
        
        fgSizer19 = wx.FlexGridSizer( 1, 4, 0, 0 )
        fgSizer19.AddGrowableCol( 2 )
        fgSizer19.AddGrowableRow( 0 )
        fgSizer19.SetFlexibleDirection( wx.BOTH )
        fgSizer19.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        fgSizer19.SetMinSize( wx.Size( -1,29 ) ) 
        
        fgSizer19.Add( ( 12, 0), 1, wx.EXPAND, 5 )
        
        self.m_title = wx.StaticText( self.m_panel21, wx.ID_ANY, u"wizard", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
        self.m_title.Wrap( -1 )
        self.m_title.SetFont( wx.Font( 12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
        
        fgSizer19.Add( self.m_title, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
        
        
        fgSizer19.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        self.m_button5 = wx.Button( self.m_panel21, wx.ID_ANY, u"x", wx.DefaultPosition, wx.Size( 16,16 ), 0 )
        self.m_button5.SetFont( wx.Font( 12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
        
        fgSizer19.Add( self.m_button5, 0, wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
        
        
        self.m_panel21.SetSizer( fgSizer19 )
        self.m_panel21.Layout()
        fgSizer17.Add( self.m_panel21, 1, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
        
        self.m_content_client = wx.Simplebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        
        fgSizer17.Add( self.m_content_client, 1, wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
        
        self.m_staticline1 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        fgSizer17.Add( self.m_staticline1, 0, wx.EXPAND |wx.ALL, 5 )
        
        fgSizer18 = wx.FlexGridSizer( 1, 4, 0, 0 )
        fgSizer18.AddGrowableCol( 0 )
        fgSizer18.SetFlexibleDirection( wx.BOTH )
        fgSizer18.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        fgSizer18.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        self.m_left_button = wx.Button( self, wx.ID_ANY, u"< Back", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_left_button.Enable( False )
        
        fgSizer18.Add( self.m_left_button, 0, wx.ALL, 5 )
        
        self.m_middle_button = wx.Button( self, wx.ID_ANY, u"Next >", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_middle_button.Enable( False )
        
        fgSizer18.Add( self.m_middle_button, 0, wx.ALL, 5 )
        
        self.m_right_button = wx.Button( self, wx.ID_ANY, u"&Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer18.Add( self.m_right_button, 0, wx.ALL, 5 )
        
        
        fgSizer17.Add( fgSizer18, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )
        
        
        self.SetSizer( fgSizer17 )
        self.Layout()
    
    def __del__( self ):
        pass
    

###########################################################################
## Class FakeStatusBarBase
###########################################################################

class FakeStatusBarBase ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,27 ), style = wx.TAB_TRAVERSAL )
        
        fgSizer23 = wx.FlexGridSizer( 1, 2, 0, 0 )
        fgSizer23.AddGrowableCol( 0 )
        fgSizer23.AddGrowableRow( 0 )
        fgSizer23.SetFlexibleDirection( wx.BOTH )
        fgSizer23.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        fgSizer23.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        self.m_grip = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap(u"../../res/status_bar_grip.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( 27,25 ), 0 )
        fgSizer23.Add( self.m_grip, 1, wx.EXPAND, 0 )
        
        
        self.SetSizer( fgSizer23 )
        self.Layout()
    
    def __del__( self ):
        pass
    

###########################################################################
## Class DesignBase
###########################################################################

class DesignBase ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 1051,482 ), style = wx.TAB_TRAVERSAL )
        
        self.m_mgr = wx.aui.AuiManager()
        self.m_mgr.SetManagedWindow( self )
        self.m_mgr.SetFlags(wx.aui.AUI_MGR_DEFAULT)
        
        self.m_scrolledWindow4 = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.SIMPLE_BORDER|wx.VSCROLL )
        self.m_scrolledWindow4.SetScrollRate( 5, 5 )
        self.m_scrolledWindow4.SetExtraStyle( wx.WS_EX_PROCESS_UI_UPDATES|wx.WS_EX_VALIDATE_RECURSIVELY )
        self.m_scrolledWindow4.SetFont( wx.Font( 10, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Sans" ) )
        self.m_scrolledWindow4.SetMinSize( wx.Size( 68,-1 ) )
        self.m_scrolledWindow4.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))  # fix windows
        self.m_mgr.AddPane( self.m_scrolledWindow4, wx.aui.AuiPaneInfo() .Name( u"controls" ).Left() .Caption( u"controls" ).CloseButton( False ).PinButton( True ).Dock().Resizable().FloatingSize( wx.Size( 156,670 ) ).Row( 0 ).MinSize( wx.Size( 68,-1 ) ).Layer( 3 ) )
        
        fgSizer30 = wx.FlexGridSizer( 20, 1, 0, 0 )
        fgSizer30.AddGrowableCol( 0 )
        fgSizer30.SetFlexibleDirection( wx.VERTICAL )
        fgSizer30.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_ALL )
        
        self.m_staticText52 = wx.StaticText( self.m_scrolledWindow4, wx.ID_ANY, u"Forms", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText52.Wrap( -1 )
        fgSizer30.Add( self.m_staticText52, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )
        
        self.wSizer4 = wx.WrapSizer( wx.HORIZONTAL )
        
        self.m_bpButton1 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/frame.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton1.SetToolTip(u"frame")
        
        self.wSizer4.Add( self.m_bpButton1, 0, 0, 5 )
        
        self.m_bpButton2 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/panel.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton2.SetToolTip(u"panel")
        
        self.wSizer4.Add( self.m_bpButton2, 0, 0, 5 )
        
        self.m_bpButton3 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/dialog.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton3.SetToolTip(u"dialog")
        
        self.wSizer4.Add( self.m_bpButton3, 0, 0, 5 )
        
        self.m_bpButton4 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/wizard.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton4.SetToolTip(u"wizard")
        
        self.wSizer4.Add( self.m_bpButton4, 0, 0, 5 )
        
        self.m_bpButton5 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/wizard_page.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton5.SetToolTip(u"wizard page")
        
        self.wSizer4.Add( self.m_bpButton5, 0, 0, 5 )
        
        self.m_bpButton6 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/menu_bar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton6.SetToolTip(u"menu bar")
        
        self.wSizer4.Add( self.m_bpButton6, 0, 0, 5 )
        
        self.m_bpButton7 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/toolbar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton7.SetToolTip(u"tool bar")
        
        self.wSizer4.Add( self.m_bpButton7, 0, 0, 5 )
        
        
        fgSizer30.Add( self.wSizer4, 1, wx.RIGHT|wx.LEFT|wx.EXPAND, 5 )
        
        self.m_staticline2 = wx.StaticLine( self.m_scrolledWindow4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        fgSizer30.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )
        
        self.m_staticText53 = wx.StaticText( self.m_scrolledWindow4, wx.ID_ANY, u"Menu && toolbar", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText53.Wrap( -1 )
        fgSizer30.Add( self.m_staticText53, 0, wx.RIGHT|wx.LEFT, 5 )
        
        wSizer6 = wx.WrapSizer( wx.HORIZONTAL )
        
        self.m_bpButton8 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/status_bar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton8.SetToolTip(u"status bar")
        
        wSizer6.Add( self.m_bpButton8, 0, 0, 5 )
        
        self.m_bpButton9 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/menu_bar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton9.SetToolTip(u"menu bar")
        
        wSizer6.Add( self.m_bpButton9, 0, 0, 5 )
        
        self.m_bpButton10 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/menu.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton10.SetToolTip(u"menu")
        
        wSizer6.Add( self.m_bpButton10, 0, 0, 5 )
        
        self.m_bpButton11 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/submenu.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton11.SetToolTip(u"sub menu")
        
        wSizer6.Add( self.m_bpButton11, 0, 0, 5 )
        
        self.m_bpButton12 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/menu_item.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton12.SetToolTip(u"menu item")
        
        wSizer6.Add( self.m_bpButton12, 0, 0, 5 )
        
        self.m_bpButton13 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/menu_separator.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton13.SetToolTip(u"menu item separator")
        
        wSizer6.Add( self.m_bpButton13, 0, 0, 5 )
        
        self.m_bpButton14 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/toolbar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton14.SetToolTip(u"tool bar")
        
        wSizer6.Add( self.m_bpButton14, 0, 0, 5 )
        
        self.m_bpButton15 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/aui_toolbar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton15.SetToolTip(u"aui tool bar")
        
        wSizer6.Add( self.m_bpButton15, 0, 0, 5 )
        
        self.m_bpButton16 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/tool.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton16.SetToolTip(u"tool")
        
        wSizer6.Add( self.m_bpButton16, 0, 0, 5 )
        
        self.m_bpButton17 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/tool_separator.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton17.SetToolTip(u"tool separator")
        
        wSizer6.Add( self.m_bpButton17, 0, 0, 5 )
        
        self.m_bpButton69 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/info_bar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton69.SetToolTip(u"info bar")
        
        wSizer6.Add( self.m_bpButton69, 0, 0, 5 )
        
        
        fgSizer30.Add( wSizer6, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
        
        self.m_staticline3 = wx.StaticLine( self.m_scrolledWindow4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        fgSizer30.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )
        
        self.m_staticText60 = wx.StaticText( self.m_scrolledWindow4, wx.ID_ANY, u"Layout", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText60.Wrap( -1 )
        fgSizer30.Add( self.m_staticText60, 0, wx.RIGHT|wx.LEFT, 5 )
        
        wSizer13 = wx.WrapSizer( wx.HORIZONTAL )
        
        self.m_bpButton70 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/sizer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton70.SetToolTip(u"box sizer")
        
        wSizer13.Add( self.m_bpButton70, 0, 0, 5 )
        
        self.m_bpButton71 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/wrap_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton71.SetToolTip(u"wrap sizer")
        
        wSizer13.Add( self.m_bpButton71, 0, 0, 5 )
        
        self.m_bpButton72 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/static_box_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton72.SetToolTip(u"static box sizer")
        
        wSizer13.Add( self.m_bpButton72, 0, 0, 5 )
        
        self.m_bpButton73 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/grid_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton73.SetToolTip(u"grid sizer")
        
        wSizer13.Add( self.m_bpButton73, 0, 0, 5 )
        
        self.m_bpButton74 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/flexible_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton74.SetToolTip(u"flex grid sizer")
        
        wSizer13.Add( self.m_bpButton74, 0, 0, 5 )
        
        self.m_bpButton75 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/grid_bag_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton75.SetToolTip(u"grid bag sizer")
        
        wSizer13.Add( self.m_bpButton75, 0, 0, 5 )
        
        self.m_bpButton76 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/dialog_buttons_sizer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton76.SetToolTip(u"dialog buttons sizer")
        
        wSizer13.Add( self.m_bpButton76, 0, 0, 5 )
        
        self.m_bpButton77 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/separator.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton77.SetToolTip(u"spacer")
        
        wSizer13.Add( self.m_bpButton77, 0, 0, 5 )
        
        
        fgSizer30.Add( wSizer13, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
        
        self.m_staticline4 = wx.StaticLine( self.m_scrolledWindow4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        fgSizer30.Add( self.m_staticline4, 0, wx.EXPAND |wx.ALL, 5 )
        
        self.m_staticText61 = wx.StaticText( self.m_scrolledWindow4, wx.ID_ANY, u"Containers", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText61.Wrap( -1 )
        fgSizer30.Add( self.m_staticText61, 0, wx.RIGHT|wx.LEFT, 5 )
        
        wSizer14 = wx.WrapSizer( wx.HORIZONTAL )
        
        self.m_bpButton87 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/panel.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton87.SetToolTip(u"panel")
        
        wSizer14.Add( self.m_bpButton87, 0, 0, 5 )
        
        self.m_bpButton88 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/splitter_window.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton88.SetToolTip(u"splitter window")
        
        wSizer14.Add( self.m_bpButton88, 0, 0, 5 )
        
        self.m_bpButton89 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/scrolled_window.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton89.SetToolTip(u"scrolled window")
        
        wSizer14.Add( self.m_bpButton89, 0, 0, 5 )
        
        self.m_bpButton90 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/notebook.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton90.SetToolTip(u"note book")
        
        wSizer14.Add( self.m_bpButton90, 0, 0, 5 )
        
        self.m_bpButton91 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/collapsible_panel.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton91.SetToolTip(u"collapsible panel")
        
        wSizer14.Add( self.m_bpButton91, 0, 0, 5 )
        
        self.m_bpButton92 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/aui_notebook.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton92.SetToolTip(u"aui note book")
        
        wSizer14.Add( self.m_bpButton92, 0, 0, 5 )
        
        self.m_bpButton93 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/list_book.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton93.SetToolTip(u"list book")
        
        wSizer14.Add( self.m_bpButton93, 0, 0, 5 )
        
        self.m_bpButton94 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/choice_book.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton94.SetToolTip(u"choice book")
        
        wSizer14.Add( self.m_bpButton94, 0, 0, 5 )
        
        self.m_bpButton95 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/simple_book.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton95.SetToolTip(u"simple book")
        
        wSizer14.Add( self.m_bpButton95, 0, 0, 5 )
        
        
        fgSizer30.Add( wSizer14, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
        
        self.m_staticline5 = wx.StaticLine( self.m_scrolledWindow4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        fgSizer30.Add( self.m_staticline5, 0, wx.EXPAND |wx.ALL, 5 )
        
        self.m_staticText62 = wx.StaticText( self.m_scrolledWindow4, wx.ID_ANY, u"Common", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText62.Wrap( -1 )
        fgSizer30.Add( self.m_staticText62, 0, wx.RIGHT|wx.LEFT, 5 )
        
        wSizer15 = wx.WrapSizer( wx.HORIZONTAL )
        
        self.m_bpButton96 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/button.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton96.SetToolTip(u"button")
        
        wSizer15.Add( self.m_bpButton96, 0, 0, 5 )
        
        self.m_bpButton97 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/bitmap_button.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton97.SetToolTip(u"bitmap button")
        
        wSizer15.Add( self.m_bpButton97, 0, 0, 5 )
        
        self.m_bpButton98 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/static_text.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton98.SetToolTip(u"static text")
        
        wSizer15.Add( self.m_bpButton98, 0, 0, 5 )
        
        self.m_bpButton99 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/text_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton99.SetToolTip(u"edit text")
        
        wSizer15.Add( self.m_bpButton99, 0, 0, 5 )
        
        self.m_bpButton100 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/static_bitmap.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton100.SetToolTip(u"bitmap")
        
        wSizer15.Add( self.m_bpButton100, 0, 0, 5 )
        
        self.m_bpButton101 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/animation_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton101.SetToolTip(u"animation")
        
        wSizer15.Add( self.m_bpButton101, 0, 0, 5 )
        
        self.m_bpButton102 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/combo_box.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton102.SetToolTip(u"combo box")
        
        wSizer15.Add( self.m_bpButton102, 0, 0, 5 )
        
        self.m_bpButton103 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/bitmap_combo_box.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton103.SetToolTip(u"bitmap combo box")
        
        wSizer15.Add( self.m_bpButton103, 0, 0, 5 )
        
        self.m_bpButton104 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/choice_box.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton104.SetToolTip(u"choice box")
        
        wSizer15.Add( self.m_bpButton104, 0, 0, 5 )
        
        self.m_bpButton105 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/list_box.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton105.SetToolTip(u"list box")
        
        wSizer15.Add( self.m_bpButton105, 0, 0, 5 )
        
        self.m_bpButton106 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/list_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton106.SetToolTip(u"list control")
        
        wSizer15.Add( self.m_bpButton106, 0, 0, 5 )
        
        self.m_bpButton107 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/check_box.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton107.SetToolTip(u"check box")
        
        wSizer15.Add( self.m_bpButton107, 0, 0, 5 )
        
        self.m_bpButton108 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/radio_box.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton108.SetToolTip(u"radio box")
        
        wSizer15.Add( self.m_bpButton108, 0, 0, 5 )
        
        self.m_bpButton109 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/radio_button.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton109.SetToolTip(u"radio button")
        
        wSizer15.Add( self.m_bpButton109, 0, 0, 5 )
        
        self.m_bpButton110 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/static_line.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton110.SetToolTip(u"static line")
        
        wSizer15.Add( self.m_bpButton110, 0, 0, 5 )
        
        self.m_bpButton111 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/slider.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton111.SetToolTip(u"slider")
        
        wSizer15.Add( self.m_bpButton111, 0, 0, 5 )
        
        self.m_bpButton112 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/gauge.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton112.SetToolTip(u"gauge")
        
        wSizer15.Add( self.m_bpButton112, 0, 0, 5 )
        
        
        fgSizer30.Add( wSizer15, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
        
        self.m_staticline6 = wx.StaticLine( self.m_scrolledWindow4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        fgSizer30.Add( self.m_staticline6, 0, wx.EXPAND |wx.ALL, 5 )
        
        self.m_staticText63 = wx.StaticText( self.m_scrolledWindow4, wx.ID_ANY, u"Data", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText63.Wrap( -1 )
        fgSizer30.Add( self.m_staticText63, 0, wx.RIGHT|wx.LEFT, 5 )
        
        wSizer16 = wx.WrapSizer( wx.HORIZONTAL )
        
        self.m_bpButton114 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/tree_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton114.SetToolTip(u"tree")
        
        wSizer16.Add( self.m_bpButton114, 0, 0, 5 )
        
        self.m_bpButton115 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/grid_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton115.SetToolTip(u"grid control")
        
        wSizer16.Add( self.m_bpButton115, 0, 0, 5 )
        
        self.m_bpButton116 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/check_listbox.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton116.SetToolTip(u"check list box")
        
        wSizer16.Add( self.m_bpButton116, 0, 0, 5 )
        
        self.m_bpButton117 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/tree_list_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton117.SetToolTip(u"tree list")
        
        wSizer16.Add( self.m_bpButton117, 0, 0, 5 )
        
        self.m_bpButton118 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/tree_list_control_column.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton118.SetToolTip(u"tree list column")
        
        wSizer16.Add( self.m_bpButton118, 0, 0, 5 )
        
        self.m_bpButton119 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/data_view_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton119.SetToolTip(u"data view")
        
        wSizer16.Add( self.m_bpButton119, 0, 0, 5 )
        
        self.m_bpButton120 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/data_view_tree_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton120.SetToolTip(u"data view tree")
        
        wSizer16.Add( self.m_bpButton120, 0, 0, 5 )
        
        self.m_bpButton121 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/data_view_list_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton121.SetToolTip(u"data view list")
        
        wSizer16.Add( self.m_bpButton121, 0, 0, 5 )
        
        self.m_bpButton122 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/data_view_column.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton122.SetToolTip(u"data view column")
        
        wSizer16.Add( self.m_bpButton122, 0, 0, 5 )
        
        self.m_bpButton123 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/data_view_column.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton123.SetToolTip(u"data view list column")
        
        wSizer16.Add( self.m_bpButton123, 0, 0, 5 )
        
        self.m_bpButton124 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/property_grid.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton124.SetToolTip(u"property grid")
        
        wSizer16.Add( self.m_bpButton124, 0, 0, 5 )
        
        self.m_bpButton125 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/property_grid_manager.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton125.SetToolTip(u"property grid manager")
        
        wSizer16.Add( self.m_bpButton125, 0, 0, 5 )
        
        self.m_bpButton126 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/property_grid_page.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton126.SetToolTip(u"property grid page")
        
        wSizer16.Add( self.m_bpButton126, 0, 0, 5 )
        
        self.m_bpButton127 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/property_grid_item.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton127.SetToolTip(u"property")
        
        wSizer16.Add( self.m_bpButton127, 0, 0, 5 )
        
        
        fgSizer30.Add( wSizer16, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
        
        self.m_staticline7 = wx.StaticLine( self.m_scrolledWindow4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        fgSizer30.Add( self.m_staticline7, 0, wx.EXPAND |wx.ALL, 5 )
        
        self.m_staticText64 = wx.StaticText( self.m_scrolledWindow4, wx.ID_ANY, u"Additional", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText64.Wrap( -1 )
        fgSizer30.Add( self.m_staticText64, 0, wx.RIGHT|wx.LEFT, 5 )
        
        wSizer17 = wx.WrapSizer( wx.HORIZONTAL )
        
        self.m_bpButton128 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/html_window.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton128.SetToolTip(u"html window")
        
        wSizer17.Add( self.m_bpButton128, 0, 0, 5 )
        
        self.m_bpButton129 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/rich_text.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton129.SetToolTip(u"rich text")
        
        wSizer17.Add( self.m_bpButton129, 0, 0, 5 )
        
        self.m_bpButton130 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/styled_text.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton130.SetToolTip(u"styled text")
        
        wSizer17.Add( self.m_bpButton130, 0, 0, 5 )
        
        self.m_bpButton131 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/toggle_button.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton131.SetToolTip(u"toggle button")
        
        wSizer17.Add( self.m_bpButton131, 0, 0, 5 )
        
        self.m_bpButton132 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/search_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton132.SetToolTip(u"search")
        
        wSizer17.Add( self.m_bpButton132, 0, 0, 5 )
        
        self.m_bpButton133 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/colour_picker.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton133.SetToolTip(u"colour picker")
        
        wSizer17.Add( self.m_bpButton133, 0, 0, 5 )
        
        self.m_bpButton134 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/font_picker.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton134.SetToolTip(u"font picker")
        
        wSizer17.Add( self.m_bpButton134, 0, 0, 5 )
        
        self.m_bpButton135 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/file_picker.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton135.SetToolTip(u"file picker")
        
        wSizer17.Add( self.m_bpButton135, 0, 0, 5 )
        
        self.m_bpButton136 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/dir_picker.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton136.SetToolTip(u"dir picker")
        
        wSizer17.Add( self.m_bpButton136, 0, 0, 5 )
        
        self.m_bpButton137 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/date_picker.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton137.SetToolTip(u"date picker")
        
        wSizer17.Add( self.m_bpButton137, 0, 0, 5 )
        
        self.m_bpButton138 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/calendar_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton138.SetToolTip(u"calendar")
        
        wSizer17.Add( self.m_bpButton138, 0, 0, 5 )
        
        self.m_bpButton139 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/scroll_bar.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton139.SetToolTip(u"scroll bar")
        
        wSizer17.Add( self.m_bpButton139, 0, 0, 5 )
        
        self.m_bpButton140 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/spin_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton140.SetToolTip(u"spin")
        
        wSizer17.Add( self.m_bpButton140, 0, 0, 5 )
        
        self.m_bpButton141 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/spin_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton141.SetToolTip(u"spin double")
        
        wSizer17.Add( self.m_bpButton141, 0, 0, 5 )
        
        self.m_bpButton142 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/spin_button.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton142.SetToolTip(u"spin button")
        
        wSizer17.Add( self.m_bpButton142, 0, 0, 5 )
        
        self.m_bpButton143 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/hyperlink_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton143.SetToolTip(u"hyperlink")
        
        wSizer17.Add( self.m_bpButton143, 0, 0, 5 )
        
        self.m_bpButton144 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/generic_dir_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton144.SetToolTip(u"generic dir")
        
        wSizer17.Add( self.m_bpButton144, 0, 0, 5 )
        
        self.m_bpButton145 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/custom_control.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton145.SetToolTip(u"custom control")
        
        wSizer17.Add( self.m_bpButton145, 0, 0, 5 )
        
        self.m_bpButton146 = wx.BitmapButton( self.m_scrolledWindow4, wx.ID_ANY, wx.Bitmap(u"../res/timer.xpm", wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.Size( -1,28 ), wx.BU_AUTODRAW )
        self.m_bpButton146.SetToolTip(u"timer")
        
        wSizer17.Add( self.m_bpButton146, 0, 0, 5 )
        
        
        fgSizer30.Add( wSizer17, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
        
        
        self.m_scrolledWindow4.SetSizer( fgSizer30 )
        self.m_scrolledWindow4.Layout()
        fgSizer30.Fit( self.m_scrolledWindow4 )
        self.m_treeCtrl = wx.TreeCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 200,-1 ), wx.TR_HAS_BUTTONS|wx.TR_LINES_AT_ROOT|wx.TR_SINGLE|wx.SIMPLE_BORDER|wx.WANTS_CHARS )
        self.m_treeCtrl.SetMinSize( wx.Size( 68,-1 ) )
        
        self.m_mgr.AddPane( self.m_treeCtrl, wx.aui.AuiPaneInfo() .Name( u"wxUI Components tree" ).Left() .Caption( u"wxUI Components tree" ).CloseButton( False ).PinButton( True ).Dock().Resizable().FloatingSize( wx.Size( 218,224 ) ).Row( 0 ).MinSize( wx.Size( 68,200 ) ).Layer( 3 ) )
        
        self.m_panel24 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_mgr.AddPane( self.m_panel24, wx.aui.AuiPaneInfo() .Name( u"wxUI preview" ).Left() .Caption( u"wxUI preview" ).CaptionVisible( False ).PinButton( True ).Movable( False ).Dock().Resizable().FloatingSize( wx.Size( 707,228 ) ).CentrePane() )
        
        fgSizer6 = wx.FlexGridSizer( 2, 1, 0, 0 )
        fgSizer6.AddGrowableCol( 0 )
        fgSizer6.AddGrowableRow( 1 )
        fgSizer6.SetFlexibleDirection( wx.BOTH )
        fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_toolBar9 = wx.ToolBar( self.m_panel24, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.m_align_left = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_left.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align left", wx.EmptyString, None ) 
        
        self.m_align_center = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_center.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align center", wx.EmptyString, None ) 
        
        self.m_align_right = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_right.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align right", wx.EmptyString, None ) 
        
        self.m_toolBar9.AddSeparator()
        
        self.m_align_top = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_top.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align top", wx.EmptyString, None ) 
        
        self.m_align_middle = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_middle.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align middle", wx.EmptyString, None ) 
        
        self.m_align_bottom = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/align_bottom.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Align bottom", wx.EmptyString, None ) 
        
        self.m_toolBar9.AddSeparator()
        
        self.m_expand = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/expand.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Expand", wx.EmptyString, None ) 
        
        self.m_stretch = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/stretch.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Stretch", wx.EmptyString, None ) 
        
        self.m_toolBar9.AddSeparator()
        
        self.m_left_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/left_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Left border", wx.EmptyString, None ) 
        
        self.m_right_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/right_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Right border", wx.EmptyString, None ) 
        
        self.m_top_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/top_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Top border", wx.EmptyString, None ) 
        
        self.m_bottom_border = self.m_toolBar9.AddTool( wx.ID_ANY, u"tool", wx.Bitmap(u"../res/bottom_border.xpm", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_CHECK, u"Bottom border", wx.EmptyString, None )
        
        self.m_toolBar9.Realize() 
        
        fgSizer6.Add( self.m_toolBar9, 0, wx.EXPAND, 5 )
        
        fgSizer22 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer22.AddGrowableCol( 0 )
        fgSizer22.AddGrowableRow( 0 )
        fgSizer22.SetFlexibleDirection( wx.BOTH )
        fgSizer22.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_panel22 = wx.Panel( self.m_panel24, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.m_panel22.SetBackgroundColour( wx.Colour(60, 100, 127) )
        
        fgSizer20 = wx.FlexGridSizer( 3, 3, 0, 0 )
        fgSizer20.AddGrowableCol( 1 )
        fgSizer20.AddGrowableRow( 1 )
        fgSizer20.SetFlexibleDirection( wx.BOTH )
        fgSizer20.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        
        fgSizer20.Add( ( 12, 12), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 12, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 12, 0), 1, wx.EXPAND, 5 )
        
        self.m_container = wx.ScrolledWindow( self.m_panel22, wx.ID_ANY, wx.Point( -1,-1 ), wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
        self.m_container.SetScrollRate( 5, 5 )
        self.m_container.SetExtraStyle( wx.WS_EX_BLOCK_EVENTS )
        self.m_container.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT ) )
        self.m_container.SetBackgroundColour( wx.Colour(60, 100, 127) )
        
        fgSizer20.Add( self.m_container, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer20.Add( ( 0, 12), 1, wx.EXPAND, 5 )
        
        
        self.m_panel22.SetSizer( fgSizer20 )
        self.m_panel22.Layout()
        fgSizer20.Fit( self.m_panel22 )
        fgSizer22.Add( self.m_panel22, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        fgSizer6.Add( fgSizer22, 1, wx.EXPAND, 5 )
        
        
        self.m_panel24.SetSizer( fgSizer6 )
        self.m_panel24.Layout()
        fgSizer6.Fit( self.m_panel24 )
        self.m_panel8 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SIMPLE_BORDER|wx.WANTS_CHARS )
        self.m_panel8.SetFont( wx.Font( 10, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Sans" ) )
        self.m_panel8.SetMinSize( wx.Size( 300,-1 ) )
        
        self.m_mgr.AddPane( self.m_panel8, wx.aui.AuiPaneInfo() .Name( u"wxUI component properties" ).Right() .Caption( u"wxUI component properties" ).CloseButton( False ).PinButton( True ).Dock().Resizable().FloatingSize( wx.Size( 310,324 ) ).Row( 0 ).Position( 3 ).MinSize( wx.Size( 290,300 ) ).Layer( 3 ) )
        
        fgSizer5 = wx.FlexGridSizer( 1, 1, 0, 0 )
        fgSizer5.AddGrowableCol( 0 )
        fgSizer5.AddGrowableRow( 0 )
        fgSizer5.SetFlexibleDirection( wx.BOTH )
        fgSizer5.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_propertyGridManager = pg.PropertyGridManager(
            self.m_panel8, wx.ID_ANY, wx.DefaultPosition,
            wx.DefaultSize, wx.propgrid.PGMAN_DEFAULT_STYLE |
                            wx.propgrid.PG_BOLD_MODIFIED |
                            wx.propgrid.PG_DESCRIPTION |
                            wx.propgrid.PG_TOOLBAR)
        self.m_propertyGridManager.SetFont( wx.Font( 11, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Liberation Sans" ) )
        self.m_propertyGridManager.SetForegroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT ) )
        self.m_propertyGridManager.SetBackgroundColour( wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW ) )
        self.m_propertyGridManager.SetMinSize( wx.Size( 300,-1 ) )
        
        
        self.m_properties = self.m_propertyGridManager.AddPage( u"Properties", wx.Bitmap(u"../res/properties_text.xpm", wx.BITMAP_TYPE_ANY) );
        
        self.m_events = self.m_propertyGridManager.AddPage( u"Events", wx.Bitmap(u"../res/events_text.xpm", wx.BITMAP_TYPE_ANY) );
        fgSizer5.Add( self.m_propertyGridManager, 0, wx.ALL|wx.EXPAND, 5 )
        
        
        self.m_panel8.SetSizer( fgSizer5 )
        self.m_panel8.Layout()
        fgSizer5.Fit( self.m_panel8 )
        
        self.m_mgr.Update()
        
        # Connect Events
        self.m_scrolledWindow4.Bind( wx.EVT_SIZE, self.OnResizeControls )
        self.m_bpButton1.Bind( wx.EVT_BUTTON, self.on_add_frame_window )
        self.m_bpButton2.Bind( wx.EVT_BUTTON, self.on_add_pane_window )
        self.m_bpButton3.Bind( wx.EVT_BUTTON, self.on_add_dialog )
        self.m_bpButton4.Bind( wx.EVT_BUTTON, self.on_add_wizard )
        self.m_bpButton5.Bind( wx.EVT_BUTTON, self.on_add_wizard_page )
        self.m_bpButton5.Bind( wx.EVT_UPDATE_UI, self.on_update_add_wizard_page )
        self.m_bpButton6.Bind( wx.EVT_BUTTON, self.on_add_menu_bar_window )
        self.m_bpButton7.Bind( wx.EVT_BUTTON, self.on_add_toolbar_window )
        self.m_bpButton8.Bind( wx.EVT_BUTTON, self.on_add_status_bar_control )
        self.m_bpButton8.Bind( wx.EVT_UPDATE_UI, self.on_update_add_status_bar_control )
        self.m_bpButton9.Bind( wx.EVT_BUTTON, self.on_add_menu_bar_control )
        self.m_bpButton9.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menu_bar_control )
        self.m_bpButton10.Bind( wx.EVT_BUTTON, self.on_add_menu_control )
        self.m_bpButton10.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menu_control )
        self.m_bpButton11.Bind( wx.EVT_BUTTON, self.on_add_submenu_control )
        self.m_bpButton11.Bind( wx.EVT_UPDATE_UI, self.on_update_add_submenu_control )
        self.m_bpButton12.Bind( wx.EVT_BUTTON, self.on_add_menu_item_control )
        self.m_bpButton12.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menu_item_control )
        self.m_bpButton13.Bind( wx.EVT_BUTTON, self.on_add_menuitem_separator_control )
        self.m_bpButton13.Bind( wx.EVT_UPDATE_UI, self.on_update_add_menuitem_separator_control )
        self.m_bpButton14.Bind( wx.EVT_BUTTON, self.on_add_tool_bar_control )
        self.m_bpButton14.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tool_bar_control )
        self.m_bpButton15.Bind( wx.EVT_BUTTON, self.on_add_auitoolbar )
        self.m_bpButton15.Bind( wx.EVT_UPDATE_UI, self.on_update_add_auitoolbar )
        self.m_bpButton16.Bind( wx.EVT_BUTTON, self.on_add_tool )
        self.m_bpButton16.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tool )
        self.m_bpButton17.Bind( wx.EVT_BUTTON, self.on_add_tool_separator )
        self.m_bpButton17.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tool_separator )
        self.m_bpButton69.Bind( wx.EVT_BUTTON, self.on_add_infobar )
        self.m_bpButton69.Bind( wx.EVT_UPDATE_UI, self.on_update_add_infobar )
        self.m_bpButton70.Bind( wx.EVT_BUTTON, self.on_add_box_sizer )
        self.m_bpButton70.Bind( wx.EVT_UPDATE_UI, self.on_update_add_box_sizer )
        self.m_bpButton71.Bind( wx.EVT_BUTTON, self.on_add_wrap_sizer )
        self.m_bpButton71.Bind( wx.EVT_UPDATE_UI, self.on_update_add_wrap_sizer )
        self.m_bpButton72.Bind( wx.EVT_BUTTON, self.on_add_staticbox_sizer )
        self.m_bpButton72.Bind( wx.EVT_UPDATE_UI, self.on_update_add_staticbox_sizer )
        self.m_bpButton73.Bind( wx.EVT_BUTTON, self.on_add_grid_sizer )
        self.m_bpButton73.Bind( wx.EVT_UPDATE_UI, self.on_update_add_grid_sizer )
        self.m_bpButton74.Bind( wx.EVT_BUTTON, self.on_add_flexgrid_sizer )
        self.m_bpButton74.Bind( wx.EVT_UPDATE_UI, self.on_update_add_flexgrid_sizer )
        self.m_bpButton75.Bind( wx.EVT_BUTTON, self.on_add_gridbag_sizer )
        self.m_bpButton75.Bind( wx.EVT_UPDATE_UI, self.on_update_add_gridbag_sizer )
        self.m_bpButton76.Bind( wx.EVT_BUTTON, self.on_add_dialogbuttons_sizer )
        self.m_bpButton76.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dialogbuttons_sizer )
        self.m_bpButton77.Bind( wx.EVT_BUTTON, self.on_add_spacer )
        self.m_bpButton77.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spacer )
        self.m_bpButton87.Bind( wx.EVT_BUTTON, self.on_add_panel )
        self.m_bpButton87.Bind( wx.EVT_UPDATE_UI, self.on_update_add_panel )
        self.m_bpButton88.Bind( wx.EVT_BUTTON, self.on_add_splitter_window )
        self.m_bpButton88.Bind( wx.EVT_UPDATE_UI, self.on_update_add_splitter_window )
        self.m_bpButton89.Bind( wx.EVT_BUTTON, self.on_add_scrolled_window )
        self.m_bpButton89.Bind( wx.EVT_UPDATE_UI, self.on_update_add_scrolled_window )
        self.m_bpButton90.Bind( wx.EVT_BUTTON, self.on_add_notebook )
        self.m_bpButton90.Bind( wx.EVT_UPDATE_UI, self.on_update_add_notebook )
        self.m_bpButton91.Bind( wx.EVT_BUTTON, self.on_add_collapsible_panel )
        self.m_bpButton91.Bind( wx.EVT_UPDATE_UI, self.on_update_add_collapsible_panel )
        self.m_bpButton92.Bind( wx.EVT_BUTTON, self.on_add_aui_notebook )
        self.m_bpButton92.Bind( wx.EVT_UPDATE_UI, self.on_update_add_aui_notebook )
        self.m_bpButton93.Bind( wx.EVT_BUTTON, self.on_add_listbook )
        self.m_bpButton93.Bind( wx.EVT_UPDATE_UI, self.on_update_add_listbook )
        self.m_bpButton94.Bind( wx.EVT_BUTTON, self.on_add_choicebook )
        self.m_bpButton94.Bind( wx.EVT_UPDATE_UI, self.on_update_add_choicebook )
        self.m_bpButton95.Bind( wx.EVT_BUTTON, self.on_add_simplebook )
        self.m_bpButton95.Bind( wx.EVT_UPDATE_UI, self.on_update_add_simplebook )
        self.m_bpButton96.Bind( wx.EVT_BUTTON, self.on_add_button )
        self.m_bpButton96.Bind( wx.EVT_UPDATE_UI, self.on_update_add_button )
        self.m_bpButton97.Bind( wx.EVT_BUTTON, self.on_add_bitmap_button )
        self.m_bpButton97.Bind( wx.EVT_UPDATE_UI, self.on_update_add_bitmap_button )
        self.m_bpButton98.Bind( wx.EVT_BUTTON, self.on_add_static_text )
        self.m_bpButton98.Bind( wx.EVT_UPDATE_UI, self.on_update_add_static_text )
        self.m_bpButton99.Bind( wx.EVT_BUTTON, self.on_add_edit_text )
        self.m_bpButton99.Bind( wx.EVT_UPDATE_UI, self.on_update_add_edit_text )
        self.m_bpButton100.Bind( wx.EVT_BUTTON, self.on_add_static_bitmap )
        self.m_bpButton100.Bind( wx.EVT_UPDATE_UI, self.on_update_add_static_bitmap )
        self.m_bpButton101.Bind( wx.EVT_BUTTON, self.on_add_animation_control )
        self.m_bpButton101.Bind( wx.EVT_UPDATE_UI, self.on_update_add_animation_control )
        self.m_bpButton102.Bind( wx.EVT_BUTTON, self.on_add_combo_box )
        self.m_bpButton102.Bind( wx.EVT_UPDATE_UI, self.on_update_add_combo_box )
        self.m_bpButton103.Bind( wx.EVT_BUTTON, self.on_add_bitmap_combo_box )
        self.m_bpButton103.Bind( wx.EVT_UPDATE_UI, self.on_update_add_bitmap_combo_box )
        self.m_bpButton104.Bind( wx.EVT_BUTTON, self.on_add_choice_box )
        self.m_bpButton104.Bind( wx.EVT_UPDATE_UI, self.on_update_add_choice_box )
        self.m_bpButton105.Bind( wx.EVT_BUTTON, self.on_add_list_box )
        self.m_bpButton105.Bind( wx.EVT_UPDATE_UI, self.on_update_add_list_box )
        self.m_bpButton106.Bind( wx.EVT_BUTTON, self.on_add_list_control )
        self.m_bpButton106.Bind( wx.EVT_UPDATE_UI, self.on_update_add_list_control )
        self.m_bpButton107.Bind( wx.EVT_BUTTON, self.on_add_check_box )
        self.m_bpButton107.Bind( wx.EVT_UPDATE_UI, self.on_update_add_check_box )
        self.m_bpButton108.Bind( wx.EVT_BUTTON, self.on_add_radio_box )
        self.m_bpButton108.Bind( wx.EVT_UPDATE_UI, self.on_update_add_radio_box )
        self.m_bpButton109.Bind( wx.EVT_BUTTON, self.on_add_radio_button )
        self.m_bpButton109.Bind( wx.EVT_UPDATE_UI, self.on_update_add_radio_button )
        self.m_bpButton110.Bind( wx.EVT_BUTTON, self.on_add_static_line )
        self.m_bpButton110.Bind( wx.EVT_UPDATE_UI, self.on_update_add_static_line )
        self.m_bpButton111.Bind( wx.EVT_BUTTON, self.on_add_slider )
        self.m_bpButton111.Bind( wx.EVT_UPDATE_UI, self.on_update_add_slider )
        self.m_bpButton112.Bind( wx.EVT_BUTTON, self.on_add_gauge )
        self.m_bpButton112.Bind( wx.EVT_UPDATE_UI, self.on_update_add_gauge )
        self.m_bpButton114.Bind( wx.EVT_BUTTON, self.on_add_tree_control )
        self.m_bpButton114.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tree_control )
        self.m_bpButton115.Bind( wx.EVT_BUTTON, self.on_add_grid )
        self.m_bpButton115.Bind( wx.EVT_UPDATE_UI, self.on_update_add_grid )
        self.m_bpButton116.Bind( wx.EVT_BUTTON, self.on_add_check_listbox )
        self.m_bpButton116.Bind( wx.EVT_UPDATE_UI, self.on_update_add_check_listbox )
        self.m_bpButton117.Bind( wx.EVT_BUTTON, self.on_add_tree_listcontrol )
        self.m_bpButton117.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tree_listcontrol )
        self.m_bpButton118.Bind( wx.EVT_BUTTON, self.on_add_tree_listcontrol_column )
        self.m_bpButton118.Bind( wx.EVT_UPDATE_UI, self.on_update_add_tree_listcontrol_column )
        self.m_bpButton119.Bind( wx.EVT_BUTTON, self.on_add_dataview_control )
        self.m_bpButton119.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_control )
        self.m_bpButton120.Bind( wx.EVT_BUTTON, self.on_add_dataview_tree_control )
        self.m_bpButton120.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_tree_control )
        self.m_bpButton121.Bind( wx.EVT_BUTTON, self.on_add_dataview_listcontrol )
        self.m_bpButton121.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_listcontrol )
        self.m_bpButton122.Bind( wx.EVT_BUTTON, self.on_add_dataview_column )
        self.m_bpButton122.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_column )
        self.m_bpButton123.Bind( wx.EVT_BUTTON, self.on_add_dataview_listcolumn )
        self.m_bpButton123.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dataview_listcolumn )
        self.m_bpButton124.Bind( wx.EVT_BUTTON, self.on_add_property_grid )
        self.m_bpButton124.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_grid )
        self.m_bpButton125.Bind( wx.EVT_BUTTON, self.on_add_property_grid_manager )
        self.m_bpButton125.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_grid_manager )
        self.m_bpButton126.Bind( wx.EVT_BUTTON, self.on_add_property_page )
        self.m_bpButton126.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_page )
        self.m_bpButton127.Bind( wx.EVT_BUTTON, self.on_add_property_grid_item )
        self.m_bpButton127.Bind( wx.EVT_UPDATE_UI, self.on_update_add_property_grid_item )
        self.m_bpButton128.Bind( wx.EVT_BUTTON, self.on_add_html_window )
        self.m_bpButton128.Bind( wx.EVT_UPDATE_UI, self.on_update_add_html_window )
        self.m_bpButton129.Bind( wx.EVT_BUTTON, self.on_add_rich_text_control )
        self.m_bpButton129.Bind( wx.EVT_UPDATE_UI, self.on_update_add_rich_text_control )
        self.m_bpButton130.Bind( wx.EVT_BUTTON, self.on_add_styledtext_control )
        self.m_bpButton130.Bind( wx.EVT_UPDATE_UI, self.on_update_add_styledtext_control )
        self.m_bpButton131.Bind( wx.EVT_BUTTON, self.on_add_toggle_button )
        self.m_bpButton131.Bind( wx.EVT_UPDATE_UI, self.on_update_add_toggle_button )
        self.m_bpButton132.Bind( wx.EVT_BUTTON, self.on_add_search_control )
        self.m_bpButton132.Bind( wx.EVT_UPDATE_UI, self.on_update_add_search_control )
        self.m_bpButton133.Bind( wx.EVT_BUTTON, self.on_add_color_picker_control )
        self.m_bpButton133.Bind( wx.EVT_UPDATE_UI, self.on_update_add_color_picker_control )
        self.m_bpButton134.Bind( wx.EVT_BUTTON, self.on_add_font_picker_control )
        self.m_bpButton134.Bind( wx.EVT_UPDATE_UI, self.on_update_add_font_picker_control )
        self.m_bpButton135.Bind( wx.EVT_BUTTON, self.on_add_file_picker_control )
        self.m_bpButton135.Bind( wx.EVT_UPDATE_UI, self.on_update_add_file_picker_control )
        self.m_bpButton136.Bind( wx.EVT_BUTTON, self.on_add_dir_picker_control )
        self.m_bpButton136.Bind( wx.EVT_UPDATE_UI, self.on_update_add_dir_picker_control )
        self.m_bpButton137.Bind( wx.EVT_BUTTON, self.on_add_date_picker_control )
        self.m_bpButton137.Bind( wx.EVT_UPDATE_UI, self.on_update_add_date_picker_control )
        self.m_bpButton138.Bind( wx.EVT_BUTTON, self.on_add_calendar_control )
        self.m_bpButton138.Bind( wx.EVT_UPDATE_UI, self.on_update_add_calendar_control )
        self.m_bpButton139.Bind( wx.EVT_BUTTON, self.on_add_scrollbar_control )
        self.m_bpButton139.Bind( wx.EVT_UPDATE_UI, self.on_update_add_scrollbar_control )
        self.m_bpButton140.Bind( wx.EVT_BUTTON, self.on_add_spin_control )
        self.m_bpButton140.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spin_control )
        self.m_bpButton141.Bind( wx.EVT_BUTTON, self.on_add_spin_double_control )
        self.m_bpButton141.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spin_double_control )
        self.m_bpButton142.Bind( wx.EVT_BUTTON, self.on_add_spin_button )
        self.m_bpButton142.Bind( wx.EVT_UPDATE_UI, self.on_update_add_spin_button )
        self.m_bpButton143.Bind( wx.EVT_BUTTON, self.on_add_hyperlink_control )
        self.m_bpButton143.Bind( wx.EVT_UPDATE_UI, self.on_update_add_hyperlink_control )
        self.m_bpButton144.Bind( wx.EVT_BUTTON, self.on_add_generic_dir_control )
        self.m_bpButton144.Bind( wx.EVT_UPDATE_UI, self.on_update_add_generic_dir_control )
        self.m_bpButton145.Bind( wx.EVT_BUTTON, self.on_add_custom_control )
        self.m_bpButton145.Bind( wx.EVT_UPDATE_UI, self.on_update_add_custom_control )
        self.m_bpButton146.Bind( wx.EVT_BUTTON, self.on_add_timer_control )
        self.m_bpButton146.Bind( wx.EVT_UPDATE_UI, self.on_update_add_timer_control )
        self.m_treeCtrl.Bind( wx.EVT_KEY_DOWN, self.on_tree_key_down )
        self.m_treeCtrl.Bind( wx.EVT_TREE_SEL_CHANGED, self.on_model_item_selected )
        self.Bind( wx.EVT_TOOL, self.on_align_left, id = self.m_align_left.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_left, id = self.m_align_left.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_center, id = self.m_align_center.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_center, id = self.m_align_center.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_right, id = self.m_align_right.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_right, id = self.m_align_right.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_top, id = self.m_align_top.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_top, id = self.m_align_top.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_middle, id = self.m_align_middle.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_middle, id = self.m_align_middle.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_align_bottom, id = self.m_align_bottom.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_align_bottom, id = self.m_align_bottom.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_expand, id = self.m_expand.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_expand, id = self.m_expand.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_stretch, id = self.m_stretch.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_stretch, id = self.m_stretch.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_left_border, id = self.m_left_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_left_border, id = self.m_left_border.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_right_border, id = self.m_right_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_right_border, id = self.m_right_border.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_top_border, id = self.m_top_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_top_border, id = self.m_top_border.GetId() )
        self.Bind( wx.EVT_TOOL, self.on_bottom_border, id = self.m_bottom_border.GetId() )
        self.Bind( wx.EVT_UPDATE_UI, self.on_update_bottom_border, id = self.m_bottom_border.GetId() )
        self.m_container.Bind( wx.EVT_LEFT_DOWN, self.on_design_mouse_left_click )
        self.m_container.Bind( wx.EVT_MOTION, self.on_design_mouse_move )
        self.m_container.Bind( wx.EVT_PAINT, self.on_design_paint )
        self.m_propertyGridManager.Bind( pg.EVT_PG_DOUBLE_CLICK, self.on_double_click_property )
        self.m_propertyGridManager.Bind( pg.EVT_PG_CHANGED, self.on_property_changed )
        self.m_propertyGridManager.Bind( pg.EVT_PG_CHANGING, self.on_property_changing )
    
    def __del__( self ):
        self.m_mgr.UnInit()
        
    
    
    # Virtual event handlers, overide them in your derived class
    def OnResizeControls( self, event ):
        event.Skip()
    
    def on_add_frame_window( self, event ):
        event.Skip()
    
    def on_add_pane_window( self, event ):
        event.Skip()
    
    def on_add_dialog( self, event ):
        event.Skip()
    
    def on_add_wizard( self, event ):
        event.Skip()
    
    def on_add_wizard_page( self, event ):
        event.Skip()
    
    def on_update_add_wizard_page( self, event ):
        event.Skip()
    
    def on_add_menu_bar_window( self, event ):
        event.Skip()
    
    def on_add_toolbar_window( self, event ):
        event.Skip()
    
    def on_add_status_bar_control( self, event ):
        event.Skip()
    
    def on_update_add_status_bar_control( self, event ):
        event.Skip()
    
    def on_add_menu_bar_control( self, event ):
        event.Skip()
    
    def on_update_add_menu_bar_control( self, event ):
        event.Skip()
    
    def on_add_menu_control( self, event ):
        event.Skip()
    
    def on_update_add_menu_control( self, event ):
        event.Skip()
    
    def on_add_submenu_control( self, event ):
        event.Skip()
    
    def on_update_add_submenu_control( self, event ):
        event.Skip()
    
    def on_add_menu_item_control( self, event ):
        event.Skip()
    
    def on_update_add_menu_item_control( self, event ):
        event.Skip()
    
    def on_add_menuitem_separator_control( self, event ):
        event.Skip()
    
    def on_update_add_menuitem_separator_control( self, event ):
        event.Skip()
    
    def on_add_tool_bar_control( self, event ):
        event.Skip()
    
    def on_update_add_tool_bar_control( self, event ):
        event.Skip()
    
    def on_add_auitoolbar( self, event ):
        event.Skip()
    
    def on_update_add_auitoolbar( self, event ):
        event.Skip()
    
    def on_add_tool( self, event ):
        event.Skip()
    
    def on_update_add_tool( self, event ):
        event.Skip()
    
    def on_add_tool_separator( self, event ):
        event.Skip()
    
    def on_update_add_tool_separator( self, event ):
        event.Skip()
    
    def on_add_infobar( self, event ):
        event.Skip()
    
    def on_update_add_infobar( self, event ):
        event.Skip()
    
    def on_add_box_sizer( self, event ):
        event.Skip()
    
    def on_update_add_box_sizer( self, event ):
        event.Skip()
    
    def on_add_wrap_sizer( self, event ):
        event.Skip()
    
    def on_update_add_wrap_sizer( self, event ):
        event.Skip()
    
    def on_add_staticbox_sizer( self, event ):
        event.Skip()
    
    def on_update_add_staticbox_sizer( self, event ):
        event.Skip()
    
    def on_add_grid_sizer( self, event ):
        event.Skip()
    
    def on_update_add_grid_sizer( self, event ):
        event.Skip()
    
    def on_add_flexgrid_sizer( self, event ):
        event.Skip()
    
    def on_update_add_flexgrid_sizer( self, event ):
        event.Skip()
    
    def on_add_gridbag_sizer( self, event ):
        event.Skip()
    
    def on_update_add_gridbag_sizer( self, event ):
        event.Skip()
    
    def on_add_dialogbuttons_sizer( self, event ):
        event.Skip()
    
    def on_update_add_dialogbuttons_sizer( self, event ):
        event.Skip()
    
    def on_add_spacer( self, event ):
        event.Skip()
    
    def on_update_add_spacer( self, event ):
        event.Skip()
    
    def on_add_panel( self, event ):
        event.Skip()
    
    def on_update_add_panel( self, event ):
        event.Skip()
    
    def on_add_splitter_window( self, event ):
        event.Skip()
    
    def on_update_add_splitter_window( self, event ):
        event.Skip()
    
    def on_add_scrolled_window( self, event ):
        event.Skip()
    
    def on_update_add_scrolled_window( self, event ):
        event.Skip()
    
    def on_add_notebook( self, event ):
        event.Skip()
    
    def on_update_add_notebook( self, event ):
        event.Skip()
    
    def on_add_collapsible_panel( self, event ):
        event.Skip()
    
    def on_update_add_collapsible_panel( self, event ):
        event.Skip()
    
    def on_add_aui_notebook( self, event ):
        event.Skip()
    
    def on_update_add_aui_notebook( self, event ):
        event.Skip()
    
    def on_add_listbook( self, event ):
        event.Skip()
    
    def on_update_add_listbook( self, event ):
        event.Skip()
    
    def on_add_choicebook( self, event ):
        event.Skip()
    
    def on_update_add_choicebook( self, event ):
        event.Skip()
    
    def on_add_simplebook( self, event ):
        event.Skip()
    
    def on_update_add_simplebook( self, event ):
        event.Skip()
    
    def on_add_button( self, event ):
        event.Skip()
    
    def on_update_add_button( self, event ):
        event.Skip()
    
    def on_add_bitmap_button( self, event ):
        event.Skip()
    
    def on_update_add_bitmap_button( self, event ):
        event.Skip()
    
    def on_add_static_text( self, event ):
        event.Skip()
    
    def on_update_add_static_text( self, event ):
        event.Skip()
    
    def on_add_edit_text( self, event ):
        event.Skip()
    
    def on_update_add_edit_text( self, event ):
        event.Skip()
    
    def on_add_static_bitmap( self, event ):
        event.Skip()
    
    def on_update_add_static_bitmap( self, event ):
        event.Skip()
    
    def on_add_animation_control( self, event ):
        event.Skip()
    
    def on_update_add_animation_control( self, event ):
        event.Skip()
    
    def on_add_combo_box( self, event ):
        event.Skip()
    
    def on_update_add_combo_box( self, event ):
        event.Skip()
    
    def on_add_bitmap_combo_box( self, event ):
        event.Skip()
    
    def on_update_add_bitmap_combo_box( self, event ):
        event.Skip()
    
    def on_add_choice_box( self, event ):
        event.Skip()
    
    def on_update_add_choice_box( self, event ):
        event.Skip()
    
    def on_add_list_box( self, event ):
        event.Skip()
    
    def on_update_add_list_box( self, event ):
        event.Skip()
    
    def on_add_list_control( self, event ):
        event.Skip()
    
    def on_update_add_list_control( self, event ):
        event.Skip()
    
    def on_add_check_box( self, event ):
        event.Skip()
    
    def on_update_add_check_box( self, event ):
        event.Skip()
    
    def on_add_radio_box( self, event ):
        event.Skip()
    
    def on_update_add_radio_box( self, event ):
        event.Skip()
    
    def on_add_radio_button( self, event ):
        event.Skip()
    
    def on_update_add_radio_button( self, event ):
        event.Skip()
    
    def on_add_static_line( self, event ):
        event.Skip()
    
    def on_update_add_static_line( self, event ):
        event.Skip()
    
    def on_add_slider( self, event ):
        event.Skip()
    
    def on_update_add_slider( self, event ):
        event.Skip()
    
    def on_add_gauge( self, event ):
        event.Skip()
    
    def on_update_add_gauge( self, event ):
        event.Skip()
    
    def on_add_tree_control( self, event ):
        event.Skip()
    
    def on_update_add_tree_control( self, event ):
        event.Skip()
    
    def on_add_grid( self, event ):
        event.Skip()
    
    def on_update_add_grid( self, event ):
        event.Skip()
    
    def on_add_check_listbox( self, event ):
        event.Skip()
    
    def on_update_add_check_listbox( self, event ):
        event.Skip()
    
    def on_add_tree_listcontrol( self, event ):
        event.Skip()
    
    def on_update_add_tree_listcontrol( self, event ):
        event.Skip()
    
    def on_add_tree_listcontrol_column( self, event ):
        event.Skip()
    
    def on_update_add_tree_listcontrol_column( self, event ):
        event.Skip()
    
    def on_add_dataview_control( self, event ):
        event.Skip()
    
    def on_update_add_dataview_control( self, event ):
        event.Skip()
    
    def on_add_dataview_tree_control( self, event ):
        event.Skip()
    
    def on_update_add_dataview_tree_control( self, event ):
        event.Skip()
    
    def on_add_dataview_listcontrol( self, event ):
        event.Skip()
    
    def on_update_add_dataview_listcontrol( self, event ):
        event.Skip()
    
    def on_add_dataview_column( self, event ):
        event.Skip()
    
    def on_update_add_dataview_column( self, event ):
        event.Skip()
    
    def on_add_dataview_listcolumn( self, event ):
        event.Skip()
    
    def on_update_add_dataview_listcolumn( self, event ):
        event.Skip()
    
    def on_add_property_grid( self, event ):
        event.Skip()
    
    def on_update_add_property_grid( self, event ):
        event.Skip()
    
    def on_add_property_grid_manager( self, event ):
        event.Skip()
    
    def on_update_add_property_grid_manager( self, event ):
        event.Skip()
    
    def on_add_property_page( self, event ):
        event.Skip()
    
    def on_update_add_property_page( self, event ):
        event.Skip()
    
    def on_add_property_grid_item( self, event ):
        event.Skip()
    
    def on_update_add_property_grid_item( self, event ):
        event.Skip()
    
    def on_add_html_window( self, event ):
        event.Skip()
    
    def on_update_add_html_window( self, event ):
        event.Skip()
    
    def on_add_rich_text_control( self, event ):
        event.Skip()
    
    def on_update_add_rich_text_control( self, event ):
        event.Skip()
    
    def on_add_styledtext_control( self, event ):
        event.Skip()
    
    def on_update_add_styledtext_control( self, event ):
        event.Skip()
    
    def on_add_toggle_button( self, event ):
        event.Skip()
    
    def on_update_add_toggle_button( self, event ):
        event.Skip()
    
    def on_add_search_control( self, event ):
        event.Skip()
    
    def on_update_add_search_control( self, event ):
        event.Skip()
    
    def on_add_color_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_color_picker_control( self, event ):
        event.Skip()
    
    def on_add_font_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_font_picker_control( self, event ):
        event.Skip()
    
    def on_add_file_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_file_picker_control( self, event ):
        event.Skip()
    
    def on_add_dir_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_dir_picker_control( self, event ):
        event.Skip()
    
    def on_add_date_picker_control( self, event ):
        event.Skip()
    
    def on_update_add_date_picker_control( self, event ):
        event.Skip()
    
    def on_add_calendar_control( self, event ):
        event.Skip()
    
    def on_update_add_calendar_control( self, event ):
        event.Skip()
    
    def on_add_scrollbar_control( self, event ):
        event.Skip()
    
    def on_update_add_scrollbar_control( self, event ):
        event.Skip()
    
    def on_add_spin_control( self, event ):
        event.Skip()
    
    def on_update_add_spin_control( self, event ):
        event.Skip()
    
    def on_update_add_spin_double_control( self, event ):
        event.Skip()
    
    
    def on_add_spin_button( self, event ):
        event.Skip()
    
    
    def on_add_hyperlink_control( self, event ):
        event.Skip()
    
    def on_update_add_hyperlink_control( self, event ):
        event.Skip()
    
    def on_add_generic_dir_control( self, event ):
        event.Skip()
    
    def on_update_add_generic_dir_control( self, event ):
        event.Skip()
    
    def on_add_custom_control( self, event ):
        event.Skip()
    
    def on_update_add_custom_control( self, event ):
        event.Skip()
    
    def on_add_timer_control( self, event ):
        event.Skip()
    
    def on_update_add_timer_control( self, event ):
        event.Skip()
    
    def on_tree_key_down( self, event ):
        event.Skip()
    
    def on_model_item_selected( self, event ):
        event.Skip()
    
    def on_align_left( self, event ):
        event.Skip()
    
    def on_update_align_left( self, event ):
        event.Skip()
    
    def on_align_center( self, event ):
        event.Skip()
    
    def on_update_align_center( self, event ):
        event.Skip()
    
    def on_align_right( self, event ):
        event.Skip()
    
    def on_update_align_right( self, event ):
        event.Skip()
    
    def on_align_top( self, event ):
        event.Skip()
    
    def on_update_align_top( self, event ):
        event.Skip()
    
    def on_align_middle( self, event ):
        event.Skip()
    
    def on_update_align_middle( self, event ):
        event.Skip()
    
    def on_align_bottom( self, event ):
        event.Skip()
    
    def on_update_align_bottom( self, event ):
        event.Skip()
    
    def on_expand( self, event ):
        event.Skip()
    
    def on_update_expand( self, event ):
        event.Skip()
    
    def on_stretch( self, event ):
        event.Skip()
    
    def on_update_stretch( self, event ):
        event.Skip()
    
    def on_left_border( self, event ):
        event.Skip()
    
    def on_update_left_border( self, event ):
        event.Skip()
    
    def on_right_border( self, event ):
        event.Skip()
    
    def on_update_right_border( self, event ):
        event.Skip()
    
    def on_top_border( self, event ):
        event.Skip()
    
    def on_update_top_border( self, event ):
        event.Skip()
    
    def on_bottom_border( self, event ):
        event.Skip()
    
    def on_update_bottom_border( self, event ):
        event.Skip()
    
    def on_design_mouse_left_click( self, event ):
        event.Skip()
    
    def on_design_mouse_move( self, event ):
        event.Skip()
    
    def on_design_paint( self, event ):
        event.Skip()
    
    def on_double_click_property( self, event ):
        event.Skip()
    
    def on_property_changed( self, event ):
        event.Skip()
    
    def on_property_changing( self, event ):
        event.Skip()
    

