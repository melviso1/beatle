import wx
from beatle.lib import wxx
from beatle.lib.wxx.agw._FlatMenu import FMRendererMgr
from ...lib import get_colour

NOWHERE= 0
MENUITEM = 1
TOOLBARITEM = 2
DROPDOWNARROWBUTTON = 3

# noinspection PyPep8Naming
class uiFakeMenuBar(wxx.MenuBar):
    """This class represents a fake menu bar,
    used to interact with the user during UI design"""

    def OnLeftDown(self, event):
        """
        Filter the message for handling model focus
        :param `event`: a :class:`MouseEvent` event to be processed.
        """
        if self._object is not None:
            pt = event.GetPosition()
            idx, where = self.HitTest(pt)
            if where == MENUITEM:
                pass  # We must let the menu to activate self.ActivateMenu(self._items[idx])
                self._items[idx].GetMenu().TrackModel()
            elif where == TOOLBARITEM:
                # we are not using this for the moment
                pass
            elif where in [DROPDOWNARROWBUTTON, NOWHERE]:
                view = self._object.view
                if view is not None:
                    view.tree_select(self._object)
        return super(uiFakeMenuBar, self).OnLeftDown(event)

    def __init__(self, parent, model):
        # we need a private renderer here. If not, all beatle could see the changes
        self._object = model
        prev_instance = getattr(FMRendererMgr, '_instance', None)
        if prev_instance is not None:
            delattr(FMRendererMgr, '_instance')
        super(uiFakeMenuBar, self).__init__(parent)
        if prev_instance:
            setattr(FMRendererMgr, '_instance', prev_instance)
        else:
            delattr(FMRendererMgr, '_instance')

    def set_kwargs(self, **kwargs):
        """The mission here is to update visual aspect"""
        colour = get_colour('foreground_colour', kwargs)
        if colour:
            self.SetForegroundColour(colour)
        colour = get_colour('background_colour', kwargs)
        if colour:
            self.SetBackgroundColour(colour)


