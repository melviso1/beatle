import wx
from beatle.lib import wxx

MENU_HT_ITEM=1

# noinspection PyPep8Naming
class uiFakeMenu(wxx.Menu):
    """This class represents a fake menu bar,
    used to interact with the user during UI design"""

    def TrackModel(self):
        """Called when the user clicks the object in order to edit properties"""
        if self._object is not None:
            view = self._object.view
            if view is not None:
                view.tree_select(self._object)

    def ProcessMouseLClickEnd(self, pos):
        """
        Processes mouse left clicks.

        :param `pos`: the position at which the mouse left button was pressed,
         an instance of :class:`wx.Point`.
        """

        # test if we are on a menu item
        res, itemIdx = self.HitTest(pos)

        if res == MENU_HT_ITEM:
            item = self._itemsArr[itemIdx]
            if item.IsSubMenu():
                item.GetSubMenu().TrackModel()
            else:
                item.TrackModel()
        super(uiFakeMenu, self).ProcessMouseLClickEnd(pos)

    def __init__(self, parent, model):
        assert(parent is not None)
        self._object = model
        self._submenu_mmenuitem = None
        self._private_wxui_parent = parent
        super(uiFakeMenu, self).__init__()

    @property
    def submenu_menuitem(self):
        return self._submenu_mmenuitem

    @submenu_menuitem.setter
    def submenu_menuitem(self, value):
        self._submenu_mmenuitem = value


    def AppendSubMenu(self, subMenu, item, helpString=""):
        """When adding a submenu, we can't change the label
        because the identifier required belongs to a dynamically
        created menu item. Fortunately, this item is returned,
        so we need to capture his value"""
        subMenu.submenu_menuitem = super(uiFakeMenu, self).AppendSubMenu(subMenu, item, helpString)


    @property
    def label_id(self):
        if self._submenu_mmenuitem is not None:
            return self._submenu_mmenuitem.GetId()
        return self.GetId()

    def set_kwargs(self, **kwargs):
        """The mission here is to update visual aspect"""
        label = kwargs.get('label', None)
        if label is not None:
            self._private_wxui_parent.SetLabel(self.label_id, label)

    def Destroy(self):
        """This call intecepts the window destroy for removing
        the menu from parent"""
        if self._parentMenu is None and self._private_wxui_parent is not None:
            try:
                self._private_wxui_parent.RemoveMenu(self)
            except Exception as e:
                wx.LogError('Failed to remove menu from parent {info}'.format(info=str(type(self._parentMenu))))
        super(uiFakeMenu, self).Destroy()

