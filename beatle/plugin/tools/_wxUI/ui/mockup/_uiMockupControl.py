import wx
from ...lib import get_colour


# noinspection PyPep8Naming
def uiMockupControl(control_class):

    """
    This is a generic mockup builder for simple classes
    in order to avoid repeating the same code
    """

    class Mockup(control_class):
        """This class handles a generic control."""

        def TryBefore(self, event):
            try:
                # grid updates can generate events for already deleted objects!
                if self._object is not None:
                    # check if this is a wx.EVT_LEFT_DOWN event
                    if type(event) is wx.MouseEvent:
                        if event.EventType == wx.EVT_LEFT_DOWN.typeId:
                            view = self._object.view
                            if view is not None:
                                view.tree_select(self._object)
                return super(Mockup, self).TryBefore(event)
            except:
                return True

        def __init__(self, parent, *args, **kwargs):
            self._parent = parent
            self._object = None
            super(Mockup, self).__init__(parent, *args)
            self.set_kwargs(**kwargs)

        def bind(self, obj):
            self._object = obj
            self.set_kwargs(**obj.kwargs)

        def set_kwargs(self, **kwargs):
            """This class doesn't have really other arguments
            that those stored in graphic state, so this method
            is also a mock-up whose function is to update the
            graphic properties."""

            self.SetLabel(kwargs.get('label', ''))

            tooltip = kwargs.get('tooltip', '')
            if len(tooltip) > 0:
                self.SetToolTip(tooltip)

            min_size = kwargs.get('minimum_size', (-1, -1))
            if min_size != (-1, -1):
                self.SetMinSize(wx.Size(min_size))

            max_size = kwargs.get('maxmimum_size', (-1, -1))
            if max_size != (-1, -1):
                self.SetMaxSize(wx.Size(max_size))

            self.SetSize(wx.Size(kwargs.get('size', (-1, -1))))
            colour = get_colour('foreground_colour', kwargs)
            if colour:
                self.SetForegroundColour(colour)
            colour = get_colour('background_colour', kwargs)
            if colour:
                self.SetBackgroundColour(colour)
            self.Enable(kwargs.get('enabled', True))
            self.Show(not kwargs.get('hidden', False))

    return Mockup