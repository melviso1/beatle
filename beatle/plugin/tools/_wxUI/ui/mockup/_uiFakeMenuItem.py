import wx
from beatle.lib import wxx


# noinspection PyPep8Naming
class uiFakeMenuItem(wxx.MenuItem):

    def TrackModel(self):
        """Called when the user clicks the object in order to edit properties"""
        if self._object is not None:
            view = self._object.view
            if view is not None:
                view.tree_select(self._object)

    def __init__(self, parent, label='', id=wx.ID_ANY, kind=wx.ITEM_NORMAL):
        self._object = None
        super(uiFakeMenuItem, self).__init__(parent, id=id, label=label, kind=kind)

    def bind(self, obj):
        self._object = obj
        self.set_kwargs(**obj.kwargs)

    def set_kwargs(self, **kwargs):
        """Update visual aspect"""
        label = kwargs.get('label', '')
        shortcut = kwargs.get('shortcut', '')
        if len(shortcut) > 0:
            label += "\t" + shortcut
        self.SetLabel(label)
        self.Enable(kwargs.get('enabled', True))
        self.Check(kwargs.get('checked', False))
        # translate map for possible kind string
        kind_map = {
            'wx.ITEM_NORMAL': wx.ITEM_NORMAL,
            'wxITEM_NORMAL': wx.ITEM_NORMAL,
            'wx.ITEM_CHECK': wx.ITEM_CHECK,
            'wxITEM_CHECK': wx.ITEM_CHECK,
            'wx.ITEM_RADIO': wx.ITEM_RADIO,
            'wxITEM_RADIO': wx.ITEM_RADIO
        }
        self.SetKind(kind_map[kwargs.get('kind','wxITEM_NORMAL')])
        self._parentMenu.UpdateRadioGroup(self)
