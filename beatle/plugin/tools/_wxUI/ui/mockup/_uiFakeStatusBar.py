import os
import wx
from .. import ui
from ...lib import get_colour


# noinspection PyPep8Naming
class uiFakeStatusBar(ui.FakeStatusBarBase):
    """This is a fake status bar"""

    def __init__(self, parent):
        self._object = None
        status_bar_height = 27
        cwd = os.getcwd()
        os.chdir(os.path.split(os.path.abspath(__file__))[0])
        super(uiFakeStatusBar, self).__init__(parent)
        os.chdir(cwd)
        self.SetMinSize(wx.Size(-1, status_bar_height))
        self.SetBackgroundColour(wx.Colour(227, 234, 243))

    def bind(self, obj):
        self._object = obj
        self.set_kwargs(**obj.kwargs)
        self.Bind(wx.EVT_LEFT_DOWN, self.on_click)

    def on_click(self, event):
        if self._object is not None:
            view = self._object.view
            if view is not None:
                view.tree_select(self._object)
        event.Skip()

    def set_kwargs(self, **kwargs):
        """The mission here is to update visual aspect"""
        bar_style = kwargs.get('status_bar_style', None)
        if bar_style is not None and bar_style&1:
            self.m_grip.Show()
        else:
            self.m_grip.Hide()
        self.Layout()
        colour = get_colour('foreground_colour', kwargs)
        if colour:
            self.SetForegroundColour(colour)
        colour = get_colour('background_colour', kwargs)
        if colour:
            self.SetBackgroundColour(colour)
