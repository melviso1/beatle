"""
tiny panel class for emulating a toolbar. We start with 42 height pixel pane
"""
import wx

# noinspection PyPep8Naming
class uiFakeToolbar(wx.ToolBar):
    """This is a fake status bar"""

    def __init__(self, parent, **kwargs):
        gross = 10
        style = kwargs.get('tool_bar_style', 4)
        if style & 4 or not style & 8:
            style = wx.Size(-1, gross)
        else:
            size = wx.Size(gross, -1)
        status_bar_height = 10  # near empty
        super(uiFakeToolbar, self).__init__(parent, id=wx.ID_ANY, size=wx.DefaultSize)
        self.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_GRAYTEXT))

    def set_kwargs(self, **kwargs):
        """This must change attributes of the status bar in the edition window."""
        pass
