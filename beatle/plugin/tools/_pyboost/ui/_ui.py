# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Aug 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################
from beatle.lib import wxx
from beatle.lib.wxx import Simplebook
import wx
import wx.xrc
from beatle.lib.wxx.agw import _CustomTreeCtrl as CT
from wx import aui

# special import for beatle development
from beatle.lib.handlers import Identifiers
###########################################################################
## Class BoostPythonModuleDialogBase
###########################################################################

class BoostPythonModuleDialogBase ( wxx.Dialog ):
	
	def __init__(self, parent):
		wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"New boost.python module", pos = wx.DefaultPosition, size = wx.Size( 822,568 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )
		
		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		
		fgSizer53 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer53.AddGrowableCol( 0 )
		fgSizer53.AddGrowableRow( 1 )
		fgSizer53.SetFlexibleDirection( wx.BOTH )
		fgSizer53.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer54 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer54.AddGrowableCol( 1 )
		fgSizer54.AddGrowableRow( 0 )
		fgSizer54.SetFlexibleDirection( wx.BOTH )
		fgSizer54.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText26 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText26.Wrap( -1 )
		fgSizer54.Add(self.m_staticText26, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl21 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer54.Add(self.m_textCtrl21, 0, wx.EXPAND|wx.ALL, 5 )
		
		
		fgSizer53.Add( fgSizer54, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )
		
		self.m_choicebook2 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel11 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		fgSizer17 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer17.AddGrowableCol( 0 )
		fgSizer17.AddGrowableRow( 0 )
		fgSizer17.SetFlexibleDirection( wx.BOTH )
		fgSizer17.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_splitter1 = wx.SplitterWindow(self.m_panel11, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
		self.m_splitter1.SetSashGravity( 0.5 )
		self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )
		self.m_splitter1.SetMinimumPaneSize( 20 )
		
		self.m_panel1 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer6 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer6.AddGrowableCol( 0 )
		fgSizer6.AddGrowableRow( 0 )
		fgSizer6.SetFlexibleDirection( wx.BOTH )
		fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer6 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel1, wx.ID_ANY, u"exposed elements" ), wx.VERTICAL )
		
		style = (CT.TR_DEFAULT_STYLE|CT.TR_MULTIPLE  | CT.TR_AUTO_CHECK_CHILD  | CT.TR_AUTO_CHECK_PARENT | CT.TR_AUTO_TOGGLE_CHILD)
		self.m_tree = CT.CustomTreeCtrl(self.m_panel1, style=style)
		sbSizer6.Add(self.m_tree, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer6.Add( sbSizer6, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_panel1.SetSizer( fgSizer6 )
		self.m_panel1.Layout()
		fgSizer6.Fit(self.m_panel1 )
		self.m_panel3 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer4 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer4.AddGrowableCol( 0 )
		fgSizer4.AddGrowableRow( 0 )
		fgSizer4.SetFlexibleDirection( wx.BOTH )
		fgSizer4.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel3, wx.ID_ANY, u"attributes" ), wx.VERTICAL )
		
		self.m_book = Simplebook( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_defaultPane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_book.AddPage(self.m_defaultPane, u"a page", False )
		self.m_classPane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer5 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer5.AddGrowableCol( 0 )
		fgSizer5.AddGrowableRow( 1 )
		fgSizer5.SetFlexibleDirection( wx.BOTH )
		fgSizer5.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_class_non_copyable = wx.CheckBox(self.m_classPane, wx.ID_ANY, u"Make non &copyable", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_class_non_copyable.SetHelpText( u"If this class has deleted or private constructor or if we don't want to expose default creator, check this box." )
		
		fgSizer5.Add(self.m_class_non_copyable, 0, wx.ALL, 5 )
		
		sbSizer61 = wx.StaticBoxSizer( wx.StaticBox(self.m_classPane, wx.ID_ANY, u"base classes" ), wx.VERTICAL )
		
		m_checkList1Choices = []
		self.m_checkList1 = wx.CheckListBox( sbSizer61.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_checkList1Choices, 0 )
		self.m_checkList1.SetToolTip( u"Select inheritance base classes \nto be exposed in python." )
		
		sbSizer61.Add(self.m_checkList1, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer5.Add( sbSizer61, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_classPane.SetSizer( fgSizer5 )
		self.m_classPane.Layout()
		fgSizer5.Fit(self.m_classPane )
		self.m_book.AddPage(self.m_classPane, u"a page", False )
		self.m_memberMethodPane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer8 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer8.AddGrowableCol( 0 )
		fgSizer8.AddGrowableRow( 0 )
		fgSizer8.SetFlexibleDirection( wx.BOTH )
		fgSizer8.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_auinotebook3 = aui.AuiNotebook(self.m_memberMethodPane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, aui.AUI_NB_BOTTOM )
		self.m_panel6 = wx.Panel(self.m_auinotebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer9 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer9.AddGrowableCol( 0 )
		fgSizer9.AddGrowableRow( 0 )
		fgSizer9.SetFlexibleDirection( wx.BOTH )
		fgSizer9.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		m_listCallPoliciesChoices = []
		self.m_listCallPolicies = wx.ListBox(self.m_panel6, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listCallPoliciesChoices, 0 )
		self.m_listCallPolicies.SetToolTip( u"Double click for item editing." )
		
		fgSizer9.Add(self.m_listCallPolicies, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer10 = wx.FlexGridSizer( 5, 1, 0, 0 )
		fgSizer10.AddGrowableCol( 0 )
		fgSizer10.AddGrowableRow( 4 )
		fgSizer10.SetFlexibleDirection( wx.BOTH )
		fgSizer10.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_bpButton3 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ADD_BOOKMARK, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		fgSizer10.Add(self.m_bpButton3, 0, wx.ALL, 5 )
		
		self.m_bpButton4 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton4.Enable( False )
		
		fgSizer10.Add(self.m_bpButton4, 0, wx.ALL, 5 )
		
		self.m_bpButton5 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_UP, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton5.Enable( False )
		
		fgSizer10.Add(self.m_bpButton5, 0, wx.ALL, 5 )
		
		self.m_bpButton6 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_DOWN, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton6.Enable( False )
		
		fgSizer10.Add(self.m_bpButton6, 0, wx.ALL, 5 )
		
		
		fgSizer10.Add( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		fgSizer9.Add( fgSizer10, 1, wx.EXPAND, 5 )
		
		
		self.m_panel6.SetSizer( fgSizer9 )
		self.m_panel6.Layout()
		fgSizer9.Fit(self.m_panel6 )
		self.m_auinotebook3.AddPage(self.m_panel6, u"call policies", False )
		
		fgSizer8.Add(self.m_auinotebook3, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		self.m_memberMethodPane.SetSizer( fgSizer8 )
		self.m_memberMethodPane.Layout()
		fgSizer8.Fit(self.m_memberMethodPane )
		self.m_book.AddPage(self.m_memberMethodPane, u"a page", False )
		
		sbSizer2.Add(self.m_book, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		fgSizer4.Add( sbSizer2, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_panel3.SetSizer( fgSizer4 )
		self.m_panel3.Layout()
		fgSizer4.Fit(self.m_panel3 )
		self.m_splitter1.SplitVertically(self.m_panel1, self.m_panel3, 0 )
		fgSizer17.Add(self.m_splitter1, 1, wx.EXPAND, 5 )
		
		
		self.m_panel11.SetSizer( fgSizer17 )
		self.m_panel11.Layout()
		fgSizer17.Fit(self.m_panel11 )
		self.m_choicebook2.AddPage(self.m_panel11, u"exports", True )
		self.m_panel12 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer18 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer18.AddGrowableCol( 0 )
		fgSizer18.AddGrowableRow( 0 )
		fgSizer18.SetFlexibleDirection( wx.BOTH )
		fgSizer18.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		m_listBox6Choices = []
		self.m_listBox6 = wx.ListBox(self.m_panel12, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox6Choices, 0 )
		self.m_listBox6.SetToolTip( u"List current conversions.\nDouble-click for editing." )
		
		fgSizer18.Add(self.m_listBox6, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer19 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer19.AddGrowableRow( 2 )
		fgSizer19.SetFlexibleDirection( wx.BOTH )
		fgSizer19.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_bpButton7 = wx.BitmapButton(self.m_panel12, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ADD_BOOKMARK, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		fgSizer19.Add(self.m_bpButton7, 0, wx.ALL, 5 )
		
		self.m_bpButton8 = wx.BitmapButton(self.m_panel12, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton8.Enable( False )
		
		fgSizer19.Add(self.m_bpButton8, 0, wx.ALL, 5 )
		
		
		fgSizer18.Add( fgSizer19, 1, wx.EXPAND, 5 )
		
		
		self.m_panel12.SetSizer( fgSizer18 )
		self.m_panel12.Layout()
		fgSizer18.Fit(self.m_panel12 )
		self.m_choicebook2.AddPage(self.m_panel12, u"conversions", False )
		fgSizer53.Add(self.m_choicebook2, 1, wx.EXPAND |wx.ALL, 5 )
		
		fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer159.AddGrowableCol( 1 )
		fgSizer159.SetFlexibleDirection( wx.BOTH )
		fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
		fgSizer159.Add(self.m_info, 0, wx.ALL, 5 )
		
		m_sdbSizer12 = wx.StdDialogButtonSizer()
		self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK )
		m_sdbSizer12.AddButton(self.m_sdbSizer12OK )
		self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL )
		m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel )
		m_sdbSizer12.Realize();
		
		fgSizer159.Add( m_sdbSizer12, 1, wx.EXPAND, 5 )
		
		
		fgSizer53.Add( fgSizer159, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( fgSizer53 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_class_non_copyable.Bind( wx.EVT_CHECKBOX, self.on_toggle_class_non_copyable )
		self.m_checkList1.Bind( wx.EVT_CHECKLISTBOX, self.on_toggle_base_class )
		self.m_listCallPolicies.Bind( wx.EVT_LISTBOX, self.on_select_call_policy )
		self.m_listCallPolicies.Bind( wx.EVT_LISTBOX_DCLICK, self.on_edit_call_policy )
		self.m_bpButton3.Bind( wx.EVT_BUTTON, self.on_add_call_policy )
		self.m_bpButton4.Bind( wx.EVT_BUTTON, self.on_delete_call_policy)
		self.m_bpButton5.Bind( wx.EVT_BUTTON, self.on_move_up_call_policy )
		self.m_bpButton6.Bind( wx.EVT_BUTTON, self.on_move_down_call_policy )
		self.m_listBox6.Bind( wx.EVT_LISTBOX, self.on_select_conversion )
		self.m_listBox6.Bind( wx.EVT_LISTBOX_DCLICK, self.on_edit_conversion )
		self.m_bpButton7.Bind( wx.EVT_BUTTON, self.on_add_conversion )
		self.m_bpButton8.Bind( wx.EVT_BUTTON, self.on_delete_conversion )
		self.m_sdbSizer12Cancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_sdbSizer12OK.Bind( wx.EVT_BUTTON, self.on_ok )
	
	def __del__(self):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_toggle_class_non_copyable(self, event ):
		event.Skip()
	
	def on_toggle_base_class(self, event ):
		event.Skip()
	
	def on_select_call_policy(self, event ):
		event.Skip()
	
	def on_edit_call_policy(self, event ):
		event.Skip()
	
	def on_add_call_policy(self, event ):
		event.Skip()
	
	def on_delete_call_policy(self, event ):
		event.Skip()
	
	def on_move_up_call_policy(self, event ):
		event.Skip()
	
	def on_move_down_call_policy(self, event ):
		event.Skip()
	
	def on_select_conversion(self, event ):
		event.Skip()
	
	def on_edit_conversion(self, event ):
		event.Skip()
	
	def on_add_conversion(self, event ):
		event.Skip()
	
	def on_delete_conversion(self, event ):
		event.Skip()
	
	def on_cancel(self, event ):
		event.Skip()
	
	def on_ok(self, event ):
		event.Skip()
	
	def m_splitter1OnIdle(self, event ):
		self.m_splitter1.SetSashPosition( 0 )
		self.m_splitter1.Unbind( wx.EVT_IDLE )
	

###########################################################################
## Class BoostPythonCallPolicyDialogBase
###########################################################################

class BoostPythonCallPolicyDialogBase ( wxx.Dialog ):
	
	def __init__(self, parent):
		wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"Edit call policy", pos = wx.DefaultPosition, size = wx.Size( 579,451 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		
		fgSizer53 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer53.AddGrowableCol( 0 )
		fgSizer53.AddGrowableRow( 0 )
		fgSizer53.SetFlexibleDirection( wx.BOTH )
		fgSizer53.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_choicebook1 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel19 = wx.Panel(self.m_choicebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer29 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer29.AddGrowableCol( 0 )
		fgSizer29.AddGrowableRow( 1 )
		fgSizer29.AddGrowableRow( 2 )
		fgSizer29.SetFlexibleDirection( wx.BOTH )
		fgSizer29.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText4 = wx.StaticText(self.m_panel19, wx.ID_ANY, u"Select returned argument:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )
		fgSizer29.Add(self.m_staticText4, 0, wx.ALL, 5 )
		
		m_listBox4Choices = []
		self.m_listBox4 = wx.ListBox(self.m_panel19, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox4Choices, 0|wx.SIMPLE_BORDER )
		fgSizer29.Add(self.m_listBox4, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_textCtrl4 = wx.TextCtrl(self.m_panel19, wx.ID_ANY, u"  Tip: \n  return_arg and return_self instantiations are models of CallPolicies which return the specified argument parameter (usually *this) of a wrapped (member) function. ", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		self.m_textCtrl4.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BACKGROUND ) )
		
		fgSizer29.Add(self.m_textCtrl4, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel19.SetSizer( fgSizer29 )
		self.m_panel19.Layout()
		fgSizer29.Fit(self.m_panel19 )
		self.m_choicebook1.AddPage(self.m_panel19, u"return arg", False )
		self.m_panel20 = wx.Panel(self.m_choicebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer291 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer291.AddGrowableCol( 0 )
		fgSizer291.AddGrowableRow( 1 )
		fgSizer291.AddGrowableRow( 2 )
		fgSizer291.SetFlexibleDirection( wx.BOTH )
		fgSizer291.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText41 = wx.StaticText(self.m_panel20, wx.ID_ANY, u"Select custodian:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText41.Wrap( -1 )
		fgSizer291.Add(self.m_staticText41, 0, wx.ALL, 5 )
		
		m_listBox41Choices = []
		self.m_listBox41 = wx.ListBox(self.m_panel20, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox41Choices, 0|wx.SIMPLE_BORDER )
		fgSizer291.Add(self.m_listBox41, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_textCtrl41 = wx.TextCtrl(self.m_panel20, wx.ID_ANY, u"  Tip: \n  return_internal_reference instantiations are models of CallPolicies which allow pointers and references to objects held internally by a free or member function argument or from the target of a member function to be returned safely without making a copy of the referent.  ", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		self.m_textCtrl41.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BACKGROUND ) )
		
		fgSizer291.Add(self.m_textCtrl41, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel20.SetSizer( fgSizer291 )
		self.m_panel20.Layout()
		fgSizer291.Fit(self.m_panel20 )
		self.m_choicebook1.AddPage(self.m_panel20, u"return internal reference", False )
		self.m_panel21 = wx.Panel(self.m_choicebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer2911 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer2911.AddGrowableCol( 0 )
		fgSizer2911.AddGrowableRow( 1 )
		fgSizer2911.AddGrowableRow( 2 )
		fgSizer2911.SetFlexibleDirection( wx.BOTH )
		fgSizer2911.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText411 = wx.StaticText(self.m_panel21, wx.ID_ANY, u"Select result conversor:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText411.Wrap( -1 )
		fgSizer2911.Add(self.m_staticText411, 0, wx.ALL, 5 )
		
		sbSizer8 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel21, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )
		
		self.m_radioBtn2 = wx.RadioButton( sbSizer8.GetStaticBox(), wx.ID_ANY, u"copy const reference", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_radioBtn2.SetToolTip( u"copy_const_reference is a model of \nResultConverterGenerator which can\n be used to wrap C++ functions returning \na reference-to-const type such that the\n referenced value iscopied into a new\nPython object." )
		
		sbSizer8.Add(self.m_radioBtn2, 0, wx.ALL, 5 )
		
		self.m_radioBtn3 = wx.RadioButton( sbSizer8.GetStaticBox(), wx.ID_ANY, u"copy non const reference", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_radioBtn3.SetToolTip( u"copy_non_const_reference is a model\nof ResultConverterGenerator which \ncan be used to wrap C++ functions \nreturning a reference-to-non-const type\n such that the referenced value is copied\n into a new Python object. " )
		
		sbSizer8.Add(self.m_radioBtn3, 0, wx.ALL, 5 )
		
		self.m_radioBtn4 = wx.RadioButton( sbSizer8.GetStaticBox(), wx.ID_ANY, u"manage new object", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_radioBtn4.SetToolTip( u"manage_new_object is a model of\nResultConverterGenerator which can\nbe used to wrap C++ functions which\nreturn a pointer to an object allocated\nwith a new-expression, and expect the\ncaller to take responsibility for deleting\nthat object. \nCreate a new object with the ownership\nof the returned value." )
		
		sbSizer8.Add(self.m_radioBtn4, 0, wx.ALL, 5 )
		
		self.m_radioBtn1 = wx.RadioButton( sbSizer8.GetStaticBox(), wx.ID_ANY, u"reference existing object", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_radioBtn1.SetToolTip( u"Create a object referencing to value, \nreference_existing_object is a model of\nResultConverterGenerator which can be\nused to wrap C++ functions which return\na reference or pointer to a C++ object. \nWhen the wrapped function is called, the\nvalue referenced by its return value is not\ncopied. A new Python object is created which\ncontains a pointer to the referent, and no\nattempt is made to ensure that the lifetime\nof the referent is at least as long as that of\nthe corresponding Python object. Thus, it can\nbe highly dangerous to use \nreference_existing_object without additional\nlifetime management from such models of\nCallPolicies as with_custodian_and_ward. \nThis class is used in the implementation of\nreturn_internal_reference.\n" )
		
		sbSizer8.Add(self.m_radioBtn1, 0, wx.ALL, 5 )
		
		self.m_radioBtn5 = wx.RadioButton( sbSizer8.GetStaticBox(), wx.ID_ANY, u"return by value", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_radioBtn5.SetToolTip( u"return_by_value is a model of ResultConverterGenerator which can be used to wrap C++ functions returning any reference or value type such that the return value is copied into a new Python object. " )
		
		sbSizer8.Add(self.m_radioBtn5, 0, wx.ALL, 5 )
		
		self.m_radioBtn6 = wx.RadioButton( sbSizer8.GetStaticBox(), wx.ID_ANY, u"return opaque pointer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_radioBtn6.SetToolTip( u"return_opaque_pointer is a model of ResultConverterGenerator which \ncan be used to wrap C++ functions returning pointers to undefined \ntypes such that the return value is copied into a new Python object.\n\nIn addition to specifying the return_opaque_pointer policy the BOOST_PYTHON_OPAQUE_SPECIALIZED_TYPE_ID macro must be used\nto define specializations for the type_id function on the type pointed \nto by returned pointer. " )
		
		sbSizer8.Add(self.m_radioBtn6, 0, wx.ALL, 5 )
		
		
		fgSizer2911.Add( sbSizer8, 1, wx.EXPAND, 5 )
		
		self.m_textCtrl411 = wx.TextCtrl(self.m_panel21, wx.ID_ANY, u"  Tip: \n  return_value_policy instantiations are simply models of CallPolicies which are composed of a ResultConverterGenerator and optional Base CallPolicies. ", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		self.m_textCtrl411.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BACKGROUND ) )
		
		fgSizer2911.Add(self.m_textCtrl411, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel21.SetSizer( fgSizer2911 )
		self.m_panel21.Layout()
		fgSizer2911.Fit(self.m_panel21 )
		self.m_choicebook1.AddPage(self.m_panel21, u"return value", False )
		self.m_panel22 = wx.Panel(self.m_choicebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer29111 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer29111.AddGrowableCol( 0 )
		fgSizer29111.AddGrowableRow( 1 )
		fgSizer29111.AddGrowableRow( 2 )
		fgSizer29111.SetFlexibleDirection( wx.BOTH )
		fgSizer29111.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer39 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer39.AddGrowableCol( 1 )
		fgSizer39.SetFlexibleDirection( wx.BOTH )
		fgSizer39.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText12 = wx.StaticText(self.m_panel22, wx.ID_ANY, u"Select custodian and ward:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )
		fgSizer39.Add(self.m_staticText12, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_checkBox4 = wx.CheckBox(self.m_panel22, wx.ID_ANY, u"postcall", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer39.Add(self.m_checkBox4, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		fgSizer29111.Add( fgSizer39, 1, wx.EXPAND, 5 )
		
		gSizer1 = wx.GridSizer( 1, 2, 0, 0 )
		
		sbSizer11 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel22, wx.ID_ANY, u"custodian" ), wx.VERTICAL )
		
		m_listBox9Choices = []
		self.m_listBox9 = wx.ListBox( sbSizer11.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox9Choices, 0 )
		sbSizer11.Add(self.m_listBox9, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		gSizer1.Add( sbSizer11, 1, wx.EXPAND, 5 )
		
		sbSizer12 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel22, wx.ID_ANY, u"ward" ), wx.VERTICAL )
		
		m_listBox10Choices = []
		self.m_listBox10 = wx.ListBox( sbSizer12.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox10Choices, 0 )
		sbSizer12.Add(self.m_listBox10, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		gSizer1.Add( sbSizer12, 1, wx.EXPAND, 5 )
		
		
		fgSizer29111.Add( gSizer1, 1, wx.EXPAND, 5 )
		
		self.m_textCtrl4111 = wx.TextCtrl(self.m_panel22, wx.ID_ANY, u"  Tip: \n   This call policy provides facilities for establishing a lifetime dependency between two of a function's Python argument or result objects. The ward object will not be destroyed until after the custodian as long as the custodian object supports weak references (Boost.Python extension classes all support weak references). If the custodian object does not support weak references and is not None, an appropriate exception will be thrown. The two class templates with_custodian_and_ward and with_custodian_and_ward_postcall differ in the point at which they take effect.\n\nIn order to reduce the chance of inadvertently creating dangling pointers, the default is to do lifetime binding before the underlying C++ object is invoked. However, before invocation the result object is not available, so with_custodian_and_ward_postcall is provided to bind lifetimes after invocation. Also, if a C++ exception is thrown after with_custodian_and_ward<>::precall but before the underlying C++ object actually stores a pointer, the lifetime of the custodian and ward objects will be artificially bound together, so one might choose with_custodian_and_ward_postcall instead, depending on the semantics of the function being wrapped.\n\nPlease note that this is not the appropriate tool to use when wrapping functions which transfer ownership of a raw pointer across the function-call boundary. Please see the FAQ if you want to do that. ", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		self.m_textCtrl4111.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BACKGROUND ) )
		
		fgSizer29111.Add(self.m_textCtrl4111, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.m_panel22.SetSizer( fgSizer29111 )
		self.m_panel22.Layout()
		fgSizer29111.Fit(self.m_panel22 )
		self.m_choicebook1.AddPage(self.m_panel22, u"with custodian and ward", True )
		fgSizer53.Add(self.m_choicebook1, 1, wx.EXPAND |wx.ALL, 5 )
		
		fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer159.AddGrowableCol( 1 )
		fgSizer159.SetFlexibleDirection( wx.BOTH )
		fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
		fgSizer159.Add(self.m_info, 0, wx.ALL, 5 )
		
		m_sdbSizer12 = wx.StdDialogButtonSizer()
		self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK )
		m_sdbSizer12.AddButton(self.m_sdbSizer12OK )
		self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL )
		m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel )
		m_sdbSizer12.Realize();
		
		fgSizer159.Add( m_sdbSizer12, 1, wx.EXPAND, 5 )
		
		
		fgSizer53.Add( fgSizer159, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( fgSizer53 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_choicebook1.Bind( wx.EVT_CHOICEBOOK_PAGE_CHANGED, self.OnChangePolicy )
		self.m_checkBox4.Bind( wx.EVT_CHECKBOX, self.OnToggleCustodianAndWardPostcall )
		self.m_listBox9.Bind( wx.EVT_LISTBOX, self.OnSelectCustodian )
		self.m_listBox10.Bind( wx.EVT_LISTBOX, self.OnSelectWard )
		self.m_sdbSizer12Cancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_sdbSizer12OK.Bind( wx.EVT_BUTTON, self.on_ok )
	
	def __del__(self):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OnChangePolicy(self, event ):
		event.Skip()
	
	def OnToggleCustodianAndWardPostcall(self, event ):
		event.Skip()
	
	def OnSelectCustodian(self, event ):
		event.Skip()
	
	def OnSelectWard(self, event ):
		event.Skip()
	
	def on_cancel(self, event ):
		event.Skip()
	
	def on_ok(self, event ):
		event.Skip()
	

###########################################################################
## Class BoostPythonConversionDialogBase
###########################################################################

class BoostPythonConversionDialogBase ( wxx.Dialog ):
	
	def __init__(self, parent):
		wxx.Dialog.__init__ (self, parent, id = wx.ID_ANY, title = u"Boost.python conversion", pos = wx.DefaultPosition, size = wx.Size( 819,532 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )
		
		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		
		fgSizer20 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer20.AddGrowableCol( 0 )
		fgSizer20.AddGrowableRow( 0 )
		fgSizer20.SetFlexibleDirection( wx.BOTH )
		fgSizer20.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_choicebook3 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel13 = wx.Panel(self.m_choicebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer24 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer24.AddGrowableCol( 0 )
		fgSizer24.AddGrowableCol( 1 )
		fgSizer24.AddGrowableRow( 0 )
		fgSizer24.SetFlexibleDirection( wx.BOTH )
		fgSizer24.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer25 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer25.AddGrowableCol( 0 )
		fgSizer25.AddGrowableRow( 0 )
		fgSizer25.AddGrowableRow( 1 )
		fgSizer25.SetFlexibleDirection( wx.BOTH )
		fgSizer25.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer7 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel13, wx.ID_ANY, u"python type" ), wx.VERTICAL )
		
		m_listBox7Choices = [ u"string", u"dict", u"list", u"object" ]
		self.m_listBox7 = wx.ListBox( sbSizer7.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox7Choices, wx.LB_SORT )
		sbSizer7.Add(self.m_listBox7, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer25.Add( sbSizer7, 1, wx.EXPAND, 5 )
		
		sbSizer8 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel13, wx.ID_ANY, u"c++ type" ), wx.VERTICAL )
		
		m_listBox8Choices = []
		self.m_listBox8 = wx.ListBox( sbSizer8.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox8Choices, wx.LB_SORT )
		sbSizer8.Add(self.m_listBox8, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer25.Add( sbSizer8, 1, wx.EXPAND, 5 )
		
		
		fgSizer24.Add( fgSizer25, 1, wx.EXPAND, 5 )
		
		sbSizer9 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel13, wx.ID_ANY, u"converter" ), wx.VERTICAL )
		
		fgSizer26 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer26.AddGrowableCol( 0 )
		fgSizer26.AddGrowableRow( 1 )
		fgSizer26.SetFlexibleDirection( wx.BOTH )
		fgSizer26.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_textCtrl6 = wx.TextCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, u"static void construct( PyObject* object, boost::python::converter::rvalue_from_python_stage1_data* data)\n{", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.NO_BORDER )
		self.m_textCtrl6.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		self.m_textCtrl6.SetMinSize( wx.Size( -1,70 ) )
		
		fgSizer26.Add(self.m_textCtrl6, 0, wx.EXPAND, 5 )

		from beatle.activity.models.cc.ui.ctrl import Editor
		self.m_editor = Editor(sbSizer9.GetStaticBox(), **self._editorArgs)
		# self.m_editor = wx.TextCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( -1,100 ), wx.TE_MULTILINE|wx.NO_BORDER )
		self.m_editor.SetMinSize( wx.Size( 600,-1 ) )
		
		fgSizer26.Add(self.m_editor, 1, wx.EXPAND, 5 )
		
		self.m_textCtrl8 = wx.TextCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, u"}", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY|wx.NO_BORDER )
		self.m_textCtrl8.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		
		fgSizer26.Add(self.m_textCtrl8, 0, wx.EXPAND|wx.BOTTOM, 5 )
		
		
		sbSizer9.Add( fgSizer26, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		fgSizer24.Add( sbSizer9, 1, wx.EXPAND, 5 )
		
		
		self.m_panel13.SetSizer( fgSizer24 )
		self.m_panel13.Layout()
		fgSizer24.Fit(self.m_panel13 )
		self.m_choicebook3.AddPage(self.m_panel13, u"from python", False )
		fgSizer20.Add(self.m_choicebook3, 1, wx.EXPAND |wx.ALL, 5 )
		
		fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer159.AddGrowableCol( 1 )
		fgSizer159.SetFlexibleDirection( wx.BOTH )
		fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
		fgSizer159.Add(self.m_info, 0, wx.ALL, 5 )
		
		m_sdbSizer12 = wx.StdDialogButtonSizer()
		self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK )
		m_sdbSizer12.AddButton(self.m_sdbSizer12OK )
		self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL )
		m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel )
		m_sdbSizer12.Realize();
		
		fgSizer159.Add( m_sdbSizer12, 1, wx.EXPAND, 5 )
		
		
		fgSizer20.Add( fgSizer159, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( fgSizer20 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_sdbSizer12Cancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_sdbSizer12OK.Bind( wx.EVT_BUTTON, self.on_ok )
	
	def __del__(self):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_cancel(self, event ):
		event.Skip()
	
	def on_ok(self, event ):
		event.Skip()
	

###########################################################################
## Class BoostPythonModuleEditorBase
###########################################################################

class BoostPythonModuleEditorBase ( wx.Panel ):
	
	def __init__(self, parent):
		wx.Panel.__init__ (self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 547,462 ), style = wx.TAB_TRAVERSAL )
		
		fgSizer53 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer53.AddGrowableCol( 0 )
		fgSizer53.AddGrowableRow( 1 )
		fgSizer53.SetFlexibleDirection( wx.BOTH )
		fgSizer53.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		fgSizer54 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer54.AddGrowableCol( 1 )
		fgSizer54.AddGrowableRow( 0 )
		fgSizer54.SetFlexibleDirection( wx.BOTH )
		fgSizer54.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText26 = wx.StaticText(self, wx.ID_ANY, u"name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText26.Wrap( -1 )
		fgSizer54.Add(self.m_staticText26, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.m_textCtrl21 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer54.Add(self.m_textCtrl21, 0, wx.EXPAND|wx.ALL, 5 )
		
		
		fgSizer53.Add( fgSizer54, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )
		
		self.m_choicebook2 = wx.Choicebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.CHB_DEFAULT )
		self.m_panel11 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		fgSizer17 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer17.AddGrowableCol( 0 )
		fgSizer17.AddGrowableRow( 0 )
		fgSizer17.SetFlexibleDirection( wx.BOTH )
		fgSizer17.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_splitter1 = wx.SplitterWindow(self.m_panel11, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
		self.m_splitter1.SetSashGravity( 0.5 )
		self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )
		self.m_splitter1.SetMinimumPaneSize( 20 )
		
		self.m_panel1 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer6 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer6.AddGrowableCol( 0 )
		fgSizer6.AddGrowableRow( 0 )
		fgSizer6.SetFlexibleDirection( wx.BOTH )
		fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer6 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel1, wx.ID_ANY, u"exposed elements" ), wx.VERTICAL )
		
		style = (CT.TR_DEFAULT_STYLE|CT.TR_MULTIPLE  | CT.TR_AUTO_CHECK_CHILD  | CT.TR_AUTO_CHECK_PARENT | CT.TR_AUTO_TOGGLE_CHILD)
		self.m_tree = CT.CustomTreeCtrl(self.m_panel1, style=style)
		sbSizer6.Add(self.m_tree, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer6.Add( sbSizer6, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_panel1.SetSizer( fgSizer6 )
		self.m_panel1.Layout()
		fgSizer6.Fit(self.m_panel1 )
		self.m_panel3 = wx.Panel(self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer4 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer4.AddGrowableCol( 0 )
		fgSizer4.AddGrowableRow( 0 )
		fgSizer4.SetFlexibleDirection( wx.BOTH )
		fgSizer4.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox(self.m_panel3, wx.ID_ANY, u"attributes" ), wx.VERTICAL )
		
		self.m_book = Simplebook( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_defaultPane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_book.AddPage(self.m_defaultPane, u"a page", False )
		self.m_classPane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer5 = wx.FlexGridSizer( 2, 1, 0, 0 )
		fgSizer5.AddGrowableCol( 0 )
		fgSizer5.AddGrowableRow( 1 )
		fgSizer5.SetFlexibleDirection( wx.BOTH )
		fgSizer5.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_class_non_copyable = wx.CheckBox(self.m_classPane, wx.ID_ANY, u"Make non &copyable", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_class_non_copyable.SetHelpText( u"If this class has deleted or private constructor or if we don't want to expose default creator, check this box." )
		
		fgSizer5.Add(self.m_class_non_copyable, 0, wx.ALL, 5 )
		
		sbSizer61 = wx.StaticBoxSizer( wx.StaticBox(self.m_classPane, wx.ID_ANY, u"base classes" ), wx.VERTICAL )
		
		m_checkList1Choices = []
		self.m_checkList1 = wx.CheckListBox( sbSizer61.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_checkList1Choices, 0 )
		self.m_checkList1.SetToolTip( u"Select inheritance base classes \nto be exposed in python." )
		
		sbSizer61.Add(self.m_checkList1, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer5.Add( sbSizer61, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_classPane.SetSizer( fgSizer5 )
		self.m_classPane.Layout()
		fgSizer5.Fit(self.m_classPane )
		self.m_book.AddPage(self.m_classPane, u"a page", False )
		self.m_memberMethodPane = wx.Panel(self.m_book, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer8 = wx.FlexGridSizer( 1, 1, 0, 0 )
		fgSizer8.AddGrowableCol( 0 )
		fgSizer8.AddGrowableRow( 0 )
		fgSizer8.SetFlexibleDirection( wx.BOTH )
		fgSizer8.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_auinotebook3 = aui.AuiNotebook(self.m_memberMethodPane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, aui.AUI_NB_BOTTOM )
		self.m_panel6 = wx.Panel(self.m_auinotebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer9 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer9.AddGrowableCol( 0 )
		fgSizer9.AddGrowableRow( 0 )
		fgSizer9.SetFlexibleDirection( wx.BOTH )
		fgSizer9.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		m_listCallPoliciesChoices = []
		self.m_listCallPolicies = wx.ListBox(self.m_panel6, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listCallPoliciesChoices, 0 )
		self.m_listCallPolicies.SetToolTip( u"Double click for item editing." )
		
		fgSizer9.Add(self.m_listCallPolicies, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer10 = wx.FlexGridSizer( 5, 1, 0, 0 )
		fgSizer10.AddGrowableCol( 0 )
		fgSizer10.AddGrowableRow( 4 )
		fgSizer10.SetFlexibleDirection( wx.BOTH )
		fgSizer10.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_bpButton3 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ADD_BOOKMARK, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		fgSizer10.Add(self.m_bpButton3, 0, wx.ALL, 5 )
		
		self.m_bpButton4 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton4.Enable( False )
		
		fgSizer10.Add(self.m_bpButton4, 0, wx.ALL, 5 )
		
		self.m_bpButton5 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_UP, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton5.Enable( False )
		
		fgSizer10.Add(self.m_bpButton5, 0, wx.ALL, 5 )
		
		self.m_bpButton6 = wx.BitmapButton(self.m_panel6, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_GO_DOWN, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton6.Enable( False )
		
		fgSizer10.Add(self.m_bpButton6, 0, wx.ALL, 5 )
		
		
		fgSizer10.Add( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		fgSizer9.Add( fgSizer10, 1, wx.EXPAND, 5 )
		
		
		self.m_panel6.SetSizer( fgSizer9 )
		self.m_panel6.Layout()
		fgSizer9.Fit(self.m_panel6 )
		self.m_auinotebook3.AddPage(self.m_panel6, u"call policies", False )
		
		fgSizer8.Add(self.m_auinotebook3, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		self.m_memberMethodPane.SetSizer( fgSizer8 )
		self.m_memberMethodPane.Layout()
		fgSizer8.Fit(self.m_memberMethodPane )
		self.m_book.AddPage(self.m_memberMethodPane, u"a page", False )
		
		sbSizer2.Add(self.m_book, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		fgSizer4.Add( sbSizer2, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_panel3.SetSizer( fgSizer4 )
		self.m_panel3.Layout()
		fgSizer4.Fit(self.m_panel3 )
		self.m_splitter1.SplitVertically(self.m_panel1, self.m_panel3, 0 )
		fgSizer17.Add(self.m_splitter1, 1, wx.EXPAND, 5 )
		
		
		self.m_panel11.SetSizer( fgSizer17 )
		self.m_panel11.Layout()
		fgSizer17.Fit(self.m_panel11 )
		self.m_choicebook2.AddPage(self.m_panel11, u"exports", True )
		self.m_panel12 = wx.Panel(self.m_choicebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer18 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer18.AddGrowableCol( 0 )
		fgSizer18.AddGrowableRow( 0 )
		fgSizer18.SetFlexibleDirection( wx.BOTH )
		fgSizer18.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		m_listBox6Choices = []
		self.m_listBox6 = wx.ListBox(self.m_panel12, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox6Choices, 0 )
		self.m_listBox6.SetToolTip( u"List current conversions.\nDouble-click for editing." )
		
		fgSizer18.Add(self.m_listBox6, 0, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer19 = wx.FlexGridSizer( 3, 1, 0, 0 )
		fgSizer19.AddGrowableRow( 2 )
		fgSizer19.SetFlexibleDirection( wx.BOTH )
		fgSizer19.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_bpButton7 = wx.BitmapButton(self.m_panel12, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ADD_BOOKMARK, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		fgSizer19.Add(self.m_bpButton7, 0, wx.ALL, 5 )
		
		self.m_bpButton8 = wx.BitmapButton(self.m_panel12, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_MENU ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
		self.m_bpButton8.Enable( False )
		
		fgSizer19.Add(self.m_bpButton8, 0, wx.ALL, 5 )
		
		
		fgSizer18.Add( fgSizer19, 1, wx.EXPAND, 5 )
		
		
		self.m_panel12.SetSizer( fgSizer18 )
		self.m_panel12.Layout()
		fgSizer18.Fit(self.m_panel12 )
		self.m_choicebook2.AddPage(self.m_panel12, u"conversions", False )
		fgSizer53.Add(self.m_choicebook2, 1, wx.EXPAND |wx.ALL, 5 )
		
		fgSizer159 = wx.FlexGridSizer( 1, 2, 0, 0 )
		fgSizer159.AddGrowableCol( 1 )
		fgSizer159.SetFlexibleDirection( wx.BOTH )
		fgSizer159.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_info = wx.BitmapButton(self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_TIP, wx.ART_BUTTON ), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.NO_BORDER )
		fgSizer159.Add(self.m_info, 0, wx.ALL, 5 )
		
		m_sdbSizer12 = wx.StdDialogButtonSizer()
		self.m_sdbSizer12OK = wx.Button(self, wx.ID_OK )
		m_sdbSizer12.AddButton(self.m_sdbSizer12OK )
		self.m_sdbSizer12Cancel = wx.Button(self, wx.ID_CANCEL )
		m_sdbSizer12.AddButton(self.m_sdbSizer12Cancel )
		m_sdbSizer12.Realize();
		
		fgSizer159.Add( m_sdbSizer12, 1, wx.EXPAND, 5 )
		
		
		fgSizer53.Add( fgSizer159, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( fgSizer53 )
		self.Layout()
		
		# Connect Events
		self.m_class_non_copyable.Bind( wx.EVT_CHECKBOX, self.on_toggle_class_non_copyable )
		self.m_checkList1.Bind( wx.EVT_CHECKLISTBOX, self.on_toggle_base_class )
		self.m_listCallPolicies.Bind( wx.EVT_LISTBOX, self.on_select_call_policy )
		self.m_listCallPolicies.Bind( wx.EVT_LISTBOX_DCLICK, self.on_edit_call_policy )
		self.m_bpButton3.Bind( wx.EVT_BUTTON, self.on_add_call_policy )
		self.m_bpButton4.Bind( wx.EVT_BUTTON, self.on_delete_call_policy)
		self.m_bpButton5.Bind( wx.EVT_BUTTON, self.on_move_up_call_policy )
		self.m_bpButton6.Bind( wx.EVT_BUTTON, self.on_move_down_call_policy )
		self.m_listBox6.Bind( wx.EVT_LISTBOX, self.on_select_conversion )
		self.m_listBox6.Bind( wx.EVT_LISTBOX_DCLICK, self.on_edit_conversion )
		self.m_bpButton7.Bind( wx.EVT_BUTTON, self.on_add_conversion )
		self.m_bpButton8.Bind( wx.EVT_BUTTON, self.on_delete_conversion )
		self.m_sdbSizer12Cancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_sdbSizer12OK.Bind( wx.EVT_BUTTON, self.on_ok )
	
	def __del__(self):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_toggle_class_non_copyable(self, event ):
		event.Skip()
	
	def on_toggle_base_class(self, event ):
		event.Skip()
	
	def on_select_call_policy(self, event ):
		event.Skip()
	
	def on_edit_call_policy(self, event ):
		event.Skip()
	
	def on_add_call_policy(self, event ):
		event.Skip()
	
	def on_delete_call_policy(self, event ):
		event.Skip()
	
	def on_move_up_call_policy(self, event ):
		event.Skip()
	
	def on_move_down_call_policy(self, event ):
		event.Skip()
	
	def OnSelectConversion(self, event ):
		event.Skip()
	
	def on_edit_conversion(self, event ):
		event.Skip()
	
	def on_add_conversion(self, event ):
		event.Skip()
	
	def on_delete_conversion(self, event ):
		event.Skip()
	
	def on_cancel(self, event ):
		event.Skip()
	
	def on_ok(self, event ):
		event.Skip()
	
	def m_splitter1OnIdle(self, event ):
		self.m_splitter1.SetSashPosition( 0 )
		self.m_splitter1.Unbind( wx.EVT_IDLE )
	

