
import wx
from beatle.lib.api import context
from beatle import local_path
from beatle.lib import wxx
from beatle.lib.handlers import identifier
from beatle.lib.tran import TransactionalMethod
from beatle.activity.models.ui.view import ModelsView 
from beatle.model.cc import CCProject, TypesFolder

from .ui import BoostPythonModuleDialog, BoostPythonModuleEditor 
from .model import pyBoostModule, pyBoostClass, pyBoostRelation, pyBoostConstructor, pyBoostMethod


class PythonBoostWizard(wx.EvtHandler):
    """Class for providing rpclib wizard"""
    instance = None

    def __init__(self):
        """Initialize ast explorer"""
        super(PythonBoostWizard, self).__init__()
        
        self._pyboost = identifier('ID_BOOST_WIZARD')
        self.project = None
        self._pyboost_menu = None
        # register custom module class
        CCProject.module_classes.append(pyBoostModule)
        
        self.append_tools_menu()
        self.Bind(wx.EVT_UPDATE_UI, self.OnUpdatePyBoost, id=self._pyboost)
        self.Bind(wx.EVT_MENU, self.on_python_boost, id=self._pyboost)
        
        # update modelview
        ModelsView.extend_tree_order(
            [pyBoostModule, pyBoostClass,
             pyBoostRelation, pyBoostConstructor, pyBoostMethod], TypesFolder)
        ModelsView.alias_map[pyBoostModule] = 'c++ boost.python "{0}"'
        ModelsView.alias_map[pyBoostClass] = 'c++ boost.python class "{0}"'
        ModelsView.alias_map[pyBoostRelation] = 'c++ boost.python relation "{0}"'
        ModelsView.alias_map[pyBoostConstructor] = 'c++ boost.python constructor'
        ModelsView.alias_map[pyBoostMethod] = 'c++ boost.python method "{0}"'
        ModelsView.edit_map.update({
            self.open_python_boost_wizard : [pyBoostModule]})
        
        ModelsView._handlers.append(self)
        
    def append_tools_menu(self):
        frame = context.get_frame()
        self._pyboost_menu = wxx.MenuItem(
            frame.menuTools, self._pyboost, u"boost.python",
            u"handle c++ boost.python interface.", wx.ITEM_NORMAL)
        self._pyboost_menu.SetBitmap(
            wx.Bitmap(local_path("plugin/tools/_pyboost/res/pyboost.xpm"), wx.BITMAP_TYPE_ANY))
        frame.add_tools_separator()
        frame.add_tools_menu('pyboost', self._pyboost_menu)

    @property
    def can_edit_open(self):
        frame = context.get_frame()
        view = frame.viewBook.GetCurrentPage()
        if type(view) is ModelsView:
            if type(view.selected) is pyBoostModule:
                return True
        return False

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        pass

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        if cls.instance is None:
            cls.instance = PythonBoostWizard()
        return cls.instance
    
    def OnUpdatePyBoost(self, event):
        """Handle the update of menu enable/disable status"""
        # get the view book
        frame = context.get_frame()
        self.project = None
        book = frame.viewBook
        try:
            view = book.GetCurrentPage()
            if view is not None:
                # get the selected item
                selected = view.selected
                if selected is not None:
                    project = getattr(selected, 'project', None)
                    if project and project.language == 'c++':
                        self.project = project
        except Exception as e:
            print(" some exception occured in rpc wizard:{}".format(e))
        event.Enable(bool(self.project))
        
    @TransactionalMethod('Add boost interface')
    @wxx.CreationDialog(BoostPythonModuleDialog, pyBoostModule)
    def on_python_boost(self, event):
        """Do add boost.python interface"""
        frame = context.get_frame()
        self.project = None
        book = frame.viewBook
        try:
            view = book.GetCurrentPage()
            if view is not None:
                # get the selected item
                selected = view.selected
                if selected is not None:
                    project = getattr(selected, 'project', None)
                    if project and project._language == 'c++':
                        return (context.get_frame(), project)
        except Exception as e:
            print(" some exception occured in boost.python wizard:{}".format(e))
        return False
        
    def open_python_boost_wizard(self, module, view=None):
        """Handles the edition of the pyBoost wizard as a document"""
        if getattr(module, '_pane', None):
            raise RuntimeError('Already open pane for module')
        frame = context.get_frame()
        pane = BoostPythonModuleEditor(frame.docBook, frame, module)
        module._pane = pane
        frame.docBook.AddPage(pane, module.tab_label, True, module.bitmap_index)

