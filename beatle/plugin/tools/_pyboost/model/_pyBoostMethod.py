"""
Created on 1 oct. 2018

@author: mel
"""

# noinspection PyPep8Naming
import re
from beatle.model import TComponent
from beatle.model.cc import Argument
from ..model._utils import TRANSLATED_OPERATORS


class pyBoostMethod(TComponent):
    """Represents the export of C++ class
    through boost.python"""

    def __init__(self, **kwargs):
        self._exported_method = kwargs['export_of']
        self._call_policies = kwargs.get('call_policies',[])
        name = self._exported_method.name
        if re.match(r"\s*operator\s+(.)*", name):
            try:
                key = next(x for x in TRANSLATED_OPERATORS.keys() if re.match(x, name))
                name = TRANSLATED_OPERATORS[key]
            except StopIteration:
                pass  # unsupported operator
        kwargs['name'] = name
        kwargs['note'] = self._exported_method.note
        super(pyBoostMethod, self).__init__(**kwargs)

    @property
    def exported_method(self):
        return self._exported_method

    @property
    def tree_label(self):
        return self._exported_method.tree_label

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_boost_method")

    @property
    def reference(self):
        """Return the reference to the method.
        First versions of this method were returning &class::method, but that complaints
        when using overloads with different argument types
        (overloads generated by default arguments are solved by other ways)
        so the solution is using declarations of the style
        static_cast<return_type (class::*)(arg_type1,...,arg_typeN)>(&class::method)
        """
        base = self.parent.wrapper_class
        method_name = self._exported_method.name
        return_type = str(self._exported_method.type_instance).format('')
        arg_types = ','.join(str(x.type_instance).format('') for x in self._exported_method(Argument))
        return f'static_cast<{return_type} ({base}::*)({arg_types})>(&{base}::{method_name})'
        # return '&{base}::{name}'.format(base=self.parent.wrapper_class, name=self._exported_method.name)

    @property
    def overload(self):
        if self not in self.parent._method_overloads:
            return ''
        handler = self.parent._method_overloads[self]
        args = self._exported_method[Argument]
        args = ', '.join(
            [f'bpy::arg("{x.name}")' for x in args if not x._default ] +
            [f'bpy::arg("{x.name}")={x._default}' for x in args if x._default])
        if args:
            args =', '.join(x for x in ['({})'.format(args),self.docstring] if len(x)>0)
        return '{handler}({args})'.format(handler=handler, args=args)


    @property
    def call_policies(self):
        policies = []
        for x in self._call_policies:
            policies = ['{name}<{args}>'.format(
                name=x[0], args=','.join([str(u) for u in x[1]]+policies))]
        return (len(policies) > 0 and '{}()'.format(policies[0])) or ''

    @property
    def docstring(self):
        return self._parent._comment_dict.get(self, '')

    @property
    def static_decl(self):
        if self._exported_method.static:
            return '.staticmethod("{name}")'.format(name=self.name)
        else:
            return ''

    def write_code(self, tabs, file_writer):
        """Write the code corresponding to ctor export"""
        overload = self.overload
        policies = self.call_policies
        if overload:
            if policies:
                file_writer.write_line('{prefix}.def("{name}", {ref}, {overload}[{policies}]){static_decl}\n'.format(
                    prefix='    '*tabs, name=self.name, ref=self.reference, overload=self.overload,
                    policies=policies, static_decl=self.static_decl))
            else:
                file_writer.write_line('{prefix}.def("{name}", {ref}, {overload}){static_decl}\n'.format(
                    prefix='    '*tabs, name=self.name, ref=self.reference, overload=self.overload,
                    static_decl=self.static_decl))
        else:
            file_writer.write_line('{prefix}.def("{name}", {expr}){static_decl}\n'.format(
                prefix='    '*tabs, name=self.name, expr=','.join(x for x in
                    (self.reference, self.overload, policies, self.docstring) if len(x) > 0),
                static_decl=self.static_decl))

