"""
Created on 1 oct. 2018

@author: mel
"""

from beatle.model import TComponent
from beatle.model.cc import Argument


# noinspection PyPep8Naming
class pyBoostConstructor(TComponent):
    """Represents the export of C++ class
    through boost.python"""

    def __init__(self, **kwargs):
        self._exported_method = kwargs['export_of']
        self._call_policies = kwargs.get('call_policies',[])
        kwargs['name'] = self._exported_method.name
        kwargs['note'] = self._exported_method.note
        #self._exported_delete_method = None
        super(pyBoostConstructor, self).__init__(**kwargs)

    @property
    def exported_method(self):
        return self._exported_method

    @property
    def tree_label(self):
        return self._exported_method.tree_label

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_boost_constructor")

    @property
    def docstring(self):
        return self._parent._comment_dict.get(self, '')

    @property
    def init(self):
        args = self._exported_method[Argument]
        arg_decl = ''
        type_decl=''
        if args:
            type_decl = ', '.join([str(x._typei).format('') for x in args if not x._default])
            type_decl_opt= ', '.join([str(x._typei).format('') for x in args if x._default])
            if type_decl_opt:
                type_decl = ', '.join(x for x in [type_decl, 'bpy::optional<{}>'.format(type_decl_opt)] if len(x)>0)
            n = ', '.join(
                ['bpy::arg("{name}")'.format(name=x.name) for x in self._exported_method[Argument] if not x._default ] +
                ['bpy::arg("{name}")={default}'.format(name=x.name,default=x._default) for x in self._exported_method[Argument] if x._default])
            if n:
                arg_decl ='({n})'.format(n=n)
        return 'bpy::init<{types}>({args})'.format(
            types=type_decl,args=','.join(x for x in [arg_decl,self.docstring] if len(x)>0))

    @property
    def call_policies(self):
        policies = []
        for x in self._call_policies:
            policies = ['{name}<{args}>'.format(
                name=x[0], args=','.join([str(u) for u in x[1]]+policies))]
        return (len(policies) > 0 and '[{}()]'.format(policies[0])) or ''

    def write_code(self, tabs, pf):
        """Write the code corresponding to ctor export"""
        pf.write_line('{prefix}.def({init}{policies})\n'.format(
            prefix='    '*tabs, init=self.init, policies=self.call_policies))

