'''
Created on 2 nov. 2018

@author: mel
'''


def multiline_comment(comment):
    """Process a multiline comment by adding '\n\' after each line"""
    if comment:
        txt = comment.replace('"','\\"')
        array = txt.splitlines()
        if len(array) == 1:
            return array[0]
        return '\\n\\\n'.join(array)
    else:
        return ''


TRANSLATED_OPERATORS = {
            r"\s*operator\s+==": '__eq__',
            r"\s*operator\s+!=": '__ne__',
            r"\s*operator\s+<": '__lt__',
            r"\s*operator\s+>": '__gt__',
            r"\s*operator\s+<=": '__le__',
            r"\s*operator\s+>=": '__ge__',
            r"\s*operator\s+\+": '__add__',
            r"\s*operator\s+\-": '__sub__',
            r"\s*operator\s+\*": '__mul__',
            r"\s*operator\s+/": '__div__',
            r"\s*operator\s+%": '__mod__',
            r"\s*operator\s+<<": '__lshift__',
            r"\s*operator\s+>>": '__rshift__',
            r"\s*operator\s+&": '__and__',
            r"\s*operator\s+|": '__or__',
            r"\s*operator\s+\^": '__xor__',
            r"\s*operator\s+\+=": '__iadd__',
            r"\s*operator\s+\-=": '__isub__',
            r"\s*operator\s+\*=": '__imul__',
            r"\s*operator\s+/=": '__idiv__',
            r"\s*operator\s+%=": '__imod__',
            r"\s*operator\s+&=": '__iand__',
            r"\s*operator\s+|=": '__ior__'
}

