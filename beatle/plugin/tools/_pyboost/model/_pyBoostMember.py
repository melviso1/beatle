"""
Created on 1 oct. 2018

@author: mel
"""

from beatle.model import TComponent


# noinspection PyPep8Naming
class pyBoostMember(TComponent):
    """Represents the export of C++ class member 
    through boost.python"""
    
    def __init__(self, **kwargs):
        self._exported_member = kwargs['export_of']
        kwargs['name'] = self._exported_member.name
        super(pyBoostMember, self).__init__(**kwargs)

    @property
    def exported_member(self):
        return self._exported_member

    @property
    def tree_label(self):
        return self._exported_member.tree_label

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_boost_member")
