'''
Created on 7 nov. 2018

@author: mel
'''

from beatle.model import TComponent


# noinspection PyPep8Naming
class pyBoostFromPythonConverter(TComponent):
    """Represents a automated python converter"""

    def __init__(self, **kwargs):
        self._python_type = kwargs['python_type']
        self._cpp_type = kwargs['cpp_type']
        self._converter = kwargs['converter']
        super(pyBoostFromPythonConverter, self).__init__(**kwargs)

    @property
    def tree_label(self):
        return 'conversion from {self._python_type} to {self._cpp_type}'.format(self=self)

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_boost_from_python_converter")

    @property
    def converter_name(self):
        py_type=self._python_type
        cpp_type=self._cpp_type.replace('::','__')
        return 'converter_from_python_{py_type}_to_{cpp_type}'.format(py_type=py_type,cpp_type=cpp_type)

    def WriteConversion(self, pf):
        if self._python_type == 'string':
            check = 'PyString_Check(object) ? object : NULL'
        elif self._python_type == 'dict':
            check = 'PyDict_Check(object)? object : NULL'
        elif self._python_type == 'list':
            check = 'PyList_Check(object)? object : NULL'
        elif self._python_type == 'bool':
            check = 'PyBool_Check(object)? object : NULL'
        elif self._python_type == 'object':
            check = 'object'  # dummy check
        else:
            check = 'NULL /*missing check!!!*/'
        code = """
struct {converter_name}
{{

  {converter_name}& register_conversion()
  {{
    boost::python::converter::registry::push_back(
      &{converter_name}::convertible,
      &{converter_name}::construct,
      boost::python::type_id<{cpp_type}>());
    return *this;
  }}
  
  static void* convertible(PyObject* object)
  {{
    return {check};
  }}

  static void construct( PyObject* object, boost::python::converter::rvalue_from_python_stage1_data* data)
  {{
      {converter}
  }}
}};""".format(
        converter_name = self.converter_name,
        converter = self._converter,
        cpp_type = self._cpp_type,
        check=check)
        pf.write_line(code)

    def RegisterConversion(self, pf):
        pf.write_line('    {converter_name}().register_conversion();'.format(converter_name=self.converter_name))



