"""
Created on 1 oct. 2018

@author: mel
"""

from beatle.model import TComponent
from beatle.model.cc import Argument

from ._pyBoostMethod import pyBoostMethod
from ._pyBoostMember import pyBoostMember
from ._pyBoostConstructor import pyBoostConstructor
from ._pyBoostRelation import pyBoostRelation
from ._utils import multiline_comment


# noinspection PyPep8Naming
class pyBoostClass(TComponent):
    """Represents the export of C++ class
    through boost.python"""

    def __init__(self, **kwargs):
        self._exported_class = kwargs['export_of']
        self._non_copyable = kwargs.get('non_copyable', False)
        self._inherits = kwargs.get('inherits',[])
        self._comment_dict = {}
        self._scoped_decl = None
        self._using_wrapper = False
        self._wrapper_class = None
        self._method_overloads = {}
        self._overloads = 0
        kwargs['name'] = self._exported_class.name
        kwargs['note'] = self._exported_class.note
        super(pyBoostClass, self).__init__(**kwargs)

    @property
    def exported_class(self):
        return self._exported_class

    @property
    def wrapper_class(self):
        return self._wrapper_class

    @property
    def name(self):
        """Until we be able to edit the exported name, the valid one is the exported class name,
        and it could have been refactored from his orginal name"""
        return self._exported_class.name

    @property
    def tree_label(self):
        return self._exported_class.tree_label

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("py_boost_class")

    @property
    def require_wrapper(self):
        """Indicates if the class require a wrapper to be
        exported to python. The presence of virtual methods
        is one of the reasons"""
        # TODO : wrapper is required here whenever a class is exposing virtual methods
        # TODO : in order to allow override on python side. Nevertheless the boost.python
        # TODO : proposed solution don't works fine because the real class exposed could be
        # TODO : accessed from foreign methods and the wrapper-exposed calls can't be called.
        # TODO : Possible solutions are rewrite this wrapping with variadic templates or
        # TODO : use conversions, although this later seems poor.
        # TODO : For the moment, we simply disable that because loses exceeds benefits
        # try:
        #     if next(x for x in self(pyBoostMethod) if x.exported_method.is_virtual):
        #         return True
        # except StopIteration:
        #     pass
        return False

    def before_write(self):
        """before writing code, prepare all"""
        self._using_wrapper = self.require_wrapper
        self._scoped_decl = self._exported_class.scoped
        if self._using_wrapper:
            self._wrapper_class = '{name}_wrapper'.format(name=self.name)
        else:
            self._wrapper_class = self._scoped_decl

    def declare_wrapper(self, pf):
        """If required, declare a wrapper class for export virtual methods."""
        if not self._using_wrapper:
            return
        pf.write_newline()
        pf.write_line('//------------------------------------------------')
        pf.write_line('// wrapper for class {self._scoped_decl}'.format(self=self))
        pf.write_line('//------------------------------------------------')
        pf.write_newline()
        pf.write_line(
            'struct {self._wrapper_class}: {self._scoped_decl}, boost::python::wrapper<{self._scoped_decl}>'.format(
                self=self))
        pf.write_line('{')

        #write ctors
        for ctor in self(pyBoostConstructor):
            args_decl = ','.join(arg.declare for arg in ctor._exported_method[Argument])
            args_call = ','.join(arg.name for arg in ctor._exported_method[Argument])
            pf.write_line('    {self._wrapper_class}({args_decl})'.format(self=self,args_decl=args_decl))
            pf.write_line('    :{self._scoped_decl}({args_call})'.format(self=self, args_call=args_call))
            pf.write_line('    {};')
            pf.write_newline()

        #write methods, redirecting for virtual and virtual pure cases
        for method in self(pyBoostMethod):
            target = method._exported_method
            args_decl = ', '.join(arg.declare for arg in target[Argument])
            args_call = ','.join(x.name for x in target[Argument])
            untyped = '{name}({args_decl})'.format(name=method.name,  args_decl=args_decl)
            pf.write_line('    {method}'.format(method = str(target._typei).format(untyped)))
            pf.write_line('    {')
            if not target._virtual:
                # simple case: redirecting
                if target._typei.is_void:
                    pf.write_line('        {self._scoped_decl}::{name}({args_call});'.format(self=self,name=target.name,args_call=args_call))
                else:
                    pf.write_line('        return {self._scoped_decl}::{name}({args_call});'.format(self=self,name=target.name,args_call=args_call))
            else:
                # pure case
                if target._pure:
                    pf.write_line('        this->get_override("{name}")({args_call});'.format(name=target.name,args_call=args_call))
                else:
                    pf.write_line('        override f = this->get_override("{name}");'.format(name=target.name))
                    pf.write_line('        if(f)')
                    pf.write_line('        {')
                    if target._typei.is_void:
                        pf.write_line('            f({args_call});'.format(args_call=args_call))
                    else:
                        pf.write_line('            return f({args_call});'.format(args_call=args_call))
                    pf.write_line('        }')
                    pf.write_line('        else')
                    pf.write_line('        {')
                    if target._typei.is_void:
                        pf.write_line('            {self._scoped_decl}::{name}({args_call});'.format(self=self,name=method.name,args_call=args_call))
                    else:
                        pf.write_line('            return {self._scoped_decl}::{name}({args_call});'.format(self=self,name=method.name,args_call=args_call))
                    pf.write_line('        }')
            pf.write_line('    }')
        pf.write_line('};')
        pf.write_newline()
        pf.write_newline()

    def declare_comment_string(self, pf):
        """Comment strings breaks style on genetared code.
        For that reason, we prefer to generate these comments
        as global variables.
        The variables are declared as :

            const char *comment_class_{class name}_ = ...  //for classes
            const char *comment_ctor_{class name}_{override number} = ...  //for ctor
            const char *comment_{class name}_{method}_{override number} = ... //for member method

        The comment names are stored in a dict self._comment_dict of the form { object : comment_name }
        where object is any instance of pyBoostClass, pyBoostMethod or pyBoostConstructor.
        """
        pf.write_line("//------------------------------------------------")
        pf.write_line("//       DOCSTRINGS FOR CLASS {name}".format(name=self.name))
        pf.write_line("//------------------------------------------------")

        self._comment_dict = {}
        #class note
        if self.note:
            varname = 'comment_class_{name}'.format(name=self.name)
            pf.write_line('const char* {varname} = "{note}";'.format(varname=varname,note=multiline_comment(self.note)))
            self._comment_dict[self] = varname

        #iterate over constructors
        interfaces = self(pyBoostConstructor)
        count = 1
        for interface in interfaces:
            if interface.note:
                varname = 'comment_ctor_{name}_{count}'.format(name=self.name, count=count)
                pf.write_line('const char* {varname} = "{note}";'.format(varname=varname,note=multiline_comment(interface.note)))
                self._comment_dict[interface] = varname
                count += 1
        #iterate over methods
        name_map = {}
        interfaces = self(pyBoostMethod)
        for interface in interfaces:
            if interface.note:
                if interface.name not in name_map:
                    name_map[interface.name] = 1
                    count = 1
                else:
                    count = name_map[interface.name] + 1
                    name_map[interface.name] = count
                varname = 'comment_{name}_{method}_{count}'.format(name=self.name, method=interface.name, count=count)
                pf.write_line('const char* {varname} = "{note}";'.format(varname=varname,note=multiline_comment(interface.note)))
                self._comment_dict[interface] = varname
        pf.write_newline()

    def declare_overloads_by_default_arguments(self, pf):
        """When a member method has default arguments,
        the method generates overload calls. This
        procedure prepares the overload interceptor"""
        if self.require_wrapper:
            name = '{name}_wrapper'.format(name=self.name)
            scoped=''
        else:
            name = self.name
            scoped='{}::'.format(self._exported_class.scoped)
        interfaces = self(pyBoostMethod)
        self._method_overloads = {}
        self._overloads = 0
        for interface in interfaces:
            method = interface._exported_method
            arguments = method[Argument]
            index = len([x for x in arguments if not x._default])
            if index < len(arguments):
                self._overloads += 1
                dispatcher = '{class_name}_{index}'.format(class_name=name, index=self._overloads)
                scoped_method = '{scoped}{name}'.format(scoped=scoped,name=method.name)
                pf.write_line(
                    f'BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS({dispatcher}, {scoped_method}, {index}, {len(arguments)})')
                self._method_overloads[interface] = dispatcher

    @property
    def bases(self):
        base_list = ','.join(x._ancestor.scoped for x in self._inherits)
        if base_list:
            return 'bases<{}>'.format(base_list)
        return ''

    @property
    def docstring(self):
        return self._comment_dict.get(self, '')

    @property
    def attributes(self):
        """Declarative attributes for the class"""
        attr = [self._wrapper_class,self.bases]
        if self._non_copyable:
            attr.append('boost::noncopyable')
        return attr

    def write_code(self, pf):
        tabs = 1
        # write header of class
        attr = self.attributes
        pf.write_line('{prefix}class_<{attributes}>({init})'.format(
            prefix='    '*tabs,
            attributes=','.join([x for x in attr if len(x) > 0]),
            init=','.join([x for x in ['"{name}"'.format(name=self.name), self.docstring, 'no_init'] if len(x) > 0])))
        tabs = 2
        mems = self(pyBoostMember)
        for mem in mems:
            data = mem._exported_member
            note = multiline_comment(data.note)
            ti = data.type_instance
            if ti._const or ti._const_ptr:
                if note:
                    expr = '{prefix}.def_readonly("{name}",&{cls}::{name},"{note}")\n'
                else:
                    expr = '{prefix}.def_readonly("{name}",&{cls}::{name})\n'
            else:
                if note:
                    expr = '{prefix}.def_readwrite("{name}",&{cls}::{name},"{note}")\n'
                else:
                    expr = '{prefix}.def_readwrite("{name}",&{cls}::{name})\n'
            pf.write_line(expr.format(prefix='    ' * tabs, name=data.prefixed_name, cls=attr[0], note=note))
        ctors = self(pyBoostConstructor)
        for ctor in ctors:
            ctor.write_code(tabs, pf)
        methods = self(pyBoostMethod)
        for method in methods:
            method.write_code(tabs, pf)
        pf.write_line(";")
        # now, we must write the relations. For the moment, we skip dealing with overrides
        # so this solution may be not working all the cases, but even that, is a good achievement.
        for relation in self(pyBoostRelation):
            relation.write_code(self._wrapper_class, pf)


