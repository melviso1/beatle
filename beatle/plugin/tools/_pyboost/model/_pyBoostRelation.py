"""
Created on 1 oct. 2020

@author: mel

La mision de esta clase es reconocer los iteradores creados por beatle y proporcionar metodos
para exportarlos a python
"""

from beatle.model import TComponent


# noinspection PyPep8Naming
class pyBoostRelation(TComponent):
    """Represents the export of C++ class iterator through boost.python"""

    def __init__(self, **kwargs):
        self._exported_relation = kwargs['export_of']
        kwargs['name'] = self._exported_relation.name
        # self._exported_delete_method = None
        super(pyBoostRelation, self).__init__(**kwargs)

    @property
    def exported_relation(self):
        return self._exported_relation

    @property
    def tree_label(self):
        return self._exported_relation.tree_label

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("child")

    def write_code(self, container, pf):
        """Write the relations """
        to_class = self._exported_relation.key.to_class
        from_class = self._exported_relation.key.from_class
        wrap_class = "wrap_container<{container},{to},{iterator}>".format(
            container=container,
            to=to_class.scoped,
            iterator=from_class.scope+self._exported_relation.format('{alias_to}_iterator'))
        name = self._exported_relation.format('{to}s_in_{from}')
        pf.write_line(
            "\tclass_<{wrap_class},boost::noncopyable>(\"{name}\", no_init)".format(
                wrap_class=wrap_class,
                name=name
            ))
        pf.write_line(
            "        .def(\"__iter__\",bpy::iterator<{wrap_class}, return_internal_reference<>>())".format(
                wrap_class=wrap_class)
        )
        pf.write_line(";")
        pf.write_line(
            "    def(\"{to}_iterator\", &{wrap_class}::elements, return_value_policy<manage_new_object>())".format(
                to=self._exported_relation.format('{to}'),
                wrap_class=wrap_class
            )
        )
        pf.write_line(";")
