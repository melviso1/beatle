# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 23:24:30 2013

@author: mel
"""

import os

from beatle.lib.tran import TransactionStack, DelayedMethod
from beatle.model.writer import Writer
from beatle.model.cc import Module
from ._pyBoostClass import pyBoostClass
from ._pyBoostFunction import pyBoostFunction
from ._pyBoostRelation import pyBoostRelation
from ._pyBoostFromPythonConverter import pyBoostFromPythonConverter


# noinspection PyPep8Naming
class pyBoostModule(Module):
    """Implements a Module representation"""
    class_container = False
    folder_container = True
    diagram_container = True
    namespace_container = True
    function_container = True
    variable_container = True
    enum_container = True

    def __init__(self, **kwargs):
        """Initialization"""
        self._content = kwargs.get('content')
        super(pyBoostModule, self).__init__(**kwargs)

    def get_kwargs(self):
        """Returns the kwargs needed for this object"""
        kwargs = {'content': self._content}
        kwargs.update(super(pyBoostModule, self).get_kwargs())
        return kwargs

    @DelayedMethod()
    def export_code_files(self, force=True, logger=None):
        """Do code generation"""
        self.update_sources(force)

    def write_code(self, file_writer):
        """Write code for module"""
        # check if relations are required
        relations = self(pyBoostRelation)
        file_writer.write_line("#include <boost/python.hpp>")
        if len(relations) > 0:
            file_writer.write_line("#include <boost/python/iterator.hpp>")
        file_writer.write_newline(2)
        file_writer.write_line("namespace bpy = boost::python;")
        file_writer.write_line("using namespace bpy;")
        file_writer.write_newline(2)
        if len(relations) > 0:
            file_writer.write_line("//------------------------------------------------")
            file_writer.write_line("//       RELATION ITERATOR WRAPPER    ")
            file_writer.write_line("//------------------------------------------------")
            file_writer.write_line("""
template<class container, class element, class raw_iterator> class wrap_container
{
    struct wrap_iterator: public raw_iterator
    {
        using iterator = wrap_iterator;
        using value_type = element;
        using pointer = element*;
        using reference = element&;
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::bidirectional_iterator_tag;
    
        wrap_iterator(container *value)
        : raw_iterator(value)
        {;}
    
        bool operator==(const iterator& value)
        {
            return raw_iterator::get()==const_cast<iterator&>(value).get()? true: false;
        }
    
        bool operator!=(const iterator& value)
        {
            return raw_iterator::get()!=const_cast<iterator&>(value).get()? true: false;
        }
        
        pointer operator++()
        {
            return raw_iterator::operator++();
        }
        
        pointer operator++(int unused)
        {
            pointer value = raw_iterator::get();
            raw_iterator::operator++();
            return value;
        }
        
        pointer operator--()
        {
            return raw_iterator::operator--();
        }
        
        pointer operator--(int unused0)
        {
            pointer value = raw_iterator::get();
            raw_iterator::operator--();
            return value;
        }
    };

    container *_reference;
    
public:
    typedef wrap_iterator iterator;
    typedef element value_type;
    typedef element& reference;
    
    wrap_container(container *value)
    : _reference(value)
    {;}
    
    iterator begin()
    {
        iterator value = iterator(_reference);
        ++value;
        return value;
    }
    iterator end()
    {
        return iterator(_reference);
    }
    static wrap_container* elements(container* value)
    {
        return new wrap_container(value);
    }
};"""
                                   )
            file_writer.write_newline(2)
        file_writer.write_line("//------------------------------------------------")
        file_writer.write_line("//       TYPE CONVERSIONS FROM PYTHON    ")
        file_writer.write_line("//------------------------------------------------")
        file_writer.write_newline()
        conversions = self(pyBoostFromPythonConverter)
        for conversion in conversions:
            conversion.WriteConversion(file_writer);

        classes = self(pyBoostClass)
        for cls in classes:
            cls.before_write()
            cls.declare_comment_string(file_writer)
            cls.declare_wrapper(file_writer)
            cls.declare_overloads_by_default_arguments(file_writer)
        file_writer.write_line('BOOST_PYTHON_MODULE({self._name})'.format(self=self))
        file_writer.write_line("{")
        for conversion in conversions:
            conversion.RegisterConversion(file_writer);
        for cls in classes:
            cls.write_code(file_writer)
        functions = self(pyBoostFunction)
        for fn in functions:
            fn.write_code(file_writer)
        file_writer.write_line("}")

    def update_sources(self, force=False):
        """does source generation"""
        sources_dir = self.project.sources_dir
        fname = os.path.realpath(os.path.join(sources_dir, self._source))
        if force or not os.path.exists(fname) or self._lastSrcTime is None:
            regenerate = True
        elif os.path.getmtime(fname) < self._lastSrcTime:
            regenerate = True
        project = self.project
        if regenerate:
            with open(fname, 'wt', encoding='ascii', errors='ignore') as f:
                pf = Writer.for_file(f)
                pf.write_line("// {user.before.include.begin}")
                pf.write_line(getattr(self, "user_code_s1",""))
                pf.write_line("// {user.before.include.end}")
                pf.write_newline()
                if project.use_master_include:
                    pf.write_line('#include "{0}"'.format(project.master_include))
                    pf.write_newline()
                pf.write_line("// {user.before.code.begin}")
                pf.write_line(getattr(self, "user_code_s2",""))
                pf.write_line("// {user.before.code.end}")
                pf.write_newline()
                self.write_code(pf)
                pf.write_newline()
                pf.write_line("// {user.after.code.begin}")
                pf.write_line(getattr(self, "user_code_s3",""))
                pf.write_line("// {user.after.code.end}")
                pf.write_newline()

    def update_headers(self, force=False):
        """Realiza la generacion de fuentes"""
        pass

    @property
    def bitmap_index(self):
        """Index of tree image"""
        from beatle.app import resources as rc
        return rc.get_bitmap_index("pyboost")

    @property
    def inner_class(self):
        """Get the innermost class container"""
        return None

    def on_undo_redo_changed(self):
        """Update from app"""
        super(Module, self).on_undo_redo_changed()
        if not TransactionStack.in_undo_redo():
            self.project.export_code_files(force=True)
