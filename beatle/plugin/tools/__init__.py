# -*- coding: utf-8 -*-


from ._ast_explorer import AstExplorer
from ._log_explorer import log_explorer
from ._web_browser import WebBrowser
from ._check_updates import CheckUpdates
from ._cpp_rpc import RPCWizard
from ._pyboost import PythonBoostWizard
from ._doxygen import DoxygenWizard
from ._wxUI import  wxUI

# Registro de extensiones que figuran en la distribucion.
# Cada tupla esta formada por la clase principal de la extensión
# y el estado de activación inicial de dicha extensión.
# Las extensiones con valor False se encuentran ocultas por
# defecto.
ALL = [
    (AstExplorer, True, 'ast explorer'),  # navegador de estructura AST de ficheros python
    (log_explorer , False, 'log explorer'), # explorador de logs
    (WebBrowser , True, 'web browser'),   # navegador web embebido
    (CheckUpdates, True, 'check updates'),  # comprobacion de actualizaciones
    (RPCWizard, True, 'rpc wizard'),     # wizard para generacion de exportacion usando RPClib
    (PythonBoostWizard, True, 'pyboost wizard'), # wizard para exportacion de python utilizando boost.python
    (DoxygenWizard, True, 'doxygen support'), # wizard para generación de documentacion utilizando doxygen.
    ( wxUI, True, 'wxWidgets UI designer'),           # wizard para edición de ui utilizando wxWidgets.
]
