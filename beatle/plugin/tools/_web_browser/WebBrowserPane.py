# -*- coding: utf-8 -*-

from beatle.app.ui import pane


class WebBrowserPane(pane.NavigatorPane):
    """Implements web browser pane"""

    def __init__(self, parent, mainframe):
        """Intialization of method editor"""
        super(WebBrowserPane, self).__init__(parent, mainframe)
        self.m_url.Show(True)

    def on_enter_url(self, event):
        """Process enter text"""
        self._url = self.m_url.GetValue()
        self.m_page.LoadURL(self._url)

    def OnPageLoading(self, event):
        """The web resource is loading.
        This method can veto the event preventing the
        web page become loaded."""
        self.m_url.SetValue(event.GetURL())


