# -*- coding: utf-8 -*-
import wx

from beatle.lib import wxx
from beatle.lib.api import context
from beatle.lib.handlers import identifier

from .WebBrowserPane import WebBrowserPane


class WebBrowser(wx.EvtHandler):
    """Class for providing ast explorer"""
    instance = None

    def __init__(self):
        """Initialize web explorer"""
        if self.instance is not None:
            raise RuntimeError('Already initialized web browser.')
        self._web_browser = None
        self._web_browser_menu = None
        super(WebBrowser, self).__init__()
        self.append_tools_menu()

    def append_tools_menu(self):
        frame = context.get_frame()
        self._web_browser = identifier('ID_WEB_BROWSER')
        self._web_browser_menu = wxx.MenuItem(
            frame.menuTools, self._web_browser, u"Web browser",
            u"show embedded web browser", wx.ITEM_NORMAL)
        frame.add_tools_menu('Web browser', self._web_browser_menu)
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_web_browser, id=self._web_browser)
        self.Bind(wx.EVT_MENU, lambda x: self.open_web_browser(), id=self._web_browser)

    @classmethod
    def add_resources(cls):
        """The mission of this method is to add custom xpm resources to the tree"""
        pass

    @classmethod
    def load(cls):
        """Setup tool for the environment"""
        if cls.instance is None:
            cls.instance = WebBrowser()
        return cls.instance

    def on_update_web_browser(self, event):
        """Handle the update"""
        event.Enable(True)

    @staticmethod
    def open_web_browser():
        """Handle the command"""
        import beatle.app.resources as rc
        frame = context.get_frame()
        pane = WebBrowserPane(frame.docBook, frame)
        frame.docBook.Freeze()
        frame.docBook.AddPage(pane, 'web', True, rc.get_bitmap_index("info"))
        pane.NavigateTo('https://www.google.com/')
        frame.docBook.Thaw()
