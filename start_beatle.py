import platform

import wx

GTK_SO = None

if platform.system() == 'Linux':
    # remove conficts beetween IM and wx
    # if 'GTK_IM_MODULE' in os.environ:
    #     del os.environ['GTK_IM_MODULE']

    import gi
    import ctypes
    ctypes.CDLL('libX11.so').XInitThreads()
    gi.require_version("Gtk", "3.0")
    GTK_SO = ctypes.CDLL('libgtk-3.so')
    GTK_SO.gtk_disable_setlocale()

    from gi.repository import Gtk, Gdk
    default_screen = Gdk.Screen.get_default()
    if default_screen is not None:
        style_provider = Gtk.CssProvider()
        css = b"""
        @import url("resource:///org/gtk/libgtk/theme/Adwaita/gtk-contained-dark.css");
        """
        style_provider.load_from_data(css)
        Gtk.StyleContext.add_provider_for_screen(default_screen,
                                                 style_provider,
                                                 Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
elif platform.system() == 'Windows':
    from wx import SizerFlags
    wx.SizerFlags.DisableConsistencyChecks()

import beatle

beatle.app.BeatleInstance(0).MainLoop()
