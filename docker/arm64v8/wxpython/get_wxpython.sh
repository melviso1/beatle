#!/bin/bash
if [ ! -f ./wxPython.tgz ]; then
	if [ ! -f ./wxPython_ori.tgz ]; then
		git clone https://github.com/wxWidgets/Phoenix.git
		cd ./Phoenix
		git fetch 
		git checkout tags/wxPython-4.2.2
		git submodule update --init --recursive
		# create a temporal copy in order to avoid full rebuild on errors
		cd ..
		tar -czvf wxPython_ori.tgz Phoenix/	
		# end
	else
		tar -xzvf wxPython_ori.tgz
	fi
	cd ./Phoenix
	find . -type f -exec touch {} + 
	cp -rf ../patches/sip.h ./wx/include/wxPython/sip.h
	cp -rf ../patches/LICENSE.txt .
	patch -p1  < ../patches/phoenix.patch
	tar --directory=./sip -xzvf ../patches/siplib.tgz
	cp -rf ../patches/wx/locale/pt_BR/LC_MESSAGES/wxstd.mo
	find . -iname *.orig -exec rm {} \;
	cd ..
	tar -czvf wxPython.tgz Phoenix/
else
	tar -xzvf ./wxPython.tgz
fi	

