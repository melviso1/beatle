#!/bin/bash
# This is a script for custom building wxPython 
#!/bin/bash
if [ ! -d ./Phoenix ]; then
	./get_wxpython.sh
fi
cd Phoenix
DOXYGEN=/usr/bin/doxygen python3 build.py dox etg sip
python3 build.py build_wx --no_magic -j `nproc` 
sudo python3 build.py install_wx --no_magic
python3 build.py build_py --no_magic -j `nproc`
sudo python3 build.py install_py --no_magic
cd wx_ext
./build.sh
./install.sh
