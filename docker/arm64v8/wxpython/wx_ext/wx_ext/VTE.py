# -*- coding: utf-8 -*-
from . import _VTE
import wx


class VTE(wx.Panel):
    """
    """
    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.NO_BORDER, name="vte"):
        self._vte = None
        super(VTE, self).__init__(parent, id ,pos, size, style, name)
        self.SetBackgroundColour(wx.Colour(31, 108, 160, 255))
        self._vte = _VTE.VTE(self.GetHandle(), pos.x, pos.y, size.x, size.y, 0)
        parent.Bind(wx.EVT_CLOSE,self.on_close)
        self.Bind(wx.EVT_SIZE,self.on_size)
        
    def Feed(self, text):
        self._vte.Feed(text)

    def on_close(self, event):
        if self._vte:
            self._vte.Destroy()
            self._vte = None
        event.Skip()

    def on_size(self, event):
        if self._vte:
            sz = event.GetSize()
            self._vte.SetSize(sz.x,sz.y)
        event.Skip()
        
    def SetSize(self, size):
        super(VTE, self).SetSize(size)
        self._vte.SetSize(size.x,size.y)

    def Destroy(self):
        if self._vte:
            self._vte.Destroy()
            self._vte = None
        event.Skip()

    def on_close(self, event):
        if self._vte:
            self._vte.Destroy()
            self._vte = None
        event.Skip()

