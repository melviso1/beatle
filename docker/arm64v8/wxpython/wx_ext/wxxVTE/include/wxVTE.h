/*****************************************************************************************
 *
 * (c) 2025-02-26 by mel viso 
 *
 * This file is wxVTE.h, part of the project wxxVTE.
 * This project is licensed under the terms of the wxWindows Library License 3.1 license.
 *
 * Read the companion LICENSE file for further details.
 *
 ****************************************************************************************/

#if !defined(WXVTE_H_INCLUDED)
#define WXVTE_H_INCLUDED


// {user.before.class wxVTE.begin}
#include <vte/vte.h>
#include <list>
#include <map>
#include <wx/wx.h>

extern IMPEXP const char wxVTENameStr[];
// {user.before.class wxVTE.end}


class wxVTE : public wxControl
{

    // {user.inside.first.class wxVTE.begin}
    // {user.inside.first.class wxVTE.end}

    public:
    wxVTE();
    wxVTE(wxWindow* parent, wxWindowID id=wxID_ANY, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxNO_BORDER, const wxString& name=wxASCII_STR(wxVTENameStr));
    private:
    bool _closing ;
    public:
    bool _ready ;
    /**
    * References while the client is not ready
    **/
    std::list<wxString> _feed ;
    private:
    /**
    * Records instances
    **/
    static std::map<GtkWidget*, wxVTE*> _record ;
    GtkWidget* _key ;
    public:
    inline const bool is_closing() const;
    inline const bool is_ready() const;
    protected:
    inline void set_ready(const bool ready);
    public:
    virtual void InitDialog();
    bool Create(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name);
    void StartShell();
    static void child_ready(VteTerminal* terminal, GPid pid, GError* error, gpointer user_data);
    bool Feed(const wxString& text);
    void on_exit_child();
    static wxVTE* Lookup(GtkWidget* widget);
    void SetSize(int sx, int sy);
    virtual bool Destroy();
    virtual ~wxVTE();
    // {user.inside.last.class wxVTE.begin}
    wxDECLARE_DYNAMIC_CLASS(wxVTE);
    wxDECLARE_EVENT_TABLE();
    // {user.inside.last.class wxVTE.end}
};

// {user.after.class wxVTE.begin}
// {user.after.class wxVTE.end}

#endif //WXVTE_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_WXVTE)
#define INCLUDE_INLINES_WXVTE



inline const bool wxVTE::is_closing() const
{
    return _closing;
}


inline const bool wxVTE::is_ready() const
{
    return _ready;
}


inline void wxVTE::set_ready(const bool ready)
{
    if(_ready==ready)
    {
        return;
    }
    _ready=ready;
    if(!_ready)
    {
        return;
    }
    for(auto& pending: _feed )
    {
        Feed(pending);
    }
    _feed.clear();    
    
    
}

#endif //(INCLUDE_INLINES_WXVTE)
#endif //INCLUDE_INLINES


