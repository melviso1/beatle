/*****************************************************************************************
 *
 * (c) 2025-02-26 by mel viso 
 *
 * This file is wxVTE.cpp, part of the project wxxVTE.
 * This project is licensed under the terms of the wxWindows Library License 3.1 license.
 *
 * Read the companion LICENSE file for further details.
 *
 ****************************************************************************************/

// {user.before.include.begin}
// {user.before.include.end}

#include "wxxVTE.h"

#include <assert.h>

// {user.before.class.wxVTE.begin}
#include<thread>
#include <sys/types.h>
#include <sys/wait.h>

extern IMPEXP const char wxVTENameStr[] = "vte";

wxIMPLEMENT_DYNAMIC_CLASS(wxVTE, wxControl);

wxBEGIN_EVENT_TABLE(wxVTE, wxControl)
//    EVT_TIMER(wxID_ANY, wxAnimationCtrl::OnTimer)
wxEND_EVENT_TABLE()

extern "C"
{
    static void
    wxgtk_vte_style_set_callback(GtkWidget* widget, GtkStyle*, wxButton* win)
    {
        /* the default button has a border around it */
        wxWindow* parent = win->GetParent();
        if (parent && parent->m_wxwindow && gtk_widget_get_can_default(widget))
        {
            GtkBorder* border = NULL;
            gtk_widget_style_get(widget, "default_border", &border, NULL);
            if (border)
            {
                win->MoveWindow(
                    win->m_x - border->left,
                    win->m_y - border->top,
                    win->m_width + border->left + border->right,
                    win->m_height + border->top + border->bottom);
                gtk_border_free(border);
            }
        }
    }
    static void wxgtk_vte_child_exited_callback(GtkWidget* widget, int status)
    {
        wxVTE * self = wxVTE::Lookup(widget);
        if( self != nullptr )
            self->on_exit_child();
    }
}
// {user.before.class.wxVTE.end}

std::map<GtkWidget*, wxVTE*> wxVTE::_record;
/**
* Constructor.
**/
wxVTE::wxVTE()
:    wxControl{}
,    _closing{false}
,    _ready{false}
,    _feed{ }
,    _key{nullptr}
{
}

/**
* Constructor.
**/
wxVTE::wxVTE(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
:    wxControl{}
,    _closing{false}
,    _ready{false}
,    _feed{ }
,    _key{nullptr}
{
    Create(parent, id, pos, size, style, name);
}



void wxVTE::InitDialog()
{
    wxInitDialogEvent event(GetId());
    event.SetEventObject(this);
    GetEventHandler()->ProcessEvent(event);
}


bool wxVTE::Create(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
{
        if (!PreCreation( parent, pos, size ) ||
            !CreateBase( parent, id, pos, size, style, name ))
        {
            wxFAIL_MSG( wxT("wxVTE creation failed") );
            return false;
        }
        m_widget = _key = vte_terminal_new();
        g_object_ref(m_widget);
        _record[m_widget] = this;
        StartShell();
    
    
        //  to do
        // g_signal_connect(window, "delete-event", G_CALLBACK (wxgtk_button_clicked_callback), this);
        g_signal_connect(m_widget, "child-exited", G_CALLBACK (wxgtk_vte_child_exited_callback), this);
        g_signal_connect_after (m_widget, "style_set", G_CALLBACK (wxgtk_vte_style_set_callback), this);
        
        m_parent->DoAddChild( this );
    
        PostCreation(size);
    
        return true;
}


void wxVTE::StartShell()
{
    gchar **envp = g_get_environ();
    gchar *command[] = {g_strdup(g_environ_getenv(envp, "SHELL")), NULL };
    g_strfreev(envp);
    vte_terminal_spawn_async(VTE_TERMINAL(m_widget),
            VTE_PTY_DEFAULT,
            NULL,                                   /* working directory  */
            command,                                /* command */
            NULL,                                   /* environment */
            static_cast<GSpawnFlags>(0),            /* spawn flags */
            NULL, NULL,                             /* child setup */
            NULL,                                   /* child pid */
            -1,                                     /* timeout */
            NULL,                                   /* cancellable */
            child_ready,                            /* callback */
            this);                                  /* user_data */
    gtk_widget_show_all(m_widget);
    if(_ready)
    {
        for(auto& pending: _feed )
        {
            Feed(pending);
        }
        _feed.clear();    
    
    }
}


void wxVTE::child_ready(VteTerminal* terminal, GPid pid, GError* error, gpointer user_data)
{
    if (!terminal) 
    {
        return;
    }
    auto self = static_cast<wxVTE *>(user_data);
    if( self == nullptr )
    {
        return;
    }
    if (pid == -1) 
    {
        if( !self->is_closing() )
        {
            self->StartShell();
        }
        return;
    }
    
    if(self->_ready || self->is_closing() )
        return;
    self->set_ready(true);
}


/**
* Envia texto al terminal.
**/
bool wxVTE::Feed(const wxString& text)
{
    auto terminal = VTE_TERMINAL(m_widget);
    if( terminal == nullptr )
    {
        return false;
    }
    if( !is_ready() )
    {
        _feed.push_back(text);
    }
    else
    {
        wxCharBuffer buffer{text.ToUTF8()};
        vte_terminal_feed_child(terminal, buffer.data(), static_cast<gssize>(text.Length()));
    }
    return true;
}


void wxVTE::on_exit_child()
{
    if( !is_closing() )
    {
        StartShell(); 
    }
}


wxVTE* wxVTE::Lookup(GtkWidget* widget)
{
    auto pos = _record.find(widget);
    if ( pos == _record.end() )
    {
        return nullptr;
    }
    return pos->second;
}


void wxVTE::SetSize(int sx, int sy)
{
        wxControl::SetSize(sx,sy);
}


bool wxVTE::Destroy()
{
    if(_key!=nullptr)
    {
        _key==nullptr;
        _closing = true;
        wxControl::Close();
    }
    return true;
}
/**
* Destructor.
**/
wxVTE::~wxVTE()
{
    Destroy();
}

// {user.after.class.wxVTE.begin}
// {user.after.class.wxVTE.end}


