/*****************************************************************************************
 *
 * (c) 2025-02-27 by mel viso 
 *
 * This file is VTE.h, part of the project _VTE.
 * This project is licensed under the terms of the wxWindows Library License 3.1 license.
 *
 * Read the companion LICENSE file for further details.
 *
 ****************************************************************************************/

#if !defined(VTE_H_INCLUDED)
#define VTE_H_INCLUDED


// {user.before.class VTE.begin}
// {user.before.class VTE.end}


class VTE : public wxVTE
{

    // {user.inside.first.class VTE.begin}
    // {user.inside.first.class VTE.end}

    public:
    VTE(size_t parent, int x, int y, int sx, int sy, long style);
    bool Feed(const std::string& str);
    static const wxPoint& make_point(int x, int y);
    static const wxSize& make_size(int sx, int sy);
    void SetSize(int sx, int sy);
    virtual bool Destroy();
    virtual ~VTE();
    // {user.inside.last.class VTE.begin}
    // {user.inside.last.class VTE.end}
};

// {user.after.class VTE.begin}
// {user.after.class VTE.end}

#endif //VTE_H_INCLUDED



