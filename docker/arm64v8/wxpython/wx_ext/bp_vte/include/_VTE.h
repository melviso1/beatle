/***

 File    :_VTE.h
 Created :28/02/2025 17:09:33
 Author  :mel viso

 Licensed under wxWindows Library License 3.1

***/

#if !defined(_VTE_H_INCLUDED)
#define _VTE_H_INCLUDED

//standard includes
/**
context INCLUDES:
**/

#include <wx/wx.h> 
#include <../../wxxVTE/include/wxxVTE.h>


//forward references
class VTE;

#include "VTE.h"

#define INCLUDE_INLINES
#include "VTE.h"


#endif //_VTE_H_INCLUDED

