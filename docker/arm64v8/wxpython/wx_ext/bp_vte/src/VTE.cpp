/*****************************************************************************************
 *
 * (c) 2025-02-27 by mel viso 
 *
 * This file is VTE.cpp, part of the project _VTE.
 * This project is licensed under the terms of the wxWindows Library License 3.1 license.
 *
 * Read the companion LICENSE file for further details.
 *
 ****************************************************************************************/

// {user.before.include.begin}
// {user.before.include.end}

#include "_VTE.h"

#include <assert.h>

// {user.before.class.VTE.begin}
// {user.before.class.VTE.end}

/**
* Constructor.
**/
VTE::VTE(size_t parent, int x, int y, int sx, int sy, long style)
:    wxVTE{
        reinterpret_cast<wxWindow*>(parent),
        wxID_ANY,
        VTE::make_point(x,y),
        VTE::make_size(sx,sy),style}
{
}



bool VTE::Feed(const std::string& str)
{
    wxString wxs(str.c_str(), wxConvUTF8);
    return wxVTE::Feed(wxs);
}


const wxPoint& VTE::make_point(int x, int y)
{
    static wxPoint p{wxDefaultPosition};
    if( x==y && x==-1)
    {
        return wxDefaultPosition;
    }
    p.x=x;
    p.y=y;
    return p;
}


const wxSize& VTE::make_size(int sx, int sy)
{
    static wxSize s{wxDefaultSize};
    if( sx==sy && sx==-1)
    {
        return wxDefaultSize;
    }
    s.x=sx;
    s.y=sy;
    return s;
}


void VTE::SetSize(int sx, int sy)
{
    wxVTE::SetSize(sx,sy);
}


bool VTE::Destroy()
{
    return wxVTE::Destroy();
}
/**
* Destructor.
**/
VTE::~VTE()
{
}

// {user.after.class.VTE.begin}
// {user.after.class.VTE.end}


