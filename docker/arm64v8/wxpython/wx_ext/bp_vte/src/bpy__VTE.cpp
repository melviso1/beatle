// {user.before.include.begin}
// {user.before.include.end}

#include "_VTE.h"

// {user.before.code.begin}
// {user.before.code.end}

#include <boost/python.hpp>


namespace bpy = boost::python;
using namespace bpy;


//------------------------------------------------
//       TYPE CONVERSIONS FROM PYTHON    
//------------------------------------------------

//------------------------------------------------
//       DOCSTRINGS FOR CLASS VTE
//------------------------------------------------
const char* comment_ctor_VTE_1 = "Constructor.";

BOOST_PYTHON_MODULE(_VTE)
{
    class_<VTE,boost::noncopyable>("VTE",no_init)
        .def(bpy::init<size_t , int , int , int , int , long >((bpy::arg("parent"), bpy::arg("x"), bpy::arg("y"), bpy::arg("sx"), bpy::arg("sy"), bpy::arg("style")),comment_ctor_VTE_1)[return_value_policy<reference_existing_object>()])
        .def("Feed", static_cast<bool  (VTE::*)(const std::string& )>(&VTE::Feed))
        .def("SetSize", static_cast<void  (VTE::*)(int ,int )>(&VTE::SetSize))
        .def("Destroy", static_cast<bool  (VTE::*)()>(&VTE::Destroy))
;
}

// {user.after.code.begin}
// {user.after.code.end}

