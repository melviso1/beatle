#!/bin/bash
docker build -t beatle3.alpine.raw .
docker-squash-python -v -t melviso/beatle3.alpine:latest beatle3.alpine.raw:latest 
docker image rm beatle3.alpine.raw:latest 
docker image prune -f
