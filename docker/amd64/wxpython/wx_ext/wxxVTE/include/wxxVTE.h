/***

 File    :wxxVTE.h
 Created :28/02/2025 16:34:51
 Author  :mel viso

 Licensed under wxWindows Library License 3.1

***/

#if !defined(WXXVTE_H_INCLUDED)
#define WXXVTE_H_INCLUDED

//standard includes
/**
context defines:
**/

#define EXPORT /* __attribute__((dllexport))
*/
#define IMPORT /* __attribute__((dllimport))
 */
#if defined(BUILD_TEST)
    #define IMPEXP 
#else
    #if defined(BUILDING_wxVTE)
        #define IMPEXP EXPORT
   #else
        #define IMPEXP IMPORT
  #endif
#endif
#define wxUSE_EXTENDED_RTTI 0

//type definition for wxWindowID
/* unspecified, dynamically created */
//type definition for wxString
/* unspecified, dynamically created */
//type definition for wxPoint
/* unspecified, dynamically created */
//type definition for wxSize
/* unspecified, dynamically created */

//forward references
class wxVTE;

#include "wxVTE.h"

#define INCLUDE_INLINES
#include "wxVTE.h"


#endif //WXXVTE_H_INCLUDED

